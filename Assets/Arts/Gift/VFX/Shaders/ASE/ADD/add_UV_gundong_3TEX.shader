// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE_VFX/ADD_UV_gundong_zhezhao_3tex"
{
	Properties
	{
		_main_Tex("main_Tex", 2D) = "white" {}
		[HDR]_Color0("Color 0", Color) = (1,1,1,1)
		_mask_TEX("mask_TEX", 2D) = "white" {}
		_ruanbian_kaiguan("ruanbian_kaiguan", Float) = 0
		_main_Tex_speed("main_Tex_speed", Vector) = (0,0,0,0)
		_mask_TEX_speed("mask_TEX_speed", Vector) = (0,0,0,0)
		_Mask_02TEX_speed("Mask_02TEX_speed", Vector) = (0,0,0,0)
		_main_Tex_tiling("main_Tex_tiling", Vector) = (0,0,0,0)
		_mask_TEX_tiling("mask_TEX_tiling", Vector) = (0,0,0,0)
		_Mask_02TEX_Tiling("Mask_02TEX_Tiling", Vector) = (0,0,0,0)
		_mask_02("mask_02", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		ZWrite Off
		Blend One One
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		#pragma target 3.0
		#pragma surface surf Unlit keepalpha addshadow fullforwardshadows nofog 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float4 screenPos;
		};

		uniform sampler2D _main_Tex;
		uniform float2 _main_Tex_speed;
		uniform float2 _main_Tex_tiling;
		uniform sampler2D _mask_TEX;
		uniform float2 _mask_TEX_speed;
		uniform float2 _mask_TEX_tiling;
		uniform sampler2D _mask_02;
		uniform float2 _Mask_02TEX_speed;
		uniform float2 _Mask_02TEX_Tiling;
		uniform float4 _Color0;
		UNITY_DECLARE_DEPTH_TEXTURE( _CameraDepthTexture );
		uniform float4 _CameraDepthTexture_TexelSize;
		uniform float _ruanbian_kaiguan;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_TexCoord53 = i.uv_texcoord * _main_Tex_tiling;
			float2 panner45 = ( 1.0 * _Time.y * _main_Tex_speed + uv_TexCoord53);
			float2 uv_TexCoord54 = i.uv_texcoord * _mask_TEX_tiling;
			float2 panner46 = ( 1.0 * _Time.y * _mask_TEX_speed + uv_TexCoord54);
			float2 uv_TexCoord61 = i.uv_texcoord * _Mask_02TEX_Tiling;
			float2 panner63 = ( 1.0 * _Time.y * _Mask_02TEX_speed + uv_TexCoord61);
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float screenDepth42 = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE( _CameraDepthTexture, ase_screenPosNorm.xy ));
			float distanceDepth42 = saturate( abs( ( screenDepth42 - LinearEyeDepth( ase_screenPosNorm.z ) ) / ( _ruanbian_kaiguan ) ) );
			o.Emission = ( ( ( tex2D( _main_Tex, panner45 ) * tex2D( _mask_TEX, panner46 ) * tex2D( _mask_02, panner63 ) ) * i.vertexColor * i.vertexColor.a * _Color0 * _Color0.a ) * distanceDepth42 ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18000
2121;32;1920;994;1577.074;464.2923;1;True;True
Node;AmplifyShaderEditor.Vector2Node;60;-1764.879,135.2072;Float;False;Property;_Mask_02TEX_Tiling;Mask_02TEX_Tiling;10;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;58;-1756.451,-63.89867;Float;False;Property;_mask_TEX_tiling;mask_TEX_tiling;9;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;57;-1735.451,-342.0638;Float;False;Property;_main_Tex_tiling;main_Tex_tiling;8;0;Create;True;0;0;False;0;0,0;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;62;-1312.06,210.6135;Float;False;Property;_Mask_02TEX_speed;Mask_02TEX_speed;7;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;52;-1303.632,11.50806;Float;False;Property;_mask_TEX_speed;mask_TEX_speed;6;0;Create;True;0;0;False;0;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;54;-1524.451,-82.89874;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;61;-1532.879,116.2072;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;53;-1501.451,-360.064;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;51;-1295.472,-212.0713;Float;False;Property;_main_Tex_speed;main_Tex_speed;5;0;Create;True;0;0;False;0;0,0;-1,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PannerNode;46;-1047.921,-81.05753;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;45;-1052.556,-271.0712;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;63;-1040.349,118.0484;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;1;-864.0643,-297.693;Inherit;True;Property;_main_Tex;main_Tex;1;0;Create;True;0;0;False;0;-1;None;b278eb957db108e4ba9c248f9fc41d44;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;43;-861.9533,-108.9024;Inherit;True;Property;_mask_TEX;mask_TEX;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;59;-858.0441,89.51884;Inherit;True;Property;_mask_02;mask_02;11;0;Create;True;0;0;False;0;-1;None;ce40c5aafbca71643a00c872b5618f22;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;4;-517.2358,-12.46301;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;-547.6097,142.8984;Float;False;Property;_Color0;Color 0;2;1;[HDR];Create;True;0;0;False;0;1,1,1,1;2,1.537255,0.6196079,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;28;-541.6949,308.9545;Float;False;Property;_ruanbian_kaiguan;ruanbian_kaiguan;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-502.6398,-126.2932;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DepthFade;42;-310.1951,290.6801;Inherit;False;True;True;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-275.3433,-47.33712;Inherit;False;5;5;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;4;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;40;-69.73651,113.6857;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;68.6792,67.43808;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;ASE_VFX/ADD_UV_gundong_zhezhao_3tex;False;False;False;False;False;False;False;False;False;True;False;False;False;False;True;False;False;False;False;False;False;Back;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;4;1;False;-1;1;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;54;0;58;0
WireConnection;61;0;60;0
WireConnection;53;0;57;0
WireConnection;46;0;54;0
WireConnection;46;2;52;0
WireConnection;45;0;53;0
WireConnection;45;2;51;0
WireConnection;63;0;61;0
WireConnection;63;2;62;0
WireConnection;1;1;45;0
WireConnection;43;1;46;0
WireConnection;59;1;63;0
WireConnection;44;0;1;0
WireConnection;44;1;43;0
WireConnection;44;2;59;0
WireConnection;42;0;28;0
WireConnection;2;0;44;0
WireConnection;2;1;4;0
WireConnection;2;2;4;4
WireConnection;2;3;24;0
WireConnection;2;4;24;4
WireConnection;40;0;2;0
WireConnection;40;1;42;0
WireConnection;0;2;40;0
ASEEND*/
//CHKSM=4F54CA08975B272C57098EC86D3FDA473507B875