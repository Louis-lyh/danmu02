// Shader created with Shader Forge v1.40 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.40;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,cpap:False,lico:0,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:False,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:7989,x:33255,y:32676,varname:node_7989,prsc:2|custl-7672-OUT,alpha-2246-OUT;n:type:ShaderForge.SFN_Tex2d,id:1962,x:31991,y:32830,ptovrint:False,ptlb:Main_tex,ptin:_Main_tex,varname:node_1962,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-5256-OUT;n:type:ShaderForge.SFN_Tex2d,id:705,x:32005,y:33194,ptovrint:False,ptlb:Mask_tex,ptin:_Mask_tex,varname:node_705,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-4503-OUT;n:type:ShaderForge.SFN_VertexColor,id:6345,x:32001,y:32295,varname:node_6345,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:4811,x:31333,y:32656,varname:node_4811,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:3638,x:31129,y:32903,varname:node_3638,prsc:2|A-2911-OUT,B-2419-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2911,x:30953,y:32903,ptovrint:False,ptlb:Main_u,ptin:_Main_u,varname:node_2911,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:2419,x:30953,y:32973,ptovrint:False,ptlb:Main_v,ptin:_Main_v,varname:node_2419,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:1986,x:31333,y:32809,varname:node_1986,prsc:2|A-4918-T,B-3638-OUT;n:type:ShaderForge.SFN_TexCoord,id:3618,x:31614,y:33194,varname:node_3618,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:4418,x:31403,y:33381,varname:node_4418,prsc:2|A-1740-OUT,B-7684-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1740,x:31200,y:33381,ptovrint:False,ptlb:Mask_u,ptin:_Mask_u,varname:_Main_u_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:7684,x:31200,y:33466,ptovrint:False,ptlb:Mask_v,ptin:_Mask_v,varname:_Main_v_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:5923,x:31614,y:33381,varname:node_5923,prsc:2|A-4418-OUT,B-2635-T;n:type:ShaderForge.SFN_Multiply,id:7672,x:32795,y:32522,varname:node_7672,prsc:2|A-6345-RGB,B-1343-OUT,C-1962-RGB,D-705-RGB,E-6875-OUT;n:type:ShaderForge.SFN_Multiply,id:2246,x:32991,y:32954,varname:node_2246,prsc:2|A-6345-A,B-1962-A,C-705-A,D-6875-OUT,E-6157-A;n:type:ShaderForge.SFN_TexCoord,id:3272,x:31129,y:33047,varname:node_3272,prsc:2,uv:1,uaff:True;n:type:ShaderForge.SFN_Append,id:8386,x:31353,y:33057,varname:node_8386,prsc:2|A-3272-Z,B-3272-W;n:type:ShaderForge.SFN_Add,id:7814,x:31647,y:32914,varname:node_7814,prsc:2|A-4811-UVOUT,B-8386-OUT;n:type:ShaderForge.SFN_Time,id:4918,x:31129,y:32780,varname:node_4918,prsc:2;n:type:ShaderForge.SFN_Add,id:2001,x:31647,y:32767,varname:node_2001,prsc:2|A-4811-UVOUT,B-1986-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:5256,x:31831,y:32830,ptovrint:False,ptlb:clampUV_open,ptin:_clampUV_open,varname:node_5256,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-2001-OUT,B-7814-OUT;n:type:ShaderForge.SFN_Color,id:6157,x:31979,y:32484,ptovrint:False,ptlb:color,ptin:_color,varname:node_6157,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7879,x:32004,y:33595,ptovrint:False,ptlb:Eroison_tex,ptin:_Eroison_tex,varname:node_7879,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-4773-OUT;n:type:ShaderForge.SFN_Add,id:4503,x:31806,y:33194,varname:node_4503,prsc:2|A-3618-UVOUT,B-5923-OUT;n:type:ShaderForge.SFN_Time,id:2635,x:31404,y:33530,varname:node_2635,prsc:2;n:type:ShaderForge.SFN_TexCoord,id:8744,x:31617,y:33595,varname:node_8744,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Append,id:832,x:31419,y:33782,varname:node_832,prsc:2|A-1280-OUT,B-9959-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1280,x:31216,y:33782,ptovrint:False,ptlb:Eroison_u,ptin:_Eroison_u,varname:_Mask_u_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:9959,x:31216,y:33867,ptovrint:False,ptlb:Eroison_v,ptin:_Eroison_v,varname:_Mask_v_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:8949,x:31617,y:33782,varname:node_8949,prsc:2|A-832-OUT,B-6791-T;n:type:ShaderForge.SFN_Add,id:4773,x:31809,y:33595,varname:node_4773,prsc:2|A-8744-UVOUT,B-8949-OUT;n:type:ShaderForge.SFN_Time,id:6791,x:31419,y:33941,varname:node_6791,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3048,x:32329,y:33676,varname:node_3048,prsc:2|A-7879-A,B-3413-OUT;n:type:ShaderForge.SFN_ValueProperty,id:3413,x:32004,y:33832,ptovrint:False,ptlb:soft,ptin:_soft,varname:node_3413,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:7740,x:32329,y:33911,varname:node_7740,prsc:2|A-53-OUT,B-3413-OUT,T-6882-OUT;n:type:ShaderForge.SFN_Vector1,id:53,x:32004,y:33942,varname:node_53,prsc:2,v1:-1.5;n:type:ShaderForge.SFN_Subtract,id:9619,x:32621,y:33790,varname:node_9619,prsc:2|A-3048-OUT,B-7740-OUT;n:type:ShaderForge.SFN_Clamp01,id:6875,x:32815,y:33790,varname:node_6875,prsc:2|IN-9619-OUT;n:type:ShaderForge.SFN_Multiply,id:1343,x:32216,y:32643,varname:node_1343,prsc:2|A-6157-RGB,B-6170-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6170,x:31991,y:32689,ptovrint:False,ptlb:color_with,ptin:_color_with,varname:node_6170,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_SwitchProperty,id:6882,x:32004,y:34027,ptovrint:False,ptlb:model,ptin:_model,varname:node_6882,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3272-U,B-5881-OUT;n:type:ShaderForge.SFN_Slider,id:5881,x:31617,y:34044,ptovrint:False,ptlb:Eroison_with,ptin:_Eroison_with,varname:node_5881,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;proporder:1962-705-2911-2419-1740-7684-5256-6157-7879-1280-9959-3413-6170-6882-5881;pass:END;sub:END;*/

Shader "zg/zg_add" {
    Properties {
        _Main_tex ("Main_tex", 2D) = "white" {}
        _Mask_tex ("Mask_tex", 2D) = "white" {}
        _Main_u ("Main_u", Float ) = 0
        _Main_v ("Main_v", Float ) = 0
        _Mask_u ("Mask_u", Float ) = 0
        _Mask_v ("Mask_v", Float ) = 0
        [MaterialToggle] _clampUV_open ("clampUV_open", Float ) = 0
        _color ("color", Color) = (0.5,0.5,0.5,1)
        _Eroison_tex ("Eroison_tex", 2D) = "white" {}
        _Eroison_u ("Eroison_u", Float ) = 0
        _Eroison_v ("Eroison_v", Float ) = 0
        _soft ("soft", Float ) = 0
        _color_with ("color_with", Float ) = 1
        [MaterialToggle] _model ("model", Float ) = 0
        _Eroison_with ("Eroison_with", Range(0, 1)) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        LOD 100
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d11 gles3 metal 
            #pragma target 3.0
            uniform sampler2D _Main_tex; uniform float4 _Main_tex_ST;
            uniform sampler2D _Mask_tex; uniform float4 _Mask_tex_ST;
            uniform sampler2D _Eroison_tex; uniform float4 _Eroison_tex_ST;
            UNITY_INSTANCING_BUFFER_START( Props )
                UNITY_DEFINE_INSTANCED_PROP( float, _Main_u)
                UNITY_DEFINE_INSTANCED_PROP( float, _Main_v)
                UNITY_DEFINE_INSTANCED_PROP( float, _Mask_u)
                UNITY_DEFINE_INSTANCED_PROP( float, _Mask_v)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _clampUV_open)
                UNITY_DEFINE_INSTANCED_PROP( float4, _color)
                UNITY_DEFINE_INSTANCED_PROP( float, _Eroison_u)
                UNITY_DEFINE_INSTANCED_PROP( float, _Eroison_v)
                UNITY_DEFINE_INSTANCED_PROP( float, _soft)
                UNITY_DEFINE_INSTANCED_PROP( float, _color_with)
                UNITY_DEFINE_INSTANCED_PROP( fixed, _model)
                UNITY_DEFINE_INSTANCED_PROP( float, _Eroison_with)
            UNITY_INSTANCING_BUFFER_END( Props )
            struct VertexInput {
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 texcoord1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                float2 uv0 : TEXCOORD0;
                float4 uv1 : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                UNITY_SETUP_INSTANCE_ID( v );
                UNITY_TRANSFER_INSTANCE_ID( v, o );
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID( i );
////// Lighting:
                float4 _color_var = UNITY_ACCESS_INSTANCED_PROP( Props, _color );
                float _color_with_var = UNITY_ACCESS_INSTANCED_PROP( Props, _color_with );
                float4 node_4918 = _Time;
                float _Main_u_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Main_u );
                float _Main_v_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Main_v );
                float2 _clampUV_open_var = lerp( (i.uv0+(node_4918.g*float2(_Main_u_var,_Main_v_var))), (i.uv0+float2(i.uv1.b,i.uv1.a)), UNITY_ACCESS_INSTANCED_PROP( Props, _clampUV_open ) );
                float4 _Main_tex_var = tex2D(_Main_tex,TRANSFORM_TEX(_clampUV_open_var, _Main_tex));
                float _Mask_u_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Mask_u );
                float _Mask_v_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Mask_v );
                float4 node_2635 = _Time;
                float2 node_4503 = (i.uv0+(float2(_Mask_u_var,_Mask_v_var)*node_2635.g));
                float4 _Mask_tex_var = tex2D(_Mask_tex,TRANSFORM_TEX(node_4503, _Mask_tex));
                float _Eroison_u_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Eroison_u );
                float _Eroison_v_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Eroison_v );
                float4 node_6791 = _Time;
                float2 node_4773 = (i.uv0+(float2(_Eroison_u_var,_Eroison_v_var)*node_6791.g));
                float4 _Eroison_tex_var = tex2D(_Eroison_tex,TRANSFORM_TEX(node_4773, _Eroison_tex));
                float _soft_var = UNITY_ACCESS_INSTANCED_PROP( Props, _soft );
                float _Eroison_with_var = UNITY_ACCESS_INSTANCED_PROP( Props, _Eroison_with );
                float _model_var = lerp( i.uv1.r, _Eroison_with_var, UNITY_ACCESS_INSTANCED_PROP( Props, _model ) );
                float node_6875 = saturate(((_Eroison_tex_var.a*_soft_var)-lerp((-1.5),_soft_var,_model_var)));
                float3 finalColor = (i.vertexColor.rgb*(_color_var.rgb*_color_with_var)*_Main_tex_var.rgb*_Mask_tex_var.rgb*node_6875);
                return fixed4(finalColor,(i.vertexColor.a*_Main_tex_var.a*_Mask_tex_var.a*node_6875*_color_var.a));
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
