// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ASE_VFX/ADD_tuowei_mod"
{
	Properties
	{
		_main_Tex("main_Tex", 2D) = "white" {}
		[HDR]_Color0("Color 0", Color) = (0,0,0,0)
		_zhezhao_01("zhezhao_01", 2D) = "white" {}
		_zhezhao_02("zhezhao_02", 2D) = "white" {}
		[Toggle(_KEYWORD0_ON)] _Keyword0("Keyword 0", Float) = 0
		_Uspeed("U speed", Float) = 0
		_Vspeed("V speed", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		ZWrite Off
		Blend One One
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma shader_feature _KEYWORD0_ON
		#pragma exclude_renderers vulkan xbox360 xboxone ps4 psp2 n3ds wiiu 
		#pragma surface surf StandardSpecular keepalpha noshadow nofog 
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _main_Tex;
		uniform float4 _main_Tex_ST;
		uniform float _Uspeed;
		uniform float _Vspeed;
		uniform float4 _Color0;
		uniform sampler2D _zhezhao_01;
		uniform float4 _zhezhao_01_ST;
		uniform sampler2D _zhezhao_02;
		uniform float4 _zhezhao_02_ST;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv0_main_Tex = i.uv_texcoord * _main_Tex_ST.xy + _main_Tex_ST.zw;
			float2 appendResult55 = (float2(( ( _Uspeed * _Time.y ) + uv0_main_Tex.x ) , ( uv0_main_Tex.y + ( _Time.y * _Vspeed ) )));
			#ifdef _KEYWORD0_ON
				float2 staticSwitch58 = appendResult55;
			#else
				float2 staticSwitch58 = uv0_main_Tex;
			#endif
			float2 uv_zhezhao_01 = i.uv_texcoord * _zhezhao_01_ST.xy + _zhezhao_01_ST.zw;
			float2 uv_zhezhao_02 = i.uv_texcoord * _zhezhao_02_ST.xy + _zhezhao_02_ST.zw;
			o.Emission = ( tex2D( _main_Tex, staticSwitch58 ) * _Color0 * tex2D( _zhezhao_01, uv_zhezhao_01 ).r * tex2D( _zhezhao_02, uv_zhezhao_02 ).b * i.vertexColor * i.vertexColor.a * _Color0.a ).rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18000
2121;32;1920;994;2669.396;716.9214;1.172352;True;True
Node;AmplifyShaderEditor.SimpleTimeNode;56;-2106.632,-121.7276;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;60;-1975.925,-296.8179;Float;False;Property;_Uspeed;U speed;6;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;61;-1995.925,36.18213;Float;True;Property;_Vspeed;V speed;7;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-1783.509,17.77797;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;52;-1860.998,-143.3439;Inherit;False;0;1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;-1791.509,-291.2224;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-1610.509,-235.2223;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;54;-1613.509,-6.221937;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;59;-1348.804,-249.9941;Inherit;False;0;1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;55;-1369.883,-123.8354;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.StaticSwitch;58;-1108.198,-191.8404;Float;False;Property;_Keyword0;Keyword 0;5;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;False;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;1;-861.9006,-213.2998;Inherit;True;Property;_main_Tex;main_Tex;0;0;Create;True;0;0;False;0;-1;None;a55d311e614132147b259991455d3528;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;4;-843.8255,-30.29157;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;44;-886.6677,323.9374;Inherit;True;Property;_zhezhao_01;zhezhao_01;3;0;Create;True;0;0;False;0;-1;None;684e5f46121acbe4ebd190720b734907;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;45;-884.0676,508.5368;Inherit;True;Property;_zhezhao_02;zhezhao_02;4;0;Create;True;0;0;False;0;-1;None;f087bbc32688a5d4eb4b653ff53a9381;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;24;-879.2603,152.7394;Float;False;Property;_Color0;Color 0;1;1;[HDR];Create;True;0;0;False;0;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-442.7433,1.473645;Inherit;False;7;7;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;COLOR;0,0,0,0;False;5;FLOAT;0;False;6;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;-263.2874,-45.59362;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;ASE_VFX/ADD_tuowei_mod;False;False;False;False;False;False;False;False;False;True;False;False;False;False;True;False;False;False;False;False;False;Off;2;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;Transparent;All;7;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;4;1;False;-1;1;False;-1;0;1;False;-1;1;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;2;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;51;0;56;0
WireConnection;51;1;61;0
WireConnection;50;0;60;0
WireConnection;50;1;56;0
WireConnection;53;0;50;0
WireConnection;53;1;52;1
WireConnection;54;0;52;2
WireConnection;54;1;51;0
WireConnection;55;0;53;0
WireConnection;55;1;54;0
WireConnection;58;1;59;0
WireConnection;58;0;55;0
WireConnection;1;1;58;0
WireConnection;2;0;1;0
WireConnection;2;1;24;0
WireConnection;2;2;44;1
WireConnection;2;3;45;3
WireConnection;2;4;4;0
WireConnection;2;5;4;4
WireConnection;2;6;24;4
WireConnection;0;2;2;0
ASEEND*/
//CHKSM=0909C1E63DFD9B52587920308BFF984EB6E8A168