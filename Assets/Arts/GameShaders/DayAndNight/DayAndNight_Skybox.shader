﻿Shader "Custom/DayAndNight_SkyBox"
{Properties {
	_Tint ("Tint Color", Color) = (.5, .5, .5, .5)
	[Gamma] _Exposure ("Exposure", Range(0, 8)) = 1.0
	_Rotation ("Rotation", Range(0, 360)) = 0
	[NoScaleOffset] _Tex01 ("早晨   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _Tex02 ("中午   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _Tex03 ("下午   (HDR)", 2D) = "grey" {}
	[NoScaleOffset] _Tex04 ("夜晚   (HDR)", 2D) = "grey" {}
}

SubShader {
	Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }
	Cull Off ZWrite Off
	
	CGINCLUDE
	#include "UnityCG.cginc"

	half4 _Tint;
	half _Exposure;
	fixed _Rotation;

	float3 RotateAroundYInDegrees (fixed3 vertex, fixed degrees)
	{
		fixed alpha = degrees * UNITY_PI / 180.0;
		fixed sina, cosa;
		sincos(alpha, sina, cosa);
		fixed2x2 m = fixed2x2(cosa, -sina, sina, cosa);
		return fixed3(mul(m, vertex.xz), vertex.y).xzy;
	}
	
	struct appdata_t {
		fixed4 vertex : POSITION;
		fixed2 texcoord : TEXCOORD0;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};
	struct v2f {
		fixed4 vertex : SV_POSITION;
		fixed2 texcoord : TEXCOORD0;
		UNITY_VERTEX_OUTPUT_STEREO
	};
	v2f vert (appdata_t v)
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
		fixed3 rotated = RotateAroundYInDegrees(v.vertex, _Rotation);
		o.vertex = UnityObjectToClipPos(rotated);
		o.texcoord = v.texcoord;
		return o;
	}
	half4 skybox_frag (v2f i, sampler2D smp, half4 smpDecode)
	{
		half4 tex = tex2D (smp, fixed2(i.texcoord.x,i.texcoord.y));
		half3 c = DecodeHDR (tex, half4(smpDecode.x,smpDecode.y,smpDecode.z,smpDecode.w));
		c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
		c *= _Exposure;
		return half4(c, 1);
	}
	ENDCG
	
	Pass {
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		sampler2D _Tex01;
		half4 _Tex01_HDR;
		sampler2D _Tex02;
		half4 _Tex02_HDR;
		sampler2D _Tex03;
		half4 _Tex03_HDR;
		sampler2D _Tex04;
		half4 _Tex04_HDR;
		//重点是这个变量，通过全局修改这个变量的参数来对四个时段的贴图采样，并融合。
		//这个变量的范围与脚本的时间一致，都是0-4循环，最后一个时段与第一个一样，都是凌晨
		float _DayAndNightChange;

		half4 frag (v2f i) : SV_Target 
		{
			//基本就是一些融合算法，先利用时间来算出每个贴图的颜色占比，然后统一加到一起。
			//这只是计算了天空盒的一面，后面的Pass算法相同
		 half4 col01 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(1- _DayAndNightChange));
		 half4 col02 = (skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(2- _DayAndNightChange)))-(skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(1- _DayAndNightChange)));
		 half4 col03 = (skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(3- _DayAndNightChange)))-(skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(2- _DayAndNightChange)));
		 half4 col04 = (skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(4- _DayAndNightChange)))-(skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(3- _DayAndNightChange)));
		 half4 col05 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(_DayAndNightChange -3));
		 half4 c = col01 +col02+col03+col04+col05;
		 return c; 
		 }
		ENDCG 
	}
	Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		sampler2D _Tex01;
		half4 _Tex01_HDR;
		sampler2D _Tex02;
		half4 _Tex02_HDR;
		sampler2D _Tex03;
		half4 _Tex03_HDR;
		sampler2D _Tex04;
		half4 _Tex04_HDR;
		float _DayAndNightChange;
		half4 frag (v2f i) : SV_Target {
		 
		 half4 col01 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(1- _DayAndNightChange));
		 half4 col02 = (skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(2- _DayAndNightChange)))-(skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(1- _DayAndNightChange)));
		 half4 col03 = (skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(3- _DayAndNightChange)))-(skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(2- _DayAndNightChange)));
		 half4 col04 = (skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(4- _DayAndNightChange)))-(skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(3- _DayAndNightChange)));
		 half4 col05 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(_DayAndNightChange -3));
		 half4 c = col01 +col02+col03+col04+col05;
		 return c; 
		 }
		ENDCG 
	}
	Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		sampler2D _Tex01;
		half4 _Tex01_HDR;
		sampler2D _Tex02;
		half4 _Tex02_HDR;
		sampler2D _Tex03;
		half4 _Tex03_HDR;
		sampler2D _Tex04;
		half4 _Tex04_HDR;
		float _DayAndNightChange;
		half4 frag (v2f i) : SV_Target 
		{
		 half4 col01 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(1- _DayAndNightChange));
		 half4 col02 = (skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(2- _DayAndNightChange)))-(skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(1- _DayAndNightChange)));
		 half4 col03 = (skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(3- _DayAndNightChange)))-(skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(2- _DayAndNightChange)));
		 half4 col04 = (skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(4- _DayAndNightChange)))-(skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(3- _DayAndNightChange)));
		 half4 col05 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(_DayAndNightChange -3));
		 half4 c = col01 +col02+col03+col04+col05;
		 return c; 
		 }
		ENDCG
	}
	Pass{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma target 2.0
		sampler2D _Tex01;
		half4 _Tex01_HDR;
		sampler2D _Tex02;
		half4 _Tex02_HDR;
		sampler2D _Tex03;
		half4 _Tex03_HDR;
		sampler2D _Tex04;
		half4 _Tex04_HDR;
		float _DayAndNightChange;
		half4 frag (v2f i) : SV_Target 
		{ 
		 half4 col01 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(1- _DayAndNightChange));
		 half4 col02 = (skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(2- _DayAndNightChange)))-(skybox_frag(i,_Tex02, _Tex02_HDR)*(saturate(1- _DayAndNightChange)));
		 half4 col03 = (skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(3- _DayAndNightChange)))-(skybox_frag(i,_Tex03, _Tex03_HDR)*(saturate(2- _DayAndNightChange)));
		 half4 col04 = (skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(4- _DayAndNightChange)))-(skybox_frag(i,_Tex04, _Tex04_HDR)*(saturate(3- _DayAndNightChange)));
		 half4 col05 = skybox_frag(i,_Tex01, _Tex01_HDR)*(saturate(_DayAndNightChange -3));
		 half4 c = col01 +col02+col03+col04+col05;
		 return c; 
		}
		ENDCG
	}	

}
}
