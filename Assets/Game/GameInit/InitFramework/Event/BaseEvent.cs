namespace GameInit.Game
{
    public class BaseEvent
    {
        protected string evtType;
        protected object evtObj;
        
        public string Type => evtType;

        public object EventObj => evtObj;

        public BaseEvent(string type, object eventObj)
        {
            evtType = type;
            evtObj = eventObj;
        }

        public BaseEvent(string type)
        {
            evtType = type;
        }

        public virtual void Reset(string type, object eventObj = null)
        {
            evtType = type;
            evtObj = eventObj;
        }

        public virtual void Clear()
        {
            evtObj = null;
            evtType = string.Empty;
        }
    }
}