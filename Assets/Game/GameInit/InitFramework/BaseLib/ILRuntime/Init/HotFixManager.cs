﻿        // using GameHotfix.Game;
using UnityEngine;

namespace GameInit.Framework
{
    public class HotFixManager
    {
        private static HotfixBaseEntry _hotFixMainEntry;
        private static readonly UpdateData _updateData = new UpdateData();
        private static readonly UpdateData _fixedUpdateData = new UpdateData();
        private static readonly UpdateData _lateUpdateData = new UpdateData();

#if USE_ILRUNTIME
        public static void Load(LoadDllType type, byte[] dll, byte[] pdb, string hotfixEntryName = "GameHotfix.Game.HotfixEntry")
        {
            if (type == LoadDllType.ILRuntime)
            {
                ILRuntimeDllLoader.Init(dll, pdb, () =>
                {
                    _hotFixMainEntry = ILRuntimeDllLoader.appdomain.Instantiate<HotfixBaseEntry>(hotfixEntryName);
                });
            }
            else
            {
                SystemDllLoader.Init(dll, pdb, () =>
                {
                    _hotFixMainEntry = (HotfixBaseEntry)SystemDllLoader.assembly.CreateInstance(hotfixEntryName);
                });
            }

            if (_hotFixMainEntry != null)
            {
                new GameObject("HotFixMono").AddComponent<HotFixBehaviour>();
            }
        }
#else
        public static void Load(string hotfixEntryName = "GameHotfix.Game.HotfixEntry")
        {
            // _hotFixMainEntry = new HotfixEntry();
            // if (_hotFixMainEntry != null)
            // {
            //     new GameObject("HotFixMono").AddComponent<HotFixBehaviour>();
            // }
        }
#endif
        
        public static object DoMain2HotFix(int funcType, params object[] args)
        {
            if (_hotFixMainEntry != null)
            {
                return _hotFixMainEntry.OnMain2HotFix(funcType, args);
            }

            return null;
        }

        class HotFixBehaviour : MonoBehaviour
        {
            void Awake()
            {
                GameObject.DontDestroyOnLoad(this.gameObject);
                _hotFixMainEntry?.Awake();
            }

            void Start()
            {
                _hotFixMainEntry?.Start();
            }

            void Update()
            {
                if (!_updateData.Check(Time.deltaTime))
                {
                    return;
                }

                _hotFixMainEntry?.Update();
            }

            void FixedUpdate()
            {
                if (!_fixedUpdateData.Check(Time.fixedDeltaTime))
                {
                    return;
                }

                _hotFixMainEntry?.FixedUpdate();
            }

            void LateUpdate()
            {
                if (!_lateUpdateData.Check(Time.deltaTime))
                {
                    return;
                }

                _hotFixMainEntry?.LateUpdate();
            }

            void OnDestroy()
            {
                _hotFixMainEntry?.OnDestroy();
            }

            void OnApplicationPause(bool pause)
            {
                _hotFixMainEntry?.OnApplicationPause(pause);
            }

            void OnApplicationFocus(bool focus)
            {
                _hotFixMainEntry?.OnApplicationFocus(focus);
            }

            void OnApplicationQuit()
            {
                _hotFixMainEntry?.OnApplicationQuit();
            }
        }

        public class UpdateData
        {
            public float    interval    = 0f;

            private float   _lastTime   = 0f;

            public bool Check(float deltaTime)
            {
                this._lastTime += deltaTime;
                if (this._lastTime >= this.interval)
                {
                    this._lastTime = 0f;
                    return true;
                }

                return false;
            }
        }
    }

    public enum LoadDllType
    {
        System,
        ILRuntime,
    }
}
