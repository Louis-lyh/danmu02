﻿#if USE_ILRUNTIME
using System;
using UnityEngine;
using AppDomain = ILRuntime.Runtime.Enviorment.AppDomain;

namespace GameInit.Framework
{
    public class ILRuntimeRegister
    {
        public static void Init(AppDomain appdomain)
        {
            RedirectionMethods(appdomain);
            RegisterCrossBindingAdaptor(appdomain);
            Type t = Type.GetType("GameInit.Game.ILRegisterMethodDelegate");
            if (t != null)
            {
                t.GetMethod("RegisterMethodDelegate")?.Invoke(null, new object[] { appdomain });
            }
            RegisterMethodDelegate(appdomain);
            RegisterValueTypeBinder(appdomain);

            LitJson.JsonMapper.RegisterILRuntimeCLRRedirection(appdomain);
        }

        /// <summary>
        /// 重定向方法
        /// </summary>
        private static void RedirectionMethods(AppDomain appdomain)
        {
            GameObjectCLR.Register(appdomain);
            ComponentCLR.Register(appdomain);
            ActivatorCLR.Register(appdomain);
        }

        /// <summary>
        /// 绑定适配器
        /// </summary>
        private static void RegisterCrossBindingAdaptor(AppDomain appdomain)
        {
            appdomain.RegisterCrossBindingAdaptor(new HotFixBaseAdapter());
            appdomain.RegisterCrossBindingAdaptor(new MonoBehaviourAdapter());
            appdomain.RegisterCrossBindingAdaptor(new CoroutineAdapter());
            appdomain.RegisterCrossBindingAdaptor(new EventTriggerAdapter());
            appdomain.RegisterCrossBindingAdaptor(new IAsyncStateMachineAdapter());
            appdomain.RegisterCrossBindingAdaptor(new KGIAPListenerAdapter());
        }

        /// <summary>
        /// 注册委托
        /// </summary>
        /// <param name="appdomain"></param>
        private static void RegisterMethodDelegate(AppDomain appdomain)
        {
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.EventSystems.PointerEventData>();
            
            /*----- 委托适配器 -----*/
            appdomain.DelegateManager.RegisterMethodDelegate<string>();
            appdomain.DelegateManager.RegisterMethodDelegate<bool>();
            appdomain.DelegateManager.RegisterMethodDelegate<sbyte>();
            appdomain.DelegateManager.RegisterMethodDelegate<byte>();
            appdomain.DelegateManager.RegisterMethodDelegate<int>();
            appdomain.DelegateManager.RegisterMethodDelegate<uint>();
            appdomain.DelegateManager.RegisterMethodDelegate<short>();
            appdomain.DelegateManager.RegisterMethodDelegate<ushort>();
            appdomain.DelegateManager.RegisterMethodDelegate<long>();
            appdomain.DelegateManager.RegisterMethodDelegate<ulong>();
            appdomain.DelegateManager.RegisterMethodDelegate<float>();
            appdomain.DelegateManager.RegisterMethodDelegate<double>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Vector2>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Vector2Int>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Vector3>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Vector3Int>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Vector4>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Rect>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.RectInt>();

            appdomain.DelegateManager.RegisterMethodDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Object>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.IO.BinaryWriter>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Object>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.GameObject>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.SkinnedMeshRenderer>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.TrailRenderer>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Int32, System.Object>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.EventSystems.BaseEventData>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Object, ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterMethodDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Int32>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.String, UnityEngine.GameObject>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.UInt64, System.Single>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.UInt32, UnityEngine.GameObject>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.String, System.Boolean>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Sprite>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.IO.BinaryReader>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.ValueTuple<System.Single, System.Single, System.Single>>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.GameObject, UnityEngine.EventSystems.PointerEventData>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Int32, System.Object[]>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.String, System.String, UnityEngine.LogType>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Int32, System.Int32>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.Transform>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.UI.Image>();
            appdomain.DelegateManager.RegisterMethodDelegate<UnityEngine.RectTransform>(); 
            appdomain.DelegateManager.RegisterMethodDelegate<System.Int32, UnityEngine.GameObject>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.String, System.String>();

            appdomain.DelegateManager.RegisterFunctionDelegate<int, int>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Type, System.Object>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.String, System.String>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Int32, System.UInt32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.UInt32, System.Boolean>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Int32, System.Boolean>();
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.RectTransform>();
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.RaycastHit, System.Single>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.UInt32, System.UInt32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.UInt64, System.Boolean>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.UInt32, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Int32, System.Int32, System.Boolean>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.UInt64, System.UInt64, System.Int32>();

            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Boolean>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.String>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Single>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.UInt32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Int64>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.UInt64>();
            appdomain.DelegateManager.RegisterFunctionDelegate<ILRuntime.Runtime.Intepreter.ILTypeInstance, ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.UInt32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.UInt32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.UInt32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.UInt64, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.Int64>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.UInt32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, ILRuntime.Runtime.Intepreter.ILTypeInstance[]>();
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.SkinnedMeshRenderer>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.UInt64, ILRuntime.Runtime.Intepreter.ILTypeInstance>, ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.UInt64, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.UInt64>();
            appdomain.DelegateManager.RegisterFunctionDelegate<MonoBehaviourAdapter.Adaptor, MonoBehaviourAdapter.Adaptor, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, ILRuntime.Runtime.Intepreter.ILTypeInstance>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.String, System.UInt32>();

            

            /*----- 委托转换器 -----*/
            // UGUI消息系统
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<UnityEngine.EventSystems.BaseEventData>>((action) =>
            {
                return new UnityEngine.Events.UnityAction<UnityEngine.EventSystems.BaseEventData>((a) =>
                {
                    ((System.Action<UnityEngine.EventSystems.BaseEventData>)action)(a);
                });
            });

            // UnityAction
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction>((action) =>
            {
                return new UnityEngine.Events.UnityAction(() =>
                {
                    ((System.Action)action)();
                });
            });

            // UnityAction<string>
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<string>>((action) =>
            {
                return new UnityEngine.Events.UnityAction<string>((str) =>
                {
                    ((System.Action<string>)action)(str);
                });
            });

            // UnityAction<int>
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<System.Int32>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<System.Int32>((arg0) =>
                {
                    ((System.Action<System.Int32>)act)(arg0);
                });
            });

            // dotween
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.TweenCallback>((act) =>
            {
                return new DG.Tweening.TweenCallback(() =>
                {
                    ((System.Action)act)();
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOGetter<System.Single>>((act) =>
            {
                return new DG.Tweening.Core.DOGetter<System.Single>(() =>
                {
                    return ((System.Func<System.Single>)act)();
                });
            });
            
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Video.VideoPlayer.EventHandler>((act) =>
            {
                return new UnityEngine.Video.VideoPlayer.EventHandler((source) =>
                {
                    ((System.Action<UnityEngine.Video.VideoPlayer>)act)(source);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<ILRuntime.Runtime.Intepreter.ILTypeInstance>>((act) =>
            {
                return new System.Comparison<ILRuntime.Runtime.Intepreter.ILTypeInstance>((x, y) =>
                {
                    return ((System.Func<ILRuntime.Runtime.Intepreter.ILTypeInstance, ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<UnityEngine.GameObject>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<UnityEngine.GameObject>((arg0) =>
                {
                    ((System.Action<UnityEngine.GameObject>)act)(arg0);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<UnityEngine.Vector2>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<UnityEngine.Vector2>((arg0) =>
                {
                    ((System.Action<UnityEngine.Vector2>)act)(arg0);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<System.Boolean>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<System.Boolean>((arg0) =>
                {
                    ((System.Action<System.Boolean>)act)(arg0);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<ILRuntime.Runtime.Intepreter.ILTypeInstance>>((act) =>
            {
                return new System.Predicate<ILRuntime.Runtime.Intepreter.ILTypeInstance>((obj) =>
                {
                    return ((System.Func<ILRuntime.Runtime.Intepreter.ILTypeInstance, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<System.Int32>>((act) =>
            {
                return new System.Predicate<System.Int32>((obj) =>
                {
                    return ((System.Func<System.Int32, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<System.UInt64>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<System.UInt64>((arg0) =>
                {
                    ((System.Action<System.UInt64>)act)(arg0);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<System.UInt64, System.Single>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<System.UInt64, System.Single>((arg0, arg1) =>
                {
                    ((System.Action<System.UInt64, System.Single>)act)(arg0, arg1);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Events.UnityAction<System.Single>>((act) =>
            {
                return new UnityEngine.Events.UnityAction<System.Single>((arg0) =>
                {
                    ((System.Action<System.Single>)act)(arg0);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Converter<System.Int32, System.UInt32>>((act) =>
            {
                return new System.Converter<System.Int32, System.UInt32>((input) =>
                {
                    return ((System.Func<System.Int32, System.UInt32>)act)(input);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOSetter<System.Single>>((act) =>
            {
                return new DG.Tweening.Core.DOSetter<System.Single>((pNewValue) =>
                {
                    ((System.Action<System.Single>)act)(pNewValue);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<UnityEngine.Application.LogCallback>((act) =>
            {
                return new UnityEngine.Application.LogCallback((condition, stackTrace, type) =>
                {
                    ((System.Action<System.String, System.String, UnityEngine.LogType>)act)(condition, stackTrace, type);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Converter<System.UInt32, System.Int32>>((act) =>
            {
                return new System.Converter<System.UInt32, System.Int32>((input) =>
                {
                    return ((System.Func<System.UInt32, System.Int32>)act)(input);
                });
            });

            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<System.UInt64>>((act) =>
            {
                return new System.Comparison<System.UInt64>((x, y) =>
                {
                    return ((System.Func<System.UInt64, System.UInt64, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<MonoBehaviourAdapter.Adaptor>>((act) =>
            {
                return new System.Comparison<MonoBehaviourAdapter.Adaptor>((x, y) =>
                {
                    return ((System.Func<MonoBehaviourAdapter.Adaptor, MonoBehaviourAdapter.Adaptor, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Converter<System.String, System.UInt32>>((act) =>
            {
                return new System.Converter<System.String, System.UInt32>((input) =>
                {
                    return ((System.Func<System.String, System.UInt32>)act)(input);
                });
            });
        }

        /// <summary>
        /// 注册值类型
        /// </summary>
        private static void RegisterValueTypeBinder(AppDomain appdomain)
        {
            appdomain.RegisterValueTypeBinder(typeof(Vector3), new Vector3Binder());
            appdomain.RegisterValueTypeBinder(typeof(Vector2), new Vector2Binder());
            appdomain.RegisterValueTypeBinder(typeof(Quaternion), new QuaternionBinder());
            appdomain.RegisterValueTypeBinder(typeof(Vector2Int), new Vector2IntBinder());
            appdomain.RegisterValueTypeBinder(typeof(Vector3Int), new Vector3IntBinder());
        }
    }
}
#endif