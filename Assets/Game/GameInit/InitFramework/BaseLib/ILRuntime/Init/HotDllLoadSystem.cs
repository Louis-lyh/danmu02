#if USE_ILRUNTIME
using System;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using AppDomain = ILRuntime.Runtime.Enviorment.AppDomain;

namespace GameInit.Framework
{
    public static class SystemDllLoader
    {
        private static Assembly _assembly = null;
    
        public static void Init(byte[] dll, byte[] pdb, System.Action completeCall)
        {
    #if UNITY_EDITOR
            _assembly = Assembly.Load(dll, pdb);
    #else
            _assembly   = Assembly.Load(dll);
    #endif
    
            var att = _assembly.GetCustomAttribute<DebuggableAttribute>();
            Logger.Log("加载的dll模式:" + (att.IsJITTrackingEnabled ? "Debug" : "Release"));
    
            completeCall?.Invoke();
        }
    
        public static Assembly assembly { get { return _assembly; } }
    }
    
    public static class ILRuntimeDllLoader
    {
        private static AppDomain _appdomain;
    
        public static void Init(byte[] dll, byte[] pdb, System.Action completeCall)
        {
            _appdomain          = new ILRuntime.Runtime.Enviorment.AppDomain();
    
            MemoryStream fs     = null;
            MemoryStream p      = null;
            if (dll != null)
            {
                fs = new MemoryStream(dll);
            }
            if (pdb != null)
            {
                p = new MemoryStream(pdb);
            }
            _appdomain.LoadAssembly(fs, p, new ILRuntime.Mono.Cecil.Pdb.PdbReaderProvider());
    
            InitializeILRuntime();
            OnHotFixLoaded();
    
            completeCall?.Invoke();
        }
    
        public static void Init(ILRuntime.Runtime.Enviorment.AppDomain appdomain)
        {
            _appdomain = appdomain;
    
            ILRuntimeRegister.Init(_appdomain);
        }
    
        private static void InitializeILRuntime()
        {
            ILRuntimeRegister.Init(_appdomain);
            Type t = Type.GetType("ILRuntime.Runtime.Generated.CLRBindings");
            if (t != null)
            {
                t.GetMethod("Initialize")?.Invoke(null, new object[] { appdomain });
            }
        }
    
        private static void OnHotFixLoaded()
        {
    #if UNITY_EDITOR
            _appdomain.DebugService.StartDebugService(56000);
    #endif
    
    #if DEBUG && (UNITY_EDITOR || UNITY_ANDROID || UNITY_IPHONE)
            _appdomain.UnityMainThreadID = System.Threading.Thread.CurrentThread.ManagedThreadId;
    #endif
        }
    
        public static AppDomain appdomain { get { return _appdomain; } }
    }
}
#endif


