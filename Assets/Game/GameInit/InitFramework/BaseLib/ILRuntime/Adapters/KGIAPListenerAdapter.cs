﻿#if USE_ILRUNTIME
using System;
using GameHotfix.Framework;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using UnityEngine.Purchasing;

namespace GameInit.Framework
{
    public class KGIAPListenerAdapter : CrossBindingAdaptor
    {
        public override Type BaseCLRType
        {
            get
            {
                return typeof(KGIAPListener);//这是你想继承的那个类
            }
        }

        public override Type AdaptorType
        {
            get
            {
                return typeof(Adaptor);//这是实际的适配器类
            }
        }

        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
        {
            return new Adaptor(appdomain, instance);//创建一个新的实例
        }

        //实际的适配器类需要继承你想继承的那个类，并且实现CrossBindingAdaptorType接口
        public class Adaptor : KGIAPListener, CrossBindingAdaptorType
        {
            ILTypeInstance instance;
            ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            //缓存这个数组来避免调用时的GC Alloc
            object[] param1 = new object[1];
            object[] param2 = new object[2];

            public Adaptor()
            {

            }

            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
                this.appdomain = appdomain;
                this.instance = instance;

                _onInitialized = instance.Type.GetMethod("OnInitialized", 2);
                _initializeGoods = instance.Type.GetMethod("InitializeGoods", 1);
                _onPurchaseFailed = instance.Type.GetMethod("OnPurchaseFailed", 1);
                _processPurchase = instance.Type.GetMethod("ProcessPurchase", 1);
                _onPurchaseStart = instance.Type.GetMethod("OnPurchaseStart", 0);
                _onRestoreStart = instance.Type.GetMethod("OnRestoreStart", 0);
                _onRestoreComplate = instance.Type.GetMethod("OnRestoreComplate", 0);
            }

            public ILTypeInstance ILInstance { get { return instance; } }

            private readonly IMethod _onInitialized;
            private readonly IMethod _initializeGoods;
            private readonly IMethod _onPurchaseFailed;
            private readonly IMethod _processPurchase;
            private readonly IMethod _onPurchaseStart;
            private readonly IMethod _onRestoreStart;
            private readonly IMethod _onRestoreComplate;

            // iap 初始化接口
            public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
            {
                param2[0] = controller;
                param2[1] = extensions;
                appdomain.Invoke(_onInitialized, instance, param2);
            }

            // iap 初始化商品
            public void InitializeGoods(ConfigurationBuilder builder)
            {
                param1[0] = builder;
                appdomain.Invoke(_initializeGoods, instance, param1);
            }

            // iap 购买失败
            public void OnPurchaseFailed(Product i)
            {
                param1[0] = i;
                appdomain.Invoke(_onPurchaseFailed, instance, param1);
            }

            // iap 处理购买
            public bool ProcessPurchase(Product i)
            {
                param1[0] = i;
                return (bool)appdomain.Invoke(_processPurchase, instance, param1);
            }

            // iap 开始购买
            public void OnPurchaseStart()
            {
                appdomain.Invoke(_onPurchaseStart, instance, null);
            }

            // iap 恢复购买
            public void OnRestoreStart()
            {
                appdomain.Invoke(_onRestoreStart, instance, null);
            }

            // iap 恢复成功
            public void OnRestoreComplate()
            {
                appdomain.Invoke(_onRestoreComplate, instance, null);
            }
        }
    }
}
#endif