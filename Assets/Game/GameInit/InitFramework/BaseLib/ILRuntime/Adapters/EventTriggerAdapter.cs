﻿#if USE_ILRUNTIME
using System;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using UnityEngine.EventSystems;

namespace GameInit.Framework
{
    public class EventTriggerAdapter : CrossBindingAdaptor
    {
        public override Type BaseCLRType
        {
            get
            {
                return typeof(UnityEngine.EventSystems.EventTrigger);//这是你想继承的那个类
            }
        }

        public override Type AdaptorType
        {
            get
            {
                return typeof(Adaptor);//这是实际的适配器类
            }
        }

        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
        {
            return new Adaptor(appdomain, instance);//创建一个新的实例
        }

        //实际的适配器类需要继承你想继承的那个类，并且实现CrossBindingAdaptorType接口
        public class Adaptor : UnityEngine.EventSystems.EventTrigger, CrossBindingAdaptorType, IMonoHandler
        {
            ILTypeInstance instance;
            ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            //缓存这个数组来避免调用时的GC Alloc
            object[] param1 = new object[1];


            public ILTypeInstance ILInstance { get { return instance; } set { instance = value; } }
            public ILRuntime.Runtime.Enviorment.AppDomain AppDomain { get { return appdomain; } set { appdomain = value; } }


            public Adaptor()
            {

            }

            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
                this.appdomain = appdomain;
                this.instance = instance;
            }


            #region MonoBehaviour
            IMethod mAwakeMethod;
            bool mAwakeMethodGot;
            bool mAwakeCalled = false;
            public void Awake()
            {
                //Unity会在ILRuntime准备好这个实例前调用Awake，所以这里暂时先不掉用
                if (instance != null)
                {
                    if (!mAwakeMethodGot)
                    {
                        mAwakeMethod = instance.Type.GetMethod("Awake", 0);
                        mAwakeMethodGot = true;
                    }

                    if (mAwakeMethod != null && !mAwakeCalled)
                    {
                        mAwakeCalled = true;
                        appdomain.Invoke(mAwakeMethod, instance, null);
                    }
                }
            }

            IMethod mStartMethod;
            bool mStartMethodGot;
            void Start()
            {
                if (instance != null)
                {
                    if (!mStartMethodGot)
                    {
                        mStartMethod = instance.Type.GetMethod("Start", 0);
                        mStartMethodGot = true;
                    }

                    if (mStartMethod != null)
                    {
                        appdomain.Invoke(mStartMethod, instance, null);
                    }
                }
            }

            IMethod mOnEnableMethod;
            bool mOnEnableMethodGot;
            public void OnEnable()
            {
                if (instance != null)
                {
                    if (!mOnEnableMethodGot)
                    {
                        mOnEnableMethod = instance.Type.GetMethod("OnEnable", 0);
                        mOnEnableMethodGot = true;
                    }

                    if (mOnEnableMethod != null)
                    {
                        appdomain.Invoke(mOnEnableMethod, instance, null);
                    }
                }
            }

            IMethod mOnDisableMethod;
            bool mOnDisableMethodGot;
            void OnDisable()
            {
                if (instance != null)
                {
                    if (!mOnDisableMethodGot)
                    {
                        mOnDisableMethod = instance.Type.GetMethod("OnDisable", 0);
                        mOnDisableMethodGot = true;
                    }

                    if (mOnDisableMethod != null)
                    {
                        appdomain.Invoke(mOnDisableMethod, instance, null);
                    }
                }
            }

            IMethod mOnDestroyMethod;
            bool mOnDestroyMethodGot;
            void OnDestroy()
            {
                if (instance != null)
                {
                    if (!mOnDestroyMethodGot)
                    {
                        mOnDestroyMethod = instance.Type.GetMethod("OnDestroy", 0);
                        mOnDestroyMethodGot = true;
                    }

                    if (mOnDestroyMethod != null)
                    {
                        appdomain.Invoke(mOnDestroyMethod, instance, null);
                    }
                }
            }
            #endregion

            bool m_bOnBeginDragGot = false;
            IMethod m_OnBeginDrag = null;
            bool isOnBeginDragInvoking = false;
            public override void OnBeginDrag(PointerEventData eventData)
            {
                if (!m_bOnBeginDragGot)
                {
                    m_OnBeginDrag = instance.Type.GetMethod("OnBeginDrag", 1);
                    m_bOnBeginDragGot = true;
                }
                if (m_OnBeginDrag != null && !isOnBeginDragInvoking)
                {
                    isOnBeginDragInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnBeginDrag, instance, param1);
                    isOnBeginDragInvoking = false;
                }
                else
                {
                    base.OnBeginDrag(eventData);
                }
            }

            bool m_bOnCancelGot = false;
            IMethod m_OnCancel = null;
            bool isOnCancelInvoking = false;
            public override void OnCancel(BaseEventData eventData)
            {
                if (!m_bOnCancelGot)
                {
                    m_OnCancel = instance.Type.GetMethod("OnCancel", 1);
                    m_bOnCancelGot = true;
                }
                if (m_OnCancel != null && !isOnCancelInvoking)
                {
                    isOnCancelInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnCancel, instance, param1);
                    isOnCancelInvoking = false;
                }
                else
                {
                    base.OnCancel(eventData);
                }
            }

            bool m_bOnDeselectGot = false;
            IMethod m_OnDeselect = null;
            bool isOnDeselectInvoking = false;
            public override void OnDeselect(BaseEventData eventData)
            {
                if (!m_bOnDeselectGot)
                {
                    m_OnDeselect = instance.Type.GetMethod("OnDeselect", 1);
                    m_bOnDeselectGot = true;
                }
                if (m_OnDeselect != null && !isOnDeselectInvoking)
                {
                    isOnDeselectInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnDeselect, instance, param1);
                    isOnDeselectInvoking = false;
                }
                else
                {
                    base.OnDeselect(eventData);
                }
            }

            bool m_bOnDragGot = false;
            IMethod m_OnDrag = null;
            bool isOnDragInvoking = false;
            public override void OnDrag(PointerEventData eventData)
            {
                if (!m_bOnDragGot)
                {
                    m_OnDrag = instance.Type.GetMethod("OnDrag", 1);
                    m_bOnDragGot = true;
                }
                if (m_OnDrag != null && !isOnDragInvoking)
                {
                    isOnDragInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnDrag, instance, param1);
                    isOnDragInvoking = false;
                }
                else
                {
                    base.OnDrag(eventData);
                }
            }

            bool m_bOnDropGot = false;
            IMethod m_OnDrop = null;
            bool isOnDropInvoking = false;
            public override void OnDrop(PointerEventData eventData)
            {
                if (!m_bOnDropGot)
                {
                    m_OnDrop = instance.Type.GetMethod("OnDrop", 1);
                    m_bOnDropGot = true;
                }
                if (m_OnDrop != null && !isOnDropInvoking)
                {
                    isOnDropInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnDrop, instance, param1);
                    isOnDropInvoking = false;
                }
                else
                {
                    base.OnDrop(eventData);
                }
            }

            bool m_bOnEndDragGot = false;
            IMethod m_OnEndDrag = null;
            bool isOnEndDragInvoking = false;
            public override void OnEndDrag(PointerEventData eventData)
            {
                if (!m_bOnEndDragGot)
                {
                    m_OnEndDrag = instance.Type.GetMethod("OnEndDrag", 1);
                    m_bOnEndDragGot = true;
                }
                if (m_OnEndDrag != null && !isOnEndDragInvoking)
                {
                    isOnEndDragInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnEndDrag, instance, param1);
                    isOnEndDragInvoking = false;
                }
                else
                {
                    base.OnEndDrag(eventData);
                }
            }

            bool m_bOnInitializePotentialDragGot = false;
            IMethod m_OnInitializePotentialDrag = null;
            bool isOnInitializePotentialDragInvoking = false;
            public override void OnInitializePotentialDrag(PointerEventData eventData)
            {
                if (!m_bOnInitializePotentialDragGot)
                {
                    m_OnInitializePotentialDrag = instance.Type.GetMethod("OnInitializePotentialDrag", 1);
                    m_bOnInitializePotentialDragGot = true;
                }
                if (m_OnInitializePotentialDrag != null && !isOnInitializePotentialDragInvoking)
                {
                    isOnInitializePotentialDragInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnInitializePotentialDrag, instance, param1);
                    isOnInitializePotentialDragInvoking = false;
                }
                else
                {
                    base.OnInitializePotentialDrag(eventData);
                }
            }

            bool m_bOnMoveGot = false;
            IMethod m_OnMove = null;
            bool isOnMoveInvoking = false;
            public override void OnMove(AxisEventData eventData)
            {
                if (!m_bOnMoveGot)
                {
                    m_OnMove = instance.Type.GetMethod("OnMove", 1);
                    m_bOnMoveGot = true;
                }
                if (m_OnMove != null && !isOnMoveInvoking)
                {
                    isOnMoveInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnMove, instance, param1);
                    isOnMoveInvoking = false;
                }
                else
                {
                    base.OnMove(eventData);
                }
            }

            bool m_bOnPointerClickGot = false;
            IMethod m_OnPointerClick = null;
            bool isOnPointerClickInvoking = false;
            public override void OnPointerClick(PointerEventData eventData)
            {
                if (!m_bOnPointerClickGot)
                {
                    m_OnPointerClick = instance.Type.GetMethod("OnPointerClick", 1);
                    m_bOnPointerClickGot = true;
                }
                if (m_OnPointerClick != null && !isOnPointerClickInvoking)
                {
                    isOnPointerClickInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnPointerClick, instance, param1);
                    isOnPointerClickInvoking = false;
                }
                else
                {
                    base.OnPointerClick(eventData);
                }
            }

            bool m_bOnPointerDownGot = false;
            IMethod m_OnPointerDown = null;
            bool isOnPointerDownInvoking = false;
            public override void OnPointerDown(PointerEventData eventData)
            {
                if (!m_bOnPointerDownGot)
                {
                    m_OnPointerDown = instance.Type.GetMethod("OnPointerDown", 1);
                    m_bOnPointerDownGot = true;
                }
                if (m_OnPointerDown != null && !isOnPointerDownInvoking)
                {
                    isOnPointerDownInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnPointerDown, instance, param1);
                    isOnPointerDownInvoking = false;
                }
                else
                {
                    base.OnPointerDown(eventData);
                }
            }

            bool m_bOnPointerEnterGot = false;
            IMethod m_OnPointerEnter = null;
            bool isOnPointerEnterInvoking = false;
            public override void OnPointerEnter(PointerEventData eventData)
            {
                if (!m_bOnPointerEnterGot)
                {
                    m_OnPointerEnter = instance.Type.GetMethod("OnPointerEnter", 1);
                    m_bOnPointerEnterGot = true;
                }
                if (m_OnPointerEnter != null && !isOnPointerEnterInvoking)
                {
                    isOnPointerEnterInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnPointerEnter, instance, param1);
                    isOnPointerEnterInvoking = false;
                }
                else
                {
                    base.OnPointerEnter(eventData);
                }
            }

            bool m_bOnPointerExitGot = false;
            IMethod m_OnPointerExit = null;
            bool isOnPointerExitInvoking = false;
            public override void OnPointerExit(PointerEventData eventData)
            {
                if (!m_bOnPointerExitGot)
                {
                    m_OnPointerExit = instance.Type.GetMethod("OnPointerExit", 1);
                    m_bOnPointerExitGot = true;
                }
                if (m_OnPointerExit != null && !isOnPointerExitInvoking)
                {
                    isOnPointerExitInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnPointerExit, instance, param1);
                    isOnPointerExitInvoking = false;
                }
                else
                {
                    base.OnPointerExit(eventData);
                }
            }

            bool m_bOnPointerUpGot = false;
            IMethod m_OnPointerUp = null;
            bool isOnPointerUpInvoking = false;
            public override void OnPointerUp(PointerEventData eventData)
            {
                if (!m_bOnPointerUpGot)
                {
                    m_OnPointerUp = instance.Type.GetMethod("OnPointerUp", 1);
                    m_bOnPointerUpGot = true;
                }
                if (m_OnPointerUp != null && !isOnPointerUpInvoking)
                {
                    isOnPointerUpInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnPointerUp, instance, param1);
                    isOnPointerUpInvoking = false;
                }
                else
                {
                    base.OnPointerUp(eventData);
                }
            }

            bool m_bOnScrollGot = false;
            IMethod m_OnScroll = null;
            bool isOnScrollInvoking = false;
            public override void OnScroll(PointerEventData eventData)
            {
                if (!m_bOnScrollGot)
                {
                    m_OnScroll = instance.Type.GetMethod("OnScroll", 1);
                    m_bOnScrollGot = true;
                }
                if (m_OnScroll != null && !isOnScrollInvoking)
                {
                    isOnScrollInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnScroll, instance, param1);
                    isOnScrollInvoking = false;
                }
                else
                {
                    base.OnScroll(eventData);
                }
            }

            bool m_bOnSelectGot = false;
            IMethod m_OnSelect = null;
            bool isOnSelectInvoking = false;
            public override void OnSelect(BaseEventData eventData)
            {
                if (!m_bOnSelectGot)
                {
                    m_OnSelect = instance.Type.GetMethod("OnSelect", 1);
                    m_bOnSelectGot = true;
                }
                if (m_OnSelect != null && !isOnSelectInvoking)
                {
                    isOnSelectInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnSelect, instance, param1);
                    isOnSelectInvoking = false;
                }
                else
                {
                    base.OnSelect(eventData);
                }
            }

            bool m_bOnSubmitGot = false;
            IMethod m_OnSubmit = null;
            bool isOnSubmitInvoking = false;
            public override void OnSubmit(BaseEventData eventData)
            {
                if (!m_bOnSubmitGot)
                {
                    m_OnSubmit = instance.Type.GetMethod("OnSubmit", 1);
                    m_bOnSubmitGot = true;
                }
                if (m_OnSubmit != null && !isOnSubmitInvoking)
                {
                    isOnSubmitInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnSubmit, instance, param1);
                    isOnSubmitInvoking = false;
                }
                else
                {
                    base.OnSubmit(eventData);
                }
            }

            bool m_bOnUpdateSelectedGot = false;
            IMethod m_OnUpdateSelected = null;
            bool isOnUpdateSelectedInvoking = false;
            public override void OnUpdateSelected(BaseEventData eventData)
            {
                if (!m_bOnUpdateSelectedGot)
                {
                    m_OnUpdateSelected = instance.Type.GetMethod("OnUpdateSelected", 1);
                    m_bOnUpdateSelectedGot = true;
                }
                if (m_OnUpdateSelected != null && !isOnUpdateSelectedInvoking)
                {
                    isOnUpdateSelectedInvoking = true;
                    param1[0] = eventData;
                    appdomain.Invoke(m_OnUpdateSelected, instance, param1);
                    isOnUpdateSelectedInvoking = false;
                }
                else
                {
                    base.OnUpdateSelected(eventData);
                }
            }
        }
    }
}
#endif