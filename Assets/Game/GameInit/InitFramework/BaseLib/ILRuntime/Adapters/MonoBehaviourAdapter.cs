﻿#if USE_ILRUNTIME

using UnityEngine;
using System;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.CLR.Method;

namespace GameInit.Framework
{
    public class MonoBehaviourAdapter : CrossBindingAdaptor
    {
        public override Type BaseCLRType
        {
            get
            {
                return typeof(MonoBehaviour);
            }
        }

        public override Type AdaptorType
        {
            get
            {
                return typeof(Adaptor);
            }
        }

        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
        {
            return new Adaptor(appdomain, instance);
        }

        public class Adaptor : MonoBehaviour, CrossBindingAdaptorType, IMonoHandler
        {
            ILTypeInstance instance;
            ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            object[] param1 = new object[1];


            public ILTypeInstance ILInstance { get { return instance; } set { instance = value; } }
            public ILRuntime.Runtime.Enviorment.AppDomain AppDomain { get { return appdomain; } set { appdomain = value; } }


            public Adaptor()
            {

            }

            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
                this.appdomain = appdomain;
                this.instance = instance;
            }

            IMethod mAwakeMethod;
            bool mAwakeMethodGot;
            bool mAwakeCalled = false;
            public void Awake()
            {
                //Unity会在ILRuntime准备好这个实例前调用Awake，所以这里暂时先不掉用
                if (instance != null)
                {
                    if (!mAwakeMethodGot)
                    {
                        mAwakeMethod = instance.Type.GetMethod("Awake", 0);
                        mAwakeMethodGot = true;
                    }

                    if (mAwakeMethod != null && !mAwakeCalled)
                    {
                        mAwakeCalled = true;
                        appdomain.Invoke(mAwakeMethod, instance, null);
                    }
                }
            }

            IMethod mStartMethod;
            bool mStartMethodGot;
            void Start()
            {
                if (instance != null)
                {
                    if (!mStartMethodGot)
                    {
                        mStartMethod = instance.Type.GetMethod("Start", 0);
                        mStartMethodGot = true;
                    }

                    if (mStartMethod != null)
                    {
                        appdomain.Invoke(mStartMethod, instance, null);
                    }
                }
            }

            IMethod mOnEnableMethod;
            bool mOnEnableMethodGot;
            public void OnEnable()
            {
                if (instance != null)
                {
                    if (!mOnEnableMethodGot)
                    {
                        mOnEnableMethod = instance.Type.GetMethod("OnEnable", 0);
                        mOnEnableMethodGot = true;
                    }

                    if (mOnEnableMethod != null)
                    {
                        appdomain.Invoke(mOnEnableMethod, instance, null);
                    }
                }
            }

            IMethod mOnDisableMethod;
            bool mOnDisableMethodGot;
            void OnDisable()
            {
                if (instance != null)
                {
                    if (!mOnDisableMethodGot)
                    {
                        mOnDisableMethod = instance.Type.GetMethod("OnDisable", 0);
                        mOnDisableMethodGot = true;
                    }

                    if (mOnDisableMethod != null)
                    {
                        appdomain.Invoke(mOnDisableMethod, instance, null);
                    }
                }
            }

            IMethod mUpdateMethod;
            bool mUpdateMethodGot;
            void Update()
            {
                if (instance != null)
                {
                    if (!mUpdateMethodGot)
                    {
                        mUpdateMethod = instance.Type.GetMethod("Update", 0);
                        mUpdateMethodGot = true;
                    }

                    if (mUpdateMethod != null)
                    {
                        appdomain.Invoke(mUpdateMethod, instance, null);
                    }
                }
            }

            IMethod mFixedUpdateMethod;
            bool mFixedUpdateMethodGot;
            void FixedUpdate()
            {
                if (instance != null)
                {
                    if (!mFixedUpdateMethodGot)
                    {
                        mFixedUpdateMethod = instance.Type.GetMethod("FixedUpdate", 0);
                        mFixedUpdateMethodGot = true;
                    }

                    if (mFixedUpdateMethod != null)
                    {
                        appdomain.Invoke(mFixedUpdateMethod, instance, null);
                    }
                }
            }

            IMethod mLateUpdateMethod;
            bool mLateUpdateMethodGot;
            void LateUpdate()
            {
                if (instance != null)
                {
                    if (!mLateUpdateMethodGot)
                    {
                        mLateUpdateMethod = instance.Type.GetMethod("LateUpdate", 0);
                        mLateUpdateMethodGot = true;
                    }

                    if (mLateUpdateMethod != null)
                    {
                        appdomain.Invoke(mLateUpdateMethod, instance, null);
                    }
                }
            }

            IMethod mOnDestroyMethod;
            bool mOnDestroyMethodGot;
            void OnDestroy()
            {
                if (instance != null)
                {
                    if (!mOnDestroyMethodGot)
                    {
                        mOnDestroyMethod = instance.Type.GetMethod("OnDestroy", 0);
                        mOnDestroyMethodGot = true;
                    }

                    if (mOnDestroyMethod != null)
                    {
                        appdomain.Invoke(mOnDestroyMethod, instance, null);
                    }
                }
            }

            IMethod mOnTriggerEnterMethod;
            bool mOnTriggerEnterMethodGot;
            void OnTriggerEnter(Collider other)
            {
                if (instance != null)
                {
                    if (!mOnTriggerEnterMethodGot)
                    {
                        mOnTriggerEnterMethod = instance.Type.GetMethod("OnTriggerEnter", 1);
                        mOnTriggerEnterMethodGot = true;
                    }

                    if (mOnTriggerEnterMethod != null)
                    {
                        param1[0] = other;
                        appdomain.Invoke(mOnTriggerEnterMethod, instance, param1);
                    }
                }
            }

            IMethod mOnTriggerExitMethod;
            bool mOnTriggerExitMethodGot;
            void OnTriggerExit(Collider other)
            {
                if (instance != null)
                {
                    if (!mOnTriggerExitMethodGot)
                    {
                        mOnTriggerExitMethod = instance.Type.GetMethod("OnTriggerExit", 1);
                        mOnTriggerExitMethodGot = true;
                    }

                    if (mOnTriggerExitMethod != null)
                    {
                        param1[0] = other;
                        appdomain.Invoke(mOnTriggerExitMethod, instance, param1);
                    }
                }
            }

            IMethod mOnDisposeMethod;
            bool mOnDisposeMethodGot;
            void OnDispose()
            {
                if (instance != null)
                {
                    if (!mOnDisposeMethodGot)
                    {
                        mOnDisposeMethod = instance.Type.GetMethod("OnDispose", 0);
                        mOnDisposeMethodGot = true;
                    }

                    if (mOnDisposeMethod != null)
                    {
                        appdomain.Invoke(mOnDisposeMethod, instance, null);
                    }
                }
            }

            public override string ToString()
            {
                IMethod m = appdomain.ObjectType.GetMethod("ToString", 0);
                m = instance.Type.GetVirtualMethod(m);
                if (m == null || m is ILMethod)
                {
                    return instance.ToString();
                }
                else
                    return instance.Type.FullName;
            }

            IMethod mOnTriggerEnter2DMethod;
            bool mOnTriggerEnter2DMethodGot;
            void OnTriggerEnter2D(Collider2D other)
            {
                if (instance != null)
                {
                    if (!mOnTriggerEnter2DMethodGot)
                    {
                        mOnTriggerEnter2DMethod = instance.Type.GetMethod("OnTriggerEnter2D", 1);
                        mOnTriggerEnter2DMethodGot = true;
                    }

                    if (mOnTriggerEnter2DMethod != null)
                    {
                        param1[0] = other;
                        appdomain.Invoke(mOnTriggerEnter2DMethod, instance, param1);
                    }
                }
            }
        }
    }
}
#endif