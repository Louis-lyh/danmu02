#if USE_ILRUNTIME
using System;
using System.Runtime.CompilerServices;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;

namespace GameInit.Framework
{
    /// <summary>
    /// task、async适配器
    /// </summary>
    public class IAsyncStateMachineAdapter : CrossBindingAdaptor
    {
        public override Type BaseCLRType
        {
            get
            {
                return typeof(IAsyncStateMachine);//这是你想继承的那个类
            }
        }

        public override Type AdaptorType
        {
            get
            {
                return typeof(Adaptor);//这是实际的适配器类
            }
        }

        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
        {
            return new Adaptor(appdomain, instance);//创建一个新的实例
        }
        
        //实际的适配器类需要继承你想继承的那个类，并且实现CrossBindingAdaptorType接口
        public class Adaptor : IAsyncStateMachine, CrossBindingAdaptorType
        {
            ILTypeInstance instance;
            ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            private readonly IMethod mMoveNext;
            private readonly IMethod mSetStateMachine;
            //缓存这个数组来避免调用时的GC Alloc
            object[] param1 = new object[1];
            public Adaptor()
            {
            }

            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
                this.appdomain = appdomain;
                this.instance = instance;
                mMoveNext = instance.Type.GetMethod("MoveNext", 0);
                mSetStateMachine = instance.Type.GetMethod("SetStateMachine");
            }
            
            public ILTypeInstance ILInstance { get { return instance; } }

            public void MoveNext()
            {
                appdomain.Invoke(mMoveNext, instance, null);
            }

            public void SetStateMachine(IAsyncStateMachine stateMachine)
            {
                appdomain.Invoke(mSetStateMachine, instance, stateMachine);
            }
        }
    }
}
#endif