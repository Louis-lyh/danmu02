#if USE_ILRUNTIME
using System;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;

namespace GameInit.Framework
{
    public class HotFixBaseAdapter : CrossBindingAdaptor
    {
        public override Type BaseCLRType
        {
            get
            {
                return typeof(HotfixBaseEntry);//这是你想继承的那个类
            }
        }

        public override Type AdaptorType
        {
            get
            {
                return typeof(Adaptor);//这是实际的适配器类
            }
        }

        public override object CreateCLRInstance(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
        {
            return new Adaptor(appdomain, instance);//创建一个新的实例
        }

        //实际的适配器类需要继承你想继承的那个类，并且实现CrossBindingAdaptorType接口
        public class Adaptor : HotfixBaseEntry, CrossBindingAdaptorType
        {
            ILTypeInstance instance;
            ILRuntime.Runtime.Enviorment.AppDomain appdomain;
            //缓存这个数组来避免调用时的GC Alloc
            object[] param1 = new object[1];
            object[] param2 = new object[2];

            public Adaptor()
            {
            }

            public Adaptor(ILRuntime.Runtime.Enviorment.AppDomain appdomain, ILTypeInstance instance)
            {
                this.appdomain = appdomain;
                this.instance = instance;
            }

            public ILTypeInstance ILInstance { get { return instance; } }

            private static readonly CrossBindingMethodInfo _awakeMethodInfo = new CrossBindingMethodInfo("Awake");
            public override void Awake()
            {
                if(_awakeMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _awakeMethodInfo.Invoke(this.instance);
            }
            
            private static readonly CrossBindingMethodInfo _startMethodInfo = new CrossBindingMethodInfo("Start");
            public override void Start()
            {
                if(_startMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _startMethodInfo.Invoke(this.instance);
            }
            
                    
            private static readonly CrossBindingMethodInfo _updateMethodInfo = new CrossBindingMethodInfo("Update");
            public override void Update()
            {
                if(_updateMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _updateMethodInfo.Invoke(this.instance);
            }
            
            private static readonly CrossBindingMethodInfo _fixedUpdateMethodInfo = new CrossBindingMethodInfo("FixedUpdate");
            public override void FixedUpdate()
            {
                if(_fixedUpdateMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _fixedUpdateMethodInfo.Invoke(this.instance);
            }
            
            private static readonly CrossBindingMethodInfo _lateUpdateMethodInfo = new CrossBindingMethodInfo("LateUpdate");
            public override void LateUpdate()
            {
                if(_lateUpdateMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _lateUpdateMethodInfo.Invoke(this.instance);
            }
            
            private static readonly CrossBindingMethodInfo _onDestroyMethodInfo = new CrossBindingMethodInfo("OnDestroy");
            public override void OnDestroy()
            {
                if(_onDestroyMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _onDestroyMethodInfo.Invoke(this.instance);
            }
            
                    
            private static readonly CrossBindingMethodInfo _onApplicationQuitMethodInfo = new CrossBindingMethodInfo("OnApplicationQuit");
            public override void OnApplicationQuit()
            {
                if(_onApplicationQuitMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.Awake();
                else
                    _onApplicationQuitMethodInfo.Invoke(this.instance);
            }

            private static readonly CrossBindingMethodInfo<bool> _onApplicationPauseMethodInfo = new CrossBindingMethodInfo<bool>("OnApplicationPause");

            public override void OnApplicationPause(bool pause)
            {
                if(_onApplicationPauseMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.OnApplicationPause(pause);
                else
                    _onApplicationPauseMethodInfo.Invoke(this.instance, pause);
            }
            
            private static readonly CrossBindingMethodInfo<bool> _onApplicationFocusMethodInfo = new CrossBindingMethodInfo<bool>("OnApplicationFocus");

            public override void OnApplicationFocus(bool pause)
            {
                if(_onApplicationFocusMethodInfo.CheckShouldInvokeBase(this.instance))
                    base.OnApplicationPause(pause);
                else
                    _onApplicationFocusMethodInfo.Invoke(this.instance, pause);
            }
            
            bool m_bOnMono2GameDllGot = false;
            IMethod m_OnMono2GameDll = null;
            public override object OnMain2HotFix(int funcType, object[] data)
            {
                if (!m_bOnMono2GameDllGot)
                {
                    m_OnMono2GameDll = instance.Type.GetMethod("OnMain2HotFix", 2);
                    m_bOnMono2GameDllGot = true;
                }
                if (m_OnMono2GameDll != null)
                {
                    param2[0] = funcType;
                    param2[1] = data;
                    return appdomain.Invoke(m_OnMono2GameDll, instance, param2);
                }
                else
                {
                    return default(object);
                }
            }
        }
    }
}
#endif