#if USE_ILRUNTIME
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;
using System.Linq;
public unsafe class ActivatorCLR
{
    public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
    {
        BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        MethodBase method;
        Type[] args;
        Type type = typeof(System.Activator);
        args = new Type[] { typeof(System.Type), typeof(System.Reflection.BindingFlags), typeof(System.Reflection.Binder), typeof(System.Object[]), typeof(System.Globalization.CultureInfo) };
        method = type.GetMethod("CreateInstance", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, CreateInstance_0);
        args = new Type[] { typeof(System.Type), typeof(System.Reflection.BindingFlags), typeof(System.Reflection.Binder), typeof(System.Object[]), typeof(System.Globalization.CultureInfo), typeof(System.Object[]) };
        method = type.GetMethod("CreateInstance", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, CreateInstance_1);
        args = new Type[] { typeof(System.Type), typeof(System.Object[]) };
        method = type.GetMethod("CreateInstance", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, CreateInstance_2);
        args = new Type[] { typeof(System.Type), typeof(System.Object[]), typeof(System.Object[]) };
        method = type.GetMethod("CreateInstance", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, CreateInstance_3);
        args = new Type[] { typeof(System.Type) };
        method = type.GetMethod("CreateInstance", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, CreateInstance_4);
    }


    static StackObject* CreateInstance_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
        StackObject* ptr_of_this_method;
        StackObject* __ret = ILIntepreter.Minus(__esp, 5);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
        System.Globalization.CultureInfo @culture = ( System.Globalization.CultureInfo ) typeof(System.Globalization.CultureInfo).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
        System.Object[] @args = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 3);
        System.Reflection.Binder @binder = ( System.Reflection.Binder ) typeof(System.Reflection.Binder).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 4);
        System.Reflection.BindingFlags @bindingAttr = ( System.Reflection.BindingFlags ) typeof(System.Reflection.BindingFlags).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 5);
        System.Type @type = ( System.Type ) typeof(System.Type).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);


        var result_of_this_method = System.Activator.CreateInstance(@type, @bindingAttr, @binder, @args, @culture);

        object obj_result_of_this_method = result_of_this_method;
        if ( obj_result_of_this_method is CrossBindingAdaptorType )
        {
            return ILIntepreter.PushObject(__ret, __mStack, ( ( CrossBindingAdaptorType ) obj_result_of_this_method ).ILInstance, true);
        }
        return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method, true);
    }

    static StackObject* CreateInstance_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
        StackObject* ptr_of_this_method;
        StackObject* __ret = ILIntepreter.Minus(__esp, 6);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
        System.Object[] @activationAttributes = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
        System.Globalization.CultureInfo @culture = ( System.Globalization.CultureInfo ) typeof(System.Globalization.CultureInfo).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 3);
        System.Object[] @args = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 4);
        System.Reflection.Binder @binder = ( System.Reflection.Binder ) typeof(System.Reflection.Binder).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 5);
        System.Reflection.BindingFlags @bindingAttr = ( System.Reflection.BindingFlags ) typeof(System.Reflection.BindingFlags).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 6);
        System.Type @type = ( System.Type ) typeof(System.Type).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);


        var result_of_this_method = System.Activator.CreateInstance(@type, @bindingAttr, @binder, @args, @culture, @activationAttributes);

        object obj_result_of_this_method = result_of_this_method;
        if ( obj_result_of_this_method is CrossBindingAdaptorType )
        {
            return ILIntepreter.PushObject(__ret, __mStack, ( ( CrossBindingAdaptorType ) obj_result_of_this_method ).ILInstance, true);
        }
        return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method, true);
    }

    static StackObject* CreateInstance_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
        StackObject* ptr_of_this_method;
        StackObject* __ret = ILIntepreter.Minus(__esp, 2);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
        System.Object[] @args = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
        System.Type @type = ( System.Type ) typeof(System.Type).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);


        var result_of_this_method = System.Activator.CreateInstance(@type, @args);

        object obj_result_of_this_method = result_of_this_method;
        if ( obj_result_of_this_method is CrossBindingAdaptorType )
        {
            return ILIntepreter.PushObject(__ret, __mStack, ( ( CrossBindingAdaptorType ) obj_result_of_this_method ).ILInstance, true);
        }
        return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method, true);
    }

    static StackObject* CreateInstance_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
        StackObject* ptr_of_this_method;
        StackObject* __ret = ILIntepreter.Minus(__esp, 3);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
        System.Object[] @activationAttributes = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
        System.Object[] @args = ( System.Object[] ) typeof(System.Object[]).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 3);
        System.Type @type = ( System.Type ) typeof(System.Type).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);


        var result_of_this_method = System.Activator.CreateInstance(@type, @args, @activationAttributes);

        object obj_result_of_this_method = result_of_this_method;
        if ( obj_result_of_this_method is CrossBindingAdaptorType )
        {
            return ILIntepreter.PushObject(__ret, __mStack, ( ( CrossBindingAdaptorType ) obj_result_of_this_method ).ILInstance, true);
        }
        return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method, true);
    }

    static StackObject* CreateInstance_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
        StackObject* ptr_of_this_method;
        StackObject* __ret = ILIntepreter.Minus(__esp, 1);

        ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
        System.Type @type = ( System.Type ) typeof(System.Type).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack));
        __intp.Free(ptr_of_this_method);


        var result_of_this_method = System.Activator.CreateInstance(@type);

        object obj_result_of_this_method = result_of_this_method;
        if ( obj_result_of_this_method is CrossBindingAdaptorType )
        {
            return ILIntepreter.PushObject(__ret, __mStack, ( ( CrossBindingAdaptorType ) obj_result_of_this_method ).ILInstance, true);
        }
        return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method, true);
    }

}
#endif