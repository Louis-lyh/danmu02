﻿#if USE_ILRUNTIME
using ILRuntime.CLR.Method;
using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Utils;
using ILRuntime.Reflection;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public unsafe class GameObjectCLR
{
    private static List<ILTypeInstance> _insList = new List<ILTypeInstance>();

    public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
    {
        Type type           = typeof(UnityEngine.GameObject);
        BindingFlags flag   = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
        MethodInfo method;
        Type[] args;

        var methods         = type.GetMethods();

        foreach (var m in methods)
        {
            // AddComponent<T>
            if (m.Name == "AddComponent" && m.GetGenericArguments().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, AddComponentT);
            }
            // GetComponent<T>
            else if (m.Name == "GetComponent" && m.GetGenericArguments().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentT);
            }
            // GetComponentInParent<T>
            else if (m.Name == "GetComponentInParent" && m.GetGenericArguments().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentInParentT);
            }
            // GetComponentInChildren<T>
            else if (m.Name == "GetComponentInChildren" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 0)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentInChildrenT);
            }
            // GetComponentInChildren<T>(Boolean)
            else if (m.Name == "GetComponentInChildren" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentInChildrenTBoolean);
            }
            // GetComponents<T>
            else if (m.Name == "GetComponents" && m.GetGenericArguments().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentsT);
            }
            // GetComponentsInChildren<T>
            else if (m.Name == "GetComponentsInChildren" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 0)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentsInChildrenT);
            }
            // GetComponentsInChildren<T>(Boolean)
            else if (m.Name == "GetComponentsInChildren" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentsInChildrenTBoolean);
            }
            // GetComponentsInParent<T>
            else if (m.Name == "GetComponentsInParent" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 0)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentsInParentT);
            }
            // GetComponentsInParent<T>(Boolean)
            else if (m.Name == "GetComponentsInParent" && m.GetGenericArguments().Length == 1 && m.GetParameters().Length == 1)
            {
                app.RegisterCLRMethodRedirection(m, GetComponentsInParentTBoolean);
            }
        }

        // AddComponent(Type)
        args    = new Type[] { typeof(System.Type) };
        method  = type.GetMethod("AddComponent", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, AddComponent);

        // GetComponent(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponent", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponent);

        // GetComponentInParent(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponentInParent", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentInParent);

        // GetComponentInChildren(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponentInChildren", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentInChildren);

        // GetComponentInChildren(Type, Boolean)
        args    = new Type[]{ typeof(System.Type), typeof(System.Boolean) };
        method  = type.GetMethod("GetComponentInChildren", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentInChildrenBoolean);

        // GetComponents(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponents", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponents);

        // GetComponentsInChildren(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponentsInChildren", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentsInChildren);

        // GetComponentsInChildren(Type, Boolean)
        args    = new Type[]{ typeof(System.Type), typeof(System.Boolean) };
        method  = type.GetMethod("GetComponentsInChildren", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentsInChildrenBoolean);

        // GetComponentsInParent(Type)
        args    = new Type[]{ typeof(System.Type) };
        method  = type.GetMethod("GetComponentsInParent", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentsInParent);

        // GetComponentsInParent(Type, Boolean)
        args    = new Type[]{ typeof(System.Type), typeof(System.Boolean) };
        method  = type.GetMethod("GetComponentsInParent", flag, null, args, null);
        app.RegisterCLRMethodRedirection(method, GetComponentsInParentBoolean);
    }

    private static StackObject* AddComponentT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // AddComponent应该有且只有1个泛型参数
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res;
            if (type is CLRType)
            {
                res     = instance.AddComponent(type.TypeForCLR);
            }
            else
            {
                var ilType              = type as ILType;
                var ilInstance          = new ILTypeInstance(ilType, false);
                var clrInstance         = instance.AddComponent(ilType.TypeForCLR) as IMonoHandler;

                clrInstance.ILInstance  = ilInstance;
                clrInstance.AppDomain   = __domain;

                ilInstance.CLRInstance  = clrInstance;

                res                     = clrInstance.ILInstance;

                //
                clrInstance.Awake();
                if (instance.activeInHierarchy)
                {
                    clrInstance.OnEnable();
                }
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* AddComponent(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret             = ILIntepreter.Minus(__esp, 2);

        var ptr             = ILIntepreter.Minus(__esp, 2);
        var instance        = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                 = ILIntepreter.Minus(__esp, 1);
        var componentType   = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType     = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res;

        if (runtimeType == null)
        {
            var type        = (componentType as ILRuntimeWrapperType).RealType;
            res             = instance.AddComponent(type);
        }
        else
        {
            var type                = runtimeType.ILType;
            var ilType              = type as ILType;
            var ilInstance          = new ILTypeInstance(ilType, false);
            var clrInstance         = instance.AddComponent(ilType.TypeForCLR) as IMonoHandler;

            clrInstance.ILInstance  = ilInstance;
            clrInstance.AppDomain   = __domain;

            ilInstance.CLRInstance  = clrInstance;

            res                     = clrInstance.ILInstance;

            //
            clrInstance.Awake();
            if (instance.activeInHierarchy)
            {
                clrInstance.OnEnable();
            }
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                res     = instance.GetComponent(type.TypeForCLR);
            }
            else
            {
                var clrInstances    = instance.GetComponents<MonoBehaviour>();
                for (int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        res         = clrInstance.ILInstance;
                        break;
                    }
                }
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponent(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret             = ILIntepreter.Minus(__esp, 2);

        var ptr             = ILIntepreter.Minus(__esp, 2);
        var instance        = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                 = ILIntepreter.Minus(__esp, 1);
        var componentType   = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType     = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res          = null;

        if (runtimeType == null)
        {
            var type        = (componentType as ILRuntimeWrapperType).RealType;
            res             = instance.GetComponent(type);
        }
        else
        {
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponents<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    res         = clrInstance.ILInstance;
                    break;
                }
            }
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentInParentT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                
                res     = instance.GetComponentInParent(type.TypeForCLR);
            }
            else
            {
                var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>();
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        res         = clrInstance.ILInstance;
                        break;
                    }
                }
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentInParent(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentInParent(type);
        }
        else
        {
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    res         = clrInstance.ILInstance;
                    break;
                }
            }
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentInChildrenT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                
                res     = instance.GetComponentInChildren(type.TypeForCLR);
            }
            else
            {
                var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>();
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        res         = clrInstance.ILInstance;
                        break;
                    }
                }
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentInChildrenTBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var genericArgument     = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                res     = instance.GetComponentInChildren(type.TypeForCLR, includeInactive);
            }
            else
            {
                var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>(includeInactive);
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        res         = clrInstance.ILInstance;
                        break;
                    }
                }
            }

            return ILIntepreter.PushObject(ret, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentInChildren(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentInChildren(type);
        }
        else
        {
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    res         = clrInstance.ILInstance;
                    break;
                }
            }
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentInChildrenBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 3);

        var ptr                 = ILIntepreter.Minus(__esp, 3);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 2);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentInChildren(type, includeInactive);
        }
        else
        {
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>(includeInactive);
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    res         = clrInstance.ILInstance;
                    break;
                }
            }
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentsT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                var comps   = instance.GetComponents(type.TypeForCLR);
                var len     = comps.Length;
                var array   = Array.CreateInstance(type.TypeForCLR, len);
                for (int i = 0; i < len; i ++)
                {
                    array.SetValue(comps[i], i);
                }
                res         = array;
            }
            else
            {
                _insList.Clear();
                var clrInstances    = instance.GetComponents<MonoBehaviour>();
                for (int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        _insList.Add(clrInstance.ILInstance);
                    }
                }
                res = _insList.ToArray();
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponents(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret             = ILIntepreter.Minus(__esp, 2);

        var ptr             = ILIntepreter.Minus(__esp, 2);
        var instance        = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                 = ILIntepreter.Minus(__esp, 1);
        var componentType   = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponents(type);
        }
        else
        {
            _insList.Clear();
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponents<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    _insList.Add(clrInstance.ILInstance);
                }
            }
            res = _insList.ToArray();
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentsInChildrenT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                var comps   = instance.GetComponentsInChildren(type.TypeForCLR);
                var len     = comps.Length;
                var array   = Array.CreateInstance(type.TypeForCLR, len);
                for (int i = 0; i < len; i ++)
                {
                    array.SetValue(comps[i], i);
                }
                res         = array;
            }
            else
            {
                _insList.Clear();
                var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>();
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        _insList.Add(clrInstance.ILInstance);
                    }
                }
                res = _insList.ToArray();
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentsInChildrenTBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var genericArgument     = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                var comps   = instance.GetComponentsInChildren(type.TypeForCLR, includeInactive);
                var len     = comps.Length;
                var array   = Array.CreateInstance(type.TypeForCLR, len);
                for (int i = 0; i < len; i ++)
                {
                    array.SetValue(comps[i], i);
                }
                res         = array;
            }
            else
            {
                _insList.Clear();
                var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>(includeInactive);
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        _insList.Add(clrInstance.ILInstance);
                    }
                }
                res = _insList.ToArray();
            }

            return ILIntepreter.PushObject(ret, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentsInChildren(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentsInChildren(type);
        }
        else
        {
            _insList.Clear();
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    _insList.Add(clrInstance.ILInstance);
                }
            }
            res = _insList.ToArray();
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentsInChildrenBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 3);

        var ptr                 = ILIntepreter.Minus(__esp, 3);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 2);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentsInChildren(type, includeInactive);
        }
        else
        {
            _insList.Clear();
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInChildren<MonoBehaviour>(includeInactive);
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    _insList.Add(clrInstance.ILInstance);
                }
            }
            res = _insList.ToArray();
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentsInParentT(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ptr         = __esp - 1;
        var instance    = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;

        if (instance == null)
        {
            throw new System.NullReferenceException();
        }

        __intp.Free(ptr);

        var genericArgument = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                var comps   = instance.GetComponentsInParent(type.TypeForCLR);
                var len     = comps.Length;
                var array   = Array.CreateInstance(type.TypeForCLR, len);
                for (int i = 0; i < len; i ++)
                {
                    array.SetValue(comps[i], i);
                }
                res         = array;
            }
            else
            {
                _insList.Clear();
                var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>();
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        _insList.Add(clrInstance.ILInstance);
                    }
                }
                res = _insList.ToArray();
            }

            return ILIntepreter.PushObject(ptr, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentsInParentTBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var genericArgument     = __method.GenericArguments;
        // 
        if (genericArgument != null && genericArgument.Length == 1)
        {
            var type    = genericArgument[0];
            object res  = null;

            if (type is CLRType)
            {
                var comps   = instance.GetComponentsInParent(type.TypeForCLR, includeInactive);
                var len     = comps.Length;
                var array   = Array.CreateInstance(type.TypeForCLR, len);
                for (int i = 0; i < len; i ++)
                {
                    array.SetValue(comps[i], i);
                }
                res         = array;
            }
            else
            {
                _insList.Clear();
                var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>(includeInactive);
                for(int i = 0; i < clrInstances.Length; i++)
                {
                    var clrInstance = clrInstances[i] as IMonoHandler;
                    if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                    {
                        _insList.Add(clrInstance.ILInstance);
                    }
                }
                res = _insList.ToArray();
            }

            return ILIntepreter.PushObject(ret, __mStack, res);
        }

        return __esp;
    }

    private static StackObject* GetComponentsInParent(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 2);

        var ptr                 = ILIntepreter.Minus(__esp, 2);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentsInParent(type);
        }
        else
        {
            _insList.Clear();
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>();
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    _insList.Add(clrInstance.ILInstance);
                }
            }
            res = _insList.ToArray();
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }

    private static StackObject* GetComponentsInParentBoolean(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
    {
        ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;

        var ret                 = ILIntepreter.Minus(__esp, 3);

        var ptr                 = ILIntepreter.Minus(__esp, 3);
        var instance            = StackObject.ToObject(ptr, __domain, __mStack) as GameObject;
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 2);
        var componentType       = StackObject.ToObject(ptr, __domain, __mStack);
        __intp.Free(ptr);

        ptr                     = ILIntepreter.Minus(__esp, 1);
        var includeInactive     = *(bool*)&ptr->Value;
        __intp.Free(ptr);

        var runtimeType         = componentType as ILRuntime.Reflection.ILRuntimeType;
        object res              = null;

        if (runtimeType == null)
        {
            var type            = (componentType as ILRuntimeWrapperType).RealType;
            res                 = instance.GetComponentsInParent(type, includeInactive);
        }
        else
        {
            _insList.Clear();
            var type            = runtimeType.ILType;
            var clrInstances    = instance.GetComponentsInParent<MonoBehaviour>(includeInactive);
            for(int i = 0; i < clrInstances.Length; i++)
            {
                var clrInstance = clrInstances[i] as IMonoHandler;
                if (clrInstance != null && clrInstance.ILInstance != null && clrInstance.ILInstance.Type == type)
                {
                    _insList.Add(clrInstance.ILInstance);
                }
            }
            res = _insList.ToArray();
        }

        return ILIntepreter.PushObject(ret, __mStack, res);
    }
}

public interface IMonoHandler
{
    ILTypeInstance ILInstance { get; set; }
    ILRuntime.Runtime.Enviorment.AppDomain AppDomain { get; set; }
    void Awake();
    void OnEnable();
}
#endif