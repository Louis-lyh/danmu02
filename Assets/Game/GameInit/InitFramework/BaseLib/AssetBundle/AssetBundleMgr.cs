using System;
using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace GameInit.Framework
{
    public class AssetBundleMgr : Singleton<AssetBundleMgr>
    {
        //资源引用
        private AssetBundleManifest _abManifest;

        // 所有已经加载过的assetBundle， key: ab名字
        private Dictionary<string, AssetBundle> _allLoadedBundles;

        // assetBundle引用资源 key: ab名字
        private Dictionary<string, int> _dictBundleCnt;

        // 资源路径对应AssetBundle
        private Dictionary<string, string> _assetPath2BundleName;

        #region 初始化配置数据

        public static long LastestAssetVersion
        {
            get { return _streamingVersion > _persistentVersion ? _streamingVersion : _persistentVersion; }
        }

        /// <summary>
        /// 资源版本号
        /// </summary>
        private static long _streamingVersion;

        /// <summary>
        /// 资源版本号
        /// </summary>
        private static long _persistentVersion;

        /// <summary>
        /// AssetBundle名字和文件结构（bundle名字，MD5，资源号）的对应
        /// </summary>
        private static Dictionary<string, GameAbInfo> _streamingStructDic;

        /// <summary>
        /// AssetBundle名字和文件结构（bundle名字，MD5，资源号）的对应
        /// </summary>
        private static Dictionary<string, GameAbInfo> _persistentStructDic;

        /// <summary>
        /// 资源路径和AssetBundle名字的映射
        /// </summary>
        private static Dictionary<string, string> k_AssetPath2BundleNameDic;
        //////////////////////////////////////////////////////////

        #endregion

        #region 初始化

        public async UniTask Init()
        {
            _streamingStructDic = new Dictionary<string, GameAbInfo>();
            _persistentStructDic = new Dictionary<string, GameAbInfo>();

            #region 加载StreamingAssets下的版本号和文件清单

            // 加载StreamingAssets下的版本号
            string verFilePath = AssetBundleConst.StreamingVersionFile;
            string abAssetFile = AssetBundleConst.StreamingAbAssetFile;
            if (Application.platform != RuntimePlatform.Android)
            {
                verFilePath = "file://" + verFilePath;
                abAssetFile = "file://" + abAssetFile;
            }

            var uri = new Uri(verFilePath);
            var req = await UnityWebRequest.Get(uri.AbsoluteUri).SendWebRequest();
            var text = req.downloadHandler?.text;
            _streamingVersion = AssetBundleUtils.DecodeVersion(text);
            req.Dispose();
            Logger.LogMagenta("AssetBundleMgr.Init() => StreamingAssets目录下资源版版本是:" + _streamingVersion);

            uri = new Uri(abAssetFile);
            req = await UnityWebRequest.Get(uri.AbsoluteUri).SendWebRequest();
            _streamingStructDic = AssetBundleUtils.DecodeStringPair(req.downloadHandler?.data);

            #endregion

            #region 加载PersistentData下的版本号和文件清单

            verFilePath = AssetBundleConst.PersistentVersionFile;
            abAssetFile = AssetBundleConst.PersistentAbAssetFile;

            if (File.Exists(verFilePath))
                _persistentVersion = AssetBundleUtils.DecodeVersion(File.ReadAllText(verFilePath));
            Logger.LogMagenta("AssetBundleMgr.Init() => PersistentAssets目录下资源版版本是:" + _persistentVersion);

            if (File.Exists(abAssetFile))
                _persistentStructDic = AssetBundleUtils.DecodeStringPair(File.ReadAllBytes(abAssetFile), false);

            #endregion

            // 选取版本最高的资源作为路径来加载配置文件
            var manifestName = AssetBundleConst.AssetsDirectory; //防止Persitent文件夹内的版本号较高但并未有manifest文件
            var assetPathFile = "";
            var manifestFile = "";
            if (_streamingVersion < _persistentVersion && _persistentStructDic.ContainsKey(manifestName))
            {
                assetPathFile = AssetBundleConst.PersistentAbAssetFile;
                manifestFile = AssetBundleConst.PersistentManifestFile;
            }
            else
            {
                assetPathFile = AssetBundleConst.StreamingAbAssetFile;
                manifestFile = AssetBundleConst.StreamingManifestFile;
            }

            // 获取资源路径和Bundle名字的对应
            byte[] assetPathFileData;
            if (assetPathFile.Contains("://") || assetPathFile.Contains(":///"))
            {
                uri = new Uri(assetPathFile);
                req = await UnityWebRequest.Get(uri.AbsoluteUri).SendWebRequest();
                assetPathFileData = req.downloadHandler?.data;
            }
            else
            {
                assetPathFileData = File.Exists(assetPathFile) ? File.ReadAllBytes(assetPathFile) : new byte[0];
            }

            _allLoadedBundles = new Dictionary<string, AssetBundle>();
            _dictBundleCnt = new Dictionary<string, int>();
            _assetPath2BundleName = new Dictionary<string, string>();
            var dic = AssetBundleUtils.DecodeStringPair(assetPathFileData);
            foreach (var rec in dic.Values)
            {
                if (rec.IncludeFiles.Count == 0)
                {
                    continue;
                }

                for (int i = 0; i < rec.IncludeFiles.Count; i++)
                {
                    if (string.IsNullOrEmpty(rec.IncludeFiles[i]))
                        continue;
                    var filename = rec.IncludeFiles[i].ToLower();
                    if (_assetPath2BundleName.ContainsKey(filename))
                    {
                        Debug.Log("[AssetBundleUtil.InitAssetBundle_Cor() => filename:" + filename + ", abname:" +
                                  rec.AbName + ", oldName:" + _assetPath2BundleName[filename] + "]");
                        continue;
                    }

                    _assetPath2BundleName.Add(filename, rec.AbName);
                }
            }

            //加载Manifest文件
            var bundle = await AssetBundle.LoadFromFileAsync(manifestFile);

            if (bundle == null)
            {
                // fixed me：这里bundle可能加载不到，需要进行修复处理，直接退出游戏
                CleanPersistent();
            }

            _abManifest = bundle?.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            bundle?.Unload(false);
        }

        #endregion

        public async UniTask PrepareCommonAssetBundle(string bundleName)
        {
            await LoadAssetBundleAsync(bundleName);
        }

        #region 同步加载资源

        public T LoadAssetSync<T>(string assetPath) where T : Object
        {
            var ext = Path.GetExtension(assetPath);
            var path = assetPath.Replace(ext, "");
            if (_assetPath2BundleName.TryGetValue(path.ToLower(), out var bundleName))
            {
                var ab = LoadAssetBundleSync(bundleName);
                assetPath = Path.GetFileNameWithoutExtension(assetPath);
                return ab?.LoadAsset<T>(assetPath);
            }

            Debug.LogError("AssetBundleMgr.LoadAssetSync() => 加载资源失败，资源路径找不到对应的AssetBundle, assetPath:" + assetPath);
            return null;
        }

        // 同步加载assetBundle
        private AssetBundle LoadAssetBundleSync(string bundleName)
        {
            if (!_allLoadedBundles.TryGetValue(bundleName, out var assetBundle))
            {
                var abPath = GetAssetBundlePath(bundleName);
                assetBundle = AssetBundle.LoadFromFile(abPath);
                _allLoadedBundles[bundleName] = assetBundle;
            }

            IncBundleDependCnt(bundleName);
            var depends = _abManifest.GetDirectDependencies(bundleName);
            foreach (var depend in depends)
            {
                LoadAssetBundleSync(depend);
            }

            return assetBundle;
        }

        #endregion

        public bool AssetExists(string assetPath)
        {
            var ext = Path.GetExtension(assetPath);
            var path = assetPath.Replace(ext, "");
            return _assetPath2BundleName.ContainsKey(path.ToLower());
        }

        #region 异步加载资源

        public async UniTask<T> LoadAssetAsync<T>(string assetPath) where T : Object
        {
            var ext = Path.GetExtension(assetPath);
            var path = assetPath.Replace(ext, "");
            if (!_assetPath2BundleName.TryGetValue(path.ToLower(), out var bundleName))
            {
                Debug.LogError("AssetBundleMgr.LoadAssetAsync() => 找不到资源路径为:" + assetPath + "对应的ab名字");
                return null;
            }

            var assetBundle = await LoadAssetBundleAsync(bundleName);
            if (assetBundle == null)
                return null;
            var asset = await assetBundle.LoadAssetAsync(Path.GetFileNameWithoutExtension(assetPath));
            return (T) asset;
        }

        public async UniTask<AsyncOperation> LoadSceneAsync(string sceneName,
            LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            if (!_assetPath2BundleName.TryGetValue(sceneName.ToLower(), out var bundleName))
            {
                Logger.LogError("AssetBundleMgr.LoadSceneAsync() => 找不到资源路径为:" + sceneName + "对应的ab名字");
                return null;
            }

            await LoadAssetBundleAsync(bundleName);
            return SceneManager.LoadSceneAsync(sceneName, loadMode);
        }

        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            if (!_assetPath2BundleName.TryGetValue(sceneName.ToLower(), out var bundleName))
            {
                Logger.LogMagenta("AssetBundleMgr.UnloadSceneAsync() => 找不到sceneName:" + sceneName + "对应的ab资源");
                return SceneManager.UnloadSceneAsync(sceneName);
            }

            UnloadAssetBundle(bundleName);
            return SceneManager.UnloadSceneAsync(sceneName);
        }

        private static Dictionary<string, List<AutoResetUniTaskCompletionSource>> _tmpLoadTask =
            new Dictionary<string, List<AutoResetUniTaskCompletionSource>>();

        private async UniTask<AssetBundle> LoadAssetBundleAsync(string bundleName)
        {
            if (!_allLoadedBundles.TryGetValue(bundleName, out var assetBundle))
            {
                var abPath = GetAssetBundlePath(bundleName);
                if (_tmpLoadTask.ContainsKey(abPath))
                {
                    var waiteTask = AutoResetUniTaskCompletionSource.Create();
                    _tmpLoadTask[abPath].Add(waiteTask);
                    await waiteTask.Task;
                    assetBundle = _allLoadedBundles[bundleName];
                }
                else
                {
                    _tmpLoadTask[abPath] = new List<AutoResetUniTaskCompletionSource>();
                    assetBundle = await AssetBundle.LoadFromFileAsync(abPath);
                    if (assetBundle != null)
                    {
                        _allLoadedBundles[bundleName] = assetBundle;
                    }

                    foreach (var value in _tmpLoadTask[abPath])
                        value.TrySetResult();
                    _tmpLoadTask.Remove(abPath);
                }
            }

            IncBundleDependCnt(bundleName);

            var dependencies = _abManifest.GetAllDependencies(bundleName);
            foreach (var dependency in dependencies)
            {
                if (_allLoadedBundles.ContainsKey(dependency))
                {
                    continue;
                }

                await LoadAssetBundleAsync(dependency);
            }

            return assetBundle;
        }

        #endregion

        public void UnloadRes(string assetPath, bool isForce = false)
        {
            if (_assetPath2BundleName.TryGetValue(assetPath, out var bundleName))
            {
                UnloadAssetBundle(bundleName, isForce);
            }
        }

        private void UnloadAssetBundle(string bundleName, bool needForceUnload = false)
        {
            string[] depBundles = _abManifest.GetDirectDependencies(bundleName);
            for (int i = 0, max = depBundles.Length; i < max; i++)
            {
                var depBundleName = depBundles[i];
                UnloadAssetBundle(depBundleName, needForceUnload);
            }

            DecBundleDependCnt(bundleName, needForceUnload);
        }

        private void IncBundleDependCnt(string bundleName)
        {
            if (!_dictBundleCnt.ContainsKey(bundleName))
            {
                _dictBundleCnt[bundleName] = 0;
            }

            _dictBundleCnt[bundleName]++;
        }

        private void DecBundleDependCnt(string bundleName, bool needForceUnload = false)
        {
            if (_dictBundleCnt.ContainsKey(bundleName))
            {
                _dictBundleCnt[bundleName]--;
                var lastCnt = needForceUnload ? 0 : _dictBundleCnt[bundleName];
                if (lastCnt == 0)
                {
                    _dictBundleCnt.Remove(bundleName);
                    var bundle = _allLoadedBundles[bundleName];
                    try
                    {
                        bundle.Unload(true);
                    }
                    catch (Exception e)
                    {
                        Logger.LogMagenta("AssetBundleMgr.DecBundleDependCnt:释放ab出错，error:" + e.Message);
                    }

                    _allLoadedBundles.Remove(bundleName);
                }
            }
        }

        private string GetAssetBundlePath(string assetBundle)
        {
            if (!_streamingStructDic.TryGetValue(assetBundle, out var streamingRec))
            {
                streamingRec = new GameAbInfo();
                streamingRec.AbMd5 = String.Empty;
                streamingRec.AbName = assetBundle;
                streamingRec.FileSize = 0;
                streamingRec.FileVer = 0;
                _streamingStructDic.Add(assetBundle, streamingRec);
            }

            _persistentStructDic.TryGetValue(assetBundle, out var persistentRec);

            var pathEnd = "/" + assetBundle;

            ///如果因为AB被全部清空后，重新生成所有AB时，md5可能不会变化，但是ver会变化，此时，直接取streaming目录下的资源
            if (persistentRec == null || streamingRec.AbMd5.Equals(persistentRec.AbMd5))
                return AssetBundleConst.StreamingABPath + pathEnd;
            var path = streamingRec.FileVer < persistentRec.FileVer
                ? AssetBundleConst.PersistentABPath + pathEnd
                : AssetBundleConst.StreamingABPath + pathEnd;
            return path;
        }

        #region 下载远程资源必要辅助接口。比如获取文件的最新MD5和version

        /// <summary>
        /// 获取Bundle的最新版本号
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        public static long GetLastestAssetVer(string bundleName)
        {
            _streamingStructDic.TryGetValue(bundleName, out var streamingRec);
            _persistentStructDic.TryGetValue(bundleName, out var persistentRec);

            if (persistentRec == null && streamingRec == null)
                return 0;
            if (persistentRec != null && streamingRec == null)
                return persistentRec.FileVer;

            if (persistentRec == null && streamingRec != null)
                return streamingRec.FileVer;

            var sv = streamingRec.FileVer;
            var pv = persistentRec.FileVer;
            return sv > pv ? sv : pv;
        }

        public static string GetLastestMD5(string bundleName)
        {
            _streamingStructDic.TryGetValue(bundleName, out var streamingRec);
            _persistentStructDic.TryGetValue(bundleName, out var persistentRec);

            if (persistentRec == null && streamingRec == null)
                return null;
            if (persistentRec != null && streamingRec == null)
                return persistentRec.AbMd5;
            if (persistentRec == null && streamingRec != null)
                return streamingRec.AbMd5;

            var sv = streamingRec.FileVer;
            var pv = persistentRec.FileVer;
            return sv > pv ? streamingRec.AbMd5 : persistentRec.AbMd5;
        }

        /// <summary>
        /// 更新MD5文件
        /// </summary>
        /// <param name="bundleName"></param>
        /// <param name="md5"></param>
        /// <param name="ver"></param>
        public static void UpdateMD5ToPersistentFile(GameAbInfo remoteAbInfo)
        {
            _persistentStructDic[remoteAbInfo.AbName] = remoteAbInfo;
        }

        /// <summary>
        /// 更新MD5文件并写入
        /// </summary>
        /// <param name="bundleName"></param>
        /// <param name="md5"></param>
        /// <param name="ver"></param>
        /// <param name="security"></param>
        public static void WriteMD5ToPersistentFile(GameAbInfo remoteAbInfo, bool security = false)
        {
            if (!Directory.Exists(AssetBundleConst.PersistentDataPath))
                Directory.CreateDirectory(AssetBundleConst.PersistentDataPath);
            UpdateMD5ToPersistentFile(remoteAbInfo);

            File.WriteAllBytes(AssetBundleConst.PersistentAbAssetFile,
                AssetBundleUtils.EncodeStringPair(_persistentStructDic, security));
        }

        #endregion

        private void CleanPersistent()
        {
            if (Directory.Exists(AssetBundleConst.PersistentDataPath))
            {
                Directory.Delete(AssetBundleConst.PersistentDataPath, true);
            }
#if !UNITY_EDITOR
            Application.Quit();
#endif
        }
    }
}