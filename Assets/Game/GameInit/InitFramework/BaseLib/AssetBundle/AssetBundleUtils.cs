using System.Collections.Generic;
using System.Text;

namespace GameInit.Framework
{
    public static class AssetBundleUtils
    {
        /// <summary>
        /// 解析资源版本号
        /// </summary>
        /// <param name="versionStr"></param>
        /// <returns></returns>
        public static long DecodeVersion(string versionStr)
        {
            return versionStr.IsValid() ? long.Parse(versionStr) : 0;
        }
        
        /// <summary>
        /// 将内容以行为单位进行分割
        /// </summary>
        /// <param name="fileList"></param>
        /// <returns></returns>
        public static string[] SplitFileList(string fileList)
        {
            string[] contents = fileList.Split('\n');
            List<string> contentList = new List<string>();
            for (int i = 0, max = contents.Length; i < max; i++)
            {
                if (!string.IsNullOrEmpty(contents[i]))
                {
                    contentList.Add(contents[i]);
                }
            }
            return contentList.ToArray();
        }
        
        /// <summary>
        /// 将加密字符串清单还原成记录文件用的数据结构
        /// </summary>
        /// <param name="md5Code"></param>
        /// <returns></returns>
        public static Dictionary<string, GameAbInfo> DecodeStringPair(byte[] buffer, bool security = true)
        {
            var dic = new Dictionary<string, GameAbInfo>();
            if (buffer == null || buffer.Length == 0)
                return dic;
            var codes = SplitFileList(System.Text.UTF8Encoding.UTF8.GetString(buffer));
            foreach (var code in codes )
            {
                var abInfo = new GameAbInfo();
                if (!abInfo.Parse(code.Trim()))
                {
                    Logger.LogMagenta("[AssetBundleUtil.DecodeStringPair() => 解析GameAbInfo出错，value:" + code + "]");
                    continue;
                }
                dic.Add(abInfo.AbName, abInfo);
            }
            return dic;
        }
        
        /// <summary>
        /// 将记录文件用的数据结构加密成一份清单
        /// </summary>
        /// <param name="dic"></param>
        /// <returns></returns>
        public static byte[] EncodeStringPair(Dictionary<string, GameAbInfo> dic, bool security)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var rec in dic.Values)
            {
                if(string.IsNullOrEmpty(rec.AbMd5))
                    continue;
                sb.AppendLine(rec.AbName + "|" + rec.AbMd5 + "|" + rec.FileSize + "|" + rec.FileVer + "|" + rec.IncludeFileToString);
            }
            var buffer = System.Text.UTF8Encoding.UTF8.GetBytes(sb.ToString());
            return buffer;
        }
    }
}