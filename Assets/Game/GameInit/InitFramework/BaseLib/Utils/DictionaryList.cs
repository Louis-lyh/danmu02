using System;
using System.Collections.Generic;

namespace GameInit.Framework
{
    public class DictionaryList<KeyT, ValueT> : IDisposable
    {
        ///////////////////////////////////以下为静态成员//////////////////////////////
        private static object DataLocker = new object();


        ///////////////////////////////////以下为非静态成员///////////////////////////
        private Dictionary<KeyT, ValueT> _dic = new Dictionary<KeyT, ValueT>(); // 用于取值
        private List<ValueT> _list = new List<ValueT>();    // 用于值的索引
        private List<KeyT> _keyList = new List<KeyT>();     // 用于键的索引

        /// <summary>
        /// 获取一个值
        /// </summary>
        public ValueT GetValue(KeyT key)
        {
            ValueT tar = default(ValueT);
            _dic.TryGetValue(key, out tar);
            return tar;
        }

        /// <summary>
        /// 获取一个值
        /// </summary>
        public bool TryGetValue(KeyT key, out ValueT value)
        {
            return _dic.TryGetValue(key, out value);
        }

        /// <summary>
        /// 获得指定位置的值
        /// </summary>
        public ValueT GetValueAt(int index)
        {
            if (index < 0 || index >= _list.Count)
                return default(ValueT);
            return _list[index];
        }

        /// <summary>
        /// 获得指定位置的值
        /// </summary>
        public ValueT GetValueAtNoCheck(int index)
        {
            return _list[index];
        }

        /// <summary>
        /// 插入一个值，如果键存在，则插入无效
        /// </summary>
        public void Insert(KeyT key, ValueT value, int index)
        {
            lock (DataLocker)
            {
                int keyIdx = _keyList.IndexOf(key);
                if (keyIdx < 0)
                {
                    _list.Insert(index, value);
                    _keyList.Insert(index, key);
                    _dic[key] = value;
                }
            }
        }

        /// <summary>
        /// 添加一个项.如果键存在，则更新该键值
        /// </summary>
        public void Add(KeyT key, ValueT value)
        {
            lock (DataLocker)
            {
                int idx = _keyList.IndexOf(key);
                if (idx >= 0)
                {//已经存在该值
                    _dic[key] = value;
                    _list[idx] = value;
                    Logger.LogError("添加了相同key:" + key+ ","+value.ToString() + "数据将会被覆盖");
                }
                else
                {
                    _dic.Add(key, value);
                    _list.Add(value);
                    _keyList.Add(key);
                }
            }
        }

        /// <summary>
        /// 是否存在键
        /// </summary>
        public bool HasKey(KeyT key)
        {
            bool haskey = _dic.ContainsKey(key);
            return haskey;
        }

        /// <summary>
        /// 获取指定位置的索引
        /// </summary>
        public KeyT GetKeyAt(int idx)
        {
            if (idx >= _keyList.Count)
                return default(KeyT);
            return _keyList[idx];
        }

        /// <summary>
        /// 获取指定位置的索引
        /// </summary>
        public KeyT GetKeyAtNoCheck(int idx)
        {
            return _keyList[idx];
        }

        /// <summary>
        /// 获得指定值的第一个位置索引
        /// </summary>
        public int IndexOfValue(ValueT value)
        {
            int idx = _list.IndexOf(value);
            return idx;
        }

        /// <summary>
        /// 获得指定键的索引
        /// </summary>
        public int IndexOfKey(KeyT key)
        {
            int idx = _keyList.IndexOf(key);
            return idx;
        }

        /// <summary>
        /// 更新值。如果键不存在，则什么都不做
        /// </summary>
        public void UpdateValue(KeyT key, ValueT value)
        {
            lock (DataLocker)
            {
                int idx = _keyList.IndexOf(key);
                if (idx >= 0)
                {//已经存在该值
                    _dic[key] = value;
                    _list[idx] = value;
                }
            }
        }

        /// <summary>
        /// 移除一个项
        /// </summary>
        public bool Remove(KeyT key)
        {
            lock (DataLocker)
            {
                int idx = _keyList.IndexOf(key);
                if (idx >= 0)
                {
                    _list.RemoveAt(idx);
                    _dic.Remove(key);
                    _keyList.RemoveAt(idx);
                }
                else
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 移除指定位置的项
        /// </summary>
        public bool RemoveAt(int index)
        {
            lock (DataLocker)
            {
                if (index >= 0)
                {
                    KeyT key = _keyList[index];
                    _list.RemoveAt(index);
                    _dic.Remove(key);
                    _keyList.RemoveAt(index);
                }
                else
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 清空
        /// </summary>
        public void Clear()
        {
            lock (DataLocker)
            {
                _dic.Clear();
                _list.Clear();
                _keyList.Clear();
            }
        }

        /// <summary>
        /// 销毁
        /// </summary>
        public void Dispose()
        {
            Clear();
            _dic = null;
            _list = null;
            _keyList = null;
        }

        /// <summary>
        /// 数量
        /// </summary>
        public int Count
        {
            get { return _dic.Count; }
        }
    }

}
