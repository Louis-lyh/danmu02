using System;
using UnityEngine;
using System.Collections.Generic;
namespace GameInit.Framework
{
    public enum InputEventType
    {
        MouseDown,
        MouseUp,
        MouseDrag,
        MouseClick,
    }

    public class InputController : MonoBehaviour
    {
        public static InputController Instance { get; private set; }
        private Dictionary<InputEventType, Delegate> _dictAllInputEvent;
        private const float Drag_Gap = 20f;

        public void AddInputEvent(InputEventType eventType, Action<UnityEngine.Vector2> action)
        {
            Delegate tempAction = null;
            if (_dictAllInputEvent.ContainsKey(eventType))
                tempAction = _dictAllInputEvent[eventType];
            _dictAllInputEvent[eventType] = (Action<UnityEngine.Vector2>)Delegate.Combine((Action<UnityEngine.Vector2>)tempAction, action);
        }

        public void RemoveInputEvent(InputEventType eventType, Action<UnityEngine.Vector2> method)
        {
            if (!HasEvent(eventType))
                return;
            _dictAllInputEvent[eventType] = (Action<UnityEngine.Vector2>)Delegate.Remove((Action<UnityEngine.Vector2>)_dictAllInputEvent[eventType], method);
        }

        private bool HasEvent(InputEventType type)
        {
            if (!_dictAllInputEvent.ContainsKey(type))
                return false;
            return _dictAllInputEvent[type] != null;
        }

        private void DispatchInputEvent(InputEventType eventType, UnityEngine.Vector2 p1)
        {
            if (!HasEvent(eventType))
                return;
            var tempAction = _dictAllInputEvent[eventType].GetInvocationList();
            for (int i = 0; i < tempAction.Length; i++)
            {
                if (tempAction[i].GetType() != typeof(Action<UnityEngine.Vector2>))
                    continue;
                var action = (Action<UnityEngine.Vector2>)tempAction[i];
                action?.Invoke(p1);
            }
        }

        private bool _blPressed;
        private bool _blClick;
        private bool _blTmpPressed;
        private UnityEngine.Vector2 _oldMousePos;
        private UnityEngine.Vector2 _tmpMousePos;

        protected void Awake()
        {
            _dictAllInputEvent = new Dictionary<InputEventType, Delegate>();
            Instance = this;
        }

        private void OnEnable()
        {
            ResetInit();
        }
        private void OnDisable()
        {
            ResetInit();
        }

        private void ResetInit()
        {
            _blPressed = false;
            _blClick = false;
        }

        protected void Update()
        {
            _blTmpPressed = Input.GetMouseButton(0);
            _tmpMousePos = Input.mousePosition;

            if (_blTmpPressed != _blPressed)
            {
                if (_blTmpPressed)
                {
                    _blClick = true;
                    DispatchInputEvent(InputEventType.MouseDown, _tmpMousePos);
                }
                else
                {
                    DispatchInputEvent(InputEventType.MouseUp, _tmpMousePos);
                    if (_blClick)
                        DispatchInputEvent(InputEventType.MouseClick, _tmpMousePos);
                    _blClick = false;
                }
            }
            else if (_blClick && CheckMoved(_oldMousePos, _tmpMousePos))
            {
                _blClick = false;
            }
            else if (_blTmpPressed && !_blClick)
            {
                DispatchInputEvent(InputEventType.MouseDrag, _tmpMousePos - _oldMousePos);
            }
            _blPressed = _blTmpPressed;
            _oldMousePos = _tmpMousePos;
        }

        private static bool CheckMoved(UnityEngine.Vector2 p1, UnityEngine.Vector2 p2)
        {
            return Mathf.Abs(p1.x - p2.x) > Drag_Gap || Mathf.Abs(p1.y - p2.y) > Drag_Gap;
        }
    }
}