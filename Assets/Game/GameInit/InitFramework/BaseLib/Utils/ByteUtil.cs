﻿using System;
using System.Text;

namespace GameInit.Framework
{
    public static class ByteUtil
    {
        public const int BYTE_LEN = 1;
        public const int USHORT_LEN = 2;
        public const int UINT_LEN = 4;
        public const int LONG_LEN = 8;
        public const int FLOAT_LEN = 4;
        public const int INT_LEN = 4;
        public const int SHORT_LEN = 2;
        public const int DOUBLE_LEN = 8;

        public static bool GetBit(uint value, int index)
        {
            return value == (value | (uint)(1 << index));
        }

        public static uint SetBit(uint value, int index, bool boolean)
        {
            if(boolean)
            {
                value = (value | (uint)(1 << index));
            }
            else
            {
                value = (value & ~(uint)(1 << index));
            }
            return value;
        }

        public static bool[] ToBitArray(uint value, int length)
        {
            bool[] temp = new bool[length];
            for(int i= 0; i< length; i++)
            {
                temp[i] = (value & 1) == 1;
                value = value >> 1;
            }
            return temp;
        }

        public static int ReadSignedInt(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[INT_LEN];

            for (int i = 0; i < INT_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += INT_LEN;
            return BitConverter.ToInt32(temp, 0);
        }

        public static sbyte ReadSignedByte(byte[] bytes, ref int startIndex)
        {
            sbyte result;
            byte temp = bytes[startIndex];
            if (temp > 127)
                result = (sbyte)(temp - 256);
            else
                result = (sbyte)temp;
            startIndex++;
            return result;
        }

        public static byte ReadUnsignedByte(byte[] bytes, ref int startIndex)
        {
            byte temp = bytes[startIndex];
            startIndex++;
            return temp;
        }

        public static bool ReadBoolean(byte[] bytes, ref int startIndex)
        {
            byte temp = bytes[startIndex];
            startIndex++;
            return Convert.ToBoolean(temp);
        }

        public static ushort ReadUnsignedShort(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[USHORT_LEN];

            for (int i = 0; i < USHORT_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += USHORT_LEN;
            return BitConverter.ToUInt16(temp, 0);
        }

        public static uint ReadUnsignedInt(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[UINT_LEN];

            for (int i = 0; i < UINT_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += UINT_LEN;
            return BitConverter.ToUInt32(temp, 0);
        }

        public static short ReadSignedShort(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[SHORT_LEN];

            for (int i = 0; i < SHORT_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += SHORT_LEN;
            return BitConverter.ToInt16(temp, 0);
        }

        public static float ReadFloat(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[FLOAT_LEN];

            for (int i = 0; i < FLOAT_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += FLOAT_LEN;
            return BitConverter.ToSingle(temp, 0);
        }

        public static long ReadLong(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[LONG_LEN];

            for (int i = 0; i < LONG_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += LONG_LEN;
            return BitConverter.ToInt64(temp, 0);
        }

        public static ulong ReadUlong(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[LONG_LEN];

            for (int i = 0; i < LONG_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += LONG_LEN;
            return BitConverter.ToUInt64(temp, 0);
        }

        public static double ReadDouble(byte[] bytes, ref int startIndex)
        {
            byte[] temp = new byte[DOUBLE_LEN];

            for (int i = 0; i < DOUBLE_LEN; i++)
            {
                temp[i] = bytes[startIndex + i];
            }
            startIndex += DOUBLE_LEN;
            return BitConverter.ToDouble(temp, 0);
        }

        public static string ReadUTFByte(byte[] bytes, int length, ref int startIndex)
        {
            byte[] temp = new byte[length];
            for (int i = 0; i < length; i++)
            {
                temp[i] = bytes[startIndex + i];
            }

            int removeCount = 0;
            for (int j = temp.Length - 1; j >= 0; j--)
            {
                if (temp[j] == 0)
                {
                    removeCount++;
                }
                else
                {
                    break;
                }
            }
            startIndex += length;
            return Encoding.UTF8.GetString(temp, 0, temp.Length - removeCount);
        }
    }
}