using System;
using UnityEngine;

namespace GameInit.Framework
{
    public static class Logger
    {
        public static bool EnableLog = true;
        private static string blueColor = "55A4FF";
        private static string greenColor = "1AFF30";
        private static string yellowColor = "EFEC1F";
        private static string magentaColor = "F86B97";
        private static string purpleColor = "F620E2";
        
        public static void Log(object info)
        {
            if (EnableLog)
                Debug.Log(info);
        }
        
        public static void LogError(object info)
        {
            Debug.LogError(info);
        }

        /// <summary>
        /// 警告Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogWarning(object info)
        {
            if (EnableLog)
                Debug.LogWarning(info);
        }

        /// <summary>
        /// 蓝色Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogBlue(object info, bool withTimestamp = false)
        {
            Log(blueColor, info, withTimestamp);
        }

        /// <summary>
        /// 绿色Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogGreen(object info, bool withTimestamp = false)
        {
            Log(greenColor, info, withTimestamp);
        }

        /// <summary>
        /// 黄色Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogYellow(object info, bool withTimestamp = false)
        {
            Log(yellowColor, info, withTimestamp);
        }

        /// <summary>
        /// 紫色Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogPurple(object info, bool withTimestamp = false)
        {
            Log(purpleColor, info, withTimestamp);
        }

        /// <summary>
        /// 品红色Log
        /// </summary>
        /// <param name="info"></param>
        public static void LogMagenta(object info, bool withTimestamp = false)
        {
            Log(magentaColor, info, withTimestamp);
        }

        private static void Log(string colorHex, object info, bool withTimestamp = false)
        {
            //Log 只在编辑器下有颜色
    #if UNITY_EDITOR
            info = string.Concat("<color=#", colorHex, ">", info.ToString(), withTimestamp ? " [" + /*Time.time.ToString("0:00")*/DateTime.UtcNow.ToLocalTime().ToString() + "]" : "", "</color>");
    #endif
            
            if (EnableLog)
                Debug.Log(info);
        }
    }
}
