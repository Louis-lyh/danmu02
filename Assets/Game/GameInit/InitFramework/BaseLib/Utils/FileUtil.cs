﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System;

namespace GameInit.Framework
{
    /// <summary>
    /// 文件写入和读取的工具。对创建文件夹，流文件操作时的try/catch进行了分装，通过委托的方式返回结果
    /// </summary>
    public class FileUtil
	{

        /// <summary>
        /// 如果路径尾部没有'/'，添加'/'，否则直接返回
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string FormatPath(string path)
        {
            return path.EndsWith("/") ? path : path + "/";
        }

		public static string GetLocalPersistentDataPath()
		{
			string path = Application.persistentDataPath + "/Documents/";

			try
			{
				if (!Directory.Exists(path))
					Directory.CreateDirectory(path);
			}
			catch (System.Exception ex)
			{
				Logger.LogMagenta("FileUtil.GetLocalPersistentDataPath() => ex:" + ex.Message);
			}
			
			return path;
		}

        /// <summary>
        /// 获取文件的路径
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetPersistentFilePath(string name)
        {
            return GetLocalPersistentDataPath() + name;
        }

        /// <summary>
        /// 检测文件是否存在
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool IsPersistentFileExit(string name)
        {
            return IsLocalFileExit(GetLocalPersistentDataPath() + name);
        }

		/// <summary>
		/// 检测文件是否存在
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static bool IsLocalFileExit(string path)
		{
			return File.Exists(path);
		}

		/// <summary>
		/// 删除本地文件
		/// </summary>
		/// <param name="path">Path.</param>
		public static void DeleteLocalFile(string path)
		{
			File.Delete(path);
		}

        /// <summary>
        /// 删除本地文件
        /// </summary>
        /// <param name="path">Path.</param>
        public static void DeletePersistentFile(string name)
        {
            File.Delete(GetLocalPersistentDataPath() + name);
        }

        /// <summary>
        /// 读取持久数据文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="onRead"></param>
        public static byte[] LoadPersistentFile(string fileName)
        {
            return LoadLocalFile(GetLocalPersistentDataPath() + fileName);
        }

		/// <summary>
		/// 读取本地文件
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static byte[] LoadLocalFile(string path)
		{
			if (IsLocalFileExit(path))
			{
				Stream stream = File.Open(path, FileMode.Open);
				byte[] buffer = new byte[(int)stream.Length];
				stream.Read(buffer, 0, (int)stream.Length);
				stream.Close();
				
				return buffer;
			}
			else
			{
				return null;
			}
			
		}
		
		/// <summary>
		/// 读取本地文件
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="onRead"></param>
		public static bool LoadLocalFile(string path, Action<BinaryReader> onRead)
		{
			bool result = false;
			byte[] buffer = LoadLocalFile(path);
			if (buffer != null)
			{
				MemoryStream ms = null;
				BinaryReader br = null;
				try
				{
					ms = new MemoryStream(buffer);
					br = new BinaryReader(ms);
					
					if (onRead != null) onRead(br);

					result = true;
				}
				catch (System.Exception e)
				{
                    Debug.LogError(e.Message);
				}
				finally
				{
					if (br != null)
					{
						br.Close();
						br = null;
					}
					if (ms != null)
					{
						ms.Close();
						ms = null;
					}
				}
			}

			return result;
		}
		
		/// <summary>
		/// 读取持久数据文件
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="onRead"></param>
        public static bool LoadPersistentFile(string fileName, Action<BinaryReader> onRead)
		{
			return LoadLocalFile(GetLocalPersistentDataPath() + fileName, onRead);
		}
		
        public static void SaveLocalFile(string fullPath, byte[] content)
        {
            string dir = Path.GetDirectoryName(fullPath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            Stream stream = null;
            stream = File.Open(fullPath, FileMode.Create);
            stream.Write(content, 0, content.Length);
            stream.Flush();
            stream.Close();
        }

		/// <summary>
		/// 存储本地文件
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="content"></param>
		public static void SaveLocalFile(string path, string fileName, byte[] content)
        {
            SaveLocalFile(path + fileName, content);
		}

        /// <summary>
        /// 存储本地文件
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveLocalFile(string fullPath, Action<BinaryWriter> onWrite)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);

            byte[] buffer = null;

            try
            {
                if (onWrite != null) onWrite(bw);
                buffer = ms.ToArray();
                SaveLocalFile(fullPath, buffer);
            }
            catch (System.Exception e)
            {
                Debug.LogError(e.Message);
            }
            finally
            {
                bw.Close();
                ms.Close();
                bw = null;
                ms = null;
            }
        }

        /// <summary>
        /// 存储本地文件
        /// </summary>
        /// <param name="fileName"></param>
        public static void SaveLocalFile(string path, string fileName, Action<BinaryWriter> onWrite)
		{
            SaveLocalFile(path + fileName, onWrite);
        }
		
		/// <summary>
		/// 存储持久数据文件
		/// </summary>
		/// <param name="fileName"></param>
		public static void SavePersistentFile(string fileName, byte[] content)
		{
			SaveLocalFile(GetLocalPersistentDataPath(), fileName, content);
		}
		
		/// <summary>
		/// 存储持久数据文件
		/// </summary>
		/// <param name="fileName"></param>
        public static void SavePersistentFile(string fileName, Action<BinaryWriter> onWrite)
		{
			SaveLocalFile(GetLocalPersistentDataPath(), fileName, onWrite);
		}

        /// <summary>
        /// 遍历文件下的所有文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="relative"></param>
        /// <param name="allfiles"></param>
        /// <param name="ignoreExtensions">忽略的文件尾名</param>
        public static void RecursiveDirectory(string path, List<string> allfiles, bool recursive, params string[] ignoreExtensions)
        {
            string[] files = Directory.GetFiles(path);
            string[] dirs = Directory.GetDirectories(path);
            for (int i = 0, max = files.Length; i < max; i++)
            {
                string file = files[i].Replace('\\', '/');
                string ext = Path.GetExtension(file);
                bool findExt = false;
                for (int j = 0, count = ignoreExtensions.Length; j < count; j++)
                {
                    if (ext.EndsWith(ignoreExtensions[j]))
                    {
                        findExt = true;
                        break;
                    }
                }
                if (findExt) continue;
                allfiles.Add(file);
            }
            if ( recursive )
            {
                for ( int i = 0, max = dirs.Length; i < max; i++ )
                {
                    RecursiveDirectory(dirs[i].Replace('\\', '/'), allfiles, recursive, ignoreExtensions);
                }
            }
        }

        /// <summary>
        /// 遍历文件下的所有文件
        /// </summary>
        /// <param name="path"></param>
        /// <param name="relative"></param>
        /// <param name="allfiles"></param>
        /// <param name="ignoreExtensions">只扫描的文件尾名</param>
        public static void RecursiveDirectory2(string path, List<string> allfiles, bool recursive, params string[] keepExtensions)
        {
            string[] files = Directory.GetFiles(path);
            string[] dirs = Directory.GetDirectories(path);
            for (int i = 0, max = files.Length; i < max; i++)
            {
                string file = files[i].Replace('\\', '/');
                string ext = Path.GetExtension(file);
                bool findExt = false;
                for (int j = 0, count = keepExtensions.Length; j < count; j++)
                {
                    if (ext.EndsWith(keepExtensions[j]))
                    {
                        findExt = true;
                        break;
                    }
                }
                if (findExt)
                {
                    allfiles.Add(file);
                }
            }

            if ( recursive )
            {
                for ( int i = 0, max = dirs.Length; i < max; i++ )
                {
                    RecursiveDirectory2(dirs[i].Replace('\\', '/'), allfiles, recursive, keepExtensions);
                }
            }
        }

        public static void SetPathToRelative(string fullPath, List<string> allfiles, bool ignoreExtension)
        {
            StringBuilder sb = new StringBuilder();
            int count = allfiles.Count;
            for (int i = 0; i < count; i++)
            {
                sb.Remove(0, sb.Length);
                sb.Append(allfiles[i]);
                sb.Replace(fullPath, string.Empty);
                if (ignoreExtension)
                {
                    string ext = Path.GetExtension(allfiles[i]);
                    if (!string.IsNullOrEmpty(ext))
                    {
                        sb.Replace(ext, string.Empty);
                    }
                }
                allfiles.Add(sb.ToString());
            }
            allfiles.RemoveRange(0, count);
        }

		/// <summary>
		/// 获取文件大小
		/// </summary>
		/// <param name="fullPath"></param>
		/// <returns></returns>
		public static long GetFileSize(string fullPath)
	        {
	            long lSize = 0;
	            if (File.Exists(fullPath))
	                lSize = new FileInfo(fullPath).Length;
	            return lSize;
	        }
	    }
}

