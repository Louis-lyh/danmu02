﻿using UnityEngine;

namespace GameInit.Framework
{
    public static class GameObjectHelper
    {
        public static void GameObjectSetParent(GameObject child, Transform parent, Vector3 localPos = default)
        {
            if (child == null)
                return;
            child.transform.SetParent(parent, false);
            child.transform.localPosition = localPos;
            child.transform.localScale = Vector3.one;
        }
    }
}