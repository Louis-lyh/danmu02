﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    public class GameLogTool : MonoBehaviour
    {
        public bool m_enableLog { get; set; }
        private Dictionary<string, LogInfoData> _dictLogDatas;
        private bool _blIsInited = false;
        private bool _isShowContent = false;
        private GUIStyle guiStyle;
        private GUIStyle guiStyleText;

        protected void Start()
        {
            if (_blIsInited)
                return;
            _blIsInited = true;
            //m_enableLog = true;
            Logger.EnableLog = true;
            _dictLogDatas = new Dictionary<string, LogInfoData>();
            Application.logMessageReceived += OnLogAdded;
            _timeleft = _updateInterval;
        }

        private void OnLogAdded(string logText, string stackTrace, LogType type)
        {
            if (_dictLogDatas.Count > 300)
                _dictLogDatas.Clear();
            string content = "{0}:{1}";
            if (type == LogType.Error)
                content = string.Format(content, type, logText + "\n" + stackTrace);
            else
                content = string.Format(content, type, logText);

            if (_dictLogDatas.ContainsKey(content))
                _dictLogDatas[content].m_logCount += 1;
            else
                _dictLogDatas.Add(content, new LogInfoData(content, type));
        }

        void OnGUI()
        {
            if (guiStyle == null)
            {
                guiStyle = GUI.skin.button;
                guiStyleText = GUI.skin.label;
                guiStyle.fontSize = 20;
                guiStyleText.fontSize = 20;
            }

            Start();
            if (_isShowContent)
                ShowLogContent();
            if (GUI.Button(new Rect(5, 150, 150, 60), _fps, guiStyle))
                _isShowContent = !_isShowContent;
        }

        private UnityEngine.Vector2 _scrollPos;

        void ShowLogContent()
        {
            GUILayout.BeginArea(new Rect(0, 210, Screen.width * 0.95f, Screen.height * 0.8f));
            GUILayout.Box(
                "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
            GUILayout.EndArea();

            GUI.skin.verticalScrollbar.fixedWidth = 30;
            GUI.skin.verticalScrollbarThumb.fixedWidth = 30;
            GUI.skin.horizontalScrollbar.fixedHeight = 30;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = 30;
            GUILayout.BeginArea(new Rect(0, 210, Screen.width * 0.95f, Screen.height * 0.8f));
            _scrollPos = GUILayout.BeginScrollView(_scrollPos, GUILayout.Width(Screen.width * 0.95f),
                GUILayout.Height(Screen.height * 0.8f));
            Dictionary<string, LogInfoData>.Enumerator rator = _dictLogDatas.GetEnumerator();
            var sb = new StringBuilder();
            while (rator.MoveNext())
            {
                switch (rator.Current.Value.m_logType)
                {
                    case LogType.Log:
                        GUI.contentColor = Color.white;
                        break;
                    case LogType.Warning:
                        GUI.contentColor = Color.yellow;
                        break;
                    case LogType.Error:
                    case LogType.Exception:
                        GUI.contentColor = Color.red;
                        var logStr = rator.Current.Key + "------" + rator.Current.Value.m_logText;
                        GUILayout.Label(logStr, guiStyleText);
                        sb.Append(logStr);
                        break;
                    default:
                        GUI.contentColor = Color.white;
                        break;
                }

                var str = rator.Current.Key + " --- Count:" + rator.Current.Value.m_logCount;
                GUILayout.Label(str, guiStyleText);
                sb.Append(str);
            }


            if (GUILayout.Button("Copy", GUILayout.Height(50)))
                GUIUtility.systemCopyBuffer = sb.ToString();

            if (GUILayout.Button("Clear", GUILayout.Height(50)))
                _dictLogDatas.Clear();
            GUILayout.EndScrollView();
            GUILayout.EndArea();
            GUI.skin.verticalScrollbar.fixedWidth = 0;
            GUI.skin.verticalScrollbarThumb.fixedWidth = 0;
            GUI.skin.horizontalScrollbar.fixedHeight = 0;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = 0;
        }

        #region FPS

        private float _updateInterval = 0.5f;
        private float _accum = 0.0f;
        private float _frames = 0f;
        private float _timeleft;
        private string _fps = "LogTool";

        // protected void Update()
        // {
        //     _timeleft -= Time.deltaTime;
        //     _accum += Time.timeScale / Time.deltaTime;
        //     ++_frames;
        //     if (_timeleft <= 0.0f)
        //     {
        //         _fps = "FPS:" + (_accum / _frames).ToString("F2");
        //         _timeleft = _updateInterval;
        //         _accum = 0.0f;
        //         _frames = 0f;
        //     }
        // }

        #endregion

        public class LogInfoData
        {
            public LogType m_logType { private set; get; }
            public string m_logText { private set; get; }
            public int m_logCount { set; get; }

            public LogInfoData(string value, LogType type)
            {
                m_logType = type;
                m_logText = value;
            }
        }
    }
}