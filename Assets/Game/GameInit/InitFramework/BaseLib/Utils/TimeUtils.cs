﻿using System;

namespace GameInit.Framework
{
    public static class TimeUtils
    {
        /// <summary>获取秒级别时间戳（10位）</summary>
        public static long GetTimestampToSeconds()
        {
            var ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }
    }
}