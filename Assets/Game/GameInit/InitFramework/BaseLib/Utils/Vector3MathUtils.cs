using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    public static class Vector3MathUtils
    {
        public const float EPSILON = 0.00001f;

        public static bool IsZero(float v, float e = EPSILON)
        {
            return Mathf.Abs(v) < EPSILON;
        }

        public static Vector3 Vector3ZeroY(Vector3 v)
        {
            return new Vector3(v.x, 0, v.z);
        }

        public static Vector3 GetDirection2D(Vector3 to, Vector3 from)
        {
            Vector3 dir = to - from;
            dir.y = 0;
            return dir.normalized;
        }

        public static float GetDistance2D(Vector3 to, Vector3 from)
        {
            Vector3 dir = to - from;
            dir.y = 0;
            return dir.magnitude;
        }

        public static Vector3 GetEulerAngles(Vector3 startPos, Vector3 targetPos)
        {
            var toTarget = targetPos - startPos;
            toTarget.Normalize();
            return Quaternion.LookRotation(toTarget).eulerAngles;
        }

        /// <summary>
        /// 判断点是否在矩形区域内
        /// </summary>
        /// <param name="startPos"></param>
        /// <param name="rotation"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        public static bool IsInRect(Vector3 startPos, Quaternion rotation, Vector3 pos, float radius = 1f)
        {
            var leftPos = startPos + rotation * Vector3.left * radius * .5f;
            var rightPos = startPos + rotation * Vector3.right * radius * 0.5f;
            var leftEndPos = leftPos + rotation * Vector3.forward * radius;
            var rightEndPos = rightPos + rotation * Vector3.forward * radius;

            return CalPoint(pos, leftEndPos, rightEndPos, rightPos, leftPos);
        }

        private static float Multiply(float p1x, float p1y, float p2x, float p2y, float p0x, float p0y)
        {
            return ((p1x - p0x) * (p2y - p0y) - (p2x - p0x) * (p1y - p0y));
        }

        private static bool CalPoint(Vector3 point, Vector3 v0, Vector3 v1, Vector3 v2, Vector3 v3)
        {
            var x = point.x;
            var y = point.z;

            var v0x = v0.x;
            var v0y = v0.z;

            var v1x = v1.x;
            var v1y = v1.z;

            var v2x = v2.x;
            var v2y = v2.z;

            var v3x = v3.x;
            var v3y = v3.z;

            return Multiply(x, y, v0x, v0y, v1x, v1y) * Multiply(x, y, v3x, v3y, v2x, v2y) <= 0
                   && Multiply(x, y, v3x, v3y, v0x, v0y) * Multiply(x, y, v2x, v2y, v1x, v1y) <= 0;
        }

        public static bool IsInPoints(Vector3 target, List<Vector3> points)
        {
            if (points.Count < 3)
            {
                return false;
            }

            var p1 = points[0];
            for (var i = 2; i < points.Count; i++)
            {
                var p2 = points[i - 1];
                var p3 = points[i];
                if (IsInTriangle(p1.x, p1.z, p2.x, p2.z, p3.x, p3.z, target.x, target.z))
                {
                    return true;
                }
            }

            return false;
        }

        // https://www.cnblogs.com/xiaobaizzz/p/12231131.html
        private static double CrossProduct(double x1, double y1, double x2, double y2)
        {
            return x1 * y2 - x2 * y1;
        }

        private static bool IsInTriangle(double x1, double y1, double x2, double y2,
            double x3, double y3, double x, double y)
        {
            //注意输入点的顺序不一定是逆时针，需要判断一下
            if (CrossProduct(x3 - x1, y3 - y1, x2 - x1, y2 - y1) >= 0)
            {
                var tx = x2;
                var ty = y2;
                x2 = x3;
                y2 = y3;
                x3 = tx;
                y3 = ty;
            }

            if (CrossProduct(x2 - x1, y2 - y1, x - x1, y - y1) < 0) return false;
            if (CrossProduct(x3 - x2, y3 - y2, x - x2, y - y2) < 0) return false;
            if (CrossProduct(x1 - x3, y1 - y3, x - x3, y - y3) < 0) return false;
            return true;
        }

        public static List<Vector3> GetRectPoints(Quaternion rot, float width, float height)
        {
            var deg = rot.eulerAngles.y;
            deg = (float) (deg / 180.0f * Math.PI);
            var sinA = Mathf.Sin(deg);
            var cosA = Mathf.Cos(deg);
            var leftBot = new Vector3(-width / 2 * cosA, 0, width * sinA / 2);
            var rightBot = new Vector3(width / 2 * cosA, 0, -width * sinA / 2);
            var leftTop = new Vector3(height * sinA - width * cosA / 2, 0, height * cosA + width * sinA / 2);
            var rightTop = new Vector3(height * sinA + width * cosA / 2, 0, height * cosA - width * sinA / 2);
            return new List<Vector3>() {leftBot, rightBot, rightTop, leftTop};
        }

        public static List<Vector3> GetSectorPoints(Quaternion rot, Vector3 pos, float range, float degree)
        {
            var list = new List<Vector3>();
            list.Add(pos);
            // 逆时针
            var de = 90 - rot.eulerAngles.y;
            var downRad = (de - degree / 2) / 180 * Mathf.PI;
            var down = range * new Vector3(Mathf.Cos(downRad), 0, Mathf.Sin(downRad));
            list.Add(pos + down);
            var midRad = de / 180 * Mathf.PI;
            var mid = range * new Vector3(Mathf.Cos(midRad), 0, Mathf.Sin(midRad));
            list.Add(pos + mid);
            var upRad = (de + degree / 2) / 180 * Mathf.PI;
            var up = range * new Vector3(Mathf.Cos(upRad), 0, Mathf.Sin(upRad));
            list.Add(pos + up);
            return list;
        }
    }
}