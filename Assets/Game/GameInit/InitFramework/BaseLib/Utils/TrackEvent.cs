//
// // SDK，导入宏定义, 导入了对应SDK，开启对应宏定义
// #define THINKINGANALYTICS_DEFINE
// #define APPSFLYER_DEFINE
// #define FIREBASE_DEFINE
//
// using System.Collections.Generic;
// using Cysharp.Text;
// using GameInit.Framework;
// using Kunggame.SDK.Appsflyer;
// using Kunggame.SDK.Firebase;
// using KungGame.SDK.ThinkingAnalytics;
// //using Newtonsoft.Json;
//
// namespace GameInit.Framework
// {
//     public enum TrackPlatform
//     {
//         ThinkingAnalytics,
//         Appsflyer,
//         Firebase
//     }
//     
//     public class TrackEvent
//     {
//         private static TrackEvent _instance = null;
//         private static Dictionary<string, object> _keyValuePairs = new Dictionary<string, object>();
//         private static string _keySend;
//
//         public static TrackEvent I => _instance ??= new TrackEvent();
//
//         // 开始计时
//         public void TimeEvent(string key)
//         {
// #if THINKINGANALYTICS_DEFINE
//         ThinkingAnalyticsSDK.TimeEvent(key);
// #endif
//         }
//
//         public TrackEvent Track(string key = "")key
//         {
//             _keyValuePairs.Clear();
//             _keySend = key;
//             return this;
//         }
//
//         public TrackEvent Add(string key, string value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, double? value)
//         {
//             if (value == null) value = 0;
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, long value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, int value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, float value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, bool value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         public TrackEvent Add(string key, object value)
//         {
//             _keyValuePairs[key] = value;
//             return this;
//         }
//
//         // 备注: normalPlatform 参数本身可以添加默认参数，但为了提醒使用数据上报时，必须清楚要上报到哪些平台，所以不添加默认参数 
//         public void Send(TrackPlatform normalPlatform, params TrackPlatform[] args)
//         {
//             var platforms = new List<TrackPlatform> {normalPlatform};
//             platforms.AddRange(args);
// // #if !KG_DEBUG
// //             foreach (var platform in platforms)
// //             {
// //             #if THINKINGANALYTICS_DEFINE
// //                 if (platform == TrackPlatform.ThinkingAnalytics)
// //                     ThinkingAnalyticsSDK.Track(_keySend, _keyValuePairs);
// //             #endif
// //                 
// //             #if APPSFLYER_DEFINE
// //                 if (platform == TrackPlatform.Appsflyer)
// //                     AppsflyerSDK.SendEvent(_keySend, _keyValuePairs);
// //             #endif
// //                 
// //             #if FIREBASE_DEFINE
// //                 if (platform == TrackPlatform.Firebase)
// //                     FirebaseSDK.LogEvent(_keySend, _keyValuePairs);
// //             #endif
// //             }
// // #endif
// #if UNITY_EDITOR
//             string str = "";
//             foreach (var item in _keyValuePairs)
//             {
//                 string s = ZString.Concat(item.Key, "=", item.Value.ToString(), " ");
//                 str += s;
//             }
//             Logger.LogYellow($"事件名-{_keySend}\n事件参数{str}");
//             //Logger.LogYellow($"事件名-{_keySend}\n事件参数{JsonConvert.SerializeObject(_keyValuePairs)}");
// #elif !KG_DEBUG
//             foreach (var platform in platforms)
//             {
//             #if THINKINGANALYTICS_DEFINE
//                 if (platform == TrackPlatform.ThinkingAnalytics)
//                     ThinkingAnalyticsSDK.Track(_keySend, _keyValuePairs);
//             #endif
//                 
//             #if APPSFLYER_DEFINE
//                 if (platform == TrackPlatform.Appsflyer)
//                     AppsflyerSDK.SendEvent(_keySend, _keyValuePairs);
//             #endif
//                 
//             #if FIREBASE_DEFINE
//                 if (platform == TrackPlatform.Firebase)
//                     FirebaseSDK.LogEvent(_keySend, _keyValuePairs);
//             #endif
//             }
// #endif
//         }
//
//         // 对于一般的用户属性，您可以调用 UserSet 来进行设置，使用该接口上传的属性将会覆盖原有的属性值，如果之前不存在该用户属性，则会新建该用户属性。
//         public void UserSet(TrackPlatform normalPlatform, params TrackPlatform[] args)
//         {
//             var platforms = new List<TrackPlatform> {normalPlatform};
//             platforms.AddRange(args);
//             
//             foreach (var platform in platforms)
//             {
//                 // ThinkingAnalyticsse
//             #if THINKINGANALYTICS_DEFINE
//                 if (platform == TrackPlatform.ThinkingAnalytics)
//                     ThinkingAnalyticsSDK.UserSet(_keyValuePairs);
//             #endif
//                 // Firebase
//             #if FIREBASE_DEFINE
//                 if(platform == TrackPlatform.Firebase)
//                     foreach (var pair in _keyValuePairs)
//                         FirebaseSDK.SetUserProperty(pair.Key, pair.Value.ToString());
//             #endif
//             }
//         }
//
//         // 如果您要上传的用户属性只要设置一次，则可以调用 UserSetOnce 来进行设置，当该属性之前已经有值的时候，将会忽略这条信息
//         public void UserSetOnce()
//         {
//             ThinkingAnalyticsSDK.UserSetOnce(_keyValuePairs);
//         }
//
//         // 当您要上传数值型的属性时，您可以调用 UserAdd 来对该属性进行累加操作，如果该属性还未被设置，则会赋值 0 后再进行计算，可传入负值，等同于相减操作
//         public void UserAdd()
//         {
//             ThinkingAnalyticsSDK.UserAdd(_keyValuePairs);
//         }
//     }
// }
