using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameInit.Framework
{
    public class UITouchListener : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
    {
        public Action<PointerEventData> OnTouchDown;
        public Action<PointerEventData> OnTouchUp;
        public Action<PointerEventData> OnClick;
        public Action<Vector3> OnTouchBegin;
        public Action<Vector3> OnTouchMove;
        public Action<Vector3> OnTouchEnd;
        public Action<Vector3> OnTouchCancel;

        private RectTransform _rect;
        private bool _touchBegan;
        private Camera _uiCamera;

        private void Start()
        {
            if (OnTouchMove != null)
            {
                // _uiCamera = GameObject.Find("UIRoot/UICamera").GetComponent<Camera>();
                // _rect = gameObject.GetComponent<RectTransform>();
            }
            _uiCamera = GameObject.Find("UIRoot/UICamera").GetComponent<Camera>();
            _rect = gameObject.GetComponent<RectTransform>();
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            OnTouchDown?.Invoke(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            OnTouchUp?.Invoke(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnClick?.Invoke(eventData);
        }

        private void Update()
        {
            if (!_rect)
            {
                return;
            }

            if (Input.GetMouseButtonDown(0) && TouchInside())
            {
                _touchBegan = true;
                OnTouchBegin?.Invoke(Input.mousePosition);
            }

            if (!_touchBegan)
            {
                return;
            }

            if (Input.GetMouseButton(0))
            {
                OnTouchMove?.Invoke(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (TouchInside())
                {
                    OnTouchEnd?.Invoke(Input.mousePosition);
                }
                else
                {
                    OnTouchCancel?.Invoke(Input.mousePosition);
                }

                _touchBegan = false;
            }
        }

        private bool TouchInside()
        {
            if (!_uiCamera)
            {
                return false;
            }

            var pos = Input.mousePosition;
            var posCenter = _uiCamera.WorldToScreenPoint(_rect.position);
            var size = _rect.sizeDelta * Screen.width / 768.0f;
            return Math.Abs(pos.x - posCenter.x) <= size.x / 2 &&
                   Math.Abs(pos.y - posCenter.y) <= size.y / 2;
        }
    }
}