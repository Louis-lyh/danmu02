using System;
using UnityEngine;
using System.Collections.Generic;
using LitJson;

namespace GameInit.Framework
{
    
    public class UserGroup : Singleton<UserGroup>
    {
        private const string USER_GROUP_LABEL = "KG_USER_GROUP_LABEL";
    
        // 用于存储用户标签
        private Dictionary<string, string> _userLabels = null;
        // 标签发生改变的回调
        private Action<string, string> _onChangeLabelCallFunc = null;

        // 初始化
        public static void Initialize()
        {
            Instance.Load();
            Instance.LoadDefaultUserGroupData();
            Instance.CreateNormalLabel();
        }

        // ---------------------- public  ----------------------
        /// <summary>
        /// add label || modify label 
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <param name="callfunc">callfunc, track property</param>
        public void AddLabel(string key, string value, bool isTrack = true)
        {
            if (this._userLabels == null)
                this._userLabels = new Dictionary<string, string>();

            // 添加内容
            if (this._userLabels.ContainsKey(key))
                this._userLabels[key] = value;
            else
                this._userLabels.Add(key, value);

            if (isTrack)
                this._onChangeLabelCallFunc?.Invoke(key, value);

            //this.Save();
        }

        /// <summary>
        /// add label, once label, no change
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <param name="callfunc">callfunc, track property</param>
        public void AddOnceLabel(string key, string value, bool isTrack = true)
        {
            if (this._userLabels == null)
                this._userLabels = new Dictionary<string, string>();

            // 标签不存在是，才添加内容
            if (!this._userLabels.ContainsKey(key))
            {
                this._userLabels.Add(key, value);
                
                if (isTrack)
                    this._onChangeLabelCallFunc?.Invoke(key, value);
            }
            this.Save();
        }
    
        // 获得用户标签
        private string GetLabel(string key)
        {
            if (this._userLabels != null && this._userLabels.ContainsKey(key))
                return this._userLabels[key];
            return "";
        }
    
        // 设置标签改变时的回调
        public void SetChangeLabelListener(Action<string, string> onChangeLabelCallFunc)
        {
            this._onChangeLabelCallFunc = onChangeLabelCallFunc;
        }

        private UserGroupStruct Match(List<UserGroupStruct> filter)
        {
            if (this._userLabels == null || this._userLabels.Count == 0)
                return null;

            var result = new List<UserGroupStruct>();
            
            foreach (var it in filter)
            {
                var labels = ToDic(it.Labels);

                // 完全匹配
                var isMatch = true;
                foreach (var label in labels)
                {
                    // 没有key
                    if (!this._userLabels.ContainsKey(label.Key))
                        isMatch = false;
                    // key存在，值不相等
                    else if (this._userLabels.ContainsKey(label.Key) && !this._userLabels[label.Key].Equals(label.Value))
                        isMatch = false;
                    // 不匹配退出
                    if (!isMatch)
                        break;
                }

                if (isMatch)
                    result.Add(it);
            }
            // 按权重排序
            result.Sort((a, b)=> -1 * a.Weight.CompareTo(b.Weight));
            // 返回权重最高
            return result.Count == 0 ? null : result[0];
        }

        public bool SelfNeedCheckDownload()
        {
            var selfUserGroup = Match(_userGroups);
            if (selfUserGroup == null)
                return false;
            int.TryParse(GetLabel("Percent"), out var percent);
            return percent <= selfUserGroup.Percent;
        }

        public string SelfMatchLevel()
        {
            var selfUserGroup = Match(_userGroups);
            if (selfUserGroup != null)
                _userGroup = selfUserGroup;

            return _userGroup == null ? "0" : _userGroup.Level.ToString();
        }

        // 默认标签
        private void CreateNormalLabel()
        {
            // 国家地区
            AddOnceLabel("Country", "CN");
            // 导量渠道
            AddOnceLabel("SourceMedia", "Facebook");
            // 导量用户类型(AEO, VO, 2.5-purchase, 3.0)
            AddOnceLabel("SourceUserType", "AEO");
            
            var percent = (int)(DateTimeToTimeStamp(DateTime.UtcNow) % 100);
            AddOnceLabel("Percent", percent.ToString());
        }
        
        //DateTime转换为Unix时间戳
        private static long DateTimeToTimeStamp(System.DateTime time) 
        {
            var dateTime = TimeZoneInfo.ConvertTimeToUtc(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            var date = (time.Ticks - dateTime.Ticks) / 10000;   //除10000调整为13位      
            return date;
        }

        // ---------------------- private ----------------------
        // 加载本地标签
        private void Load()
        {
            if (PlayerPrefs.HasKey(USER_GROUP_LABEL))
            {
                var data = PlayerPrefs.GetString(USER_GROUP_LABEL);
                Logger.Log("UserGroup Value:" + data);
                _userLabels = JsonMapper.ToObject<Dictionary<string, string>>(data);
            }
            else
            {
                _userLabels = new Dictionary<string, string>();
            }
        }

        // 保存
        private void Save()
        {
            var data = JsonMapper.ToJson(_userLabels);
            PlayerPrefs.SetString(USER_GROUP_LABEL, data);
        }

        // toDic
        private Dictionary<string, string> ToDic(string str, char split = ',', char kvSplit = '|')
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            var tmpArray = str.Split(split);
            foreach (var item in tmpArray)
            {
                var temp = item.Split(kvSplit);

                if (temp.Length == 2)
                    dic[temp[0]] = temp[1];
            }

            return dic;
        }

        #region UserGroup

        private List<UserGroupStruct> _userGroups;
        private UserGroupStruct _userGroup;
        private void LoadDefaultUserGroupData()
        {
            var defaultConfig = Resources.Load<TextAsset>("GatewayConfig");
            _userGroups = new List<UserGroupStruct>();
            SetUserGroupData(defaultConfig.text);
        }

        public void SetUserGroupData(string value)
        {           
            var json = JsonMapper.ToObject(value);
            _userGroups.Clear();
            for (var i = 0; i < json.Count; i++)
            {
                var userGroup = JsonMapper.ToObject<UserGroupStruct>(json[i].ToJson());
                _userGroups.Add(userGroup);
            }

            _userGroup = Match(_userGroups);
        }

        #endregion
    }

    public class UserGroupStruct
    {
        public int ID;
        public string Labels;
        public string Weight;
        public int Level;
        public int Percent;
    }
}