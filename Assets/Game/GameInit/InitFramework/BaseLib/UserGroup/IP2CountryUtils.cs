﻿// using System;
// using System.Net;
// using System.Net.Security;
// using System.Security.Cryptography.X509Certificates;
// using System.Text;
// using Cysharp.Threading.Tasks;
// using Kunggame.SDK.Common;
// using LitJson;
// using UnityEngine;
// using UnityEngine.Networking;
// using Object = System.Object;
//
//
// namespace GameInit.Framework
// {
//     public class IP2CountryUtils : Singleton<IP2CountryUtils>
//     {
//         private const string COUNTRY_KEY = "CountryKey";
//         private const string COUNTRY_ID_KEY = "CountryIdKey";
//         
//         private const string IP2CountryUrl = "https://pro.ip-api.com/json/?key=whs0KAP0Hbdbv6Q";
//         // {
//         //     "query": "24.48.0.1",
//         //     "status": "success",
//         //     "country": "Canada",
//         //     "countryCode": "CA",
//         //     "region": "QC",
//         //     "regionName": "Quebec",
//         //     "city": "Montreal",
//         //     "zip": "H1K",
//         //     "lat": 45.6085,
//         //     "lon": -73.5493,
//         //     "timezone": "America/Toronto",
//         //     "isp": "Le Groupe Videotron Ltee",
//         //     "org": "Videotron Ltee",
//         //     "as": "AS5769 Videotron Telecom Ltee"
//         // }
//
//         private string _country;
//         private string _countryId;
//
//         public void StartQuery()
//         {
//             _country = PlayerPrefs.GetString(COUNTRY_KEY, "");
//             _countryId = PlayerPrefs.GetString(COUNTRY_ID_KEY, "");
//             if (_countryId.IsValid())
//                 return;
//             SendQueryCountry();
//         }
//
//         private async void SendQueryCountry()
//         {
//             var data = await Get(IP2CountryUrl);
//             if (data.IsValid())
//             {
//                 var result = JsonMapper.ToObject(data);
//                 var status = result["status"].ToJson();
//                 if (status == "\"success\"")
//                 {
//                     _countryId = result["countryCode"].ToJson();
//                     _country = result["country"].ToJson();
//                     Logger.LogMagenta("country_id:" + _country + ", country:" + _countryId);
//                 }
//             }
//             else
//             {
//                 _countryId = KGUtils.GetCountryCode();
//                 _country = "";
//                 Logger.LogMagenta("IP2CountryUtils.StartQuery() => 查询失败,走自定义接口");
//             }
//
//             PlayerPrefs.SetString(COUNTRY_KEY, _country);
//             PlayerPrefs.SetString(COUNTRY_ID_KEY, _countryId);
//             PlayerPrefs.Save();
//             UserGroup.Instance.AddOnceLabel("Country", _countryId);
//         }
//         
//         private static async UniTask<string> Get(string url)
//         {
//             if (url.StartsWith("https"))
//                 ServicePointManager.ServerCertificateValidationCallback = RemoteCertificateValidationCallback;
//             var request = UnityWebRequest.Get(url);
//             request.timeout = 5000;
//             string result = null;
//             try
//             {
//                 var async = await request.SendWebRequest();
//                 if (async.result == UnityWebRequest.Result.Success)
//                 {
//                     //成功
//                     result = Encoding.UTF8.GetString(async.downloadHandler.data);
//                 }
//                 else
//                 {
//                     Logger.LogError("HttpManager.Get() => Get请求失败,error:" + async.error);
//                 }
//             }
//             catch (Exception e)
//             {
//                 Logger.LogError("HttpManager.Get() => Get请求出错,Error:" + e.Message);
//             }
//             request.Dispose();
//             return result;
//         }
//         
//         private static bool RemoteCertificateValidationCallback(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
//         {
//             var isOk = true;
//             // If there are errors in the certificate chain,
//             // look at each error to determine the cause.
//             if (sslPolicyErrors != SslPolicyErrors.None)
//             {
//                 for (int i=0; i<chain.ChainStatus.Length; i++)
//                 {
//                     if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
//                     {
//                         continue;
//                     }
//                     chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
//                     chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
//                     chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan (0, 1, 0);
//                     chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
//                     var chainIsValid = chain.Build ((X509Certificate2)certificate);
//                     if (!chainIsValid)
//                     {
//                         isOk = false;
//                         break;
//                     }
//                 }
//             }
//             return isOk;
//         }
//     }
// }