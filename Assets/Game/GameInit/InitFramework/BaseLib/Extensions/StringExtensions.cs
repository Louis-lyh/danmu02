﻿using System;
using System.Collections.Generic;

namespace GameInit.Framework
{
    public static class StringExtensions
    {
        /// <summary>
        /// 在url的尾部加上时间戳
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string UrlFormat(this string url)
        {
            string t = DateTime.Now.Ticks.ToString();
            if (url.Contains("?"))
            {
                return url + "&t=" + t;
            }
            else
            {
                return url + "?t=" + t;
            }
        }
        
        public static List<int> ToListInt(this string self, char splitChar = '|')
        {
            if (!self.IsValid())
                return null;
            var result = new List<int>();
            var temp = self.Split(splitChar);
            foreach (var v in temp)
            {
                int.TryParse(v, out var value);
                result.Add(value);
            }
            return result;
        }
        
        public static List<float> ToListFloat(this string self, char splitChar = '|')
        {
            if (!self.IsValid())
                return null;
            var result = new List<float>();
            var temp = self.Split(splitChar);
            foreach (var v in temp)
            {
                float.TryParse(v, out var value);
                result.Add(value);
            }
            return result;
        }

        public static List<string> ToList(this string self, char splitChar = '|')
        {
            var result = new List<string>();
            if (!self.IsValid())
            {
                result.Add(self);
            }
            else
            {
                var temp = self.Split(splitChar);
                foreach (var str in temp)
                    result.Add(str);
            }
            return result;
        }

        public static bool IsValid(this string self)
        {
            return !string.IsNullOrEmpty(self) && self != "0";
        }
    }
}