﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace GameInit.Framework
{
    internal class UnityWebRequestProcess : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public static class UnityWebRequestExtensionsExtensions
    {
        private static readonly UnityWebRequestProcess _process;

        static UnityWebRequestExtensionsExtensions()
        {
            var go = new GameObject() { name = "UnityWebRequest" };
            _process = go.AddComponent<UnityWebRequestProcess>();
        }

        /// <summary>
        /// 设置连接的监听
        /// </summary>
        /// <param name="UnityWebRequest">request.</param>
        /// <param name="onSuccess">On success.</param>
        /// <param name="onFailure">On failure.</param>
        public static void SendWebRequest(this UnityWebRequest request, Action onSuccess, Action<string> onFailure, int timeout = 10)
        {
            _process.StartCoroutine(CorSendWebRequest(request, onSuccess, onFailure, timeout));
        }

        private static IEnumerator CorSendWebRequest(UnityWebRequest request, Action onSuccess, Action<string> onFailure, int timeout)
        {
            //超时时间暂时设置为
            request.timeout = timeout;
            yield return request.SendWebRequest();
            if (string.IsNullOrEmpty(request.error))
                onSuccess?.Invoke();
            else
                onFailure?.Invoke(request.error);
        }
    }
}