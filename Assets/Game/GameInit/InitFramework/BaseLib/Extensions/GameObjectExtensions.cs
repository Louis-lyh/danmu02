﻿using UnityEngine;

namespace GameInit.Framework
{
    public static class GameObjectExtensions
    {
        public static T GetComponent<T>(this Transform self, bool ifNotAdd) where T : Component
        {
            return self == null ? null : self.gameObject.GetComponent<T>(ifNotAdd);
        }
        
        public static T GetComponent<T>(this GameObject self, bool ifNotAdd) where T : Component
        {
            var result = self.GetComponent<T>();
            if (result != null)
                return result;
            if (ifNotAdd)
                result = self.AddComponent<T>();
            return result;
        }

        public static T Find<T>(this Transform self, string path) where T : Component
        {
            var child = self.Find(path);
            return child == null ? null : child.GetComponent<T>();
        }

        public static RectTransform FindRectTransform(this Transform self, string path)
        {
            var child = self.Find(path);
            return child == null ? null : child as RectTransform;
        }
    }
}