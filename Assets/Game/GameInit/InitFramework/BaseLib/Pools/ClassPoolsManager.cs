﻿using System.Collections.Generic;

namespace GameInit.Framework
{
    public static class ClassPoolsManager<T>where T : IClassPool, new()
    {
        private static readonly List<ClassPools<T>> list = new List<ClassPools<T>>();
        public static T Pop()
        {
            var pool = GetPool();
            if (pool != null)
                return pool.Pop();
            pool = new ClassPools<T>();
            list.Add(pool);
            return pool.Pop();
        }
        
        public static void Push(T msg)
        {
            var pool = GetPool();
            pool?.Push(msg);
        }
        
        private static ClassPools<T> GetPool()
        {
            for (int i = 0, count = list.Count; i < count; i++)
            {
                var item = list[i];
                if(typeof(T) == item.DefaultType)
                {
                    return item;
                }
            }

            return null;
        }
    }

}
