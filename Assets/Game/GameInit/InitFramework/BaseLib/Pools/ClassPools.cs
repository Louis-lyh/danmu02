﻿using System;
using System.Collections.Generic;

namespace GameInit.Framework
{
    internal class ClassPools<T> where T : IClassPool, new()
    {
        public Type DefaultType { get; } = typeof(T);
        private readonly List<T> list = new List<T>();
        public T Pop()
        {
            if (list.Count == 0)
                return new T();
            var result = list[0];
            list.Remove(result);
            return result;
        }

        public void Push(T t)
        {
            if (!list.Contains(t))
            {
                t.Reset();
                list.Add(t);
            }
            else
            {
                Logger.LogMagenta("重复放入对象池：" + t.GetType().Name);
            }
        }
    }
}
