using UnityEngine;

namespace GameInit.Framework
{
    public class SetUpConfig : MonoBehaviour
    {
        [Tooltip("是否为调试模式")]
        public bool OpenDebug = true;
        [Tooltip("热更方式，默认为System")]
        public LoadDllType HotFixType = LoadDllType.System;
        [Tooltip("资源加载是否为AB模式")]
        public bool UseAssetBundle;
        [Tooltip("启用远程AssetBundle,开启热更下载")]
        public bool UseRemoteAssetBundle;
        [Tooltip("资源服务器地址")]
        public string GameAssetRemoteUrl = "http://172.31.128.128/ftp/hot/zob";
        [Tooltip("是否允许在编辑器上使用AB模式时不使用AB加载")]
        public bool AllowTestAB;

        //启动时间
        private System.DateTime _upTime;
        public System.DateTime UpTime => _upTime;
        
        public static SetUpConfig Instance { get; private set; }

        private void Awake()
        {
            //设置语言环境为英语
            System.Globalization.CultureInfo.DefaultThreadCurrentCulture =
                new System.Globalization.CultureInfo("en-US");

            //记录启动时间
            _upTime = System.DateTime.UtcNow;

            Instance = this;
#if !UNITY_EDITOR
            OpenDebug = false;
            HotFixType = LoadDllType.ILRuntime;
            UseAssetBundle = true;
            AllowTestAB = false;
            // UseRemoteAssetBundle = true;
            // GameAssetRemoteUrl = "https://kunggame.oss-accelerate.aliyuncs.com/circlematch/config";
#endif
            Logger.EnableLog = OpenDebug;
            Application.targetFrameRate = 90;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            // if (OpenDebug)
            //     gameObject.AddComponent<GameLogTool>();
            //gameObject.AddComponent<GameLogTool>();
            DontDestroyOnLoad(gameObject);
            // UserGroup.Initialize();
            ModuleManager.Instance.Init();
            ModuleManager.Instance.InsertModule(new SetupHotfixModule());
            ModuleManager.Instance.InsertModule(new AssetBundleUpdateModule());
            // IP2CountryUtils.Instance.StartQuery();
        }
        
        protected void Start()
        {
            ModuleManager.Instance.Run();
        }

        protected void Update()
        {
            TimerHeap.Tick();
            FrameTimerHeap.Tick();
        }
    }
}