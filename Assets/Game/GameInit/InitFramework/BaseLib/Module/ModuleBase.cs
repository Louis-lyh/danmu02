
using System;

namespace GameInit.Framework
{
    public abstract class ModuleBase : IDisposable
    {
        public void Enter()
        {
            OnEnter();
        }

        internal void Exit()
        {
            OnExit();
            ModuleManager.Instance.ModuleEnd(this);
        }

        protected virtual void OnExit()
        {
            
        }
        protected abstract void OnEnter();

        public virtual void Dispose()
        {
        }
    }
}