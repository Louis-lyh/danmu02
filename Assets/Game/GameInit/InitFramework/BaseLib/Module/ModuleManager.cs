using System.Collections.Generic;

namespace GameInit.Framework
{
    public class ModuleManager : Singleton<ModuleManager>
    {
        private bool _isInit;
        private List<ModuleBase> _allModule;
        private ModuleBase _curModule;

        public void Init()
        {
            _isInit = true;
            _allModule = new List<ModuleBase>();
        }

        public void InsertModule(ModuleBase module, int index = 0)
        {
            if (_allModule.Contains(module))
            {
                Logger.LogMagenta("重复添加Module, module type:" + module.GetType());
                return;
            }
            _allModule.Insert(index, module);
        }

        internal void ModuleEnd(ModuleBase moduleBase)
        {
            if (_curModule != moduleBase)
                return;
            _allModule.Remove(moduleBase);
            _curModule = null;
            Run();
        }

        public void Run()
        {
            if (!_isInit || _allModule.Count == 0)
            {
                return;
            }
            _curModule = _allModule[0];
            _curModule.Enter();
        }
    }
}