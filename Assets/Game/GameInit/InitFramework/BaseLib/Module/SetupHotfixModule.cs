using UnityEditor;
using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 启动热更代码模块逻辑
    /// </summary>
    public class SetupHotfixModule : ModuleBase
    {
#if USE_ILRUNTIME
        private static readonly string HotfixDll = "HotfixDll/HotFixDll.bytes";
        private static readonly string HotfixPDB = "HotfixDll/HotFixPDB.bytes";
#endif
        protected override async void OnEnter()
        {
#if USE_ILRUNTIME
            TextAsset dllAsset = null;
            TextAsset pdbAsset = null;
            if (SetUpConfig.Instance.UseAssetBundle)
            {
                dllAsset = await AssetBundleMgr.Instance.LoadAssetAsync<TextAsset>(HotfixDll);
                pdbAsset = await AssetBundleMgr.Instance.LoadAssetAsync<TextAsset>(HotfixPDB);
            }
            else
            {
#if UNITY_EDITOR
                dllAsset = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Res/" + HotfixDll);
                pdbAsset = AssetDatabase.LoadAssetAtPath<TextAsset>("Assets/Res/" + HotfixPDB);
#endif
            }

            if (dllAsset == null)
            {
                Logger.LogError("热更代码加载失败");
                return;
            }
            
            HotFixManager.Load(SetUpConfig.Instance.HotFixType, dllAsset.bytes, SetUpConfig.Instance.OpenDebug ? pdbAsset?.bytes : null);
#else
            HotFixManager.Load();
#endif
            Exit();
        }

        protected override void OnExit()
        {
            
        }
    }
}