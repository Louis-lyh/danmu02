using UnityEngine;
using System.Text;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源热更下载模块
    /// </summary>
    public class AssetBundleUpdateModule : ModuleBase
    {
        protected override async void OnEnter()
        {
            if (!SetUpConfig.Instance.UseAssetBundle)
            {
                Exit();
                return;
            }
#if UNITY_EDITOR
            if (SetUpConfig.Instance.AllowTestAB) ResourceManager.SetLoader(new DefaultResLoader());
            else ResourceManager.SetLoader(new BundleResLoader());
#else
            ResourceManager.SetLoader(new BundleResLoader());
#endif
            await AssetBundleMgr.Instance.Init();
            await LoadingManager.Instance.InitLoadingView();
            DownloadMgr.Instance.Init();
            if (Application.internetReachability != NetworkReachability.NotReachable && SetUpConfig.Instance.UseRemoteAssetBundle)
            {
                var bytes = await DownloadMgr.Instance.GetRemoteDataAsync(AssetBundleConst.GatewayFile);
                if (bytes == null)
                {
                    Exit();
                    return;
                }
                UserGroup.Instance.SetUserGroupData(UTF8Encoding.Default.GetString(bytes));
                if (!UserGroup.Instance.SelfNeedCheckDownload())
                {
                    Exit();
                    return;
                }
                await DownloadMgr.Instance.StartDownload(true);
            }
            Exit();
        }

        protected override void OnExit()
        {
            LoadingManager.Instance.HideLoading();
            base.OnExit();
        }
    }
}