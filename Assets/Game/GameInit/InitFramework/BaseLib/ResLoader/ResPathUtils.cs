﻿using Cysharp.Text;

namespace GameInit.Framework
{
    public class ResPathUtils
    {
        //获取UI路径
        public static string GetUIPrefab(string name)
        {
            return ZString.Concat("UI/", name, ".prefab");
        }

        public static string GetSoundPath(string name)
        {
            return ZString.Concat("Sound/", name,"");
        }

        //获取配置数据
        public static string GetConfigPath(string cfgName)
        {
            return ZString.Concat("Config/", cfgName, ".bytes");
        }
        
        public static string GetLanguageConfigPath(string cfgName)
        {
            return ZString.Concat("Config/", "LangSplit/", cfgName, ".bytes");
        }
        
        public static string GetAtlasPrefab(string name)
        {
            return ZString.Concat("Atlas/", name, ".prefab");
        }

        public static string GetSpriteAtlasPath(string name)
        {
            return ZString.Concat("Atlas/", name, ".spriteAtlas");
        }
        
        public static string GetFontPath(string fontName)
        {
            return ZString.Concat("Fonts/", fontName);
        }
        // 获取材质球
        public static string GetMat(string name)
        {
            return ZString.Concat("Material/", name, ".mat");
        }
    }
}