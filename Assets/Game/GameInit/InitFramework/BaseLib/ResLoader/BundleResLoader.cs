using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    public class BundleResLoader : IResourceLoader
    {
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="loadMode"></param>
        /// <returns></returns>
        public UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            return AssetBundleMgr.Instance.LoadSceneAsync(sceneName, loadMode);
        }

        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            return AssetBundleMgr.Instance.UnloadSceneAsync(sceneName);
        }

        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T LoadAssetSync<T>(string path) where T : Object
        {
            return AssetBundleMgr.Instance.LoadAssetSync<T>(path);
        }

        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            return AssetBundleMgr.Instance.LoadAssetAsync<T>(path);
        }

        /// <summary>
        /// 卸载一个资源
        /// </summary>
        /// <param name="path"></param>
        public void UnloadRes(string path, bool isForce = false)
        {
            AssetBundleMgr.Instance.UnloadRes(path, isForce);
        }
        
        public bool AssetExists(string path)
        {
            return AssetBundleMgr.Instance.AssetExists(path);
        }
    }
}