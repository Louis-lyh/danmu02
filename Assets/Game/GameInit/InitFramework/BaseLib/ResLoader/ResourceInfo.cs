using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Framework
{
    /// <summary>
    /// 资源加载完成信息
    /// </summary>
    public class ResourceInfo : IClassPool
    {
        public static Transform Parent;
        /// <summary>
        /// 引用次数
        /// </summary>
        public int useCount;
        /// <summary>
        /// 资源
        /// </summary>
        public UnityEngine.Object res;
        /// <summary>
        /// 使用列表
        /// </summary>
        private List<GameObject> _useList;
        /// <summary>
        /// 空闲列表
        /// </summary>
        private List<GameObject> _freeList;

        public GameObject Pop()
        {
            if(res != null)
            {
                Init();
                GameObject go = null;
                CheckNull();
                if (_freeList.Count > 0)
                {
                    go = _freeList[0];
                    _freeList.RemoveAt(0);
                }
                else
                {
                    go = GameObject.Instantiate(res) as GameObject;
                    SetTransform(go);
                }
                _useList.Add(go);
                return go;
            }
            return null;
        }
        
        public void Push(GameObject go, bool isDestroyImmediate = false)
        {
            if (res != null)
            {
                Init();
                if (_useList.Contains(go))
                    _useList.Remove(go);
                if (isDestroyImmediate)
                {
                    GameObject.Destroy(go);
                    return;
                }
                _freeList.Add(go);
                SetTransform(go);
            }
            else
            {
                Logger.LogMagenta("这个地方有点问题，正常不应该走这个Push:" + go.name);
            }
        }

        private void SetTransform(GameObject go)
        {
            go.transform.SetParent(Parent, false);
            go.SetActive(false);
        }
        
        private void Init()
        {
            _useList ??= new List<GameObject>();
            _freeList ??= new List<GameObject>();
            // 初始化对象池父节点
            if (Parent == null)
            {
                Parent = new GameObject().transform;
                GameObject.DontDestroyOnLoad(Parent.gameObject);
                Parent.name = "UnitGameObjectPool";
            }
        }
        
        private void CheckNull()
        {
            if(_useList != null)
            {
                for (var i = _useList.Count - 1; i >= 0; i--)
                {
                    var temp = _useList[i];
                    if (temp == null)
                        _useList.RemoveAt(i);
                }
            }

            if (_freeList == null)
                return;
            for (var i = _freeList.Count - 1; i >= 0; i--)
            {
                var temp = _freeList[i];
                if (temp == null)
                    _freeList.RemoveAt(i);
            }
        }
        
        public void Reset()
        {
            if (_useList != null)
            {
                for (int i = 0, count = _useList.Count; i < count; i++)
                    GameObject.Destroy(_useList[i]);
            }

            if (_freeList != null)
            {
                for (int i = 0, count = _freeList.Count; i < count; i++)
                    GameObject.Destroy(_freeList[i]);
            }
            
            _freeList = null;
            _useList = null;
            useCount = 0;
            res = null;
        }
    }
}