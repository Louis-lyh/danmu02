#if UNITY_EDITOR
using System.IO;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    public class DefaultResLoader : IResourceLoader
    {
        private const string PATH_PREFIX = "Assets/Res/";
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="loadMode"></param>
        /// <returns></returns>
        public async UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive)
        {
            return SceneManager.LoadSceneAsync(sceneName, loadMode);
        }

        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        public AsyncOperation UnloadSceneAsync(string sceneName)
        {
            return SceneManager.UnloadSceneAsync(sceneName);
        }

        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T LoadAssetSync<T>(string path) where T : Object
        {
            return AssetDatabase.LoadAssetAtPath<T>(PATH_PREFIX + path);
        }

        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            var fullPath = PATH_PREFIX + path;
            var result = AssetDatabase.LoadAssetAtPath<T>(fullPath);
            await UniTask.Yield();
            return result;
        }

        /// <summary>
        /// 卸载一个资源
        /// </summary>
        /// <param name="path"></param>
        public void UnloadRes(string path, bool isForce = false)
        {
            
        }

        public bool AssetExists(string path)
        {
            var fullPath = PATH_PREFIX + path;
            return File.Exists(fullPath);
        }
    }
}
#endif
