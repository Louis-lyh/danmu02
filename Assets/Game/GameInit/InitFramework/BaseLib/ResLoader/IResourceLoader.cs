using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameInit.Framework
{
    public interface IResourceLoader
    {
        /// <summary>
        /// 加载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="loadMode"></param>
        /// <returns></returns>
        UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode loadMode = LoadSceneMode.Additive);
        /// <summary>
        /// 卸载场景
        /// </summary>
        /// <param name="sceneName"></param>
        /// <returns></returns>
        AsyncOperation UnloadSceneAsync(string sceneName);
        /// <summary>
        /// 同步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T LoadAssetSync<T>(string path) where T : Object;
        /// <summary>
        /// 异步加载资源
        /// </summary>
        /// <param name="path"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        UniTask<T> LoadAssetAsync<T>(string path) where T : Object;
        
        /// <summary>
        /// 卸载一个资源
        /// </summary>
        /// <param name="path"></param>
        void UnloadRes(string path, bool isForce = false);

        bool AssetExists(string path);
    }
}