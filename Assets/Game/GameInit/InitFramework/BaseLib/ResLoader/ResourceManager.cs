using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Framework
{
    public static class ResourceManager
    {
        private static IResourceLoader _loader;

        static ResourceManager()
        {
#if UNITY_EDITOR
            SetLoader(new DefaultResLoader());
#endif
        }

        #region 对象池
        /// <summary>
        /// 路径和资源的对应
        /// </summary>
        private static readonly DictionaryList<string, ResourceInfo> _pathResourceInfoDic = new DictionaryList<string, ResourceInfo>();
        /// <summary>
        /// 实例化对象和路径对应
        /// </summary>
        private static readonly DictionaryList<GameObject, string> _objPathDic = new DictionaryList<GameObject, string>();
        private static GameObject CreateObjFromPool(string path)
        {
            GameObject targetObj = null;
            // 先从对象池获取对象
            var info = _pathResourceInfoDic.GetValue(path);
            // 如果从对象池获取到了对象，则直击进行回调操作
            if (info != null)
            {
                targetObj = info.Pop();
                _objPathDic.Add(targetObj, path);
                info.useCount++;

                //生成物品以后，默认摆放到很远的位置，不在0点，避免在场景中生成无效的
                targetObj.transform.position = Vector3.zero;
                targetObj.SetActive(true);
            }
            return targetObj;
        }

        /// <summary>
        /// 回收资源（如果是实例化的资源，尽量用这个回收）
        /// </summary>
        /// <param name="go"></param>
        /// <param name="delTime"></param>
        public static void CollectObj(GameObject go, bool isDestroyImmediate = false)
        {
            if (go == null)
                return;
            if (_objPathDic.HasKey(go))
            {
                var path = _objPathDic.GetValue(go);
                _objPathDic.Remove(go);
                var info = _pathResourceInfoDic.GetValue(path);
                if (info != null)
                {
                    if (info.useCount > 0)
                        info.useCount--;
                    info.Push(go, isDestroyImmediate);
                    UnloadRes(path);
                }
                else
                {
                    Logger.LogYellow("回收资源时找不到资源信息");
                    GameObject.Destroy(go);
                }
            }
            else
            {
                GameObject.Destroy(go);
            }
        }
        #endregion
        
        public static void SetLoader(IResourceLoader loader)
        {
            if (loader == null)
                return;
            Logger.Log("ResourceManager.SetLoader() => 设置加载器，loader:" + loader);
            _loader = loader;
        }
        
        /// 同步加载资源
        public static T LoadAssetSync<T>(string path) where T : Object
        {
            return _loader?.LoadAssetSync<T>(path);
        }

        private static readonly Dictionary<string, List<AutoResetUniTaskCompletionSource>> _tmpLoadTask = new Dictionary<string, List<AutoResetUniTaskCompletionSource>>();
        public static async UniTask<GameObject> ConstructObjAsync(string path)
        {
            var targetObj = CreateObjFromPool(path);
            // 如果从对象池获取到了对象，则直击进行回调操作
            if ( targetObj != null )
            {
                return targetObj;
            }
            
            if (_tmpLoadTask.ContainsKey(path))
            {
                var waiteTask = AutoResetUniTaskCompletionSource.Create();
                _tmpLoadTask[path].Add(waiteTask);
                await waiteTask.Task;
                targetObj = CreateObjFromPool(path);
                return targetObj;
            }
            
            _tmpLoadTask[path] = new List<AutoResetUniTaskCompletionSource>();
            targetObj = await LoadAssetAsync<GameObject>(path);
            
            var info = ClassPoolsManager<ResourceInfo>.Pop();
            info.res = targetObj;
            _pathResourceInfoDic.Add(path, info);
            var obj = info.Pop();
            if (obj == null)
            {
                Logger.LogError("ResourceManager.ConstructObjAsync() => 加载资源出错，资源不存在，path:" + path);
                _pathResourceInfoDic.Remove(path);
            }
            else
            {
                _objPathDic.Add(obj, path);
            }
            foreach (var value in  _tmpLoadTask[path])
                value.TrySetResult();
            _tmpLoadTask.Remove(path);
            return obj;
        }
        
        /// 异步加载资源
        public static UniTask<T> LoadAssetAsync<T>(string path) where T : Object
        {
            return _loader.LoadAssetAsync<T>(path);
        }
		
        /// 卸载资源
        public static void UnloadRes(string path, bool unload = false)
        {
            _loader?.UnloadRes(path, unload);
        }

        /// 异步加载场景
        public static UniTask<AsyncOperation> LoadSceneAsync(string sceneName, LoadSceneMode mode = LoadSceneMode.Additive)
        {
            return _loader.LoadSceneAsync(sceneName, mode);
        }

        /// 卸载场景
        public static void UnloadSceneAsync(string sceneName)
        {
            _loader.UnloadSceneAsync(sceneName);
        }

        public static bool AssetExists(string path)
        {
            return _loader.AssetExists(path);
        }
    }
}