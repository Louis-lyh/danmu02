using UnityEngine;

namespace GameInit.Framework
{
    public class SpriteAtlasInfo : MonoBehaviour
    {
        public string key;

        public string[] pngNames;
    }
}