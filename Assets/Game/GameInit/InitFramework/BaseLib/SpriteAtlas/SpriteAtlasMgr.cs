using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.U2D;

namespace GameInit.Framework
{
    public class SpriteAtlasMgr : Singleton<SpriteAtlasMgr>
    {
        private const string SPRITE_ATLAS_INFO_FILE = "AtlasInfoFile";
        private static Dictionary<string, string> _atlasDict;

        public async UniTask ReloadAtlasInfo()
        {
            _atlasDict ??= new Dictionary<string, string>();
            _atlasDict.Clear();

            var obj = await ResourceManager.LoadAssetAsync<GameObject>(ResPathUtils.GetAtlasPrefab(SPRITE_ATLAS_INFO_FILE));
            if (obj == null)
                return;
            var infos = obj.GetComponents<SpriteAtlasInfo>();
            foreach (var info in infos)
            {
                foreach (var png in info.pngNames)
                {
                    _atlasDict[png] = info.key;
                }
            }
        }

        public async UniTask<Sprite> LoadSprite(string pngName)
        {
            if (string.IsNullOrEmpty(pngName) || !_atlasDict.TryGetValue(pngName, out var atlasName))
            {
                return null;
            }

            return await LoadSprite(atlasName, pngName);
        }

        public async UniTask<Sprite> LoadSprite(string atlasName, string pngName)
        {
            var atlas = await ResourceManager.LoadAssetAsync<SpriteAtlas>(ResPathUtils.GetSpriteAtlasPath(atlasName));
            return atlas.GetSprite(pngName);
        }
        //同步加载
        public  Sprite LoadSpriteSync(string atlasName, string pngName)
        {
            var atlas =  ResourceManager.LoadAssetSync<SpriteAtlas>(ResPathUtils.GetSpriteAtlasPath(atlasName));
            return atlas.GetSprite(pngName);
        }
    }
}