﻿using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    public class LoadingManager : Singleton<LoadingManager>
    {
        private class LoadingView
        {
            private Text _contentText;
            private GameObject _loadingObject;
            private Image _barImage;
            internal RectTransform _rectTransform;

            internal void SetDisplayObject(GameObject gameObject)
            {
                _loadingObject = gameObject;
                _rectTransform = gameObject.transform as RectTransform;
                _contentText = _loadingObject.transform.Find("ProgressBar/TextProgress").GetComponent<Text>();
                _barImage = _loadingObject.transform.Find<Image>("ProgressBar/ImageProgress");
            }

            internal void ShowLoading(string content, float progress = 0f)
            {
                UpdateProgress(progress, true);
                _loadingObject.SetActive(true);
                _contentText.text = "Loading Server Resources...";
            }

            internal void UpdateProgress(float progress, bool isForceProgress = false)
            {
                progress = Mathf.Min(progress, 1f);
                if (isForceProgress)
                {
                    _barImage.DOKill();
                    _barImage.fillAmount = progress;
                }
                else
                {
                    _barImage.DOFillAmount(progress, 1.5f);
                }
            }

            internal void HideLoading(bool immediateHide = true)
            {
                if (immediateHide)
                    Hide();
                else
                    _barImage.DOFillAmount(1f, 0.3f).onComplete = Hide;
            }

            internal void Hide()
            {
                //_loadingObject.SetActive(false);
                //_barImage.fillAmount = 0f;
                //_barImage.DOKill();
                _contentText.text = "Server Resource Load Completed...";
            }

            internal void Close()
            {
                //_loadingObject.SetActive(false);
            }
        }

        public async UniTask InitLoadingView()
        {
            if (_loadingView != null)
                return;
            var loadingObject = await ResourceManager.ConstructObjAsync(ResPathUtils.GetUIPrefab("uiLoading"));
            if (loadingObject == null)
            {
                Logger.LogError("LoadingManager.ShowLoading() => 找不到uiLoading显示对象");
                return;
            }
            loadingObject.name = "uiLoading";
            _loadingView = new LoadingView();
            _loadingView.SetDisplayObject(loadingObject);
            var parent = GameObject.Find("TopRoot").transform;
            _loadingView._rectTransform.SetParent(parent, false);
            _loadingView._rectTransform.localPosition = Vector3.zero;
            _loadingView._rectTransform.localScale = Vector3.one;
        }

        private LoadingView _loadingView;
        public void ShowLoading(string content, float progress = 1f)
        {
            if (_loadingView == null)
                return;
            _loadingView.ShowLoading(content, progress);
            _loadingView._rectTransform.SetAsLastSibling();
        }

        public void HideLoading(bool immediateHide = true)
        {
            _loadingView?.HideLoading(immediateHide);
        }

        public void UpdateProgress(float progress)
        {
            _loadingView?.UpdateProgress(progress);
        }

        public void CloseLoading()
        {
            _loadingView?.Close();
        }
    }
}