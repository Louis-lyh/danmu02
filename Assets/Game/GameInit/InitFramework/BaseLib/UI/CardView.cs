using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;//别忘了引用

//卡牌状态，正面、背面
public enum CardState
{
    Front,
    Back
}
public class CardView : MonoBehaviour
{
    
    private GameObject _mFront;//卡牌正面
    private GameObject _mBack;//卡牌背面
    private CardState _mCardState = CardState.Front;//卡牌当前的状态，是正面还是背面？
    public float mTime = 0.3f;
    private bool _isActive = false;//true代表正在执行翻转，不许被打断

    /// <summary>
    /// 初始化卡牌角度，根据mCardState
    /// </summary>
    public void Init(GameObject mFront, GameObject mBack,CardState mCardState)
    {
        _mFront = mFront;
        _mBack = mBack;
        _mCardState = mCardState;
        if(mCardState==CardState.Front)
        {
            //如果是从正面开始，则将背面旋转90度，这样就看不见背面了
            mFront.transform.eulerAngles = Vector3.zero;
            mBack.transform.eulerAngles = new Vector3(0, 90, 0);
        }
        else
        {
            //从背面开始，同理
            mFront.transform.eulerAngles = new Vector3(0, 90, 0);
            mBack.transform.eulerAngles = Vector3.zero;
        }
    }
    /// <summary>
    /// 翻转到背面
    /// </summary>
    public void ToBack()
    {
        if (_isActive)
            return;
        
        _isActive = true;
        _mFront.transform.DORotate(new Vector3(0, 90, 0), mTime).OnComplete(() =>
        {
            _mBack.transform.DORotate(new Vector3(0, 0, 0), mTime);
            _isActive = false;
        });
    }
    /// <summary>
    /// 翻转到正面
    /// </summary>
    public void ToFront()
    {
        if (_isActive)
            return;
        
        _isActive = true;
        _mBack.transform.DORotate(new Vector3(0, 90, 0), mTime).OnComplete(() =>
        {
            _mFront.transform.DORotate(new Vector3(0, 0, 0), mTime);
            _isActive = false;
        });
    }
}
