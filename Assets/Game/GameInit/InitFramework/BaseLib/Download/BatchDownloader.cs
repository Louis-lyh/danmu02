﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace GameInit.Framework
{
    public enum DownloadStep
    {
        CdnUrl = 0,
        BackupCdnUrl,
        BackToSource,
    }
    
    public struct DownloadInfo
    {
        public string url;
        public string dir;
        public string fileName;
        public long fileSize;
        public string fileMD5;
        public string fileVer;
        // public string backupUrl;
    }

    /// <summary>
    /// 批量下载工具
    /// </summary>
    public class BatchDownloader
    {
        /// <summary>
        /// 下载总进度
        /// </summary>
        public float contentProgress
        {
            get
            {
                return (float)downloadedContentSize / contentSize;
            }
        }
        /// <summary>
        /// 更新文件的总大小
        /// </summary>
        public long contentSize { get; private set; }
        /// <summary>
        /// 已下载的文件总大小
        /// </summary>
        public long downloadedContentSize
        {
            get
            {
                if (_unityWebRequest  == null || !_loadYieldInstruction.keepWaiting)
                {
                    return _downloadedContentSize;
                }
                else
                {
                    return _downloadedContentSize + currentDownloadedContentSize;
                }
            }
        }

        /// <summary>
        /// 当前下载的文件的Url
        /// </summary>
        public string CurrentDownUrl { get; private set; }

        /// <summary>
        /// 当前下载设置的超时时间
        /// </summary>
        public int CurrentTimeOut { get; private set; }

        /// <summary>
        /// 当前文件的文件名
        /// </summary>
        public string CurrentContentName { get; private set; }

        /// <summary>
        /// 当前文件大小
        /// </summary>
        private long currentContentSize;// { get; private set; }
        /// <summary>
        /// 当前文件MD5
        /// </summary>
        public string currentContentMD5 { get; private set; }
        /// <summary>
        /// 当前文件的下载进度
        /// </summary>
        public float currentContentProgress
        {
            get
            {
                return _unityWebRequest.downloadProgress;
            }
        }
        /// <summary>
        /// 当前文件已下载的大小
        /// </summary>
        public long currentDownloadedContentSize
        {
            get
            {
                return (long)(currentContentProgress * currentContentSize);
            }
        }
        /// <summary>
        /// 当前文件是否下载完毕
        /// </summary>
        public bool isCurrentLoadOk
        {
            get
            {
                return !_loadYieldInstruction.keepWaiting;
            }
        }

        /// <summary>
        /// 总文件数量
        /// </summary>
        public int totalFileCnt { get; private set; }

        /// <summary>
        /// 当前文件序号
        /// </summary>
        public int currentFileIndex
        {
            get
            {
                return totalFileCnt - _downloadInfoQueue.Count + 1;
            }
        }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string error { get; private set; }

        private readonly Queue<DownloadInfo> _downloadInfoQueue;
        private readonly InternalYieldInstruction _loadYieldInstruction;
        private DownloadInfo _currentDownloadInfo;
        private UnityWebRequest _unityWebRequest;
        private DownloadStep _curDownloadStep;
        private long _downloadedContentSize;

        public BatchDownloader()
        {
            _loadYieldInstruction = new InternalYieldInstruction();
            _downloadInfoQueue = new Queue<DownloadInfo>(100);
            contentSize = 0;
        }

        public void AddFile(string dir, string url, string fileName, long size, string md5, string ver = "")
        {
            var info = new DownloadInfo();
            info.dir = FileUtil.FormatPath(dir);
            info.url = FileUtil.FormatPath(url);
            info.fileName = fileName;
            info.fileSize = size;
            info.fileMD5 = md5;
            info.fileVer = ver;
            _downloadInfoQueue.Enqueue(info);
            contentSize += size;
            totalFileCnt = _downloadInfoQueue.Count;
        }
        /// <summary>
        /// 准备下一个文件下载
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            if (_downloadInfoQueue.Count > 0)
            {
                _currentDownloadInfo = _downloadInfoQueue.Dequeue();
                CurrentContentName = Path.GetFileName(_currentDownloadInfo.fileName);
                currentContentSize = _currentDownloadInfo.fileSize;
                currentContentMD5 = _currentDownloadInfo.fileMD5;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 开始下载当前的文件。可以指定覆盖原文件还是断点续传
        /// </summary>
        public CustomYieldInstruction LoadCurrent(bool overrideFile, bool urlGet)
        {
            if (!_loadYieldInstruction.keepWaiting)
            {
                error = string.Empty;
                _curDownloadStep = DownloadStep.CdnUrl;
                RequestDownload();
            }

            return _loadYieldInstruction;
        }

        private void OnDownloadFinish()
        {
            string fullPath = _currentDownloadInfo.dir + _currentDownloadInfo.fileName;
            if (!Directory.Exists(_currentDownloadInfo.dir))
                Directory.CreateDirectory(_currentDownloadInfo.dir);

            File.WriteAllBytes(fullPath, _unityWebRequest.downloadHandler.data);

            _downloadedContentSize += currentContentSize;
            _loadYieldInstruction.InternalSetWaiting(false);
            _unityWebRequest.Dispose();
        }

        private void OnDownloadFailed(string errorMsg)
        {
            _unityWebRequest.Dispose();
            if (_curDownloadStep == DownloadStep.BackToSource)
            {
                error = errorMsg;
                _loadYieldInstruction.InternalSetWaiting(false);
            }
            else
            {
                _curDownloadStep = _curDownloadStep == DownloadStep.CdnUrl ? DownloadStep.BackupCdnUrl : DownloadStep.BackToSource;
                RequestDownload();
            }
        }

        private void RequestDownload()
        {
            string url = "";
            int timeOut = 10;
            switch (_curDownloadStep)
            {
                case DownloadStep.CdnUrl:
                    timeOut = 120;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
                case DownloadStep.BackupCdnUrl:
                    timeOut = 180;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
                case DownloadStep.BackToSource:
                    timeOut = 240;
                    url = _currentDownloadInfo.url + _currentDownloadInfo.fileName;
                    break;
            }

            if (!string.IsNullOrEmpty(_currentDownloadInfo.fileVer))
            {
                url = url + "_" + _currentDownloadInfo.fileVer;
                if (_curDownloadStep == DownloadStep.BackupCdnUrl)
                    url = url.UrlFormat();
            }
            else
            {
                url = url.UrlFormat();
            }
            CurrentDownUrl = url;
            CurrentTimeOut = timeOut;
            _unityWebRequest = UnityWebRequest.Get(url);
            _unityWebRequest.SendWebRequest(OnDownloadFinish, OnDownloadFailed, 0);
            _loadYieldInstruction.InternalSetWaiting(true);
        }
    }
}