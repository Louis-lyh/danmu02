﻿using System;
using System.IO;
using System.Text;
using Cysharp.Text;
using Cysharp.Threading.Tasks;
using UnityEngine.Networking;
using UnityEngine;

namespace GameInit.Framework
{
    public class DownloadMgr : Singleton<DownloadMgr>
    {
        /// <summary>
        /// 用流量下载的最大数据量
        /// </summary>
        private static readonly int CARRIER_DOWNLOAD_MAX_SIZE = 1048576 * 5; // 5MB

        private bool _downloadError;
        private long _remoteAssetsVer;
        private int _repeatCount;
        private string _remoteUrl;
        private bool _isShowProgress;

        public void Init()
        {
            _remoteUrl = ZString.Concat(SetUpConfig.Instance.GameAssetRemoteUrl, "/", AssetBundleConst.Platform, "/",
                Application.version, "/");
        }

        public async UniTask StartDownload(bool isShowProgress = false)
        {
            _repeatCount = 3;
            _isShowProgress = isShowProgress;
            var bytes = await GetRemoteDataAsync(AssetBundleConst.VersionFile);
            if (bytes != null)
            {
                var str = UTF8Encoding.Default.GetString(bytes);
                _remoteAssetsVer = AssetBundleUtils.DecodeVersion(str);
            }
            else
            {
                _remoteAssetsVer = 10086;
            }

            await CheckRemote();
        }

        private async UniTask CheckRemote()
        {
            var localVersion = AssetBundleMgr.LastestAssetVersion; // 如果要更新AB的话，必定之前为了加载出背景界面而初始化过AB
            Logger.Log("DownloadMgr.CheckRemote() => remoteVersion: " + _remoteAssetsVer + ", localVersion:" +
                       localVersion);
            // 如果远端资源号大于客户端资源号，则开始启动下载
            if (_remoteAssetsVer > localVersion)
            {
                _downloadError = false;
                await StartDownloadAssets();
                if (_downloadError)
                {
                    _repeatCount--;
                    if (_repeatCount <= 0)
                        await CheckRemote();
                    return;
                }

                // 因为AB有更新释放掉之前全部加载过的AB，这样已经加载出的背景界面与进度条只在缓存中存有数据，和AB不再联系
                AssetBundle.UnloadAllAssetBundles(false);
                await AssetBundleMgr.Instance.Init();
                Logger.Log("DownloadMgr.CheckRemote() => 更新完成");
            }
            else
            {
                Logger.Log("DownloadMgr.CheckRemote() => 当前已是最新的资源,不需要更新!");
            }
        }

        /// <summary>
        /// 执行热更新下载
        /// </summary>
        /// <returns></returns>
        private async UniTask StartDownloadAssets()
        {
            var bytes = await GetRemoteDataAsync(AssetBundleConst.AbAssetsInfoFile);
            var remoteAbInfos = AssetBundleUtils.DecodeStringPair(bytes);
            var downloader = new BatchDownloader();
            // 计算要更新的ab文件
            foreach (var kv in remoteAbInfos)
            {
                var rec = kv.Value;
                // 如果远端资源号大于客户端资源号，并且远端MD5和客户端MD5不匹配（防止资源下载过了，但check文件还没写入到本地），将该文件加入下载列表
                if (rec.FileVer > AssetBundleMgr.GetLastestAssetVer(rec.AbName))
                {
                    var localMD5 = AssetBundleMgr.GetLastestMD5(rec.AbName);
                    //localMD5为null的时候，表示新增的资源文件
                    if (string.IsNullOrEmpty(localMD5) || !localMD5.Equals(rec.AbMd5))
                    {
                        var fileUrl = Path.Combine(_remoteUrl, AssetBundleConst.AssetsDirectory);
                        downloader.AddFile(AssetBundleConst.PersistentABPath, fileUrl, rec.AbName, rec.FileSize,
                            rec.AbMd5, rec.FileVer.ToString());
                    }
                }
            }

            // 更新AbAssetsInfoFile文件
            downloader.AddFile(AssetBundleConst.PersistentDataPath, _remoteUrl, AssetBundleConst.AbAssetsInfoFile, 100,
                "");
            // 更新Ver文件
            downloader.AddFile(AssetBundleConst.PersistentDataPath, _remoteUrl, AssetBundleConst.VersionFile, 100, "");
            Logger.Log("DownloadMgr.StartDownloadAssets() => 开始下载远程资源: totalFileCnt:" + downloader.totalFileCnt +
                       ", contentSize:" + downloader.contentSize);
            if (_isShowProgress)
                LoadingManager.Instance.ShowLoading("开始下载远程资源，本次更新" + this.FormatFileSize(downloader.contentSize));

            _downloadError = false;
            var writeCnt = 0;
            while (downloader.MoveNext())
            {
                downloader.LoadCurrent(true, true);
                while (!downloader.isCurrentLoadOk)
                {
                    // 进度条
                    if (_isShowProgress)
                        LoadingManager.Instance.UpdateProgress(downloader.contentProgress);
                    await UniTask.WaitForEndOfFrame();
                }

                if (!string.IsNullOrEmpty(downloader.error))
                {
                    // 下载出错
                    Logger.LogError("DownloadMgr.StartDownloadAssets() => 下载文件出错,文件url:" + downloader.CurrentDownUrl +
                                    ", 文件名:" + downloader.CurrentContentName + ", 设置的超时时间:" +
                                    downloader.CurrentTimeOut + ", error:" + downloader.error + "]");
                    _downloadError = true;
                    break;
                }

                // TODO 这里更新本地的版本文件并保存
                if (remoteAbInfos.ContainsKey(downloader.CurrentContentName))
                {
                    writeCnt++;
                    ///资源目录下（/Assets/）单个文件下载完成后，把CDN上面文件信息写入到沙箱目录文件里面
                    var recStruct = remoteAbInfos[downloader.CurrentContentName];
                    if (writeCnt > 10)
                    {
                        writeCnt = 0;
                        AssetBundleMgr.WriteMD5ToPersistentFile(recStruct);
                    }
                    else
                    {
                        AssetBundleMgr.UpdateMD5ToPersistentFile(recStruct);
                    }
                }

                Logger.Log("下载完成 " + downloader.CurrentContentName);
            }
        }

        private string FormatFileSize(long size)
        {
            if (size < 1024) // 小于1K
                return $"{size:0.##}B";
            if (size < 1048576) // 小于1M
                return $"{size / 1024:0.##}KB";
            return $"{size / 1048576:0.##}MB";
        }

        public async UniTask<byte[]> GetRemoteDataAsync(string file)
        {
            var remoteAssetInfoUrl = Path.Combine(_remoteUrl, file).UrlFormat();
            Logger.Log("DownloadMgr.GetRemoteDataAsync() => url:" + _remoteUrl);
            try
            {
                var webRequest = UnityWebRequest.Get(remoteAssetInfoUrl);
                webRequest.timeout = 5;
                webRequest.redirectLimit = 0;
                webRequest.useHttpContinue = false;
                webRequest.chunkedTransfer = false;
                var request = await webRequest.SendWebRequest();
                return request.downloadHandler?.data;
            }
            catch (Exception ex)
            {
                Logger.LogError("DownloadMgr.GetRemoteDataAsync() => 请求文件出错，url:" + remoteAssetInfoUrl + ", error:" +
                                ex.Message);
                return null;
            }
        }
    }
}