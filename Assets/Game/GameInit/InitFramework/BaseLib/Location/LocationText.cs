﻿using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    [RequireComponent(typeof(Text))]
    public class LocationText : MonoBehaviour
    {
        public int LanguageID;
        public Font UseFont;
        public Text Text { get; private set; }

        private string _extContent = "";

        protected void Awake()
        {
            Text = GetComponent<Text>();
            UseFont = Text.font;
            LocationManager.RegisterLocationText(this);
            SwitchLanguage();
        }

        public void SetLanguageID(int languageId, string ext = "")
        {
            if (languageId == LanguageID)
                return;
            LanguageID = languageId;
            _extContent = ext;
            Text.text = LocationManager.GetLanguage(LanguageID) + _extContent;
        }

        protected void OnDestroy()
        {
            LocationManager.UnRegisterLocationText(this);
        }

        public void SwitchLanguage()
        {
            UseFont = LocationManager.CurrentUseFont;            
            if (UseFont != null)
                Text.font = UseFont;
            if (LanguageID == 0)
                return;
            Text.text = LocationManager.GetLanguage(LanguageID) + _extContent;
        }
    }
}