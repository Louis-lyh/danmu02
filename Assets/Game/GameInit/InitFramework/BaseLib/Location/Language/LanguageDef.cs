﻿namespace GameInit.Framework
{
    public class LanguageDef
    {
        /// <summary>
        /// 0
        /// </summary>
        public int ID;

        /// <summary>
        /// 1
        /// </summary>
        public string Lang;
        
        public void Parse(byte[] bytes, ref int byteIndex)
        {
            ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            Lang = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
        }
    }
}