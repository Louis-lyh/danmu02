﻿using System.Collections.Generic;

namespace GameInit.Framework
{
    public class LanguageConfig
    {
        public void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            LanguageDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new LanguageDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取Lang_CNDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static LanguageDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out LanguageDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, LanguageDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, LanguageDef> _defDic = new Dictionary<int, LanguageDef>();
    }
}