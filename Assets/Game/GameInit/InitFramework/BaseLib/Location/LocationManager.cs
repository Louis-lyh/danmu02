﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameInit.Framework
{
    public static class LocationManager
    {
        public static string LANG_KEY = "InfinityWarTDLanguage";

        private static SystemLanguage _curLanguage = SystemLanguage.English;
        public static SystemLanguage CurLanguage => _curLanguage;

        /// <summary>
        /// 当前字体
        /// </summary>
        public static Font CurrentUseFont;

        public static async UniTask Init()
        {
            var lang = PlayerPrefs.GetInt(LANG_KEY, (int) Application.systemLanguage);
            _curLanguage = AdjustLanguage((SystemLanguage) lang);
            await LoadLanguageConfigAndFont();
        }

        #region Location Text

        private static readonly Dictionary<int, LocationText> _locationText2TextComponent =
            new Dictionary<int, LocationText>();

        private static readonly List<LocationText> _allLocationTexts = new List<LocationText>();

        public static bool TodoGetLocationTextByHashCode(int hashCode, out LocationText value)
        {
            return _locationText2TextComponent.TryGetValue(hashCode, out value);
        }

        public static void RegisterLocationText(LocationText locationText)
        {
            if (_allLocationTexts.Contains(locationText))
                return;
            var hashCode = locationText.Text.GetHashCode();
            if (!_locationText2TextComponent.ContainsKey(hashCode))
                _locationText2TextComponent[hashCode] = locationText;
            _allLocationTexts.Add(locationText);
        }

        public static void UnRegisterLocationText(LocationText locationText)
        {
            if (!_allLocationTexts.Contains(locationText))
                return;
            var hashCode = locationText.Text.GetHashCode();
            if (_locationText2TextComponent.ContainsKey(hashCode))
                _locationText2TextComponent.Remove(hashCode);
            _allLocationTexts.Remove(locationText);
        }

        #endregion

        #region Location Image

        private static readonly List<LocationImage> _allLocationImages = new List<LocationImage>();
        private static string locationImageExt = "_cn";

        public static void RegisterLocationImage(LocationImage locationImage)
        {
            if (_allLocationImages.Contains(locationImage))
                return;
            _allLocationImages.Add(locationImage);
        }

        public static void UnRegisterLocationImage(LocationImage locationImage)
        {
            if (!_allLocationImages.Contains(locationImage))
                return;
            _allLocationImages.Remove(locationImage);
        }

        public static UniTask<Sprite> LoadLocationSprite(string name)
        {
            return SpriteAtlasMgr.Instance.LoadSprite(name + locationImageExt);
        }

        #endregion

        private static SystemLanguage AdjustLanguage(SystemLanguage language)
        {
            switch (language)
            {
                case SystemLanguage.Korean:
                    //韩文
                    break;
                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    //中文
                    language = SystemLanguage.Chinese;
                    break;
                default:
                    language = SystemLanguage.English;
                    //英语
                    break;
            }

            return language;
        }

        public static async void SwitchLanguage(SystemLanguage language)
        {
            language = AdjustLanguage(language);
            if (language == _curLanguage)
                return;
            _curLanguage = language;
            await LoadLanguageConfigAndFont();
            foreach (var locationText in _allLocationTexts)
                locationText.SwitchLanguage();
            foreach (var locationText in _allLocationImages)
                locationText.SwitchLanguage();
        }

        public static string GetLanguage(int key)
        {
            _def = LanguageConfig.Get(key);
            return _def == null ? +key + "缺失" : _def.Lang;
        }

        public static string FormatLanguage(int key, params object[] args)
        {
            if (args != null)
            {
                return string.Format(GetLanguage(key), args);
            }

            return GetLanguage(key);
        }

        private static readonly LanguageConfig _langCnConfig = new LanguageConfig();
        private static LanguageDef _def;

        private static async UniTask LoadLanguageConfigAndFont()
        {
            var fontName = "";
            var langeConfig = "";
            switch (_curLanguage)
            {
                case SystemLanguage.Chinese:
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    fontName = "Erya-Struggle.ttf";
                    langeConfig = "Lang_CN";
                    locationImageExt = "_cn";
                    break;
                // case SystemLanguage.Korean:
                //     fontName = "MiSans-Heavy.ttf";
                //     langeConfig = "Lang_HN";
                //     locationImageExt = "_hn";
                //     break;
                default:
                    fontName = "MiSans-Heavy.ttf";
                    langeConfig = "Lang_EN";
                    locationImageExt = "_en";
                    break;
            }

            PlayerPrefs.SetInt(LANG_KEY, (int) _curLanguage);
            PlayerPrefs.Save();

            CurrentUseFont = await ResourceManager.LoadAssetAsync<Font>(ResPathUtils.GetFontPath(fontName));
            var cfgAssetText =
                await ResourceManager.LoadAssetAsync<TextAsset>(ResPathUtils.GetLanguageConfigPath(langeConfig));
            if (cfgAssetText == null || cfgAssetText.bytes == null)
                return;
            _langCnConfig.Deserialize(cfgAssetText.bytes);
        }
    }
}