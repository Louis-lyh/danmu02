﻿using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Framework
{
    [RequireComponent(typeof(Image))]
    public class LocationImage : MonoBehaviour
    {
        public string ImageName;
        private Image _image;
        private void Awake()
        {
            _image = GetComponent<Image>();
            SwitchLanguage();
            LocationManager.RegisterLocationImage(this);
        }

        public async void SwitchLanguage()
        {
            if(!ImageName.IsValid())
                return;
            _image.sprite = await LocationManager.LoadLocationSprite(ImageName);
            _image.SetNativeSize();
        }
        
        private void OnDestroy()
        {
            _image = null;
            LocationManager.UnRegisterLocationImage(this);
        }
    }
}