﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityExtension.Animator
{
    [RequireComponent(typeof(UnityEngine.Animator))]
    public class AnimatorEvent : MonoBehaviour
    {
        private UnityEngine.Animator mAnimator;

        List<Action<GameObject, string>> mEvents;

        public UnityEngine.Animator LoadAnimator()
        {
            if (mAnimator == null)
                mAnimator = GetComponent<UnityEngine.Animator>();

            return mAnimator;
        }

        public void NotifyEvent(string param)
        {
            if (mEvents != null)
            {
                for (int i = 0; i < mEvents.Count; i++)
                    mEvents[i]?.Invoke(gameObject, param);
            }
        }

        [HideInInspector]
        public void RegEvent(Action<GameObject, string> action)
        {
            if (mEvents == null)
                mEvents = new List<Action<GameObject, string>>();
            mEvents.Add(action);
        }

        public void ClearEvents()
        {
            if (mEvents == null)
                mEvents = new List<Action<GameObject, string>>();
            mEvents.Clear();
        }
    }

    public static class AnimatorEventExtend
    {
        public static void RegisterAnimatorEvent(this Component component, Action<GameObject, string> callback)
        {
            if (component.GetComponent<AnimatorEvent>() == null) return;
            component.GetComponent<AnimatorEvent>().RegEvent(callback);
        }
    }
}