﻿#if USE_IAP
using UnityEngine.Purchasing;

namespace GameInit.Framework
{
    public interface IIAPListener
    {
        void OnInitialized(IStoreController controller, IExtensionProvider extensions);
        void OnPurchaseFailed(Product i);
        bool ProcessPurchase(Product i);
        void OnPurchaseStart();
        void OnRestoreStart();
        void OnRestoreComplete();
    }
}
#endif