﻿#if USE_IAP
using UnityEngine.Purchasing;
using System;
using System.Collections.Generic;

namespace GameInit.Framework
{
    public class ShopGood
    {
        public int ConfigId;
        public string ProductID;
        public int ShopType;
    }

    public class IAPManager : Singleton<IAPManager>, IIAPListener
    {
        private IAPStore _iapStore;
        public void Init(List<ShopGood> shopGoods)
        {
            _iapStore ??= new IAPStore();
            _iapStore.Init(this, shopGoods);
        }

        private Func<Product, int, bool> _onBuyProcessAction;
        private Action _buyFailedAction;
        public bool Purchase(string productId, Func<Product, int, bool> successAction, Action failedAction)
        {
            _onBuyProcessAction = successAction;
            _buyFailedAction = failedAction;
            return _iapStore.InitiatePurchase(productId);
        }
        
        #region 支付

        // 支付模块初始化完成
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            // _isPayReady = true;
        }

        // 支付失败
        public void OnPurchaseFailed(Product i)
        {
            // 执行支付失败回调
            _buyFailedAction?.Invoke();
            _buyFailedAction = null;
        }

        // 处理订单信息
        public bool ProcessPurchase(Product product)
        {
            var productId = product.definition.id;
            Logger.LogGreen("process purchase " + productId);
            OnPurchaseComplete();
            var id = _iapStore.Goods[productId];
            return _onBuyProcessAction.Invoke(product, id);
        }

        private static void OnPurchaseComplete()
        {
            
        }

        // 开始支付
        public void OnPurchaseStart()
        {
            
        }

        // 恢复支付
        public void OnRestoreStart()
        {
            
        }

        // 恢复支付完成
        public void OnRestoreComplete()
        {
            
        }
        #endregion
    }
}
#endif

