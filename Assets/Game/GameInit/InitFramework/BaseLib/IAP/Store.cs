using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

namespace GameHotfix.Framework
{
    public interface KGIAPListener
    {
        // iap 初始化接口
        void OnInitialized(IStoreController controller, IExtensionProvider extensions);

        // iap 初始化商品
        void InitializeGoods(ConfigurationBuilder builder);

        // iap 购买失败
        void OnPurchaseFailed(Product i);

        // iap 处理购买
        bool ProcessPurchase(Product i);

        // iap 开始购买
        void OnPurchaseStart();

        // iap 恢复购买
        void OnRestoreStart();

        // iap 恢复成功
        void OnRestoreComplate();
    }

    public class Store : IStoreListener
    {
        private static Store instance = null;
        private bool m_PurchaseInProgress = false;
        private IStoreController Controller = null;
        private IExtensionProvider Extensions = null;
        private KGIAPListener kGIAPListener = null;

        public static Store getInstance()
        {
            if (instance == null)
                instance = new Store();
            return instance;
        }

        public static void Initialize(KGIAPListener iapListener)
        {
            if (instance == null)
                instance = new Store();
            instance.Init(iapListener);
        }

        // 初始化应用内商品
        public void Init(KGIAPListener iapListener)
        {
            this.kGIAPListener = iapListener;

            //var module = StandardPurchasingModule.Instance();
            //var builder = ConfigurationBuilder.Instance(module);
            //var catalog = ProductCatalog.LoadDefaultCatalog();

            var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

            this.kGIAPListener.InitializeGoods(builder);

            //builder.AddProduct("100_gold_coins", ProductType.Consumable, new IDs
            //{
            //    {"100_gold_coins_google", GooglePlay.Name},
            //    {"100_gold_coins_mac", MacAppStore.Name}
            //});

            //IAPConfigurationHelper.PopulateConfigurationBuilder(ref builder, catalog);

#if AGGRESSIVE_INTERRUPT_RECOVERY_GOOGLEPLAY
        // For GooglePlay, if we have access to a backend server to deduplicate purchases, query purchase history
        // when attempting to recover from a network-interruption encountered during purchasing. Strongly recommend
        // deduplicating transactions across app reinstallations because this relies upon the on-device, deletable
        // TransactionLog database.
        builder.Configure<IGooglePlayConfiguration>().aggressivelyRecoverLostPurchases = true;
        // Use purchaseToken instead of orderId for all transactions to avoid non-unique transactionIDs for a
        // single purchase; two ProcessPurchase calls for one purchase, differing only by which field of the receipt
        // is used for the Product.transactionID. Automatically true if aggressivelyRecoverLostPurchases is enabled
        // and this API is not called at all.
        builder.Configure<IGooglePlayConfiguration>().UsePurchaseTokenForTransactionId(true);
#endif

#if INTERCEPT_PROMOTIONAL_PURCHASES
        // On iOS and tvOS we can intercept promotional purchases that come directly from the App Store.
        // On other platforms this will have no effect; OnPromotionalPurchase will never be called.
        builder.Configure<IAppleConfiguration>().SetApplePromotionalPurchaseInterceptorCallback(OnPromotionalPurchase);
        Debug.Log("Setting Apple promotional purchase interceptor callback");
#endif

            UnityPurchasing.Initialize(this, builder);
        }

        // 初始化成功
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            this.Controller = controller;
            this.Extensions = extensions;

            // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
            // On non-Apple platforms this will have no effect; OnDeferred will never be called.
            this.Extensions.GetExtension<IAppleExtensions>().RegisterPurchaseDeferredListener(OnDeferred);

            // kg iap listener
            this.kGIAPListener.OnInitialized(controller, extensions);
        }

        // 初始化失败
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Debug.Log("Billing failed to initialize!");
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    Debug.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Debug.Log("No products available for purchase!");
                    break;
            }
        }

        // 购买失败
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            m_PurchaseInProgress = false;

            Debug.Log("Purchase failed: " + i.definition.id);
            Debug.Log(p);

            // Detailed debugging information
            if (this.Extensions != null)
            {
                var transaction = this.Extensions.GetExtension<ITransactionHistoryExtensions>();
                Debug.Log("Store specific error code: " + transaction.GetLastStoreSpecificPurchaseErrorCode());
                if (transaction.GetLastPurchaseFailureDescription() != null)
                {
                    Debug.Log("Purchase failure description message: " +
                              transaction.GetLastPurchaseFailureDescription().message);
                }
            }

            this.kGIAPListener.OnPurchaseFailed(i);
        }

        // 处理购买
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            m_PurchaseInProgress = false;

            var product = e.purchasedProduct;

            // validator receipt
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
        var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);
            try
            {
                validator.Validate(product.receipt);
            }
            catch (IAPSecurityException)
            {
                Debug.Log("Invalid receipt, not unlocking content");
                return PurchaseProcessingResult.Complete;
            }
#endif

            if (this.kGIAPListener.ProcessPurchase(product))
                return PurchaseProcessingResult.Complete;
            else
                return PurchaseProcessingResult.Pending;
        }

        /// <summary>
        /// iOS Specific.
        /// This is called as part of Apple's 'Ask to buy' functionality,
        /// when a purchase is requested by a minor and referred to a parent
        /// for approval.
        ///
        /// When the purchase is approved or rejected, the normal purchase events
        /// will fire.
        /// </summary>
        /// <param name="item">Item.</param>
        private void OnDeferred(Product item)
        {
            Debug.Log("Purchase deferred: " + item.definition.id);

            this.kGIAPListener.ProcessPurchase(item);
        }

        // 启动购买
        public void InitiatePurchase(string productId)
        {
            if (m_PurchaseInProgress)
            {
                Debug.Log("Please wait, purchase in progress");
                return;
            }

            if (this.Controller == null || this.Extensions == null)
            {
                Debug.LogError("Purchasing is not initialized");
                return;
            }

            if (this.Controller.products.WithID(productId) == null)
            {
                Debug.LogError("No product has id " + productId);
                return;
            }

            m_PurchaseInProgress = true;

            this.kGIAPListener.OnPurchaseStart();

            this.Controller.InitiatePurchase(this.Controller.products.WithID(productId));
        }

        // 恢复购买
        public void RestorePurchase()
        {
#if UNITY_IPHONE
            this.kGIAPListener.OnRestoreStart();
            this.Extensions.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
            {
                this.kGIAPListener.OnRestoreComplate();
            });
#endif
        }

        public Product GetProduct(string productID)
        {
            if (this.Controller != null && this.Controller.products != null && !string.IsNullOrEmpty(productID))
            {
                return this.Controller.products.WithID(productID);
            }

            Debug.LogError("CodelessIAPStoreListener attempted to get unknown product " + productID);
            return null;
        }

        // checkIfProductIsAvailableForSubscriptionManager
        //private bool checkIfProductIsAvailableForSubscriptionManager(string receipt)
        //{
        //    var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
        //    if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
        //    {
        //        FDebug.Log("The product receipt does not contain enough information");
        //        return false;
        //    }
        //    var store = (string)receipt_wrapper["Store"];
        //    var payload = (string)receipt_wrapper["Payload"];

        //    if (payload == null)
        //        return false;

        //    switch (store)
        //    {
        //        case GooglePlay.Name:
        //            {
        //                var payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(payload);
        //                if (!payload_wrapper.ContainsKey("json"))
        //                {
        //                    FDebug.Log("The product receipt does not contain enough information, the 'json' field is missing");
        //                    return false;
        //                }
        //                var original_json_payload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode((string)payload_wrapper["json"]);
        //                if (original_json_payload_wrapper == null || !original_json_payload_wrapper.ContainsKey("developerPayload"))
        //                {
        //                    FDebug.Log("The product receipt does not contain enough information, the 'developerPayload' field is missing");
        //                    return false;
        //                }
        //                var developerPayloadJSON = (string)original_json_payload_wrapper["developerPayload"];
        //                var developerPayload_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(developerPayloadJSON);
        //                if (developerPayload_wrapper == null || !developerPayload_wrapper.ContainsKey("is_free_trial") || !developerPayload_wrapper.ContainsKey("has_introductory_price_trial"))
        //                {
        //                    FDebug.Log("The product receipt does not contain enough information, the product is not purchased using 1.19 or later");
        //                    return false;
        //                }
        //                return true;
        //            }
        //        case AppleAppStore.Name:
        //        case AmazonApps.Name:
        //        case MacAppStore.Name:
        //            {
        //                return true;
        //            }
        //        default:
        //            {
        //                return false;
        //            }
        //    }
        //}
    }
}