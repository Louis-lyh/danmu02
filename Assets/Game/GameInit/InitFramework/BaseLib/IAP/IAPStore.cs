﻿#if USE_IAP
using System.Collections.Generic;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
using UnityEngine;
using UnityEngine.Purchasing.Security;
#endif

namespace GameInit.Framework
{
    internal class IAPStore : IStoreListener
    {
        private bool _purchaseInProgress;
        private IStoreController _controller;
        private IExtensionProvider _extensions;
        private IIAPListener _iapListener;
        public readonly Dictionary<string, int> Goods = new Dictionary<string, int>();

        // 初始化失败
        public void OnInitializeFailed(InitializationFailureReason error)
        {
            Logger.Log("Billing failed to initialize!");
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    Logger.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    Logger.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    Logger.Log("No products available for purchase!");
                    break;
            }
        }
        
        private void AddProducts(ConfigurationBuilder builder, List<ShopGood> shopGoods)
        {
            foreach (var good in shopGoods)
            {
                builder.AddProduct(good.ProductID, GetProductType(good.ShopType), new IDs() {{good.ProductID, GetStoreName()}});
                Goods.Add(good.ProductID, good.ConfigId);
            }
        }
        
        private static string GetStoreName()
        {
#if UNITY_ANDROID || UNITY_EDITOR
            const string name = GooglePlay.Name;
#elif UNITY_IOS
            const string name = AppleAppStore.Name;
#endif
            return name;
        }
        
        private static ProductType GetProductType(int type)
        {
            switch (type)
            {
                case 2:
                    return ProductType.NonConsumable; // 去广告
                case 3:
                    return ProductType.Subscription; // 订阅
                default:
                    return ProductType.Consumable; // 消耗品
            }
        }
        
        // 初始化应用内商品
        public void Init(IIAPListener iapListener, List<ShopGood> shopGoods)
        {
            _iapListener = iapListener;

            var module = StandardPurchasingModule.Instance();
            var builder = ConfigurationBuilder.Instance(module);
            AddProducts(builder, shopGoods);
#if UNITY_ANDROID && !UNITY_EDITOR
            //builder.Configure<IGooglePlayConfiguration>().SetPublicKey("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt1qrw3wP00J1Vv0Z9CVc/vnan/i6XAqOQJYwZAOhkJ+utcTz465Hd1cupsOVBRI5Ic9pvgmBFdm1hjdqeJTxEWcvBd5EEFpAQ3AAlGGysfSPlBA2RCL+zIKGVdpUGhX4NWEBgkSrKrd7ZeMsOcVQjj3Yr7RAXfJJCTnA2Yf31XpWUce5bHtQhQC52KsZNH9arhfEOp4e/Dvz0xiMsTOnfsFivovPFw2MthLaw+U6R5goq6LMHXwu4butphChYiGil6xlnEq5t9P4DQOOTdjAwsBy7FRQAJKj5dVxQFqnpLjO+w9RKF8AJeW8uFotKuP0tawxpYt3XlHTKr6wEndDuQIDAQAB");
#endif

#if AGGRESSIVE_INTERRUPT_RECOVERY_GOOGLEPLAY
        // For GooglePlay, if we have access to a backend server to deduplicate purchases, query purchase history
        // when attempting to recover from a network-interruption encountered during purchasing. Strongly recommend
        // deduplicating transactions across app reinstallations because this relies upon the on-device, deletable
        // TransactionLog database.
        builder.Configure<IGooglePlayConfiguration>().aggressivelyRecoverLostPurchases = true;
        // Use purchaseToken instead of orderId for all transactions to avoid non-unique transactionIDs for a
        // single purchase; two ProcessPurchase calls for one purchase, differing only by which field of the receipt
        // is used for the Product.transactionID. Automatically true if aggressivelyRecoverLostPurchases is enabled
        // and this API is not called at all.
        builder.Configure<IGooglePlayConfiguration>().UsePurchaseTokenForTransactionId(true);
#endif

#if INTERCEPT_PROMOTIONAL_PURCHASES
        // On iOS and tvOS we can intercept promotional purchases that come directly from the App Store.
        // On other platforms this will have no effect; OnPromotionalPurchase will never be called.
        builder.Configure<IAppleConfiguration>().SetApplePromotionalPurchaseInterceptorCallback(OnPromotionalPurchase);
        Debug.Log("Setting Apple promotional purchase interceptor callback");
#endif
            UnityPurchasing.Initialize(this, builder);
        }
        
        // 处理购买
        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            _purchaseInProgress = false;
            var product = e.purchasedProduct;
            // validator receipt
#if (UNITY_IPHONE || UNITY_ANDROID) && !UNITY_EDITOR
            var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
                AppleTangle.Data(), Application.identifier);
            try
            {
                validator.Validate(product.receipt);
            }
            catch (IAPSecurityException)
            {
                Debug.Log("Invalid receipt, not unlocking content");
                return PurchaseProcessingResult.Complete;
            }
#endif
            return _iapListener.ProcessPurchase(product)
                ? PurchaseProcessingResult.Complete
                : PurchaseProcessingResult.Pending;
        }
        
        // 购买失败
        public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
        {
            _purchaseInProgress = false;

            Logger.Log("Purchase failed: " + i.definition.id);
            Logger.Log(p);

            // Detailed debugging information
            if (_extensions != null)
            {
                var transaction = _extensions.GetExtension<ITransactionHistoryExtensions>();
                Logger.Log("Store specific error code: " + transaction.GetLastStoreSpecificPurchaseErrorCode());
                if (transaction.GetLastPurchaseFailureDescription() != null)
                    Logger.Log("Purchase failure description message: " + transaction.GetLastPurchaseFailureDescription().message);
            }

            _iapListener.OnPurchaseFailed(i);
        }
        
        
        // 初始化成功
        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            _controller = controller;
            _extensions = extensions;

            // On Apple platforms we need to handle deferred purchases caused by Apple's Ask to Buy feature.
            // On non-Apple platforms this will have no effect; OnDeferred will never be called.
            _extensions.GetExtension<IAppleExtensions>().RegisterPurchaseDeferredListener(OnDeferred);

            //iap listener
            _iapListener.OnInitialized(controller, extensions);
        }
        
        
        /// <summary>
        /// iOS Specific.
        /// This is called as part of Apple's 'Ask to buy' functionality,
        /// when a purchase is requested by a minor and referred to a parent
        /// for approval.
        ///
        /// When the purchase is approved or rejected, the normal purchase events
        /// will fire.
        /// </summary>
        /// <param name="item">Item.</param>
        private void OnDeferred(Product item)
        {
            Logger.Log("Purchase deferred: " + item.definition.id);

            _iapListener.ProcessPurchase(item);
        }

        // 恢复购买
        public void RestorePurchase()
        {
#if UNITY_IPHONE
            _iIAPListener.OnRestoreStart();
            _extensions.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
            {
                this._iIAPListener.OnRestoreComplete();
            });
#endif
        }

        public Product GetProduct(string productID)
        {
            if (_controller?.products != null && !string.IsNullOrEmpty(productID))
            {
                return _controller.products.WithID(productID);
            }

            Logger.LogError("CodelessIAPStoreListener attempted to get unknown product " + productID);
            return null;
        }
        
        // 启动购买
        public bool InitiatePurchase(string productId)
        {
            if (_purchaseInProgress)
            {
                Logger.Log("Please wait, purchase in progress");
                return false;
            }
            
            if (_controller == null || _extensions == null)
            {
                Logger.LogError("Purchasing is not initialized");
                return false;
            }
            
            if (_controller.products.WithID(productId) == null)
            {
                Logger.LogError("No product has id " + productId);
                return false;
            }
            
            _purchaseInProgress = true;
            _iapListener.OnPurchaseStart();
            _controller.InitiatePurchase(_controller.products.WithID(productId));
            return true;
        }
    }
}
#endif