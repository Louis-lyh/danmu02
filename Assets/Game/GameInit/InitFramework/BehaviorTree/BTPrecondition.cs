﻿namespace GameInit.Game
{
    public abstract class BTPrecondition : BTTreeNode
    {
        public BTPrecondition(int maxChildCount)
            : base(maxChildCount)
        {
            
        }
        
        public abstract bool IsTrue(BTWorkingData wData);
    }
    
    public abstract class BTPreconditionLeaf : BTPrecondition
    {
        public BTPreconditionLeaf()
            : base(0)
        {
            
        }
    }
    
    public abstract class BTPreconditionUnary : BTPrecondition
    {
        public BTPreconditionUnary(BTPrecondition lhs)
            : base(1)
        {
            AddChild(lhs);
        }
    }
    public abstract class BTPreconditionBinary : BTPrecondition
    {
        public BTPreconditionBinary(BTPrecondition lhs, BTPrecondition rhs)
            : base(2)
        {
            AddChild(lhs).AddChild(rhs);
        }
    }
    
    public class BTPreconditionTrue : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            return true;
        }
    }
    
    public class BTPreconditionFalse : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            return false;
        }
    }
    
    public class BTPreconditionNot : BTPreconditionUnary
    {
        public BTPreconditionNot(BTPrecondition lhs)
            : base(lhs)
        {}
        public override bool IsTrue(BTWorkingData wData)
        {
            return !GetChild<BTPrecondition>(0).IsTrue(wData);
        }
    }
    
    public class BTPreconditionAnd : BTPreconditionBinary
    {
        public BTPreconditionAnd(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) &&
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
    
    public class BTPreconditionOr : BTPreconditionBinary
    {
        public BTPreconditionOr(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) ||
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
    
    public class BTPreconditionXor : BTPreconditionBinary
    {
        public BTPreconditionXor(BTPrecondition lhs, BTPrecondition rhs)
            : base(lhs, rhs)
        { }
        public override bool IsTrue(BTWorkingData wData)
        {
            return GetChild<BTPrecondition>(0).IsTrue(wData) ^
                   GetChild<BTPrecondition>(1).IsTrue(wData);
        }
    }
}