﻿namespace GameInit.Game
{
    public class BTActionNonPrioritizedSelector : BTActionPrioritizedSelector
    {
        public BTActionNonPrioritizedSelector()
            : base()
        {
        }
        
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            var thisContext = GetContext<BTActionPrioritizedSelectorContext>(wData);
            if (!IsIndexValid(thisContext.currentSelectedIndex))
                return base.OnEvaluate(wData);
            var node = GetChild<BTAction>(thisContext.currentSelectedIndex);
            return node.Evaluate(wData) || base.OnEvaluate(wData);
        }
    }
}
