﻿namespace GameInit.Game
{
    public class BTActionLoop : BTAction
    {
        public const int INFINITY = -1;
        //--------------------------------------------------------
        protected class TBTActionLoopContext : TBTActionContext
        {
            internal int currentCount;

            public TBTActionLoopContext()
            {
                currentCount = 0;
            }
        }
        
        private int _loopCount;
        public BTActionLoop()
            : base(1)
        {
            _loopCount = INFINITY;
        }
        
        public BTActionLoop SetLoopCount(int count)
        {
            _loopCount = count;
            return this;
        }
        
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionLoopContext>(wData);
            var checkLoopCount = (_loopCount == INFINITY || thisContext.currentCount < _loopCount);
            if (!checkLoopCount || !IsIndexValid(0)) 
            {
                return false;
            }

            var node = GetChild<BTAction>(0);
            return node.Evaluate(wData);
        }
        protected override int OnUpdate(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionLoopContext>(wData);
            var runningStatus = BTRunningStatus.FINISHED;
            if (!IsIndexValid(0))
                return runningStatus;
            var node = GetChild<BTAction>(0);
            runningStatus = node.Update(wData);
            if (!BTRunningStatus.IsFinished(runningStatus))
                return runningStatus;
            thisContext.currentCount++;
            if (thisContext.currentCount < _loopCount || _loopCount == INFINITY)
            {
                runningStatus = BTRunningStatus.EXECUTING;
            }
            return runningStatus;
        }
        protected override void OnTransition(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionLoopContext>(wData);
            if (IsIndexValid(0)) 
            {
                BTAction node = GetChild<BTAction>(0);
                node.Transition(wData);
            }
            thisContext.currentCount = 0;
        }
    }
}