﻿using System.Collections.Generic;
using GameInit.Framework;

namespace GameInit.Game
{
    public class BTTreeNode
    {
        private const int defaultChildCount = -1;
        private List<BTTreeNode> _children;
        private readonly int _maxChildCount;
        public BTTreeNode(int maxChildCount = -1)
        {
            _children = new List<BTTreeNode>();
            if (maxChildCount >= 0) 
            {
                _children.Capacity = maxChildCount;
            }
            _maxChildCount = maxChildCount;
        }

        public BTTreeNode()
            : this(defaultChildCount)
        {
            
        }

        public virtual void Dispose()
        {
            _children = null;
        }

        public BTTreeNode AddChild(BTTreeNode node)
        {
            if (_maxChildCount >= 0 && _children.Count >= _maxChildCount)
            {
                Logger.LogWarning("BTTreeNode.AddChild() => exceeding child count");
                return this;
            }
            _children.Add(node);
            return this;
        }
        
        public int GetChildCount()
        {
            return _children.Count;
        }
        
        public bool IsIndexValid(int index)
        {
            return index >= 0 && _children != null && index < _children.Count;
        }
        
        public T GetChild<T>(int index) where T : BTTreeNode 
        {
            if (index < 0 || index >= _children.Count) {
                return null;
            }
            return (T)_children[index];
        }
    }
}