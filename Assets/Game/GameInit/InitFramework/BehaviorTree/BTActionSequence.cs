﻿namespace GameInit.Game
{
    public class BTActionSequence : BTAction
    {
        protected class TBTActionSequenceContext : TBTActionContext
        {
            internal int currentSelectedIndex;
            public TBTActionSequenceContext()
            {
                currentSelectedIndex = -1;
            }
        }
        
        public BTActionSequence()
            : base(-1)
        {
        }
        // 判断-评估 节点可以执行
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 获取当前状态
            var thisContext = GetContext<TBTActionSequenceContext>(wData);
            // 重置节点序号
            var checkedNodeIndex = -1;
            // 判断节点是否合法
            if (IsIndexValid(thisContext.currentSelectedIndex)) 
            {
                checkedNodeIndex = thisContext.currentSelectedIndex; // 选择当前
            } 
            else
            {
                checkedNodeIndex = 0; // 选择第一个
            }
            
            // 判断节点是否合法
            if (IsIndexValid(checkedNodeIndex)) 
            {
                // 拿到节点
                var node = GetChild<BTAction>(checkedNodeIndex);
                if (node.Evaluate(wData))
                {
                    thisContext.currentSelectedIndex = checkedNodeIndex;
                    return true;
                }
            }
            return false;
        }
        protected override int OnUpdate(BTWorkingData wData)
        {
            // 状态
            var thisContext = GetContext<TBTActionSequenceContext>(wData);
            // 重置运行状态
            var runningStatus = BTRunningStatus.FINISHED;
            // 获取节点
            var node = GetChild<BTAction>(thisContext.currentSelectedIndex);
            // 运行节点
            runningStatus = node.Update(wData);
            // 判断运行节点是否错误
            if (BTRunningStatus.IsError(runningStatus)) 
            {
                thisContext.currentSelectedIndex = -1; // 错误重置序号退出
                return runningStatus;
            }
            // 判断是否完成
            if (BTRunningStatus.IsFinished(runningStatus))
            {
                // 完成节点序号加一
                thisContext.currentSelectedIndex++;
                // 判断序号是否有效
                if (IsIndexValid(thisContext.currentSelectedIndex))
                {
                    runningStatus = BTRunningStatus.EXECUTING; // 执行
                } 
                else 
                {
                    // 重置序号
                    thisContext.currentSelectedIndex = -1;
                }
            }
            return runningStatus;
        }
        
        protected override void OnTransition(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionSequenceContext>(wData);
            var node = GetChild<BTAction>(thisContext.currentSelectedIndex);
            node?.Transition(wData);
            thisContext.currentSelectedIndex = -1;
        }
    }
}