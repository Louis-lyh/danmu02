﻿namespace GameInit.Game
{
    public static class BTRunningStatus
    {
        public const int EXECUTING   = 0;
        public const int FINISHED    = 1;
        public const int TRANSITION  = 2;
        private static bool IsOk(int runningStatus)
        {
            return runningStatus == FINISHED;
        }
        // 是否错误
        public static bool IsError(int runningStatus)
        {
            return runningStatus < 0;
        }
        // 判断是否完成
        public static bool IsFinished(int runningStatus)
        {
            return IsOk(runningStatus) || IsError(runningStatus);
        }
        // 是否运行中
        public static bool IsExecuting(int runningStatus)
        {
            return !IsFinished(runningStatus);
        }
    }
}