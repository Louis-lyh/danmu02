﻿namespace GameInit.Game
{
    public class TBTActionContext
    {
    }

    public abstract class BTAction : BTTreeNode
    {
        protected BTPrecondition _precondition;
        public BTAction(int maxChildCount)
            : base(maxChildCount)
        {
        }

        public override void Dispose()
        {
            _precondition = null;
            base.Dispose();
        }

        public bool Evaluate(BTWorkingData wData)
        {
            return (_precondition == null || _precondition.IsTrue(wData)) && OnEvaluate(wData);
        }
        public int Update(BTWorkingData wData)
        {
            return OnUpdate(wData);
        }
        public void Transition(BTWorkingData wData)
        {
            OnTransition(wData);
        }
        
        public BTAction SetPrecondition(BTPrecondition precondition)
        {
            _precondition = precondition;
            return this;
        }
        
        protected T GetContext<T>(BTWorkingData wData) where T : TBTActionContext, new()
        {
            int uniqueKey = GetHashCode();
            T thisContext;
            if (wData.Context.ContainsKey(uniqueKey) == false)
            {
                thisContext = new T();
                wData.Context.Add(uniqueKey, thisContext);
            }
            else
            {
                thisContext = (T)wData.Context[uniqueKey];
            }
            return thisContext;
        }
        // 评估
        protected virtual bool OnEvaluate(BTWorkingData wData)
        {
            return true;
        }
        // 更新
        protected virtual int OnUpdate(BTWorkingData wData)
        {
            return BTRunningStatus.FINISHED;
        }
        // 变动-结束        
        protected virtual void OnTransition(BTWorkingData wData)
        {
        }
    }
}
