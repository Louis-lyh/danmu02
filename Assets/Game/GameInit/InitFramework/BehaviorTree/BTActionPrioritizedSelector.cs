﻿using GameHotfix.Game;
using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 行为树优先级选择器
    /// </summary>
    public class BTActionPrioritizedSelector : BTAction
    {
        /// <summary>
        /// 行为树状态
        /// </summary>
        protected class BTActionPrioritizedSelectorContext : TBTActionContext
        {
            // 当前选择节点
            internal int currentSelectedIndex;
            // 上一个选择节点
            internal int lastSelectedIndex;

            public BTActionPrioritizedSelectorContext()
            {
                currentSelectedIndex = -1;
                lastSelectedIndex = -1;
            }
        }
        
        public BTActionPrioritizedSelector()
            : base(-1)
        {
        }
        // 判断选择那个节点
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            // 当前状态
            var thisContext = GetContext<BTActionPrioritizedSelectorContext>(wData);
            // 重置当前选择节点
            thisContext.currentSelectedIndex = -1;
            // 获取孩子节点数量
            var childCount = GetChildCount();
            for(var i = 0; i < childCount; i++) 
            {
                var node = GetChild<BTAction>(i);
                // 判断是否满足条件
                if (node.Evaluate(wData))
                {
                    // 选择当前节点
                    thisContext.currentSelectedIndex = i;
                    return true;
                }
            }
            return false;
        }
        
        protected override int OnUpdate(BTWorkingData wData)
        {
            // 获取状态
            var thisContext = GetContext<BTActionPrioritizedSelectorContext>(wData);
            //
            var b = wData.As<FighterBehaviourData>();
            // 初始化运行状态
            var runningState = BTRunningStatus.FINISHED;
            // 当前行为节点不是上一次行为节点
            if (thisContext.currentSelectedIndex != thisContext.lastSelectedIndex) 
            {
                // 判断上一次节点是否有效
                if (IsIndexValid(thisContext.lastSelectedIndex))
                {
                    var node = GetChild<BTAction>(thisContext.lastSelectedIndex);
                    // 处理上个节点状态
                    node.Transition(wData);
                }
                thisContext.lastSelectedIndex = thisContext.currentSelectedIndex;
            }
            if (IsIndexValid(thisContext.lastSelectedIndex)) 
            {
                var node = GetChild<BTAction>(thisContext.lastSelectedIndex);
                runningState = node.Update(wData);
                if (BTRunningStatus.IsFinished(runningState)) 
                {
                    thisContext.lastSelectedIndex = -1;
                }
            }
            return runningState;
        }
        
        protected override void OnTransition(BTWorkingData wData)
        {
            // 获取状态
            var thisContext = GetContext<BTActionPrioritizedSelectorContext>(wData);
            // 拿到上一个节点
            var node = GetChild<BTAction>(thisContext.lastSelectedIndex);
            if (node != null)
            {
                node.Transition(wData);    // 结束
            }
            // 重置上一个节点
            thisContext.lastSelectedIndex = -1;
        }
    }
}