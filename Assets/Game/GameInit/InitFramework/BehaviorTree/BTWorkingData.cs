﻿using System.Collections.Generic;

namespace GameInit.Game
{
    public class BTWorkingData
    {
        // 状态字典
        internal Dictionary<int, TBTActionContext> _context;
        internal Dictionary<int, TBTActionContext> Context => _context;
        
        public BTWorkingData()
        {
            _context = new Dictionary<int, TBTActionContext>();
        }
        
        public T As<T>() where T : BTWorkingData
        {
            return (T)this;
        }

        public virtual void Dispose()
        {
            _context?.Clear();
            _context = null;
        }
    }
}