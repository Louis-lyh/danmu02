﻿using System.Collections.Generic;

namespace GameInit.Game
{
    public class BTActionParallel : BTAction
    {
        public enum ECHILDREN_RELATIONSHIP
        {
            AND, OR
        }
        //-------------------------------------------------------
        protected class TBTActionParallelContext : TBTActionContext
        {
            internal List<bool> evaluationStatus;
            internal List<int> runningStatus;

            public TBTActionParallelContext()
            {
                evaluationStatus = new List<bool>();
                runningStatus = new List<int>();
            }
        }
        
        private ECHILDREN_RELATIONSHIP _evaluationRelationship;
        private ECHILDREN_RELATIONSHIP _runningStatusRelationship;
        public BTActionParallel()
            : base(-1)
        {
            _evaluationRelationship = ECHILDREN_RELATIONSHIP.AND;
            _runningStatusRelationship = ECHILDREN_RELATIONSHIP.OR;
        }
        
        public BTActionParallel SetEvaluationRelationship(ECHILDREN_RELATIONSHIP v)
        {
            _evaluationRelationship = v;
            return this;
        }
        
        public BTActionParallel SetRunningStatusRelationship(ECHILDREN_RELATIONSHIP v)
        {
            _runningStatusRelationship = v;
            return this;
        }
        
        protected override bool OnEvaluate(BTWorkingData wData)
        {
            TBTActionParallelContext thisContext = GetContext<TBTActionParallelContext>(wData);
            InitListTo<bool>(thisContext.evaluationStatus, false);
            var finalResult = false;
            for (int i = 0; i < GetChildCount(); ++i)
            {
                var node = GetChild<BTAction>(i);
                var ret = node.Evaluate(wData);
                if (_evaluationRelationship == ECHILDREN_RELATIONSHIP.AND && ret == false) 
                {
                    finalResult = false;
                    break;
                }
                if (ret)
                {
                    finalResult = true;
                }
                thisContext.evaluationStatus[i] = ret;
            }
            return finalResult;
        }
        protected override int OnUpdate(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionParallelContext>(wData);
            if (thisContext.runningStatus.Count != GetChildCount()) 
            {
                InitListTo<int>(thisContext.runningStatus, BTRunningStatus.EXECUTING);
            }
            var hasFinished  = false;
            var hasExecuting = false;
            for (var i = 0; i < GetChildCount(); ++i)
            {
                if (thisContext.evaluationStatus[i] == false)
                {
                    continue;
                }
                if (BTRunningStatus.IsFinished(thisContext.runningStatus[i])) 
                {
                    hasFinished = true;
                    continue;
                }
                var node = GetChild<BTAction>(i);
                var runningStatus = node.Update(wData);
                if (BTRunningStatus.IsFinished(runningStatus))
                {
                    hasFinished  = true;
                }
                else 
                {
                    hasExecuting = true;
                }
                thisContext.runningStatus[i] = runningStatus;
            }
            if (_runningStatusRelationship == ECHILDREN_RELATIONSHIP.OR && hasFinished 
                || _runningStatusRelationship == ECHILDREN_RELATIONSHIP.AND && hasExecuting == false) 
            {
                InitListTo<int>(thisContext.runningStatus, BTRunningStatus.EXECUTING);
                return BTRunningStatus.FINISHED;
            }
            return BTRunningStatus.EXECUTING;
        }
        protected override void OnTransition(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionParallelContext>(wData);
            for (var i = 0; i < GetChildCount(); ++i) 
            {
                var node = GetChild<BTAction>(i);
                node.Transition(wData);
            }
            
            //clear running status
            InitListTo<int>(thisContext.runningStatus, BTRunningStatus.EXECUTING);
        }
        
        private void InitListTo<T>(List<T> list, T value)
        {
            int childCount = GetChildCount();
            if (list.Count != childCount)
            {
                list.Clear();
                for (var i = 0; i < childCount; ++i)
                {
                    list.Add(value);
                }
            } 
            else
            {
                for (var i = 0; i < childCount; ++i)
                {
                    list[i] = value;
                }
            }
        }
    }
}