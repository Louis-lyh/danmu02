﻿namespace GameInit.Game
{
    public abstract class BTActionLeaf : BTAction
    {
        /// <summary>
        /// 准备
        /// </summary>
        private const int ACTION_READY = 0;
        /// <summary>
        /// 运行
        /// </summary>
        private const int ACTION_RUNNING = 1;
        /// <summary>
        /// 完成
        /// </summary>
        private const int ACTION_FINISHED = 2;
        /// <summary>
        /// 状态-上下文
        /// </summary>
        class TBTActionLeafContext : TBTActionContext
        {
            // 状态
            internal int  status;
            // 是否需要退出
            internal bool needExit;

            private object _userData;
            public T getUserData<T>() where T : class, new()
            {
                if (_userData == null) {
                    _userData = new T();
                }
                return (T)_userData;
            }
            public TBTActionLeafContext()
            {
                status = ACTION_READY;
                needExit = false;

                _userData = null;
            }
        }
        public BTActionLeaf()
            : base(0)
        {
        }
        // 执行
        protected sealed override int OnUpdate(BTWorkingData wData)
        {
            // 重置运行状态
            var runningState = BTRunningStatus.FINISHED;
            // 获取上下文状态
            var thisContext = GetContext<TBTActionLeafContext>(wData);
            // 准备状态
            if (thisContext.status == ACTION_READY) 
            {
                OnEnter(wData);// 进入行为节点
                thisContext.needExit = true;//需要退出
                thisContext.status = ACTION_RUNNING;//运行状态
            }
            // 运行状态
            if (thisContext.status == ACTION_RUNNING) 
            {
                runningState = OnExecute(wData); // 运行
                // 判断是否完成
                if (BTRunningStatus.IsFinished(runningState))    
                {
                    thisContext.status = ACTION_FINISHED; // 完成
                }
            }
            // 完成状态
            if (thisContext.status == ACTION_FINISHED)
            {
                // 判断是否需要退出处理
                if (thisContext.needExit) 
                {
                    OnExit(wData, runningState); // 退出
                }
                thisContext.status = ACTION_READY;
                thisContext.needExit = false;
            }
            return runningState;
        }
        
        protected sealed override void OnTransition(BTWorkingData wData)
        {
            var thisContext = GetContext<TBTActionLeafContext>(wData);
            if (thisContext.needExit) 
            {
                OnExit(wData, BTRunningStatus.TRANSITION);
            }
            thisContext.status = ACTION_READY;
            thisContext.needExit = false;
        }
        
        protected virtual void OnEnter(BTWorkingData wData)
        {
        }
        
        protected virtual int OnExecute(BTWorkingData wData)
        {
            return BTRunningStatus.FINISHED;
        }
        
        protected virtual void OnExit(BTWorkingData wData, int runningStatus)
        {

        }
    }
}