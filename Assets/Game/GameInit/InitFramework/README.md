# KGFramework_BaseLib

- 功能说明

    框架母包库,包含基础的工具(Editor)、第三方工具库(ThirdParty)、初始化库(Game)

- 使用方法

    通过子模块方式添加，子模块路径为Assets\GameInit\InitFramework



Git地址：

SSH：   git@172.31.128.128:Roy2022/kgframework_baselib.git

Http:   http://172.31.128.128:9999/Roy2022/kgframework_baselib.git


