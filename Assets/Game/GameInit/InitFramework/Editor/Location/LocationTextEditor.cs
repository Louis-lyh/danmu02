﻿using System;
using GameInit.Framework;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Framework.Editor
{
    //[CustomEditor(typeof(LocationText))]
    public class LocationTextEditor : UnityEditor.Editor
    {
        // public override void OnInspectorGUI()
        // {
        //     var component = (LocationText)target;
        //     base.OnInspectorGUI();
        //     component.LanguageID = Convert.ToInt32(EditorGUILayout.TextField("语言表配置ID", component.LanguageID.ToString()));
        //     component.UseFont = (Font)EditorGUILayout.ObjectField("当前字体", component.UseFont, typeof(Font), true);
        // }

        [MenuItem("Assets/Kunggame/Prefab本地化/添加LocationText脚本", false, 15)]
        [MenuItem("Kunggame/Prefab本地化/添加LocationText脚本", false, 15)]
        private static void AddLocationText()
        {
            if (Selection.activeGameObject == null)
                return;
            var components = Selection.activeGameObject.GetComponentsInChildren<Text>(true);
            if (components == null || components.Length == 0)
                return;
            foreach (var component in components)
                component.gameObject.GetComponent<LocationText>(true);
            PrefabUtility.SavePrefabAsset(Selection.activeGameObject);
        }
    }
}