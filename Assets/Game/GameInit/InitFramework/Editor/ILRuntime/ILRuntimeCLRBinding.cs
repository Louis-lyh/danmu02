﻿#if UNITY_EDITOR && USE_ILRUNTIME
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using GameInit.Framework;

namespace Framework.Editor
{
    [System.Reflection.Obfuscation(Exclude = true)]
    public static class ILRuntimeCLRBinding
    {
        [MenuItem("Kunggame/ILRuntime/Generate CLR Binding Code by Analysis",false, 12)]
        static void GenerateCLRBindingByAnalysis()
        {
            //用新的分析热更dll调用引用来生成绑定代码
            ILRuntime.Runtime.Enviorment.AppDomain domain = new ILRuntime.Runtime.Enviorment.AppDomain();
            using (FileStream fs = new FileStream("Assets/Res/HotFixDll/HotFixDll.bytes", FileMode.Open, FileAccess.Read))
            {
                domain.LoadAssembly(fs);
                ILRuntimeDllLoader.Init(domain);
                ILRuntime.Runtime.CLRBinding.BindingCodeGenerator.GenerateBindingCode(domain, "Assets/Game/GameInit/Game/ILHelper/ILBinding");
                AssetDatabase.Refresh();
            }
        }

        [MenuItem("Kunggame/ILRuntime/Clear CLR Binding Code", false, 12)]
        static void ClearCLRBinding()
        {
            ILRuntime.Runtime.CLRBinding.BindingCodeGenerator.GenerateBindingCode(new List<Type>(), "Assets/Game/GameInit/Game/ILHelper/ILBinding");
            AssetDatabase.Refresh();
        }
    }
}
#endif

