//请配置需要自定义打AB的文件夹
//无需要搜索依赖目录: 以下自动打包 不需要配置
//Config HotFixDll Sound Arts/Fonts Atlas

//非场景prefab资源搜索(需依赖): 以下自动打包 不需要配置
//UI Effect GamePlay

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Editor
{
    [CreateAssetMenu(menuName = "ScriptableObjects/AssetBundleScriptableObject")]
    public class AssetBundleSobjConfig : ScriptableObject
    {
        //无需要搜索依赖目录
        public List<CWithoutDependencies> WithoutDList = new List<CWithoutDependencies>();
        
        //非场景prefab资源搜索(需依赖)recursive
        public List<CWithDependencies> WithDList = new List<CWithDependencies>();
    }

    [System.Serializable]
    public class CWithoutDependencies
    {
        public string Name = "";
        public bool Recursive = false;
        public bool BlInOneBundle = false;
        public string InOneBundleName = "";
    }

    [System.Serializable]
    public class CWithDependencies
    {
        public string Name = "";
        public bool Recursive = true;
    }
}
