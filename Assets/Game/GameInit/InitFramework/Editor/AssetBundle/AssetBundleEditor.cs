using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using GameInit.Framework;
using FileUtil = GameInit.Framework.FileUtil;
using Logger = GameInit.Framework.Logger;

namespace Framework.Editor
{
    /**
     * 
     * 游戏AB生成工具,实现说明，AB生成由两个目录组成：ABOutPut_Path、ABFinally_Path、ABUpload_Path
     *     1、ABOutPut_Path: ab文件源目录，此目录下资源为源始资源生成的ab文件，不管目标ab是否需要加密，此目录下的资源都不会参与，目的是为了增量打包（加快导出速度）
     *     2、ABFinally_Path： ab最终输出目录，根据需求处理是否加密
     *     3、ABUpload_Path：ab上传到FTP目录，文件带了版本号
     * SearchAllResource() 根据需求设置需要生成ab的资源目录
     * CopyVideoToAssets() 游戏中mp4文件不作任何处理，直接复制到ab输出目录
     */
    public class AssetBundleTool
    {
        #region 调用生成AB入口

        [MenuItem("Kunggame/AssetBundle/Build Android Bundle",false, 11)]
        public static UniTask BuildAndroidBundle()
        {
            return OneKeyBuildAssetBundle(BuildTarget.Android, targetPathIsAsset: true);
        }

        [MenuItem("Kunggame/AssetBundle/Build IOS Bundle",false, 11)]
        public static UniTask BuildIOSBundle()
        {
            return OneKeyBuildAssetBundle(BuildTarget.iOS, targetPathIsAsset:true);
        }

        [MenuItem("Kunggame/AssetBundle/Build Win Bundle",false, 11)]
        public static void BuildWinBundle()
        {
            OneKeyBuildAssetBundle(BuildTarget.StandaloneWindows, targetPathIsAsset:true);
        }

        #endregion
        static string AssetsRoot = Application.dataPath + "/";
        private static string OutPutAssetBundleRoot = Application.dataPath + "/../AssetBundles/";
        private static string ABOutPut_Path = OutPutAssetBundleRoot + "Original/StreamingAssets/" + AssetBundleConst.Platform + "/";
        public static string ABFinally_Path = OutPutAssetBundleRoot + "Finally/StreamingAssets/" + AssetBundleConst.Platform + "/";
        private static string ABUpload_Path = OutPutAssetBundleRoot + "Upload/" + AssetBundleConst.Platform + "/";
        private static UniTask OneKeyBuildAssetBundle(BuildTarget target, bool encryptAssetBundle = false, bool targetPathIsAsset = false)
        {
            try
            {
                SearchAllResource();
                BuildAssetBundle(target, encryptAssetBundle, targetPathIsAsset);
            }
            catch (Exception e)
            {
                Logger.LogError("AssetBundleEditor.OneKeyBuildAssetBundle() => 生成ab出错，Error:" + e.Message);
            }
            EditorUtility.ClearProgressBar();
            AssetDatabase.Refresh();
            return UniTask.CompletedTask;
        }

        #region 生成AssetBundle配置文件
        static void GenAssetBundleConfig(List<AssetBundleBuild> list)
        {
            string finallyAssetPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
            string streamingInfoFile = ABFinally_Path + AssetBundleConst.AbAssetsInfoFile;
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(finallyAssetPath, allFiles, false, ".DS_Store", ".meta", ".manifest");

            if(Directory.Exists(ABUpload_Path))
                Directory.Delete(ABUpload_Path, true);
            string uploadAssetDir = ABUpload_Path + AssetBundleConst.AssetsDirectory + "/";
            Directory.CreateDirectory(uploadAssetDir);
            
            Dictionary<string, GameAbInfo> assetBundleInfos = new Dictionary<string, GameAbInfo>();
            long ver = DateTime.Now.Ticks;
            
            //生成动态加载资源，对应的assetbundle名字
            string abName, assets;
            string resSplit = "/res/";
            GameAbInfo gameAbInfo;
            foreach (var assetBundle in list)
            {
                abName = assetBundle.assetBundleName;
                gameAbInfo = new GameAbInfo();
                gameAbInfo.AbName = abName;
                gameAbInfo.FileVer = ver;
                foreach (var assetName in assetBundle.assetNames)
                {
                    assets = assetName.ToLower();
                    var ext = Path.GetExtension(assets);
                    
                    if (ext.Equals(".unity"))
                    {
                        assets = Path.GetFileNameWithoutExtension(assets);
                        gameAbInfo.AddFile(assets);
                    }
                    else
                    {
                        if (assets.Contains(resSplit))
                        {
                            assets = assets.Replace(ext, "");
                            int resourcesIndex = assets.IndexOf(resSplit);
                            assets = assets.Substring(resourcesIndex + resSplit.Length);
                            gameAbInfo.AddFile(assets);
                        }
                    }
                }

                if (assetBundleInfos.ContainsKey(abName))
                {
                    Logger.LogMagenta("[AssetBundleTool.GenAssetBundleConfig() => 重复AB资源, abName:" + abName + "]");
                    continue;
                }

                assetBundleInfos.Add(abName, gameAbInfo);
            }

            string file, fileName, md5Value;
            long fileSize;
            int i;
            for (i = allFiles.Count - 1; i >= 0; i--)
            {
                file = allFiles[i];
                fileName = Path.GetFileName(file);
                md5Value = MD5Util.MD5file(file);
                fileSize = FileUtil.GetFileSize(file);
                if (assetBundleInfos.ContainsKey(fileName))
                {
                    gameAbInfo = assetBundleInfos[fileName];
                }
                else
                {
                    //没有的话，应该只有mp4文件，mp4文件不生成ab
                    gameAbInfo = new GameAbInfo();
                    gameAbInfo.AbName = fileName;
                    gameAbInfo.AddFile(fileName);
                    gameAbInfo.FileVer = ver;
                    assetBundleInfos.Add(fileName, gameAbInfo);
                }

                gameAbInfo.AbMd5 = md5Value;
                gameAbInfo.FileSize = fileSize;
            }
            // _bundleLogs.Clear();

            if (File.Exists(streamingInfoFile))
            {
                var oldInfos = AssetBundleUtils.DecodeStringPair(File.ReadAllBytes(streamingInfoFile), false);
                foreach (var value in assetBundleInfos.Values)
                {
                    if (oldInfos.ContainsKey(value.AbName))
                    {
                        if (oldInfos[value.AbName].AbMd5.Equals(value.AbMd5))
                        {
                            value.FileVer = oldInfos[value.AbName].FileVer;
                            value.FileSize = oldInfos[value.AbName].FileSize;
                        }
                        // else
                        // {
                        //     AddBuildLog(value.AbName, value.AbMd5, value.FileVer, oldInfos[value.AbName].AbMd5, false);
                        // }
                    }
                    // else
                    // {
                    //     AddBuildLog(value.AbName, value.AbMd5, value.FileVer, "", true);
                    // }
                }
            }
            // else
            // {
            //     foreach (var abInfo in assetBundleInfos.Values)
            //         AddBuildLog(abInfo.AbName, abInfo.AbMd5, abInfo.FileVer, "", true);
            // }
            
            //Ver文件
            FileWriteAllText(ABFinally_Path + AssetBundleConst.VersionFile, ver.ToString());
            //AbAssetsInfoFile文件
            FileWriteAllBytes(ABFinally_Path + AssetBundleConst.AbAssetsInfoFile, AssetBundleUtils.EncodeStringPair(assetBundleInfos, true));

            string uploadFile;
            for (i = allFiles.Count - 1; i >= 0; i--)
            {
                file = allFiles[i];
                fileName = Path.GetFileName(file);

                if (assetBundleInfos.ContainsKey(fileName))
                {
                    uploadFile = uploadAssetDir + fileName + "_" + assetBundleInfos[fileName].FileVer;
                    File.Copy(file, uploadFile);
                }
            }
            File.Copy(ABFinally_Path + AssetBundleConst.VersionFile, ABUpload_Path + AssetBundleConst.VersionFile);
            File.Copy(ABFinally_Path + AssetBundleConst.AbAssetsInfoFile, ABUpload_Path + AssetBundleConst.AbAssetsInfoFile);
            Logger.Log("导出AB完成");
        }

        //static Dictionary<string, BuildAssetBundleLog> _bundleLogs = new Dictionary<string, BuildAssetBundleLog>();
        // static void AddBuildLog(string key, string newHash, long ver, string oldHash, bool isAddBundle)
        // {
        //     if (_bundleLogs.ContainsKey(key))
        //     {
        //         Logger.LogMagenta("[AssetBundleTool.AddBuildLog() => 重复增加AssetBundle, key:" + key + "]");
        //         return;
        //     }
        //
        //     if (string.IsNullOrEmpty(newHash))
        //         return;
        //
        //     BuildAssetBundleLog bundleLog = new BuildAssetBundleLog();
        //     bundleLog.Key = key;
        //     bundleLog.NewHash = newHash;
        //     bundleLog.Ver = ver;
        //     bundleLog.OldHash = oldHash;
        //     bundleLog.IsAddAssetBundle = isAddBundle;
        //     _bundleLogs.Add(key, bundleLog);
        // }

        static void CreateFileDir(string path)
        {
            if (!File.Exists(path))
            {
                var dir = Path.GetDirectoryName(path);
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
        }
        static void FileWriteAllBytes(string path, byte[] bytes)
        {
            CreateFileDir(path);
            File.WriteAllBytes(path,bytes);
        }
        static void FileWriteAllLines(string path, string[] contents)
        {
            CreateFileDir(path);
            File.WriteAllLines(path, contents);
        }
        static void FileWriteAllText(string path, string contents)
        {
            CreateFileDir(path);
            File.WriteAllText(path, contents);
        }
        #endregion

        /// <summary>
        /// 目标平台，是否加密，是否直接打到StreamingAssets下
        /// </summary>
        /// <param name="target"></param>
        /// <param name="encryptAssetBundle"></param>
        /// <param name="targetPathIsAsset"></param>
        private static UniTask BuildAssetBundle(BuildTarget target, bool encryptAssetBundle = false, bool targetPathIsAsset = false)
        {
            EditorUtility.DisplayProgressBar("Build All Resource to AssetBundle", "Please wait...", 1f);
            // 创建存放AssetBundle文件的文件夹
            
            if (targetPathIsAsset)
            {
                ABOutPut_Path = Application.streamingAssetsPath + "/" + AssetBundleConst.Platform + "/";
                ABFinally_Path = ABOutPut_Path;
            }
            string abPath = ABOutPut_Path + AssetBundleConst.AssetsDirectory;
            if (!Directory.Exists(abPath))
                Directory.CreateDirectory(abPath);
            string bundleName;
            foreach (var kv in _dictTmpCount)
            {
                if (kv.Value > 1)
                {
                    int idx = kv.Key.LastIndexOf("/");
                    if (idx > 0)
                    {
                        bundleName = FormatPath2Name(kv.Key.Substring(0, idx));
                        AddBundleAssetFile(bundleName, kv.Key);
                    }
                }
            }
            
            List<AssetBundleBuild> list = new List<AssetBundleBuild>();
            foreach (var kv in _dictBundles)
            {
                AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
                assetBundleBuild.assetBundleName = kv.Key;
                assetBundleBuild.assetNames = kv.Value.ToArray();
                list.Add(assetBundleBuild);
            }
            
            AssetBundleManifest manifest = BuildPipeline.BuildAssetBundles(abPath, list.ToArray(),
                BuildAssetBundleOptions.ChunkBasedCompression | BuildAssetBundleOptions.DeterministicAssetBundle, target);
            EditorUtility.ClearProgressBar();
            EditorUtility.DisplayProgressBar("正在生成配置文件", "等等...", 1f);
            // 遍历获取所有AssetBundle文件, 并且清理以前冗余的AB文件
            string[] newAssetBundles = manifest.GetAllAssetBundles();
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(ABOutPut_Path + AssetBundleConst.AssetsDirectory, allFiles, false, ".DS_Store", ".manifest");
            if(!targetPathIsAsset)
            {
                string finallyAssetPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
                if (Directory.Exists(finallyAssetPath))
                    Directory.Delete(finallyAssetPath, true);
                Directory.CreateDirectory(finallyAssetPath);

                string outputPath = finallyAssetPath + "/";
                string file;
                for (int i = allFiles.Count - 1; i >= 0; i--)
                {
                    file = allFiles[i];
                    var fileName = Path.GetFileName(file);
                    if (!newAssetBundles.Contains(fileName))
                    {
                        //删除旧的assetbundle文件
                        string delManifest = allFiles[i] + ".manifest";
                        if (File.Exists(delManifest))
                            File.Delete(delManifest);
                        File.Delete(allFiles[i]);
                    }
                    else
                    {
                        byte[] fileData = File.ReadAllBytes(file);
                        using (FileStream fs = new FileStream(outputPath + fileName, FileMode.OpenOrCreate, FileAccess.Write))
                            fs.Write(fileData, 0, fileData.Length);
                    }
                }
            }
            GenAssetBundleConfig(list);
            EditorUtility.ClearProgressBar();
            return UniTask.CompletedTask;
        }

        #region 搜索资源
        /// bundle名称对应的资源列表
        private static Dictionary<string, List<string>> _dictBundles = new Dictionary<string, List<string>>();
        /// 记录单个资源，所属bundle名字
        private static Dictionary<string, string> _dictTmpRecord = new Dictionary<string, string>();
        /// 非场景资源依赖文件，先计数处理，如果被引用资源大于1，则生成公用ab
        private static Dictionary<string, int> _dictTmpCount = new Dictionary<string, int>();

        static List<string> FilesToAssetPathFiles(string path, bool recursive = false)//List<string> files)
        {
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(path, allFiles, recursive, ".meta", ".DS_Store");
            List<string> values = new List<string>();
            string assetPath;
            foreach (var file in allFiles)
            {
                if (file.StartsWith("Assets/"))
                {
                    assetPath = file;
                }
                else
                {
                    assetPath = "Assets/" + file.Replace(AssetsRoot, string.Empty);
                }
                values.Add(assetPath);
            }
            return values;
        }

        #region 添加Bundle文件
        static void AddBundleAssetFile(string bundleName, string assetFile)
        {
            List<string> listBundleAssets;
            if (_dictTmpRecord.ContainsKey(assetFile))
                return;
            bundleName = GetBundleName(bundleName);
            if (_dictBundles.ContainsKey(bundleName))
            {
                listBundleAssets = _dictBundles[bundleName];
            }
            else
            {
                listBundleAssets = new List<string>();
                _dictBundles.Add(bundleName, listBundleAssets);
            }
            listBundleAssets.Add(assetFile);
            _dictTmpRecord.Add(assetFile, bundleName);
        }

        static void AddBundleAssetFile(string bundleName, List<string> assetFiles)
        {
            for (int i = 0; i < assetFiles.Count; i++)
                AddBundleAssetFile(bundleName, assetFiles[i]);
        }
        /// <summary>
        /// 获取bundleName,图集和贴图打在一个AB里
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        static string GetBundleName(string bundleName)
        {
            string result = null;
            string prefix = "arts$ui$";
            if (bundleName.Contains(prefix))
            {
                result = bundleName.Replace(prefix, "res$atlas$") + "$spriteatlas";
                if (!result.StartsWith("assets$"))
                {
                    result = "assets$" + result;
                }
            }
            else
            {
                result = bundleName;
            }
            
            return result;
        }
        #endregion

        static void CollectBundleBuildWithoutDependencies(string path, bool recursive = false, bool blInOneBundle = false, string InOneBundleName = "")
        {
            List<string> allAssetFiles = FilesToAssetPathFiles(path, recursive);
            string bundleName;
            foreach (var file in allAssetFiles)
            {
                bundleName = FormatPath2Name(!blInOneBundle ? file : InOneBundleName);
                AddBundleAssetFile(bundleName, file);
            }
        }

        static void CollectBundleBuildWithDependencies(string path, bool recursive = false)
        {
            List<string> allAssetFiles = FilesToAssetPathFiles(path, recursive);
            for (int i = 0; i < allAssetFiles.Count; i++)
                ParseAssetDependencies(allAssetFiles[i]);
        }

        static void ParseAssetDependencies(string assetPath, bool blSceneDepends = false)
        {
            string[] depends = AssetDatabase.GetDependencies(assetPath);
            Action<string> DoParseAsset = (path) =>
            {
                string fileExtension, bundleName, depAssetPath;
                depAssetPath = path; 
                fileExtension = Path.GetExtension(depAssetPath);
                bundleName = FormatPath2Name(depAssetPath);
                if (_dictTmpRecord.ContainsKey(depAssetPath))
                {
                    if (!blSceneDepends)
                    {
                        if (_dictTmpCount.ContainsKey(depAssetPath))
                            _dictTmpCount[depAssetPath] += 1;
                        else
                            _dictTmpCount[depAssetPath] = 1;
                    }
                    return;
                }
                switch (fileExtension.ToLower())
                {
                    case ".shader":
                        AddBundleAssetFile(FormatPath2Name("Assets/Arts/GameShaders"), depAssetPath);
                        break;
                    case ".prefab":
                    case ".mat":
                        AddBundleAssetFile(bundleName, depAssetPath);
                        ParseAssetDependencies(depAssetPath, blSceneDepends);
                        break;
                    case ".png":
                    case ".jpg":
                    //case ".mat":
                    case ".anim":
                    case ".tga":
                    case ".fbx":
                    case ".controller":
                    case ".mesh":
                    case ".asset":
                        //如果是场景文件依赖，就按资源目录生成ab文件(具体原因可以参考:https://answer.uwa4d.com/question/5c35666c9efb1b001255de54)
                        if (blSceneDepends)
                        {
                            int idx = depAssetPath.LastIndexOf("/");
                            if (idx > 0)
                                bundleName = FormatPath2Name(depAssetPath.Substring(0, idx));
                            AddBundleAssetFile(bundleName, depAssetPath);
                            if (_dictTmpCount.ContainsKey(depAssetPath))
                                _dictTmpCount.Remove(depAssetPath);
                        }
                        else
                        {
                            if (_dictTmpCount.ContainsKey(depAssetPath))
                                _dictTmpCount[depAssetPath] += 1;
                            else
                                _dictTmpCount[depAssetPath] = 1;
                        }
                        break;
                }
            };

            if (depends.Length > 0)
            {
                for (int i = 0; i < depends.Length; i++)
                    DoParseAsset(depends[i]);
            }
            else
            {
                DoParseAsset(assetPath);
            }
        }

        /// <summary>
        /// 设置需要生成AB的目录
        /// </summary>
        /// <param name="reBuildShaderVariant"></param>
        static void SearchAllResource()
        {
            _dictBundles.Clear();
            _dictTmpRecord.Clear();
            _dictTmpCount.Clear();
            EditorUtility.DisplayProgressBar("Set GameRes AssetBundleName", "Please wait...设置Resource目录", 1f);
            
            //加载AssetBundleScriptableObject
            var assetBundleSobj = AssetDatabase.LoadAssetAtPath<AssetBundleSobjConfig>(
                "Assets/Game/GameInit/InitFramework/Editor/AssetBundle/AssetBundleScriptableObject.asset");

            #region 无需要搜索依赖目录
            string ResourcePath = AssetsRoot + "Res/";
            CollectBundleBuildWithoutDependencies(ResourcePath + "Config", true);
#if USE_ILRUNTIME
            CollectBundleBuildWithoutDependencies(ResourcePath + "HotFixDll");
#endif
            CollectBundleBuildWithoutDependencies(ResourcePath + "Fonts", false, true, "font");
            CollectBundleBuildWithoutDependencies(ResourcePath + "Atlas");
            CollectBundleBuildWithoutDependencies(ResourcePath + "Sound", true);
            
            //CollectBundleBuildWithoutDependencies(ResourcePath + "Chessboard", true, true, "Chessboard");
            //CollectBundleBuildWithoutDependencies(ResourcePath + "Difficulty", true, true, "Difficulty");
            //CollectBundleBuildWithoutDependencies(ResourcePath + "Texture", true, true, "Texture");
            //CollectBundleBuildWithoutDependencies(ResourcePath + "Mesh", true, true, "Mesh");
            //CollectBundleBuildWithoutDependencies(ResourcePath + "GameLevelConfig", true);
            //CollectBundleBuildWithoutDependencies(ResourcePath + "MapConfig", true);
            //CollectBundleBuildWithoutDependencies(ResourcePath + "Material", true, true, "Material");
            
            //AssetBundleScriptableObject->CWithoutDependencies
            if (assetBundleSobj != null)
            {
                for (int i = 0; i < assetBundleSobj.WithoutDList.Count; i++)
                {
                    var cwd = assetBundleSobj.WithoutDList[i];
                    CollectBundleBuildWithoutDependencies(ResourcePath + cwd.Name, cwd.Recursive, cwd.BlInOneBundle, cwd.InOneBundleName);
                }
            }

            //shader
            CollectShaders();
            #endregion

            ////UI目录
            var dirs = Directory.GetDirectories("Assets/Arts/UI");
            foreach (var dir in dirs)
            {
                var d = dir.Replace("Assets/", "").Replace('\\', '/');
                CollectBundleBuildWithoutDependencies(dir, true, true, d);
            }

            #region 非场景prefab资源搜索
            CollectBundleBuildWithDependencies(ResourcePath + "UI", true);
            CollectBundleBuildWithDependencies(ResourcePath + "Effect", true);
            CollectBundleBuildWithDependencies(ResourcePath + "GamePlay", true);
            
            //CollectBundleBuildWithDependencies(ResourcePath + "Model", true);
            //CollectBundleBuildWithDependencies(ResourcePath + "Bullet", true);
            //CollectBundleBuildWithDependencies(ResourcePath + "Monster", true);
            //CollectBundleBuildWithDependencies(ResourcePath + "Dogs", true);
            
            //AssetBundleScriptableObject->CWithDependencies
            if (assetBundleSobj != null)
            {
                for (int i = 0; i < assetBundleSobj.WithDList.Count; i++)
                {
                    var cwd = assetBundleSobj.WithDList[i];
                    CollectBundleBuildWithDependencies(ResourcePath + cwd.Name, cwd.Recursive);
                }
            }
            
            #endregion

            EditorUtility.ClearProgressBar();
        }

        static void CollectShaders()
        {
            List<string> allShader = new List<string>();
            List<string> tmpList = new List<string>();
            
            ////Spine shaders
            //FileUtil.RecursiveDirectory(AssetsRoot + "ThirdParty/Framework/Spine/Runtime/spine-unity/Shaders", tmpList, true, ".meta", ".cginc");
            //allShader.AddRange(tmpList);

            List<string> allAssetPathShaders = new List<string>();
            string assetPath;
            foreach (var file in allShader)
            {
                assetPath = "Assets/" + file.Replace(AssetsRoot, string.Empty);
                allAssetPathShaders.Add(assetPath);
            }
            AddBundleAssetFile(FormatPath2Name("Assets/Arts/GameShaders"), allAssetPathShaders);
        }
        
        /// <summary>
        /// 将文件路径转为AB名字
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string FormatPath2Name(string path)
        {
            var abName = path.Replace('/', '$').Replace('.', '$').ToLower();
            return abName;
        }
        #endregion

        #region 测试AssetBundle加密
        //[MenuItem("Tools/AssetBundle/Test Build AB")]
        static void TestBuildAB()
        {
            string path = Application.dataPath + "/../AssetBundles/Android/GameRes/";
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] fileInfos = dir.GetFiles("*.*");
            string[] tmp;
            string newFile;
            foreach (var file in fileInfos)
            {
                tmp = file.FullName.Split('_');
                if(tmp.Length== 1)
                    continue;
                if (tmp.Length > 2)
                    newFile = file.FullName.Replace("_" + tmp[tmp.Length - 1], "");
                else
                    newFile = tmp[0];
                File.Copy(file.FullName, newFile);
                File.Delete(file.FullName);
            }
        }

        [MenuItem("Kunggame/AssetBundle/Revert AB FileName",false, 11)]
        static void RevertAbFileName()
        {
            string finallyAssetPath = ABFinally_Path + AssetBundleConst.AssetsDirectory;
            List<string> allFiles = new List<string>();
            FileUtil.RecursiveDirectory(finallyAssetPath, allFiles, false, ".DS_Store");
            string fileFullName;
            string newFileName;
            
            for (int i = allFiles.Count - 1; i >=0 ; i--)//int i = 0; i < allFiles.Count; i++)
            {
                fileFullName = allFiles[i];
                if(!fileFullName.Contains("_"))
                    continue;
                int startIndex = fileFullName.LastIndexOf("_");
                string verstr = fileFullName.Substring(startIndex, fileFullName.Length - startIndex);
                newFileName = fileFullName.Replace(verstr, "");
                if(File.Exists(newFileName))
                    continue;
                File.Move(fileFullName, newFileName);
            }
        }
        #endregion
    }
}