namespace Framework.Editor
{
    public static class AutoBuildTemplate
    {
        public static string DialogTemplate =
            @"using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class #ClassName#View : AlertDialogView
    {
#Member#

        public #ClassName#View()
            : base(#ResName#)
        {

        }

        protected override void ParseComponent()
        {
#Find#
        }
    }
}
";
        public static string WindowTemplate =
            @"using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class #ClassName#Window : UIWindowBase
    {
#Member#

        public #ClassName#Window()
        {
            _uiName = #ResName#;
        }

        protected override void ParseComponent()
        {
#Find#
        }
    }
}
";

        public static string SubViewTemplate =
            @"using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class #ClassName# : UIBaseView
    {
#Member#

        protected override void ParseComponent()
        {
#Find#
        }
    }
}
";
        
        public static string ListViewTemplate =
            @"using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class #ClassName# : ListView
    {
#Member#

        protected override void ParseComponent()
        {
#Find#
        }
    }
}
";

        public static string ListViewItemTemplate = 
            @"using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class #ClassName# : ListItemBase
    {
#Member#

        protected override void ParseComponent()
        {
#Find#
        }

        public override ListItemBase Clone(GameObject instance)
        {
            var item = new #ClassName#();
            item.SetDisplayObject(instance);
            return item;
        }
    }
}
";
    }
}