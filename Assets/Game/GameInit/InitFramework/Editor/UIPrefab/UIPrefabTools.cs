using Cysharp.Text;

namespace Framework.Editor
{
    using UnityEngine;
    using UnityEditor;
    using System.Linq;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Logger = GameInit.Framework.Logger;


    public static class UIPrefabTools
    {
        internal enum NodeType
        {
            SubView,
            SubViewItem,
            ListView,
            ListViewItem,
            Normal,
        }

        internal class Node
        {
            internal readonly GameObject _gameObject;
            internal string _path;
            internal NodeType _nodeType = NodeType.Normal;
            internal List<Node> Children { get; } = new List<Node>();

            internal Node(GameObject obj)
            {
                _gameObject = obj;
            }

            internal Node AddChildren(Node node)
            {
                if (!Children.Contains(node))
                {
                    Children.Add(node);
                }

                return this;
            }
        }


        private static readonly Dictionary<string, string> ExportTypes = new Dictionary<string, string>()
        {
            {"E_Image", "Image"},
            {"E_Button", "Button"},
            {"E_Text", "Text"},
            {"E_Transform", "Transform"},
            {"E_RectTransform", "RectTransform"},
            {"E_ScrollRect", "ScrollRect"},
            {"E_GameObject", "GameObject"},
            {"E_Toggle", "Toggle"},
            {"E_Input", "InputField"},
            {"E_SubView", "SubView"},
            {"E_SubView_Item", "GameObject"},
            {"E_ListView", "GameObject"},
            {"E_ListView_Item", "GameObject"}
        };

        //自动生成目录
        private static readonly string ExportWindowPathRoot =
            Application.dataPath + "/GameLogic/Hotfix/AutoGenUI/UIWindow/{0}/";

        private static readonly string ExportDialogPathRoot =
            Application.dataPath + "/GameLogic/Hotfix/AutoGenUI/UIDialog/{0}/";

        private static void FindParentNode(GameObject root, Node node, string parentPath = "")
        {
            for (var i = 0; i < root.transform.childCount; i++)
            {
                var child = root.transform.GetChild(i);
                var nodeName = string.IsNullOrEmpty(parentPath) ? child.name : parentPath + "/" + child.name;
                if (ExportTypes.Keys.Contains(child.tag))
                {
                    var childNode = new Node(child.gameObject);
                    node.AddChildren(childNode);
                    childNode._path = nodeName;

                    if (child.tag == "E_SubView" || child.tag == "E_SubView_Item" || child.tag == "E_ListView_Item" ||
                        child.tag == "E_ListView")
                    {
                        switch (child.tag)
                        {
                            case "E_SubView":
                                childNode._nodeType = NodeType.SubView;
                                break;
                            case "E_ListView":
                                childNode._nodeType = NodeType.ListView;
                                break;
                            case "E_ListView_Item":
                                childNode._nodeType = NodeType.ListViewItem;
                                break;
                            default:
                                childNode._nodeType = NodeType.SubViewItem;
                                break;
                        }

                        nodeName = "";
                        FindParentNode(child.gameObject, childNode, nodeName);
                        continue;
                    }
                }

                FindParentNode(child.gameObject, node, nodeName);
            }
        }

        private static string _exportWindowRootPath;

        private static void ExportScript(GameObject gameObject, string path, bool isWindowUI)
        {
            if (gameObject == null)
            {
                Logger.LogError("UIPrefabTools.ExportScript() => 生成脚本失败，选择的GameObject为空!!!!");
                return;
            }

            var root = new Node(gameObject);
            FindParentNode(gameObject, root);
            if (root.Children.Count == 0)
                return;
            var exportWindowName = "";
            // if (isWindowUI)
            // {
            //     exportWindowName = gameObject.name.Replace("ui", "UI");
            // }
            // else
            // {
            //     exportWindowName = gameObject.name.Replace("ui", "");
            // }
            //
            var name = gameObject.name;
            if (name.StartsWith("ui"))
            {
                exportWindowName = isWindowUI ? "UI" + name.Substring(2) : name.Substring(2);
            }
            else
            {
                exportWindowName = gameObject.name;
            }            
            _exportWindowRootPath = ZString.Format(path, exportWindowName);
            if (Directory.Exists(_exportWindowRootPath))
                Directory.Delete(_exportWindowRootPath, true);
            Directory.CreateDirectory(_exportWindowRootPath);

            GenerateTemplate(root, out var members, out var findStr);
            var windowTemplate = string.Empty;
            switch (gameObject.tag)
            {
                case "E_SubView":
                    windowTemplate = AutoBuildTemplate.SubViewTemplate.Replace("#Member#", members);
                    windowTemplate = windowTemplate.Replace("#Find#", findStr);
                    windowTemplate = windowTemplate.Replace("#ClassName#", exportWindowName);
                    WriteFile(_exportWindowRootPath + exportWindowName + "_UI.cs", windowTemplate);
                    break;
                default:
                    if (isWindowUI)
                    {
                        windowTemplate = AutoBuildTemplate.WindowTemplate.Replace("#Member#", members);
                        windowTemplate = windowTemplate.Replace("#Find#", findStr);
                        windowTemplate = windowTemplate.Replace("#ClassName#", exportWindowName);
                        windowTemplate = windowTemplate.Replace("#ResName#", "\"" + gameObject.name + "\"");
                        WriteFile(_exportWindowRootPath + exportWindowName + "Window_UI.cs", windowTemplate);
                    }
                    else
                    {
                        windowTemplate = AutoBuildTemplate.DialogTemplate.Replace("#Member#", members);
                        windowTemplate = windowTemplate.Replace("#Find#", findStr);
                        windowTemplate = windowTemplate.Replace("#ClassName#", exportWindowName);
                        windowTemplate = windowTemplate.Replace("#ResName#", "\"" + gameObject.name + "\"");
                        WriteFile(_exportWindowRootPath + exportWindowName + "View_UI.cs", windowTemplate);
                    }
                    break;
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Logger.LogMagenta("脚本导出完成");
        }

        private static void WriteFile(string file, string fileContent)
        {
            //文件写入
            FileStream fileStream;
            if (!File.Exists(file))
            {
                fileStream = File.Create(file);
            }
            else
            {
                fileStream = File.Open(file, FileMode.Truncate);
            }

            StreamWriter writer = new StreamWriter(fileStream, Encoding.UTF8);
            writer.Write(fileContent);
            writer.Close();
        }

        private static void GenerateTemplate(Node root, out string members, out string findStr)
        {
            members = "\t\t";
            members = "\t\t";
            findStr = "\t\t\t";
            for (var i = 0; i < root.Children.Count; i++)
            {
                var node = root.Children[i];
                var typeStr = "";
                var name = "_" + node._gameObject.name[0].ToString().ToLower() + node._gameObject.name.Substring(1);

                if (node._nodeType != NodeType.Normal)
                {
                    GenerateTemplate(node, out var subMembers, out var subFindStr);

                    var subViewStr = string.Empty;
                    switch (node._nodeType)
                    {
                        case NodeType.ListView:
                            subViewStr = AutoBuildTemplate.ListViewTemplate.Replace("#Member#", subMembers);
                            break;
                        case NodeType.ListViewItem:
                            subViewStr = AutoBuildTemplate.ListViewItemTemplate.Replace("#Member#", subMembers);
                            break;
                        default:
                            subViewStr = AutoBuildTemplate.SubViewTemplate.Replace("#Member#", subMembers);
                            break;
                    }

                    subViewStr = subViewStr.Replace("#Find#", subFindStr);
                    subViewStr = subViewStr.Replace("#ClassName#", node._gameObject.name);

                    WriteFile(_exportWindowRootPath + node._gameObject.name + "_UI.cs", subViewStr);

                    if (node._nodeType == NodeType.SubViewItem)
                    {
                        typeStr = ExportTypes[node._gameObject.tag];
                        findStr += name + " = " + "Find(\"" + node._path + "\");\n\t\t\t";
                    }
                    else
                    {
                        typeStr = node._gameObject.name;
                        findStr += name + " = new " + typeStr + "();\n\t\t\t" + name + ".SetDisplayObject(Find(\"" +
                                   node._path + "\"));\n\t\t\t";
                    }
                }
                else
                {
                    typeStr = ExportTypes[node._gameObject.tag];
                    if (typeStr == "GameObject")
                        findStr += name + " = " + "Find(\"" + node._path + "\");\n\t\t\t";
                    else
                        findStr += name + " = " + "Find<" + typeStr + ">(\"" + node._path + "\");\n\t\t\t";
                }

                members += "private " + typeStr + " " + name + ";\n\t\t";
            }
        }

        [MenuItem("Assets/Kunggame/UI脚本导出/生成UIWindow Prefab脚本", false, 15)]
        [MenuItem("Kunggame/UI脚本导出/生成UIWindow Prefab脚本", false, 15)]
        private static void CreateScript()
        {
            ExportScript(Selection.activeGameObject, ExportWindowPathRoot, true);
        }

        [MenuItem("Assets/Kunggame/UI脚本导出/生成UIDialog Prefab脚本", false, 15)]
        [MenuItem("Kunggame/UI脚本导出/生成UIDialog Prefab脚本", false, 15)]
        private static void CreateDialogScript()
        {
            ExportScript(Selection.activeGameObject, ExportDialogPathRoot, false);
        }
    }
}