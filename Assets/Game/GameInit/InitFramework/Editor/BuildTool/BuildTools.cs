﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace Framework.Editor
{
    public static class BuildTools
    {
        #region Android 相关接口
        [MenuItem("Kunggame/Build/Android/Build Debug APK", false, 20)]
        private static async void BuildDebugAndroidAPK()
        {
            await BuildCommonResource();
            BuildAndroid(true);
        }
        
        [MenuItem("Kunggame/Build/Android/Build Release APK", false, 20)]
        private static async void BuildReleaseAndroidAPK()
        {
            await BuildCommonResource();
            BuildAndroid();
        }

        [MenuItem("Kunggame/Build/Android/Build Android AAB", false, 20)]
        private static async void BuildAndroidAAB()
        {
            await BuildCommonResource();            
            //生成aab
            await BuildAndroid(false, true);
            //生成apk
            // await BuildAndroid();
        }


        private static UniTask BuildAndroid(bool isDebug = false, bool isAAB = false)
        {
            PlayerSettings.SplashScreen.show = false;
            EditorUserBuildSettings.development = false;
            EditorUserBuildSettings.buildAppBundle = isAAB;
           // Logger.Log("GetScriptingDefineSymbolsForGroup " + PlayerSettings.GetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android));
            // 设定打包的keystore
            PlayerSettings.Android.keystoreName = Application.dataPath + "/../KeyStore/BalloonBubble3D.keystore";
            PlayerSettings.Android.keystorePass = "kunggame.cn023";
            PlayerSettings.Android.keyaliasName = "kunggame";
            PlayerSettings.Android.keyaliasPass = "kunggame.cn023";

            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
            PlayerSettings.Android.targetArchitectures = isDebug ? AndroidArchitecture.ARMv7 : AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

            var BuildVersion = (int.Parse(DateTime.Now.ToString("yyMMddHH"))).ToString();
            EditorUserBuildSettings.androidBuildSystem = AndroidBuildSystem.Gradle;
            // PlayerSettings.Android.blitType = AndroidBlitType.Always;
            PlayerSettings.Android.bundleVersionCode = int.Parse(BuildVersion);

            // 因为移动了资源文件夹，刷新资源
            AssetDatabase.Refresh();
            //设定包名
            string suffix = isDebug ? "Debug" : "Release";
            string toPath = string.Format(PlayerSettings.productName + "_"
                                          + PlayerSettings.bundleVersion + "_"
                                          + BuildVersion + "_"
                                          + suffix
                                          + ".apk");
            // try catch为了让逻辑能走完
            try
            {
                BuildPipeline.BuildPlayer(GetBuildScenes(), toPath, BuildTarget.Android, BuildOptions.None);
            }
            catch (Exception e)
            {
                Logger.LogError("BuildTools.BuildAndroid() => 打包失败,Error:" + e.Message);
            }

            return UniTask.CompletedTask;
        }
        #endregion
        
        #region 生成公用资源

        static string[] GetBuildScenes()
        {
            List<string> names = new List<string>();

            foreach (EditorBuildSettingsScene e in EditorBuildSettings.scenes)
            {
                if (e == null)
                    continue;
                if (e.enabled)
                    names.Add(e.path);
            }
            return names.ToArray();
        }
        
        private static async UniTask BuildCommonResource()
        {
            EditorUtility.DisplayProgressBar("Build Apk", "generator common resource, please waite..", 1f);
            await ParseConfigWindow.BuildAllConfig();
            await SpriteAtlasEditor.BuildAllAtlas();
#if UNITY_ANDROID
            await AssetBundleTool.BuildAndroidBundle();
#else
            await AssetBundleTool.BuildIOSBundle();
#endif
            EditorUtility.ClearProgressBar();
        }

        #endregion
    }
}