﻿using System;
using UnityEngine;

namespace Framework.Editor
{
    public class Cache
    {
        private string _sourceFolder = "Assets"; 
        // 脚本路径
        private static string _dbOutputFolder = "Assets/GameLogic/Hotfix/Config";
        private static string _configOutputFolder = "Assets/GameLogic/Hotfix/Config";
        private static string _configOutputFolder_Game = "Assets/Game/GameInit/Game/GameCommon/Config/Cfgs";
        private static string _defOutputFolder_Game = "Assets/Game/GameInit/Game/GameCommon/Config/Defs";
        
        // 数据资源路径
        private static string _assetOutputFolder = "Assets/Res/Config";
        public static string _assetOutPutJsonFolder = "Assets/Res/JsonConfig";
        
        // 配置脚本文件夹
        public static string ConfigOutputFolder => _configOutputFolder;
        // 配置字段脚本文件夹
        public static string DefOutputFolder => _dbOutputFolder + "/Def"; 
        // 配置数据文件夹
        public static string AssetOutputFolder => _assetOutputFolder;
        // json数据文件夹
        public static string AssetOutPutJsonFolder => _assetOutPutJsonFolder;
        
        // 非热更配置脚本文件夹
        public static string ConfigOutputFolder_Game => _configOutputFolder_Game;
        // 非热更配置字段脚本文件夹
        public static string DefOutputFolder_Game => _defOutputFolder_Game;

    }
}