﻿using System.Collections.Generic;
using System.IO;
using GameInit.Framework;

namespace Framework.Editor
{
    /// <summary>
    /// Config解析器，负责将txt解析成*.cs
    /// </summary>
    public class SheetGenerator
    {
        private static string templeteUsing =
            @"using GameInit.Framework;
";
        private static string templeteNamespace =
    @"
namespace /*NamespaceName*/
{
/*ClassCode*/
}
";

        private static string templete =
    @"[System.Serializable]
    public class /*ClassName*/
    {
    /*DeclareProperties*/

    /*DeclareFunctions*/
    }
    ";
        private static string templete2 =
    @"    /// <summary>
        /// {0}
        /// </summary>
        public {1} {2};

    ";
        /// <summary>
        /// 生成二进制解析Def脚本
        /// </summary>
        public static void GenerateBinaryDef(List<SheetSource> sheets, string outputFolder)
        {
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            if (!outputFolder.EndsWith("/"))
            {
                outputFolder += "/";
            }



            foreach (SheetSource src in sheets)
            {
                Logger.LogMagenta("表：" + src.className);
                string idType = src.GetType(0);
                string idField = src.GetField(0);

                // 属性声明
                string properties = "";

                for (int x = 0; x < src.column; x++)
                {
                    string comment = src.GetComment(x);
                    string csType = src.GetType(x);
                    string field = src.GetField(x);
                    if (!src.IsClient(x))
                        continue;
                    if (string.IsNullOrEmpty(field))
                    {
                        UnityEngine.Debug.LogError("第" + (x + 1) + "列字段是空，跳过解析此列！");
                        continue;
                    }
                    string declare = string.Format(templete2, comment, csType, field);
                    properties += declare;
                }

                // 函数声明
                string paramName1 = "bytes";
                string paramName2 = "byteIndex";
                string fucntions = "    public void Parse(byte[] " + paramName1 + ", ref int " + paramName2 + ")\n\t\t{\n";
                for (int x = 0; x < src.column; x++)
                {
                    string comment = src.GetComment(x);
                    string csType = src.GetType(x);
                    string field = src.GetField(x);
                    if (!src.IsClient(x))
                        continue;
                    if (string.IsNullOrEmpty(field))
                    {
                        UnityEngine.Debug.LogError("第" + (x + 1) + "列字段是空，跳过解析此列！");
                        continue;
                    }
                    string declare = ConfigTools.SourceValue2ParseFunction(csType, field, paramName1, "ref " + paramName2);
                    fucntions += ("\t\t\t" + declare + "\n");
                }
                fucntions += "\n\t\t}";
                // 替换
                string content = templete;
                content = content.Replace("[System.Serializable]", "");
                content = content.Replace("/*ClassName*/", src.className);
                content = content.Replace("/*DeclareProperties*/", properties);
                content = content.Replace("/*DeclareFunctions*/", fucntions);
                content = content.Replace("/*IDType*/", idType);
                content = content.Replace("/*IDField*/", idField);

                // 自定义命名空间
                if (!string.IsNullOrEmpty(ConfigConstants.NAMESPACE_NAME))
                {
                    string oldContent = content;
                    content = templeteNamespace;
                    content = content.Replace("/*NamespaceName*/", ConfigConstants.NAMESPACE_NAME);
                    content = content.Replace("/*ClassCode*/", oldContent);
                }

                content = templeteUsing + content;
                // 写入
                string outputPath = outputFolder + src.className + ".cs";
                ConfigTools.WriteFile(outputPath, content);
            }
        }
    }
}

