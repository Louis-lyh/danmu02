﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using LitJson;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UnityEditor;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace Framework.Editor
{
    public class ParseConfigWindow : EditorWindow
    {
        private static string CONFIG_PATH_KEY;
        private static string _path;
        private static string _dirFullName;

        private static string ExcelPath
        {
            get
            {
                if (!_path.IsValid())
                {
                    CONFIG_PATH_KEY = Application.productName + "_Config";
                    _path = PlayerPrefs.GetString(CONFIG_PATH_KEY, "");
                }

                return _path;
            }
            set
            {
                if (_path == value)
                    return;
                _path = value;
                PlayerPrefs.SetString(CONFIG_PATH_KEY, _path);
                PlayerPrefs.Save();
            }
        }

        [MenuItem("Kunggame/配置表工具/一键生成Config", false, 13)]
        public static UniTask BuildAllConfig()
        {
            if (!ExcelPath.IsValid())
            {
                EditorUtility.DisplayDialog("导表出错", "Excel目录为空，请通过 \"Kunggame->配置表工具->导表设置\" 设置表目录", "ok");
                return UniTask.CompletedTask;
            }

            var dir = new DirectoryInfo(ExcelPath);
            var fileInfos = dir.GetFiles("*.xlsx", SearchOption.AllDirectories); //获取文件列表
            if (fileInfos.Length == 0)
                return UniTask.CompletedTask;
            var files = new List<string>();
            _dirFullName = dir.FullName;
            foreach (var fileInfo in fileInfos)
            {
                if (fileInfo.Name.StartsWith("~"))
                    continue;
                files.Add(fileInfo.FullName);
            }

            // 如果输出文件夹不存在，先创建
            if (!Directory.Exists(Cache.AssetOutputFolder))
                Directory.CreateDirectory(Cache.AssetOutputFolder);
            if (!Directory.Exists(Cache.AssetOutPutJsonFolder))
                Directory.CreateDirectory(Cache.AssetOutPutJsonFolder);

            // 获取源
            var sheets = new List<SheetSource>(); // 表格
            foreach (var file in files)
                ReadSourceFile(file, ref sheets);
            if (sheets.Count == 0)
                return UniTask.CompletedTask;
            BinaryExport(sheets);
            Serialize(sheets);
            AssetDatabase.Refresh();
            return UniTask.CompletedTask;
        }

        #region GUI操作

        [MenuItem("Kunggame/配置表工具/导表设置", false, 13)]
        public static ParseConfigWindow OpenParseWindow()
        {
            var window = GetWindow<ParseConfigWindow>("配置表工具");
            window.minSize = new UnityEngine.Vector2(500, 100);
            window.maxSize = new UnityEngine.Vector2(500, 100);
            return window;
        }

        void OnGUI()
        {
            GUILayout.Space(18);
            GUILayout.BeginHorizontal();
            GUILayout.Label("选择excel路径");
            GUILayout.TextField(ExcelPath, GUILayout.Width(300));
            if (GUILayout.Button("选择路径", GUILayout.Width(80)))
            {
                var path = EditorUtility.OpenFolderPanel("Excel根目录:", ExcelPath, "");
                if (path.IsValid())
                    ExcelPath = path;
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(10);

            GUILayout.BeginHorizontal();
            GUILayout.Space(150);
            if (GUILayout.Button("一键导表", GUILayout.Width(200), GUILayout.Height(40)))
            {
                BuildAllConfig();
            }

            GUILayout.EndHorizontal();
            GUILayout.Space(30);
        }

        #endregion

        /// <summary>
        /// 导出配置文件
        /// </summary>
        private static void BinaryExport(List<SheetSource> sheets)
        {
            // 生成def脚本
            if (sheets.Count > 0)
                SheetGenerator.GenerateBinaryDef(sheets, Cache.DefOutputFolder); // 生成Sheets
            // 生成Config
            BinaryConfigGenerator.Generate(sheets, Cache.ConfigOutputFolder);
        }

        /// <summary>
        /// 序列化
        /// </summary>
        private static void Serialize(List<SheetSource> sheets)
        {
            byte[] bytes;
            // Sheet db
            foreach (var source in sheets)
            {
                Logger.LogBlue("source:" + source.className);
                try
                {
                    bytes = Serializer.Serialize2Bytes(source);
                    SaveBinaryFile(bytes, source);
                }
                catch (Exception e)
                {
                    Logger.LogError(source.className + " 导出出错");
                    throw;
                }

                // try
                // {
                //     Type type = Serializer.FindType(source.originalName + "Config");
                //     object t = Assembly.GetAssembly(type).CreateInstance(type.FullName);
                //     if (t != null)
                //     {
                //         var method = t.GetType().GetMethod("Deserialize");
                //         method.Invoke(t, new object[] {bytes});
                //         var getMethod = t.GetType().GetMethod("GetDefDic");
                //         var value = getMethod.Invoke(null, null);
                //         JsonWriter jw = new JsonWriter();
                //         jw.PrettyPrint = true;
                //         LitJson.JsonMapper.ToJson(value, jw);
                //         var json = ConfigTools.JsonFormat(jw.ToString());
                //         ConfigTools.WriteFile(Cache._assetOutPutJsonFolder + "/" + source.originalName + ".json", json);
                //     }
                // }
                // catch (Exception e)
                // {
                //     Logger.LogError("ParseConfigWindow.Serialize() => 写入json数据出错, message:" + e.Message);
                // }
            }

            AssetDatabase.Refresh();
        }

        private static void SaveBinaryFile(byte[] bytes, SheetSource source)
        {
            if (bytes.Length == 0)
            {
                Debug.LogError(source.originalName + "序列化失败。。。文件大小为0");
                return;
            }

            var filePath = "";
            var dir = Cache.AssetOutputFolder;
            if (source.relativePath.IsValid())
            {
                dir = Cache.AssetOutputFolder + "/" + source.relativePath;
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }

            filePath = dir + "/" + source.originalName + ".bytes";
            ConfigTools.WriteFile(filePath, bytes);
        }

        /// <summary>
        /// 读取源文件
        /// </summary>
        private static void ReadSourceFile(string sourcePath, ref List<SheetSource> sheets)
        {
            // 不存在
            if (!File.Exists(sourcePath))
            {
                //this.ShowNotification(new GUIContent("没有找到文件！"));
                return;
            }

            FileInfo file = new FileInfo(sourcePath);

            // 临时文件
            if (IsTemporaryFile(file.Name))
            {
                //this.ShowNotification(new GUIContent(file.Name + ": 该文件是临时文件！"));
                return;
            }

            // 检测文件类型
            OriginalType type;
            if (!TypeEnabled(file.Extension, out type))
            {
                //this.ShowNotification(new GUIContent(file.Name + ": 不可导出类型！"));
                return;
            }

            // 读数据流
            FileStream fileStream = file.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            ExcelWorksheet excelData = null;
            string content = "";

            // 读取Excel
            if (type == OriginalType.Xlsx)
            {
                var package = new ExcelPackage(fileStream);
                excelData = package.Workbook.Worksheets[1];

                fileStream.Close();
            }
            // 其他
            else
            {
                // 读Byte
                byte[] bytes;
                BinaryReader br = new BinaryReader(fileStream);
                bytes = br.ReadBytes((int) fileStream.Length);
                StreamReader renderer = new StreamReader(fileStream);
                content = renderer.ReadToEnd();

                ConfigTools.DetectTextEncoding(bytes, out content); // 转换不同的编码格式

                if (string.IsNullOrEmpty(content))
                {
                    //this.ShowNotification(new GUIContent(file.Name + ": 内容为空！"));
                    return;
                }
            }

            // parseFile
            SheetSource source;
            try
            {
                switch (type)
                {
                    case OriginalType.Txt:
                    case OriginalType.Csv:
                        source = SheetParser.Parse(content, file.Name);
                        sheets.Add(source);
                        break;
                    case OriginalType.Xlsx:
                        var relativePath = file.FullName.Replace(_dirFullName, "");
                        var lastIndex = relativePath.LastIndexOf(file.Name);
                        lastIndex = Mathf.Max(0, lastIndex - 1);
                        relativePath = relativePath.Substring(0, lastIndex);
                        source = SheetParser.Parse(excelData, file.Name, relativePath);
                        sheets.Add(source);
                        break;
                }
            }
            catch (Exception ex)
            {
                //this.ShowNotification(new GUIContent(file.Name + ": 解析失败，请检查格式是否正确！"));
                Debug.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// 是否是临时文件 like ~$Equip
        /// </summary>
        private static bool IsTemporaryFile(string fileName)
        {
            return Regex.Match(fileName, @"^\~\$").Success;
        }

        /// <summary>
        /// 检测类型是否启用
        /// </summary>
        private static bool TypeEnabled(string extension, out OriginalType type)
        {
            switch (extension)
            {
                case ".txt":
                    type = OriginalType.Txt;
                    return true;
                case ".csv":
                    type = OriginalType.Csv;
                    return true;
                case ".xlsx":
                case ".xls":
                case ".xlsm":
                    type = OriginalType.Xlsx;
                    return true;
            }

            type = OriginalType.Txt;
            return false;
        }
    }
}