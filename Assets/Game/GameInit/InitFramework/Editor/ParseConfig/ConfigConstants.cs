﻿using System;
using System.Globalization;
using System.Collections.Generic;

namespace Framework.Editor
{
    public static class ConfigConstants
    {
        public static readonly int ROW_COMMENT = 0; // 说明行
        public static readonly int ROW_ISCLIENT = 1; // 是否是客户端需要的字段
        public static readonly int ROW_TYPE = 2; // 类型行
        public static readonly int ROW_FIELD = 3; // 字段行

        public static readonly int ROW_NUM = 4; // 无效行数 用于序列化处理

        public static readonly char ARRAY_SPLIT_1 = '|'; // 一维数组分隔符
        public static readonly char ARRAY_SPLIT_2 = ';'; // 二维数组分隔符

        public static readonly string NAMESPACE_NAME = "GameHotfix.Game"; // 导出类的命名空间，空字符串不添加namespace

        public static readonly string[] EMPTY_VALUES = new string[1]
        {
            "-",
        };

        public static readonly NumberStyles NUMBER_STYPLES = NumberStyles.AllowCurrencySymbol |
                                                             NumberStyles.AllowExponent |
                                                             NumberStyles.AllowDecimalPoint |
                                                             NumberStyles.AllowParentheses |
                                                             NumberStyles.AllowTrailingSign |
                                                             NumberStyles.AllowLeadingSign |
                                                             NumberStyles.AllowTrailingWhite |
                                                             NumberStyles.AllowLeadingWhite;

        public static readonly Type OBJECT_ARRAY_TYPE = typeof(object[]);
        public static readonly Type OBJECT_DICTIONARY_TYPE = typeof(Dictionary<string, object>);
    }
}