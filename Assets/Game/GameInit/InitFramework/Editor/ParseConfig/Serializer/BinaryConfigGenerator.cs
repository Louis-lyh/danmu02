﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace Framework.Editor
{
    /// <summary>
    /// 用于生成解析器
    /// </summary>
    public class BinaryConfigGenerator
    {
        private static string templeteUsing =
    @"using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

";

        private static string templeteNamespace =
@"namespace /*NamespaceName*/
{
/*ClassCode*/
}
";

        private const string template_sheet =
    @"    public class /*ConfigName*/ : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            /*ClassName*/ def;
            for (int i = 0; i < defCount; i++)
            {
                def = new /*ClassName*/();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def./*IDField*/, def);
            }
        }
        
        /// <summary>
        /// 通过/*IDField*/获取/*ClassName*/的实例
        /// </summary>
        /// <param name=""/*IDField*/"">索引</param>
        public static /*ClassName*/ Get(/*IDType*/ /*IDField*/)
        {
            _defDic.TryGetValue(/*IDField*/, out /*ClassName*/ def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary</*IDType*/, /*ClassName*/> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary</*IDType*/, /*ClassName*/> _defDic = new Dictionary</*IDType*/, /*ClassName*/>();
    }
    ";

        public static void Generate(List<SheetSource> sheets, string outputFolder)
        {
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            if (!outputFolder.EndsWith("/"))
            {
                outputFolder += "/";
            }

            // sheets
            foreach (SheetSource sheet in sheets)
            {
                string configName = sheet.originalName + "Config";
                string outputPath = outputFolder + configName + ".cs";
                if (File.Exists(outputPath))
                {
                    continue;
                }

                string idType = sheet.GetType(0);
                string idField = sheet.GetField(0);

                string content = template_sheet;
                content = content.Replace("/*ConfigName*/", configName);

                content = content.Replace("/*SourceName*/", sheet.originalName);
                content = content.Replace("/*IDField*/", idField);

                content = content.Replace("/*ClassName*/", sheet.className);
                content = content.Replace("/*IDType*/", idType);

                // 自定义命名空间
                if (!string.IsNullOrEmpty(ConfigConstants.NAMESPACE_NAME))
                {
                    string oldContent = content;
                    content = templeteNamespace;
                    content = content.Replace("/*NamespaceName*/", ConfigConstants.NAMESPACE_NAME);
                    content = content.Replace("/*ClassCode*/", oldContent);
                }

                // 加入using
                content = templeteUsing + content;
                Debug.Log(outputPath + "|||" + content);
                ConfigTools.WriteFile(outputPath, content);
            }
        }
    }

}