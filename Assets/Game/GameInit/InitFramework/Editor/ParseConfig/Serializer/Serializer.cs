﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using ILRuntime.Runtime;
using System.Text;
using UnityEngine;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using Logger = GameInit.Framework.Logger;

namespace Framework.Editor
{
    /// <summary>
    /// 序列化器，通过反射的方式序列化出SerializableList
    /// </summary>
    public class Serializer
    {
        // public static object Serialize(StructSource source)
        // {
        //     Type t = FindType(source.dbName);
        //     if (t == null)
        //     {
        //         Debug.LogError("找不到DB类！");
        //         return null;
        //     }
        //
        //     object set = ScriptableObject.CreateInstance(t);
        //
        //     string fieldName = source.originalName;
        //
        //     //使用 FromJson 获取json对象
        //     Type jsonType = FindType(source.className);
        //     object jsonObj = SerializeObject(jsonType, source.obj);
        //
        //     FieldInfo fieldInfo = t.GetField(fieldName);
        //     fieldInfo.SetValue(set, jsonObj);
        //
        //     return set;
        // }

        /// <summary>
        /// 序列化结构对象
        /// </summary>
        /// <param name="inputObject"></param>
        /// <returns></returns>
        private static object SerializeObject(Type outputType, Dictionary<string, object> inputObject)
        {
            object obj = Activator.CreateInstance(outputType);

            foreach (string field in inputObject.Keys)
            {
                FieldInfo fieldInfo = outputType.GetField(field);
                Type fieldType = fieldInfo.FieldType; //生成类的类型

                object value = inputObject[field];
                Type type = value.GetType(); //解析的类型

                //对象
                if (type == typeof(Dictionary<string, object>))
                {
                    object subOjbect = SerializeObject(fieldType, (Dictionary<string, object>) value);
                    fieldInfo.SetValue(obj, subOjbect);
                }
                //数组
                else if (type.IsArray)
                {
                    object array = SerializeArray(fieldType, (object[]) value);
                    fieldInfo.SetValue(obj, array);
                }
                //String\Number\Bool
                else
                {
                    fieldInfo.SetValue(obj, value);
                }
            }

            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputType">FieldType</param>
        /// <param name="inputArray">object[]</param>
        /// <returns></returns>
        private static object SerializeArray(Type outputType, object[] inputArray)
        {
            //普通数组
            if (outputType.IsArray)
            {
                Type elementType = outputType.GetElementType();
                Array outputArray = Array.CreateInstance(elementType, inputArray.Length);

                if (inputArray[0].GetType() == typeof(Dictionary<string, object>))
                {
                    for (int i = 0; i < inputArray.Length; i++)
                    {
                        outputArray.SetValue(SerializeObject(elementType, (Dictionary<string, object>) inputArray[i]),
                            i);
                    }
                }
                else
                {
                    for (int i = 0; i < inputArray.Length; i++)
                    {
                        outputArray.SetValue(inputArray[i], i);
                    }
                }

                return outputArray;
            }
            //自定义数组
            else
            {
                object arrayObject = Activator.CreateInstance(outputType);
                for (int i = 0, l = inputArray.Length; i < l; i++)
                {
                    string field = "arg" + i;
                    FieldInfo fieldInfo = outputType.GetField(field);

                    //对象
                    if (inputArray[i].GetType() == typeof(Dictionary<string, object>))
                    {
                        object value = SerializeObject(fieldInfo.FieldType, (Dictionary<string, object>) inputArray[i]);
                        fieldInfo.SetValue(arrayObject, value);
                    }
                    //数组
                    else if (inputArray[i].GetType().IsArray)
                    {
                        fieldInfo.SetValue(arrayObject, SerializeArray(fieldInfo.FieldType, (object[]) inputArray[i]));
                    }
                    else
                    {
                        fieldInfo.SetValue(arrayObject, inputArray[i]);
                    }
                }

                return arrayObject;
            }
        }

        /// <summary>
        /// 根据类名查找类
        /// </summary>
        public static Type FindType(string qualifiedTypeName)
        {
            if (!string.IsNullOrEmpty(ConfigConstants.NAMESPACE_NAME))
            {
                qualifiedTypeName = ConfigConstants.NAMESPACE_NAME + "." + qualifiedTypeName;
            }

            Type t = Type.GetType(qualifiedTypeName);

            if (t != null)
            {
                return t;
            }
            else
            {
                foreach (Assembly asm in AppDomain.CurrentDomain.GetAssemblies())
                {
                    t = asm.GetType(qualifiedTypeName);
                    if (t != null)
                        return t;
                }

                return null;
            }
        }

        ///*****************************************二进制方式**********************************************************///
        public static byte[] Serialize2Bytes(SheetSource source)
        {
            return Source2ConfigBytes(source);
        }


        /// <summary>
        /// 从源数据反射为对应的配置数组
        /// </summary>
        /// <returns></returns>
        private static byte[] Source2ConfigBytes(SheetSource source)
        {
            ByteArray ba = new ByteArray();
            Type configType = FindType(source.className);

            int count = source.row - ConfigConstants.ROW_NUM;
            int dataCount = 0;

            for (int y = ConfigConstants.ROW_NUM, i = 0; i < count; y++, i++)
            {
                string idString = source.GetValue(y, 0);

                // 忽略id是空的数据
                if (string.IsNullOrEmpty(idString))
                {
                    continue;
                }

                dataCount++;
            }

            ba.WriteSignedInt(dataCount);

            for (int y = ConfigConstants.ROW_NUM, i = 0; i < count; y++, i++)
            {
                string idString = source.GetValue(y, 0);

                // 忽略id是空的数据
                if (string.IsNullOrEmpty(idString))
                {
                    continue;
                }

                for (int x = 0; x < source.column; x++)
                {
                    string comment = source.GetComment(x);
                    string valueType = source.GetType(x);
                    string valueField = source.GetField(x);
                    // 忽略字段是空的数据
                    if (string.IsNullOrEmpty(valueField))
                    {
                        continue;
                    }
                    if(!source.IsClient(x))
                        continue;

                    string valueString = source.GetValue(y, x);
                    bool isEmptyValue = ConfigTools.IsEmptyValue(valueString);

                    if (!isEmptyValue && (valueString.StartsWith(" ") || valueString.EndsWith(" ")))
                    {
                        Debug.LogError("id:" + idString + ", 字段:" + comment + ", 值:" + valueString +
                                       ", 开头或结尾有空格!");
                    }

                    string csharpType = ConfigTools.SourceBaseType2CSharpBaseType(valueType);
                    switch (csharpType)
                    {
                        case "bool":
                            ba.WriteBoolean(valueString == "1" || valueString == "true" || valueString == "True" ||
                                            valueString == "TRUE");
                            break;
                        case "byte":
                            ba.WriteUnsignedByte(isEmptyValue ? default(byte) : byte.Parse(valueString));
                            break;
                        case "ushort":
                            ba.WriteUnsignedShort(isEmptyValue ? default(ushort) : ushort.Parse(valueString));
                            break;
                        case "uint":
                            ba.WriteUnsignedInt(isEmptyValue ? default(uint) : uint.Parse(valueString));
                            break;
                        case "sbyte":
                            ba.WriteSignedByte(isEmptyValue ? default(sbyte) : sbyte.Parse(valueString));
                            break;
                        case "short":
                            ba.WriteSignedShort(isEmptyValue ? default(short) : short.Parse(valueString));
                            break;
                        case "int":
                            ba.WriteSignedInt(isEmptyValue ? default(int) : int.Parse(valueString));
                            break;
                        case "long":
                            ba.WriteLong(isEmptyValue ? default(long) : long.Parse(valueString));
                            break;
                        case "ulong":
                            ba.WriteUnsignedLong(isEmptyValue ? default(ulong) : ulong.Parse(valueString));
                            break;
                        case "float":
                            ba.WriteFloat(isEmptyValue ? default(float) : float.Parse(valueString));
                            break;
                        case "double":
                            ba.WriteDouble(isEmptyValue ? default(double) : double.Parse(valueString));
                            break;
                        case "string":
                            valueString = string.IsNullOrEmpty(valueString) ? "" : valueString;
                            valueString.Trim();
                            byte[] temp = Encoding.UTF8.GetBytes(valueString);
                            int len = temp.Length;
                            ba.WriteUnsignedShort((ushort) len);
                            ba.WriteBytes(temp);
                            break;
                        default:
                            Logger.Log("类型缺省:" + csharpType);
                            break;
                    }
                }
            }

            // return configs;
            return ba.Bytes;
        }
    }
}
