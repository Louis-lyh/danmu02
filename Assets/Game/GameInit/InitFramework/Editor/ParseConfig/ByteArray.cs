﻿using System;
using System.Text;
using UnityEngine;
using System.Collections.Generic;

namespace Framework.Editor
{
    public class ByteArray
    {
        private const int BYTE_LEN = 1;
        private const int USHORT_LEN = 2;
        private const int UINT_LEN = 4;
        private const int LONG_LEN = 8;
        private const int FLOAT_LEN = 4;
        private const int INT_LEN = 4;
        private const int SHORT_LEN = 2;
        private const int DOUBLE_LEN = 8;

        private int _length = 0;
        private List<byte> _byteList;

        private byte[] _ushortData = new byte[USHORT_LEN];
        private byte[] _uintData = new byte[UINT_LEN];
        private byte[] _intData = new byte[INT_LEN];
        private byte[] _shortData = new byte[SHORT_LEN];
        private byte[] _floatData = new byte[FLOAT_LEN];
        private byte[] _longData = new byte[LONG_LEN];

        private int _pointer = 0;

        public ByteArray()
        {
            _byteList = new List<byte>();
        }

        public void WriteBoolean(bool value)
        {
            _length = _length + BYTE_LEN;
            _byteList.Add(Convert.ToByte(value));
        }

        public void WriteUnsignedByte(byte value)
        {
            _length = _length + BYTE_LEN;
            _byteList.Add(Convert.ToByte(value));
        }

        public void WriteSignedByte(sbyte value)
        {
            _length = _length + BYTE_LEN;
            _byteList.Add(Convert.ToByte(value));
        }

        public void WriteUnsignedShort(ushort value)
        {
            _length = _length + USHORT_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteSignedShort(short value)
        {
            _length = _length + SHORT_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteUnsignedInt(uint value)
        {
            _length = _length + UINT_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteSignedInt(int value)
        {
            _length = _length + INT_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteUnsignedLong(ulong value)
        {
            _length = _length + LONG_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteLong(long value)
        {
            _length = _length + LONG_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteFloat(float value)
        {
            _length = _length + FLOAT_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteDouble(double value)
        {
            _length = _length + DOUBLE_LEN;
            byte[] temp = BitConverter.GetBytes(value);
            _byteList.AddRange(temp);
        }

        public void WriteUTFBytes(string value, int length = 0)
        {
            byte[] temp = Encoding.UTF8.GetBytes(value);
            if (length == 0)
            {
                _length = _length + temp.Length;
                _byteList.AddRange(temp);
            }
            else
            {
                _length = _length + length;
                for (int i = 0; i < length; i++)
                {
                    if (i < temp.Length)
                    {
                        _byteList.Add(temp[i]);
                    }
                    else
                    {
                        _byteList.Add(0);
                    }
                }
            }
        }

        public void WriteBytes(byte[] value, int length = 0)
        {
            if (length > 0)
            {
                _length = _length + length;

                for (int i = 0; i < length; i++)
                {
                    if (i < value.Length)
                    {
                        _byteList.Add(value[i]);
                    }
                    else
                    {
                        _byteList.Add(0);
                    }
                }
            }
            else
            {
                _length = _length + value.Length;
                _byteList.AddRange(value);
            }
        }

        public void WriteBytes(ByteArray value, int length = 0)
        {
            if (length > 0)
            {
                _length = _length + length;

                if (length < value._length)
                {
                    _byteList.AddRange(value._byteList.GetRange(value._pointer, length));
                }
                else
                {
                    _byteList.AddRange(value._byteList.GetRange(value._pointer, value._length));
                    for (int i = value._length, cnt = length; i < cnt; i++)
                    {
                        _byteList.Add(0);
                    }
                }
                value._length = value._length - length;
                value._pointer += length;
            }
            else
            {
                _length = _length + value._length;
                _byteList.AddRange(value._byteList.GetRange(value._pointer, value._length));
                value.Clear();
            }
        }

        public bool ReadBoolean()
        {
            if (_length < BYTE_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return false;
            }
            else
            {
                byte temp = _byteList[_pointer];
                _length = _length - BYTE_LEN;
                _pointer += BYTE_LEN;
                return Convert.ToBoolean(temp);
            }
        }

        public byte ReadUnsignedByte()
        {
            if (_length < BYTE_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                byte temp = _byteList[_pointer];
                _length = _length - BYTE_LEN;
                _pointer += BYTE_LEN;
                return Convert.ToByte(temp);
            }
        }

        public ushort ReadUnsignedShort()
        {
            if (_length < USHORT_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                _ushortData = _byteList.GetRange(_pointer, USHORT_LEN).ToArray();
                _length = _length - USHORT_LEN;
                _pointer += USHORT_LEN;

                return BitConverter.ToUInt16(_ushortData, 0);
            }
        }

        public uint ReadUnsignedInt()
        {
            if (_length < UINT_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                _uintData = _byteList.GetRange(_pointer, UINT_LEN).ToArray();

                _length = _length - UINT_LEN;
                _pointer += UINT_LEN;

                return BitConverter.ToUInt32(_uintData, 0);
            }
        }

        public int ReadSignedInt()
        {
            if (_length < INT_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                _intData = _byteList.GetRange(_pointer, INT_LEN).ToArray();

                _length = _length - INT_LEN;
                _pointer += INT_LEN;

                return BitConverter.ToInt32(_intData, 0);
            }
        }

        public short ReadSignedShort()
        {
            if (_length < SHORT_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                _shortData = _byteList.GetRange(_pointer, SHORT_LEN).ToArray();
                _length = _length - SHORT_LEN;
                _pointer += SHORT_LEN;

                return BitConverter.ToInt16(_shortData, 0);
            }
        }

        public float ReadFloat()
        {
            if (_length < FLOAT_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0f;
            }
            else
            {
                _floatData = _byteList.GetRange(_pointer, FLOAT_LEN).ToArray();

                _length = _length - FLOAT_LEN;
                _pointer += FLOAT_LEN;

                return BitConverter.ToSingle(_floatData, 0);
            }
        }

        public ulong ReadUnsignedLong()
        {
            if (_length < LONG_LEN)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return 0;
            }
            else
            {
                _longData = _byteList.GetRange(_pointer, LONG_LEN).ToArray();

                _length = _length - LONG_LEN;
                _pointer += LONG_LEN;

                return BitConverter.ToUInt64(_longData, 0);
            }
        }

        public static bool Test = false;

        public string ReadUTFByte(int length)
        {
            if (_length < length)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
                return "";
            }
            else
            {
                var tempList = _byteList.GetRange(_pointer, length);
                byte[] temp = tempList.ToArray();

                _length = _length - length;
                _pointer += length;

                int zeroPos = tempList.IndexOf(0);
                int removeCount = zeroPos >= 0 ? length - zeroPos : 0;
                return Encoding.UTF8.GetString(temp, 0, temp.Length - removeCount);
            }
        }

        public void ReadBytes(byte[] value, int length = 0)
        {
            if (_length < length)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
            }
            else
            {
                if (length > 0)
                {
                    for (int i = 0; i < length; i++)
                    {
                        value[i] = _byteList[_pointer + i];
                    }

                    _length = _length - length;
                    _pointer += length;
                }
                else
                {
                    for (int i = 0; i < _length; i++)
                    {
                        value[i] = _byteList[_pointer + i];
                    }
                    Clear();
                }
            }
        }

        public void ReadBytes(ByteArray value, int length = 0)
        {
            if (_length < length)
            {
                Debug.LogException(new Exception("ByteArray == Not Enough Data Length"));
            }
            else
            {
                if (length > 0)
                {
                    value._length = value._length + length;


                    value._byteList.AddRange(_byteList.GetRange(_pointer, length).ToArray());
                    _length = _length - length;
                    _pointer += length;
                }
                else
                {
                    value._length = value._length + _length;
                    value._byteList.AddRange(_byteList.GetRange(_pointer, _length));
                    Clear();
                }
            }
        }

        public byte[] Bytes
        {
            get { return _byteList.ToArray(); }
        }

        public int Length
        {
            get { return _length; }

            set
            {
                _length = value;
                int cnt = _byteList.Count - _pointer;
                if (cnt > _length)
                {
                    _pointer = _byteList.Count - _length;
//                _byteList.RemoveRange(_length, _byteList.Count - _length);
                }
                else if (cnt < _length)
                {
                    while (_byteList.Count - _pointer < _length)
                    {
                        _byteList.Add(0);
                    }
                }
            }
        }

        public void Clear()
        {
            _pointer = 0;
            _length = 0;
            _byteList.Clear();
        }

        public ByteArray Clone()
        {
            ByteArray temp = new ByteArray();
            temp._pointer = _pointer;
            temp._length = _length;
            temp._byteList.AddRange(_byteList);
            return temp;
        }

        public static ByteArray Get()
        {
            if (_cache.Count > 0)
                return _cache.Dequeue();
            return new ByteArray();
        }

        public static void Cache(ByteArray byteArr)
        {
            byteArr.Clear();
            _cache.Enqueue(byteArr);
        }

        private static Queue<ByteArray> _cache = new Queue<ByteArray>();
    }

}