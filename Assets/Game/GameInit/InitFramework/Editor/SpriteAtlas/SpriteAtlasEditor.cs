using Cysharp.Threading.Tasks;

namespace Framework.Editor
{
    using UnityEditor;
    using UnityEngine;
    using System.IO;
    using System.Collections.Generic;
    using UnityEngine.U2D;
    using UnityEditor.U2D;
    using System.Reflection;
    using GameInit.Framework;
    using Logger = GameInit.Framework.Logger;
    public static class SpriteAtlasEditor
    {
        /// <summary>
        /// 图集所在图片的文件夹路径
        /// </summary>
        private const string ATLAS_PATH = "Assets/Arts/UI/";
        /// <summary>
        /// 图集输出路径
        /// </summary>
        private const string OUTPUT_PATH = "Assets/Res/Atlas/{0}.spriteAtlas";

        private static string DataPathToAssetPath(string path)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
                return path.Substring(path.IndexOf("Assets\\"));
            else
                return path.Substring(path.IndexOf("Assets/"));
        }
        [MenuItem("Assets/Kunggame/生成所有图集", false, 14)]
        [MenuItem("Kunggame/UI Atlas/生成所有图集", false, 15)]
        public static UniTask BuildAllAtlas()
        {
            var rootDir = new DirectoryInfo(Application.dataPath + "/Arts/UI/");
            foreach (var dir in rootDir.GetDirectories())
            {
                var files = dir.GetFiles("*.png", SearchOption.AllDirectories);
                var sortList = new List<SortStruct>();
                foreach (var file in files)
                {
                    if(file.FullName.EndsWith(".meta"))
                        continue;
                    var obj = AssetDatabase.LoadMainAssetAtPath(DataPathToAssetPath(file.FullName));
                    var path = AssetDatabase.GetAssetPath(obj);
                    sortList.Add(new SortStruct()
                    {
                        obj = obj,
                        path = path,
                    });
                }
                if(sortList.Count == 0)
                    continue;
                sortList.Sort((a, b) => a.path.CompareTo(b.path));
                BuildPNGAtlasByGUID(sortList, true);
            }
            AssetDatabase.Refresh();
            return UniTask.CompletedTask;
        }

        [MenuItem("Assets/Kunggame/生成图集 #&U", false, 14)]
        [MenuItem("Kunggame/UI Atlas/生成当前文件夹图集", false, 14)]
        private static void BuildPNGAtlasIncluded()
        {
            BuildPNGAtlas(true);
        }

        private class AtlasInfo
        {
            public List<UnityEngine.Object> objs;
            public bool isHaveAlpha;
        }
        private struct SortStruct
        {
            public UnityEngine.Object obj;
            public string path;
        }
        private static void BuildPNGAtlas(bool includeInBuild)
        {
            var guid = Selection.assetGUIDs[0];
            var gPath = AssetDatabase.GUIDToAssetPath(guid);
            if(!gPath.Contains(ATLAS_PATH))
            {
                Logger.LogError("所选择的文件夹路径有误，正确的要包含：" + ATLAS_PATH);
                return;
            }
            var selectionObj = Selection.GetFiltered(typeof(Texture2D), SelectionMode.DeepAssets);
            var sortList = new List<SortStruct>();
            foreach (var obj in selectionObj)
            {
                var path = AssetDatabase.GetAssetPath(obj);
                sortList.Add(new SortStruct()
                {
                    obj = obj,
                    path = path,
                });
            }

            sortList.Sort((a, b) => a.path.CompareTo(b.path));
            BuildPNGAtlasByGUID(sortList, includeInBuild);
        }

        private static void BuildPNGAtlasByGUID(List<SortStruct> sortList, bool includeInBuild)
        {
            var atlasDic = new Dictionary<string, AtlasInfo>();
            foreach ( var item in sortList)
            {
                var path = item.path;
                var atlasName = Path.GetFileName(Path.GetDirectoryName(path));
                if (!atlasDic.TryGetValue(atlasName, out var info))
                {
                    info = new AtlasInfo();
                    info.objs = new List<UnityEngine.Object>();
                    atlasDic.Add(atlasName, info);
                }
                if(info.isHaveAlpha == false)
                {
                    info.isHaveAlpha = IsHaveAlpha(path);
                }
                info.objs.Add(item.obj);
            }
            // 创建文件夹
            var infoPath = "Assets/Res/Atlas";
            if (!Directory.Exists(infoPath))
            {
                Directory.CreateDirectory(infoPath);
            }
            else
            {
                var files = Directory.GetFiles(infoPath, "*.spriteAtlas", SearchOption.AllDirectories);
                for(int i = files.Length - 1; i >= 0; i--)
                {
                    var fileName = Path.GetFileNameWithoutExtension(files[i]);
                    if(atlasDic.ContainsKey(fileName))
                    {
                        File.Delete(files[i]);
                    }
                }
            }
            // 创建保存图集信息的对象
            var infoFile = infoPath + "/AtlasInfoFile.prefab";
            GameObject infoObj;
            var resObj = AssetDatabase.LoadAssetAtPath<GameObject>(infoFile);
            if (resObj == null)
            {
                Logger.Log(infoFile);
                infoObj = new GameObject();
                PrefabUtility.SaveAsPrefabAsset(infoObj, infoFile);
            }
            else
            {
                infoObj = GameObject.Instantiate(resObj) as GameObject;
            }
            var infoSavers = infoObj.GetComponents<SpriteAtlasInfo>();
            var infoDic = new Dictionary<string, SpriteAtlasInfo>();
            foreach ( var saver in infoSavers )
            {
                infoDic.Add(saver.key, saver);
            }


            foreach ( var kvp in atlasDic )
            {
                var name = kvp.Key;
                var objs = kvp.Value.objs.ToArray();
                var isHaveAlpha = kvp.Value.isHaveAlpha;
                // 生成图集
                SpriteAtlas spriteAtlas = new SpriteAtlas();
                SpriteAtlasPackingSettings spriteAtlasPackingSettings = spriteAtlas.GetPackingSettings();
                spriteAtlasPackingSettings.enableRotation = false;
                spriteAtlasPackingSettings.enableTightPacking = false;
                spriteAtlasPackingSettings.padding = 2;
                spriteAtlas.SetPackingSettings(spriteAtlasPackingSettings);

                TextureImporterPlatformSettings iosPlatformSettings = spriteAtlas.GetPlatformSettings("iPhone");
                iosPlatformSettings.format = TextureImporterFormat.PVRTC_RGBA4;
                iosPlatformSettings.overridden = true;
                iosPlatformSettings.maxTextureSize = 2048;
                iosPlatformSettings.textureCompression = TextureImporterCompression.Compressed;
                iosPlatformSettings.allowsAlphaSplitting = true;
                iosPlatformSettings.compressionQuality = 50;
                spriteAtlas.SetPlatformSettings(iosPlatformSettings);

                TextureImporterPlatformSettings androidPlatformSettings = spriteAtlas.GetPlatformSettings("Android");
                if(isHaveAlpha)
                {
                    androidPlatformSettings.format = TextureImporterFormat.ETC2_RGBA8;
                }
                else
                {
                    androidPlatformSettings.format = TextureImporterFormat.ETC2_RGB4;
                }
                androidPlatformSettings.overridden = true;
                spriteAtlas.SetPlatformSettings(androidPlatformSettings);

                TextureImporterPlatformSettings wind64PlatformSettings = spriteAtlas.GetPlatformSettings("Standalone");
                wind64PlatformSettings.format = TextureImporterFormat.DXT5;
                wind64PlatformSettings.overridden = true;
                spriteAtlas.SetPlatformSettings(wind64PlatformSettings);

                spriteAtlas.Add(objs);
                spriteAtlas.SetIncludeInBuild(includeInBuild);
                spriteAtlas.SetPackingSettings(new SpriteAtlasPackingSettings() {enableRotation = true, padding = 4});
                Logger.Log("Build Atlas " + name);
                AssetDatabase.CreateAsset(spriteAtlas, string.Format(OUTPUT_PATH, name));//"Assets/Res/Atlas/" + name + ".spriteAtlas");
                
                
                string[] pngNames = new string[objs.Length];
                int i = 0;
                foreach ( var obj in objs )
                {
                    pngNames[i] = Path.GetFileNameWithoutExtension(AssetDatabase.GetAssetPath(obj));
                    i++;
                }

                // 保存图集信息
                if ( infoDic.ContainsKey(name) )
                {
                    infoDic[name].pngNames = pngNames;
                }
                else
                {
                    var saver = infoObj.AddComponent<SpriteAtlasInfo>();
                    saver.key = name;
                    saver.pngNames = pngNames;
                }
            }
            PrefabUtility.SaveAsPrefabAsset(infoObj, infoFile);
            GameObject.DestroyImmediate(infoObj);
            AssetDatabase.Refresh();
            SpriteAtlasUtility.PackAllAtlases(EditorUserBuildSettings.activeBuildTarget,false);
            foreach (var kvp in atlasDic)
            {
                GetTextureCount(AssetDatabase.LoadAssetAtPath(string.Format(OUTPUT_PATH, kvp.Key), typeof(SpriteAtlas)) as SpriteAtlas); //"Assets/Res/Atlas/" + kvp.Key + ".spriteAtlas", typeof(SpriteAtlas)) as SpriteAtlas);
            }
            AssetDatabase.Refresh();
            Logger.Log("Build Finish");
        }

        /// <summary>
        /// 获取spriteatlas生成的贴图个数
        /// </summary>
        /// <param name="atlas"></param>
        /// <returns></returns>
        private static int GetTextureCount(SpriteAtlas atlas)
        {
            var getPreviewTextureMI = typeof(SpriteAtlasExtensions).GetMethod("GetPreviewTextures", BindingFlags.Static | BindingFlags.NonPublic);
            var atlasTextures = (Texture2D[])getPreviewTextureMI.Invoke(null, new System.Object[] { atlas });
            var result = atlasTextures == null ? 0 : atlasTextures.Length;
            if(result > 1)
                Logger.LogError("这个图集太大了，超过了2048，去分割一下吧！当前图集名字：" + atlas.name + "<<<个数：" + result);
            return result;
        }
        
        private static bool IsHaveAlpha(string path)
        {
            var import = AssetImporter.GetAtPath(path) as TextureImporter;
            return import.DoesSourceTextureHaveAlpha();
        }
    }
}