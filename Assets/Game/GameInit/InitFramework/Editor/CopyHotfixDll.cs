#if USE_ILRUNTIME
using System.IO;
using UnityEditor;
using GameInit.Framework;

namespace Framework.Editor
{
    [InitializeOnLoad]
    public class CopyHotfixDll
    {
        private const string ScriptAssembliesDir = "Library/ScriptAssemblies";
        private const string CodeDir = "Assets/Res/HotFixDll/";
        private const string HotfixDll = "GameLogic.dll";
        private const string HotfixPdb = "GameLogic.pdb";

        static CopyHotfixDll()
        {
            if (EditorApplication.isPlaying)
                return;
            if (!Directory.Exists(CodeDir))
            {
                Directory.CreateDirectory(CodeDir);
            }
            File.Copy(Path.Combine(ScriptAssembliesDir, HotfixDll), Path.Combine(CodeDir, "HotFixDll.bytes"), true);
            File.Copy(Path.Combine(ScriptAssembliesDir, HotfixPdb), Path.Combine(CodeDir, "HotFixPDB.bytes"), true);
            Logger.LogMagenta($"复制HotFixLogic.dll, HotFixLogicPdb.pdb完成");
            AssetDatabase.Refresh();
        }
    }
}
#endif