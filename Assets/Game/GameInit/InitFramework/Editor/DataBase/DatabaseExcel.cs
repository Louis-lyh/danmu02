﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using UnityEditor;
using UnityEngine;

namespace GameHotfix.GameEditor
{
    public class DatabaseExcel
    {
        private const int START_ROW_INDEX = 2;

        //Excel数据类
        private ExcelPackage mExcelResult;
        // Excel Name
        private string mExcelName;
        // 行、列长度
        private int mColCount;
        private int mRowCount;
        
        // 命名空间
        private string mNamespace;
        
        // 类名
        public string ClassName => mExcelName;

        // 构造方法
        public DatabaseExcel(string excelFilePath, string nameSpace)
        {
            FileStream stream = File.Open(excelFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            mExcelResult = new ExcelPackage(stream);
            mExcelName = Path.GetFileNameWithoutExtension(excelFilePath);
            mNamespace = nameSpace;

            this.ReadHeadInfo();
        }
    
        // 生成数据库定义文件
        public void GenerateStruct(string filePath, Encoding encoding)
        {
            //检查是否是空数据表
            if (mExcelResult.Workbook.Worksheets.Count < 1)
                return;

            int row = -1, col = -1;
            try
            {
                var DBStoreTypeString = new[] {"None", "Save", "Permanent"};
                var DBPutTypeString = new[] {"Local", "Remote"};

                //默认读取第一个数据表
                ExcelWorksheet mExcel = mExcelResult.Workbook.Worksheets[1];

                // 构造方法字符
                StringBuilder methodString = new StringBuilder();
                // 初始化字符
                StringBuilder initString = new StringBuilder();
                // 主类
                StringBuilder builder = new StringBuilder();
                // 添加程序集
                builder.AppendLine("using System;");
                builder.AppendLine("using System.Collections.Generic;");
                builder.AppendLine("using Newtonsoft.Json;");
                builder.AppendLine("");
                // 命名空间开始
                builder.AppendLine($"namespace {this.mNamespace}");
                // 命名空间作用域开始
                builder.AppendLine("{");
                // json 标记
                builder.AppendSpace().AppendLine("[JsonObject(MemberSerialization.OptIn)]");
                // 类定义
                builder.AppendSpace().AppendLine($"public class I{this.mExcelName}");
                // 类作用域开始
                builder.AppendSpace().AppendLine("{");

                // 数据集
                builder.AppendSpace().AppendSpace().AppendLine($"public GameHotfix.Game.AssembleDB<GameHotfix.Game.{this.mExcelName}> Ass;");

                // declare property
                for (int i = START_ROW_INDEX; i <= this.mRowCount; i++)
                {
                    // 空行
                    builder.AppendLine("");
                    
                    row = i;
                    // 注释
                    col = 4;
                    builder.AppendSpace().AppendSpace().AppendLine($"// {mExcel.GetValue<string>(i, col)}");
                    // 自定义属性标记
                    col = 5;
                    if (!int.TryParse(mExcel.GetValue<string>(i, col), out int storeType))
                    {
                        throw new Exception("存储类型错误");
                    }
                    col = 6;
                    if (!int.TryParse(mExcel.GetValue<string>(i, col), out int putType))
                    {
                        throw new Exception("上传类型错误");
                    }
                    builder.AppendSpace().AppendSpace().AppendLine($"[DBValue(DBStoreType.{DBStoreTypeString[storeType]}, DBPutType.{DBPutTypeString[putType]})]");
                    // 属性名
                    col = 2;
                    string property = mExcel.GetValue<string>(i, col);
                    // 类型
                    col = 1;
                    string declareType = mExcel.GetValue<string>(i, col);
                    string propertyName = property.First().ToString().ToLower() + property.Substring(1);
                    
                    // 设置初始数据字符
                    col = 3;
                    initString.AppendSpace().AppendSpace().AppendSpace().AppendLine($"{propertyName} = {mExcel.GetValue<string>(i, col)};");
                    
                    builder.AppendSpace().AppendSpace().AppendLine($"[JsonProperty] protected {declareType} {propertyName};");

                    // 成员名
                    string menberName = property.First().ToString().ToUpper() + property.Substring(1);
                    // List 成员定义
                    if (declareType.StartsWith("List"))
                    {
                        string baseType = Regex.Match(declareType, "<(.+)>").Groups[1].Value;
                        builder.AppendSpace().AppendSpace().AppendLine($"public DBList<{baseType}> {menberName} = new DBList<{baseType}>();");
                        
                        // 初始DBList结构
                        methodString.AppendSpace().AppendSpace().AppendSpace().AppendLine($"{menberName}.Init(() => {propertyName}, (value) => {propertyName} = value, () => Ass.Save(\"{propertyName}\", {propertyName}));");
                        // RecordStoreIndexData.Init(() => recordStoreIndexData, (value) => recordStoreIndexData = value, () => Ass.Save("recordStoreIndexData", recordStoreIndexData));
                    }
                    // 通胀定义
                    else
                    {
                        builder.AppendSpace().AppendSpace().AppendLine($"public virtual {declareType} {menberName}");
                        // { getter | setter 作用域开始
                        builder.AppendSpace().AppendSpace().AppendLine("{");
                        // getter
                        builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"get => {propertyName};");
                        // setter
                        // 因需去掉DB模块中网络存储功能，即注释掉下列代码，数据一律存在本地 - timegorush
                        // 数据不保存在本地
                        // if (storeType == (int)DBStoreType.None)
                        // {
                        //     builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"set => {propertyName} = value;");
                        //     // } 结束
                        //     builder.AppendSpace().AppendSpace().AppendLine("}");
                        // }
                        // // 数据保存在本地
                        // else
                        {
                            builder.AppendSpace().AppendSpace().AppendSpace().AppendLine("set");
                            builder.AppendSpace().AppendSpace().AppendSpace().AppendLine("{");
                            builder.AppendSpace().AppendSpace().AppendSpace().AppendSpace().AppendLine($"{propertyName} = value;");
                            builder.AppendSpace().AppendSpace().AppendSpace().AppendSpace().AppendLine($"Ass.Save(\"{propertyName}\", value);");
                            builder.AppendSpace().AppendSpace().AppendSpace().AppendLine("}");
                            // } getter | setter 作用域结束
                            builder.AppendSpace().AppendSpace().AppendLine("}");

                            // 扩展设置方法(字典)
                            if (declareType.StartsWith("Dictionary"))
                            {
                                var types = Regex.Match(declareType, "<(.+),(.+)>").Groups;
                                string keyType = types[1].Value.Trim(), valueType = types[2].Value.Trim();
                                builder.AppendSpace().AppendSpace().AppendLine($"public virtual void Set{menberName}({keyType} key, {valueType} value)");
                                builder.AppendSpace().AppendSpace().AppendLine("{");
                                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"{propertyName}[key] = value;");
                                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"Ass.Save($\"{propertyName}" + ".{key}\"" +", value);");
                                builder.AppendSpace().AppendSpace().AppendLine("}");
                            }
                            // 扩展设置方法(类)
                            else if (this.IsClass(declareType))
                            {
                                builder.AppendSpace().AppendSpace().AppendLine($"public virtual void Set{menberName}(string key, object value)");
                                builder.AppendSpace().AppendSpace().AppendLine("{");
                                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"Ass.SetValue({propertyName}, key, value);");
                                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"Ass.Save($\"{propertyName}" + ".{key}\"" +", value);");
                                builder.AppendSpace().AppendSpace().AppendLine("}");
                            }
                        }
                    }
                }
                
                // 构造方法
                builder.AppendLine("");
                builder.AppendSpace().AppendSpace().AppendLine($"public I{this.mExcelName}()");
                builder.AppendSpace().AppendSpace().AppendLine("{");
                builder.Append(methodString);
                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine("InitData();");
                builder.AppendSpace().AppendSpace().AppendLine("}");
                
                // InitData 方法
                builder.AppendLine("");
                builder.AppendSpace().AppendSpace().AppendLine($"public void InitData()");
                builder.AppendSpace().AppendSpace().AppendLine("{");
                builder.Append(initString);
                builder.AppendSpace().AppendSpace().AppendLine("}");
                
                // 类作用域结束
                builder.AppendSpace().AppendLine("}");
                
                // 自定义类 开始
                this.GenerateCustomerClass(builder);
                // 自定义类 结束
                
                // 命名空间作用域结束
                builder.AppendLine("}");

                //写入文件
                WriteFile(builder.ToString(), filePath, encoding);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                EditorUtility.DisplayDialog("Excel导出异常", $"文件名称:{this.mExcelName}\n错误信息:[{row}, {col}]\n", "查看控制台");
            }
        }
        
        // 生成自定义类
        public void GenerateCustomerClass(StringBuilder builder)
        {
            // 遍历Excel所有表
            for (int i = 2; i <= mExcelResult.Workbook.Worksheets.Count; i++)
            {
                var table = mExcelResult.Workbook.Worksheets[i];
                // 解析表头信息
                var desc = table.GetValue<string>(1, 1);
                if (string.IsNullOrEmpty(desc))
                    continue;
                var array = desc.Split(',');
                bool IsCustomerClass = false;
                foreach (var info in array)
                {
                    var kv = info.Split(':');
                    switch (kv[0])
                    {
                        case "IsCustomerClass":
                        {
                            if (bool.TryParse(kv[1], out bool result))
                                IsCustomerClass = result;
                            break;
                        }
                    }
                }

                // 不是自定义，继续下一个table
                if (!IsCustomerClass)
                    continue;
                
                // 自定义类都可以序列化
                builder.AppendLine("");
                builder.AppendSpace().AppendLine("[Serializable]");
                // 类定义
                builder.AppendSpace().AppendLine($"public class {table.Name}");
                // 类作用域 开始
                builder.AppendSpace().AppendLine("{");
                
                // 属性定义 开始
                for (int row = 3; row <= table.Dimension.Rows; row++)
                {
                    var type = table.GetValue<string>(row, 1);
                    var name = table.GetValue<string>(row, 2);
                    var value = table.GetValue<string>(row, 3);
                    var comment = table.GetValue<string>(row, 4);
                    comment = string.IsNullOrEmpty(comment) ? "None" : comment; 

                    // 注释
                    builder.AppendSpace().AppendSpace().AppendLine($"// {comment}");
                    // 定义
                    if (string.IsNullOrEmpty(value))
                        builder.AppendSpace().AppendSpace().AppendLine($"public {type} {name};");
                    else
                        builder.AppendSpace().AppendSpace().AppendLine($"public {type} {name} = {value};");
                }
                // 属性定义 结束
                
                // 类作用域 结束
                builder.AppendSpace().AppendLine("}");
            }
        }
        
        // 生成数据库扩展文件
        public void GenerateExtend(string filePath, Encoding encoding)
        {
            // 如果文件存在，不在生成
            if (File.Exists(filePath))
                return;
            
            //检查是否是空数据表
            if (mExcelResult.Workbook.Worksheets.Count < 1)
                return;
            
            // 主类
            StringBuilder builder = new StringBuilder();
            // 添加程序集
            builder.AppendLine("using System;");
            builder.AppendLine("using System.Collections.Generic;");
            builder.AppendLine("using Newtonsoft.Json;");
            builder.AppendLine("");
            // 命名空间开始
            builder.AppendLine($"namespace {this.mNamespace}");
            // 命名空间作用域开始
            builder.AppendLine("{");
            // json 标记
            builder.AppendSpace().AppendLine("[JsonObject(MemberSerialization.OptIn)]");
            // 类定义
            builder.AppendSpace().AppendLine($"public class {this.mExcelName} : I{this.mExcelName}");
            // 类作用域开始
            builder.AppendSpace().AppendLine("{");
            // 重写构造方法
            builder.AppendSpace().AppendSpace().AppendLine($"public {this.mExcelName}() : base() {{ }}");
            // 类作用域结束
            builder.AppendSpace().AppendLine("}");
            // 命名空间作用域结束
            builder.AppendLine("}");
            
            //写入文件
            WriteFile(builder.ToString(), filePath, encoding);
        }
        
        // 生成DB初始化构文件
        public static void GenerateDBInitFile(List<DatabaseExcel> excels, string filePath, Encoding encoding)
        {
            // 主类builder
            StringBuilder builder = new StringBuilder();
            
            // 命名空间开始
            builder.AppendLine($"namespace GameHotfix.Game");
            // 命名空间作用域开始
            builder.AppendLine("{");
            
            // 类定义
            builder.AppendSpace().AppendLine("public static class DB");
            // 类作用域 开始
            builder.AppendSpace().AppendLine("{");
            
            // 定义属性
            foreach (var excel in excels)
                builder.AppendSpace().AppendSpace().AppendLine($"public static {excel.ClassName} {excel.ClassName};");
            builder.AppendLine("");
            
            // 定义初始方法
            builder.AppendSpace().AppendSpace().AppendLine("public static void Init()");
            builder.AppendSpace().AppendSpace().AppendLine("{");
            foreach (var excel in excels)
                builder.AppendSpace().AppendSpace().AppendSpace().AppendLine($"{excel.ClassName} = DataManager.I.Create<{excel.ClassName}>();");
            builder.AppendLine("");
            builder.AppendSpace().AppendSpace().AppendSpace().AppendLine("DataManager.I.LoadLocalData();");
            
            builder.AppendSpace().AppendSpace().AppendLine("}");
            
            // 类作用域 结束
            builder.AppendSpace().AppendLine("}");
            // 命名空间作用域结束
            builder.AppendLine("}");
            
            WriteFile(builder.ToString(), filePath, encoding);
        }

        private static void WriteFile(string content, string path, Encoding encoding)
        {
            using (FileStream fileStream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (TextWriter textWriter = new StreamWriter(fileStream, encoding))
                {
                    textWriter.Write(content);
                }
            }
        }

        private bool IsClass(string str)
        {
            return !(str.Equals("int") || str.Equals("string") || str.Equals("float") || str.Equals("bool") || str.Equals("double"));
        }

        // 读取表头信息
        private void ReadHeadInfo()
        {
            // 默认读取第一个数据表
            ExcelWorksheet mExcel = mExcelResult.Workbook.Worksheets[1];
            // 判断数据表内是否存在数据
            if (mExcel.Dimension.Rows < 2)
                return;
            // 读取Excel数据的行和列的数量
            this.mRowCount = mExcel.Dimension.Rows;
            this.mColCount = mExcel.Dimension.Columns;
        }
    }

    static class StringBuilderExtend
    {
        public static StringBuilder AppendSpace(this StringBuilder builder)
        {
            return builder.Append("    ");
        }
    }
}