﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace GameHotfix.GameEditor
{
    // 本地设置
    public class DatabaseSetting
    {
        // 工程路径
        public string ProjectPath = "";
        public string ExcelPath;
        public string OutPutPath;
        public bool IsClearTable;
        // 命名空间
        public string NameSpace;

        public string RealOutPutPath => $"{ProjectPath}{OutPutPath}";
        public string RealExcelPath => $"{ProjectPath}{ExcelPath}";
    }
    
    public class DatabaseTool : EditorWindow
    {
        // 设置文件配置
        private const string SETTING_FILE_NAME = "KGDatabaseToolSetting.json";

        // 本地设置
        private DatabaseSetting mSetting;

        [MenuItem("Kunggame/Tools/本地数据库管理工具")]
        private static void ShowWnd()
        {
            GetWindow<DatabaseTool>().Show();
        }
        
        [MenuItem("Kunggame/Tools/截图" + " %F2", false, 10)]
        private static async void Screenshot()
        {
            string folderPath = Application.dataPath;

            if (!Directory.Exists(folderPath))
                Directory.CreateDirectory(folderPath);

            var stampString = $"{Application.productName}-{DateTime.Now:yyyyMMdd-HHmmss}";
            ScreenCapture.CaptureScreenshot(Path.Combine(folderPath, stampString + ".png"));

            // await Task.Delay(1000);
            EditorUtility.RevealInFinder(Path.Combine(folderPath, stampString + ".png"));
        }

        // 打开
        private void OnEnable()
        {
            this.titleContent = new GUIContent("本地数据数据库管理工具");
            this.ReadToolSetting();
            this.LoadData();
        }

        private void OnGUI()
        {
            // 重新载入
            if (GUILayout.Button("Reload"))
            {
                this.SetToolPath();
                this.LoadData();
            }

            // 配置表路径为空
            if (string.IsNullOrEmpty(this.mSetting.ExcelPath) || string.IsNullOrEmpty(this.mSetting.OutPutPath))
                return;

            // 配置表路径
            GUILayout.BeginHorizontal();
            EditorGUILayout.TextField("Config Path", this.mSetting.ExcelPath);
            if (GUILayout.Button("...", GUILayout.Width(40)))
            {
                string outputPath = EditorUtility.OpenFolderPanel("选择配置路径", this.mSetting.RealExcelPath, "");
            
                if (!string.IsNullOrEmpty(outputPath))
                {
                    this.mSetting.ExcelPath = outputPath.Remove(0, this.mSetting.ProjectPath.Length); 
                    this.SaveToolSetting();
                }
            }
            GUILayout.EndHorizontal();
        
            // 配置表输出路径
            GUILayout.BeginHorizontal();
            EditorGUILayout.TextField("Out Path", this.mSetting.OutPutPath);
            if (GUILayout.Button("...", GUILayout.Width(40)))
            {
                string outputPath = EditorUtility.OpenFolderPanel("选择配置路径", this.mSetting.RealOutPutPath, "");
                if (!string.IsNullOrEmpty(outputPath))
                {
                    this.mSetting.OutPutPath = outputPath.Remove(0, this.mSetting.ProjectPath.Length);
                    this.SaveToolSetting();
                }
            }
            GUILayout.EndHorizontal();
            
            // 命名空间
            var customerNameSpace = EditorGUILayout.TextField("Class Namespace", this.mSetting.NameSpace);
            if (!string.IsNullOrEmpty(customerNameSpace) && !customerNameSpace.Equals(this.mSetting.NameSpace))
            {
                this.mSetting.NameSpace = customerNameSpace;
                this.SaveToolSetting();
            }

            // 是否清除Table表
            if (EditorGUILayout.Toggle("Clear Tables", this.mSetting.IsClearTable) != this.mSetting.IsClearTable)
            {
                this.mSetting.IsClearTable = !this.mSetting.IsClearTable;
                this.SaveToolSetting();
            }
            
            // 生成本地配置
            if (GUILayout.Button("构建配置"))
            {
                this.OnClickBuild();
            }
            
            // 清空数据
            if (GUILayout.Button("清空数据"))
            {
                PlayerPrefs.DeleteAll();
            }
        }

        // 初始数据
        private void LoadData()
        {
            if (string.IsNullOrEmpty(this.mSetting.ExcelPath))
                return;
        }
        
        // 设置输入、输出路径
        private void SetToolPath()
        {
            // 选择配置表路径
            if (string.IsNullOrEmpty(this.mSetting.ExcelPath))
            {
                string outputPath = EditorUtility.OpenFolderPanel("选择配置路径", this.mSetting.RealExcelPath, "");
                if (!string.IsNullOrEmpty(outputPath))
                    this.mSetting.ExcelPath = outputPath.Remove(0, this.mSetting.ProjectPath.Length);
            }
        
            // 选择配置输出路径
            if (string.IsNullOrEmpty(this.mSetting.OutPutPath))
            {
                string outputPath = EditorUtility.OpenFolderPanel("选择配置路径", this.mSetting.RealOutPutPath, "");
                if (!string.IsNullOrEmpty(outputPath))
                    this.mSetting.OutPutPath = outputPath.Remove(0, this.mSetting.ProjectPath.Length);
            }
        
            this.SaveToolSetting();
        }
        
        // 构建
        private void OnClickBuild()
        {
            // 清除文件夹
            this.ClearJsonFolder();
            // 生成数据库文件
            this.GenDatabase();
            // 刷新
            AssetDatabase.Refresh();
        }

        // 清除Json文件夹
        private void ClearJsonFolder()
        {
            if (!this.mSetting.IsClearTable)
                return;
            
            List<string> folders = new List<string>();

            if (this.mSetting.IsClearTable)
            {
                folders.Add("Tables");
            }
        
            foreach (var folder in folders)
            {
                var destFolder = $"{this.mSetting.RealOutPutPath}/{folder}/";
                var files = Directory.GetFiles(destFolder);
                foreach (var file in files)
                    File.Delete(file);
            }
        }
        
        // 生成数据库文件
        private void GenDatabase()
        {
            var folderPath = $"{this.mSetting.RealExcelPath}/";
            var files = Directory.GetFiles(folderPath, "*.xlsx");
            
            // 读取Excel
            List<DatabaseExcel> excels = new List<DatabaseExcel>();
            foreach (var file in files)
                excels.Add(new DatabaseExcel(file, this.mSetting.NameSpace));
            
            // 生成Json文件
            for (int i = 0; i < files.Length; i++)
            {
                var file = files[i];
                var excel = excels[i];
                    
                // 导出Json(管理工具文件夹)
                var fileName = Path.GetFileNameWithoutExtension(file);
                
                // 生成DB结构文件
                string structFile = $"{this.mSetting.RealOutPutPath}/Tables/I{fileName}.cs";
                excel.GenerateStruct(structFile, new UTF8Encoding(false));
                // 生成DB扩展文件
                if (fileName.Contains("DB") == false)
                    fileName = "DB" + fileName;
                string extendFile = $"{this.mSetting.RealOutPutPath}/TableExtend/{fileName}.cs";
                excel.GenerateExtend(extendFile, new UTF8Encoding(false));
            }
            
            string dbFilePath = $"{this.mSetting.RealOutPutPath}/TableExtend/Database.cs";
            DatabaseExcel.GenerateDBInitFile(excels, dbFilePath, new UTF8Encoding(false));
        }
        
        // 读取设置
        private void ReadToolSetting()
        {
            string projectPath = $"{System.Environment.CurrentDirectory}";
            string filePath = $"{projectPath}/ProjectSettings/{SETTING_FILE_NAME}";
            if (!File.Exists(filePath))
                File.Create(filePath).Close();
            var text = File.ReadAllText(filePath);
            if (string.IsNullOrEmpty(text))
            {
                this.mSetting = this.mSetting ?? new DatabaseSetting
                {
                    ProjectPath = projectPath,
                    IsClearTable = true,
                    NameSpace = "KungGame.Database"
                };
            }
            else
            {
                this.mSetting = JsonUtility.FromJson<DatabaseSetting>(text);
                this.mSetting.ProjectPath = projectPath;
            }
        }
        
        // 保存设置
        private void SaveToolSetting()
        {
            string filePath = $"{this.mSetting.ProjectPath}/ProjectSettings/{SETTING_FILE_NAME}";
            var data = JsonUtility.ToJson(this.mSetting);
        
            this.WriteFile(filePath, data);
        }
        
        // 写入设置文件
        private void WriteFile(string file, string text)
        {
            using (FileStream fileStream = new FileStream(file, FileMode.Create, FileAccess.Write))
            {
                using (TextWriter textWriter = new StreamWriter(fileStream, new UTF8Encoding(false)))
                {
                    textWriter.Write(text);
                }
            }
        }
    }
}