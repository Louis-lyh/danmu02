using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class System_Security_Cryptography_X509Certificates_X509ChainPolicy_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            Type[] args;
            Type type = typeof(System.Security.Cryptography.X509Certificates.X509ChainPolicy);
            args = new Type[]{typeof(System.Security.Cryptography.X509Certificates.X509RevocationFlag)};
            method = type.GetMethod("set_RevocationFlag", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, set_RevocationFlag_0);
            args = new Type[]{typeof(System.Security.Cryptography.X509Certificates.X509RevocationMode)};
            method = type.GetMethod("set_RevocationMode", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, set_RevocationMode_1);
            args = new Type[]{typeof(System.TimeSpan)};
            method = type.GetMethod("set_UrlRetrievalTimeout", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, set_UrlRetrievalTimeout_2);
            args = new Type[]{typeof(System.Security.Cryptography.X509Certificates.X509VerificationFlags)};
            method = type.GetMethod("set_VerificationFlags", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, set_VerificationFlags_3);


        }


        static StackObject* set_RevocationFlag_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509RevocationFlag @value = (System.Security.Cryptography.X509Certificates.X509RevocationFlag)typeof(System.Security.Cryptography.X509Certificates.X509RevocationFlag).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Security.Cryptography.X509Certificates.X509ChainPolicy instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509ChainPolicy)typeof(System.Security.Cryptography.X509Certificates.X509ChainPolicy).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RevocationFlag = value;

            return __ret;
        }

        static StackObject* set_RevocationMode_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509RevocationMode @value = (System.Security.Cryptography.X509Certificates.X509RevocationMode)typeof(System.Security.Cryptography.X509Certificates.X509RevocationMode).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Security.Cryptography.X509Certificates.X509ChainPolicy instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509ChainPolicy)typeof(System.Security.Cryptography.X509Certificates.X509ChainPolicy).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RevocationMode = value;

            return __ret;
        }

        static StackObject* set_UrlRetrievalTimeout_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.TimeSpan @value = (System.TimeSpan)typeof(System.TimeSpan).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)16);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Security.Cryptography.X509Certificates.X509ChainPolicy instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509ChainPolicy)typeof(System.Security.Cryptography.X509Certificates.X509ChainPolicy).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.UrlRetrievalTimeout = value;

            return __ret;
        }

        static StackObject* set_VerificationFlags_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509VerificationFlags @value = (System.Security.Cryptography.X509Certificates.X509VerificationFlags)typeof(System.Security.Cryptography.X509Certificates.X509VerificationFlags).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Security.Cryptography.X509Certificates.X509ChainPolicy instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509ChainPolicy)typeof(System.Security.Cryptography.X509Certificates.X509ChainPolicy).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.VerificationFlags = value;

            return __ret;
        }



    }
}
