using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_TopPlayerInfo_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.TopPlayerInfo);

            field = type.GetField("UserName", flag);
            app.RegisterCLRFieldGetter(field, get_UserName_0);
            app.RegisterCLRFieldSetter(field, set_UserName_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_UserName_0, AssignFromStack_UserName_0);
            field = type.GetField("Order", flag);
            app.RegisterCLRFieldGetter(field, get_Order_1);
            app.RegisterCLRFieldSetter(field, set_Order_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_Order_1, AssignFromStack_Order_1);
            field = type.GetField("HeadHurl", flag);
            app.RegisterCLRFieldGetter(field, get_HeadHurl_2);
            app.RegisterCLRFieldSetter(field, set_HeadHurl_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_HeadHurl_2, AssignFromStack_HeadHurl_2);


        }



        static object get_UserName_0(ref object o)
        {
            return ((GameInit.Game.TopPlayerInfo)o).UserName;
        }

        static StackObject* CopyToStack_UserName_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.TopPlayerInfo)o).UserName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_UserName_0(ref object o, object v)
        {
            ((GameInit.Game.TopPlayerInfo)o).UserName = (System.String)v;
        }

        static StackObject* AssignFromStack_UserName_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @UserName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.TopPlayerInfo)o).UserName = @UserName;
            return ptr_of_this_method;
        }

        static object get_Order_1(ref object o)
        {
            return ((GameInit.Game.TopPlayerInfo)o).Order;
        }

        static StackObject* CopyToStack_Order_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.TopPlayerInfo)o).Order;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_Order_1(ref object o, object v)
        {
            ((GameInit.Game.TopPlayerInfo)o).Order = (System.Int32)v;
        }

        static StackObject* AssignFromStack_Order_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @Order = ptr_of_this_method->Value;
            ((GameInit.Game.TopPlayerInfo)o).Order = @Order;
            return ptr_of_this_method;
        }

        static object get_HeadHurl_2(ref object o)
        {
            return ((GameInit.Game.TopPlayerInfo)o).HeadHurl;
        }

        static StackObject* CopyToStack_HeadHurl_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.TopPlayerInfo)o).HeadHurl;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_HeadHurl_2(ref object o, object v)
        {
            ((GameInit.Game.TopPlayerInfo)o).HeadHurl = (System.String)v;
        }

        static StackObject* AssignFromStack_HeadHurl_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @HeadHurl = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.TopPlayerInfo)o).HeadHurl = @HeadHurl;
            return ptr_of_this_method;
        }



    }
}
