using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_EdgeBubbleInfo_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.EdgeBubbleInfo);

            field = type.GetField("Type", flag);
            app.RegisterCLRFieldGetter(field, get_Type_0);
            app.RegisterCLRFieldSetter(field, set_Type_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_Type_0, AssignFromStack_Type_0);
            field = type.GetField("HeadUrl", flag);
            app.RegisterCLRFieldGetter(field, get_HeadUrl_1);
            app.RegisterCLRFieldSetter(field, set_HeadUrl_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_HeadUrl_1, AssignFromStack_HeadUrl_1);
            field = type.GetField("Name", flag);
            app.RegisterCLRFieldGetter(field, get_Name_2);
            app.RegisterCLRFieldSetter(field, set_Name_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_Name_2, AssignFromStack_Name_2);
            field = type.GetField("EventName", flag);
            app.RegisterCLRFieldGetter(field, get_EventName_3);
            app.RegisterCLRFieldSetter(field, set_EventName_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_EventName_3, AssignFromStack_EventName_3);
            field = type.GetField("EventContent", flag);
            app.RegisterCLRFieldGetter(field, get_EventContent_4);
            app.RegisterCLRFieldSetter(field, set_EventContent_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_EventContent_4, AssignFromStack_EventContent_4);

            app.RegisterCLRCreateDefaultInstance(type, () => new GameInit.Game.EdgeBubbleInfo());


        }

        static void WriteBackInstance(ILRuntime.Runtime.Enviorment.AppDomain __domain, StackObject* ptr_of_this_method, IList<object> __mStack, ref GameInit.Game.EdgeBubbleInfo instance_of_this_method)
        {
            ptr_of_this_method = ILIntepreter.GetObjectAndResolveReference(ptr_of_this_method);
            switch(ptr_of_this_method->ObjectType)
            {
                case ObjectTypes.Object:
                    {
                        __mStack[ptr_of_this_method->Value] = instance_of_this_method;
                    }
                    break;
                case ObjectTypes.FieldReference:
                    {
                        var ___obj = __mStack[ptr_of_this_method->Value];
                        if(___obj is ILTypeInstance)
                        {
                            ((ILTypeInstance)___obj)[ptr_of_this_method->ValueLow] = instance_of_this_method;
                        }
                        else
                        {
                            var t = __domain.GetType(___obj.GetType()) as CLRType;
                            t.SetFieldValue(ptr_of_this_method->ValueLow, ref ___obj, instance_of_this_method);
                        }
                    }
                    break;
                case ObjectTypes.StaticFieldReference:
                    {
                        var t = __domain.GetType(ptr_of_this_method->Value);
                        if(t is ILType)
                        {
                            ((ILType)t).StaticInstance[ptr_of_this_method->ValueLow] = instance_of_this_method;
                        }
                        else
                        {
                            ((CLRType)t).SetStaticFieldValue(ptr_of_this_method->ValueLow, instance_of_this_method);
                        }
                    }
                    break;
                 case ObjectTypes.ArrayReference:
                    {
                        var instance_of_arrayReference = __mStack[ptr_of_this_method->Value] as GameInit.Game.EdgeBubbleInfo[];
                        instance_of_arrayReference[ptr_of_this_method->ValueLow] = instance_of_this_method;
                    }
                    break;
            }
        }


        static object get_Type_0(ref object o)
        {
            return ((GameInit.Game.EdgeBubbleInfo)o).Type;
        }

        static StackObject* CopyToStack_Type_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.EdgeBubbleInfo)o).Type;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_Type_0(ref object o, object v)
        {
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.Type = (GameInit.Game.EdgeBubbleType)v;
            o = ins;
        }

        static StackObject* AssignFromStack_Type_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.EdgeBubbleType @Type = (GameInit.Game.EdgeBubbleType)typeof(GameInit.Game.EdgeBubbleType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.Type = @Type;
            o = ins;
            return ptr_of_this_method;
        }

        static object get_HeadUrl_1(ref object o)
        {
            return ((GameInit.Game.EdgeBubbleInfo)o).HeadUrl;
        }

        static StackObject* CopyToStack_HeadUrl_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.EdgeBubbleInfo)o).HeadUrl;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_HeadUrl_1(ref object o, object v)
        {
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.HeadUrl = (System.String)v;
            o = ins;
        }

        static StackObject* AssignFromStack_HeadUrl_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @HeadUrl = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.HeadUrl = @HeadUrl;
            o = ins;
            return ptr_of_this_method;
        }

        static object get_Name_2(ref object o)
        {
            return ((GameInit.Game.EdgeBubbleInfo)o).Name;
        }

        static StackObject* CopyToStack_Name_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.EdgeBubbleInfo)o).Name;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_Name_2(ref object o, object v)
        {
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.Name = (System.String)v;
            o = ins;
        }

        static StackObject* AssignFromStack_Name_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @Name = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.Name = @Name;
            o = ins;
            return ptr_of_this_method;
        }

        static object get_EventName_3(ref object o)
        {
            return ((GameInit.Game.EdgeBubbleInfo)o).EventName;
        }

        static StackObject* CopyToStack_EventName_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.EdgeBubbleInfo)o).EventName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_EventName_3(ref object o, object v)
        {
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.EventName = (System.String)v;
            o = ins;
        }

        static StackObject* AssignFromStack_EventName_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @EventName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.EventName = @EventName;
            o = ins;
            return ptr_of_this_method;
        }

        static object get_EventContent_4(ref object o)
        {
            return ((GameInit.Game.EdgeBubbleInfo)o).EventContent;
        }

        static StackObject* CopyToStack_EventContent_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.EdgeBubbleInfo)o).EventContent;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_EventContent_4(ref object o, object v)
        {
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.EventContent = (System.String)v;
            o = ins;
        }

        static StackObject* AssignFromStack_EventContent_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @EventContent = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.EdgeBubbleInfo ins =(GameInit.Game.EdgeBubbleInfo)o;
            ins.EventContent = @EventContent;
            o = ins;
            return ptr_of_this_method;
        }



    }
}
