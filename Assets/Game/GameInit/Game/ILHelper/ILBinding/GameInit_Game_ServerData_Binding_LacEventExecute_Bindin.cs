using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_ServerData_Binding_LacEventExecute_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.ServerData.LacEventExecute);

            field = type.GetField("userName", flag);
            app.RegisterCLRFieldGetter(field, get_userName_0);
            app.RegisterCLRFieldSetter(field, set_userName_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_userName_0, AssignFromStack_userName_0);
            field = type.GetField("score", flag);
            app.RegisterCLRFieldGetter(field, get_score_1);
            app.RegisterCLRFieldSetter(field, set_score_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_score_1, AssignFromStack_score_1);
            field = type.GetField("rank", flag);
            app.RegisterCLRFieldGetter(field, get_rank_2);
            app.RegisterCLRFieldSetter(field, set_rank_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_rank_2, AssignFromStack_rank_2);
            field = type.GetField("extension", flag);
            app.RegisterCLRFieldGetter(field, get_extension_3);
            app.RegisterCLRFieldSetter(field, set_extension_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_extension_3, AssignFromStack_extension_3);
            field = type.GetField("headUrl", flag);
            app.RegisterCLRFieldGetter(field, get_headUrl_4);
            app.RegisterCLRFieldSetter(field, set_headUrl_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_headUrl_4, AssignFromStack_headUrl_4);
            field = type.GetField("lcType", flag);
            app.RegisterCLRFieldGetter(field, get_lcType_5);
            app.RegisterCLRFieldSetter(field, set_lcType_5);
            app.RegisterCLRFieldBinding(field, CopyToStack_lcType_5, AssignFromStack_lcType_5);
            field = type.GetField("userId", flag);
            app.RegisterCLRFieldGetter(field, get_userId_6);
            app.RegisterCLRFieldSetter(field, set_userId_6);
            app.RegisterCLRFieldBinding(field, CopyToStack_userId_6, AssignFromStack_userId_6);
            field = type.GetField("bType", flag);
            app.RegisterCLRFieldGetter(field, get_bType_7);
            app.RegisterCLRFieldSetter(field, set_bType_7);
            app.RegisterCLRFieldBinding(field, CopyToStack_bType_7, AssignFromStack_bType_7);
            field = type.GetField("guildId", flag);
            app.RegisterCLRFieldGetter(field, get_guildId_8);
            app.RegisterCLRFieldSetter(field, set_guildId_8);
            app.RegisterCLRFieldBinding(field, CopyToStack_guildId_8, AssignFromStack_guildId_8);
            field = type.GetField("winStreak", flag);
            app.RegisterCLRFieldGetter(field, get_winStreak_9);
            app.RegisterCLRFieldSetter(field, set_winStreak_9);
            app.RegisterCLRFieldBinding(field, CopyToStack_winStreak_9, AssignFromStack_winStreak_9);
            field = type.GetField("likeNum", flag);
            app.RegisterCLRFieldGetter(field, get_likeNum_10);
            app.RegisterCLRFieldSetter(field, set_likeNum_10);
            app.RegisterCLRFieldBinding(field, CopyToStack_likeNum_10, AssignFromStack_likeNum_10);
            field = type.GetField("giftNum", flag);
            app.RegisterCLRFieldGetter(field, get_giftNum_11);
            app.RegisterCLRFieldSetter(field, set_giftNum_11);
            app.RegisterCLRFieldBinding(field, CopyToStack_giftNum_11, AssignFromStack_giftNum_11);
            field = type.GetField("giftType", flag);
            app.RegisterCLRFieldGetter(field, get_giftType_12);
            app.RegisterCLRFieldSetter(field, set_giftType_12);
            app.RegisterCLRFieldBinding(field, CopyToStack_giftType_12, AssignFromStack_giftType_12);
            field = type.GetField("giftValue", flag);
            app.RegisterCLRFieldGetter(field, get_giftValue_13);
            app.RegisterCLRFieldSetter(field, set_giftValue_13);
            app.RegisterCLRFieldBinding(field, CopyToStack_giftValue_13, AssignFromStack_giftValue_13);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_userName_0(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).userName;
        }

        static StackObject* CopyToStack_userName_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).userName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_userName_0(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).userName = (System.String)v;
        }

        static StackObject* AssignFromStack_userName_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @userName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.LacEventExecute)o).userName = @userName;
            return ptr_of_this_method;
        }

        static object get_score_1(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).score;
        }

        static StackObject* CopyToStack_score_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).score;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_score_1(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).score = (System.Int64)v;
        }

        static StackObject* AssignFromStack_score_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @score = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).score = @score;
            return ptr_of_this_method;
        }

        static object get_rank_2(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).rank;
        }

        static StackObject* CopyToStack_rank_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).rank;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_rank_2(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).rank = (System.Int32)v;
        }

        static StackObject* AssignFromStack_rank_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @rank = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).rank = @rank;
            return ptr_of_this_method;
        }

        static object get_extension_3(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).extension;
        }

        static StackObject* CopyToStack_extension_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).extension;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_extension_3(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).extension = (System.Collections.Generic.Dictionary<System.String, System.Int32>)v;
        }

        static StackObject* AssignFromStack_extension_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Int32> @extension = (System.Collections.Generic.Dictionary<System.String, System.Int32>)typeof(System.Collections.Generic.Dictionary<System.String, System.Int32>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.LacEventExecute)o).extension = @extension;
            return ptr_of_this_method;
        }

        static object get_headUrl_4(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).headUrl;
        }

        static StackObject* CopyToStack_headUrl_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).headUrl;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_headUrl_4(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).headUrl = (System.String)v;
        }

        static StackObject* AssignFromStack_headUrl_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @headUrl = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.LacEventExecute)o).headUrl = @headUrl;
            return ptr_of_this_method;
        }

        static object get_lcType_5(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).lcType;
        }

        static StackObject* CopyToStack_lcType_5(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).lcType;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_lcType_5(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).lcType = (GameInit.Game.ServerData.ELogicActionType)v;
        }

        static StackObject* AssignFromStack_lcType_5(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.ServerData.ELogicActionType @lcType = (GameInit.Game.ServerData.ELogicActionType)typeof(GameInit.Game.ServerData.ELogicActionType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            ((GameInit.Game.ServerData.LacEventExecute)o).lcType = @lcType;
            return ptr_of_this_method;
        }

        static object get_userId_6(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).userId;
        }

        static StackObject* CopyToStack_userId_6(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).userId;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_userId_6(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).userId = (System.String)v;
        }

        static StackObject* AssignFromStack_userId_6(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @userId = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.LacEventExecute)o).userId = @userId;
            return ptr_of_this_method;
        }

        static object get_bType_7(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).bType;
        }

        static StackObject* CopyToStack_bType_7(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).bType;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_bType_7(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).bType = (GameInit.Game.ServerData.EBarrageType)v;
        }

        static StackObject* AssignFromStack_bType_7(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.ServerData.EBarrageType @bType = (GameInit.Game.ServerData.EBarrageType)typeof(GameInit.Game.ServerData.EBarrageType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            ((GameInit.Game.ServerData.LacEventExecute)o).bType = @bType;
            return ptr_of_this_method;
        }

        static object get_guildId_8(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).guildId;
        }

        static StackObject* CopyToStack_guildId_8(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).guildId;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_guildId_8(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).guildId = (System.Int32)v;
        }

        static StackObject* AssignFromStack_guildId_8(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @guildId = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).guildId = @guildId;
            return ptr_of_this_method;
        }

        static object get_winStreak_9(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).winStreak;
        }

        static StackObject* CopyToStack_winStreak_9(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).winStreak;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_winStreak_9(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).winStreak = (System.Int32)v;
        }

        static StackObject* AssignFromStack_winStreak_9(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @winStreak = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).winStreak = @winStreak;
            return ptr_of_this_method;
        }

        static object get_likeNum_10(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).likeNum;
        }

        static StackObject* CopyToStack_likeNum_10(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).likeNum;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_likeNum_10(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).likeNum = (System.Int32)v;
        }

        static StackObject* AssignFromStack_likeNum_10(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @likeNum = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).likeNum = @likeNum;
            return ptr_of_this_method;
        }

        static object get_giftNum_11(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).giftNum;
        }

        static StackObject* CopyToStack_giftNum_11(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).giftNum;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_giftNum_11(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).giftNum = (System.Int32)v;
        }

        static StackObject* AssignFromStack_giftNum_11(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @giftNum = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).giftNum = @giftNum;
            return ptr_of_this_method;
        }

        static object get_giftType_12(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).giftType;
        }

        static StackObject* CopyToStack_giftType_12(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).giftType;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_giftType_12(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).giftType = (GameInit.Game.ServerData.EGiftType)v;
        }

        static StackObject* AssignFromStack_giftType_12(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.ServerData.EGiftType @giftType = (GameInit.Game.ServerData.EGiftType)typeof(GameInit.Game.ServerData.EGiftType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            ((GameInit.Game.ServerData.LacEventExecute)o).giftType = @giftType;
            return ptr_of_this_method;
        }

        static object get_giftValue_13(ref object o)
        {
            return ((GameInit.Game.ServerData.LacEventExecute)o).giftValue;
        }

        static StackObject* CopyToStack_giftValue_13(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.LacEventExecute)o).giftValue;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_giftValue_13(ref object o, object v)
        {
            ((GameInit.Game.ServerData.LacEventExecute)o).giftValue = (System.Int32)v;
        }

        static StackObject* AssignFromStack_giftValue_13(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @giftValue = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.LacEventExecute)o).giftValue = @giftValue;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.ServerData.LacEventExecute();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
