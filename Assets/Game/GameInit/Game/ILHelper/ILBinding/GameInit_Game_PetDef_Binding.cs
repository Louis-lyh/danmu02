using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_PetDef_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.PetDef);

            field = type.GetField("ID", flag);
            app.RegisterCLRFieldGetter(field, get_ID_0);
            app.RegisterCLRFieldSetter(field, set_ID_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_ID_0, AssignFromStack_ID_0);
            field = type.GetField("PreExpLimit", flag);
            app.RegisterCLRFieldGetter(field, get_PreExpLimit_1);
            app.RegisterCLRFieldSetter(field, set_PreExpLimit_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_PreExpLimit_1, AssignFromStack_PreExpLimit_1);
            field = type.GetField("ExpLimit", flag);
            app.RegisterCLRFieldGetter(field, get_ExpLimit_2);
            app.RegisterCLRFieldSetter(field, set_ExpLimit_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_ExpLimit_2, AssignFromStack_ExpLimit_2);
            field = type.GetField("RoleId", flag);
            app.RegisterCLRFieldGetter(field, get_RoleId_3);
            app.RegisterCLRFieldSetter(field, set_RoleId_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_RoleId_3, AssignFromStack_RoleId_3);


        }



        static object get_ID_0(ref object o)
        {
            return ((GameInit.Game.PetDef)o).ID;
        }

        static StackObject* CopyToStack_ID_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.PetDef)o).ID;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ID_0(ref object o, object v)
        {
            ((GameInit.Game.PetDef)o).ID = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ID_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ID = ptr_of_this_method->Value;
            ((GameInit.Game.PetDef)o).ID = @ID;
            return ptr_of_this_method;
        }

        static object get_PreExpLimit_1(ref object o)
        {
            return ((GameInit.Game.PetDef)o).PreExpLimit;
        }

        static StackObject* CopyToStack_PreExpLimit_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.PetDef)o).PreExpLimit;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_PreExpLimit_1(ref object o, object v)
        {
            ((GameInit.Game.PetDef)o).PreExpLimit = (System.Int32)v;
        }

        static StackObject* AssignFromStack_PreExpLimit_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @PreExpLimit = ptr_of_this_method->Value;
            ((GameInit.Game.PetDef)o).PreExpLimit = @PreExpLimit;
            return ptr_of_this_method;
        }

        static object get_ExpLimit_2(ref object o)
        {
            return ((GameInit.Game.PetDef)o).ExpLimit;
        }

        static StackObject* CopyToStack_ExpLimit_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.PetDef)o).ExpLimit;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ExpLimit_2(ref object o, object v)
        {
            ((GameInit.Game.PetDef)o).ExpLimit = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ExpLimit_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ExpLimit = ptr_of_this_method->Value;
            ((GameInit.Game.PetDef)o).ExpLimit = @ExpLimit;
            return ptr_of_this_method;
        }

        static object get_RoleId_3(ref object o)
        {
            return ((GameInit.Game.PetDef)o).RoleId;
        }

        static StackObject* CopyToStack_RoleId_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.PetDef)o).RoleId;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_RoleId_3(ref object o, object v)
        {
            ((GameInit.Game.PetDef)o).RoleId = (System.Int32)v;
        }

        static StackObject* AssignFromStack_RoleId_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @RoleId = ptr_of_this_method->Value;
            ((GameInit.Game.PetDef)o).RoleId = @RoleId;
            return ptr_of_this_method;
        }



    }
}
