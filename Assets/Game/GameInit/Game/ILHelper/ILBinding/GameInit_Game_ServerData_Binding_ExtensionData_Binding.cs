using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_ServerData_Binding_ExtensionData_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.ServerData.ExtensionData);

            field = type.GetField("type", flag);
            app.RegisterCLRFieldGetter(field, get_type_0);
            app.RegisterCLRFieldSetter(field, set_type_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_type_0, AssignFromStack_type_0);
            field = type.GetField("extensions", flag);
            app.RegisterCLRFieldGetter(field, get_extensions_1);
            app.RegisterCLRFieldSetter(field, set_extensions_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_extensions_1, AssignFromStack_extensions_1);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_type_0(ref object o)
        {
            return ((GameInit.Game.ServerData.ExtensionData)o).type;
        }

        static StackObject* CopyToStack_type_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.ExtensionData)o).type;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_type_0(ref object o, object v)
        {
            ((GameInit.Game.ServerData.ExtensionData)o).type = (System.Int32)v;
        }

        static StackObject* AssignFromStack_type_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @type = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.ExtensionData)o).type = @type;
            return ptr_of_this_method;
        }

        static object get_extensions_1(ref object o)
        {
            return ((GameInit.Game.ServerData.ExtensionData)o).extensions;
        }

        static StackObject* CopyToStack_extensions_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.ExtensionData)o).extensions;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_extensions_1(ref object o, object v)
        {
            ((GameInit.Game.ServerData.ExtensionData)o).extensions = (System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>)v;
        }

        static StackObject* AssignFromStack_extensions_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>> @extensions = (System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>)typeof(System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.ExtensionData)o).extensions = @extensions;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.ServerData.ExtensionData();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
