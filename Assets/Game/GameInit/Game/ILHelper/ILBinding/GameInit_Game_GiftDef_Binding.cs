using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_GiftDef_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.GiftDef);

            field = type.GetField("Exp", flag);
            app.RegisterCLRFieldGetter(field, get_Exp_0);
            app.RegisterCLRFieldSetter(field, set_Exp_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_Exp_0, AssignFromStack_Exp_0);
            field = type.GetField("ExpHalo", flag);
            app.RegisterCLRFieldGetter(field, get_ExpHalo_1);
            app.RegisterCLRFieldSetter(field, set_ExpHalo_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_ExpHalo_1, AssignFromStack_ExpHalo_1);
            field = type.GetField("ExpPet", flag);
            app.RegisterCLRFieldGetter(field, get_ExpPet_2);
            app.RegisterCLRFieldSetter(field, set_ExpPet_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_ExpPet_2, AssignFromStack_ExpPet_2);
            field = type.GetField("BuffId", flag);
            app.RegisterCLRFieldGetter(field, get_BuffId_3);
            app.RegisterCLRFieldSetter(field, set_BuffId_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_BuffId_3, AssignFromStack_BuffId_3);
            field = type.GetField("BossBuffId", flag);
            app.RegisterCLRFieldGetter(field, get_BossBuffId_4);
            app.RegisterCLRFieldSetter(field, set_BossBuffId_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_BossBuffId_4, AssignFromStack_BossBuffId_4);


        }



        static object get_Exp_0(ref object o)
        {
            return ((GameInit.Game.GiftDef)o).Exp;
        }

        static StackObject* CopyToStack_Exp_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.GiftDef)o).Exp;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_Exp_0(ref object o, object v)
        {
            ((GameInit.Game.GiftDef)o).Exp = (System.Int32)v;
        }

        static StackObject* AssignFromStack_Exp_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @Exp = ptr_of_this_method->Value;
            ((GameInit.Game.GiftDef)o).Exp = @Exp;
            return ptr_of_this_method;
        }

        static object get_ExpHalo_1(ref object o)
        {
            return ((GameInit.Game.GiftDef)o).ExpHalo;
        }

        static StackObject* CopyToStack_ExpHalo_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.GiftDef)o).ExpHalo;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ExpHalo_1(ref object o, object v)
        {
            ((GameInit.Game.GiftDef)o).ExpHalo = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ExpHalo_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ExpHalo = ptr_of_this_method->Value;
            ((GameInit.Game.GiftDef)o).ExpHalo = @ExpHalo;
            return ptr_of_this_method;
        }

        static object get_ExpPet_2(ref object o)
        {
            return ((GameInit.Game.GiftDef)o).ExpPet;
        }

        static StackObject* CopyToStack_ExpPet_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.GiftDef)o).ExpPet;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ExpPet_2(ref object o, object v)
        {
            ((GameInit.Game.GiftDef)o).ExpPet = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ExpPet_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ExpPet = ptr_of_this_method->Value;
            ((GameInit.Game.GiftDef)o).ExpPet = @ExpPet;
            return ptr_of_this_method;
        }

        static object get_BuffId_3(ref object o)
        {
            return ((GameInit.Game.GiftDef)o).BuffId;
        }

        static StackObject* CopyToStack_BuffId_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.GiftDef)o).BuffId;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_BuffId_3(ref object o, object v)
        {
            ((GameInit.Game.GiftDef)o).BuffId = (System.String)v;
        }

        static StackObject* AssignFromStack_BuffId_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @BuffId = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.GiftDef)o).BuffId = @BuffId;
            return ptr_of_this_method;
        }

        static object get_BossBuffId_4(ref object o)
        {
            return ((GameInit.Game.GiftDef)o).BossBuffId;
        }

        static StackObject* CopyToStack_BossBuffId_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.GiftDef)o).BossBuffId;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_BossBuffId_4(ref object o, object v)
        {
            ((GameInit.Game.GiftDef)o).BossBuffId = (System.String)v;
        }

        static StackObject* AssignFromStack_BossBuffId_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @BossBuffId = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.GiftDef)o).BossBuffId = @BossBuffId;
            return ptr_of_this_method;
        }



    }
}
