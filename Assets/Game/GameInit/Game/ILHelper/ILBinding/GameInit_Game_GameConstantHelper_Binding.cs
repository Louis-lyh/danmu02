using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_GameConstantHelper_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.GameConstantHelper);
            args = new Type[]{typeof(System.Int32)};
            method = type.GetMethod("ConvertToChinese", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, ConvertToChinese_0);
            args = new Type[]{typeof(System.String)};
            method = type.GetMethod("StringToIntDic", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, StringToIntDic_1);
            args = new Type[]{};
            method = type.GetMethod("Init", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Init_2);

            field = type.GetField("StartGameTime", flag);
            app.RegisterCLRFieldGetter(field, get_StartGameTime_0);
            app.RegisterCLRFieldSetter(field, set_StartGameTime_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_StartGameTime_0, AssignFromStack_StartGameTime_0);
            field = type.GetField("RandomPlayerNum", flag);
            app.RegisterCLRFieldGetter(field, get_RandomPlayerNum_1);
            app.RegisterCLRFieldSetter(field, set_RandomPlayerNum_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_RandomPlayerNum_1, AssignFromStack_RandomPlayerNum_1);
            field = type.GetField("FieldBossReviveCd", flag);
            app.RegisterCLRFieldGetter(field, get_FieldBossReviveCd_2);
            app.RegisterCLRFieldSetter(field, set_FieldBossReviveCd_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_FieldBossReviveCd_2, AssignFromStack_FieldBossReviveCd_2);
            field = type.GetField("FieldBossOpenTime", flag);
            app.RegisterCLRFieldGetter(field, get_FieldBossOpenTime_3);
            app.RegisterCLRFieldSetter(field, set_FieldBossOpenTime_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_FieldBossOpenTime_3, AssignFromStack_FieldBossOpenTime_3);
            field = type.GetField("DanMuExp", flag);
            app.RegisterCLRFieldGetter(field, get_DanMuExp_4);
            app.RegisterCLRFieldSetter(field, set_DanMuExp_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_DanMuExp_4, AssignFromStack_DanMuExp_4);
            field = type.GetField("OpenLikeAddGame", flag);
            app.RegisterCLRFieldGetter(field, get_OpenLikeAddGame_5);
            app.RegisterCLRFieldSetter(field, set_OpenLikeAddGame_5);
            app.RegisterCLRFieldBinding(field, CopyToStack_OpenLikeAddGame_5, AssignFromStack_OpenLikeAddGame_5);
            field = type.GetField("CancelBossSkillTime", flag);
            app.RegisterCLRFieldGetter(field, get_CancelBossSkillTime_6);
            app.RegisterCLRFieldSetter(field, set_CancelBossSkillTime_6);
            app.RegisterCLRFieldBinding(field, CopyToStack_CancelBossSkillTime_6, AssignFromStack_CancelBossSkillTime_6);
            field = type.GetField("CancelBossSkillLikeMultiple", flag);
            app.RegisterCLRFieldGetter(field, get_CancelBossSkillLikeMultiple_7);
            app.RegisterCLRFieldSetter(field, set_CancelBossSkillLikeMultiple_7);
            app.RegisterCLRFieldBinding(field, CopyToStack_CancelBossSkillLikeMultiple_7, AssignFromStack_CancelBossSkillLikeMultiple_7);
            field = type.GetField("DayAndNightTime", flag);
            app.RegisterCLRFieldGetter(field, get_DayAndNightTime_8);
            app.RegisterCLRFieldSetter(field, set_DayAndNightTime_8);
            app.RegisterCLRFieldBinding(field, CopyToStack_DayAndNightTime_8, AssignFromStack_DayAndNightTime_8);
            field = type.GetField("MaxDay", flag);
            app.RegisterCLRFieldGetter(field, get_MaxDay_9);
            app.RegisterCLRFieldSetter(field, set_MaxDay_9);
            app.RegisterCLRFieldBinding(field, CopyToStack_MaxDay_9, AssignFromStack_MaxDay_9);
            field = type.GetField("WerewolfTime", flag);
            app.RegisterCLRFieldGetter(field, get_WerewolfTime_10);
            app.RegisterCLRFieldSetter(field, set_WerewolfTime_10);
            app.RegisterCLRFieldBinding(field, CopyToStack_WerewolfTime_10, AssignFromStack_WerewolfTime_10);
            field = type.GetField("ExcessiveSpeed", flag);
            app.RegisterCLRFieldGetter(field, get_ExcessiveSpeed_11);
            app.RegisterCLRFieldSetter(field, set_ExcessiveSpeed_11);
            app.RegisterCLRFieldBinding(field, CopyToStack_ExcessiveSpeed_11, AssignFromStack_ExcessiveSpeed_11);
            field = type.GetField("FieldBossTime", flag);
            app.RegisterCLRFieldGetter(field, get_FieldBossTime_12);
            app.RegisterCLRFieldSetter(field, set_FieldBossTime_12);
            app.RegisterCLRFieldBinding(field, CopyToStack_FieldBossTime_12, AssignFromStack_FieldBossTime_12);
            field = type.GetField("LgCameraFollowCurPos", flag);
            app.RegisterCLRFieldGetter(field, get_LgCameraFollowCurPos_13);
            app.RegisterCLRFieldSetter(field, set_LgCameraFollowCurPos_13);
            app.RegisterCLRFieldBinding(field, CopyToStack_LgCameraFollowCurPos_13, AssignFromStack_LgCameraFollowCurPos_13);


        }


        static StackObject* ConvertToChinese_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Int32 @number = ptr_of_this_method->Value;


            var result_of_this_method = GameInit.Game.GameConstantHelper.ConvertToChinese(@number);

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static StackObject* StringToIntDic_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.String @str = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);


            var result_of_this_method = GameInit.Game.GameConstantHelper.StringToIntDic(@str);

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static StackObject* Init_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);


            GameInit.Game.GameConstantHelper.Init();

            return __ret;
        }


        static object get_StartGameTime_0(ref object o)
        {
            return GameInit.Game.GameConstantHelper.StartGameTime;
        }

        static StackObject* CopyToStack_StartGameTime_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.StartGameTime;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_StartGameTime_0(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.StartGameTime = (System.Int32)v;
        }

        static StackObject* AssignFromStack_StartGameTime_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @StartGameTime = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.StartGameTime = @StartGameTime;
            return ptr_of_this_method;
        }

        static object get_RandomPlayerNum_1(ref object o)
        {
            return GameInit.Game.GameConstantHelper.RandomPlayerNum;
        }

        static StackObject* CopyToStack_RandomPlayerNum_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.RandomPlayerNum;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_RandomPlayerNum_1(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.RandomPlayerNum = (System.String)v;
        }

        static StackObject* AssignFromStack_RandomPlayerNum_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @RandomPlayerNum = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.GameConstantHelper.RandomPlayerNum = @RandomPlayerNum;
            return ptr_of_this_method;
        }

        static object get_FieldBossReviveCd_2(ref object o)
        {
            return GameInit.Game.GameConstantHelper.FieldBossReviveCd;
        }

        static StackObject* CopyToStack_FieldBossReviveCd_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.FieldBossReviveCd;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_FieldBossReviveCd_2(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.FieldBossReviveCd = (System.Int32)v;
        }

        static StackObject* AssignFromStack_FieldBossReviveCd_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @FieldBossReviveCd = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.FieldBossReviveCd = @FieldBossReviveCd;
            return ptr_of_this_method;
        }

        static object get_FieldBossOpenTime_3(ref object o)
        {
            return GameInit.Game.GameConstantHelper.FieldBossOpenTime;
        }

        static StackObject* CopyToStack_FieldBossOpenTime_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.FieldBossOpenTime;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_FieldBossOpenTime_3(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.FieldBossOpenTime = (System.String)v;
        }

        static StackObject* AssignFromStack_FieldBossOpenTime_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @FieldBossOpenTime = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.GameConstantHelper.FieldBossOpenTime = @FieldBossOpenTime;
            return ptr_of_this_method;
        }

        static object get_DanMuExp_4(ref object o)
        {
            return GameInit.Game.GameConstantHelper.DanMuExp;
        }

        static StackObject* CopyToStack_DanMuExp_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.DanMuExp;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_DanMuExp_4(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.DanMuExp = (System.Int32)v;
        }

        static StackObject* AssignFromStack_DanMuExp_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @DanMuExp = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.DanMuExp = @DanMuExp;
            return ptr_of_this_method;
        }

        static object get_OpenLikeAddGame_5(ref object o)
        {
            return GameInit.Game.GameConstantHelper.OpenLikeAddGame;
        }

        static StackObject* CopyToStack_OpenLikeAddGame_5(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.OpenLikeAddGame;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_OpenLikeAddGame_5(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.OpenLikeAddGame = (System.Int32)v;
        }

        static StackObject* AssignFromStack_OpenLikeAddGame_5(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @OpenLikeAddGame = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.OpenLikeAddGame = @OpenLikeAddGame;
            return ptr_of_this_method;
        }

        static object get_CancelBossSkillTime_6(ref object o)
        {
            return GameInit.Game.GameConstantHelper.CancelBossSkillTime;
        }

        static StackObject* CopyToStack_CancelBossSkillTime_6(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.CancelBossSkillTime;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_CancelBossSkillTime_6(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.CancelBossSkillTime = (System.Int32)v;
        }

        static StackObject* AssignFromStack_CancelBossSkillTime_6(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @CancelBossSkillTime = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.CancelBossSkillTime = @CancelBossSkillTime;
            return ptr_of_this_method;
        }

        static object get_CancelBossSkillLikeMultiple_7(ref object o)
        {
            return GameInit.Game.GameConstantHelper.CancelBossSkillLikeMultiple;
        }

        static StackObject* CopyToStack_CancelBossSkillLikeMultiple_7(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.CancelBossSkillLikeMultiple;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_CancelBossSkillLikeMultiple_7(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.CancelBossSkillLikeMultiple = (System.Int32)v;
        }

        static StackObject* AssignFromStack_CancelBossSkillLikeMultiple_7(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @CancelBossSkillLikeMultiple = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.CancelBossSkillLikeMultiple = @CancelBossSkillLikeMultiple;
            return ptr_of_this_method;
        }

        static object get_DayAndNightTime_8(ref object o)
        {
            return GameInit.Game.GameConstantHelper.DayAndNightTime;
        }

        static StackObject* CopyToStack_DayAndNightTime_8(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.DayAndNightTime;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_DayAndNightTime_8(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.DayAndNightTime = (System.Int32)v;
        }

        static StackObject* AssignFromStack_DayAndNightTime_8(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @DayAndNightTime = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.DayAndNightTime = @DayAndNightTime;
            return ptr_of_this_method;
        }

        static object get_MaxDay_9(ref object o)
        {
            return GameInit.Game.GameConstantHelper.MaxDay;
        }

        static StackObject* CopyToStack_MaxDay_9(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.MaxDay;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_MaxDay_9(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.MaxDay = (System.Int32)v;
        }

        static StackObject* AssignFromStack_MaxDay_9(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @MaxDay = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.MaxDay = @MaxDay;
            return ptr_of_this_method;
        }

        static object get_WerewolfTime_10(ref object o)
        {
            return GameInit.Game.GameConstantHelper.WerewolfTime;
        }

        static StackObject* CopyToStack_WerewolfTime_10(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.WerewolfTime;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_WerewolfTime_10(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.WerewolfTime = (System.Int32)v;
        }

        static StackObject* AssignFromStack_WerewolfTime_10(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @WerewolfTime = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.WerewolfTime = @WerewolfTime;
            return ptr_of_this_method;
        }

        static object get_ExcessiveSpeed_11(ref object o)
        {
            return GameInit.Game.GameConstantHelper.ExcessiveSpeed;
        }

        static StackObject* CopyToStack_ExcessiveSpeed_11(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.ExcessiveSpeed;
            __ret->ObjectType = ObjectTypes.Float;
            *(float*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ExcessiveSpeed_11(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.ExcessiveSpeed = (System.Single)v;
        }

        static StackObject* AssignFromStack_ExcessiveSpeed_11(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Single @ExcessiveSpeed = *(float*)&ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.ExcessiveSpeed = @ExcessiveSpeed;
            return ptr_of_this_method;
        }

        static object get_FieldBossTime_12(ref object o)
        {
            return GameInit.Game.GameConstantHelper.FieldBossTime;
        }

        static StackObject* CopyToStack_FieldBossTime_12(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.FieldBossTime;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_FieldBossTime_12(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.FieldBossTime = (System.Int32)v;
        }

        static StackObject* AssignFromStack_FieldBossTime_12(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @FieldBossTime = ptr_of_this_method->Value;
            GameInit.Game.GameConstantHelper.FieldBossTime = @FieldBossTime;
            return ptr_of_this_method;
        }

        static object get_LgCameraFollowCurPos_13(ref object o)
        {
            return GameInit.Game.GameConstantHelper.LgCameraFollowCurPos;
        }

        static StackObject* CopyToStack_LgCameraFollowCurPos_13(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = GameInit.Game.GameConstantHelper.LgCameraFollowCurPos;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_LgCameraFollowCurPos_13(ref object o, object v)
        {
            GameInit.Game.GameConstantHelper.LgCameraFollowCurPos = (System.String)v;
        }

        static StackObject* AssignFromStack_LgCameraFollowCurPos_13(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @LgCameraFollowCurPos = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            GameInit.Game.GameConstantHelper.LgCameraFollowCurPos = @LgCameraFollowCurPos;
            return ptr_of_this_method;
        }



    }
}
