using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_HaloDef_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.HaloDef);

            field = type.GetField("ID", flag);
            app.RegisterCLRFieldGetter(field, get_ID_0);
            app.RegisterCLRFieldSetter(field, set_ID_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_ID_0, AssignFromStack_ID_0);
            field = type.GetField("HaloName", flag);
            app.RegisterCLRFieldGetter(field, get_HaloName_1);
            app.RegisterCLRFieldSetter(field, set_HaloName_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_HaloName_1, AssignFromStack_HaloName_1);
            field = type.GetField("PreExpLimit", flag);
            app.RegisterCLRFieldGetter(field, get_PreExpLimit_2);
            app.RegisterCLRFieldSetter(field, set_PreExpLimit_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_PreExpLimit_2, AssignFromStack_PreExpLimit_2);
            field = type.GetField("ExpLimit", flag);
            app.RegisterCLRFieldGetter(field, get_ExpLimit_3);
            app.RegisterCLRFieldSetter(field, set_ExpLimit_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_ExpLimit_3, AssignFromStack_ExpLimit_3);


        }



        static object get_ID_0(ref object o)
        {
            return ((GameInit.Game.HaloDef)o).ID;
        }

        static StackObject* CopyToStack_ID_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.HaloDef)o).ID;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ID_0(ref object o, object v)
        {
            ((GameInit.Game.HaloDef)o).ID = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ID_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ID = ptr_of_this_method->Value;
            ((GameInit.Game.HaloDef)o).ID = @ID;
            return ptr_of_this_method;
        }

        static object get_HaloName_1(ref object o)
        {
            return ((GameInit.Game.HaloDef)o).HaloName;
        }

        static StackObject* CopyToStack_HaloName_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.HaloDef)o).HaloName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_HaloName_1(ref object o, object v)
        {
            ((GameInit.Game.HaloDef)o).HaloName = (System.String)v;
        }

        static StackObject* AssignFromStack_HaloName_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @HaloName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.HaloDef)o).HaloName = @HaloName;
            return ptr_of_this_method;
        }

        static object get_PreExpLimit_2(ref object o)
        {
            return ((GameInit.Game.HaloDef)o).PreExpLimit;
        }

        static StackObject* CopyToStack_PreExpLimit_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.HaloDef)o).PreExpLimit;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_PreExpLimit_2(ref object o, object v)
        {
            ((GameInit.Game.HaloDef)o).PreExpLimit = (System.Int32)v;
        }

        static StackObject* AssignFromStack_PreExpLimit_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @PreExpLimit = ptr_of_this_method->Value;
            ((GameInit.Game.HaloDef)o).PreExpLimit = @PreExpLimit;
            return ptr_of_this_method;
        }

        static object get_ExpLimit_3(ref object o)
        {
            return ((GameInit.Game.HaloDef)o).ExpLimit;
        }

        static StackObject* CopyToStack_ExpLimit_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.HaloDef)o).ExpLimit;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ExpLimit_3(ref object o, object v)
        {
            ((GameInit.Game.HaloDef)o).ExpLimit = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ExpLimit_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ExpLimit = ptr_of_this_method->Value;
            ((GameInit.Game.HaloDef)o).ExpLimit = @ExpLimit;
            return ptr_of_this_method;
        }



    }
}
