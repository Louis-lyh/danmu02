using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_SkillBossTips_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.SkillBossTips);

            field = type.GetField("RoleData", flag);
            app.RegisterCLRFieldGetter(field, get_RoleData_0);
            app.RegisterCLRFieldSetter(field, set_RoleData_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_RoleData_0, AssignFromStack_RoleData_0);
            field = type.GetField("Reward", flag);
            app.RegisterCLRFieldGetter(field, get_Reward_1);
            app.RegisterCLRFieldSetter(field, set_Reward_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_Reward_1, AssignFromStack_Reward_1);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_RoleData_0(ref object o)
        {
            return ((GameInit.Game.SkillBossTips)o).RoleData;
        }

        static StackObject* CopyToStack_RoleData_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.SkillBossTips)o).RoleData;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_RoleData_0(ref object o, object v)
        {
            ((GameInit.Game.SkillBossTips)o).RoleData = (GameInit.Game.RoleData)v;
        }

        static StackObject* AssignFromStack_RoleData_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.RoleData @RoleData = (GameInit.Game.RoleData)typeof(GameInit.Game.RoleData).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.SkillBossTips)o).RoleData = @RoleData;
            return ptr_of_this_method;
        }

        static object get_Reward_1(ref object o)
        {
            return ((GameInit.Game.SkillBossTips)o).Reward;
        }

        static StackObject* CopyToStack_Reward_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.SkillBossTips)o).Reward;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_Reward_1(ref object o, object v)
        {
            ((GameInit.Game.SkillBossTips)o).Reward = (System.Collections.Generic.Dictionary<System.String, System.Int32>)v;
        }

        static StackObject* AssignFromStack_Reward_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Int32> @Reward = (System.Collections.Generic.Dictionary<System.String, System.Int32>)typeof(System.Collections.Generic.Dictionary<System.String, System.Int32>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.SkillBossTips)o).Reward = @Reward;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.SkillBossTips();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
