using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_SoundManager_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.SoundManager);
            args = new Type[]{};
            method = type.GetMethod("Init", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Init_0);
            args = new Type[]{};
            method = type.GetMethod("Destroy", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Destroy_1);
            args = new Type[]{typeof(System.Single)};
            method = type.GetMethod("Update", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Update_2);
            args = new Type[]{};
            method = type.GetMethod("RecoveryBgm", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, RecoveryBgm_3);
            args = new Type[]{};
            method = type.GetMethod("StopBgm", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, StopBgm_4);

            field = type.GetField("IsOpenVoice", flag);
            app.RegisterCLRFieldGetter(field, get_IsOpenVoice_0);
            app.RegisterCLRFieldSetter(field, set_IsOpenVoice_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_IsOpenVoice_0, AssignFromStack_IsOpenVoice_0);
            field = type.GetField("IsOpenMusic", flag);
            app.RegisterCLRFieldGetter(field, get_IsOpenMusic_1);
            app.RegisterCLRFieldSetter(field, set_IsOpenMusic_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_IsOpenMusic_1, AssignFromStack_IsOpenMusic_1);


        }


        static StackObject* Init_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.SoundManager instance_of_this_method = (GameInit.Game.SoundManager)typeof(GameInit.Game.SoundManager).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.Init();

            return __ret;
        }

        static StackObject* Destroy_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.SoundManager instance_of_this_method = (GameInit.Game.SoundManager)typeof(GameInit.Game.SoundManager).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.Destroy();

            return __ret;
        }

        static StackObject* Update_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Single @deltaTime = *(float*)&ptr_of_this_method->Value;

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            GameInit.Game.SoundManager instance_of_this_method = (GameInit.Game.SoundManager)typeof(GameInit.Game.SoundManager).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.Update(@deltaTime);

            return __ret;
        }

        static StackObject* RecoveryBgm_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.SoundManager instance_of_this_method = (GameInit.Game.SoundManager)typeof(GameInit.Game.SoundManager).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RecoveryBgm();

            return __ret;
        }

        static StackObject* StopBgm_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.SoundManager instance_of_this_method = (GameInit.Game.SoundManager)typeof(GameInit.Game.SoundManager).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.StopBgm();

            return __ret;
        }


        static object get_IsOpenVoice_0(ref object o)
        {
            return ((GameInit.Game.SoundManager)o).IsOpenVoice;
        }

        static StackObject* CopyToStack_IsOpenVoice_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.SoundManager)o).IsOpenVoice;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method ? 1 : 0;
            return __ret + 1;
        }

        static void set_IsOpenVoice_0(ref object o, object v)
        {
            ((GameInit.Game.SoundManager)o).IsOpenVoice = (System.Boolean)v;
        }

        static StackObject* AssignFromStack_IsOpenVoice_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Boolean @IsOpenVoice = ptr_of_this_method->Value == 1;
            ((GameInit.Game.SoundManager)o).IsOpenVoice = @IsOpenVoice;
            return ptr_of_this_method;
        }

        static object get_IsOpenMusic_1(ref object o)
        {
            return ((GameInit.Game.SoundManager)o).IsOpenMusic;
        }

        static StackObject* CopyToStack_IsOpenMusic_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.SoundManager)o).IsOpenMusic;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method ? 1 : 0;
            return __ret + 1;
        }

        static void set_IsOpenMusic_1(ref object o, object v)
        {
            ((GameInit.Game.SoundManager)o).IsOpenMusic = (System.Boolean)v;
        }

        static StackObject* AssignFromStack_IsOpenMusic_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Boolean @IsOpenMusic = ptr_of_this_method->Value == 1;
            ((GameInit.Game.SoundManager)o).IsOpenMusic = @IsOpenMusic;
            return ptr_of_this_method;
        }



    }
}
