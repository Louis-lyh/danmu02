using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_ServerData_Binding_Rank_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.ServerData.Rank);

            field = type.GetField("userName", flag);
            app.RegisterCLRFieldGetter(field, get_userName_0);
            app.RegisterCLRFieldSetter(field, set_userName_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_userName_0, AssignFromStack_userName_0);
            field = type.GetField("score", flag);
            app.RegisterCLRFieldGetter(field, get_score_1);
            app.RegisterCLRFieldSetter(field, set_score_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_score_1, AssignFromStack_score_1);
            field = type.GetField("headUrl", flag);
            app.RegisterCLRFieldGetter(field, get_headUrl_2);
            app.RegisterCLRFieldSetter(field, set_headUrl_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_headUrl_2, AssignFromStack_headUrl_2);


        }



        static object get_userName_0(ref object o)
        {
            return ((GameInit.Game.ServerData.Rank)o).userName;
        }

        static StackObject* CopyToStack_userName_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Rank)o).userName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_userName_0(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Rank)o).userName = (System.String)v;
        }

        static StackObject* AssignFromStack_userName_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @userName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.Rank)o).userName = @userName;
            return ptr_of_this_method;
        }

        static object get_score_1(ref object o)
        {
            return ((GameInit.Game.ServerData.Rank)o).score;
        }

        static StackObject* CopyToStack_score_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Rank)o).score;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_score_1(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Rank)o).score = (System.Int64)v;
        }

        static StackObject* AssignFromStack_score_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @score = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Rank)o).score = @score;
            return ptr_of_this_method;
        }

        static object get_headUrl_2(ref object o)
        {
            return ((GameInit.Game.ServerData.Rank)o).headUrl;
        }

        static StackObject* CopyToStack_headUrl_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Rank)o).headUrl;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_headUrl_2(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Rank)o).headUrl = (System.String)v;
        }

        static StackObject* AssignFromStack_headUrl_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @headUrl = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.Rank)o).headUrl = @headUrl;
            return ptr_of_this_method;
        }



    }
}
