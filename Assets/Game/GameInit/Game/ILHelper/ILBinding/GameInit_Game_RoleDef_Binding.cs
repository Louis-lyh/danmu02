using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_RoleDef_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.RoleDef);

            field = type.GetField("ResId", flag);
            app.RegisterCLRFieldGetter(field, get_ResId_0);
            app.RegisterCLRFieldSetter(field, set_ResId_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_ResId_0, AssignFromStack_ResId_0);
            field = type.GetField("SpeciesName", flag);
            app.RegisterCLRFieldGetter(field, get_SpeciesName_1);
            app.RegisterCLRFieldSetter(field, set_SpeciesName_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_SpeciesName_1, AssignFromStack_SpeciesName_1);
            field = type.GetField("Attack", flag);
            app.RegisterCLRFieldGetter(field, get_Attack_2);
            app.RegisterCLRFieldSetter(field, set_Attack_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_Attack_2, AssignFromStack_Attack_2);


        }



        static object get_ResId_0(ref object o)
        {
            return ((GameInit.Game.RoleDef)o).ResId;
        }

        static StackObject* CopyToStack_ResId_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleDef)o).ResId;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_ResId_0(ref object o, object v)
        {
            ((GameInit.Game.RoleDef)o).ResId = (System.Int32)v;
        }

        static StackObject* AssignFromStack_ResId_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @ResId = ptr_of_this_method->Value;
            ((GameInit.Game.RoleDef)o).ResId = @ResId;
            return ptr_of_this_method;
        }

        static object get_SpeciesName_1(ref object o)
        {
            return ((GameInit.Game.RoleDef)o).SpeciesName;
        }

        static StackObject* CopyToStack_SpeciesName_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleDef)o).SpeciesName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_SpeciesName_1(ref object o, object v)
        {
            ((GameInit.Game.RoleDef)o).SpeciesName = (System.String)v;
        }

        static StackObject* AssignFromStack_SpeciesName_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @SpeciesName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.RoleDef)o).SpeciesName = @SpeciesName;
            return ptr_of_this_method;
        }

        static object get_Attack_2(ref object o)
        {
            return ((GameInit.Game.RoleDef)o).Attack;
        }

        static StackObject* CopyToStack_Attack_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleDef)o).Attack;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_Attack_2(ref object o, object v)
        {
            ((GameInit.Game.RoleDef)o).Attack = (System.Int32)v;
        }

        static StackObject* AssignFromStack_Attack_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @Attack = ptr_of_this_method->Value;
            ((GameInit.Game.RoleDef)o).Attack = @Attack;
            return ptr_of_this_method;
        }



    }
}
