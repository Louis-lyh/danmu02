using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_PetData_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.PetData);

            field = type.GetField("_owner", flag);
            app.RegisterCLRFieldGetter(field, get__owner_0);
            app.RegisterCLRFieldSetter(field, set__owner_0);
            app.RegisterCLRFieldBinding(field, CopyToStack__owner_0, AssignFromStack__owner_0);


        }



        static object get__owner_0(ref object o)
        {
            return ((GameInit.Game.PetData)o)._owner;
        }

        static StackObject* CopyToStack__owner_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.PetData)o)._owner;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set__owner_0(ref object o, object v)
        {
            ((GameInit.Game.PetData)o)._owner = (GameInit.Game.FighterUnit)v;
        }

        static StackObject* AssignFromStack__owner_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.FighterUnit @_owner = (GameInit.Game.FighterUnit)typeof(GameInit.Game.FighterUnit).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.PetData)o)._owner = @_owner;
            return ptr_of_this_method;
        }



    }
}
