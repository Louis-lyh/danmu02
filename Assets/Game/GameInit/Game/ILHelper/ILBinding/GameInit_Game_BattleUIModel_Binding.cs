using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_BattleUIModel_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            Type[] args;
            Type type = typeof(GameInit.Game.BattleUIModel);
            args = new Type[]{typeof(System.Action<GameInit.Game.FighterUnit>)};
            method = type.GetMethod("AddRefreshUnitBlood", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, AddRefreshUnitBlood_0);
            args = new Type[]{typeof(System.Action<System.Int64>)};
            method = type.GetMethod("AddDeleteUnitBlood", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, AddDeleteUnitBlood_1);
            args = new Type[]{typeof(System.Action<GameInit.Game.FighterUnit, System.Boolean>)};
            method = type.GetMethod("AddSetCollectProgress", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, AddSetCollectProgress_2);
            args = new Type[]{typeof(System.Action<GameInit.Game.FighterUnit, System.Single>)};
            method = type.GetMethod("AddUpdateCollectProgress", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, AddUpdateCollectProgress_3);
            args = new Type[]{};
            method = type.GetMethod("RemoveRefreshUnitBlood", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, RemoveRefreshUnitBlood_4);
            args = new Type[]{};
            method = type.GetMethod("RemoveDeleteUnitBlood", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, RemoveDeleteUnitBlood_5);
            args = new Type[]{};
            method = type.GetMethod("RemoveSetCollectProgress", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, RemoveSetCollectProgress_6);
            args = new Type[]{};
            method = type.GetMethod("RemoveUpdateCollectProgress", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, RemoveUpdateCollectProgress_7);
            args = new Type[]{typeof(System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>), typeof(GameInit.Game.VictoryRankingType)};
            method = type.GetMethod("OpenVictoryRanking", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, OpenVictoryRanking_8);
            args = new Type[]{};
            method = type.GetMethod("Init", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Init_9);


        }


        static StackObject* AddRefreshUnitBlood_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Action<GameInit.Game.FighterUnit> @callBack = (System.Action<GameInit.Game.FighterUnit>)typeof(System.Action<GameInit.Game.FighterUnit>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)8);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.AddRefreshUnitBlood(@callBack);

            return __ret;
        }

        static StackObject* AddDeleteUnitBlood_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Action<System.Int64> @callBack = (System.Action<System.Int64>)typeof(System.Action<System.Int64>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)8);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.AddDeleteUnitBlood(@callBack);

            return __ret;
        }

        static StackObject* AddSetCollectProgress_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Action<GameInit.Game.FighterUnit, System.Boolean> @callBack = (System.Action<GameInit.Game.FighterUnit, System.Boolean>)typeof(System.Action<GameInit.Game.FighterUnit, System.Boolean>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)8);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.AddSetCollectProgress(@callBack);

            return __ret;
        }

        static StackObject* AddUpdateCollectProgress_3(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Action<GameInit.Game.FighterUnit, System.Single> @callBack = (System.Action<GameInit.Game.FighterUnit, System.Single>)typeof(System.Action<GameInit.Game.FighterUnit, System.Single>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)8);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.AddUpdateCollectProgress(@callBack);

            return __ret;
        }

        static StackObject* RemoveRefreshUnitBlood_4(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RemoveRefreshUnitBlood();

            return __ret;
        }

        static StackObject* RemoveDeleteUnitBlood_5(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RemoveDeleteUnitBlood();

            return __ret;
        }

        static StackObject* RemoveSetCollectProgress_6(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RemoveSetCollectProgress();

            return __ret;
        }

        static StackObject* RemoveUpdateCollectProgress_7(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.RemoveUpdateCollectProgress();

            return __ret;
        }

        static StackObject* OpenVictoryRanking_8(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 3);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.VictoryRankingType @type = (GameInit.Game.VictoryRankingType)typeof(GameInit.Game.VictoryRankingType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo> @infos = (System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>)typeof(System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 3);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.OpenVictoryRanking(@infos, @type);

            return __ret;
        }

        static StackObject* Init_9(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            GameInit.Game.BattleUIModel instance_of_this_method = (GameInit.Game.BattleUIModel)typeof(GameInit.Game.BattleUIModel).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            instance_of_this_method.Init();

            return __ret;
        }



    }
}
