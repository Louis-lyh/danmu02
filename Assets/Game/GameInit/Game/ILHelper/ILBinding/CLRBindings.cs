using System;
using System.Collections.Generic;
using System.Reflection;

namespace ILRuntime.Runtime.Generated
{
    class CLRBindings
    {

//will auto register in unity
#if UNITY_5_3_OR_NEWER
        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
#endif
        static private void RegisterBindingAction()
        {
            ILRuntime.Runtime.CLRBinding.CLRBindingUtils.RegisterBindingAction(Initialize);
        }

        internal static ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector3> s_UnityEngine_Vector3_Binding_Binder = null;
        internal static ILRuntime.Runtime.Enviorment.ValueTypeBinder<GameInit.Framework.Vector2> s_GameInit_Framework_Vector2_Binding_Binder = null;
        internal static ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Quaternion> s_UnityEngine_Quaternion_Binding_Binder = null;
        internal static ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector2Int> s_UnityEngine_Vector2Int_Binding_Binder = null;
        internal static ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector3Int> s_UnityEngine_Vector3Int_Binding_Binder = null;

        /// <summary>
        /// Initialize the CLR binding, please invoke this AFTER CLR Redirection registration
        /// </summary>
        public static void Initialize(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            UnityEngine_Input_Binding.Register(app);
            GameInit_Framework_TimerHeap_Binding.Register(app);
            GameInit_Game_SkillBossTips_Binding.Register(app);
            GameInit_Game_RoleData_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_Int32_Binding.Register(app);
            GameInit_Game_DataModelBase_1_GameNoticeModel_Binding.Register(app);
            GameInit_Game_EventDispatcher_Binding.Register(app);
            System_Object_Binding.Register(app);
            GameInit_Game_GameConstantHelper_Binding.Register(app);
            System_String_Binding.Register(app);
            System_Runtime_CompilerServices_AsyncVoidMethodBuilder_Binding.Register(app);
            GameInit_Game_DataModelBase_1_BattleUIModel_Binding.Register(app);
            UnityEngine_Component_Binding.Register(app);
            UnityEngine_CanvasGroup_Binding.Register(app);
            GameInit_Game_RoleResConfig_Binding.Register(app);
            UnityEngine_UI_Image_Binding.Register(app);
            UnityEngine_Object_Binding.Register(app);
            UnityEngine_Color_Binding.Register(app);
            UnityEngine_UI_Graphic_Binding.Register(app);
            DG_Tweening_ShortcutExtensions46_Binding.Register(app);
            GameInit_Game_Singleton_1_BattleManager_Binding.Register(app);
            GameInit_Game_BattleManager_Binding.Register(app);
            UnityEngine_GameObject_Binding.Register(app);
            UnityEngine_UI_Text_Binding.Register(app);
            GameInit_Framework_Singleton_1_SpriteAtlasMgr_Binding.Register(app);
            GameInit_Game_RoleResDef_Binding.Register(app);
            GameInit_Framework_SpriteAtlasMgr_Binding.Register(app);
            UnityEngine_Sprite_Binding.Register(app);
            UnityEngine_Rect_Binding.Register(app);
            UnityEngine_Vector2_Binding.Register(app);
            UnityEngine_RectTransform_Binding.Register(app);
            GameInit_Game_GameNoticeModel_Binding.Register(app);
            DG_Tweening_TweenSettingsExtensions_Binding.Register(app);
            DG_Tweening_DOTween_Binding.Register(app);
            UnityEngine_Vector3_Binding.Register(app);
            UnityEngine_Transform_Binding.Register(app);
            GameInit_Game_BaseEvent_Binding.Register(app);
            GameInit_Framework_Logger_Binding.Register(app);
            GameInit_Game_DataModelBase_1_BattleModel_Binding.Register(app);
            GameInit_Game_BattleModel_Binding.Register(app);
            DG_Tweening_TweenExtensions_Binding.Register(app);
            DG_Tweening_ShortcutExtensions_Binding.Register(app);
            GameInit_Game_FighterUnit_Binding.Register(app);
            GameInit_Game_FighterUnitData_Binding.Register(app);
            GameInit_Game_SkillData_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_Sprite_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_Sprite_Binding_Awaiter_Binding.Register(app);
            UnityEngine_Mathf_Binding.Register(app);
            System_Single_Binding.Register(app);
            System_Collections_Generic_List_1_ILTypeInstance_Binding.Register(app);
            System_Collections_Generic_List_1_FighterUnitRole_Binding.Register(app);
            GameInit_Game_FighterUnitRole_Binding.Register(app);
            GameInit_Game_ServerData_Binding_LacEventExecute_Binding.Register(app);
            System_Int64_Binding.Register(app);
            System_Int32_Binding.Register(app);
            System_Action_Binding.Register(app);
            GameInit_Game_VictoryRankingInfo_Binding.Register(app);
            System_Collections_Generic_List_1_GameObject_Binding.Register(app);
            System_Collections_Generic_List_1_VictoryRankingInfo_Binding.Register(app);
            UnityEngine_UI_LayoutRebuilder_Binding.Register(app);
            UnityEngine_Application_Binding.Register(app);
            System_Array_Binding.Register(app);
            GameInit_Game_ServerManager_Binding.Register(app);
            GameInit_Game_BattleUIModel_Binding.Register(app);
            System_DateTime_Binding.Register(app);
            UnityEngine_Random_Binding.Register(app);
            System_Action_1_Int64_Binding.Register(app);
            System_Math_Binding.Register(app);
            GameInit_Game_RoleDef_Binding.Register(app);
            System_Action_1_Int32_Binding.Register(app);
            System_Collections_Generic_List_1_RoleData_Binding.Register(app);
            System_Collections_Generic_List_1_Transform_Binding.Register(app);
            System_Collections_Generic_List_1_Transform_Binding_Enumerator_Binding.Register(app);
            System_IDisposable_Binding.Register(app);
            GameInit_Game_UnitDataBase_Binding.Register(app);
            GameInit_Game_FighterBehaviourData_Binding.Register(app);
            GameInit_Game_Singleton_1_ServerTimerManager_Binding.Register(app);
            GameInit_Game_ServerTimerManager_Binding.Register(app);
            System_TimeSpan_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_Binding_Awaiter_Binding.Register(app);
            GameInit_Game_UnitBase_Binding.Register(app);
            GameInit_Game_PetData_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_Int64_ILTypeInstance_Binding.Register(app);
            UnityEngine_UI_InputField_Binding.Register(app);
            UnityEngine_Events_UnityEvent_1_String_Binding.Register(app);
            System_Text_StringBuilder_Binding.Register(app);
            GameInit_Framework_StringExtensions_Binding.Register(app);
            UnityEngine_UI_ScrollRect_Binding.Register(app);
            UnityEngine_UI_Toggle_Binding.Register(app);
            UnityEngine_Events_UnityEvent_1_Boolean_Binding.Register(app);
            System_Char_Binding.Register(app);
            System_Collections_Generic_List_1_Action_Binding.Register(app);
            GameInit_Game_EdgeBubbleInfo_Binding.Register(app);
            System_Console_Binding.Register(app);
            System_Collections_Generic_List_1_HorseRaceLampInfo_Binding.Register(app);
            System_Collections_Generic_List_1_List_1_Transform_Binding.Register(app);
            GameInit_Game_HorseRaceLampInfo_Binding.Register(app);
            System_Collections_Generic_Queue_1_GameObject_Binding.Register(app);
            GameInit_Game_ServerData_Binding_Rank_Binding.Register(app);
            GameInit_Game_DataModelBase_1_RankInfo_Binding.Register(app);
            GameInit_Game_RankInfo_Binding.Register(app);
            System_Collections_Generic_List_1_GameInit_Game_ServerData_Binding_Rank_Binding.Register(app);
            UnityEngine_Bounds_Binding.Register(app);
            GameInit_Game_HaloDef_Binding.Register(app);
            GameInit_Game_PetDef_Binding.Register(app);
            GameInit_Game_RoleConfig_Binding.Register(app);
            GameInit_Game_TopPlayerInfo_Binding.Register(app);
            System_Collections_Generic_List_1_TopPlayerInfo_Binding.Register(app);
            Cysharp_Text_ZString_Binding.Register(app);
            GameInit_Game_GiftConfig_Binding.Register(app);
            GameInit_Game_GiftDef_Binding.Register(app);
            GameInit_Game_FighterUnitBoss_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_Int32_Int32_Binding.Register(app);
            System_Linq_Enumerable_Binding.Register(app);
            System_Collections_Generic_List_1_Int32_Binding.Register(app);
            GameInit_Game_VictoryInfo_Binding.Register(app);
            GameInit_Game_ServerData_Binding_StopGameData_Binding.Register(app);
            GameInit_Game_ServerData_Binding_ExtensionData_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_Dictionary_2_String_Int32_Binding.Register(app);
            System_Collections_Generic_List_1_String_Binding.Register(app);
            System_Collections_Generic_List_1_GameInit_Game_ServerData_Binding_Settlement_Binding.Register(app);
            GameInit_Game_ServerData_Binding_Settlement_Binding.Register(app);
            GameInit_Game_ServerData_Binding_StopGameSendData_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_Int64_Binding.Register(app);
            GameInit_Game_Singleton_1_MonsterManager_Binding.Register(app);
            GameInit_Game_MonsterManager_Binding.Register(app);
            GameInit_Game_Singleton_1_FruitManager_Binding.Register(app);
            GameInit_Game_FruitManager_Binding.Register(app);
            GameInit_Game_UnitRefresher_1_MonsterManager_Binding.Register(app);
            GameInit_Game_UnitRefresher_1_FruitManager_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_Int32_ILTypeInstance_Binding.Register(app);
            GameInit_Framework_ByteUtil_Binding.Register(app);
            UnityEngine_ColorUtility_Binding.Register(app);
            UnityEngine_Shader_Binding.Register(app);
            System_Action_1_Single_Binding.Register(app);
            GameInit_Framework_Singleton_1_RVOManager_Binding.Register(app);
            GameInit_Framework_RVOManager_Binding.Register(app);
            UnityEngine_Animation_Binding.Register(app);
            UnityEngine_Behaviour_Binding.Register(app);
            GameInit_Framework_ResourceManager_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_GameObject_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_GameObject_Binding_Awaiter_Binding.Register(app);
            UnityEngine_Quaternion_Binding.Register(app);
            GameInit_Game_GiftEffectListener_Binding.Register(app);
            DG_Tweening_ShortcutExtensions43_Binding.Register(app);
            UnityEngine_SpriteRenderer_Binding.Register(app);
            GameInit_Game_Singleton_1_SoundManager_Binding.Register(app);
            GameInit_Game_SoundManager_Binding.Register(app);
            UnityEngine_Time_Binding.Register(app);
            UnityEngine_SceneManagement_SceneManager_Binding.Register(app);
            GameInit_Game_Singleton_1_BattleInfoManager_Binding.Register(app);
            GameInit_Game_BattleInfoManager_Binding.Register(app);
            GameInit_Game_Singleton_1_CameraManager_Binding.Register(app);
            GameInit_Game_CameraManager_Binding.Register(app);
            GameInit_Framework_ResPathUtils_Binding.Register(app);
            GameInit_Game_Singleton_1_GameConfigManager_Binding.Register(app);
            GameInit_Game_GameConfigManager_Binding.Register(app);
            GameInit_Game_SkillConfig_Binding.Register(app);
            GameInit_Game_EffectConfig_Binding.Register(app);
            GameInit_Game_ConstantConfig_Binding.Register(app);
            GameInit_Game_RoleAnimationTimeConfig_Binding.Register(app);
            GameInit_Game_BuffConfig_Binding.Register(app);
            GameInit_Game_ShakeConfig_Binding.Register(app);
            GameInit_Game_HaloConfig_Binding.Register(app);
            GameInit_Game_PetConfig_Binding.Register(app);
            GameInit_Game_DropRewardConfig_Binding.Register(app);
            GameInit_Game_SoundVolConfig_Binding.Register(app);
            System_Environment_Binding.Register(app);
            ILRuntime_Runtime_Extensions_Binding.Register(app);
            UnityEngine_PlayerPrefs_Binding.Register(app);
            LitJson_JsonMapper_Binding.Register(app);
            System_Exception_Binding.Register(app);
            System_Activator_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_String_Binding.Register(app);
            System_Action_2_Int32_Action_Binding.Register(app);
            UnityEngine_Camera_Binding.Register(app);
            UnityEngine_UI_RawImage_Binding.Register(app);
            UnityEngine_Resources_Binding.Register(app);
            System_Type_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_Int32_ILTypeInstance_Binding_Enumerator_Binding.Register(app);
            System_Collections_Generic_KeyValuePair_2_Int32_ILTypeInstance_Binding.Register(app);
            System_Reflection_MemberInfo_Binding.Register(app);
            System_Reflection_FieldInfo_Binding.Register(app);
            System_Convert_Binding.Register(app);
            Cysharp_Threading_Tasks_CompilerServices_AsyncUniTaskMethodBuilder_1_Sprite_Binding.Register(app);
            UnityEngine_Texture_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_Sprite_Binding.Register(app);
            UnityEngine_Networking_UnityWebRequestTexture_Binding.Register(app);
            UnityEngine_Networking_UnityWebRequest_Binding.Register(app);
            Cysharp_Threading_Tasks_UnityAsyncExtensions_Binding.Register(app);
            Cysharp_Threading_Tasks_UnityAsyncExtensions_Binding_UnityWebRequestAsyncOperationAwaiter_Binding.Register(app);
            UnityEngine_Networking_DownloadHandlerTexture_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_List_1_Transform_Binding.Register(app);
            GameInit_Framework_Singleton_1_UserGroup_Binding.Register(app);
            GameInit_Framework_UserGroup_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_ILTypeInstance_Binding.Register(app);
            Cysharp_Threading_Tasks_CompilerServices_AsyncUniTaskMethodBuilder_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_ILTypeInstance_Binding_Enumerator_Binding.Register(app);
            System_Collections_Generic_KeyValuePair_2_String_ILTypeInstance_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_TextAsset_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_TextAsset_Binding_Awaiter_Binding.Register(app);
            UnityEngine_TextAsset_Binding.Register(app);
            GameInit_Framework_Singleton_1_DownloadMgr_Binding.Register(app);
            GameInit_Framework_DownloadMgr_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_Action_1_ILTypeInstance_Binding.Register(app);
            System_Action_1_ILTypeInstance_Binding.Register(app);
            GameInit_Framework_LocationManager_Binding.Register(app);
            GameInit_Framework_LocationText_Binding.Register(app);
            UnityEngine_UI_Button_Binding.Register(app);
            UnityEngine_Events_UnityEvent_Binding.Register(app);
            UnityEngine_UI_Shadow_Binding.Register(app);
            System_Collections_IEnumerator_Binding.Register(app);
            UnityEngine_Screen_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_Material_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_Material_Binding_Awaiter_Binding.Register(app);
            Cysharp_Threading_Tasks_CompilerServices_AsyncUniTaskMethodBuilder_1_String_Binding.Register(app);
            System_Security_Cryptography_X509Certificates_X509Chain_Binding.Register(app);
            System_Security_Cryptography_X509Certificates_X509ChainStatus_Binding.Register(app);
            System_Security_Cryptography_X509Certificates_X509ChainPolicy_Binding.Register(app);
            System_Text_Encoding_Binding.Register(app);
            UnityEngine_Networking_UploadHandlerRaw_Binding.Register(app);
            UnityEngine_Networking_DownloadHandlerBuffer_Binding.Register(app);
            GameInit_Game_WebRequestCertificate_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_String_String_Binding_Enumerator_Binding.Register(app);
            System_Collections_Generic_KeyValuePair_2_String_String_Binding.Register(app);
            UnityEngine_Networking_DownloadHandler_Binding.Register(app);
            LitJson_JsonData_Binding.Register(app);
            System_Collections_Generic_ICollection_1_String_Binding.Register(app);
            System_Net_ServicePointManager_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_AsyncOperation_Binding.Register(app);
            Cysharp_Threading_Tasks_UniTask_1_AsyncOperation_Binding_Awaiter_Binding.Register(app);
            System_Collections_Generic_List_1_Button_Binding.Register(app);
            System_Collections_Generic_List_1_InputField_Binding.Register(app);
            UnityEngine_Events_UnityEventBase_Binding.Register(app);
            System_Collections_Generic_Queue_1_ILTypeInstance_Binding.Register(app);
            UnityEngine_Debug_Binding.Register(app);
            System_Collections_Generic_List_1_ILTypeInstance_Binding_Enumerator_Binding.Register(app);
            UnityEngine_Events_UnityEvent_1_Vector2_Binding.Register(app);
            System_Collections_Generic_List_1_UnityAction_1_Vector2_Binding.Register(app);
            UnityEngine_Canvas_Binding.Register(app);
            UnityEngine_EventSystems_PointerEventData_Binding.Register(app);
            System_Collections_Generic_List_1_UnityAction_1_Vector2_Binding_Enumerator_Binding.Register(app);
            UnityEngine_Events_UnityAction_1_Vector2_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_UInt32_ILTypeInstance_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_Int32_Stack_1_UInt32_Binding.Register(app);
            UnityEngine_UI_CanvasScaler_Binding.Register(app);
            System_UInt32_Binding.Register(app);
            System_Collections_Generic_Stack_1_UInt32_Binding.Register(app);
            System_Collections_Generic_Dictionary_2_UInt32_ILTypeInstance_Binding_Enumerator_Binding.Register(app);
            System_Collections_Generic_KeyValuePair_2_UInt32_ILTypeInstance_Binding.Register(app);

            ILRuntime.CLR.TypeSystem.CLRType __clrType = null;
            __clrType = (ILRuntime.CLR.TypeSystem.CLRType)app.GetType (typeof(UnityEngine.Vector3));
            s_UnityEngine_Vector3_Binding_Binder = __clrType.ValueTypeBinder as ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector3>;
            __clrType = (ILRuntime.CLR.TypeSystem.CLRType)app.GetType (typeof(GameInit.Framework.Vector2));
            s_GameInit_Framework_Vector2_Binding_Binder = __clrType.ValueTypeBinder as ILRuntime.Runtime.Enviorment.ValueTypeBinder<GameInit.Framework.Vector2>;
            __clrType = (ILRuntime.CLR.TypeSystem.CLRType)app.GetType (typeof(UnityEngine.Quaternion));
            s_UnityEngine_Quaternion_Binding_Binder = __clrType.ValueTypeBinder as ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Quaternion>;
            __clrType = (ILRuntime.CLR.TypeSystem.CLRType)app.GetType (typeof(UnityEngine.Vector2Int));
            s_UnityEngine_Vector2Int_Binding_Binder = __clrType.ValueTypeBinder as ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector2Int>;
            __clrType = (ILRuntime.CLR.TypeSystem.CLRType)app.GetType (typeof(UnityEngine.Vector3Int));
            s_UnityEngine_Vector3Int_Binding_Binder = __clrType.ValueTypeBinder as ILRuntime.Runtime.Enviorment.ValueTypeBinder<UnityEngine.Vector3Int>;
        }

        /// <summary>
        /// Release the CLR binding, please invoke this BEFORE ILRuntime Appdomain destroy
        /// </summary>
        public static void Shutdown(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            s_UnityEngine_Vector3_Binding_Binder = null;
            s_GameInit_Framework_Vector2_Binding_Binder = null;
            s_UnityEngine_Quaternion_Binding_Binder = null;
            s_UnityEngine_Vector2Int_Binding_Binder = null;
            s_UnityEngine_Vector3Int_Binding_Binder = null;
        }
    }
}
