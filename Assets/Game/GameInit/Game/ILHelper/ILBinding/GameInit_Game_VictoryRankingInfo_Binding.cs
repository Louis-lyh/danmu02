using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_VictoryRankingInfo_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.VictoryRankingInfo);

            field = type.GetField("Name", flag);
            app.RegisterCLRFieldGetter(field, get_Name_0);
            app.RegisterCLRFieldSetter(field, set_Name_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_Name_0, AssignFromStack_Name_0);
            field = type.GetField("CurScore", flag);
            app.RegisterCLRFieldGetter(field, get_CurScore_1);
            app.RegisterCLRFieldSetter(field, set_CurScore_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_CurScore_1, AssignFromStack_CurScore_1);
            field = type.GetField("HeadUrl", flag);
            app.RegisterCLRFieldGetter(field, get_HeadUrl_2);
            app.RegisterCLRFieldSetter(field, set_HeadUrl_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_HeadUrl_2, AssignFromStack_HeadUrl_2);
            field = type.GetField("Level", flag);
            app.RegisterCLRFieldGetter(field, get_Level_3);
            app.RegisterCLRFieldSetter(field, set_Level_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_Level_3, AssignFromStack_Level_3);
            field = type.GetField("Species", flag);
            app.RegisterCLRFieldGetter(field, get_Species_4);
            app.RegisterCLRFieldSetter(field, set_Species_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_Species_4, AssignFromStack_Species_4);
            field = type.GetField("MonthlyScore", flag);
            app.RegisterCLRFieldGetter(field, get_MonthlyScore_5);
            app.RegisterCLRFieldSetter(field, set_MonthlyScore_5);
            app.RegisterCLRFieldBinding(field, CopyToStack_MonthlyScore_5, AssignFromStack_MonthlyScore_5);
            field = type.GetField("MonthlyOrder", flag);
            app.RegisterCLRFieldGetter(field, get_MonthlyOrder_6);
            app.RegisterCLRFieldSetter(field, set_MonthlyOrder_6);
            app.RegisterCLRFieldBinding(field, CopyToStack_MonthlyOrder_6, AssignFromStack_MonthlyOrder_6);
            field = type.GetField("WinStreak", flag);
            app.RegisterCLRFieldGetter(field, get_WinStreak_7);
            app.RegisterCLRFieldSetter(field, set_WinStreak_7);
            app.RegisterCLRFieldBinding(field, CopyToStack_WinStreak_7, AssignFromStack_WinStreak_7);
            field = type.GetField("Order", flag);
            app.RegisterCLRFieldGetter(field, get_Order_8);
            app.RegisterCLRFieldSetter(field, set_Order_8);
            app.RegisterCLRFieldBinding(field, CopyToStack_Order_8, AssignFromStack_Order_8);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_Name_0(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).Name;
        }

        static StackObject* CopyToStack_Name_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).Name;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_Name_0(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).Name = (System.String)v;
        }

        static StackObject* AssignFromStack_Name_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @Name = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.VictoryRankingInfo)o).Name = @Name;
            return ptr_of_this_method;
        }

        static object get_CurScore_1(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).CurScore;
        }

        static StackObject* CopyToStack_CurScore_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).CurScore;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_CurScore_1(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).CurScore = (System.Int64)v;
        }

        static StackObject* AssignFromStack_CurScore_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @CurScore = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).CurScore = @CurScore;
            return ptr_of_this_method;
        }

        static object get_HeadUrl_2(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).HeadUrl;
        }

        static StackObject* CopyToStack_HeadUrl_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).HeadUrl;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_HeadUrl_2(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).HeadUrl = (System.String)v;
        }

        static StackObject* AssignFromStack_HeadUrl_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @HeadUrl = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.VictoryRankingInfo)o).HeadUrl = @HeadUrl;
            return ptr_of_this_method;
        }

        static object get_Level_3(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).Level;
        }

        static StackObject* CopyToStack_Level_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).Level;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_Level_3(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).Level = (System.Int32)v;
        }

        static StackObject* AssignFromStack_Level_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @Level = ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).Level = @Level;
            return ptr_of_this_method;
        }

        static object get_Species_4(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).Species;
        }

        static StackObject* CopyToStack_Species_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).Species;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_Species_4(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).Species = (System.String)v;
        }

        static StackObject* AssignFromStack_Species_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @Species = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.VictoryRankingInfo)o).Species = @Species;
            return ptr_of_this_method;
        }

        static object get_MonthlyScore_5(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).MonthlyScore;
        }

        static StackObject* CopyToStack_MonthlyScore_5(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).MonthlyScore;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_MonthlyScore_5(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).MonthlyScore = (System.Int64)v;
        }

        static StackObject* AssignFromStack_MonthlyScore_5(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @MonthlyScore = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).MonthlyScore = @MonthlyScore;
            return ptr_of_this_method;
        }

        static object get_MonthlyOrder_6(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).MonthlyOrder;
        }

        static StackObject* CopyToStack_MonthlyOrder_6(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).MonthlyOrder;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_MonthlyOrder_6(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).MonthlyOrder = (System.Int32)v;
        }

        static StackObject* AssignFromStack_MonthlyOrder_6(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @MonthlyOrder = ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).MonthlyOrder = @MonthlyOrder;
            return ptr_of_this_method;
        }

        static object get_WinStreak_7(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).WinStreak;
        }

        static StackObject* CopyToStack_WinStreak_7(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).WinStreak;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_WinStreak_7(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).WinStreak = (System.Int32)v;
        }

        static StackObject* AssignFromStack_WinStreak_7(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @WinStreak = ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).WinStreak = @WinStreak;
            return ptr_of_this_method;
        }

        static object get_Order_8(ref object o)
        {
            return ((GameInit.Game.VictoryRankingInfo)o).Order;
        }

        static StackObject* CopyToStack_Order_8(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryRankingInfo)o).Order;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_Order_8(ref object o, object v)
        {
            ((GameInit.Game.VictoryRankingInfo)o).Order = (System.Int32)v;
        }

        static StackObject* AssignFromStack_Order_8(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @Order = ptr_of_this_method->Value;
            ((GameInit.Game.VictoryRankingInfo)o).Order = @Order;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.VictoryRankingInfo();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
