using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_ServerData_Binding_Settlement_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.ServerData.Settlement);

            field = type.GetField("monthlyScore", flag);
            app.RegisterCLRFieldGetter(field, get_monthlyScore_0);
            app.RegisterCLRFieldSetter(field, set_monthlyScore_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_monthlyScore_0, AssignFromStack_monthlyScore_0);
            field = type.GetField("rank", flag);
            app.RegisterCLRFieldGetter(field, get_rank_1);
            app.RegisterCLRFieldSetter(field, set_rank_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_rank_1, AssignFromStack_rank_1);
            field = type.GetField("winStreak", flag);
            app.RegisterCLRFieldGetter(field, get_winStreak_2);
            app.RegisterCLRFieldSetter(field, set_winStreak_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_winStreak_2, AssignFromStack_winStreak_2);
            field = type.GetField("userId", flag);
            app.RegisterCLRFieldGetter(field, get_userId_3);
            app.RegisterCLRFieldSetter(field, set_userId_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_userId_3, AssignFromStack_userId_3);
            field = type.GetField("userName", flag);
            app.RegisterCLRFieldGetter(field, get_userName_4);
            app.RegisterCLRFieldSetter(field, set_userName_4);
            app.RegisterCLRFieldBinding(field, CopyToStack_userName_4, AssignFromStack_userName_4);
            field = type.GetField("camp", flag);
            app.RegisterCLRFieldGetter(field, get_camp_5);
            app.RegisterCLRFieldSetter(field, set_camp_5);
            app.RegisterCLRFieldBinding(field, CopyToStack_camp_5, AssignFromStack_camp_5);
            field = type.GetField("score", flag);
            app.RegisterCLRFieldGetter(field, get_score_6);
            app.RegisterCLRFieldSetter(field, set_score_6);
            app.RegisterCLRFieldBinding(field, CopyToStack_score_6, AssignFromStack_score_6);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_monthlyScore_0(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).monthlyScore;
        }

        static StackObject* CopyToStack_monthlyScore_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).monthlyScore;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_monthlyScore_0(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).monthlyScore = (System.Int64)v;
        }

        static StackObject* AssignFromStack_monthlyScore_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @monthlyScore = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Settlement)o).monthlyScore = @monthlyScore;
            return ptr_of_this_method;
        }

        static object get_rank_1(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).rank;
        }

        static StackObject* CopyToStack_rank_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).rank;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_rank_1(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).rank = (System.Int32)v;
        }

        static StackObject* AssignFromStack_rank_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @rank = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Settlement)o).rank = @rank;
            return ptr_of_this_method;
        }

        static object get_winStreak_2(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).winStreak;
        }

        static StackObject* CopyToStack_winStreak_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).winStreak;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_winStreak_2(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).winStreak = (System.Int32)v;
        }

        static StackObject* AssignFromStack_winStreak_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @winStreak = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Settlement)o).winStreak = @winStreak;
            return ptr_of_this_method;
        }

        static object get_userId_3(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).userId;
        }

        static StackObject* CopyToStack_userId_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).userId;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_userId_3(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).userId = (System.String)v;
        }

        static StackObject* AssignFromStack_userId_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @userId = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.Settlement)o).userId = @userId;
            return ptr_of_this_method;
        }

        static object get_userName_4(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).userName;
        }

        static StackObject* CopyToStack_userName_4(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).userName;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_userName_4(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).userName = (System.String)v;
        }

        static StackObject* AssignFromStack_userName_4(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @userName = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.Settlement)o).userName = @userName;
            return ptr_of_this_method;
        }

        static object get_camp_5(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).camp;
        }

        static StackObject* CopyToStack_camp_5(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).camp;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_camp_5(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).camp = (System.Int32)v;
        }

        static StackObject* AssignFromStack_camp_5(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @camp = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Settlement)o).camp = @camp;
            return ptr_of_this_method;
        }

        static object get_score_6(ref object o)
        {
            return ((GameInit.Game.ServerData.Settlement)o).score;
        }

        static StackObject* CopyToStack_score_6(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.Settlement)o).score;
            __ret->ObjectType = ObjectTypes.Long;
            *(long*)&__ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_score_6(ref object o, object v)
        {
            ((GameInit.Game.ServerData.Settlement)o).score = (System.Int64)v;
        }

        static StackObject* AssignFromStack_score_6(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int64 @score = *(long*)&ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.Settlement)o).score = @score;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.ServerData.Settlement();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
