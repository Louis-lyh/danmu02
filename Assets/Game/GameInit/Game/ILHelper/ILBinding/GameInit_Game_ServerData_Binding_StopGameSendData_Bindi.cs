using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_ServerData_Binding_StopGameSendData_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.ServerData.StopGameSendData);

            field = type.GetField("winCamp", flag);
            app.RegisterCLRFieldGetter(field, get_winCamp_0);
            app.RegisterCLRFieldSetter(field, set_winCamp_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_winCamp_0, AssignFromStack_winCamp_0);
            field = type.GetField("scores", flag);
            app.RegisterCLRFieldGetter(field, get_scores_1);
            app.RegisterCLRFieldSetter(field, set_scores_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_scores_1, AssignFromStack_scores_1);
            field = type.GetField("camps", flag);
            app.RegisterCLRFieldGetter(field, get_camps_2);
            app.RegisterCLRFieldSetter(field, set_camps_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_camps_2, AssignFromStack_camps_2);
            field = type.GetField("extensions", flag);
            app.RegisterCLRFieldGetter(field, get_extensions_3);
            app.RegisterCLRFieldSetter(field, set_extensions_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_extensions_3, AssignFromStack_extensions_3);

            args = new Type[]{};
            method = type.GetConstructor(flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Ctor_0);

        }



        static object get_winCamp_0(ref object o)
        {
            return ((GameInit.Game.ServerData.StopGameSendData)o).winCamp;
        }

        static StackObject* CopyToStack_winCamp_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.StopGameSendData)o).winCamp;
            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method;
            return __ret + 1;
        }

        static void set_winCamp_0(ref object o, object v)
        {
            ((GameInit.Game.ServerData.StopGameSendData)o).winCamp = (System.Int32)v;
        }

        static StackObject* AssignFromStack_winCamp_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Int32 @winCamp = ptr_of_this_method->Value;
            ((GameInit.Game.ServerData.StopGameSendData)o).winCamp = @winCamp;
            return ptr_of_this_method;
        }

        static object get_scores_1(ref object o)
        {
            return ((GameInit.Game.ServerData.StopGameSendData)o).scores;
        }

        static StackObject* CopyToStack_scores_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.StopGameSendData)o).scores;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_scores_1(ref object o, object v)
        {
            ((GameInit.Game.ServerData.StopGameSendData)o).scores = (System.Collections.Generic.Dictionary<System.String, System.Int64>)v;
        }

        static StackObject* AssignFromStack_scores_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Int64> @scores = (System.Collections.Generic.Dictionary<System.String, System.Int64>)typeof(System.Collections.Generic.Dictionary<System.String, System.Int64>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.StopGameSendData)o).scores = @scores;
            return ptr_of_this_method;
        }

        static object get_camps_2(ref object o)
        {
            return ((GameInit.Game.ServerData.StopGameSendData)o).camps;
        }

        static StackObject* CopyToStack_camps_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.StopGameSendData)o).camps;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_camps_2(ref object o, object v)
        {
            ((GameInit.Game.ServerData.StopGameSendData)o).camps = (System.Collections.Generic.Dictionary<System.String, System.Int32>)v;
        }

        static StackObject* AssignFromStack_camps_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Int32> @camps = (System.Collections.Generic.Dictionary<System.String, System.Int32>)typeof(System.Collections.Generic.Dictionary<System.String, System.Int32>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.StopGameSendData)o).camps = @camps;
            return ptr_of_this_method;
        }

        static object get_extensions_3(ref object o)
        {
            return ((GameInit.Game.ServerData.StopGameSendData)o).extensions;
        }

        static StackObject* CopyToStack_extensions_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.ServerData.StopGameSendData)o).extensions;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_extensions_3(ref object o, object v)
        {
            ((GameInit.Game.ServerData.StopGameSendData)o).extensions = (System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>)v;
        }

        static StackObject* AssignFromStack_extensions_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>> @extensions = (System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>)typeof(System.Collections.Generic.Dictionary<System.String, System.Collections.Generic.Dictionary<System.String, System.Int32>>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.ServerData.StopGameSendData)o).extensions = @extensions;
            return ptr_of_this_method;
        }


        static StackObject* Ctor_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* __ret = ILIntepreter.Minus(__esp, 0);

            var result_of_this_method = new GameInit.Game.ServerData.StopGameSendData();

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }


    }
}
