using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class System_Security_Cryptography_X509Certificates_X509Chain_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            MethodBase method;
            Type[] args;
            Type type = typeof(System.Security.Cryptography.X509Certificates.X509Chain);
            args = new Type[]{};
            method = type.GetMethod("get_ChainStatus", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, get_ChainStatus_0);
            args = new Type[]{};
            method = type.GetMethod("get_ChainPolicy", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, get_ChainPolicy_1);
            args = new Type[]{typeof(System.Security.Cryptography.X509Certificates.X509Certificate2)};
            method = type.GetMethod("Build", flag, null, args, null);
            app.RegisterCLRMethodRedirection(method, Build_2);


        }


        static StackObject* get_ChainStatus_0(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509Chain instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509Chain)typeof(System.Security.Cryptography.X509Certificates.X509Chain).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.ChainStatus;

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static StackObject* get_ChainPolicy_1(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 1);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509Chain instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509Chain)typeof(System.Security.Cryptography.X509Certificates.X509Chain).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.ChainPolicy;

            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static StackObject* Build_2(ILIntepreter __intp, StackObject* __esp, IList<object> __mStack, CLRMethod __method, bool isNewObj)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            StackObject* ptr_of_this_method;
            StackObject* __ret = ILIntepreter.Minus(__esp, 2);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 1);
            System.Security.Cryptography.X509Certificates.X509Certificate2 @certificate = (System.Security.Cryptography.X509Certificates.X509Certificate2)typeof(System.Security.Cryptography.X509Certificates.X509Certificate2).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            ptr_of_this_method = ILIntepreter.Minus(__esp, 2);
            System.Security.Cryptography.X509Certificates.X509Chain instance_of_this_method = (System.Security.Cryptography.X509Certificates.X509Chain)typeof(System.Security.Cryptography.X509Certificates.X509Chain).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            __intp.Free(ptr_of_this_method);

            var result_of_this_method = instance_of_this_method.Build(@certificate);

            __ret->ObjectType = ObjectTypes.Integer;
            __ret->Value = result_of_this_method ? 1 : 0;
            return __ret + 1;
        }



    }
}
