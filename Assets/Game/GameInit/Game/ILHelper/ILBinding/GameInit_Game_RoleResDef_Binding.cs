using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_RoleResDef_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.RoleResDef);

            field = type.GetField("ImageAtlas", flag);
            app.RegisterCLRFieldGetter(field, get_ImageAtlas_0);
            app.RegisterCLRFieldSetter(field, set_ImageAtlas_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_ImageAtlas_0, AssignFromStack_ImageAtlas_0);
            field = type.GetField("RoleImage", flag);
            app.RegisterCLRFieldGetter(field, get_RoleImage_1);
            app.RegisterCLRFieldSetter(field, set_RoleImage_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_RoleImage_1, AssignFromStack_RoleImage_1);
            field = type.GetField("IconAtlas", flag);
            app.RegisterCLRFieldGetter(field, get_IconAtlas_2);
            app.RegisterCLRFieldSetter(field, set_IconAtlas_2);
            app.RegisterCLRFieldBinding(field, CopyToStack_IconAtlas_2, AssignFromStack_IconAtlas_2);
            field = type.GetField("RoleIcon", flag);
            app.RegisterCLRFieldGetter(field, get_RoleIcon_3);
            app.RegisterCLRFieldSetter(field, set_RoleIcon_3);
            app.RegisterCLRFieldBinding(field, CopyToStack_RoleIcon_3, AssignFromStack_RoleIcon_3);


        }



        static object get_ImageAtlas_0(ref object o)
        {
            return ((GameInit.Game.RoleResDef)o).ImageAtlas;
        }

        static StackObject* CopyToStack_ImageAtlas_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleResDef)o).ImageAtlas;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_ImageAtlas_0(ref object o, object v)
        {
            ((GameInit.Game.RoleResDef)o).ImageAtlas = (System.String)v;
        }

        static StackObject* AssignFromStack_ImageAtlas_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @ImageAtlas = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.RoleResDef)o).ImageAtlas = @ImageAtlas;
            return ptr_of_this_method;
        }

        static object get_RoleImage_1(ref object o)
        {
            return ((GameInit.Game.RoleResDef)o).RoleImage;
        }

        static StackObject* CopyToStack_RoleImage_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleResDef)o).RoleImage;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_RoleImage_1(ref object o, object v)
        {
            ((GameInit.Game.RoleResDef)o).RoleImage = (System.String)v;
        }

        static StackObject* AssignFromStack_RoleImage_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @RoleImage = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.RoleResDef)o).RoleImage = @RoleImage;
            return ptr_of_this_method;
        }

        static object get_IconAtlas_2(ref object o)
        {
            return ((GameInit.Game.RoleResDef)o).IconAtlas;
        }

        static StackObject* CopyToStack_IconAtlas_2(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleResDef)o).IconAtlas;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_IconAtlas_2(ref object o, object v)
        {
            ((GameInit.Game.RoleResDef)o).IconAtlas = (System.String)v;
        }

        static StackObject* AssignFromStack_IconAtlas_2(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @IconAtlas = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.RoleResDef)o).IconAtlas = @IconAtlas;
            return ptr_of_this_method;
        }

        static object get_RoleIcon_3(ref object o)
        {
            return ((GameInit.Game.RoleResDef)o).RoleIcon;
        }

        static StackObject* CopyToStack_RoleIcon_3(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.RoleResDef)o).RoleIcon;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_RoleIcon_3(ref object o, object v)
        {
            ((GameInit.Game.RoleResDef)o).RoleIcon = (System.String)v;
        }

        static StackObject* AssignFromStack_RoleIcon_3(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.String @RoleIcon = (System.String)typeof(System.String).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.RoleResDef)o).RoleIcon = @RoleIcon;
            return ptr_of_this_method;
        }



    }
}
