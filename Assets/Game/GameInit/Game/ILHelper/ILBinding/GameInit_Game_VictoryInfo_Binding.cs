using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;

using ILRuntime.CLR.TypeSystem;
using ILRuntime.CLR.Method;
using ILRuntime.Runtime.Enviorment;
using ILRuntime.Runtime.Intepreter;
using ILRuntime.Runtime.Stack;
using ILRuntime.Reflection;
using ILRuntime.CLR.Utils;

namespace ILRuntime.Runtime.Generated
{
    unsafe class GameInit_Game_VictoryInfo_Binding
    {
        public static void Register(ILRuntime.Runtime.Enviorment.AppDomain app)
        {
            BindingFlags flag = BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static | BindingFlags.DeclaredOnly;
            FieldInfo field;
            Type[] args;
            Type type = typeof(GameInit.Game.VictoryInfo);

            field = type.GetField("VictoryRankingInfos", flag);
            app.RegisterCLRFieldGetter(field, get_VictoryRankingInfos_0);
            app.RegisterCLRFieldSetter(field, set_VictoryRankingInfos_0);
            app.RegisterCLRFieldBinding(field, CopyToStack_VictoryRankingInfos_0, AssignFromStack_VictoryRankingInfos_0);
            field = type.GetField("VictoryRankingType", flag);
            app.RegisterCLRFieldGetter(field, get_VictoryRankingType_1);
            app.RegisterCLRFieldSetter(field, set_VictoryRankingType_1);
            app.RegisterCLRFieldBinding(field, CopyToStack_VictoryRankingType_1, AssignFromStack_VictoryRankingType_1);


        }



        static object get_VictoryRankingInfos_0(ref object o)
        {
            return ((GameInit.Game.VictoryInfo)o).VictoryRankingInfos;
        }

        static StackObject* CopyToStack_VictoryRankingInfos_0(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryInfo)o).VictoryRankingInfos;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_VictoryRankingInfos_0(ref object o, object v)
        {
            ((GameInit.Game.VictoryInfo)o).VictoryRankingInfos = (System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>)v;
        }

        static StackObject* AssignFromStack_VictoryRankingInfos_0(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo> @VictoryRankingInfos = (System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>)typeof(System.Collections.Generic.List<GameInit.Game.VictoryRankingInfo>).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)0);
            ((GameInit.Game.VictoryInfo)o).VictoryRankingInfos = @VictoryRankingInfos;
            return ptr_of_this_method;
        }

        static object get_VictoryRankingType_1(ref object o)
        {
            return ((GameInit.Game.VictoryInfo)o).VictoryRankingType;
        }

        static StackObject* CopyToStack_VictoryRankingType_1(ref object o, ILIntepreter __intp, StackObject* __ret, IList<object> __mStack)
        {
            var result_of_this_method = ((GameInit.Game.VictoryInfo)o).VictoryRankingType;
            return ILIntepreter.PushObject(__ret, __mStack, result_of_this_method);
        }

        static void set_VictoryRankingType_1(ref object o, object v)
        {
            ((GameInit.Game.VictoryInfo)o).VictoryRankingType = (GameInit.Game.VictoryRankingType)v;
        }

        static StackObject* AssignFromStack_VictoryRankingType_1(ref object o, ILIntepreter __intp, StackObject* ptr_of_this_method, IList<object> __mStack)
        {
            ILRuntime.Runtime.Enviorment.AppDomain __domain = __intp.AppDomain;
            GameInit.Game.VictoryRankingType @VictoryRankingType = (GameInit.Game.VictoryRankingType)typeof(GameInit.Game.VictoryRankingType).CheckCLRTypes(StackObject.ToObject(ptr_of_this_method, __domain, __mStack), (CLR.Utils.Extensions.TypeFlags)20);
            ((GameInit.Game.VictoryInfo)o).VictoryRankingType = @VictoryRankingType;
            return ptr_of_this_method;
        }



    }
}
