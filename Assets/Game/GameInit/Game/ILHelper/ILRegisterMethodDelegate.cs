#if USE_ILRUNTIME
using System;
using GameInit.Framework;
using UnityEngine;
using AppDomain = ILRuntime.Runtime.Enviorment.AppDomain;

namespace GameInit.Game
{
    public class ILRegisterMethodDelegate
    {
        /// <summary>
        /// 注册委托
        /// </summary>
        public static void RegisterMethodDelegate(AppDomain appdomain)
        {
#if UNITY_EDITOR
            Debug.LogWarning("这个是留给各自项目做委托注册、转换用的，框架层已支持大部分委托转换...");
#endif
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.Purchasing.Product, System.Int32, System.Boolean>();
            appdomain.DelegateManager.RegisterMethodDelegate<System.Threading.Tasks.Task>();
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Single>();
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Framework.CoroutineAdapter.Adaptor, GameInit.Framework.CoroutineAdapter.Adaptor, System.Int32>();
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Framework.CoroutineAdapter.Adaptor, System.Boolean>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<GameInit.Framework.CoroutineAdapter.Adaptor>>((act) =>
            {
                return new System.Predicate<GameInit.Framework.CoroutineAdapter.Adaptor>((obj) =>
                {
                    return ((Func<GameInit.Framework.CoroutineAdapter.Adaptor, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<GameInit.Framework.CoroutineAdapter.Adaptor>>((act) =>
            {
                return new System.Comparison<GameInit.Framework.CoroutineAdapter.Adaptor>((x, y) =>
                {
                    return ((Func<GameInit.Framework.CoroutineAdapter.Adaptor, GameInit.Framework.CoroutineAdapter.Adaptor, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.Boolean>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>>>((act) =>
            {
                return new System.Predicate<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>>((obj) =>
                {
                    return ((Func<System.Collections.Generic.KeyValuePair<System.Int32, ILRuntime.Runtime.Intepreter.ILTypeInstance>, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.Message>();
            appdomain.DelegateManager.RegisterDelegateConvertor<GameInit.Game.DelegateMsgHandler>((act) =>
            {
                return new GameInit.Game.DelegateMsgHandler((msg) =>
                {
                    ((Action<GameInit.Game.Message>)act)(msg);
                });
            });
            appdomain.DelegateManager.RegisterMethodDelegate<System.Object[]>();
            appdomain.DelegateManager.RegisterDelegateConvertor<GameInit.Game.Timer.TimerAction>((act) =>
            {
                return new GameInit.Game.Timer.TimerAction((param) =>
                {
                    ((Action<System.Object[]>)act)(param);
                });
            });
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOSetter<UnityEngine.Vector2>>((act) =>
            {
                return new DG.Tweening.Core.DOSetter<UnityEngine.Vector2>((pNewValue) =>
                {
                    ((Action<UnityEngine.Vector2>)act)(pNewValue);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.Vector2>();
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOGetter<UnityEngine.Vector2>>((act) =>
            {
                return new DG.Tweening.Core.DOGetter<UnityEngine.Vector2>(() =>
                {
                    return ((Func<UnityEngine.Vector2>)act)();
                });
            });
            appdomain.DelegateManager.RegisterMethodDelegate<Pomelo.DotNetClient.Message>();
            appdomain.DelegateManager.RegisterMethodDelegate<SimpleJson.JsonObject>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.BaseEvent>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.FighterUnit>();
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOSetter<System.Int32>>((act) =>
            {
                return new DG.Tweening.Core.DOSetter<System.Int32>((pNewValue) =>
                {
                    ((Action<System.Int32>)act)(pNewValue);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<System.Int32>();
            appdomain.DelegateManager.RegisterDelegateConvertor<DG.Tweening.Core.DOGetter<System.Int32>>((act) =>
            {
                return new DG.Tweening.Core.DOGetter<System.Int32>(() =>
                {
                    return ((Func<System.Int32>)act)();
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Game.RoleData, GameInit.Game.RoleData, System.Int32>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<GameInit.Game.RoleData>>((act) =>
            {
                return new System.Comparison<GameInit.Game.RoleData>((x, y) =>
                {
                    return ((Func<GameInit.Game.RoleData, GameInit.Game.RoleData, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.FighterUnit, System.Boolean>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.FighterUnit, System.Single>();
            appdomain.DelegateManager.RegisterFunctionDelegate<UnityEngine.Transform, System.Boolean>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<UnityEngine.Transform>>((act) =>
            {
                return new System.Predicate<UnityEngine.Transform>((obj) =>
                {
                    return ((Func<UnityEngine.Transform, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Game.ServerData.Settlement, GameInit.Game.ServerData.Settlement, System.Int32>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.ServerData.LacEventExecute>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.ServerData.StopGameData>();
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.ServerData.RankData>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<GameInit.Game.ServerData.Settlement>>((act) =>
            {
                return new System.Comparison<GameInit.Game.ServerData.Settlement>((x, y) =>
                {
                    return ((Func<GameInit.Game.ServerData.Settlement, GameInit.Game.ServerData.Settlement, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Game.ServerData.Settlement, System.Boolean>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Predicate<GameInit.Game.ServerData.Settlement>>((act) =>
            {
                return new System.Predicate<GameInit.Game.ServerData.Settlement>((obj) =>
                {
                    return ((Func<GameInit.Game.ServerData.Settlement, System.Boolean>)act)(obj);
                });
            });
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Game.ServerData.Rank, GameInit.Game.ServerData.Rank, System.Int32>();
            
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<GameInit.Game.ServerData.Rank>>((act) =>
            {
                return new System.Comparison<GameInit.Game.ServerData.Rank>((x, y) =>
                {
                    return ((Func<GameInit.Game.ServerData.Rank, GameInit.Game.ServerData.Rank, System.Int32>)act)(x, y);
                });
            });
            
            appdomain.DelegateManager.RegisterFunctionDelegate<GameInit.Game.FighterUnitRole, GameInit.Game.FighterUnitRole, System.Int32>();
            appdomain.DelegateManager.RegisterDelegateConvertor<System.Comparison<GameInit.Game.FighterUnitRole>>((act) =>
            {
                return new System.Comparison<GameInit.Game.FighterUnitRole>((x, y) =>
                {
                    return ((Func<GameInit.Game.FighterUnitRole, GameInit.Game.FighterUnitRole, System.Int32>)act)(x, y);
                });
            });
            appdomain.DelegateManager.RegisterMethodDelegate<GameInit.Game.ServerData.StartGameData>();

        }
    }
}
#endif