using UnityEngine;

namespace GameInit.Game
{
    public class GiftEffectListener : MonoBehaviour
    {
        private System.Action _startAction;
        private System.Action _loopAction;

        public void InitListen(Animation animation, System.Action startAction, System.Action loopAction)
        {
            _startAction = startAction;
            AddEvent(animation, "start", "StartEvent");

            _loopAction = loopAction;
            AddEvent(animation, "loop", "LoopEvent");
        }

        private void AddEvent(Animation animation, string clipName, string functionName)
        {
            AnimationClip clip = animation.GetClip(clipName);
            if (clip == null) return;
            
            float length = clip.length;

            AnimationEvent evt = new AnimationEvent();
            evt.functionName = functionName;
            evt.time = length;

            clip.AddEvent(evt);
        }

        private void StartEvent()
        {
            _startAction.Invoke();
        }

        private void LoopEvent()
        {
            _loopAction.Invoke();
        }
    }
}