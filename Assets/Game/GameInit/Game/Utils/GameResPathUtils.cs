using Cysharp.Text;

namespace GameInit.Game
{
    public static class GameResPathUtils
    {

        public static string GetFighterUnitPath(string name)
        {
            return ZString.Concat("Role/", name, ".prefab");
        }
        // 获得GPU动画预制体
        public static string GetFighterUnitPath_GPU(string name)
        {
            return ZString.Concat("GPUAnimation/", name,"/","Pfb_",name, ".prefab");
        }
        // 获得GPU动画材质球
        public static string GetMat_GPUAction(string pfbName,string name)
        {
            return ZString.Concat("GPUAnimation/", pfbName,"/","Mat_",pfbName,"_", name,".mat");
        } 
        // 获取特效
        public static string GetEffectPrefab(string name)
        {
            return ZString.Concat("Effect/", name, ".prefab");
        }
        // 获取子弹
        public static string GetBulletPrefab(string name)
        {
            return ZString.Concat("Bullet/", name, ".prefab");
        }
        
        // 获取果实
        public static string GetFruitPrefab(string name)
        {
            return ZString.Concat("Fruit/", name, ".prefab");
        }

    }
}