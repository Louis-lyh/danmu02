﻿using System.Net.Mail;
using UnityEngine;

namespace GameInit.Game
{
    public class InputManager : Singleton<InputManager>
    {

        public void Update(float deltaTime)
        {
            InputKey();
        }
        
        // 
        private void InputKey()
        {
            if(Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9) )
            {
               GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventSetGiftTips); 
            }
            if(Input.GetKeyDown(KeyCode.F10))
            {
                GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventDoubleExp); 
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                
            }
        }
    }
}