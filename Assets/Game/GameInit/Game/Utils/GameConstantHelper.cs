using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace GameInit.Game
{
    public class GameConstantHelper : MonoBehaviour
    {
        // 夜晚来临时间
        public static int StartGameTime = 1;
        
        // 怪兽最低数量
        public static int MonsterLowerLimit = 0;
        // 刷新怪兽玩家最低要求
        public static int RefreshMonsterPlayerLowerLimit = 0;
        // 刷新怪兽时间上限
        public static int RefreshMonsterUpperLimit = 0;
        // 刷新怪兽间隔时间
        public static float RefreshMonsterInterval = 0;
        // 刷新怪兽倍数
        public static float RefreshMonsterMultiple = 0;
        // 刷新毛毛虫的等级
        public static string MonsterLevelOdds = "";
        // 随机怪兽的id
        public static string MonsterIdOdds = "";
        // 跟随玩家等级的怪兽
        public static string MonsterLevelBuyPlayer = "";
        
        // 果实最低数量
        public static int FruitLowerLimit = 0;
        // 刷新果实玩家最低要求
        public static int RefreshFruitPlayerLowerLimit = 0;
        // 刷新果实间隔时间
        public static int RefreshFruitInterval = 0;
        // 刷新果实时间上限
        public static int RefreshFruitUpperLimit = 0;
        // 刷新果实倍数(玩家数量的倍数)
        public static float RefreshFruitMultiple = 0;
        // 刷新果实数量上限
        public static int FruitMaxLimit = 0;
        // 野外bossId(魔法镜召唤,甜甜圈召唤)
        public static string RefreshFruitOdds_FieldBoss = "";
        // 野外boss模式复活cd
        public static int FieldBossReviveCd = 0;
        // 野外boss开始时间
        public static string FieldBossOpenTime = "";
            
        // 平静状态间隔时间
        public static string NormalIntervalTime = "";
        // 懒惰状态间隔时间
        public static string LazyIntervalTime = "";
        // 进化欲望最低等级（首次加入大于最低等级进化欲望为旺盛）
        public static int UpgradeDesireLowLevel = 0;
        
        // 刷新职业概率
        public static string RefreshesAnimalOdds = "";
        // 刷新水果概率
        public static string RefreshFruitOdds = "";
        // 水果初始刷新时间
        public static int StartRefreshFruitTime = 0;


        // boss生命值倍数
        public static string BossHpMultiple = "";
        // boss生命值比例
        public static float BossHpRecoveryRatio = 0;
        
        // 点赞礼物上限
        public static int LikeMaxLimit = 0;
        // 单次点赞经验
        public static int LikeExp = 0;
        // 单次666经验
        public static int DanMuExp = 0;
        
        // 最大天数(即一轮游戏总共为几天几夜)
        public static int MaxDay = 3;
        // 一天的时间(秒 用于昼夜)
        public static int DayAndNightTime = 258;
        // 一天进入到黑夜的时间(秒 即狼人出现的时间)
        public static int WerewolfTime = 180;
        // 过度速度(狼人死亡后过度到白天的速度)
        public static float ExcessiveSpeed = 10f;
        // 震动cd
        public static float ShakeCd = 6f;
        // 第三人称相机跟随点初始化位置(x,y,z)
        public static string LgCameraFollowCurPos = "";
        // 野外boss时间
        public static int FieldBossTime = 0;
        // 野外bossid
        public static string FieldBossIds = "";
        
        // 随机boss的玩家数量
        public static string RandomPlayerNum = "";
        // 怪兽boss对应的玩家等级
        public static string MonsterBossLevelByPlayer = "";
        
        // 点赞气泡通知起始值
        public static int LikeBubbleStartValue = 0;
        // 击杀积分气泡通知起始值
        public static int ScaleBubbleStartValue = 0;
        
        // 取消boss声波技能的玩家数量倍数
        public static int CancelBossSkillLikeMultiple = 0;
        // boss声波技能倒计时
        public static int CancelBossSkillTime = 0;
        // 刷新小狼人倍数(玩家数量的倍数)
        public static string RefreshLittleWerewolfMultiple = "";
        // 小狼人数量范围
        public static string LittleWerewolfRange = "";
        
        // 免费声波技能倒计时时间
        public static float FreeBossSkillLikeTime = 0;
        // 免费召唤小狼人时间
        public static float FreeLittleWerewolfTime = 0;
        
        // 恢复最大生命值比列
        public static float RestoreMaxHpRatio = 0;
        
        // 连胜积分奖励
        public static float AdditionalScorePercent = 0;
        // 连胜数的积分奖励欠缺最大上限
        public static float AdditionalScorePercentUpLimit = 0;
        
        // 夜晚时间超过多少不能加入(-1不限制)
        public static int ProhibitJoinNightTime = 0;
        
        // 玩家被动切换目标冷切时间
        public static float PassiveRoleSetTargetTypeCD = 0; 
        
        // 大哥入场特效最低排名
        public static int TopPlayerLowLimit = 1;
        
        // 积分分成比例
        public static string ScorePoolRatio = "";

        // 玩家攻击boss获得积分倍数
        public static float RoleAttackBossScoreMultiple = 0;
        // 玩家攻击怪兽boss获得积分倍数
        public static float RoleAttackMonsterBossScoreMultiple = 0;
        // boss攻击玩家获得积分倍数
        public static float BossAttackRoleScoreMultiple = 0;
        
        // 是否开启点赞加入游戏（1可以，0不行）
        public static int OpenLikeAddGame;
        
        // BossId
        public static string BossId = "";
        // 玩家boss大小
        public static float PlayerBossScale = 1;
        
        // 进入黑夜召唤妖怪头目数量(玩家数量的倍数)
        public static float MonsterMultipleInStartBoss = 0;
        // 进入boss阶段召唤妖怪头目数量范围
        public static string MonsterRangeInStartBoss = "";
        // 怪兽boss对应的玩家等级
        public static int MonsterLevelByPlayerInStartBoss = 1;
        
        //初始化
        public static void Init()
        {
            Dictionary<int, ConstantDef> dict = ConstantConfig.GetDefDic();
            var mytype = typeof(GameConstantHelper);
            foreach (FieldInfo info in mytype.GetFields())
            {
                using (Dictionary<int, ConstantDef>.Enumerator itr = dict.GetEnumerator())
                {
                    while (itr.MoveNext())
                    {
                        ConstantDef def = itr.Current.Value;
                        if (def.FieldName == info.Name)
                        {
                            if (info.FieldType.Name == "Int32")
                            {
                                int i = Convert.ToInt32(def.Value);
                                info.SetValue(mytype, i);
                            }
                            else if (info.FieldType.Name == "Single")
                            {
                                float f = Convert.ToSingle(def.Value);
                                info.SetValue(mytype, f);
                            }
                            else
                                info.SetValue(mytype, def.Value);
                            break;
                        }
                    }
                }
            }
        }
        
        // 逗号分隔字符转转列表
        public static List<float> StringToFloatList(string str)
        {
            if(string.IsNullOrEmpty(str))
                return new List<float>();
            
            var strList = str.Split(',');
            List<float> floatList = new List<float>();
            for (int i = 0; i < strList.Length; i++)
            {
                var value = float.Parse(strList[i]);
                floatList.Add(value);
            }

            return floatList;
        }
        public static List<int> StringToIntList(string str)
        {
            if(string.IsNullOrEmpty(str))
                return new List<int>();
            
            var strList = str.Split(',');
            List<int> floatList = new List<int>();
            for (int i = 0; i < strList.Length; i++)
            {
                var value = int.Parse(strList[i]);
                floatList.Add(value);
            }

            return floatList;
        }

        // 逗号、竖线 转换指点
        public static Dictionary<int, int> StringToIntDic(string str)
        {
            if(string.IsNullOrEmpty(str))
                return new Dictionary<int, int>();
           
            var strList = str.Split(',');
            var strDic = new Dictionary<int,int>();
            
            for (int i = 0; i < strList.Length; i++)
            {
                var levelRatioStr = strList[i].Split('|');
                var key = int.Parse(levelRatioStr[0]);
                var value = int.Parse(levelRatioStr[1]);
                strDic.Add(key,value);
            }

            return strDic;
        }


        // 转化为字符串
        public static string UpgradeDesireToString(UpgradeDesire upgradeDesire)
        {
            switch (upgradeDesire)
            {
                case UpgradeDesire.Lazy:
                    return "睡眠";
                case UpgradeDesire.Normal:
                    return "懒惰";
                case UpgradeDesire.Vigorous:
                    return "旺盛";
            }

            return "";
        }
        // 单位类型转字符串
        public static string UnitTypeToString(UnitType unitType)
        {
            switch (unitType)
            {
                case UnitType.Boss:
                    return "Boss";
                case UnitType.Bullet:
                    return "子弹";
                case UnitType.Fruit:
                    return "果实";
                case UnitType.Monster:
                    return "小怪";
                case UnitType.SmallWerewolf:
                    return "小狼人";
                case UnitType.Role:
                    return "玩家";
            }
            return String.Empty;
        }
        // 阿拉伯转中文
        /// <summary>
        /// 数字转文字(最多2位数)
        /// </summary>
        /// <param name="numberStr"></param>
        /// <returns></returns>
        public static string ConvertToChinese(int number)
        {
            if (number < 0 || number > 999999999)
            {
                throw new ArgumentException("输入的数字超出范围（0 到 9999）");
            }

            if (number == 0)
            {
                return "零";
            }
            int recordSrcNum = number;//记录下源数字
            string[] chineseDigits = { "", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
            string[] unitDigits = { "", "十", "百", "千" };

            string result = "";

            int unitIndex = 0;
            while (number > 0)
            {
                int digit = number % 10;
                if (digit > 0)
                {
                    result = chineseDigits[digit] + unitDigits[unitIndex] + result;
                }
                else
                {
                    // 处理连续的零
                    if (result.Length > 0 && result[0] != '零')
                    {
                        result = "零" + result;
                    }
                }

                number /= 10;
                unitIndex++;
            }
            if (number >= 10 && number < 20) //单独处理10-19，否则会生成一十
            {
                return result.Substring(1);
            }
            return result;
        }
        /// <summary>
        /// 获取贝塞尔曲线长度
        /// </summary>
        /// <param name="p0"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns></returns>
        public static float GetBezierLength(Vector2 p0,Vector2 p1,Vector2 p2) {
            var ax = p0.x - 2 * p1.x + p2.x;
            var ay = p0.y - 2 * p1.y + p2.y;
            var bx = 2 * p1.x - 2 * p0.x;
            var by = 2 * p1.y - 2 * p0.y;
            var A = 4 * (ax * ax + ay * ay);
            var B = 4 * (ax * bx + ay * by);
            var C = bx * bx + by * by;

            var Sabc = 2 * Mathf.Sqrt(A + B + C);
            var A_2 = Mathf.Sqrt(A);
            var A_32 = 2 * A * A_2;
            var C_2 = 2 * Mathf.Sqrt(C);
            var BA = B / A_2;

            return ((A_32 * Sabc + A_2 * B * (Sabc - C_2) + (4 * C * A - B * B) * Mathf.Log((2 * A_2 + BA + Sabc) / (BA + C_2))) / (4 * A_32));
        }
        
        /// <summary>
        /// 转换时间戳
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
        {
            // Unix时间戳是从1970年1月1日00:00:00开始的秒数
            System.DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dateTime;
        }
        
        /// <summary>
        /// DateTime 转 Unix
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long DateTimeToUnixTimeStamp(System.DateTime dateTime)
        {
            // 转换为UTC时间，减去时间的毫秒数，然后除以1000得到Unix时间戳
            return (dateTime.ToUniversalTime().Ticks - 621355968000000000) / 10000000;
        }
        
        /// <summary>
        /// 二阶贝塞尔曲线
        /// </summary>
        public static Vector3 BezierCurvePos(Vector3 p0, Vector3 p1, Vector3 p2, float t)
        {
            Vector3 B = Vector3.zero;
            float t1 = (1 - t) * (1 - t);
            float t2 = 2 * t * (1 - t);
            float t3 = t * t;
            B += t1 * p0 + t2 * p1 + t3 * p2;
            return B;
        }
    }
}


