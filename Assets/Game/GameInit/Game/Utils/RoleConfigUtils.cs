using System.Collections.Generic;
using System.Linq;
using GameHotfix.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public static class RoleConfigUtils
    { 
        // 玩家id
        private static List<int> _listAllPlayerId;
        // 角色表配置
        private static Dictionary<int, List<RoleDef>> _dictRoleConfigs;
        
        // 随机玩家id配置
        private static Dictionary<int, int> _dicRandomConfigs;
        // 比例值总数
        private static int _randomRatioCount;
        
        
        // 加载角色配置
        private static void ParseRoleConfig()
        {
            if (_listAllPlayerId != null && _dictRoleConfigs != null)
                return;
            
            // 角色配置
            var dict = RoleConfig.GetDefDic();
            if (dict == null)
                return;
            
            _listAllPlayerId = new List<int>();
            _dictRoleConfigs = new Dictionary<int, List<RoleDef>>();
            
            foreach (var kv in dict)
            {
                // 记录所有配置
                if (!_dictRoleConfigs.TryGetValue(kv.Value.RoleID, out var list))
                {
                    list = new List<RoleDef>();
                    _dictRoleConfigs[kv.Value.RoleID] = list;
                }
                list.Add(kv.Value);

                // 记录玩家id
                if (_listAllPlayerId.Contains(kv.Value.RoleID) || kv.Value.RoleType == 0)
                    continue;
                _listAllPlayerId.Add(kv.Value.RoleID);
                
            }
            Logger.LogYellow("ParseRoleConfig");
            
            // 根据等级排序
            foreach (var kv in _dictRoleConfigs)
                kv.Value.Sort(SortRoleDef);
        }
        // 排序逻辑
        private static int SortRoleDef(RoleDef a, RoleDef b)
        {
            return a.Level > b.Level ? 1 : -1;
        }
        
        // 初始化随机配置
        private static void InitRandomConfig()
        {
            if(_dicRandomConfigs != null)
                return;
            
            var randomStr = GameConstantHelper.RefreshesAnimalOdds;
            var randomDic = GameConstantHelper.StringToIntDic(randomStr);
            
            _dicRandomConfigs = new Dictionary<int, int>();
            // 比例总数
            _randomRatioCount = 0;
            // 初始化配置
            var keys = randomDic.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var id = keys[i];
                var ratio = randomDic[id];

                // 累计
                _randomRatioCount += ratio;
                
                _dicRandomConfigs.Add(id,_randomRatioCount);
            }
        }

        // 随机角色id
        public static int RandomRoleID()
        {
            // 读取角色配置
            ParseRoleConfig();
            
            //初始化随机配置
            InitRandomConfig();
            
            // 随机值
            var randomValue = Random.Range(1, _randomRatioCount + 1);
            
            // 初始化配置
            var keys = _dicRandomConfigs.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var id = keys[i];
                var ratio = _dicRandomConfigs[id];

                if (randomValue <= ratio)
                    return id;
            }

            return 0;
        }
       
        // 获取玩家配置
        public static RoleDef GetRoleDef(int roleId, int exp = 0)
        {
            if (exp == 0)
                return RoleConfig.Get(roleId + 1);
            
            // 初始英雄数据
            ParseRoleConfig();
            if (!_dictRoleConfigs.TryGetValue(roleId, out var list))
                return null;
            
            // 遍历
            for (int i = 0; i < list.Count; i++)
            {
                var t = list[i];
                if (t.ExpMax > exp)
                    return t;
            }
            return null;
        }

        // 获取玩家配置
        public static RoleDef GetRoleDef_Level(int roleId, int level)
        {
            ParseRoleConfig();
            if (!_dictRoleConfigs.TryGetValue(roleId, out var list))
                return null;
            
            // 遍历
            for (int i = 0; i < list.Count; i++)
            {
                var t = list[i];
                if (t.Level == level)
                    return t;
            }
            return null;
        }
    }
}