using System;
using UnityEngine;

namespace GameInit.Game
{
    public class AutoDestroyScript : MonoBehaviour 
    {
        [SerializeField]
        private float lifeTime = 1.0f;
        private float _lastTime;

        private bool _isRunning = false;
        public Action OnDestroyAction;
        private void Update()
        {
            if (!_isRunning)
                return;
            if (_lastTime <= 0.01f)
            {
                _isRunning = false;
                OnDestroyAction?.Invoke();
                return;
            }

            _lastTime -= Time.deltaTime;
        }
        // 设置存在时间
        public void SetLifeTime(float time)
        {
            lifeTime = time;
            _lastTime = lifeTime;
            _isRunning = true;
        }
        // 添加存在时间
        public void AddLifeTime(float time)
        {
            if(!_isRunning)
                return;
            
            lifeTime += time;
            _lastTime += time;
        }

        private void OnDestroy()
        {
            OnDestroyAction = null;
        }
    }
}