﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-28
|FUNCTION：Single column
+-----------------------------------------------------------------------------*/

using System;

namespace GameInit.Game
{
    public class Singleton<T>
    {
        protected static readonly T _instance = Activator.CreateInstance<T>();

        public static T Instance
        {
            get { return Singleton<T>._instance; }
        }

        static Singleton()
        {
            
        }

        protected Singleton()
        {
            
        }
    }
}