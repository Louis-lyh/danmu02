using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class SoundPlayer : UpdateBase
    {
        private AudioSource _source;
        private bool _isLoop;
        public Transform Transform { get; }

        public long SoundPlayerId;
        public string SoundName { get; private set; }

        public SoundPlayer(AudioSource source)
        {
            _source = source;
            Transform = _source.transform;
        }

        public async void PlaySound(string name, bool blLoop, float vol = 1f, float delayTime = 0, SoundVolDef def = null)
        {
            SoundName = name;
            // if (_source.isPlaying) _source.Stop();
            _isLoop = blLoop;
            
            _source.gameObject.name = "s_" + name;
            var clip = await ResourceManager.LoadAssetAsync<AudioClip>(ResPathUtils.GetSoundPath(name));
            if (!_source.gameObject.activeInHierarchy)
                return;
            if (clip == null)
                Logger.LogError("SoundPlayer.PlaySound() => 加载音频文件失败，name:" + name);
            _source.clip = clip;
            _source.loop = blLoop;
            _source.volume = vol;
            
            //淡入
            // if (def != null && def.FadeIn > 0)
            // {
            //     _source.volume = 0;
            //     GameHelper.NumberTo(def.FadeInTime, 0, vol, f =>
            //     {
            //         _source.volume = f;
            //     });
            // }
            
            //淡出
            // if (def != null && def.FadeOut > 0)
            // {
            //     float time = clip.length - def.FadeOutTime;
            //     TimerHeap.AddTimer((uint)(time * 1000), 0, () =>
            //     {
            //         GameHelper.NumberTo(time, _source.volume, 0, f =>
            //         {
            //             if (_source != null) _source.volume = f;
            //         });
            //     });
            // }
            
            if (!Vector3MathUtils.IsZero(delayTime))
            {
                StartDelay(delayTime);
                return;
            }
            OnInitialize();
            _source.Play();
        }

        private uint _delayTimerKey;

        private void RemoveDelayTimer()
        {
            if (_delayTimerKey == 0)
                return;
            FrameTimerHeap.DelTimer(_delayTimerKey);
            _delayTimerKey = 0;
        }

        private void StartDelay(float delay)
        {
            RemoveDelayTimer();
            _delayTimerKey = FrameTimerHeap.AddTimer((uint)(delay * 1000), 0, () =>
            {
                RemoveDelayTimer();
                OnInitialize();
                _source.Play();
            });
        }

        protected override void OnUpdate(float deltaTime)
        {
            if (!_isInitialed || _isLoop)
                return;
            if (!_source.isPlaying)
                SoundManager.Instance.StopSound(SoundPlayerId);
        }

        public void StopSound()
        {
            RemoveDelayTimer();
            _source.Stop();
            _source.clip = null;
            _source.loop = false;
            _isLoop = false;
            _isInitialed = false;
        }

        public void SetVolume(float value)
        {
            if (_source != null)
                _source.volume = value;
        }

        protected override void OnDispose()
        {
            _source = null;
            RemoveDelayTimer();
            base.OnDispose();
        }
    }
}