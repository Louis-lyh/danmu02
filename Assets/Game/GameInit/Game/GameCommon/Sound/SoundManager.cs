using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class SoundManager : Singleton<SoundManager>
    {
        private Queue<SoundPlayer> _playerPool;

        private Transform _soundPoolRoot;
        private Transform _soundRunRoot;

        private Dictionary<long, SoundPlayer> _dictRunningPlayer;

        private Dictionary<string, int> _dictQueueLimit;

        public bool IsOpenVoice = true; // 开启音效
        public bool IsOpenMusic = true; // 开启音乐

        public void Init()
        {
            var soundRoot = GameObject.Find("SoundRoot");
            _soundPoolRoot = soundRoot.transform.Find("SoundPoolRoot");
            _soundRunRoot = soundRoot.transform.Find("SoundRunRoot");

            _playerPool = new Queue<SoundPlayer>();
            _dictRunningPlayer = new Dictionary<long, SoundPlayer>();
            _dictQueueLimit = new Dictionary<string, int>();
        }

        private long _startId = 0;

        private SoundPlayer GetSoundPlayer()
        {
            SoundPlayer player;
            if (_playerPool.Count > 0)
            {
                player = _playerPool.Dequeue();
            }
            else
            {
                var soundObject = new GameObject();
                var result = soundObject.AddComponent<AudioSource>();
                player = new SoundPlayer(result);
            }

            _startId++;
            player.SoundPlayerId = _startId;
            player.Transform.SetParent(_soundRunRoot, false);
            return player;
        }

        private long _PlaySound(string name, float vol, float delay, int queueLimit, bool isLoop = false, SoundVolDef def = null)
        {
            var player = GetSoundPlayer();
            if (_dictRunningPlayer.ContainsKey(player.SoundPlayerId))
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 重复添加音频播放器,id:" + player.SoundPlayerId + "]");
                return player.SoundPlayerId;
            }

            player.PlaySound(name, isLoop, vol, delay, def);
            _dictRunningPlayer.Add(player.SoundPlayerId, player);

            if (queueLimit > 0)
            {
                if (_dictQueueLimit.ContainsKey(name)) _dictQueueLimit[name] += 1;
                else _dictQueueLimit.Add(name, 1);
            }

            return player.SoundPlayerId;
        }

        public long PlaySound(int id, bool isLoop = false)
        {
            if (IsOpenVoice == false)
                return -1;

            SoundVolDef def = SoundVolConfig.Get(id);
            if (def == null)
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 无效的音频id:" + id);
                return -1;
            }

            string name = def.SoundName;
            if (def.QueueLimit > 0 && _dictQueueLimit.ContainsKey(name) && _dictQueueLimit[name] >= def.QueueLimit)
            {
                return -1;
            }

            return _PlaySound(name, def.Volume, def.DelayTime, def.QueueLimit, isLoop, def);
        }

        private int _index;
        private KeyValuePair<long, SoundPlayer> _keyValuePair;

        public void Update(float deltaTime)
        {
            if (_dictRunningPlayer == null)
                return;
            for (_index = 0; _index < _dictRunningPlayer.Count; _index++)
            {
                _keyValuePair = _dictRunningPlayer.ElementAt(_index);
                _keyValuePair.Value.Update(deltaTime);
            }
        }

        public void StopAllSound()
        {
            if (_dictRunningPlayer == null)
                return;
            foreach (var kv in _dictRunningPlayer)
            {
                kv.Value.StopSound();

                _playerPool.Enqueue(kv.Value);
                kv.Value.Transform.SetParent(_soundPoolRoot, false);
            }

            _dictRunningPlayer.Clear();
            _dictQueueLimit.Clear();
            
            _bgm = 0;
            _bgm_id = 0;
        }

        public void StopSound(long playerId)
        {
            if (!_dictRunningPlayer.TryGetValue(playerId, out var player))
            {
                Logger.LogError("SoundManager.StopSound() => 移出SoundPlayer失败，id:" + playerId);
                return;
            }

            _dictRunningPlayer.Remove(playerId);

            string name = player.SoundName;
            if (_dictQueueLimit.ContainsKey(name))
            {
                int num = _dictQueueLimit[name];
                num -= 1;
                if (num <= 0) _dictQueueLimit.Remove(name);
                else _dictQueueLimit[name] = num;
            }

            player.StopSound();
            _playerPool.Enqueue(player);
            player.Transform.SetParent(_soundPoolRoot, false);
        }

        private long _bgm = 0;
        private int _bgm_id = 0;

        public void PlayBackGroundSound(int id, bool isLoop = true)
        {
            if(id > 0)
                _bgm_id = id;

            if (IsOpenMusic == false)
                return;

            SoundVolDef def = SoundVolConfig.Get(id);
            if (def == null)
            {
                Logger.LogMagenta("[SoundManager.PlaySound() => 无效的音频id:" + id);
                return;
            }

            if (_bgm > 0) StopBgm();

            _bgm = _PlaySound(def.SoundName, def.Volume, def.DelayTime, def.QueueLimit, isLoop);
            _bgm_id = id;
        }

        public void RecoveryBgm()
        {
            if (_bgm_id > 0) PlayBackGroundSound(_bgm_id);
        }

        public void StopBgm()
        {
            StopSound(_bgm);
            _bgm = 0;
        }

        public void ClearBgm()
        {
            StopSound(_bgm);
            _bgm = 0;
            _bgm_id = 0;
        }

        public void Destroy()
        {
            if (_dictRunningPlayer != null)
            {
                foreach (var kv in _dictRunningPlayer)
                {
                    kv.Value.Dispose();
                }

                _dictRunningPlayer.Clear();
            }

            if (_playerPool == null)
                return;
            while (_playerPool.Count > 0)
            {
                var player = _playerPool.Dequeue();
                player.Dispose();
            }
        }
    }
}