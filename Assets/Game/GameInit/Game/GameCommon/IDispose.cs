namespace GameInit.Game
{
    public interface IDispose
    {
        void Dispose();
    }
}