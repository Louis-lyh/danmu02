﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：The basis of all units
+-----------------------------------------------------------------------------*/

using System;

namespace GameInit.Game
{
    public abstract class XObject : IDisposable
    {
        private bool mDisposed = false;

        ~XObject()
        {
            if (!mDisposed)
                Dispose(false);
        }

        public void Dispose()
        {
            if (!mDisposed)
                Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                mDisposed = true;
                OnDispose();
                GC.SuppressFinalize(this);
            }
            
            // else
            // {
            //     StringBuilder sb = new StringBuilder();
            //     sb.AppendFormat("Memory leak!!!(Type: {0}), (Instance: {1})", this.GetType().ToString(), DebugName);
            //     Debug.LogError(sb.ToString());
            // }
        }

        protected abstract void OnDispose();
    }
}