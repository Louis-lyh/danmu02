﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;

namespace GameInit.Game
{
    public sealed class ObjectPoolManager : Singleton<ObjectPoolManager>
    {
        private Dictionary<string, ObjectPool> mPoolHash = new Dictionary<string, ObjectPool>();  //<对象池名字, 对象池>

        public Dictionary<string, ObjectPool> PoolHash
        {
            get { return mPoolHash; }
        }

        public void Destroy()
        {
            using (Dictionary<string, ObjectPool>.Enumerator itr = mPoolHash.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    itr.Current.Value.Dispose();
                }
            }
            mPoolHash.Clear();
        }

        //capacity 为池子的容积
        public ObjectPool GetPool(string name, int capacity = 0)
        {
            ObjectPool pool = null;
            if (!mPoolHash.TryGetValue(name, out pool))
            {
                pool = new ObjectPool(name, capacity);
                mPoolHash.Add(name, pool);
            }
            return pool;
        }
    }
}