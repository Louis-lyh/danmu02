﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：
+-----------------------------------------------------------------------------*/

using UnityEngine;
using System.Collections.Generic;
using Cysharp.Text;
using GameInit.Framework;

namespace GameInit.Game
{
    public sealed class ObjectPool : XObject
    {
        private GameObject mRoot;
        private Stack<GameObject> mPool = new Stack<GameObject>();
        private GameObject mPrefab;

        //构造时创建容积为capacity数量的池子
        public ObjectPool(string name, int capacity)
            : this(name, ResourceManager.LoadAssetSync<GameObject>(ZString.Concat(name, ".prefab")), capacity)
        {
            
        }
        
        public ObjectPool(string name, GameObject prefab, int capacity)
        {
            if (null == prefab)
            {
                GameInit.Framework.Logger.LogError(ZString.Concat(name, " non-existent"));
            }
            
            mPrefab = prefab;

            mRoot = new GameObject();
            mRoot.name = ZString.Concat("ObjectPool_", name);
            
            for (int i = 0; i < capacity; ++i)
            {
                GameObject go = GameObject.Instantiate(prefab) as GameObject;
                go.transform.SetParent(mRoot.transform, false);
                go.SetActive(false);
                mPool.Push(go);
            }
        }

        //从对象池中拿出对象使用
        public GameObject Spawn(bool active = true)
        {
            if (mPool.Count > 0)
            {
                GameObject go = mPool.Pop();
                if (active) go.SetActive(true);
                return go;
            }
            
            return GameObject.Instantiate(mPrefab) as GameObject;
        }

        //当池中对象不使用的时候返还给对象池
        public void Unspawn(GameObject go)
        {
            if (go != null)
            {
                go.transform.SetParent(mRoot.transform, false);
                go.SetActive(false);
                mPool.Push(go);
            }
        }

        protected override void OnDispose()
        {
            using (Stack<GameObject>.Enumerator itr = mPool.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    if (itr.Current != null)
                        GameObject.Destroy(itr.Current);
                }
            }
            mPool.Clear();
        }
    }
}
