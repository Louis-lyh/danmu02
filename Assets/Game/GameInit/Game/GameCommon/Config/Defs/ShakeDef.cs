﻿using GameInit.Framework;

namespace GameInit.Game
{

    public class ShakeDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 继续时间
        /// </summary>
        public float Duration;

        /// <summary>
        /// 力量
        /// </summary>
        public float Strength;

        /// <summary>
        /// 震动
        /// </summary>
        public int Vibrato;

        /// <summary>
        /// 随机性
        /// </summary>
        public float Randomness;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Duration = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Strength = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Vibrato = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Randomness = ByteUtil.ReadFloat(bytes,ref byteIndex);

		}
    }
    
}
