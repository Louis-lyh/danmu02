﻿using GameInit.Framework;

namespace GameInit.Game
{
    public class RoleResDef
        {
            /// <summary>
            /// 索引ID
            /// </summary>
            public int ID;

            /// <summary>
            /// 物种名
            /// </summary>
            public string SpeciesName;

            /// <summary>
            /// 资源
            /// </summary>
            public string ResModel;

            /// <summary>
            /// 角色Icon
            /// </summary>
            public string RoleIcon;

            /// <summary>
            /// Icon图集
            /// </summary>
            public string IconAtlas;

            /// <summary>
            /// 角色图片
            /// </summary>
            public string RoleImage;

            /// <summary>
            /// 图片图集
            /// </summary>
            public string ImageAtlas;

    

            public void Parse(byte[] bytes, ref int byteIndex)
            {
                ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
                SpeciesName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
                ResModel = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
                RoleIcon = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
                IconAtlas = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
                RoleImage = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
                ImageAtlas = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

            }
        }
}