﻿using GameInit.Framework;

namespace GameInit.Game
{
    public class PetDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 资源id
        /// </summary>
        public int RoleId;

        /// <summary>
        /// 经验上限
        /// </summary>
        public int ExpLimit;

        /// <summary>
        /// 上一级经验
        /// </summary>
        public int PreExpLimit;

        /// <summary>
        /// 增加攻击力
        /// </summary>
        public int PetAttack;

        /// <summary>
        /// 增加攻击系数
        /// </summary>
        public int AttackBonus;

        /// <summary>
        /// 增加移速
        /// </summary>
        public float PetSpeed;

        /// <summary>
        /// 增加移速加成
        /// </summary>
        public int SpeedBonus;

        /// <summary>
        /// 增加生命
        /// </summary>
        public int PetHp;

        /// <summary>
        /// 增生命值加成
        /// </summary>
        public int HpBonus;

    

        public void Parse(byte[] bytes, ref int byteIndex)
        {
            ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            RoleId = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            ExpLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            PreExpLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            PetAttack = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            AttackBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            PetSpeed = ByteUtil.ReadFloat(bytes,ref byteIndex);
            SpeedBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            PetHp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
            HpBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);

        }
    }
}