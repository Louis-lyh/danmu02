﻿using GameInit.Framework;

namespace GameInit.Game
{

	public class GiftDef
	{
		/// <summary>
		/// 编号
		/// </summary>
		public int ID;

		/// <summary>
		/// 增加经验
		/// </summary>
		public int Exp;

		/// <summary>
		/// 增加光环经验
		/// </summary>
		public int ExpHalo;

		/// <summary>
		/// 增加宠物经验
		/// </summary>
		public int ExpPet;

		/// <summary>
		/// 增益id
		/// </summary>
		public string BuffId;

		/// <summary>
		/// Boss增益id
		/// </summary>
		public string BossBuffId;

    

		public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Exp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ExpHalo = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ExpPet = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			BuffId = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			BossBuffId = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
	}
    
}
