﻿using GameInit.Framework;

namespace GameInit.Game
{

      public class BuffDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 类型
        /// </summary>
        public int Type;

        /// <summary>
        /// Buff作用对象类型
        /// </summary>
        public int EffectUnitType;

        /// <summary>
        /// 属性类型
        /// </summary>
        public int AttrKey;

        /// <summary>
        /// buff作用类型
        /// </summary>
        public int EffectType;

        /// <summary>
        /// 作用间隔(秒)
        /// </summary>
        public float Interval;

        /// <summary>
        /// 触发概率(百分比)
        /// </summary>
        public int TriggerRate;

        /// <summary>
        /// Buff作用值
        /// </summary>
        public int Value;

        /// <summary>
        /// 作用值类型
        /// </summary>
        public int ValueType;

        /// <summary>
        /// 是否可以累计
        /// </summary>
        public int IsAddUp;

        /// <summary>
        /// 累计上限
        /// </summary>
        public int AddUpLimit;

        /// <summary>
        /// 效果范围
        /// </summary>
        public float Range;

        /// <summary>
        /// 持续时间（秒）
        /// </summary>
        public float Duration;

        /// <summary>
        /// Buff特效
        /// </summary>
        public string Effect;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Type = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			EffectUnitType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			AttrKey = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			EffectType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Interval = ByteUtil.ReadFloat(bytes,ref byteIndex);
			TriggerRate = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Value = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ValueType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			IsAddUp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			AddUpLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Range = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Duration = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Effect = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
    }
    
}
