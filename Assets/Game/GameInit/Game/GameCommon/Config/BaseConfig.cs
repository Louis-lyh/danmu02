﻿namespace GameInit.Game
{
    public abstract class BaseConfig
    {
        public abstract void Deserialize(byte[] bytes);
    }
}