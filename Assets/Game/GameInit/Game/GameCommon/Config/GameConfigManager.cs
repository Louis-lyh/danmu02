using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class ConfigEvent
    {
        public const string CONFIG_RELOAD = "configReload";
    }

    public sealed class GameConfigManager : Singleton<GameConfigManager>
    {
        private readonly Dictionary<string, BaseConfig> _allConfigs;
        private string _level = "";
        
        public GameConfigManager()
        {
            _allConfigs = new Dictionary<string, BaseConfig>();
            UserGroup.Instance.SetChangeLabelListener(OnUserGroupLabelChange);
        }
        
        public void Destroy()
        {
            UserGroup.Instance.SetChangeLabelListener(null);
            _allConfigs.Clear();
        }

        private void OnUserGroupLabelChange(string key, string value)
        {
            var level = UserGroup.Instance.SelfMatchLevel();
            if (_level == level)
                return;
            _level = level;
            ReloadConfig();
        }
        
        private string GetLevel()
        {
            _level = UserGroup.Instance.SelfMatchLevel();
            return _level;
        }
        
        private async UniTask ReloadConfig()
        {
            await DownloadMgr.Instance.StartDownload();
            await LoadConfig(true);
        }
        
        public async UniTask LoadConfig(bool isReload = false)
        {
            var level = GetLevel();
            foreach (var kv in _allConfigs)
            {
                var path = ResPathUtils.GetConfigPath(kv.Key);
                if (!ResourceManager.AssetExists(path))
                {
                    if(isReload)
                        continue;
                    path = ResPathUtils.GetConfigPath(kv.Key);
                }
                var configText = await ResourceManager.LoadAssetAsync<TextAsset>(path);
                kv.Value.Deserialize(configText?.bytes);
            }
        }

        public void AddConfig(string cfgName, BaseConfig config)
        {
            if (_allConfigs.ContainsKey(cfgName))
            {
                Logger.LogMagenta("ConfigMgr.AddConfig() => 重复添加配置表，cfgName:" + cfgName);
                return;
            }

            _allConfigs[cfgName] = config;
        }
    }
}