﻿using System.Collections.Generic;
using GameInit.Framework;

namespace GameInit.Game
{
    public class HaloConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            HaloDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new HaloDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取HaloDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static HaloDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out HaloDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, HaloDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, HaloDef> _defDic = new Dictionary<int, HaloDef>();
    }
}