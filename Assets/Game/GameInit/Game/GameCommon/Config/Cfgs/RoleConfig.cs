﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;
using GameInit.Game;

namespace GameInit.Game
{
    public class RoleConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            RoleDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new RoleDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取RoleDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static RoleDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out RoleDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, RoleDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, RoleDef> _defDic = new Dictionary<int, RoleDef>();
    }
    
}
