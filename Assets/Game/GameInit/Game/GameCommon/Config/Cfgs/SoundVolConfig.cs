﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameInit.Game
{
    public class SoundVolConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            SoundVolDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new SoundVolDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取SoundVolDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static SoundVolDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out SoundVolDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, SoundVolDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, SoundVolDef> _defDic = new Dictionary<int, SoundVolDef>();
    }
    
}
