﻿using System.Collections.Generic;
using GameInit.Framework;

namespace GameInit.Game
{
    public class DropRewardConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            DropRewardDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new DropRewardDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取DropRewardDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static DropRewardDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out DropRewardDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, DropRewardDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, DropRewardDef> _defDic = new Dictionary<int, DropRewardDef>();
    }
}