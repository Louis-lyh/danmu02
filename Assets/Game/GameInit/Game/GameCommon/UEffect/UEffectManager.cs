﻿using UnityEngine;
using System.Collections.Generic;
using Cysharp.Text;
using GameInit.Framework;

namespace GameInit.Game
{
    public class UEffectManager : Singleton<UEffectManager>
    {
        private List<UEffect> mEffectList = new List<UEffect>();

        public void Destroy()
        {
            using (List<UEffect>.Enumerator itr = mEffectList.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    if (itr.Current != null)
                    {
                        // if (itr.Current.Pool != null)
                        //     itr.Current.Pool.Unspawn(itr.Current.gameObject);
                        // else
                        //     GameObject.Destroy(itr.Current.gameObject);
                        GameObject.Destroy(itr.Current.gameObject);
                    }
                }
            }
            mEffectList.Clear();
        }

        public void DestroyEffect(UEffect effect)
        {
            if (effect == null)
                return;
            mEffectList.Remove(effect);
            // if (effect.Pool != null)
            //     effect.Pool.Unspawn(effect.gameObject);
            // else
            //     GameObject.Destroy(effect.gameObject);
            GameObject.Destroy(effect.gameObject);
        }

        public void TimerDestroyEffect(params object[] param)
        {
            UEffect effect = (UEffect)param[0];

            if (effect == null)
                return;

            mEffectList.Remove(effect);
            // if (effect.Pool != null)
            //     effect.Pool.Unspawn(effect.gameObject);
            // else
            //     GameObject.Destroy(effect.gameObject);
            GameObject.Destroy(effect.gameObject);
        }

        //private GameObject LoadEffectObj(string name, bool pool, bool active)
        // private GameObject LoadEffectObj(string name, bool active)
        // {
        //     GameObject go = null;
        //     if (pool)
        //         go = ObjectPoolManager.Instance.GetPool("effect", name).Spawn(active);
        //     else
        //     {
        //         GameObject prefab = Resources.Load<GameObject>(name);
        //         if (prefab == null)
        //         {
        //             StringBuilder sb = new StringBuilder();
        //             sb.AppendFormat("the name \"{0}\" of effect is not exist!!!", name);
        //             Debug.LogError(sb.ToString());
        //             return null;
        //         }
        //         go = GameObject.Instantiate(prefab) as GameObject;
        //         go.SetActive(active);
        //     }
        //     return go;
        // }

        //将特效直接播放到场景的世界坐标系中(预制名，是否对象池，位置，旋转，是否是循环特效，删除时间，是否激活)
        //public UEffect PlayEffectInWorld(string name, bool pool, Vector3 pos, Quaternion rotation, bool effectPool, float deTime = 1.25f, bool active = true)
        public async void PlayEffectInWorld(string name, Vector3 pos, Quaternion rotation, bool effectPool, float deTime = 1.25f, bool active = true)
        {
            string path = ZString.Concat("Effect/", name, ".prefab");
            GameObject prefab = await ResourceManager.LoadAssetAsync<GameObject>(path);
            GameObject go = GameObject.Instantiate(prefab) as GameObject;
            go.SetActive(active);

            UEffect effect = go.GetComponent<UEffect>();
            if (effect == null)
                effect = go.AddComponent<UEffect>();

            //effect.Pool = pool ? ObjectPoolManager.Instance.GetPool("effect", name) : null;
            effect.Active = active;
            if (!effectPool)
                effect.OnLifeTimeEnd = DestroyEffect;
            else
                TimerManager.Instance.AddTimer(deTime, false, 0, TimerDestroyEffect, effect);

            go.transform.position = pos;

            if (rotation != Quaternion.identity)
                go.transform.rotation = rotation;

            mEffectList.Add(effect);
        }

        //将特效挂到一个物体上(预制名，是否对象池，位置，旋转，是否是循环特效，是否激活)
        //public UEffect PlayEffectInObj(string name, bool pool, GameObject obj, Vector3 pos, Quaternion rotation, bool effectPool, float deTime = 1.25f, bool active = true)
        public async void PlayEffectInObj(string name, GameObject obj, Vector3 pos, Quaternion rotation, bool effectPool, float deTime = 1.25f, bool active = true)
        {
            string path = ZString.Concat("Effect/", name, ".prefab");
            GameObject prefab = await ResourceManager.LoadAssetAsync<GameObject>(path);
            GameObject go = GameObject.Instantiate(prefab) as GameObject;
            go.SetActive(active);

            UEffect effect = go.GetComponent<UEffect>();
            if (effect == null)
                effect = go.AddComponent<UEffect>();

            //effect.Pool = pool ? ObjectPoolManager.Instance.GetPool("effect", name) : null;
            effect.Active = active;
            if (!effectPool)
                effect.OnLifeTimeEnd = DestroyEffect;
            else
                TimerManager.Instance.AddTimer(deTime, false, 0, TimerDestroyEffect, effect);

            go.transform.SetParent(obj.transform);

            go.transform.localPosition = pos;

            if (rotation != Quaternion.identity)
                go.transform.localRotation = rotation;
            
            go.transform.localScale = Vector3.one;

            mEffectList.Add(effect);
        }
    }
}