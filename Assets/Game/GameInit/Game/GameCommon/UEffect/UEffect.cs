﻿namespace GameInit.Game
{
    using System;
    using UnityEngine;

    public sealed class UEffect : MonoBehaviour
    {
        private float mCurTime = 0;

        //private ObjectPool mPool;

        private ParticleSystem mParticleSystem;

        private Action<UEffect> onDestroyed;
        private Action<UEffect> onLifeTimeEnd;

        #region property
        public float LifeTime
        {
            get;
            set;
        }

        public bool Active
        {
            get { return gameObject.activeSelf; }
            set
            {
                gameObject.SetActive(value);
                Play();
            }
        }

        // public ObjectPool Pool
        // {
        //     get { return mPool; }
        //     set { mPool = value; }
        // }

        public Action<UEffect> OnDestroyed
        {
            get { return onDestroyed; }
            set { onDestroyed = value; }
        }

        public Action<UEffect> OnLifeTimeEnd
        {
            get { return onLifeTimeEnd; }
            set
            {
                LifeTime = 0;
                ParticleSystem[] trs = GetComponentsInChildren<ParticleSystem>();
                for (int i = 0; i < trs.Length; ++i)
                {
                    if (LifeTime < (trs[i].main.duration + trs[i].main.startDelayMultiplier))
                        LifeTime = (trs[i].main.duration + trs[i].main.startDelayMultiplier);
                }
                onLifeTimeEnd = value;
            }
        }
        #endregion

        void Awake()
        {
            mParticleSystem = transform.GetComponent<ParticleSystem>();
        }

        void Update()
        {
            mCurTime += Time.deltaTime;

            if (mCurTime >= LifeTime)
            {
                if (onLifeTimeEnd != null)
                    onLifeTimeEnd(this);
            }
        }

        void OnEnable()
        {
            onDestroyed = null;
            onLifeTimeEnd = null;
            LifeTime = 0;
            mCurTime = 0;
        }

        void OnDisable()
        {
            LifeTime = 0;
            mCurTime = 0;
        }

        void OnDestroy()
        {
            if (onDestroyed != null)
                onDestroyed(this);
        }

        public void Play()
        {
            if (mParticleSystem != null)
                mParticleSystem.Play();
        }
    }
}