﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-28
|FUNCTION：Managing messages in games
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;

namespace GameInit.Game
{
    //消息处理委托
    public delegate void DelegateMsgHandler(Message msg);

    public sealed class MessageManager : Singleton<MessageManager>
    {
        //一个消息对应多个处理
        private Dictionary<string, List<DelegateMsgHandler>> mMsgHandlerHash = new Dictionary<string, List<DelegateMsgHandler>>();
        private Queue<Message> mMsgQueue = new Queue<Message>();    //消息队列

        public void Destroy()
        {
            mMsgHandlerHash.Clear();
            mMsgQueue.Clear();
        }

        public void SendMessage(Message msg)   //发送消息立即处理
        {
            List<DelegateMsgHandler> handlers = null;
            if (mMsgHandlerHash.TryGetValue(msg.Name, out handlers))
            {
                using (List<DelegateMsgHandler>.Enumerator itr = handlers.GetEnumerator())
                {
                    while (itr.MoveNext())
                    {
                        itr.Current(msg);
                    }
                }
            }
        }

        public void PostMessage(Message msg)   //推入一个消息到队列 相当于隔帧在处理该消息 对应Update
        {
            mMsgQueue.Enqueue(msg);
        }

        //所以此Update应在GameManager中最先调用
        public void Update(float fTick)
        {
            using (Queue<Message>.Enumerator itr = mMsgQueue.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    SendMessage(itr.Current);
                }
            }
            mMsgQueue.Clear();
        }
        
        //注册
        public void Register(string name, DelegateMsgHandler handler)
        {
            List<DelegateMsgHandler> handlers = null;
            if (!mMsgHandlerHash.TryGetValue(name, out handlers))
            {
                handlers = new List<DelegateMsgHandler>();
                mMsgHandlerHash.Add(name, handlers);
            }

            handlers.Add(handler);
        }
        //注销
        public void Unregister(string name, DelegateMsgHandler handler)
        {
            List<DelegateMsgHandler> handlers = null;
            if (mMsgHandlerHash.TryGetValue(name, out handlers))
                handlers.Remove(handler);
        }
    }
}