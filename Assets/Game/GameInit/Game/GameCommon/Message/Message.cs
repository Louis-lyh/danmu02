﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-28
|FUNCTION：Messages passed in the game
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;

namespace GameInit.Game
{
    public class Message
    {
        private Dictionary<string, object> mArgHash = new Dictionary<string, object>();

        public string Name { get; set; }

        public Message(string name)
        {
            Name = name;
        }

        public void AddArg(string name, object arg)
        {
            mArgHash.Add(name, arg);
        }

        public object GetArg(string name)
        {
            object arg = null;
            if (mArgHash.TryGetValue(name, out arg))
                return arg;
            return null;
        }

        public void SetArg(string name, object arg)
        {
            mArgHash[name] = arg;
        }
    }
}