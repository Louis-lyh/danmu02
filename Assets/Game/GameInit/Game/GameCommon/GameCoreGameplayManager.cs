/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-28
|FUNCTION：Logic operation of master package
+-----------------------------------------------------------------------------*/

using GameHotfix.Game;
using UnityEngine;
using GameInit.Framework;

namespace GameInit.Game
{
    public class GameCoreGameplayManager : MonoBehaviour
    {
        private static GameCoreGameplayManager _instance;
        public static GameCoreGameplayManager Instance
        {
            get { return _instance; }
        }
        
        //初始化标记
        private bool _isInit;
        //销毁标记
        private bool _isDestroy;
        //激活标志
        private bool _isActive;
        //暂停标志
        private bool _isPause;

        void Awake()
        {
            if (_instance == null) _instance = this;
        }

        void Start()
        {
            _isActive = false;
            _isPause = false;
            
            Init();
        }
        
        void OnDestroy()
        {
            Destroy();

            _instance = null;
        }
        
        void Update()
        {
            //fTick
            float fTick = Time.deltaTime;
            
            //定时器
            TimerManager.Instance.Update(fTick);
            //消息
            MessageManager.Instance.Update(fTick);
            // 刷新战斗
            BattleManager.Instance.Update(fTick);
            //相机
            CameraManager.Instance.Update(fTick);
            // 输入
            InputManager.Instance.Update(fTick);
            
            //暂定
            if (_isPause) return;
        }

        void LateUpdate()
        {
            // //激活
            // if (!_isActive) return;
            // //暂定
            // if (_isPause) return;
            
            //fTick
            float fTick = Time.deltaTime;
            
            //相机
            CameraManager.Instance.LateUpdate(fTick);
        }
        
        void FixedUpdate()
        {
            
        }

        //Init
        public void Init()
        {
            if (_isInit) return;
            _isInit = true;
            _isDestroy = false;
            
            // 寻路
            RVOManager.Instance.Init();
            
            //
            CameraManager.Instance.Init();
        }
        
        //销毁
        public void Destroy()
        {
            if (_isDestroy) return;
            _isDestroy = true;
            _isInit = false;
            
            //销毁消息管理器
            MessageManager.Instance.Destroy();
            //定时器
            TimerManager.Instance.Destroy();
            //特效
            UEffectManager.Instance.Destroy();
            //相机
            CameraManager.Instance.Destroy();
            //RVO
            RVOManager.Instance.Dispose();
        }
        
        //激活与否
        public void SetActive(bool b)
        {
            _isActive = b;
        }
        
        //暂停与否
        public void SetPause(bool b)
        {
            _isPause = b;
        }
    }
}