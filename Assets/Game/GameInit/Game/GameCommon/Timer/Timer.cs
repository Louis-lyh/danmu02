﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：Synchronous timer
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;

namespace GameInit.Game
{
    public sealed class Timer : XObject
    {
        public delegate void TimerAction(params object[] param);

        private float mCurTime;
        private float mTotalTime;
        private bool mRepeat;
        private int mRepeatCount;
        private int mCurCount;
        private TimerAction mAction;
        private List<object> mParamList = new List<object>();

        public Timer(float duration, bool repeat, int repeatCount, TimerAction action, params object[] param)
        {
            mCurTime = 0f;
            mTotalTime = duration;
            mRepeat = repeat;
            mRepeatCount = repeatCount;
            mCurCount = 0;

            mAction = action;
            mParamList.AddRange(param);
        }

        public bool Update(float fTick)
        {
            mCurTime += fTick;
            if (mCurTime >= mTotalTime && mAction != null)
            {
                mAction(mParamList.ToArray());

                mCurTime -= mTotalTime;
                mCurCount++;

                return mRepeat ? (mRepeatCount < 0 ? false : (mCurCount < mRepeatCount ? false : true)) : true;
            }
            else
            {
                return false;
            }
        }

        public void Stop()
        {
            mTotalTime = 0f;
            mRepeat = false;
        }

        protected override void OnDispose()
        {
            mAction = null;
            mParamList.Clear();
        }
    }
}