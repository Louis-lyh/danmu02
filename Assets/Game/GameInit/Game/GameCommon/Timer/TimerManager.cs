﻿/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：Synchronous timer
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;

namespace GameInit.Game
{
    public sealed class TimerManager : Singleton<TimerManager>
    {
        private List<Timer> mTempTimerList = new List<Timer>();
        private List<Timer> mTimerList = new List<Timer>();
        private List<Timer> mFinishedList = new List<Timer>();

        public void Destroy()
        {
            using (List<Timer>.Enumerator itr = mTimerList.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    itr.Current.Dispose();
                }
            }
            mTimerList.Clear();
        }

        public Timer AddTimer(float duration, bool repeat, int repeatCount, Timer.TimerAction action, params object[] param)
        {
            Timer timer = new Timer(duration, repeat, repeatCount, action, param);
            mTempTimerList.Add(timer);

            return timer;
        }

        public void Update(float fTick)
        {
            if (mTempTimerList.Count > 0)
            {
                mTimerList.AddRange(mTempTimerList);
                mTempTimerList.Clear();
            }

            mFinishedList.Clear();
            using (List<Timer>.Enumerator itr = mTimerList.GetEnumerator())
            {
                while (itr.MoveNext())
                {
                    if (itr.Current.Update(fTick))
                    {
                        mFinishedList.Add(itr.Current);
                    }
                }
            }

            using (List<Timer>.Enumerator itr2 = mFinishedList.GetEnumerator())
            {
                while (itr2.MoveNext())
                {
                    itr2.Current.Dispose();
                    mTimerList.Remove(itr2.Current);
                }
            }
        }
    }
}