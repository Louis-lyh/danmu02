namespace GameInit.Game
{
    public static class ServerUrl
    {
        /// <summary>
        /// 字节后台 获取Token 地址
        /// </summary>
        public const string AccessTokenRelease = "https://developer.toutiao.com/api/apps/v2/token";
        
        /// <summary>
        /// 字节后台 获取Token 沙盒地址
        /// </summary>
        public const string AccessTokenDebug = "https://open-sandbox.douyin.com/api/apps/v2/token";

        /// <summary>
        /// Game Server Host
        /// </summary>
        public const string Host = "api.zhuoyue007.cn"; 
        //public const string Host = "118.116.82.65";

        /// <summary>
        /// Game Server Host
        /// </summary>
        public const int Port = 10086;
        
        /// <summary>
        /// Login
        /// </summary>
        public const string Login = "gate.connector.Login";
        
        /// <summary>
        /// StartGame
        /// </summary>
        public const string StartGame = "game.logic.StartGame";
        
        /// <summary>
        /// StopGame
        /// </summary>
        public const string StopGame = "game.logic.StopGame";
        
        /// <summary>
        /// Rank
        /// </summary>
        public const string Rank = "game.logic.Rank";
        
        /// <summary>
        /// GetExtension
        /// </summary>
        public const string GetExtension = "game.logic.GetExtension";
        
        /// <summary>
        /// SetExtension
        /// </summary>
        public const string SetExtension = "game.logic.SetExtension";
        
        /// <summary>
        /// LogicAction
        /// </summary>
        public const string LogicAction = "game.logic.Action";
        
        /// <summary>
        /// ServerTime
        /// </summary>
        public const string ServerTime = "game.logic.ServerTime";
        
        /// <summary>
        /// BarrageContent
        /// </summary>
        public const string BarrageContentBirth = "1";
        public const string BarrageContentAttack = "打怪";
        public const string BarrageContentCollect = "采集";
        public const string BarrageContentPosition = "定位";
        public const string BarrageContentExp = "666";
        public const string BarrageContentCheck = "查询";
        
        /// <summary>
        /// GiftId    
        /// </summary>
        public const string GiftIdWand  = "n1/Dg1905sj1FyoBlQBvmbaDZFBNaKuKZH6zxHkv8Lg5x2cRfrKUTb8gzMs=";
        public const string GiftIdMagicmirror  = "fJs8HKQ0xlPRixn8JAUiL2gFRiLD9S6IFCFdvZODSnhyo9YN8q7xUuVVyZI=";
        public const string GiftIdDoughnut  = "PJ0FFeaDzXUreuUBZH6Hs+b56Jh0tQjrq0bIrrlZmv13GSAL9Q1hf59fjGk=";
        public const string GiftIdEnergycell  = "IkkadLfz7O/a5UR45p/OOCCG6ewAWVbsuzR/Z+v1v76CBU+mTG/wPjqdpfg=";
        public const string GiftIdDemonbomb  = "gx7pmjQfhBaDOG2XkWI2peZ66YFWkCWRjZXpTqb23O/epru+sxWyTV/3Ufs=";
        public const string GiftIdMysteriousairdrop  = "pGLo7HKNk1i4djkicmJXf6iWEyd+pfPBjbsHmd3WcX0Ierm2UdnRR7UINvI=";
    }
}