using BestHTTP;
using UnityEngine;

namespace Game.GameInit
{
    public partial class HttpManager
    {
        private static HTTPRequest Request(System.Uri url, HTTPMethods methodType, bool isKeepAlive, bool disableCache, System.Action<HTTPRequest, HTTPResponse> callback)
        {
            // Debug.Log($"====HTTPMgr.Request sUrl:{url.ToString()}");
            var request = new HTTPRequest(url, methodType, isKeepAlive, disableCache, (req, resp) =>
            {
                switch (req.State)
                {
                    // The request finished without any problem.
                    case HTTPRequestStates.Finished:
                        if (resp.IsSuccess)
                        {
                            // Debug.Log("====HTTPMgr.Request finished Successfully");
                            callback?.Invoke(req, resp);
                        } 
                        else
                        {
                            Debug.LogWarning($"Request finished Successfully, but the server sent an error. Status Code: {resp.StatusCode}-{resp.Message} Message: {resp.DataAsText}");
                            callback?.Invoke(req, null);
                        }
                        break;

                    // The request finished with an unexpected error. The request's Exception property may contain more info about the error.
                    case HTTPRequestStates.Error:
                        Debug.LogError("Request Finished with Error! " + (req.Exception != null ? (req.Exception.Message + "\n" + req.Exception.StackTrace) : "No Exception"));
                        callback?.Invoke(req, null);
                        break;

                    // The request aborted, initiated by the user.
                    case HTTPRequestStates.Aborted:
                        Debug.LogWarning("Request Aborted!");
                        callback?.Invoke(req, null);
                        break;

                    // Connecting to the server is timed out.
                    case HTTPRequestStates.ConnectionTimedOut:
                        Debug.LogError("Connection Timed Out!");
                        callback?.Invoke(req, null);
                        break;

                    // The request didn't finished in the given time.
                    case HTTPRequestStates.TimedOut:
                        Debug.LogError("Processing the request Timed Out!");
                        callback?.Invoke(req, null);
                        break;
                    
                    // Default
                    default:
                        callback?.Invoke(req, null);
                        break;
                }
            });
            
            return request;
        }

        public static HTTPRequest Post(System.Uri url, System.Action<HTTPRequest, HTTPResponse> callback)
        {
            var request = Request(url, HTTPMethods.Post, true, true, callback);
            // request.ConnectTimeout = TimeSpan.FromSeconds(5);
            // request.Timeout = TimeSpan.FromSeconds(5);
            // request.Timeout = System.TimeSpan.FromSeconds(System.Double.NegativeInfinity);
            return request;
        }
        
        public static HTTPRequest Get(System.Uri url, System.Action<HTTPRequest, HTTPResponse> callback)
        {
            var request = Request(url, HTTPMethods.Get, false, true, callback);
            // request.Timeout = System.TimeSpan.FromSeconds(System.Double.NegativeInfinity);
            return request;
        }
    }
}