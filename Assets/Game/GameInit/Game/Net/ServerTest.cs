using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Game
{
    public class ServerTest : MonoBehaviour
    {
        private Text _msgText;

        private GameObject _view;

        private int _number;
        
        void Start()
        {
            //Test
            GameObject textObj = this.transform.Find("Scroll View/Viewport/TextMsg").gameObject;
            _msgText = textObj.GetComponent<Text>();
            _msgText.text = "";
            
            //View
            _view = this.transform.Find("Scroll View").gameObject;
            _view.SetActive(false);
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F12))
            {
                _view.SetActive(!_view.activeSelf);
            }
            else if (Input.GetKeyDown(KeyCode.F11))
            {
                _msgText.text = "";
            }
        }
        
        //text
        public void AddText(string str)
        {
            if (_number > 50)
            {
                _msgText.text = "";

                _number = 0;
            }
            
            string oldStr = _msgText.text;

            string newStr = "";
            if (string.IsNullOrEmpty(oldStr)) newStr = str;
            else newStr = oldStr + "\n" + str;

            _msgText.text = newStr;

            _number++;
        }
    }
}