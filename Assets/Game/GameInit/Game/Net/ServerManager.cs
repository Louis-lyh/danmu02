using Pomelo.DotNetClient;
using System;
using System.Collections.Generic;
using Cysharp.Text;
using SimpleJson;
using UnityEngine;
using LitJson;

namespace GameInit.Game
{
    public class ServerManager : MonoBehaviour
    {
        //Instance
        private static ServerManager _instance;

        public static ServerManager Instance
        {
            get { return _instance; }
        }

        //Connection
        private Connection _connection;

        //Log
        private ServerTest _serverLog;

        void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(this);
            }
            else
            {
                GameInit.Framework.Logger.LogGreen("ServerManager Existed");
            }
        }

        void Start()
        {
            //Connection
            if (_connection == null) _connection = new Connection();

            //Log
            if (_serverLog == null)
            {
                GameObject objWnd = GameObject.FindWithTag("ServerTestWnd");
                _serverLog = objWnd.GetComponent<ServerTest>();
                if (_serverLog == null) _serverLog = objWnd.AddComponent<ServerTest>();
            }
        }

        void OnDestroy()
        {
            if (_connection != null) _connection.Disconnect();
        }

        void Update()
        {
            if (_connection != null) _connection.Update();
        }

        //Show Log
        public void ShowLogText(string str)
        {
            _serverLog.AddText(str);
        }

        #region Connect Login
        //token
        private string _token;

        //完成标记
        private bool _connectLoginComplete;
        public bool ConnectLoginComplete => _connectLoginComplete;
        
        // 开始游戏返回信息
        private int _guildId;
        public int GuildId => _guildId;

        //登录成功后的uid
        private string _currentUId;

        //登录成功后的房间号
        private long _currentRoomId;

        //action-log
        private Action<string> _connectLogAction;

        private void LogConnectMsg(string msg)
        {
            _connectLogAction?.Invoke(msg);
        }

        //action-cmd
        private Action<ServerData.LacEventExecute> _cmdAction;

        public void Connect(string token, Action<string> logAction, Action<ServerData.LacEventExecute> cmdAction)
        {
            _token = token;
            _connectLogAction = logAction;
            _cmdAction = cmdAction;

            if (_connection.netWorkState != NetWorkState.DISCONNECTED)
                return;

            _connectLoginComplete = false;

            //log
            LogConnectMsg("connect to server...");
            //connect
            _connection.InitClient(ServerUrl.Host, ServerUrl.Port, msgObj =>
            {
                JsonObject user = new JsonObject();
                _connection.connect(user, data =>
                {
                    //添加监听
                    AddActionListen();

                    //login
                    Login();
                });
            });
        }

        private void Login()
        {
            //login msg
            JsonObject msg = new JsonObject();
            msg.Add("token", _token);
            //login
            _connection.request(ServerUrl.Login, msg, LoginCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.Login, " ", msg));
        }

        private void LoginCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("login back json: " + jsonObj);
            ShowLogText("login back json: " + jsonObj);

            //登录结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取登录信息
                ServerData.LoginBackData info = JsonMapper.ToObject<ServerData.LoginBackData>(jsonObj["data"].ToString());

                _currentUId = info.uid;
                _currentRoomId = info.roomId;
                _guildId = info.guildId;

                GameInit.Framework.Logger.LogGreen(ZString.Concat("login succeeded. uid:", _currentUId, " roomid:", _currentRoomId));
                LogConnectMsg("waiting to start the game...");

                //当主播房间处于游戏中，不用再开始游戏
                if (info.inGame)
                {
                    //到这里才出现开始冒险的按钮
                    _connectLoginComplete = true;
                }
                else
                {
                    //执行开始游戏
                    StartGame((info) =>
                    {
                        //到这里才出现开始冒险的按钮
                        _connectLoginComplete = true;
                    });
                }
                
                // //执行开始游戏
                // StartGame(() =>
                // {
                //     //到这里才出现开始冒险的按钮
                //     _connectLoginComplete = true;
                // });
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("login error: " + jsonObj["msg"]);
                LogConnectMsg("login failed...");
            }
        }
        #endregion

        #region StartGame
        private Action<ServerData.StartGameData> _startGameCallback;

        public void StartGame(Action<ServerData.StartGameData> callback)
        {
            _startGameCallback = callback;

            //start game msg
            JsonObject msg = new JsonObject();
            //start game
            _connection.request(ServerUrl.StartGame, msg, StartGameCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.StartGame, " ", msg));
        }

        private void StartGameCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("start game back json: " + jsonObj);
            ShowLogText(ZString.Concat("start game back json: " + jsonObj));

            //开始本轮游戏结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取开始游戏信息
                ServerData.StartGameData info =
                    JsonMapper.ToObject<ServerData.StartGameData>(jsonObj["data"].ToString());

                string roundId = info.roundId;

                //log
                GameInit.Framework.Logger.LogGreen(ZString.Concat("start game succeeded. roundid:", roundId));
                ShowLogText(ZString.Concat("开始游戏成功, 当前轮数为: ", roundId));
                ShowLogText(ZString.Concat("开始游戏成功, 返回信息：", jsonObj["data"]));

                //执行开始本轮游戏
                if (_startGameCallback != null)
                {
                    _startGameCallback.Invoke(info);
                    _startGameCallback = null;
                }
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("start game error: " + jsonObj["msg"]);
            }
        }
        #endregion

        #region StopGame
        private Action<ServerData.StopGameData> _stopGameCallback;

        public void StopGame(ServerData.StopGameSendData sendData, Action<ServerData.StopGameData> stopGameCallback)
        {
            _stopGameCallback = stopGameCallback;

            //start game msg
            JsonObject msg = new JsonObject();
            msg.Add("winCamp", sendData.winCamp);
            JsonObject scores = new JsonObject();
            foreach (var v in sendData.scores)
            {
                string key = v.Key;
                long value = v.Value;
                scores.Add(key, value);
            }

            JsonObject camps = new JsonObject();
            foreach (var v in sendData.camps)
            {
                string key = v.Key;
                int value = v.Value;
                camps.Add(key, value);
            }

            msg.Add("scores", scores);
            msg.Add("camps", camps);
            //start game
            _connection.request(ServerUrl.StopGame, msg, StopGameCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.StopGame, " ", msg));
        }

        private void StopGameCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("stop game back json: " + jsonObj);
            ShowLogText(ZString.Concat("stop game back json: " + jsonObj));

            //结束本轮游戏结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取结束游戏信息
                ServerData.StopGameData info = JsonMapper.ToObject<ServerData.StopGameData>(jsonObj["data"].ToString());

                string roundId = info.roundId;

                //log
                GameInit.Framework.Logger.LogGreen(ZString.Concat("end game succeeded. roundid:", roundId));
                ShowLogText(ZString.Concat("结束游戏成功, 当前轮数为: ", roundId));

                //执行结束本轮游戏
                if (_stopGameCallback != null)
                {
                    _stopGameCallback.Invoke(info);
                    _stopGameCallback = null;
                }
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("stop game error: " + jsonObj["msg"]);
            }
        }
        #endregion

        #region GetRank
        private Action<ServerData.RankData> _getRankCallback;

        public void GetRank(ServerData.ERankType type, Action<ServerData.RankData> callback)
        {
            _getRankCallback = callback;

            string rankType = "";
            if (type == ServerData.ERankType.e_player) rankType = "player";
            else if (type == ServerData.ERankType.e_werwolf) rankType = "werwolf";

            //start game msg
            JsonObject msg = new JsonObject();
            msg.Add("rankType", rankType);
            //get rank
            _connection.request(ServerUrl.Rank, msg, GetRankCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.Rank, " ", msg));
        }

        private void GetRankCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("get rank back json: " + jsonObj);
            ShowLogText(ZString.Concat("get rank back json: " + jsonObj));

            //获取排行榜结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取排行榜信息
                ServerData.RankData info = JsonMapper.ToObject<ServerData.RankData>(jsonObj["data"].ToString());

                //log
                GameInit.Framework.Logger.LogGreen("get rank succeeded.");

                //执行
                if (_getRankCallback != null)
                {
                    _getRankCallback.Invoke(info);
                    _getRankCallback = null;
                }
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("get rank error: " + jsonObj["msg"]);
            }
        }
        #endregion

        #region Extension

        private Action _setExtensionCallback;
        public void SetExtension(ServerData.ExtensionData sendData, Action setExtensionCallback)
        {
            _setExtensionCallback = setExtensionCallback;
                
            //SetExtension msg
            JsonObject msg = new JsonObject();
            msg.Add("type", sendData.type);
            
            JsonObject extensions = new JsonObject();
            foreach (var v in sendData.extensions)
            {
                string key = v.Key;
                var extension = v.Value;
                
                JsonObject temp = new JsonObject();
                foreach (var ext in extension)
                {
                    temp.Add(ext.Key,ext.Value);
                }
                extensions.Add(key, temp);
            }
            msg.Add("extensions", extensions);
            
            //SetExtension
            _connection.request(ServerUrl.SetExtension, msg, SetExtensionCallBack);

            //log
            GameInit.Framework.Logger.LogGreen("set extension json: " + msg);
            ShowLogText(ZString.Concat(ServerUrl.SetExtension, " ", msg));
        }

        private void SetExtensionCallBack(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("set extension back json: " + jsonObj);
            ShowLogText(ZString.Concat("set extension back json: " + jsonObj));

            //设置扩展数据返回结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                _setExtensionCallback?.Invoke();
                _setExtensionCallback = null;
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("set extension error: " + jsonObj["msg"]);
            }
        }
        
        // 获取扩展数据
        private Action<Dictionary<string,Dictionary<string,int>>> _getExtensionCallback;
        public void GetExtension(string[] uids,  Action<Dictionary<string,Dictionary<string,int>>> callback)
        {
            _getExtensionCallback = callback;
            
            //start game msg
            JsonObject msg = new JsonObject();
            msg.Add("uids", uids);
            //get extension
            _connection.request(ServerUrl.GetExtension, msg, GetExtensionCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.GetExtension, " ", msg));
        }

        private void GetExtensionCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("get extension back json: " + jsonObj);
            ShowLogText(ZString.Concat("get extension back json: " + jsonObj));

            //获取扩展结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取扩展信息
                Dictionary<string,Dictionary<string,int>> info = JsonMapper.ToObject<Dictionary<string,Dictionary<string,int>>>(jsonObj["data"].ToString());

                //log
                GameInit.Framework.Logger.LogGreen("get extension succeeded.");

                //执行
                if (_getExtensionCallback != null)
                {
                    _getExtensionCallback.Invoke(info);
                    _getExtensionCallback = null;
                }
            }
            else //失败
            {
                GameInit.Framework.Logger.LogError("get extension error: " + jsonObj["msg"]);
            }
        }
        #endregion

        #region ServerTime
        private Action<long> _getServerTimeBack;
        
        // 获取时间
        public void GetServerTime(Action<long> callback)
        {
            _getServerTimeBack = callback;
            
            //start game msg
            JsonObject msg = new JsonObject();
            //get rank
            _connection.request(ServerUrl.ServerTime, msg, GetServerTimeCallback);

            //log
            ShowLogText(ZString.Concat(ServerUrl.ServerTime, " ", msg));
        }

        private void GetServerTimeCallback(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("get Server Time json: " + jsonObj);
            ShowLogText(ZString.Concat("get Server Time json: " + jsonObj));

            //获取排行榜结果
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取排行榜信息
                long time = long.Parse(jsonObj["data"].ToString());

                //log
                GameInit.Framework.Logger.LogGreen("get Server Time succeeded." + time);

                //执行
                if (_getServerTimeBack != null)
                {
                    _getServerTimeBack.Invoke(time);
                    _getServerTimeBack = null;
                }
            }
            else //失败
            {
                _getServerTimeBack.Invoke(DateTime.Now.Ticks);
                GameInit.Framework.Logger.LogError("get Server Time error: " + jsonObj["msg"]);
            }
        }
        

        #endregion

        #region Action
        private void AddActionListen()
        {
            _connection.on(ServerUrl.LogicAction, ActionListen);
        }

        private void ActionListen(Pomelo.DotNetClient.Message message)
        {
            JsonObject jsonObj = message.jsonObj;

            //log
            GameInit.Framework.Logger.LogGreen("logic action back json: " + jsonObj);
            ShowLogText("logic action back json: " + jsonObj);

            //逻辑事件分析
            string code = jsonObj["code"].ToString();
            if (code == "1") //成功
            {
                //获取消息
                ServerData.LogicActionData info = JsonMapper.ToObject<ServerData.LogicActionData>(jsonObj["data"].ToString());
                
                string msgType = info.msgType;
                
                //log
                GameInit.Framework.Logger.LogGreen(ZString.Concat("logic action get. msgtype:", msgType));

                //分析
                ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
                lacEventExecute.userId = info.userId;
                lacEventExecute.userName = info.userName;
                lacEventExecute.headUrl = info.headUrl;
                // 连胜
                lacEventExecute.winStreak = info.winStreak;
                // 排名
                lacEventExecute.rank = info.rank;
                // 分数 
                lacEventExecute.score = info.score;
                // 扩展数据
                lacEventExecute.extension = info.extension;
                // 工会id
                lacEventExecute.guildId = info.guildId;

                switch (msgType)
                {
                    case "live_comment": //弹幕
                    {
                        lacEventExecute.lcType = ServerData.ELogicActionType.e_comment;

                        string content = info.content;
                        switch (content)
                        {
                            case ServerUrl.BarrageContentBirth: //诞生
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_birth;
                            } break;
                            case ServerUrl.BarrageContentAttack: //打怪
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_attack;
                            } break;
                            case ServerUrl.BarrageContentCollect: //采集
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_collect;
                            } break;
                            case ServerUrl.BarrageContentPosition: //定位
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_position;
                            } break;
                            case ServerUrl.BarrageContentExp: //加经验
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_exp;
                            } break;
                            case ServerUrl.BarrageContentCheck: //查询
                            {
                                lacEventExecute.bType = ServerData.EBarrageType.e_check;
                            }break;
                        }
                        //处理
                        ActionRunComment(lacEventExecute);
                    } break;
                    case "live_like": //点赞
                    {
                        lacEventExecute.lcType = ServerData.ELogicActionType.e_like;

                        lacEventExecute.likeNum = info.likeNum;

                        //处理
                        ActionRunLike(lacEventExecute);
                    } break;
                    case "live_gift": //礼物
                    {
                        lacEventExecute.lcType = ServerData.ELogicActionType.e_gift;

                        lacEventExecute.giftNum = info.giftNum;
                        lacEventExecute.giftValue = info.giftValue;

                        string giftid = info.giftId;
                        switch (giftid)
                        {
                            case ServerUrl.GiftIdWand: //仙女棒
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_wand;
                            } break;
                            case ServerUrl.GiftIdMagicmirror: //魔方镜
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_magicmirror;
                            } break;
                            case ServerUrl.GiftIdDoughnut: //甜甜圈
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_doughnut;
                            } break;
                            case ServerUrl.GiftIdEnergycell: //能量电池
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_energycell;
                            } break;
                            case ServerUrl.GiftIdDemonbomb: //恶魔炸弹
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_demonbomb;
                            } break;
                            case ServerUrl.GiftIdMysteriousairdrop: //神秘空投
                            {
                                lacEventExecute.giftType = ServerData.EGiftType.e_mysteriousairdrop;
                            } break;
                        }

                        //处理
                        ActionRunGift(lacEventExecute);
                    } break;
                }
            }
        }

        //行为处理-弹幕
        public void ActionRunComment(ServerData.LacEventExecute lacEventExecute)
        {
            switch (lacEventExecute.bType)
            {
                case ServerData.EBarrageType.e_birth: //诞生
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "诞生"));
                } break;
                case ServerData.EBarrageType.e_attack: //打怪
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "打怪"));
                } break;
                case ServerData.EBarrageType.e_collect: //采集
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "采集"));
                } break;
                case ServerData.EBarrageType.e_position: //定位
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "定位"));
                } break;
                case ServerData.EBarrageType.e_exp: //经验
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "经验"));
                } break;
                case ServerData.EBarrageType.e_check: //查询
                {
                    ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "查询"));
                } break;
            }

            if (_cmdAction != null) _cmdAction(lacEventExecute);
        }

        //行为处理-点赞
        public void ActionRunLike(ServerData.LacEventExecute lacEventExecute)
        {
            ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "点赞 数量为: ", lacEventExecute.likeNum));

            if (_cmdAction != null) _cmdAction(lacEventExecute);
        }

        //行为处理-礼物
        public void ActionRunGift(ServerData.LacEventExecute lacEventExecute)
        {
            ShowLogText(ZString.Concat(lacEventExecute.userName, " => ", "礼物类型: ", lacEventExecute.giftType, " 礼物数量: ", lacEventExecute.giftNum, " 礼物价值: ", lacEventExecute.giftValue));

            if (_cmdAction != null) _cmdAction(lacEventExecute);
        }
        #endregion
    }
}