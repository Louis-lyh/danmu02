using System.Collections.Generic;

namespace GameInit.Game
{
    public partial class ServerData
    {
        /// <summary>
        /// 字节后台 获取Token 请求信息
        /// </summary>
        public class AccessTokenRequest
        {
            public string appid;
            public string secret;
            public string grant_type;
        } 
        
        /// <summary>
        /// 字节后台 获取Token 返回信息
        /// </summary>
        public class AccessTokenData
        {
            public string access_token;     //token
            public int expires_in;          //token时限
        }
        public class AccessTokenInfo
        {
            public int err_no;              //错误
            public string err_tips;         //提示
            public AccessTokenData data;    //data
        }
        
        /// <summary>
        /// 登录返回信息
        /// </summary>
        public class LoginBackData
        {
            public string uid;
            public long roomId;
            public string avatar;       //头像url
            public string nickname;     //昵称
            public bool inGame;         //是否已在游戏中，短线处理。
            public int guildId;        // 公会id
        }
        // public class LoginBackInfo
        // {
        //     public string code;         //成功为1、其他失败
        //     public string msg;          //非1时有错误消息
        //     public LoginBackData data;  //数据
        // }
        
        /// <summary>
        /// 排行结构
        /// </summary>
        public class Rank
        {
            public string userId;       //用户唯一id
            public string userName;     //用户昵称
            public string headUrl;      //用户头像
            public long score;           //月总分
            public int winStreak;       //连胜
            public Dictionary<string,int> extension;    //扩展数据
        }
        
        /// <summary>
        /// 结算结构
        /// </summary>
        public class Settlement
        {
            public string userId;       //用户唯一id
            public string userName;     //用户昵称
            public string headUrl;      //用户头像
            public long score;           //本局分数
            public long monthlyScore;    //月总分
            public int winStreak;       //连胜
            public int rank;            //排行
            public int camp;            //派别 0:小动物 1：狼人
            public Dictionary<string,int> extension;    // 扩展数据
        }

        /// <summary>
        /// 开始游戏返回信息
        /// </summary>
        public class StartGameData
        {
            public string roundId;      //本局轮数
            public int guildId;    // 工会id
        }
        // public class StartGameInfo
        // {
        //     public string code;         //成功为1、其他失败
        //     public string msg;          //非1时有错误消息
        //     public StartGameData data;  //数据
        // }

        /// <summary>
        /// 结束游戏发送信息
        /// </summary>
        public class StopGameSendData
        {
            public int winCamp;                         //胜利阵营 0是动物，1数狼人
            public Dictionary<string, long> scores;      //积分(uid, 分数)
            public Dictionary<string, int> camps;       //阵营(uid, 0是动物，1数狼人)
            public Dictionary<string, Dictionary<string,int>> extensions; // 扩展数据
        }

        /// <summary>
        /// 结束游戏返回信息
        /// </summary>
        public class StopGameData
        {
            public string roundId;                //本局轮数
            public List<Settlement> rankList ;    //公共排行榜
        }
        // public class StopGameInfo
        // {
        //     public string code;         //成功为1、其他失败
        //     public string msg;          //非1时有错误消息
        //     public StopGameData data;   //数据
        // }
        
        /// <summary>
        /// 排行榜类型
        /// </summary>
        public enum ERankType
        {
            e_player = 1,
            e_werwolf
        }
        
        /// <summary>
        /// 排行榜返回信息
        /// </summary>
        public class RankData
        {
            public List<Rank> list;
        }
        // public class RankInfo
        // {
        //     public string code;         //成功为1、其他失败
        //     public string msg;          //非1时有错误消息
        //     public RankData data;       //数据
        // }
        
        /// <summary>
        /// 上传扩展数据信息
        /// </summary>
        public class ExtensionData
        {
            public int type;        //1:set, 2:add, 3:max;
            public Dictionary<string,Dictionary<string,int>> extensions;
        }

        /// <summary>
        /// 逻辑事件返回信息
        /// </summary>
        public class LogicActionData
        {
            public string msgType;          //消息类型 live_gift、live_like、live_comment、根据不同消息获取下面对应值
            public string userId;           //用户唯一id
            public string userName;         //用户昵称
            public string headUrl;          //用户头像
            public string content;          //消息内容
            public string giftId;           //礼物id
            public int giftNum;             //礼物数量
            public int giftValue;           //礼物价值
            public int likeNum;             //点赞数量
            public int rank;                //全局排行
            public long score;                //全局排行
            public int winStreak;           //连胜
            public Dictionary<string,int> extension;        //扩展数据
            public int guildId;    // 工会id

        }
        // public class LogicActionInfo
        // {
        //     public string code;             //成功为1、其他失败
        //     public string msg;              //非1时有错误消息
        //     public LogicActionData data;    //数据
        // }

        /// <summary>
        /// 逻辑事件类型
        /// </summary>
        public enum ELogicActionType
        {
            e_comment = 1,  //弹幕
            e_like,         //点赞
            e_gift          //礼物
        }
        /// <summary>
        /// 弹幕类型
        /// </summary>
        public enum EBarrageType
        {
            e_birth = 1,    //诞生
            e_attack,       //打怪
            e_collect,      //采集
            e_position,     //定位
            e_exp,        //经验
            e_check,       //查询
        }

        /// <summary>
        /// 礼物类型
        /// </summary>
        public enum EGiftType
        {
            e_wand = 1,             //仙女棒
            e_magicmirror,          //魔方镜
            e_doughnut,             //甜甜圈
            e_energycell,           //能量电池
            e_demonbomb,            //恶魔炸弹
            e_mysteriousairdrop     //神秘空投
        }
        
        // 扩展数据类型
        public enum ExtensionType
        {
            PetExp = 0,
            HaloExp,
            HeroExp,
            KillCount,
        }
        
        /// <summary>
        /// 事件执行结构
        /// </summary>
        public class LacEventExecute
        {
            public ELogicActionType lcType;     //事件类型

            public string userId;               //用户唯一id
            public string userName;             //用户昵称
            public string headUrl;              //用户头像
            
            public EBarrageType bType;          //弹幕类型

            public int winStreak;               //连胜
            public int rank;                    //全局排行
            public long score;                    // 分数

            public int likeNum;                 //点赞数量

            public EGiftType giftType;          //礼物类型
            public int giftNum;                 //礼物数量
            public int giftValue;               //礼物价值
            
            public Dictionary<string,int> extension;        //扩展数据
            public int guildId;              // 工会id
        }
    }
}