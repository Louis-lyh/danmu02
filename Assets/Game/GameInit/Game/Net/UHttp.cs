using System;
using System.Collections.Generic;
using System.Text;

namespace Game.GameInit
{
    public partial class UHttp
    {
        public static bool IsBlack; // 黑名单

        public static void Post(string rest, Dictionary<string, object> body, Action<string> callback)
        {
            if (IsBlack)
            {
                return;
            }

            Uri url = new Uri(rest);
            var request = HttpManager.Post(url, (req, resp) =>
            {
                if (resp == null)
                {
                    callback?.Invoke("null");
                    return;
                }
                
                callback?.Invoke(resp.DataAsText);
            });
            
            request.AddHeader("Content-Type", "application/json");
            foreach (var item in body)
            {
                request.AddField(item.Key, item.Value.ToString());
            }
            
            request.Send();
        }

        public static void Post(string rest, string body, Action<string> callback)
        {
            if (IsBlack)
            {
                return;
            }
            
            Uri url = new Uri(rest);
            var request = HttpManager.Post(url, (req, resp) =>
            {
                if (resp == null)
                {
                    callback?.Invoke("null");
                    return;
                }
                
                callback?.Invoke(resp.DataAsText);
            });
            
            request.AddHeader("Content-Type", "application/json");
            request.RawData = Encoding.UTF8.GetBytes(body);
            
            request.Send();
        }

        public static void Get(string rest, Dictionary<string, string> queryParams, Action<string> callback)
        {
            if (IsBlack)
            {
                return;
            }

            bool first = true;
            if (queryParams != null)
            {
                foreach (var item in queryParams)
                {
                    string query = (first ? "?" : "&") + item.Key + "=" + item.Value;
                    first = false;
                    rest += query;
                }
            }

            Uri url = new Uri(rest);
            var request = HttpManager.Get(url, (req, resp) =>
            {
                if (resp == null)
                {
                    callback?.Invoke("null");
                    return;
                }
                
                callback?.Invoke(resp.DataAsText);
            });
            
            request.AddHeader("Content-Type", "application/json");
            request.Send();
        }
    }
}