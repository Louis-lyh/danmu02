﻿using System;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class ServerTimerManager : Singleton<ServerTimerManager>
    {
        // 当前时间
        public long GameTime{ get; private set; }
        // 时间定时器
        private uint _gameTimer;
        /// <summary>
        /// 开始游戏时间
        /// </summary>
        public void StartTime()
        {
            // 编辑器
            if (Application.platform == RuntimePlatform.WindowsEditor)
                GameTime = GameConstantHelper.DateTimeToUnixTimeStamp(DateTime.Now);
            // 获取时间
            else
            {
                GameTime = GameConstantHelper.DateTimeToUnixTimeStamp(DateTime.Now);
                ServerManager.Instance.GetServerTime((time) => { GameTime = time;});
            }


            // 定时刷新时间
            _gameTimer = TimerHeap.AddTimer(0, 1000, () =>
            {
                GameTime += 1;
                //
                var tempTime = GameConstantHelper.UnixTimeStampToDateTime(GameTime);
                // 刷新时间
                BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventRefreshGameTime,tempTime);
            });
        }
        
        /// <summary>
        /// 停止游戏时间
        /// </summary>
        private void StopTime()
        {
            TimerHeap.DelTimer(_gameTimer);            
        }
        
        /// <summary>
        /// 获取当前小时
        /// </summary>
        /// <returns></returns>
        public int GetHour()
        {
            return GameConstantHelper.UnixTimeStampToDateTime(GameTime).Hour;
        }
    }
}