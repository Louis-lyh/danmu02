using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameInit.Framework
{
    // Rvo寻路
    public class RVOManager : Singleton<RVOManager>
    {
        // 寻路模拟器
        private List<Simulator> _simulators;

        // 英雄代理
        private List<Dictionary<int, GameAgent>> _gameAgentList;

        public void Init()
        {
            // 初始化模拟器
            InitSimulator();
            // 初始化代理器字典
            InitGameAgentDic();
        }

        // 初始化模拟器
        private void InitSimulator()
        {
            _simulators = new List<Simulator>();
            foreach (int i in Enum.GetValues(typeof(RVOLayer)))
            {
                // 障碍不创建
                if (i == (int) RVOLayer.Node)
                    continue;

                var simulator = new Simulator();
                _simulators.Add(simulator);
                // 设置刷新时间
                simulator.setTimeStep(0.25f);
                // 这个代理的默认属性
                simulator.setAgentDefaults(0.5f, 1, 1f,
                    1f, 0.4f, 0.1f,
                    new Vector2(0.0f, 0.0f));
                // add in awake
                simulator.processObstacles();
            }
        }

        // 初始化代理器字典
        private void InitGameAgentDic()
        {
            _gameAgentList = new List<Dictionary<int, GameAgent>>();
            foreach (int i in Enum.GetValues(typeof(RVOLayer)))
            {
                // 障碍不创建
                if (i == (int) RVOLayer.Node)
                    continue;

                _gameAgentList.Add(new Dictionary<int, GameAgent>());
            }
        }

        // 刷新
        public void Update(float deltaTime)
        {
            // 刷新模拟器
            for (int i = 0; i < _simulators.Count; i++)
            {
                _simulators[i].doStep();
            }

            // 刷新代理逻辑
            for (int i = 0; i < _gameAgentList.Count; i++)
            {
                var gameAgentDic = _gameAgentList[i];
                var gameAgentKeys = gameAgentDic.Keys.ToList();
                for (int j = 0; j < gameAgentKeys.Count; j++)
                {
                    var key = gameAgentKeys[j];
                    gameAgentDic[key].Update();
                }
            }
        }

        // 删除代理
        public void DeleteAgent(int sid)
        {
            for (int i = 0; i < _gameAgentList.Count; i++)
            {
                var gameAgentDic = _gameAgentList[i];
                if (gameAgentDic.ContainsKey(sid))
                {
                    gameAgentDic.Remove(sid); // 移除字典
                    _simulators[i].delAgent(sid); // 移除模拟器
                }
            }
        }

        // 创建代理
        public GameAgent CreateAgent(Transform self, float radius, float speed, RVOLayer layer)
        {
            // 障碍不创建代理
            if (layer == RVOLayer.Node)
                return null;

            int sid = -1;
            int type = (int) layer;
            // 二维坐标
            var pos = new Vector2(self.position.x, self.position.z);
            // 添加代理
            sid = _simulators[type].addAgent(pos,
                7f, 10, 3f, 3f, radius, speed,
                new Vector2(0f, 0f));

            if (sid == -1)
            {
                Debug.LogError("创建代理失败");
                return null;
            }

            // 创建
            GameAgent gameAgent = new GameAgent(sid, self, _simulators[type]);
            // 加入字典
            _gameAgentList[type].Add(sid, gameAgent);

            return gameAgent;
        }

        // 修改速度半径
        public void SetAgent(int id, float radius, float speed, RVOLayer layer)
        {
            int layerInt = (int) layer;
            _simulators[layerInt].setAgentRadius(id, radius);
            _simulators[layerInt].setAgentMaxSpeed(id, speed);
        }

        // 修改速度 
        public void SetSpeed(int id, float speed, RVOLayer layer)
        {

            int layerInt = (int) layer;
            _simulators[layerInt].setAgentMaxSpeed(id, speed);
        }

        // 修改位置
        public void SetPos(int id, Vector3 pos, RVOLayer layer)
        {
            // 二维坐标
            var posV2 = new Vector2(pos.x, pos.z);

            int layerInt = (int) layer;
            _simulators[layerInt].setAgentPosition(id, posV2);
        }

        // 修改寻路层级
        public void SetLayer(GameAgent gameAgent, float radius, float speed, RVOLayer layer)
        {
            // 删除以前的层级
            DeleteAgent(gameAgent.Sid);

            // 障碍不创建代理
            if (layer == RVOLayer.Node)
                return;

            int sid = -1;
            int type = (int) layer;
            // 二维坐标
            var position = gameAgent.Transform.position;
            var posVector2 = new Vector2(position.x, position.z);
            // 添加代理
            sid = _simulators[type].addAgent(posVector2,
                7f, 10, 3f, 3f, radius, speed,
                new Vector2(0f, 0f));

            if (sid == -1)
            {
                Debug.LogError("更新代理失败");
                return;
            }

            // 创建
            gameAgent.UpdateData(sid, _simulators[type]);
            // 加入字典
            _gameAgentList[type].Add(sid, gameAgent);
        }
        // 创建障碍
        public List<int> CreateObstacle(Vector3 Position, float sideLength, float sideWidth, RVOLayer layer)
        {
            float minX = Position.x - sideWidth;
            float minZ = Position.z - sideLength;

            float maxX = Position.x + sideWidth;
            float maxZ = Position.z + sideLength;

            IList<Vector2> obstacle = new List<Vector2>();
            obstacle.Add(new Vector2(maxX, maxZ));
            obstacle.Add(new Vector2(minX, maxZ));
            obstacle.Add(new Vector2(minX, minZ));
            obstacle.Add(new Vector2(maxX, minZ));
            
            return CreateObstacle(obstacle,layer);
        }
        // 创建障碍
        public List<int> CreateObstacle(Vector3 Position, float sideLength, RVOLayer layer)
        {
            float minX = Position.x - sideLength;
            float minZ = Position.z - sideLength;
            float maxX = Position.x + sideLength;
            float maxZ = Position.z + sideLength;

            IList<Vector2> obstacle = new List<Vector2>();
            obstacle.Add(new Vector2(maxX, maxZ));
            obstacle.Add(new Vector2(minX, maxZ));
            obstacle.Add(new Vector2(minX, minZ));
            obstacle.Add(new Vector2(maxX, minZ));
            
            return CreateObstacle(obstacle,layer);
        }
    
        // 创建障碍
        public List<int> CreateObstacle(IList<Vector2> obstacle, RVOLayer layer)
        {
            // 不为障碍 加入指定寻路层级
            if (layer != RVOLayer.Node)
            {
                // 添加障碍
                var pointIndexList = _simulators[(int)layer].addObstacle(obstacle);
                // 重新计算障碍
                _simulators[(int)layer].processObstacles();
                // 返回障碍索引
                return pointIndexList;
            }
            // 为障碍 加入所有寻路层级
            else
            {
                var pointIndexLost = new List<int>();
                for (int i = 0; i < _simulators.Count; i++)
                {
                    var simulator = _simulators[i];
                    // 添加障碍
                    var points = simulator.addObstacle(obstacle);
                    // 加入列表
                    for (int j = 0; j < points.Count; j++)
                        pointIndexLost.Add(points[j]);
                    // 重新结算障碍
                    simulator.processObstacles();
                }
                Debug.Log("pointIndexLost "+pointIndexLost);
                return pointIndexLost;
            }
        } 
        // 删除障碍
        public void DeleteObstacle(List<int> obstacleIndexs,RVOLayer layer)
        {
            // 不是障碍层 删除指定障碍
            if (layer != RVOLayer.Node)
            {
                // 删除障碍
                _simulators[(int)layer].DeleteObstacle(obstacleIndexs);
                // 重新计算障碍
                _simulators[(int)layer].processObstacles();
            }
            // 障碍层 删除所有层中的障碍
            else
            {
                for (int i = 0; i < _simulators.Count; i++)
                {
                    var simulator = _simulators[i];
                    // 删除障碍
                    simulator.DeleteObstacle(obstacleIndexs);
                    // 重新计算障碍
                    simulator.processObstacles();
                }
            }
        }

        public void Dispose()
        {
            // 清理模拟器
            for (int i = 0; i < _simulators.Count; i++)
            {
                _simulators[i].Clear();
            }
            _simulators.Clear();
            
            // 清理字典
            _gameAgentList.Clear();
        }
    }
    
    // 寻路层级
    public enum RVOLayer
    {
        Node = -1,    // 障碍
        Layer01 = 0, 
        Layer02 = 1,
        Layer03 = 2,
        Layer04 = 3,
        Layer05 = 4,
    }

}


