﻿using System;
using GameInit.Framework;
using UnityEngine;
using Random = System.Random;
using Vector2 = GameInit.Framework.Vector2;

public class GameAgent
{
    public int Sid { get; private set; }

    /** Random number generator. */
    private Random m_random = new Random();
    // 模拟器
    private Simulator _simulator;
    // 战斗单位
    public Transform Transform { get; }
    // 目标位置 
    private Vector2 _targetPos;
    // 是否移动 
    private bool _isMoving;
    // 暂停代理
    private bool _stopAgent;
    public GameAgent(int sid,Transform self,Simulator simulator)
    {
        Sid = sid;
        Transform = self;
        _simulator = simulator;
        _stopAgent = false;
    }
    // 更新数据
    public void UpdateData(int sid,Simulator simulator)
    {
        Sid = sid;
        _simulator = simulator;
    }

    // 开始移动
    public void StartMove(Vector3 pos)
    {
        _targetPos = new Vector2(pos.x,pos.z);
        _isMoving = true;
    }
    // 停止移动
    public void StopMove()
    {
        _isMoving = false;
    }

    // 获取速度方向
    public Vector3 GetVel()
    {
        // 获取速度
        Vector2 vel = _simulator.getAgentPrefVelocity(Sid);
        return new Vector3(vel.x(),0,vel.y());
    }

    public void Update()
    {
        
        // id正确
        if (Sid >= 0)
        {
            // 获取位置
            Vector2 pos = _simulator.getAgentPosition(Sid);
            // 获取速度
            Vector2 vel = _simulator.getAgentPrefVelocity(Sid);
            // 修改位置
            var oldPos = Transform.position;
            Transform.position = new Vector3(pos.x(), oldPos.y, pos.y());
            // 修改朝向
            if (Math.Abs(vel.x()) > 0.001f || Math.Abs(vel.y()) > 0.001f)
                Transform.forward = new Vector3(vel.x(), 0, vel.y()).normalized;
        }
        // 重置速度
        if (!_isMoving)
        {
            _simulator.setAgentPrefVelocity(Sid, new Vector2(0, 0));
            return;
        }

        // 移动方向
        Vector2 goalVector = _targetPos - _simulator.getAgentPosition(Sid);
        if (RVOMath.absSq(goalVector) > 1.0f)
        {
            goalVector = RVOMath.normalize(goalVector);
        }
        // 设置移动方向
        _simulator.setAgentPrefVelocity(Sid, goalVector);
    
        /* Perturb a little to avoid deadlocks due to perfect symmetry. */
        float angle = (float) m_random.NextDouble()*2.0f*(float) Math.PI;
        float dist = (float) m_random.NextDouble()*0.0001f;
        // 设置速度
        _simulator.setAgentPrefVelocity(Sid, _simulator.getAgentPrefVelocity(Sid) +
                                             dist*
                                             new Vector2((float) Math.Cos(angle), (float) Math.Sin(angle)));
    }
}