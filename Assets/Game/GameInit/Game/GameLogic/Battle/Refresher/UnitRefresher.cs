﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 游戏单位刷新器
    /// </summary>
    public class UnitRefresher<T> : Singleton<T>
    {
        // 开始
        protected bool _start;
        // id
        protected int UnitId;
        /// <summary>
        /// 需要生成的怪兽数量
        /// </summary>
        protected int NeedUnitCount;
        /// <summary>
        /// 累计刷新时间
        /// </summary>
        protected float RefreshTimeCount;
        /// <summary>
        /// 刷新间隔时间
        /// </summary>
        protected float RefreshInterval;
        /// <summary>
        /// 刷新次数
        /// </summary>
        protected int RefreshCount;
        /// <summary>
        /// 刷新时间上限
        /// </summary>
        protected float RefreshTimeUpperLimit;
        /// <summary>
        /// 经验怪兽概率列表
        /// </summary>
        protected Dictionary<int,int> UnitLevelRandom;

        /// <summary>
        /// 每秒刷新多少个
        /// </summary>
        /// <returns></returns>
        protected int RefreshCountSecond;

        private float _refreshSecond;

        // 初始化 
        public virtual void Init()
        {
            // 怪兽id
            UnitId = 0;
            // 每秒刷新多少个
            RefreshCountSecond = 20;
        }
        
        // 开始刷新
        public virtual void Start()
        {
            // 开始
            _start = true;
            // 刷新时间
            _refreshSecond = 0;
        }
        
        // 停止刷新
        public virtual void Stop()
        {
            // 停止
            _start = false;
        }
        
        public void Update(float time)
        {
            if(!_start)
                return;
            
            // 超过刷新时间退出
            if(RefreshTimeCount > RefreshTimeUpperLimit)
                return;

            // 时间累计
            RefreshTimeCount += time;
            if (RefreshTimeCount >= RefreshCount * RefreshInterval)
            {
                // 获取需要生成的怪兽数量
                NeedUnitCount +=  GetUnitCount();
                // 刷新次数加一
                RefreshCount++;
            }
            
            // 刷新倒计时
            _refreshSecond += time;
            if(NeedUnitCount == 0 || _refreshSecond < 1f)
                return;

            var count = Mathf.Min(RefreshCountSecond, NeedUnitCount);
            // 生成怪兽
            for (int i = 0; i < count; i++)
            {
                UnitId++;
                // 创建
                CreateUnit(UnitId);
            }
            // 减少数量
            NeedUnitCount -= count;
            // 重置倒计时
            _refreshSecond = 0;
        }
        
        // 初始化随机配置
        protected Dictionary<int, int> ParseUnitConfigToDic(string unitRatioConfig)
        {
            var unitRandomConfig = new Dictionary<int, int>();
            
            // 字符串转为 dic<config,ratio>
            var monsterLevelDic = GameConstantHelper.StringToIntDic(unitRatioConfig);
            
            // 累计的概率
            var valueCount = 0;
            // 等级列表
            var configs = monsterLevelDic.Keys.ToList();
            for (int i = 0; i < configs.Count; i++)
            {
                var config = configs[i];
                var value = monsterLevelDic[config];
                // 累计
                valueCount += value;
                // 加入指点
                unitRandomConfig.Add(config,valueCount);
            }

            return unitRandomConfig;
        }
        
        /// <summary>
        /// 随机
        /// </summary>
        /// <param name="unitRandomConfigs">随机数据</param>
        /// <returns></returns>
        protected int RandomUnitConfig(Dictionary<int, int> unitRandomConfigs)
        {
            var configs = unitRandomConfigs.Keys.ToList();
            var maxLevel = configs[configs.Count - 1];
            // 比例上限
            var maxRatio = unitRandomConfigs[maxLevel];
            
            // 随机值
            var randomValue = Random.Range(1, maxRatio + 1);

            for (int i = 0; i < configs.Count; i++)
            {
                var key = configs[i];
                if (randomValue <= unitRandomConfigs[key])
                {
                    return key;
                }
            }
            
            return 1;
        }
        /// <summary>
        /// 获取随机位置
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected Vector3 GetPos(int id)
        {
            // 单元格大小
            var CellSize = BattleConst.CellSize;
            // 单个开始位置
            var SrcPos = BattleConst.SrcPos;
            // 计算位置 
            var index = id % 9;
            var x = index % 3;
            var y = Mathf.Floor(index / 3f);
            var cellPos = SrcPos + new Vector3((x) * CellSize.x, 0, (y) * CellSize.z);
            var random = new Vector3((Random.Range(0, 4f)) * CellSize.x / 4, 
                0, 
                (Random.Range(0, 4f)) * CellSize.z / 4);
            var pos = cellPos + random;

            return pos;
        }

        /// <summary>
        /// 获取刷新单位数量
        /// </summary>
        /// <returns></returns>
        protected virtual int GetUnitCount()
        {
            return 0;
        }
        
        // 生成怪兽
        protected virtual void CreateUnit(int id){}
    }
}