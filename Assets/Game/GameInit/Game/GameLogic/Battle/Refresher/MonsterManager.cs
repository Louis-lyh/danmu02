﻿
 using System;
 using System.Collections.Generic;
 using System.Linq;
 using Cysharp.Threading.Tasks;
 using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;
 using Random = UnityEngine.Random;

 namespace GameInit.Game
{
    /// <summary>
    /// 刷新怪兽
    /// </summary>
    public class MonsterManager : UnitRefresher<MonsterManager>
    {
        // 随机id表
        private Dictionary<int, int> _monsterIdRandom;
        // 跟随玩家等级字典
        private Dictionary<int, int> _monsterLevelBuyPlayer;
        
        // 精英怪列表
        private List<long> _monsterListInStartBoss;
        // 消灭boss阶段怪兽回调
        private Action _callBackInStartBoss;
        
        // 怪兽守卫id
        private long _uniIdMonsterGuard;
        
        // 初始化 
        public override void Init()
        {
            // 怪兽id
            UnitId = (int)UnitType.Monster * 100000;
            // 怪兽守卫
            _uniIdMonsterGuard = (int) UnitType.MonsterGuard * 100000;
            
            // 初始化
            UnitLevelRandom = ParseUnitConfigToDic(GameConstantHelper.MonsterLevelOdds);
            // 随机id权重字典
            _monsterIdRandom = ParseUnitConfigToDic(GameConstantHelper.MonsterIdOdds);
            // 跟随玩家等级字典
            _monsterLevelBuyPlayer = ParseUnitConfigToDic(GameConstantHelper.MonsterLevelBuyPlayer);
            // 每秒刷新多少个
            RefreshCountSecond = 10;
            // 精英怪列表
            _monsterListInStartBoss = new List<long>();
        }
        // 开始刷新
        public override void Start()
        {
            // 开始
            _start = true;
            // ------ 重置属性 ------
            // 累计刷新时间
            RefreshTimeCount = 0;
            // 刷新间隔时间
            RefreshInterval = GameConstantHelper.RefreshMonsterInterval;    
            // 刷新次数
            RefreshCount = 0;
            // 刷新时间上限
            RefreshTimeUpperLimit = GameConstantHelper.RefreshMonsterUpperLimit;   
            NeedUnitCount = 0;
            
            // 开启怪兽守卫定时器
            StartMonsterGuardTimer();
        }
        // 停止刷新
        public override void Stop()
        {
            // 停止
            _start = false;
            // 移除所有怪兽
            BattleManager.Instance.RemoveAllMonster();
        }

        /// <summary>
        /// 获取刷新怪兽数量
        /// </summary>
        /// <returns></returns>
        protected override int GetUnitCount()
        {
            // 玩家数量
            var roleCount = BattleManager.Instance.GetFighterCount(UnitType.Role);
            // 怪兽数量
            var monsterCount = BattleManager.Instance.GetFighterCount(UnitType.Monster);
            
            // 需要生成的怪兽数量
            var needUnit = 0;
            // 玩家数量小于最小数量刷最低数量怪兽
            if(roleCount <= GameConstantHelper.RefreshMonsterPlayerLowerLimit)
                // 刷新怪兽最低数量
                needUnit = GameConstantHelper.MonsterLowerLimit - monsterCount;
            else
                // 怪兽数量为玩家的倍数
                needUnit = (int)(roleCount * GameConstantHelper.RefreshMonsterMultiple) - monsterCount;
            
            return needUnit;
        }
        // 生成怪兽
        protected override void CreateUnit(int id)
        {
            // 随机怪兽id
            var defId = RandomUnitConfig(_monsterIdRandom);
            // 数据
            var monsterData = new MonsterData(id);
            
            // 等级
            var level = MonsterLevel(defId);

            // 初始化数据
            if (!monsterData.InitFighter(defId,$"Unit_{id}_{defId}_{level}", level))
                return;
            
            // 开始位置
            var pos = GetPos(id);
            // 更新开始位置
            monsterData.SetStartPos(pos);
            // 生成怪兽
            BattleManager.Instance.AddFighterUnit(monsterData);
        }
        
        // 获取怪兽等级
        private int MonsterLevel(int defId)
        {
            if (_monsterLevelBuyPlayer.ContainsKey(defId))
            {
                // 玩家排名
                var playerNum = _monsterLevelBuyPlayer[defId];
                // 获取对应玩家等级
                var players = BattleManager.Instance.GetTopUnit(playerNum);
                if (players.Count >= playerNum)
                    return players[playerNum - 1].FighterData.Level;
            }
            
            // 随机等级
            return RandomUnitConfig(UnitLevelRandom);
        }

        #region 生成精英怪
        // boss召唤怪兽
        public void BossSummonMonster()
        {
            // 玩家数量
            var roleCount = BattleManager.Instance.GetFighterCount(UnitType.Role); 
            // 小狼人数量
            var smallWerewolf = (int)Mathf.Ceil(roleCount * GetLittleWerewolfMultiple());
            
            // 下线
            var limitStr = GameConstantHelper.LittleWerewolfRange.Split(',');
            var lowLimit = int.Parse(limitStr[0]);
            var upLimit = int.Parse(limitStr[1]);

            smallWerewolf = Mathf.Clamp(smallWerewolf, lowLimit, upLimit);
            
            // boss等级
            var bossLevel = BattleManager._instance.BossUnit.FighterData.Level;
            
            for (int i = 0; i < smallWerewolf; i++)
            {
                UnitId++;
                var id = UnitId;
                // 获取小狼人位置
                var pos = GetSmallWerewolfPos(id,0);
                // 创建小狼人
                TimerHeap.AddTimer(400, 0, () =>
                {
                    if (BattleManager.Instance.IsNight)
                        CreateSmallWerewolf(id,bossLevel,pos);
                });
            }
        }
        // 获取小狼人生成倍数
        private float GetLittleWerewolfMultiple()
        {
            var multipleStr = GameConstantHelper.RefreshLittleWerewolfMultiple.Split(',');
            var day = BattleManager.Instance.Day;
            if(day <= multipleStr.Length && day > 0)
                return float.Parse(multipleStr[day - 1]);

            return float.Parse(multipleStr[multipleStr.Length - 1]);
        }
       
        
        // 在开始阶段创建怪兽
        public void CreateMonsterInStartBoss(Action callBack)
        {
            // 怪兽击杀完回调
            _callBackInStartBoss = callBack;
            
            // 玩家数量
            var roleCount = BattleManager.Instance.GetFighterCount(UnitType.Role);
            var monsterMultiple = GameConstantHelper.MonsterMultipleInStartBoss;
            // 怪兽数量
            var monsterCount = (int) (roleCount * monsterMultiple);
            // 数量范围
            var range = GameConstantHelper.MonsterRangeInStartBoss.Split(',');
            monsterCount = Mathf.Clamp(monsterCount, int.Parse(range[0]), int.Parse(range[1]));
            // 等级
            var roleRanking = GameConstantHelper.MonsterLevelByPlayerInStartBoss;
            var roleList = BattleManager.Instance.GetTopUnit(roleRanking);
            var level = 1;
            if (roleList.Count > 0)
                level = roleList[roleList.Count - 1].FighterData.Level;
            
            // 创建怪兽
            while (monsterCount > 0)
            {
                var count = Mathf.Min(monsterCount, 20);
                
                TimerHeap.AddTimer(30 * (uint)(monsterCount / 20), 0, () =>
                {
                    for (int i = 0; i < count; i++)
                    {
                        UnitId++;
                        // 获取小狼人位置
                        var pos = GetSmallWerewolfPos(UnitId, 2);
                        // 创建小狼人
                        if (BattleManager.Instance.IsNight)
                            CreateSmallWerewolf(UnitId, level, pos);
                        // 加入列表
                        _monsterListInStartBoss.Add(UnitId);
                    }
                });
                
                // 减少20
                monsterCount -= 20;
            }
        }
        
        // 移除开始阶段怪兽
        public void RemoveMonsterInStartBoss(long id)
        {
            if (_monsterListInStartBoss.Contains(id))
                _monsterListInStartBoss.Remove(id);
            
            // 为零回调
            if (_monsterListInStartBoss.Count == 0)
            {
                _callBackInStartBoss?.Invoke();
                _callBackInStartBoss = null;
            }
        }

        // 获取小狼人位置
        private Vector3 GetSmallWerewolfPos(int id,int y)
        {
            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            // 计算位置 
            var index = id % 9;
            var x = index % 3;
            var cellPos = srcPos + new Vector3((x) * cellSize.x, 0, (y) * cellSize.z);
            var random = new Vector3((Random.Range(0, 4f)) * cellSize.x / 4, 
                0, 
                (Random.Range(0, 4f)) * cellSize.z / 4);
            var pos = cellPos + random;

            return pos;
        }


        // 生成小狼人
        private void CreateSmallWerewolf(int id,int level,Vector3 pos)
        {
            // 小狼人配置id
            var _defId = BattleConst.SmallWerewolfId;
            // 数据
            var smallWerewolfData = new MonsterData(id,UnitType.SmallWerewolf);
            // 初始化数据
            if (!smallWerewolfData.InitFighter(_defId,$"SmallWerewolf_"+id, level))
                return;

            // 更新开始位置
            smallWerewolfData.SetStartPos(pos);
           
            // 生成怪兽
            BattleManager.Instance.AddFighterUnit(smallWerewolfData);
            
            // 生成特效
            var effectID = smallWerewolfData.RoleDef.ComingEffect;
            var effectPos = pos;
            var effectScale = smallWerewolfData.ModelScale * Vector3.one;
            if (effectID != 0)
                Effect.CreateSceneEffect(effectID, effectPos, effectScale,Quaternion.identity);
        }
    
        #endregion

        #region 怪兽守卫

        private void CreateMonsterGuard(int needCount)
        {
            // 怪兽守卫id配置
            var idArray = BattleConst.MonsterGuardId.Split(',');
            
            for (int i = 0; i < needCount; i++)
            {
                var unitId = _uniIdMonsterGuard++;
                var id = int.Parse(idArray[i % idArray.Length]);
                // 数据
                var data = new MonsterGuardData(unitId);
                // 初始化数据
                if(!data.InitFighter(id,"MonsterGuard"))
                    return;
                
                // 更新开始位置
                data.SetStartPos(GetMonsterGuardPos(unitId));
                
                // 怪兽守卫
                BattleManager.Instance.AddFighterUnit(data);
            }
        }

        private Vector3 GetMonsterGuardPos(long index)
        {
            index = index % 4;
            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            // 计算位置 
            var y = (index / 2) * 3;
            var x = (index % 2) * 3;
            var cellPos = srcPos + new Vector3(x * cellSize.x, 0, y * cellSize.z);

            return cellPos;
        }
        // 怪兽守卫定时器
        private uint _monsterGuardTimer;
        /// <summary>
        /// 怪兽守卫定时器
        /// </summary>
        private void StartMonsterGuardTimer()
        {
             // 删除怪兽守卫定时器
            TimerHeap.DelTimer(_monsterGuardTimer);
            _monsterGuardTimer = TimerHeap.AddTimer(0, 10000, () =>
            {
                var hour = ServerTimerManager.Instance.GetHour();
                if (hour >= 20 && hour <= 22 && !BattleManager.Instance.IsPeace)
                {
                    if(BattleManager.Instance.GetFighterCount(UnitType.MonsterGuard) > 0)
                        BattleManager.Instance.RemoveAllMonsterGuard();
                }
                else
                {
                    // 当前怪兽
                    var guardCount = BattleManager.Instance.GetFighterCount(UnitType.MonsterGuard);
                    CreateMonsterGuard(4 - guardCount);
                }
            });
        }

        #endregion
        public void Dispose()
        {
            // 删除怪兽守卫定时器
            TimerHeap.DelTimer(_monsterGuardTimer);
        }
    }
}