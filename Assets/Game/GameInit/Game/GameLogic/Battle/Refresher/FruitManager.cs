﻿using System.Collections.Generic;
using DG.Tweening;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class FruitManager :  UnitRefresher<FruitManager>
    {
        // 初始化 
        public override void Init()
        {
            // 果实id
            UnitId = (int)UnitType.Fruit * 100000;
            
            // 初始化随机等级权重字典
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                UnitLevelRandom = ParseUnitConfigToDic(GameConstantHelper.RefreshFruitOdds);
            else if(BattleManager.Instance.BattleType == BattleType.FieldBoss)
                UnitLevelRandom = ParseUnitConfigToDic(GameConstantHelper.RefreshFruitOdds_FieldBoss);
            
            // 每秒刷新多少个
            RefreshCountSecond = 20;
        }
        // 开始刷新
        public override void Start()
        {
            // 开始
            _start = true;
            // ------ 重置属性 ------
            // 刷新时间
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                RefreshTimeCount = -GameConstantHelper.StartRefreshFruitTime;

            // 刷新间隔时间
            RefreshInterval = GameConstantHelper.RefreshFruitInterval;    
            // 刷新次数
            RefreshCount = 0;
            // 刷新时间上限
            RefreshTimeUpperLimit = GameConstantHelper.RefreshFruitUpperLimit;
            NeedUnitCount = 0;
        }
        // 停止刷新
        public override void Stop()
        {
            // 停止
            _start = false;
            // 移除所有水果
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                BattleManager.Instance.RemoveAllFruit();
        }
        /// <summary>
        /// 获取刷新的水果数量
        /// </summary>
        /// <returns></returns>
        protected override int GetUnitCount()
        {
            // 玩家数量
            var roleCount = BattleManager.Instance.GetFighterCount(UnitType.Role);
            // 果实数量
            var unitCount = BattleManager.Instance.GetFighterCount(UnitType.Fruit);
            // 需要生成的果实数量
            var needUnit = 0;
            // 玩家数量小于最小数量刷最低数量果实
            if(roleCount <= GameConstantHelper.RefreshFruitPlayerLowerLimit)
                // 刷新果实最低数量
                needUnit = GameConstantHelper.FruitLowerLimit - unitCount;
            else
                // 果实数量为玩家的倍数
                needUnit = (int)(roleCount * GameConstantHelper.RefreshFruitMultiple) - unitCount;
            
            // 搜集果实提示
            GameNoticeModel.Instance.ShowCollectTips("输入“<color=#EAE678FF>采集</color>”命令，抢粽子啦！");
            
            return needUnit;
        }
        // 生成果实
        protected override void CreateUnit(int id)
        {
            // 果实配置id
            var defId = BattleConst.FruitId;
            // 随机等级
            var level = RandomUnitConfig(UnitLevelRandom);
            // 数据
            var FruitData = new FruitData(id);
            // 初始化数据
            if (!FruitData.InitFighter(defId,$"Fruit_"+id, level))
                return;
            
            // 开始位置
            var pos = GetPos(id);
            // 更新开始位置
            FruitData.SetStartPos(pos);
            // 生成果实
            BattleManager.Instance.AddFighterUnit(FruitData);
        }
       
        /// <summary>
        /// 创建boss奖励糖果
        /// </summary>
        /// <param name="pos"></param>
        public void CreateCandy_BossReward(Vector3 pos,int dropRewardId,FighterUnit fighterUnit)
        {
            // 配置
            var def = DropRewardConfig.Get(dropRewardId);
            if(def == null)
                return;
            // 初始化糖果随机等级权重字典
            var candyRandom = ParseUnitConfigToDic(def.DropOdds);
            
            // 玩家数量
            var roleCount = BattleManager.Instance.GetFighterCount(UnitType.Role, true);
            // 糖果数量
            var candyCount = roleCount * def.DropMultiple;
            var rangeStr = def.DropRange.Split(',');
            var minCandy = int.Parse(rangeStr[0]);
            var maxCandy = int.Parse(rangeStr[1]);
            candyCount = Mathf.Clamp(candyCount, minCandy, maxCandy);
            
            // 糖果掉落半径范围
            var radiusStr = def.DropRadius.Split(',');
            var minRadius = float.Parse(radiusStr[0]);
            var maxRadius = float.Parse(radiusStr[1]);
            
            for (int i = 0; i < candyCount; i++)
            {
                // id
                var defId = RandomUnitConfig(candyRandom);
                // 创建糖果
                var fruitData = CreateCandy(++UnitId,defId,pos);
                // 随机半径
                var radius = Random.Range(minRadius, maxRadius);
                // 随机方向
                var dir = Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)) * Vector3.forward;
                // 掉落位置
                var comingPos = pos + dir.normalized * radius;
                // 设置出行位置
                fruitData.SetComingPos(comingPos,def.FlyingKiller ? fighterUnit : null);
                // 生成果实
                BattleManager.Instance.AddFighterUnit(fruitData);
            }
            
            // 额外飞行动画
            if(!string.IsNullOrEmpty(def.FlyingAnimation))
                FlyAnimation(pos, dropRewardId, fighterUnit);
        }
        
        /// <summary>
        /// 创建果实
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pos"></param>
        /// <returns></returns>
        private FruitData CreateCandy(int id,int defId,Vector3 pos)
        {
            // 数据
            var FruitData = new FruitData(id);
            // 初始化数据
            if (!FruitData.InitFighter(defId,$"Fruit_Candy_"+id))
                return null;

            // 更新开始位置
            FruitData.SetStartPos(pos);

            return FruitData;
        }

        #region boss击杀飞行动画
         /// <summary>
        /// 飞行动画
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="dropRewardId"></param>
        /// <param name="fighterUnit"></param>
        private async void FlyAnimation(Vector3 pos,int dropRewardId,FighterUnit fighterUnit)
        {
            if(fighterUnit == null)
                return;
            
            // 配置
            var def = DropRewardConfig.Get(dropRewardId);
            // 糖果掉落半径范围
            var radiusStr = def.DropRadius.Split(',');
            var minRadius = float.Parse(radiusStr[0]);
            var maxRadius = float.Parse(radiusStr[1]);

            int count = BattleManager.Instance.GetFighterCount(UnitType.Role);
            count = Mathf.Clamp(count, 6, 30);
            
            string name = GameResPathUtils.GetFruitPrefab(def.FlyingAnimation);;

            for (int i = 0; i < count; i++)
            {
                var item = new GameObject();
                item.name = "FlyAnimation"+i;
                var prefab = await ResourceManager.ConstructObjAsync(name);
                prefab.transform.SetParent(item.transform);
                item.transform.localScale = Vector3.one * 0.45f;
                
                // 随机半径
                var radius = Random.Range(minRadius, maxRadius);
                // 随机方向
                var dir = Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0)) * Vector3.forward;
                // 掉落位置
                var comingPos = pos + dir.normalized * radius;
                
                var dis = Vector3.Distance(pos, comingPos);
                var p1 = (pos + comingPos) / 2 + Vector3.up * dis;
                // 长度
                var length = GameConstantHelper.GetBezierLength(pos,p1,comingPos);
                // 拖尾
                Effect.CreateEffect(4019,item.transform, Vector3.zero, Vector3.one, Quaternion.identity);
                // 移动
                DOTween.To(
                        ()=>0f, 
                        (value) =>
                        {
                            var posV = GameConstantHelper.BezierCurvePos(pos, p1, comingPos, value);
                            if(posV.magnitude > 0.01f)
                                item.transform.position = posV;
                        }, 
                        1f, length * 0.3f)
                    .OnComplete(()=>FlyTarget(item.transform,fighterUnit))
                    .SetEase(Ease.Linear);
            }
        }
        
        /// <summary>
        /// 飞向目标
        /// </summary>
        private void FlyTarget(Transform item,FighterUnit fighterUnit)
        {
            if(FlyTargetDeath(fighterUnit))
                return;

            var radius = fighterUnit.Radius;
            // 移动
            uint timer = 0;
            timer = TimerHeap.AddTimer(1, 30, () =>
            {
                var targetPos = fighterUnit.Position + Vector3.up * radius;
                item.position = Vector3.MoveTowards(item.position, targetPos,1 * 0.3f);
                // 目标死亡结束 或者到达目标结束
                if (FlyTargetDeath(fighterUnit) || Vector3.Distance(item.position, targetPos) < 0.01f)
                {
                    ResourceManager.CollectObj(item.GetChild(0).gameObject);
                    GameObject.Destroy(item.gameObject);
                    TimerHeap.DelTimer(timer);
                }
            });
        }
        
        /// <summary>
        /// 飞行目标死亡
        /// </summary>
        /// <returns></returns>
        private bool FlyTargetDeath(FighterUnit fighterUnit)
        {
            if(fighterUnit == null 
               || fighterUnit.FighterData == null
               || fighterUnit.BehaviorData == null 
               || fighterUnit.BehaviorData.IsDeath)
                return true;
            
            return false;
        }

        

        #endregion
        
       
        private void Clear()
        {
            
        }

        public void Dispose()
        {
               
        }
    }
}