using GameHotfix.Framework;

namespace GameInit.Game
{
    public class BattleEvent : BaseEvent
    {
        public const string SCENE_UNIT_DEATH = "sceneUnitDeath";

        public BattleEvent(string type) 
            : base(type)
        {
        }


        public FighterUnitData AddFighterData { get; private set; }

        public static BattleEvent CreateAddFighterUnitEvent(string evtType, FighterUnitData fighterUnitData)
        {
            return new BattleEvent(evtType) { AddFighterData = fighterUnitData };
        }
        
    }
}