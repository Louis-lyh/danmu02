﻿using System.Collections.Generic;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public partial class BattleModel
    {
        // 添加多个buff
        public void AddBuff(List<int> buffs, FighterUnitData source, FighterUnitData target, int mulDuration = 1,int mulValue = 1)
        {
            if (buffs == null)
                return;
            for (var i = 0; i < buffs.Count; i++)
            {
                if(buffs[i] == 0)
                    continue;
                AddBuff(buffs[i], source, target,mulDuration,mulValue);
            }
        }
        // 添加buff
        public void AddBuff(int buffId, FighterUnitData source, FighterUnitData target,int mulDuration = 1,int mulValue = 1)
        {
            // buff配置
            var def = CopyBuffDef(BuffConfig.Get(buffId));
            if (def == null)
            {
                Logger.LogMagenta("BattleModel.AddBuff() => 添加buff失败，buff找不到配置, id:" + buffId);
                return;
            }
            
            // 随机触发
            if (Random.Range(0, 100) > def.TriggerRate)
                return;
            
            // 时间叠加
            if(def.Duration > 0)
                def.Duration *= mulDuration;
            
            // 作用值叠加
            if(def.IsAddUp == 1)
                def.Value *= mulValue;
            
            // 记录buff数据
            var buff = target.AddBuff(def);
            
            // 信息
            buff.BuffOwnerUID = target.UnitID;
            buff.BuffCreateUID = source.UnitID;
            // 加入battleManager
            BattleManager.Instance.AddBuff(target, buff,def);
        }

        private BuffDef CopyBuffDef(BuffDef buffDef)
        {
            BuffDef newBuffDef = new BuffDef();
            newBuffDef.ID = buffDef.ID;
            newBuffDef.Type = buffDef.Type;
            newBuffDef.EffectUnitType = buffDef.EffectUnitType;
            newBuffDef.AttrKey = buffDef.AttrKey;
            newBuffDef.EffectType = buffDef.EffectType;
            newBuffDef.Interval = buffDef.Interval;
            newBuffDef.TriggerRate = buffDef.TriggerRate;
            newBuffDef.Value = buffDef.Value;
            newBuffDef.ValueType = buffDef.ValueType;
            newBuffDef.IsAddUp = buffDef.IsAddUp;
            newBuffDef.AddUpLimit = buffDef.AddUpLimit;
            newBuffDef.Range = buffDef.Range;
            newBuffDef.Duration = buffDef.Duration;
            newBuffDef.Effect = buffDef.Effect;
            
            return newBuffDef;
        }
    }
    
    

}