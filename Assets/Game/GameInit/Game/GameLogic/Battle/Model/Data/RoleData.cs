using System;
using System.Linq;
using GameHotfix.Game;
using GameInit.Framework;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class RoleData : FighterUnitData
    {
        // 用户id
        public string UserId { get; private set; }
        // 工会id
        public int GuildId{ get; protected set; }
        // 用户头像
        public string HeadUrl { get; protected set; }
        // 点赞次数
        public int LikeCount { get; protected set; }
        // 击杀积分
        public long KillScore { get; protected set; }
        // 击败玩家次数
        public int KillRoleCount{ get; protected set; }
        // 连胜次数
        public int WinStreak { get; protected set; }
        // 光环经验
        public int HaloExp{ get; protected set; }
        // 宠物经验
        public int PetExp{ get; protected set; }
        // 显示文本
        public string ShowTipsText { get; private set; }
        // 显示点赞
        public bool ShowLikeTips;
        
        public RoleData(string userId, long uid, UnitType unitType = UnitType.Role)
            : base(unitType)
        {
            UserId = userId;
            UnitID = uid;
            GuildId = 0;
            // 初始化
            Init();
        }

        private void Init()
        {
            // 攻击目标
            if (BattleManager.Instance.BossUnit is FighterUnitBoss)
                _targetType = UnitType.Boss;
            else if (BattleManager.Instance.BossUnit is MonsterBoss)
                _targetType = UnitType.MonsterBoss;
            else
                _targetType = UnitType.Monster;
            
            if(BattleModel.Instance.FieldBossCount > 0)
                _targetType = UnitType.FieldBoss;
            
            // 显示
            ShowTipsText = String.Empty;
        }
        
        /// <summary>
        /// 修改UnitID
        /// </summary>
        /// <param name="uid"></param>
        public void ChangeUnitID(long uid)
        {
            UnitID = uid;
        }

        // 修改配置
        public bool ChangeDef(int level = 0)
        {
            // 角色配置
            RoleDef = RoleConfigUtils.GetRoleDef_Level(RoleDef.RoleID, level);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.InitFighter() => 找不到角色配置文件，role id:" + RoleDef.RoleID + ", level:" +
                                level + "]");
                return false;
            }
            
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }


        // 显示对话框
        public void SetDialogText(string text)    
        {
            // 字符串
            ShowTipsText = text;
        }

        // 设置攻击目标
        public override void SetTargetType(UnitType targetType)
        {

            if (BattleManager.Instance.BattleType == BattleType.Normal)
            {
                // 晚上
                if (BattleManager.Instance.IsNight)
                {
                    if (targetType == UnitType.Monster)
                    {
                        // boss 和 怪兽来回切换
                        if (_targetType == UnitType.Boss)
                            _targetType = UnitType.Monster;
                        else
                        {
                            if (BattleManager.Instance.BossUnit is FighterUnitBoss
                                && !BattleManager.Instance.BossUnit.IsDeath)
                                _targetType = UnitType.Boss;
                            else if (BattleManager.Instance.BossUnit is MonsterBoss
                                     && !BattleManager.Instance.BossUnit.IsDeath)
                                _targetType = UnitType.MonsterBoss;
                            else
                            {
                                // boss死了 切换为怪兽
                                _targetType = UnitType.Monster;
                            }
                        }
                    }
                    else
                        _targetType = targetType;
                }
                // 白天
                else
                    _targetType = targetType;
            }
            else if (BattleManager.Instance.BattleType == BattleType.FieldBoss)
            {
                if (targetType == UnitType.Monster)
                    _targetType = UnitType.FieldBoss;
                else
                    _targetType = targetType;
            }
        }


        // 设置玩家信息
        public void SetPlayerInfo(string headUrl,int guildId, int winStreak,int haloExp,int petExp)
        {
            HeadUrl = headUrl;
            GuildId = guildId;
            WinStreak = winStreak;
            HaloExp = haloExp;
            PetExp = petExp;
        }

        // 点赞次数
        private int ShowLikeBubble;

        public void Like(int count, bool openLimit = true)
        {
            // 增加点赞
            LikeCount += count;

            // 初始化显示次数
            if (LikeCount < GameConstantHelper.LikeBubbleStartValue)
                ShowLikeBubble = 0;
            
            // 通知
            if (LikeCount >= GameConstantHelper.LikeBubbleStartValue * Mathf.Pow(2, ShowLikeBubble))
            {
                GameNoticeModel.Instance.SendEdgeBubble(Name, HeadUrl,
                    "点赞数量", "" + LikeCount, EdgeBubbleType.LikeCount);
                ShowLikeBubble++;
            }


            // 判断是否超过限制 不增加经验
            if (LikeCount >= GameConstantHelper.LikeMaxLimit && openLimit)
            {
                return;
            }

            var unit = GetUnit();
            // 增加经验
            unit?.AddExp(count * GameConstantHelper.LikeExp);
            
            // 显示点赞
            ShowLikeTips = true;
        }

        protected virtual FighterUnit GetUnit()
        {
            return BattleManager.Instance.GetFighterByUID(UnitID);
        }
        
        /// <summary>
        /// 增加击杀数
        /// </summary>
        /// <param name="count"></param>
        public void AddKillRoleCount(int count = 1)
        {
            KillRoleCount += count;
        }
        /// <summary>
        /// 重置击杀数
        /// </summary>
        public void ResetKillRoleCount()
        {
            KillRoleCount = 0;
        }

        /// <summary>
        /// 增加击杀积分
        /// </summary>
        /// <param name="scoreCount">击杀积分</param>
        public void AddSkillScale(int scoreCount)
        {
            // 加入积分池
            BattleManager.Instance.AddScore(scoreCount);
            
            // 额外积分
            var additionalScore = 0f;
            if (WinStreak >= 2)
            {
                // 额外积分百分比
                var addScorePercent = Math.Min(GameConstantHelper.AdditionalScorePercentUpLimit,
                    GameConstantHelper.AdditionalScorePercent);
                
                additionalScore = scoreCount * ((WinStreak - 1) * addScorePercent/ 100);
            }

            // 累计积分
            AddScore(scoreCount + (int) additionalScore);
            // 显示积分
            KillScoreBubble();
        }
        // 增加积分
        public void AddScore(long scoreCount)
        {
            KillScore += scoreCount;
        }

        #region 光环
        // 增加光环经验
        public bool AddHaloExp(int exp)
        {
            HaloExp += exp;
            
            if (_haloDef != null)
            {
                // 升级
                if (_haloDef.ExpLimit <= HaloExp)
                {
                    _haloDef = GetHaloDef();
                    return true;
                }
            }
            else
            {
                _haloDef = GetHaloDef();
                return true;
            }

            return false;
        }
        // 光环配置
        private HaloDef _haloDef;
        public HaloDef HaloDef()
        {
            // 存在返回配置
            if (_haloDef != null)
                return _haloDef;
            
            return GetHaloDef();
        }
        private HaloDef GetHaloDef()
        {
            // 不存在
            var keys = HaloConfig.GetDefDic().Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var def = HaloConfig.Get(keys[i]);
                if (def.ExpLimit > HaloExp)
                {
                    _haloDef = def;
                    return def;
                }
            }
            
            return HaloConfig.Get(keys[keys.Count - 1]);
        }
        #endregion

        #region 宠物
        // 增加宠物经验
        public bool AddPetExp(int exp)
        {
            // 增加经验
            PetExp += exp;
            if (_petDef != null)
            {
                // 升级
                if (_petDef.ExpLimit <= PetExp)
                {
                    _petDef = GetPetDef();
                    return true;
                }
            }
            else
            {
                _petDef = GetPetDef();
                return true;
            }

            return false;
        }
        // 宠物配置
        private PetDef _petDef;
        public PetDef PetDef()
        {
            // 存在返回配置
            if (_petDef != null)
                return _petDef;
            
            return GetPetDef();
        }
        private PetDef GetPetDef()
        {
            // 不存在
            var keys = PetConfig.GetDefDic().Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var def = PetConfig.Get(keys[i]);
                if (def.ExpLimit > PetExp)
                {
                    _petDef = def;
                    return def;
                }
            }
            
            return PetConfig.Get(keys[keys.Count - 1]);
        }
        #endregion

        // 积分气泡
        public int KillScoreBubbleCount;
        private void KillScoreBubble()
        {
            // 积分小于显示最低显示积分重置次数
            if (KillScore < GameConstantHelper.ScaleBubbleStartValue)
                KillScoreBubbleCount = 0;
            
            
            // 击杀积分通知 ( 等比数列)
            if (KillScore >= GameConstantHelper.ScaleBubbleStartValue * Mathf.Pow(2, KillScoreBubbleCount))
            {
                // 显示气泡
                GameNoticeModel.Instance.SendEdgeBubble(Name, HeadUrl,
                    "击败积分", "" + KillScore, EdgeBubbleType.KillScore);
                // 次数加一
                KillScoreBubbleCount++;
            }
        }
        
        // 攻击加成
        public override float AttackAddition(float attackValue)
        {
            // 加成属性
            var addValue = 1 + GetAttributeAddition(AttributeType.Attack) / 100f;
            // 光环加成
            var haloValue = HaloDef().AttackBonus / 100f;
            // 宠物加成
            var patValue = PetDef().AttackBonus / 100f;
            
            // 光环基础攻击力
            attackValue += HaloDef().HaloAttack;
            // 宠物基础攻击力
            attackValue += PetDef().PetAttack;
            
            attackValue *= (addValue + haloValue + patValue);
            
            return attackValue;
        }
        
        // 速度加成
        protected override float SpeedAddition(float speedValue)
        {
            var addRatio = 1f;
            // buff加成
            if (AdditionAttribute.ContainsKey(AttributeType.MoveSpeed))
                addRatio += AdditionAttribute[AttributeType.MoveSpeed] / 100f;
            // 光环加成
            var haloValue = HaloDef().SpeedBonus / 100f;
            // 宠物加成
            var petValue = PetDef().SpeedBonus / 100f;
            
            // 光环基础速度
            speedValue += HaloDef().HaloSpeed;
            // 宠物基础速度
            speedValue += PetDef().PetSpeed;
            
            // 速度加成
            speedValue *= (addRatio + haloValue + petValue);
            return speedValue;
        }
        
        // 生命值加成
        public override int HpAddition(int hpValue)
        {
            var hp = (float) hpValue;
            var addRatio = 1f;
            // 光环加成
            var haloValue = HaloDef().HpBonus / 100f;
            // 宠物加成
            var petValue = PetDef().HpBonus / 100f;
            
            // 光环基础速度
            hp += HaloDef().HaloHp;
            // 宠物基础速度
            hp += PetDef().PetHp;
            
            // 速度加成
            hp *= (addRatio + haloValue + petValue);
            return (int) hp;
        }


        public void Copy(RoleData roleData)
        {
            // 头像
            HeadUrl = roleData.HeadUrl;
            // 点赞次数
            LikeCount = roleData.LikeCount;
            // 击杀数
            KillRoleCount = roleData.KillRoleCount;
            // 击杀积分
            KillScore = roleData.KillScore;
            // 连胜次数
            WinStreak = roleData.WinStreak;
            // 显示气泡次数
            KillScoreBubbleCount = roleData.KillScoreBubbleCount;
            // 经验
            Exp = roleData.Exp;
            // 光环经验
            HaloExp = roleData.HaloExp;
            // 宠物经验
            PetExp = roleData.PetExp;
        }
    }
}