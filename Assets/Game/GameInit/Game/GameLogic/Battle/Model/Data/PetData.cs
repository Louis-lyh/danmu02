﻿using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 宠物数据
    /// </summary>
    public class PetData : FighterUnitData
    {
        // 主人
        public FighterUnit _owner;
        
        public PetData(long id,FighterUnit owner,UnitType unitType = UnitType.Pet) 
            : base(unitType)
        {
            UnitID = id;
            _owner = owner;
        }
        public bool InitFighter(int roleId, string name)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfig.Get(roleId);
            if (RoleDef == null)
            {
                Logger.LogError("[InitFighter() => 找不到角色配置文件，role id:" + roleId + ", Level:" +Level + "]");
                return false;
            }

            Exp = RoleDef.ExpMax - 1;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            // 修改尺寸
            ModelScale *= _owner.FighterData.ModelScale;
            return true;
        }
        // 升级配置
        public  bool UpgradeData(int defId)
        {
            // 玩家配置
            RoleDef = RoleConfig.Get(defId);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.RefreshData() => 找不到角色配置文件，role id:" + RoleDef.RoleID + ", exp:" + Exp + "]");
                return false;
            }

            // 初始化属性
            InitAttribute(true);
            // 初始化技能
            InitSkill();
            // 修改尺寸
            ModelScale *= _owner.FighterData.ModelScale;
            return true;
        }
        
        // 速度加成
        protected override float SpeedAddition(float speedValue)
        {
            return _owner.FighterData.GetMoveSpeed();
        }
    }
}