﻿using System;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class BossData : RoleData
    {

        public BossData(string userId, long uid, UnitType unitType = UnitType.Boss)
            : base(userId, uid, unitType)
        {
        }

        public override bool InitFighter(int roleId, string name, int level = 0)
        {
            Name = name;
            // 玩家配置
            RoleDef = RoleConfigUtils.GetRoleDef_Level(roleId, level);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.InitFighter() => 找不到角色配置文件，role id:" + roleId + ", level:" +
                                level + "]");
                return false;
            }

            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            // 攻击目标
            _targetType = UnitType.Role;
            
            // 修改尺寸
            ModelScale *= GameConstantHelper.PlayerBossScale;
            return true;
        }
        
        // 刷新配置
        public override bool UpgradeData(int roleId,int exp)
        {
            // 玩家配置
            RoleDef = RoleConfigUtils.GetRoleDef(roleId, exp);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.RefreshData() => 找不到角色配置文件，role id:" + RoleDef.RoleID + ", exp:" +
                                Exp + "]");
                return false;
            }

            // 升级属性
            UpgradeBossAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        
        // 升级属性
        private void UpgradeBossAttribute()
        {
         
            // 初始化生命值
            CurrentHp +=(int)((GameConstantHelper.BossHpRecoveryRatio / 100f) * MaxHp);
            CurrentHp = Math.Min(CurrentHp,MaxHp);    
            
            Level = (int) RoleDef.Level; // 等级
            LastExpMax = ExpMax;     // 上一级经验
            ExpMax = RoleDef.ExpMax; // 最大经验
            ModelScale = RoleDef.ModelScale * GameConstantHelper.PlayerBossScale;// 角色缩放
            ModelRes = RoleResConfig.Get(RoleDef.ResId).ResModel;// 模型文件名
            SearchRange = RoleDef.SearchRange;// 搜索范围
            UnitActionType = (UnitAnimationType) RoleDef.ResModelType;// 动画类型
            ExpReward = RoleDef.ExpReward; // 经验奖励
            RVOLayer = (RVOLayer) RoleDef.RVOLayer;// 寻路层级
        }
        
        // 获取攻击范围
        public override float GetAttackRange(float percentage = 1)
        {
            // 攻击距离加半径
            return _defaultSkillDef.AttackDistance * percentage + ModelScale;
        }
        
        protected override FighterUnit GetUnit()
        {
            var unit = BattleManager.Instance.BossUnit as FighterUnitBoss;
            return unit;
        }
    }
}