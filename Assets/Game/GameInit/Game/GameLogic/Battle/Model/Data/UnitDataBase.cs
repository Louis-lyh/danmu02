using GameHotfix.Framework;

namespace GameInit.Game
{
    public abstract class UnitDataBase : EventDispatcher
    {
        public UnitType UnitType { get; protected set; }
        public string ModelRes { get; protected set; }
        
        public long UnitID { get; protected set; }
        public int UnitConfigID { get; protected set; }
    }
}