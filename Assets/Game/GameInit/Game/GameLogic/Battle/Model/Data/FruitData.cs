﻿using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class FruitData : FighterUnitData
    {
        // 出现目标位置
        public Vector3 ComingPos { get; private set; }
        // 是否有出现动画
        public bool IsComing { get; private set; }
        // 飞向的目标
        public FighterUnit FlyTarget { get; private set; }
        public FruitData(long id,UnitType unitType = UnitType.Fruit)
            : base(unitType)
        {
            UnitID = id;
            _targetType = UnitType.Role;
        }
        
        public virtual bool InitFighter(int roleId, string name, int Level = 1)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfigUtils.GetRoleDef_Level(roleId, Level);
            if (RoleDef == null)
            {
                Logger.LogError("[InitFighter() => 找不到角色配置文件，role id:" + roleId + ", Level:" +Level + "]");
                return false;
            }

            Exp = RoleDef.ExpMax - 1;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        public virtual bool InitFighter(int defId, string name)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfig.Get(defId);
            if (RoleDef == null)
            {
                Logger.LogError("[InitFighter() => 找不到角色配置文件，defId:" + defId + ", Level:" +Level + "]");
                return false;
            }

            Exp = RoleDef.ExpMax - 1;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        // 设置出现位置
        public void SetComingPos(Vector3 pos,FighterUnit flyTarget)
        {
            ComingPos = pos;
            IsComing = true;
            FlyTarget = flyTarget;
        }

    }
}