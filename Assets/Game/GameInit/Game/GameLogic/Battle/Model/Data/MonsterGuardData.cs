﻿using GameInit.Framework;

namespace GameInit.Game
{
    public class MonsterGuardData: FighterUnitData
    {
        public MonsterGuardData(long unitID,UnitType unitType = UnitType.MonsterGuard)
            : base(unitType)
        {
            UnitID = unitID;
            _targetType = UnitType.Role;
        }
        
        public virtual bool InitFighter(int roleId, string name, int Level = 1)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfig.Get(roleId);

            if (RoleDef == null)
            {
                Logger.LogError("[InitFighter() => 找不到角色配置文件，role id:" + roleId + ", Level:" +Level + "]");
                return false;
            }

            Exp = RoleDef.ExpMax - 1;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        
    }
}