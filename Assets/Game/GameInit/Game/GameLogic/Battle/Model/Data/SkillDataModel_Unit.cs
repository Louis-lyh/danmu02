﻿namespace GameInit.Game
{
    public partial class SkillDataModel
    {
        // 创建技能
        public SkillData CreateSkillData(int skillId,int index = 0)
        {
            if (!_skillData.TryGetValue(skillId, out var data))
            {
                data = new SkillData(skillId,1,index);
                _skillData.Add(skillId, data);
            }

            return data;
        }
        
        // 移除技能
        public void RemoveSkillData(int skillId)
        {
            if (!_skillData.ContainsKey(skillId))
                return;
            _skillData.Remove(skillId);
        }
    }
}