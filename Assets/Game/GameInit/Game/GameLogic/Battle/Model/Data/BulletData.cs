﻿using UnityEngine;

namespace GameInit.Game
{
    public class BulletData : UnitDataBase
    {
        // 子弹位置
        public Vector3 Pos { get; private set; }
        // 目标位置
        public Vector3 TargetPos { get; private set; }
        // 子弹所属者
        public FighterUnit Attack { get; private set; }
        // 子弹追踪者
        public FighterUnit Target { get; private set; }
        // 子弹资源
        public string BulletRes { get; private set; }
        // 击中特效
        public string TargetEffect { get; private set; }
        // 子弹速度
        public float BulletMoveSpeed { get; private set; }
        // 子弹伤害类型
        public SkillDamageType BulletDamageType { get; private set; }
        // 伤害半径
        public float DamageRadius{ get; private set; }
        // 技能数据
        public SkillData SkillData { get; private set; }
        
        public BulletData(SkillData skillData, long uid)
        {
            SkillData = skillData;
            UnitID = uid;
            UnitType = UnitType.Bullet;
        }
        
        public void SetData(FighterUnit owner, Vector3 startPos,Vector3 endPos, FighterUnit target = null)
        {
            // 子弹拥有者
            Attack = owner;
            // 目标对象
            Target = target;
            // 目标位置
            TargetPos = endPos;
            // 攻击对象身上的某个位置
            if (target != null)
            {
                var scale = target.FighterData.ModelScale;
                var dir = BulletBase.GetBulletHitPos(SkillData.SkillDef.BulletHitPosType);
                TargetPos +=  dir* scale;
            }
            // 开始位置
            Pos = startPos;
            ParseConfig();
        }
        // 读取配置
        private void ParseConfig()
        {
            BulletRes = SkillData.SkillDef.BulletRes;
            TargetEffect = SkillData.SkillDef.TargetEffect;
            BulletMoveSpeed = SkillData.SkillDef.BulletSpeed;
            ModelRes = SkillData.SkillDef.BulletRes;
            BulletDamageType = (SkillDamageType)SkillData.SkillDef.DamageType;
            DamageRadius = SkillData.SkillDef.DamageRadius;
        }
    }
}