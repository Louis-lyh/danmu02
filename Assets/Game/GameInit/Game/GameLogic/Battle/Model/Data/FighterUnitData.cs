using System.Collections.Generic;
using System.Linq;
using GameHotfix.Game;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public abstract class FighterUnitData : UnitDataBase
    {
        #region 属性
        public int Level { get; protected set; }
        public string Name { get; protected set; }
        public int CurrentHp { get; protected set; }
        public int MaxHp { get; private set; }
        public int Exp { get; protected set; }
        public int ExpMax { get; protected set; }
        public int LastExpMax{ get; protected set; }
        public Vector3 StartPos { get; private set; }
        public float ModelScale { get; protected set; }
        public float SearchRange { get; protected set; }
        public int ExpReward { get; protected set; }
        public float NameHeight { get; protected set; }
        public RVOLayer RVOLayer { get; protected set; }
        #endregion
        // 角色配置
        public RoleDef RoleDef { get; protected set; }

        // 默认攻击技能
        protected SkillDef _defaultSkillDef;

        // 目标类型
        protected UnitType _targetType;

        // 战斗单位类型
        public UnitType TargetType => _targetType;

        public UnitAnimationType UnitActionType { get; protected set; }
        
        // 技能配置
        private Dictionary<int, int> _skillConfig;
        
        // 技能
        private List<SkillData> _skillDatas;

        // 无敌状态
        private bool _invincible;

        public bool Invincible
        {
            get => _invincible;
            set => _invincible = value;
        }

        protected FighterUnitData(UnitType unitType)
        {
            UnitType = unitType;
        }

        public virtual bool InitFighter(int roleId, string name, int defaultExp = 0)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfigUtils.GetRoleDef(roleId, defaultExp);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.InitFighter() => 找不到角色配置文件，role id:" + roleId + ", exp:" +
                                defaultExp + "]");
                return false;
            }

            Exp = defaultExp;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        // 升级配置
        public virtual bool UpgradeData(int roleId,int exp)
        {
            // 玩家配置
            RoleDef = RoleConfigUtils.GetRoleDef(roleId, exp);

            if (RoleDef == null)
            {
                Logger.LogError("[FighterUnitData.RefreshData() => 找不到角色配置文件，role id:" + RoleDef.RoleID + ", exp:" + Exp + "]");
                return false;
            }

            // 初始化属性
            InitAttribute(true);
            // 初始化技能
            InitSkill();
            return true;
        }
        
        // 初始化属性
        protected void InitAttribute(bool isUpgrade = false)
        {
            // 加成后的最大生命值
            var upgradeMaxHp = HpAddition(RoleDef.Hp);
            
            // 升级回血
            if (isUpgrade)
            {
                // hp比例
                var hpRatio = (CurrentHp / (float)MaxHp);
                // 额外回血比例
                var exportRatio = (GameConstantHelper.RestoreMaxHpRatio / 100f);
                // 升级回血
                var hp = (hpRatio + exportRatio) * upgradeMaxHp;
                // 限制范围
                hp = Mathf.Min(hp, upgradeMaxHp);

                CurrentHp = (int)hp;
            }
            // 初始化生命值
            else
                CurrentHp = upgradeMaxHp;
            
            MaxHp = upgradeMaxHp; // 最大生命值
            Level = (int) RoleDef.Level; // 等级
            LastExpMax = RoleDef.LastExpMax;    // 上一级经验
            ExpMax = RoleDef.ExpMax; // 最大经验
            ModelScale = RoleDef.ModelScale;// 角色缩放
            ModelRes = RoleResConfig.Get(RoleDef.ResId).ResModel;// 模型文件名
            SearchRange = RoleDef.SearchRange;// 搜索范围
            UnitActionType = (UnitAnimationType) RoleDef.ResModelType;// 动画类型
            ExpReward = RoleDef.ExpReward; // 经验奖励
            NameHeight = RoleDef.NameHeight; 
            RVOLayer = (RVOLayer) RoleDef.RVOLayer;// 寻路层级
        }
        // 初始化技能
        protected void InitSkill()
        {
            // 技能配置
            _skillConfig = GameConstantHelper.StringToIntDic(RoleDef.SkillConfig);
            
            // 初始化技能列表
            if (_skillDatas == null)
                _skillDatas = new List<SkillData>();

            var keyList = _skillConfig.Keys.ToList();
            // 记录技能
            for (int i = 0; i < keyList.Count || i < _skillDatas.Count; i++)
            {
                if(i < keyList.Count)
                {
                    var skillId = keyList[i];
                    var aniId = _skillConfig[skillId];
                    // 默认技能
                    if (i == 0)
                        _defaultSkillDef = SkillConfig.Get(skillId);
                    // 添加技能
                    AddSkillList(skillId,aniId, i);
                }
                // 移除旧技能
                else if(i < _skillDatas.Count)
                    RemoveSkill(i);
            }
        }
        // 加入技能列表
        private void AddSkillList(int skillId,int aniId,int index)
        {
            // 不存在
            if (_skillDatas.Find(item => item.ID == skillId) == null)
            {
                // 移除旧技能
                RemoveSkill(index);
                // 加入列表
                SkillData skillData = new SkillData(skillId,aniId, index);
                _skillDatas.Add(skillData);
            }
        }
        // 移除技能
        private void RemoveSkill(int index)
        {
            var item = _skillDatas.Find(item => item.SkillIndex == index);
            // 存在移除
            if (item != null)
                _skillDatas.Remove(item);
        }

        // 更新
        public void Update(float deltaTime)
        {
            // 更新技能数据
            for (int i = 0; i < _skillDatas.Count; i++)
            {
                _skillDatas[i].Update(deltaTime);
            }
        }

        #region Get

        // 获取主动技能
        public SkillData GetActiveSkill()
        {
            for (int i = _skillDatas.Count - 1; i >= 0; i--)
            {
                var skillData = _skillDatas[i];
                if (skillData.IsReady && skillData.SkillDef.CastType == 1)
                    return skillData;
            }

            return null;
        }

        // 获取技能
        public SkillData GetSkill(int index)
        {
            for (int i = 0; i < _skillDatas.Count; i++)
            {
                if (_skillDatas[i].SkillIndex == index)
                    return _skillDatas[i];
            }
            
            return null;
        }
        // 获取技能
        public SkillData GetSkill_SKillId(int skillId)
        {
            for (int i = 0; i < _skillDatas.Count; i++)
            {
                if (_skillDatas[i].ID == skillId)
                    return _skillDatas[i];
            }
            
            return null;
        }

        // 获取被动技能
        public SkillData GetPassiveSKill()
        {
            for (int i = _skillDatas.Count - 1; i >= 0; i--)
            {
                var skillData = _skillDatas[i];
                if (skillData.IsReady && skillData.SkillDef.CastType == 0)
                    return skillData;
            }

            return null;
        }

        // 主动技能是否准备好
        public bool IsActiveSkillReady()
        {
            return GetActiveSkill() != null;
        }
        // 被动技能是否准备好
        public bool IsPassiveSkillReady()
        {
            return GetPassiveSKill() != null;
        }

        // 获取攻击范围
        public virtual float GetAttackRange(float percentage = 1)
        {
            // 攻击距离加半径
            return _defaultSkillDef.AttackDistance *(ModelScale / 2) * percentage + ModelScale / 2;
        }
        
        // 攻击加成
        public virtual float AttackAddition(float attackValue)
        {
            // 加成属性
            var addValue = 1 + GetAttributeAddition(AttributeType.Attack) / 100;
            attackValue *= addValue;
            return attackValue;
        }
        // 速度加成
        protected virtual float SpeedAddition(float speedValue)
        {
            // 速度加成
            var addRatio = 1f;
            if (AdditionAttribute.ContainsKey(AttributeType.MoveSpeed))
                addRatio += AdditionAttribute[AttributeType.MoveSpeed] / 100f;
            
            // 加成后的速度
            speedValue *= addRatio;
            
            return speedValue;
        }
        // 生命值加成
        public virtual int HpAddition(int hpValue)
        {
            return hpValue;
        }

        // 攻击伤害
        public DamageInfo CalcAttackDamage(SkillDef skillDef,int targetMaxHp)
        {
            // 伤害信息
            DamageInfo info = new DamageInfo();

            // ------ 技能伤害 ------
            var basisDamage = skillDef.DamageValue;
            
            // -------攻击力比例伤害 ------
            // 加成后的攻击力
            var attack = AttackAddition(RoleDef.Attack);
            // 攻击力比例伤害
            var ratio = skillDef.DamageRatio / 10000f;
            var damageRatio = attack * ratio;
            
            // ------技能生命值百分比伤害-------
            var damageHpRatio = skillDef.DamageHpRatio / 10000f;
            var damageHp = damageHpRatio * targetMaxHp;
            
            // ------ 所有伤害和 ------
            var damage = damageRatio + basisDamage + damageHp;
            
            // ------ 伤害暴击 ------
            // 暴击率
            var criticalRate = RoleDef.CriticalRate;
            // 暴击伤害
            var criticalDamage = RoleDef.CriticalDamage / 100f;
            var randomCriticalRate = Random.Range(0, 100);
            // 暴击
            if (randomCriticalRate < criticalRate)
            {
                damage *= criticalDamage;
                info.IsCritical = true;
            }
            
            // ----- 最终伤害
            info.Value = (int)damage;
            return info;
        }
        // 获取速度
        public float GetMoveSpeed()
        {
            // 加成后的速度
            var speed = SpeedAddition(RoleDef.MoveSpeed);
            return speed;
        }

        #endregion

        #region Set
        // 设置初始Hp
        public void SetHp(int maxHp,int curHp = -1)
        {
            MaxHp = maxHp;
            
            if (curHp == -1)
                CurrentHp = maxHp;
            else
                CurrentHp = curHp;
        }
        // 增加经验
        public bool AddExp(int exp)
        {
            Exp += exp;
            return ExpMax < Exp;
        }
        // 开始位置
        public void SetStartPos(Vector3 pos)
        {
            StartPos = pos;
        }
        // 修改目标类型
        public virtual void SetTargetType(UnitType targetType)
        {
            _targetType = targetType;
        }
        // 扣血
        public void DecCurHp(DamageInfo damage, GameObject obj)
        {
            float d = UnityEngine.Random.Range(0.9f, 1.1f) * damage.Value;
            damage.Value = (int)d;
            
            // 护盾
            var shieldValue = GetAttributeAddition(AttributeType.Shield);
            if (shieldValue > 0)
            {
                // 剩余伤害
                var residue = damage.Value - (int)shieldValue;
                // 更新护盾值
                AddAttributeAddition(AttributeType.Shield, -residue);
                if (this is RoleData)
                    Logger.LogBlue($"DecCurHp {damage} - {shieldValue}");
                if(residue < 0)
                    return;
                
                // 减去剩余伤害
                CurrentHp -= residue;
            }
            else
            {
                CurrentHp -= damage.Value;
            }

            if (CurrentHp <= 0)
                CurrentHp = 0;
            
            // 伤害数值
            if (UnitType == UnitType.Boss)
            {
                Vector3 cP = Vector3.up * RoleDef.HurtNumberH;
                float x = UnityEngine.Random.Range(-50f, 50f);
                float y = UnityEngine.Random.Range(0f, 30f);
                Vector3 textPos = cP + new Vector3(x, y, 0f);

                float initScale = RoleDef.HurtNumberS;
                float riseDis = 15f;
                
                BattleInfoManager.Instance.ProduceHurtNumber(damage.IsCritical, damage.Value, obj.transform.position, textPos, initScale, riseDis);
            }
            else if (UnitType == UnitType.Role || UnitType == UnitType.Monster || UnitType == UnitType.SmallWerewolf)
            {
                Vector3 cP = Vector3.up * RoleDef.HurtNumberH;
                float x = UnityEngine.Random.Range(-25f, 25f);
                float y = UnityEngine.Random.Range(0f, 15f);
                Vector3 textPos = cP + new Vector3(x, y, 0f);
                
                float initScale = RoleDef.HurtNumberS;
                float riseDis = 15f;
                
                BattleInfoManager.Instance.ProduceHurtNumber(damage.IsCritical, damage.Value, obj.transform.position, textPos, initScale, riseDis);
            }
        }
        
        // 回血
        public void RestoreHp(int value,float percent)
        {
            // 回血值
            var restoreHp = (int)(value + (percent / 100) * MaxHp);
            CurrentHp +=  restoreHp;
            // 限制
            CurrentHp = Mathf.Min(CurrentHp, MaxHp);
        }

        // 设置技能攻速
        public void SetSkillSpeed(int skillIndex,float skillSpeed)
        {
            _skillDatas[skillIndex].SetSpeed(skillSpeed);
            Logger.LogGreen($"SetSkillSpeed {skillSpeed}");
        }

        #endregion
        #region buff 相关数据
        // buff数据字典
        protected readonly Dictionary<int, BuffData> BuffDataDic = new Dictionary<int, BuffData>();
        // 判断是否存在buff
        public bool CheckHasFetchBuffs(IList<int> fetchBuffs)
        {
            if (fetchBuffs == null || BuffDataDic.Count == 0)
                return false;
            for (var i = 0; i < fetchBuffs.Count; i++)
            {
                if (BuffDataDic.ContainsKey(fetchBuffs[i]))
                    return true;
            }

            return false;
        }
        // 添加buff数据
        public BuffData AddBuff(BuffDef cfg)
        {
            // 存在返回
            if (BuffDataDic.TryGetValue(cfg.ID, out var buffData))
            {
                return buffData;
            }
            // 新实例化
            buffData = BuffData.ConstructBuffData(cfg);
            BuffDataDic.Add(cfg.ID, buffData);
            return buffData;
        }
        // 移除buff
        public void RemoveBuff(int buffId)
        {
            if (!BuffDataDic.ContainsKey(buffId))
                return;
            BuffDataDic.Remove(buffId);
        }
        #endregion
        #region 加成属性
        // 加成属性字典
        protected readonly Dictionary<int, float> AdditionAttribute = new Dictionary<int, float>();
        // 添加加成属性
        public void AddAttributeAddition(int attributeKey, float value,bool isAddUp = false,int addUpLimit = 0)
        {
            if (!AdditionAttribute.ContainsKey(attributeKey))
            {
                AdditionAttribute.Add(attributeKey,value);
            }
            else
            {
                // 累计
                if (isAddUp)
                {
                    AdditionAttribute[attributeKey] += value;
                    // 限制范围 (-1 为没有上限)
                    if (AdditionAttribute[attributeKey] > addUpLimit && addUpLimit != -1)
                        AdditionAttribute[attributeKey] = addUpLimit;
                }
                else
                {
                    AdditionAttribute[attributeKey] = value;
                }
            }
        }
        // 移除
        public void RemoveAttributeAddition(int attributeKey)
        {
            // 重置
            AdditionAttribute[attributeKey] = 0;
        }

        // 获取加成属性
        public float GetAttributeAddition(int attributeKey)
        {
            return AdditionAttribute.ContainsKey(attributeKey) ? AdditionAttribute[attributeKey] : 0f;
        }
        // 属性更改
        public virtual void OnAttributeChange()
        {
            // 
            if(AdditionAttribute.TryGetValue(AttributeType.AttackSpeed,out float value))
            {
                SetSkillSpeed(0,value / 100f);
            }
        }
        #endregion
    }
}