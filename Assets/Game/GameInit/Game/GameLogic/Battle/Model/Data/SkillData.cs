using GameInit.Game;

namespace GameInit.Game
{
    public class SkillData
    {
        private readonly int _skillId;    // 技能id
        public int ID => _skillId;
        public int SkillIndex { get; private set; } // 技能索引
        public SkillDef SkillDef { get; private set; } // 技能配置
        public SkillType SkillType{ get; private set; }    // 技能类型
        public bool IsReady{ get; private set; }// 是否准备好了
        // 动画名称
        public int ActionId{ get; private set; }
        
        // 技能冷却时间
        private float _skillCd;
        public float CurSkillCd =>_skillCd;
        // 攻击速度
        private float _skillSpeed;
        public SkillData(int skillId,int aniId,int index)
        {
            _skillId = skillId;
            SkillDef = SkillConfig.Get(_skillId);
            SkillType = (SkillType) SkillDef.SkillType;
            // 攻击速度
            _skillSpeed = 1f;
            StartCd();// 启动cd
            // 技能索引
            SkillIndex = index;
            // 动画id
            ActionId = aniId;

        }
        
        public void Update(float deltaTime)
        {
            // cd时间
            _skillCd -= deltaTime;
            if (_skillCd <= 0)
                IsReady = true;
            else
                IsReady = false;
        }
        
        // 启动cd
        public void StartCd()
        {
            _skillCd = SkillDef.SkillCD / _skillSpeed;
            IsReady = false;
        }
        // 清除cd
        public void ClearCd()
        {
            _skillCd = 0;
            IsReady = true;
        }
        // 设置攻速
        public void SetSpeed(float skillSpeed)
        {
            _skillSpeed = 1 + skillSpeed;
        }
    }

}


