using GameInit.Framework;

namespace GameInit.Game
{
    public class MonsterData : FighterUnitData
    {
        public MonsterData(long id,UnitType unitType = UnitType.Monster)
            : base(unitType)
        {
            UnitID = id;
            _targetType = UnitType.Role;
        }
        
        public virtual bool InitFighter(int roleId, string name, int Level = 1)
        {
            Name = name;
            // 角色配置
            RoleDef = RoleConfigUtils.GetRoleDef_Level(roleId, Level);

            if (RoleDef == null)
            {
                Logger.LogError("[InitFighter() => 找不到角色配置文件，role id:" + roleId + ", Level:" +Level + "]");
                return false;
            }

            Exp = RoleDef.ExpMax - 1;
            // 初始化属性
            InitAttribute();
            // 初始化技能
            InitSkill();
            return true;
        }
        
    }
}