﻿using System.Collections.Generic;

namespace GameInit.Game
{
    public partial class BattleModel
    {
        // 玩家数据
        private Dictionary<string, ServerData.LacEventExecute> _playerExecuteData;
        
        /// <summary>
        /// 初始化
        /// </summary>
        private void InitData()
        {
            _playerExecuteData = new Dictionary<string, ServerData.LacEventExecute>();
        }
        
        /// <summary>
        /// 添加玩家执行数据
        /// </summary>
        /// <param name="data"></param>
        public void AddPlayerExecuteData(ServerData.LacEventExecute data)
        {
            if (_playerExecuteData.ContainsKey(data.userId))
                _playerExecuteData[data.userId] = data;
            else 
                _playerExecuteData.Add(data.userId,data);
        }
        
        /// <summary>
        /// 获取玩家执行数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ServerData.LacEventExecute GetPlayerExecuteData(string userId)
        {
            if (_playerExecuteData.ContainsKey(userId))
                return _playerExecuteData[userId];
            else
                return null;
        }

        private void DisposeData()
        {
            _playerExecuteData.Clear();
        }

    }
}