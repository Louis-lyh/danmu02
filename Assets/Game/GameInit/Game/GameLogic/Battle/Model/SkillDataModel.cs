using System.Collections.Generic;
using GameHotfix.Framework;

namespace GameInit.Game
{
    public partial class SkillDataModel : DataModelBase<SkillDataModel>
    {
        // 技能数据字典
        private Dictionary<int, SkillData> _skillData;
        
        public void Init()
        {
            _skillData = new Dictionary<int, SkillData>();
        }

        public void DisposeModel()
        {
            _skillData?.Clear();
        }
    }
    // 技能类型
    public enum SkillType
    {
        // 近距离
        DefaultCloseRange = 1,
        // 远距离
        DefaultRemote = 2,
        // 加属性
        AddAttribute = 3,
        // 扇形
        FanShaped = 4,
        // 圆形
        Circular = 5,
        // 召唤技能
        Call = 6,
        // 狼人声波技能
        WerewolfSonicSkill = 7,
        // 落雷
        Thunderbolt = 8,
    }
    // 技能释放类型
    public enum CastType
    {
        // 被动
        Passive,
        // 主动
        Initiative,
    }
    // 伤害计算类型
    public enum DamageCalcType
    {
        // 数值
        Number,
        // 百分比
        Percentage,
    }
    // 伤害类型
    public enum DamageType
    {
        // 单体
        Single,
        // 范围
        AOE,
    }

}

