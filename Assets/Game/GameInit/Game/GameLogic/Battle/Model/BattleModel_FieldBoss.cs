﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public partial class BattleModel
    {
        #region 野外boss
        // bossId
        private int _fieldBossId;
        // boss序号
        private int _fieldBossIndex;
        // 野外boss
        private Dictionary<long, FieldBossData> _fieldBossDatas;
        public int FieldBossCount => _fieldBossDatas.Count;
        // 占位字典
        private long[][] _fieldBossPosArray;

        private void InitFieldBoss()
        {
            _fieldBossDatas = new Dictionary<long, FieldBossData>();
            _fieldBossPosArray = new long[2][];
            _fieldBossPosArray[0] = new long[]{0,0,0};
            _fieldBossPosArray[1] = new long[]{0,0,0};
        }

        // boss阶段
        public void CallFieldBoss(int fieldBossIndex)
        {
            // 6只野外boss不能在创建了
            if(_fieldBossDatas.Count >= 6)
                return;
            
            _fieldBossIndex = fieldBossIndex;
            // 拿到id
            var idStr = GameConstantHelper.FieldBossIds.Split(',');
            _fieldBossId = int.Parse(idStr[fieldBossIndex]);
            
            // 当前id的boss数量
            var fieldBossList = _fieldBossDatas.Keys.ToList();
            var count = fieldBossList.FindAll((item) => _fieldBossDatas[item].RoleDef.RoleID == _fieldBossId).Count;
            // 同类型数量不能超过三个
            if(count >= 3)
                return;

            // 第几名
            var topNum = GetBossTopNum(GameConstantHelper.MonsterBossLevelByPlayer);
            // 前几名数量
            var unitFirst = BattleManager.Instance.GetTopUnit(topNum);
            // log
            Logger.LogBlue($"第几名 = topNum = {topNum} unitFirst.Count = {unitFirst.Count}");
            var level = 1;
            if (unitFirst.Count > 0)
                level = unitFirst[unitFirst.Count - 1].FighterData.Level;
            
            // bossId
            _fieldBossId += level;
            var roleDef = RoleConfig.Get(_fieldBossId);
            // 打开boss来临界面
            //BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventOpenBossTips,roleDef);
            // 创建怪兽boss
            CreateFieldBoss();
        }
        
        // 创建怪兽boss
        private void CreateFieldBoss()
        {
            // 初始化数据
            var bossData = new FieldBossData(GenerateUnitID());
            // 初始化数据
            if (!bossData.InitFighter(_fieldBossId, bossData.Name))
                return;
            
            // 加入字典
            _fieldBossDatas.Add(bossData.UnitID,bossData);
            
            // 位置坐标
            var posIndex = 0;
            var posArray = _fieldBossPosArray[_fieldBossIndex];
            for (int i = 0; i < posArray.Length; i++)
            {
                if (posArray[i] == 0)
                {
                    posArray[i] = bossData.UnitID;
                    if(i == 0)
                        posIndex = 1;
                    else if (i == 1)
                        posIndex = 0;
                    else
                        posIndex = i;
                    break;
                }
            }

            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            var bossPos = srcPos;
            switch (_fieldBossIndex)
            {
                case 0:
                    bossPos += new Vector3((posIndex * 1.5f) * cellSize.x, 0,0);
                    break;
                case 1:
                    bossPos += new Vector3((posIndex * 1.5f) * cellSize.x, 0, 2.5f * cellSize.z);    
                    break;
            }
            // 更新开始位置
            bossData.SetStartPos(bossPos);
            // 生成boss
            BattleManager.Instance.AddFighterUnit(bossData);
            // 所有玩家攻击boss
            BattleManager.Instance.SetAllFighterAttackType(UnitType.FieldBoss);
        }
        
        /// <summary>
        /// 移除野外boss数
        /// </summary>
        /// <param name="uintId"></param>
        public void RemoveFieldBossData(long uintId)
        {
            if (_fieldBossDatas.ContainsKey(uintId))
                _fieldBossDatas.Remove(uintId);

            for (int i = 0; i < _fieldBossPosArray.Length; i++)
            {
                for (int j = 0; j < _fieldBossPosArray[i].Length; j++)
                {
                    if (_fieldBossPosArray[i][j] == uintId)
                        _fieldBossPosArray[i][j] = 0;
                }
            }

        }
        
        /// <summary>
        /// 复活
        /// </summary>
        /// <param name="userId"></param>
        public void Revive(string userId)
        {
            var roleData = GetRoleData(userId,false);
            var role = BattleManager.Instance.GetFighterByUID(roleData.UnitID, true);
            
            if(role == null)
                return;
            
            if(role.IsDeath)
                role.BehaviorData.Relive();
        }

        public void DisposeFieldBoss()
        {
            _fieldBossDatas.Clear();
        }

        #endregion
    }
}