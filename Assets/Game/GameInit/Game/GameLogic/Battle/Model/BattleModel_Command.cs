﻿using System.Collections.Generic;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 战斗指令模块
    /// </summary>
    public partial class BattleModel
    {
        // 玩家数据
        private Dictionary<string, RoleData> _dictUserIdUnit;
        
        private void InitCommand()
        {
            _dictUserIdUnit = new Dictionary<string, RoleData>();
        }
        
        /// <summary>
        /// 从玩家字典中移除
        /// </summary>
        /// <param name="userId"></param>
        public void RemoveRoleData(string userId)
        {
            if (_dictUserIdUnit.ContainsKey(userId))
                _dictUserIdUnit.Remove(userId);
        }

        //加入游戏
        public bool AddRole(string userId, string name,int guildId, string headUrl, int winStreak,int haloExp ,int petExp, int defaultExp)
        {
            // 添加玩家
            return AddRoleUni(userId, name, guildId,headUrl, winStreak,haloExp,petExp, defaultExp);
        }

        //加入游戏
        private bool AddRoleUni(string userId,string name,int guildId,string headUrl,int winStreak,int haloExp,int petExp,int defaultExp = 0)
        {
            // 夜晚时间限制加入
            if (GameConstantHelper.ProhibitJoinNightTime != -1 
                && BattleManager.Instance.NightTime >= GameConstantHelper.ProhibitJoinNightTime)
                return false;
            
            // 夜晚不能其他公会加入
            if (BattleManager.Instance.IsNight && !BattleManager.Instance.GuildId.Equals(guildId))
                return false;
            
            foreach (var VARIABLE in _dictUserIdBoss)
            {
                if (VARIABLE.Value.UserId.Equals(userId))
                {
                    Logger.LogBlue($"加入游戏 不允许boss加入 {VARIABLE.Value.Name}->{name}");
                    return false;
                }
            }


            // 不允许玩家重复加入
            if(_dictUserIdUnit.ContainsKey(userId))
                return false;
            
            
            // 获取角色id
            var roleID = RoleConfigUtils.RandomRoleID();

             //roleID = 20000;    
             //defaultExp =22047;
            if (roleID == 0)
            {
                Logger.LogError("[BattleModel.AddRole() => 随机角色ID出错，添加失败]");
                return false;
            }
            
            // 角色数据
            var roleData = new RoleData(userId,GenerateUnitID());
            // 设置玩家信息
            roleData.SetPlayerInfo(headUrl,guildId,winStreak,haloExp,petExp);
            // 初始化数据
            if (!roleData.InitFighter(roleID, name, defaultExp))
                return false;

            // 加入字典
            _dictUserIdUnit.Add(userId, roleData);
            
            // 显示世界排名玩家入场
            GameNoticeModel.Instance.ShowTopPlayer(roleData);
            
            // 获取开始位置
            var pos = GetStartPos(roleData.UnitID);
            
            // 更新开始位置
            roleData.SetStartPos(pos);
            
            // 生成英雄
            BattleManager.Instance.AddFighterUnit(roleData);
            
            return true;
        }
        // 获取开始位置
        private Vector3 GetStartPos(long unitId)
        {
            // 单元格大小
            var CellSize = BattleConst.CellSize;
            // 单个开始位置
            var SrcPos = BattleConst.SrcPos;
            // 计算位置 
            var index = unitId % 9;
            var x = index % 3;
            var y = Mathf.Floor(index / 3f);
            var cellPos = SrcPos + new Vector3((x) * CellSize.x, 0, (y) * CellSize.z);
            var random = new Vector3((Random.Range(0, 4f)) * CellSize.x / 4, 
                0, 
                (Random.Range(0, 4f)) * CellSize.z / 4);
            var pos = cellPos + random;
            return pos;
        }
        // 切换攻击目标
        public void SetTargetType(string userId, UnitType unitType)
        {
            if (_dictUserIdUnit.ContainsKey(userId))
            {
                var roleDate = _dictUserIdUnit[userId];
                // 更改攻击目标
                roleDate?.SetTargetType(unitType);
                // 角色
                var roleUnit = GetRoleUnit(userId, false);
                // 重新搜索目标
                roleUnit?.SearchAttackTarget();
            }
        }
       
        private void DisposeCommand()
        {
            _dictUserIdUnit?.Clear();
        }
    }
}