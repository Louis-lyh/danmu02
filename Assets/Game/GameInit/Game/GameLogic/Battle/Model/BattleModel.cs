using System.Collections.Generic;
using GameHotfix.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 战斗模块
    /// </summary>
    public partial class BattleModel : DataModelBase<BattleModel>
    {
        // 游戏结束
        public const string EventBattleEnd = "Event_BattleEnd";
        // 夜晚阶段结束
        public const string EventNightEnd = "Event_NightEnd";
        
        public void Init()
        {
            InitUnit();
            InitCommand();
            InitGiftSkill();
            InitData();
            InitFieldBoss();
            _uid = 0;

            // 注册事件
            AddEvent();
        }

        public void DisposeModel()
        {
            DisposeCommand();
            DisposeUnit();
            DisposeData();
            DisposeFieldBoss();
            // 注销事件
            RemoveEvent();
        }

        private void AddEvent()
        {
            // 游戏结束
            AddEventListener(EventBattleEnd,BattleEnd);
        }

        private void RemoveEvent()
        {
            // 游戏结束
            RemoveEventListener(EventBattleEnd,BattleEnd);
        }

        /// <summary>
        /// 创建boss
        /// </summary>
        public void CreateBoss()
        {
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                CreateNormalBoss();
            else if(BattleManager.Instance.BattleType == BattleType.FieldBoss)
                CreateFieldBoss();
        }


        // 游戏结束
        private void BattleEnd(BaseEvent baseEvent)
        {
            // 游戏结束
            BattleManager.Instance.ExitBattle();
            // 清理技能
            ClearGiftSkill();
        }
        
        private static long _uid;
        public static long GenerateUnitID()
        {
            _uid++;
            return _uid;
        }
    }
}