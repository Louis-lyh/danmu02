﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public partial class BattleModel
    {
        // 小狼人间隔时间
        private uint _littleWerewolfInterval = 0;
        // 小狼人定时器列表
        private List<uint> _littleWerewolfTimers;
        
        // 声波技能个数
        private List<int> _werewolfSonicList;
        // 是否在声波技能中
        private bool _inWerewolfSonic = false;
        
        
        // 免费技能定时器
        private uint _freeSonicSkillTimer;
        private uint _freeLittleWerewolfTimer;

        // 初始化
        public void InitGiftSkill()
        {
            // 小狼人
            _littleWerewolfInterval = 0;
            _littleWerewolfTimers = new List<uint>();
            
            // 声波
            _werewolfSonicList = new List<int>(); 
            _inWerewolfSonic = false;
        }


        #region 玩家技能
        /// <summary>
        /// 所有玩家增加护盾
        /// </summary>
        /// <param name="giftNum">礼物数量</param>
        public void AddShield_AllRole(int giftNum)
        {
            // 所有玩家增加护盾
            var unitList = BattleManager.Instance.AllRole();
            for (int i = 0; i < unitList.Count; i++)
            {
                // 玩家
                var unit = unitList[i];
                // 护盾
                AddBuff(7007,unit.FighterData,unit.FighterData,
                    1,giftNum);
            }
        }
        #endregion

        #region 狼人礼物技能
        
        // 启动免费boss技能
        public void StartFreeBossSkill()
        {
            // 免费声波技能
            var freeBossSkillLikeTime = (uint)GameConstantHelper.FreeBossSkillLikeTime * 1000;
            _freeSonicSkillTimer = TimerHeap.AddTimer(freeBossSkillLikeTime, 0, () =>
            {
                WerewolfSonicSkills(1, 100025);
            });
            // 显示声波技能icon
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventFreeSonicSkillIcon,GameConstantHelper.FreeBossSkillLikeTime);
            
            
            // 免费小狼人技能
            var freeLittleWerewolfTime = (uint) GameConstantHelper.FreeLittleWerewolfTime * 1000;
            _freeLittleWerewolfTimer = TimerHeap.AddTimer(freeLittleWerewolfTime, 0, () =>
            {
                LittleWerewolfSkills(1);
                // boss死亡跳出
                if(BattleManager.Instance.BossUnit == null)
                    return;
                
                // 显示小狼人
                var roleData = BattleManager.Instance.BossUnit.FighterData;
                GameNoticeModel.Instance.ShowDangerousTips(roleData?.Name,"召唤了<color=#EAE678FF>小怪</color>," +
                                                                          "输入“<color=#EAE678FF><size=25>打怪</size></color>“攻击小怪");
            });
            // 显示小狼人技能icon
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventFreeLittleWerewolfSkill,GameConstantHelper.FreeLittleWerewolfTime);

        }
        
         /// <summary>
        /// 小狼人技能
        /// </summary>
        /// <param name="giftNum">礼物数量</param>
        public void LittleWerewolfSkills(int giftNum)
        {
            for (uint i = 0; i < giftNum; i++)
            {
                // 生成间隔
                var interval = _littleWerewolfInterval;

                var timer = TimerHeap.AddTimer(interval, 0, () =>
                {
                    if (BattleManager.Instance.BossUnit != null &&
                        !BattleManager.Instance.BossUnit.IsDeath)
                    {
                        // 触发狼人技能
                        MonsterManager.Instance.BossSummonMonster();
                    }
                    
                    // 间隔时间减少
                    if(_littleWerewolfInterval > 0)
                        _littleWerewolfInterval -= 1000;
                    
                    // 移除列表
                    _littleWerewolfTimers.RemoveAt(0);
                });
                
                // 增加时间间隔
                _littleWerewolfInterval += 1000;
                // 加入列表
                _littleWerewolfTimers.Add(timer);
            }
        }
        
        /// <summary>
        /// 狼人声波技能
        /// </summary>
        /// <param name="giftNum">技能数量</param>
        /// <param name="skillId">技能id</param>
        public void WerewolfSonicSkills(int giftNum,int skillId)
        {
            var boss = BattleManager.Instance.BossUnit;
            if(boss == null)
                return;

            // 不在在声波技能中
            if (!_inWerewolfSonic)
            {
                // 启动技能
                StartWerewolfSonicSkills(skillId);
                
                // 减少一个
                giftNum--;
            }
            
            for (int i = 0; i < giftNum; i++)
            {
                _werewolfSonicList.Add(skillId); 
            }
        }
        
        // 启动声波技能
        private void StartWerewolfSonicSkills(int skillId)
        {
            // boss死亡
            if (BattleManager.Instance.BossUnit == null 
            || BattleManager.Instance.BossUnit.BehaviorData.IsDeath)
            {
                _werewolfSonicList.Clear();
                _inWerewolfSonic = false;
                return;
            }
            // 启动技能
            // 免费技能
            if(skillId == 100025)
                BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventOpenBossSkill,skillId);
            // 付费技能
            else if (skillId == 100022)
                BattleManager.Instance.BossUnit.FighterData.GetSkill_SKillId(100022)?.ClearCd();
                    
            _inWerewolfSonic = true;
        }
        
        // 声波技能结束回调
        public void WerewolfSonicSkillEnd()
        {
            _inWerewolfSonic = false;
            if (_werewolfSonicList.Count > 0)
            {
                // 启动技能
                StartWerewolfSonicSkills(_werewolfSonicList[0]);
                _werewolfSonicList.RemoveAt(0);
            }
        }
        
        // 清除狼人技能
        public void ClearBossGiftSkill()
        {
            // 删除狼人定时
            for (int i = 0; i < _littleWerewolfTimers.Count; i++)
            {
                TimerHeap.DelTimer(_littleWerewolfTimers[i]);
            }
            
            // 重置小狼人技能
            _littleWerewolfInterval = 0;
            _littleWerewolfTimers = new List<uint>();
            
            // 重置声波技能
            _werewolfSonicList.Clear();
            _inWerewolfSonic = false;
            
            // 清除免费技能
            TimerHeap.DelTimer(_freeSonicSkillTimer);
            TimerHeap.DelTimer(_freeLittleWerewolfTimer);
        }
        #endregion
        
        public void ClearGiftSkill()
        {
            // 清楚狼人技能
            ClearBossGiftSkill();
        }
    }
}