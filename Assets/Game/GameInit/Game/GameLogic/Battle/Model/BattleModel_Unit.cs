using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DG.Tweening;
using GameInit.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;
using Random = UnityEngine.Random;

namespace GameInit.Game
{
    /// <summary>
    /// 战斗工具模块
    /// </summary>
    public partial class BattleModel
    {
        // boss数据
        private Dictionary<string, BossData> _dictUserIdBoss;
        // 当前玩家boss的状态数据
        private RoleData _curPlayerBossRoleData;
        // 历史玩家boss数据
        private List<RoleData> _playerBossRoleDataList;
        
        private void InitUnit()
        {
            _dictUserIdBoss = new Dictionary<string, BossData>();
            // 历史玩家boss 数据
            _playerBossRoleDataList = new List<RoleData>(); 
        }

        // 获得boss玩家userID
        public string GetBossPlayerUerId()
        {
            if (_curPlayerBossRoleData == null)
                return string.Empty;
            else
                return _curPlayerBossRoleData.UserId;
        }
        
        // 获取历史boss
        public RoleData GetBossRoleData(string userId)
        {
            return _playerBossRoleDataList.Find((item) => item.UserId.Equals(userId));
        }

        #region boss阶段
        // 最强角色
        private FighterUnit _firstRole;
        // bossId
        private int _bossId;
        // boss阶段
        public void StartBossStage()
        {
            var bossIdList = GameConstantHelper.StringToIntList(GameConstantHelper.BossId);
            var day = BattleManager.Instance.Day;
            _bossId = day - 1 < bossIdList.Count ? bossIdList[day - 1] : -1;
            
            // 玩家boss
            if (_bossId == -1)
            {
                // 上一个boss玩家
                if(_curPlayerBossRoleData != null)
                    _firstRole = BattleManager.Instance.GetFighterByUID(_curPlayerBossRoleData.UnitID);
                
                // 最强角色
                if(_firstRole == null)
                    _firstRole = GetRoleTopNum();

                // 打开boss来临界面
                BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventOpenBossTips,_firstRole.FighterData as RoleData);
            }
            // 怪兽boss
            else
            {
                // 第几名
                var topNum = GetBossTopNum(GameConstantHelper.MonsterBossLevelByPlayer);
                
                // 前几名数量
                var unitFirst = BattleManager.Instance.GetTopUnit(topNum);
                // log
                Logger.LogBlue($"第几名 = topNum = {topNum} unitFirst.Count = {unitFirst.Count}");
                var level = 1;
                if (unitFirst.Count > 0)
                    level = unitFirst[unitFirst.Count - 1].FighterData.Level;
                // bossId
                _bossId += level;
                var roleDef = RoleConfig.Get(_bossId);
                // 打开boss来临界面
                BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventOpenBossTips,roleDef);
            }
        }
        
        // 创建boss
        public void CreateNormalBoss()
        {
            if (_bossId == -1)
            {
                // 玩家变为boss
                 CreatePlayerToBoss();
            }
            else
            {
                // 先创建怪兽
                MonsterManager.Instance.CreateMonsterInStartBoss(() =>
                {
                    // 创建怪兽boss
                    if(BattleManager.Instance.IsNight)
                        CreateMonsterBoss();
                });
            }
        }
        
        // 修改玩家位置
        public void ChangeAllPlayerPos()
        {
            if (_bossId == -1)
            {
                // 设置玩家位置
                SetAllPlayerPos(new []{1,0,2,3,5,6,7,8});
            }
            else
            {
                // 设置玩家位置
                SetAllPlayerPos(new []{1,0,2});
            }
        }

        // 玩家变为boss
        private void CreatePlayerToBoss()
        {
            if(_firstRole == null)
                return;
            var roleData = _firstRole.FighterData as RoleData;

            // 创建boss
            CreatePlayerToBoss(roleData);
            // 删除玩家
            _firstRole.DoDamage(null,new DamageInfo(int.MaxValue,false));
            // 所有玩家攻击boss
            BattleManager.Instance.SetAllFighterAttackType(UnitType.Boss);
            // 重置
            _firstRole = null;
        }
        // 获取前几名英雄
        private FighterUnit GetRoleTopNum()
        {
            // 前几名
            var topNum = GetBossTopNum(GameConstantHelper.RandomPlayerNum);
            
            // 前几名数量
            var unitFirst = BattleManager.Instance.GetTopUnit(topNum);
            if (unitFirst.Count == 0)
            {
                Logger.LogError("没有玩家");
                BattleModel.Instance.DispatchEvent(BattleModel.EventBattleEnd,UnitType.Boss);
                return null;
            }
            var unit = unitFirst[Random.Range(0, unitFirst.Count)];
            if(unit == null)
                return null;
            
            return unit;
        }
        public int GetBossTopNum(string topNumStr)
        {
            // 随机boss的玩家数量
            var topNumList = GameConstantHelper.StringToIntList(topNumStr);
            // 数量
            var topNum = 1;
            var curDay = BattleManager.Instance.Day;
            
            if (curDay == 0)
                curDay = 1;
            
            if (curDay <= topNumList.Count)
                topNum = topNumList[curDay - 1];

            return topNum;
        }
        // 创建boss
        private void CreatePlayerToBoss(RoleData unitData)
        {
            // 生成boss
            // boss数据
            var bossData = new BossData(unitData.UserId,unitData.UnitID);
            bossData.Copy(unitData);
            
            // bossId
            var bossId = unitData.RoleDef.RoleID;
            // 初始化数据
            if (!bossData.InitFighter(bossId, unitData.Name, unitData.Level))
                return;
            
            // boss玩家状态数据
            _curPlayerBossRoleData = unitData;
            
            // 记录boss玩家
            if(!_playerBossRoleDataList.Contains(unitData))
                _playerBossRoleDataList.Add(unitData);
            
            // 加入字典
            if (_dictUserIdBoss.ContainsKey(unitData.UserId))
                _dictUserIdBoss[unitData.UserId] = bossData;
            else 
                _dictUserIdBoss.Add(unitData.UserId,bossData);

            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            var bossPos = srcPos + new Vector3(1.5f * cellSize.x, 0, 1.5f * cellSize.z);
            // 更新开始位置
            bossData.SetStartPos(bossPos);
            // 生成boss
            BattleManager.Instance.AddFighterUnit(bossData);
            // 显示boss血条
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowBossHp,bossData);
        }
        // 创建怪兽boss
        private void CreateMonsterBoss()
        {
            // 初始化数据
            var bossData = new MonsterBossData(GenerateUnitID());
            // 初始化数据
            if (!bossData.InitFighter(_bossId, bossData.Name))
                return;
            
            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            var bossPos = srcPos + new Vector3(1.5f * cellSize.x, 0, 2f * cellSize.z);
            // 更新开始位置
            bossData.SetStartPos(bossPos);
            // 生成boss
            BattleManager.Instance.AddFighterUnit(bossData);
            // 显示boss血条
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowBossHp,bossData);
        }
        // 复活boss玩家
        public async void ReviveBossPlayer()
        {
            if(_bossId  > 0 || _curPlayerBossRoleData == null)
                return;
            // boss玩家数据
            var roleData = _curPlayerBossRoleData;
            
            // 判断boss是否死亡
            var unitBoss = BattleManager.Instance.BossUnit as FighterUnitBoss;
            if (unitBoss == null || unitBoss.IsDeath)
            {
                _curPlayerBossRoleData = null;
            }
            else
            {
                // 复制
                roleData.Copy(unitBoss.BossData);
                // 刷新数据
                roleData.ChangeDef(unitBoss.BossData.Level);
                // 延迟击杀boss
                await Task.Delay(2000);
                // 杀死boss
                unitBoss?.DoDamage(null,new DamageInfo(int.MaxValue,false),true);
            }
            
            // 复活
            await Task.Delay(3000);
            // 回血
            roleData.RestoreHp(0,100);
            // 修改id
            roleData.ChangeUnitID(GenerateUnitID());
            // 复活boss玩家
            ReviveBossPlayer(roleData);
            
           
        }
        // 复活boss玩家
        private bool ReviveBossPlayer(RoleData roleData)
        {
            // 存在退出
            if(_dictUserIdUnit.ContainsKey(roleData.UserId))
                return false;
            
            // 加入字典
            _dictUserIdUnit.Add(roleData.UserId,roleData);

            if (_dictUserIdBoss.ContainsKey(roleData.UserId))
                _dictUserIdBoss.Remove(roleData.UserId);
            
            // 单元格大小
            var cellSize = BattleConst.CellSize;
            // 单个开始位置
            var srcPos = BattleConst.SrcPos;
            var rolePos = srcPos + new Vector3(1.5f * cellSize.x, 0, 2.5f * cellSize.z);
            // 更新开始位置
            roleData.SetStartPos(rolePos);
            
            // 生成boss 玩家
            BattleManager.Instance.AddFighterUnit(roleData);
            
            return true;
        }
        // 设置玩家位置
        private void SetAllPlayerPos(int[] posCells)
        {
            var allPlayers = BattleManager.Instance.AllRole();
            for (int i = 0; i < allPlayers.Count; i++)
            {
                // 单元格大小
                var cellSize = BattleConst.CellSize;
                // 单个开始位置
                var srcPos = BattleConst.SrcPos;
                
                // 单元格序号
                var index = i % posCells.Length;
                var cellIndex = posCells[index];
                // 计算位置 
                var x = cellIndex % 3;
                var y = cellIndex / 3;
                var cellPos = srcPos + new Vector3((x) * cellSize.x, 0, (y) * cellSize.z);
                var random = new Vector3((Random.Range(0, 4f)) * cellSize.x / 4, 
                    0, 
                    (Random.Range(0, 4f)) * cellSize.z / 4);
                var pos = cellPos + random;
                
                // 设置位置
                if(allPlayers[i] != null)
                    allPlayers[i].SetPos(pos);
            }

        }

        #endregion

        // 获取动画时间数据
        public RoleAnimationTimeDef GetActionTimeDef(string roleName)
        {
            var configDic = RoleAnimationTimeConfig.GetDefDic();
            var keys = configDic.Keys.ToList();

            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var def = configDic[key];
                if (def.RoleName.Equals(roleName))
                    return def;
            }
            return null;
        }
        
        #region Bullet相关
        // 创建子弹_攻击目标
        public void AddBullet(SkillData skillData, FighterUnit attack, Vector3 startPos,Vector3 endPos, FighterUnit target)
        {
            // 实例化子弹数据
            var bulletData = new BulletData(skillData, GenerateUnitID());
            // 初始化子弹数据
            bulletData.SetData(attack, startPos, endPos,target);
            // 创建子弹
            AddBullet(bulletData);
        }
        // 创建子弹
        private void AddBullet(BulletData bulletData)
        {
            BattleManager.Instance.AddBullet(bulletData);
        }
        #endregion

        #region 宠物
        // 创建宠物
        public long AddPet(int defId,FighterUnit owner)
        {
            // 唯一id
            var unitId = GenerateUnitID();
            // 宠物数据
            var petData = new PetData(unitId,owner);
            // 初始化
            petData.InitFighter(defId,owner.FighterData.Name +"_Pet");
            // 开始位置
            var startPos = (petData.ModelScale / 2 + owner.FighterData.ModelScale / 2 + 1) * -owner.UnitContainerTransform.forward;
            startPos += owner.Position;
            startPos.y = 0;
            petData.SetStartPos(startPos);
            // 创建
            BattleManager.Instance.AddFighterUnit(petData);
            return unitId;
        }
        #endregion
        #region Get Battle Data
        // 获取unit
        public RoleData GetRoleData(string userId,bool containBoss)
        {
            if (_dictUserIdUnit.ContainsKey(userId))
            {
                return _dictUserIdUnit[userId];
            }
            
            // 查看是否是 boss的userId
            if (containBoss && _dictUserIdBoss.ContainsKey(userId))
                return _dictUserIdBoss[userId];
            
            return null;
        }
        // 获取Unit
        public FighterUnit GetRoleUnit(string userId,bool containBoss)
        {
            var unitData = GetRoleData(userId,containBoss);
            if (unitData == null)
            {
                return null;
            }
            // 玩家
            var unit = BattleManager.Instance.GetFighterByUID(unitData.UnitID,true);
            
            // 判断是否是boss
            if(containBoss)
                if(BattleManager.Instance.BossUnit != null)
                    if (BattleManager.Instance.BossUnit.FighterData.UnitID == unitData.UnitID)
                        return BattleManager.Instance.BossUnit;


            return unit;
        }
        // 获取所有
        public List<RoleData> GetAllRoleData()
        {
            // 所有角色数据列表
            List<RoleData> allRoleData = new List<RoleData>();

            // 小动物数据
            var units = _dictUserIdUnit.Values.ToList();
            for (int i = 0; i < units.Count; i++)
            {
                allRoleData.Add(units[i]);
            }

            // bss 数据
            var bossList = _dictUserIdBoss.Values.ToList();
            for (int i = 0; i < bossList.Count; i++)
            {
                // 没用相同角色加入列表
                if(allRoleData.Find(item=>item.UserId == bossList[i].UserId) == null)
                    allRoleData.Add(bossList[i]);   
            }
            return allRoleData;
        }
        // 前十名分积分池积分
        public void TopTenAddScore()
        {
            // 比例数据
            var scoreRatioStr = GameConstantHelper.ScorePoolRatio.Split(',');
            // 玩家数据
            var roleDates = GetAllRoleData();
            // 取前十
            if (roleDates.Count > 10)
                roleDates = roleDates.GetRange(0, 10);
            // 排序
            roleDates.Sort((a, b) =>
                {
                    if (b.KillScore > a.KillScore)
                        return 1;
                    else if(b.KillScore < a.KillScore)
                        return -1;
                    else
                        return 0;
                });

            for (int i = 0; i < roleDates.Count; i++)
            {
                // 比例
                var scoreRatio = scoreRatioStr.Length > i? float.Parse(scoreRatioStr[i]) : 0;
                // 分数
                double score = scoreRatio / 100 * BattleManager.Instance.ScorePool;
                
                roleDates[i].AddScore((long)score);
            }
        }

        #endregion
        
        private void DisposeUnit()
        {
            _dictUserIdBoss.Clear();
            _curPlayerBossRoleData = null;
            _playerBossRoleDataList.Clear();
        }
    }
}