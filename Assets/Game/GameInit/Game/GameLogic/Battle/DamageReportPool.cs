﻿using System.Collections.Generic;

namespace GameInit.Game
{
    /// <summary>
    /// 伤害结果
    /// </summary>
    public class DamageReport
    {
        #region 对象池
        // 伤害结果池子
        private static Queue<DamageReport> _reportPool;
        // 获取伤害结果对象
        public static DamageReport GetDamageReport(int damage, bool isCritical = false)
        {
            var report = GetDamageReport();
            report.SetData(damage, isCritical);
            return report;
        }
        // 获取伤害结果对象
        public static DamageReport GetDamageReport()
        {
            _reportPool ??= new Queue<DamageReport>();
            var report = _reportPool.Count == 0 ? new DamageReport() : _reportPool.Dequeue();
            return report;
        }
        // 回收伤害结果对象
        public static void CollectReport(DamageReport report)
        { 
            // _reportPool?.Enqueue(report);
        }
        // 销毁
        public static void Dispose()
        {
            _reportPool?.Clear();
            _reportPool = null;
        }

        #endregion
        // 伤害
        public int Damage { get; private set; }
        // 是否暴击
        public bool IsCritical { get; private set; }
        
        public void SetData(int damage, bool isCritical)
        {
            Damage = damage;
            IsCritical = isCritical;
        }
    }
}