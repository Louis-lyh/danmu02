using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameInit.Game
{
    public class Effect : IDispose
    {
        #region 特效时长获取函数
        // 特效配置列表
        private static List<EffectDef> _allEffectConfigs;
        // 初始化配置列表
        private static void InitEffectDef()
        {
            // 特效配置列表初始化
            if (_allEffectConfigs == null)
            {
                _allEffectConfigs = new List<EffectDef>();
                var dict = EffectConfig.GetDefDic();
                foreach (var kv in dict)
                    _allEffectConfigs.Add(kv.Value);
            }
        }

        // 获取特效配置
        public static EffectDef GetEffectDef(string effectName)
        {
            // 初始化配置列表
            if (_allEffectConfigs == null)
                InitEffectDef();
            
            // 通过特效名返回特效
            for (var i = 0; i < _allEffectConfigs.Count; i++)
            {
                if (_allEffectConfigs[i].EffectName == effectName)
                    return _allEffectConfigs[i];
            }
            return null;
        }
        // 获取特效配置
        public static EffectDef GetEffectDef(int effectId)
        {
            // 初始化配置列表
            if (_allEffectConfigs == null)
                InitEffectDef();
            
            // 通过特效名返回特效
            for (var i = 0; i < _allEffectConfigs.Count; i++)
            {
                if (_allEffectConfigs[i].ID == effectId)
                    return _allEffectConfigs[i];
            }
            return null;
        }
        #endregion
        // 创建场景特效,将特效GameObject添加到UnitRoot节点下（与unit平级）
        public static Effect CreateSceneEffect(int effectId, Vector3 pos,Vector3 scale,Quaternion quaternion)
        {
            return CreateEffect(effectId, BattleManager.Instance.UnitRoot, pos, scale,quaternion,false);
        }
        // 创建特效
        public static Effect CreateEffect(int effectId, Transform parent, Vector3 pos,Vector3 scale,Quaternion quaternion, bool isLocalPos = false)
        {
            // 获取配置
            var def = GetEffectDef(effectId);
            if (def == null)
                return null;
            
            // 创建新特效
            var effect = new Effect();
            effect.Create(def, parent, pos,scale,quaternion,isLocalPos);
            return effect;
        }

        // 创建场景特效,将特效GameObject添加到UnitRoot节点下（与unit平级）
        public static Effect CreateSceneEffect(string effect, Vector3 pos,Vector3 scale,Quaternion quaternion)
        {
            return CreateEffect(effect, BattleManager.Instance.UnitRoot, pos,scale,quaternion,false);
        }
        // 创建特效
        public static Effect CreateEffect(string effectName, Transform parent, Vector3 pos,Vector3 scale,Quaternion quaternion,bool isLocalPos = false)
        {
            if (string.IsNullOrEmpty(effectName))
                return null;
            // 获取配置
            var def = GetEffectDef(effectName);
            
            var effect = new Effect();
            effect.Create(def, parent, pos,scale,quaternion, isLocalPos);
            return effect;
        }
        // 特效对象
        private GameObject _effectObject;
        public GameObject Object => _effectObject;
        // 自动销毁脚本
        private AutoDestroyScript _autoDestroyScript; 
        // 特效销毁回调
        public Action<Effect> OnEffectTimeOver;
        // 创建超过回调 
        public Action OnCreateComplete;
        // 是否销毁
        private bool _isDisposed;    
        public string EffectName { get; private set; }
        private string _path;
        private  async void Create(EffectDef def, Transform parent, Vector3 pos,Vector3 scale,Quaternion quaternion, bool isLocalPos = false)
        {
            _isDisposed = false;
            // 初始化名字
            EffectName = def.EffectName;
            // 获取特效路径
            _path = GameResPathUtils.GetEffectPrefab(EffectName);
            // 从对象池加载特效
            _effectObject = await BattleResPool.ConstructObjectAsync(_path);
            
            if (_isDisposed)
            {
                BattleResPool.CollectObject(_path, _effectObject);
                OnEffectTimeOver = null;
                return;
            }
            
            if (_effectObject == null)
            {
                OnEffectAutoDestroy();
                return;
            }
            
            // 设置位置
            if (isLocalPos)
                _effectObject.transform.localPosition = GetPos(def.PosType,pos); //Vector3.zero;
            else
                _effectObject.transform.position = pos;
            // 初始化
            _effectObject.SetActive(true);
            _effectObject.transform.SetParent(parent, false);
            _effectObject.transform.localScale = scale * def.PosScale; // 设置缩放
            _effectObject.transform.localRotation = quaternion;
            
            // 设置循环
            SetLoop(def.Loop,def.Time);

            await UniTask.NextFrame();
            
            // 成功回调
            OnCreateComplete?.Invoke();
            OnCreateComplete = null;
        }
        
        // 设置循环
        private void SetLoop(int loopType,float lifeTime)
        {
            // 循环退出不用添加销毁脚本
            if (loopType == 1)
                return;

            // 添加自动销毁函数
            _autoDestroyScript = _effectObject.AddComponent<AutoDestroyScript>();
            _autoDestroyScript.SetLifeTime(lifeTime);
            // 销毁回调
            _autoDestroyScript.OnDestroyAction = OnEffectAutoDestroy;
        }

        // 添加存活时间
        public void AddLife(string effectName)
        {
            // 获取配置
            var def = GetEffectDef(effectName);
            if (_autoDestroyScript != null)
            {
                _autoDestroyScript.AddLifeTime(def.Time);
            }
        }

        // 销毁
        private void OnEffectAutoDestroy()
        {
            if (OnEffectTimeOver != null)
            {
                OnEffectTimeOver.Invoke(this);
                OnEffectTimeOver = null;
                return;
            }
            Dispose();
        }
        // 获取位置
        private Vector3 GetPos(int dir,Vector3 pos)
        {
            switch (dir)
            {
                // 脚后方
                case 5:
                    return pos + Vector3.back * 0.5f;
            }
            return pos;
        }

        public void Dispose()
        {
            // 回收特效
            if (_effectObject != null)
            {
                BattleResPool.CollectObject(_path, _effectObject);
                _effectObject = null;
            }
            // 删除组件
            if (_autoDestroyScript != null)
            {
                GameObject.Destroy(_autoDestroyScript);
                _autoDestroyScript = null;
            }

            _isDisposed = true;
            OnEffectTimeOver = null;
        }
    }
}