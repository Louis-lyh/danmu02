using Cysharp.Threading.Tasks;
using GameInit.Game.Pet;

namespace GameInit.Game
{
    /// <summary>
    ///  角色工厂
    /// </summary>
    public static class UnitFactory
    {
        public static async UniTask<T> CreatUnit<T>(UnitDataBase data) where T : UnitBase
        {
            UnitBase unit = null;
            switch (data.UnitType)
            {
                case UnitType.Role:
                    unit = new FighterUnitRole();
                    break;
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    unit = new Monster(data.UnitType);
                    break;
                case UnitType.Boss:
                    unit = new FighterUnitBoss();
                    break;
                case UnitType.MonsterBoss:
                    unit = new MonsterBoss();
                    break;
                case UnitType.FieldBoss:
                    unit = new FieldBoss();
                    break;
                case UnitType.MonsterGuard:
                    unit = new MonsterGuard();
                    break;
                case UnitType.Bullet:
                    unit = BulletBase.CreateBullet(data);
                    break;
                case UnitType.Fruit:
                    unit = new FruitUnit(data);
                    break;
                case UnitType.Pet:
                    unit = new PetUnit();
                    break;
            }
            
            if (unit == null)
                return null;
            // 初始化
            await unit.CreatUnit(data);
            return (T)unit;
        }
    }
}