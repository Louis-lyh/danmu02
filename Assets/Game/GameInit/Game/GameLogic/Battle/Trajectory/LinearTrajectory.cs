using System;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 目标点
    /// </summary>
    public class TargetPosLinearTrajectory : TrajectoryBase
    {
        private float _moveStep;

        protected override void OnStart()
        {
            base.OnStart();
            MoveDir = (TargetPos - CurPos).normalized;
            var deg = Mathf.Atan2(-MoveDir.x, MoveDir.y) * 180 / Math.PI;
            CurrentAngle = new Vector3(0, (float) deg, 0);
            // 旋转回调
            UpdateEulerAction?.Invoke(CurrentAngle);
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            // 移动距离
            _moveStep = MoveSpeed * deltaTime;
            CurPos += MoveDir * _moveStep;
            // 移动回调
            UpdatePosAction?.Invoke(CurPos);
            // 旋转
            CurrentAngle = Vector3MathUtils.GetEulerAngles(CurPos, TargetPos);
            // 更新旋转
            UpdateEulerAction?.Invoke(CurrentAngle);
            var distToTarget = Vector3MathUtils.GetDistance2D(TargetPos, CurPos);
            // 没有达到位置
            if (distToTarget > _moveStep)
                return;
            // 到达位置返回目标位置
            UpdatePosAction?.Invoke(TargetPos);
            // 结束回调
            MoveEndAction?.Invoke();
            _isInitialed = false;
        }
    }

    /// <summary>
    /// 目标对象，需要追踪
    /// </summary>
    public class TargetLinearTrajectory : TrajectoryBase
    {
        private float _moveStep;
        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            // 移动方向
            MoveDir = (TargetPos - CurPos).normalized;
            // 移动距离
            _moveStep = MoveSpeed * Time.deltaTime;
            // 当前位置
            CurPos += MoveDir * _moveStep;
            // 更新位置
            UpdatePosAction?.Invoke(CurPos);
            // 旋转
            CurrentAngle = Vector3MathUtils.GetEulerAngles(CurPos, TargetTransform.localPosition);
            // 更新旋转
            UpdateEulerAction?.Invoke(CurrentAngle);
            // 距离
            var distToTarget = Vector3MathUtils.GetDistance2D(TargetTransform.localPosition, CurPos);
            // 大于移动距离继续
            if (distToTarget > _moveStep)
                return;
            
            // 移动结束
            UpdatePosAction?.Invoke(TargetTransform.position);
            MoveEndAction?.Invoke();
            _isInitialed = false;
        }
    }
}