using System;
using GameHotfix.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 轨道基类
    /// </summary>
    public abstract class TrajectoryBase : UpdateBase
    {
        // 移动结束回调
        protected Action MoveEndAction;
        // 更新位置回调
        protected Action<Vector3> UpdatePosAction;
        // 更新角度回调
        protected Action<Vector3> UpdateEulerAction;
        // 目标
        protected Transform TargetTransform;
        // 目标位置
        protected Vector3 TargetPos;
        // 当前位置
        protected Vector3 CurPos;
        // 移动速度
        protected float MoveSpeed;
        // 移动方向
        protected Vector3 MoveDir;
        // 当前角度
        protected Vector3 CurrentAngle;
        // 轨道类型
        public int TrajectoryType { get; private set; }
        // 开始移动
        public void StartMove(Vector3 startPos,Vector3 endPos, Transform targetTransform, float speed)
        {
            MoveSpeed = speed;
            CurPos = startPos;
            TargetTransform = targetTransform;
            TargetPos = endPos;
            OnStart();
            OnInitialize();
        }
        // 改变速度
        public void ChangeMoveSpeed(float speed)
        {
            MoveSpeed = speed;
        }

        protected virtual void OnStart(){}
        
        // 销毁
        protected override void OnDispose()
        {
            MoveEndAction = null;
            UpdatePosAction = null;
            base.OnDispose();
        }

        #region static

        public const int TargetLinear = 1; // 目标线性轨迹
        public const int Parabola = 2;    // 抛物线
        public const int TargetPosLinear = 3; // 目标位置线性
        public const int LinearTargetEffect = 4; // 线性目标效果
        
        // 创建轨道
        public static TrajectoryBase CreateTrajectory(int trajectory, Action moveEnd, Action<Vector3> updatePos, Action<Vector3> updateEuler)
        {
            TrajectoryBase trajectoryBase = null;
            switch (trajectory)
            {
                case TargetLinear: 
                    trajectoryBase = new TargetLinearTrajectory();
                    break;
                case TargetPosLinear:
                case LinearTargetEffect:
                    trajectoryBase = new TargetPosLinearTrajectory();
                    break;
            }
            
            // 没有该类型轨道退出
            if (trajectoryBase == null)
            {
                Logger.LogError($"没有改类型轨道 {trajectory} ");
                return trajectoryBase;
            }

            // 轨道类型
            trajectoryBase.TrajectoryType = trajectory;
            // 移动结束回调
            trajectoryBase.MoveEndAction = moveEnd;
            // 更新位置回调
            trajectoryBase.UpdatePosAction = updatePos;
            // 更新旋转回调
            trajectoryBase.UpdateEulerAction = updateEuler;
            return trajectoryBase;
        }

        #endregion
    }
}

