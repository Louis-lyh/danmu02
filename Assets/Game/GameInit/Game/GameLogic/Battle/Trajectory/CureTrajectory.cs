using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Game
{
    public class CureTrajectory : TrajectoryBase
    {
        /// <summary>
        /// 路径点
        /// </summary>
        private List<Vector3> _lstPathPoints = new List<Vector3>();
        private float _intervalTime = 0.02f; //进入下一个路径点的间隔时间
        private float _pastTime = 0;
        private int _indexPoint = 1;

        public Vector3 MiddlePos;
        protected override void OnStart()
        {
            base.OnStart();
            Vector3 pos1, pos2;
            _lstPathPoints = new List<Vector3>();
            for (int i = 0; i < 25; i++)
            {           
                //一
                pos1 = Vector3.Lerp(CurPos, MiddlePos, i / 25f);
                pos2 = Vector3.Lerp(MiddlePos, TargetPos, i / 25f);
                //二
                var p = Vector3.Lerp(pos1, pos2, i / 25f);
                _lstPathPoints.Add(p);            
            }
            _lstPathPoints.Add(TargetPos);
            _indexPoint = 1;
            _pastTime = 0;
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            _pastTime += deltaTime;
            if (_pastTime > _intervalTime)
            {
                _pastTime = 0;
                var pos = Vector3.Lerp(_lstPathPoints[_indexPoint - 1], _lstPathPoints[_indexPoint], 1);
                UpdatePosAction?.Invoke(pos);
                _indexPoint++;
                if (_indexPoint >= _lstPathPoints.Count)
                {
                    MoveEndAction?.Invoke();
                }
            }
        }
    }
}