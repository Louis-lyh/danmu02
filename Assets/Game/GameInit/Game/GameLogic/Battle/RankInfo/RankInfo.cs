﻿using System;
using System.Collections.Generic;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 排行榜数据
    /// </summary>
    public class RankInfo : DataModelBase<RankInfo>
    {
        // 狼人数据
        private List<ServerData.Rank> _werewolfRanks;
        public List<ServerData.Rank> WerewolfRanks =>_werewolfRanks;
        
        // 小动物数据
        private List<ServerData.Rank> _smallAnimalsRanks;
        public List<ServerData.Rank> SmallAnimalsRanks =>_smallAnimalsRanks;

        public void LoadRanks()
        {
            // 编辑器
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                _werewolfRanks = TestData().list;
                _smallAnimalsRanks = TestData().list;
                // 刷新狼人排行榜
                GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowBossRanking);
            }
            else
            {
                // 获取狼人数据
                GetWerewolf(null);
            }
        }
        
        // 获取狼人数据
        private void GetWerewolf(Action callBack)
        {
            // 狼人数据
            ServerManager.Instance.GetRank(ServerData.ERankType.e_werwolf, data =>
            {
                _werewolfRanks = data?.list;
                // 排序
                _werewolfRanks?.Sort((a, b) =>
                {
                    if (b.score > a.score)
                        return 1;
                    else if (b.score < a.score)
                        return -1;
                    else
                        return 0;
                });
                // 刷新狼人排行榜
                GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowBossRanking);
                // 回调
                callBack?.Invoke();
                
                //定时 1秒
                TimerHeap.AddTimer(1000, 0, () =>
                {
                    // 获取玩家数据
                    GetPlayer(null);
                });
            });
        }
        
        // 获取玩家数据
        private void GetPlayer(Action callBack)
        {
            // 小动物数据
            ServerManager.Instance.GetRank(ServerData.ERankType.e_player, data =>
            {
                _smallAnimalsRanks = data?.list;
                // 排序
                _smallAnimalsRanks?.Sort((a, b) =>
                {
                    if (b.score > a.score)
                        return 1;
                    else if (b.score < a.score)
                        return -1;
                    else
                        return 0;
                });
                // 获取玩家数据
                callBack?.Invoke();
            });
        }
        
        // 排名
        public int RankOrder(string userId,ServerData.ERankType eRankType)
        {
            var order = 0;
            
            if ( eRankType == ServerData.ERankType.e_player)
            {
                if (_smallAnimalsRanks == null)
                {
                    order = 888;
                }
                else
                {
                    order = _smallAnimalsRanks.FindIndex(rank => 
                                rank.userId.Equals(userId)
                            ) + 1;
                }
                
                // log
                ServerManager.Instance.ShowLogText("世界排名排名："+eRankType.ToString()+" "+order +" , "+_smallAnimalsRanks?.Count);
            }
            else if(eRankType == ServerData.ERankType.e_werwolf)
            {
                if (_werewolfRanks == null)
                    order = 777;
                else
                    order = _werewolfRanks.FindIndex(rank => rank.userId.Equals(userId)) + 1;
                
                // log
                ServerManager.Instance.ShowLogText("狼王排名："+eRankType.ToString()+" "+order+" , "+_werewolfRanks?.Count);
            }

            if (order == 0)
                order = 999;
            
           
           
            return order;
        }


        // 测试数据
        private ServerData.RankData TestData()
        {
            
            List<string> userID = new List<string>(){"168921","374801","414201","255421","33517","194011","251291"};
            
            ServerData.RankData testData = new ServerData.RankData();
            testData.list = new List<ServerData.Rank>();
        
            for (int i = 0; i < 30; i++)
            {
                ServerData.Rank rank = new ServerData.Rank();
                rank.userName = "玩家 " + i;
                rank.headUrl = "";
                rank.score = 922337203685477580 - i;
                rank.userId = i < userID.Count ? userID[i] : "";
                
                testData.list.Add(rank);
            }

            return testData;
        }
        
    }
}