﻿using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class RestoreHPBuffAction : BuffActionBase
    {
        public RestoreHPBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }
        // 叠加
        public override void Superposition(BuffDef def)
        {
            // 叠加持续事件
            _buffData.AddDuration(def.Duration);
        }
        protected override void OnBuffTrigger()
        {
            // 添加特效
            AddEffect();
            if(_buffData.ValueType == 1)
                // 回血
                Fighter.FighterData.RestoreHp(_buffData.Value, 0);
            else if(_buffData.ValueType == 0)
                // 回血
                Fighter.FighterData.RestoreHp(0, _buffData.Value);

        }

        protected override void RemoveEffect()
        {
            
        }
    }
}