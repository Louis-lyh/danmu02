namespace GameInit.Game
{
    public static class BuffType
    {
        /// <summary>
        /// 属性增益buff
        /// </summary>
        public const int ATTRIBUTE_BUFF = 1;
        
        /// <summary>
        /// 属性de buff
        /// </summary>
        public const int ATTRIBUTE_DEBUFF = 2;
        
        /// <summary>
        /// 群体增益buff
        /// </summary>
        public const int ATTRIBUTE_ALL_BUFF = 3;
        
        
        /// <summary>
        /// 进化欲望
        /// </summary>
        public const int UPGRADE_DESIRE = 4;
        
        /// <summary>
        /// 点赞
        /// </summary>
        public const int LIKE = 5;
        
        /// <summary>
        /// 模型放大
        /// </summary>
        public const int BODYSCALE = 6;
        
        /// <summary>
        /// 护盾
        /// </summary>
        public const int SHIELD = 7;
        
        /// <summary>
        /// 自动释放技能
        /// </summary>
        public const int AUTO_Skill = 8;
        
        /// <summary>
        /// 回血buff
        /// </summary>
        public const int Restore_HP = 9;
        
        /// <summary>
        /// 修改角色
        /// </summary>
        public const int Change_Role = 10;
    }
    /// <summary>
    /// buff 工厂
    /// </summary>
    public static class BuffFactory
    {
        public static BuffActionBase CreateBuffAction(BuffData data, FighterUnit fighter)
        {
            BuffActionBase action = null;
            switch (data.BuffType)
            {
                case BuffType.ATTRIBUTE_BUFF:
                    action = new AttributeBuffAction(fighter);
                    break;
                case BuffType.UPGRADE_DESIRE:
                    action = new UpgradeDesireBuffAction(fighter);
                    break;
                case BuffType.LIKE:
                    action = new LikeBuffAction(fighter);
                    break;
                case BuffType.BODYSCALE:
                    action = new BodyScaleBuffAction(fighter);
                    break;
                case BuffType.SHIELD:
                    action = new ShieldBuffAction(fighter);
                    break;
                case BuffType.AUTO_Skill:
                    action = new AutoSkillBuffAction(fighter);
                    break;
                case BuffType.Restore_HP:
                    action = new RestoreHPBuffAction(fighter);
                    break;
                case BuffType.Change_Role:
                    action = new ChangeRoleBuffAction(fighter);
                    break;
            }
            return action;
        }
    }
}