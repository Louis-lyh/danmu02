﻿using GameHotfix.Game;

namespace GameInit.Game
{
    public class ShieldBuffAction : BuffActionBase
    {
        // 是否触发
        private bool _isTrigger;
        public ShieldBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }

        public override void Start(BuffData data)
        {
            OnInitialize();
            _buffData = data;
            _isTrigger = false;
        }

        protected override void OnUpdate(float deltaTime)
        {
            if (_buffData == null)
            {
                OnBuffEnd();
                return;
            }
            
            // 是否间隔触发
            if (!_isTrigger)
            {
                OnBuffTrigger();
                _isTrigger = true;
            }
            
            // 护盾小于零移除
            var value = Fighter.FighterData.GetAttributeAddition(AttributeType.Shield);
            if(value <= 0)
                OnBuffEnd();
        }

        protected override void OnBuffTrigger()
        {
            // 添加特效
            AddEffect();
            // 添加增益属性
            Fighter.FighterData.AddAttributeAddition(_buffData.AttributeKey, _buffData.Value);
            // 属性改变
            Fighter.FighterData.OnAttributeChange();
        }
        
        // 移除属性效果
        protected override void RemoveBuffEffect()
        {
            Fighter.FighterData.RemoveAttributeAddition(_buffData.AttributeKey);
            Fighter.FighterData.OnAttributeChange();
        }
    }
}