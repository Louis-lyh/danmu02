using System.Collections.Generic;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public partial class BuffData
    {
        // 拥有者id
        public long BuffOwnerUID { get; set; }
        // 创建者id
        public long BuffCreateUID { get; set; }
        // 配置id
        private int _configId;
        // 配置
        private BuffDef _buffDef;
        
        // 初始化buff
        private BuffData InitBuff(int configId)
        {
            _configId = configId;
            return InitBuff(BuffConfig.Get(configId));;
        }
        private BuffData InitBuff(BuffDef def)
        {
            _buffDef = def;
            if (_buffDef == null)
            {
                Logger.LogMagenta("BuffData.InitBuff() => buff配置找不到");
                return null;
            }

            _configId = def.ID;
            return this;
        }
        // 刷新数据
        public void RefreshBuffDef(BuffDef def)
        {
            _buffDef = def;
        }

        // 增加时间
        public void AddDuration(float time)
        {
            _buffDef.Duration += time;
        }

        // 重置
        private void Reset()
        {
            _buffDef = null;
            BuffOwnerUID = 0;
            BuffCreateUID = 0;
        }
        // 判断效果是否相同
        public bool IsSameEffect(BuffData data)
        {
            if (data == null)
                return false;
            return data.BuffType == BuffType && data.AttributeKey == AttributeKey;
        }
    }
}