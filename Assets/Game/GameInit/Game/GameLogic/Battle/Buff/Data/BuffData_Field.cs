﻿namespace GameInit.Game
{
    /// <summary>
    /// 字段
    /// </summary>
    public partial class BuffData
    {
        public int AttributeKey => _buffDef.AttrKey;

        public int BuffType => _buffDef.Type;

        public int EffectType => _buffDef.EffectType;
        public float Interval => _buffDef.Interval;
        public float DurationTime => _buffDef.Duration;
        public int BuffId => _configId;
        public int ValueType => _buffDef.ValueType;
        public int Value => _buffDef.Value;
        public string Effect => _buffDef.Effect;
        public bool IsUpAdd => _buffDef.IsAddUp == 1;
        public int AddUpLimit => _buffDef.AddUpLimit;
    }
}