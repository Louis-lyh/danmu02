﻿using System.Collections.Generic;

namespace GameInit.Game
{
    /// <summary>
    /// 对象池
    /// </summary>
    public partial class BuffData
    {
        // buff 对象池
        private static readonly Queue<BuffData> BuffDataPool = new Queue<BuffData>();
        // 创建buff对象
        public static BuffData ConstructBuffData(int configId, long owner, long creator)
        {
            var result = BuffDataPool.Count > 0 ? BuffDataPool.Dequeue() : new BuffData();
            result.BuffOwnerUID = owner;
            result.BuffCreateUID = creator;
            return result.InitBuff(configId);
        }
        // 创建buffData对象
        public static BuffData ConstructBuffData(BuffDef def)
        {
            var result = BuffDataPool.Count > 0 ? BuffDataPool.Dequeue() : new BuffData();
            return result.InitBuff(def);
        }
        // 回收对像
        public static void Collect(BuffData data)
        {
            data.Reset();
            BuffDataPool.Enqueue(data);
        }
    }
}