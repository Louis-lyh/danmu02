﻿using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace GameInit.Game
{
    /// <summary>
    /// 点赞buff
    /// </summary>
    public class LikeBuffAction: BuffActionBase
    {
        private RoleData _roleData;
        public LikeBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }

        public override void Superposition(BuffDef def)
        {
            _buffData.AddDuration(def.Duration);
        }

        protected override void OnBuffTrigger()
        {
            // 点赞
            if(_roleData == null)
                _roleData = Fighter.FighterData as RoleData;
            // 点赞
            _roleData.Like(_buffData.Value,false);
        }
    }
}