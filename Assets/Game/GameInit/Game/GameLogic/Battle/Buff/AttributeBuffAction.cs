using GameHotfix.Game;
using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 属性增加增益buff
    /// </summary>
    public class AttributeBuffAction : BuffActionBase
    {
        public AttributeBuffAction(FighterUnit unit)
            : base(unit)
        {
            
        }
        // 触发
        protected override void OnBuffTrigger()
        {
            // 添加特效
            AddEffect();
            // 是否累计
            bool isUpAdd = _buffData.IsUpAdd;
            // 累计上限
            var addUpLimit = _buffData.AddUpLimit;
            switch (_buffData.AttributeKey)
            {
                case AttributeType.Shield: // 护盾
                case AttributeType.Attack: // 攻击力
                case AttributeType.AttackSpeed: // 攻速
                    // 添加增益属性
                    Fighter.FighterData.AddAttributeAddition(_buffData.AttributeKey, _buffData.Value,isUpAdd,addUpLimit);
                    // 属性改变
                    Fighter.FighterData.OnAttributeChange();
                    break;
                case AttributeType.MoveSpeed: // 移动速度
                    // 添加增益属性
                    Fighter.FighterData.AddAttributeAddition(_buffData.AttributeKey, _buffData.Value,isUpAdd,addUpLimit);
                    // 属性改变
                    Fighter.FighterData.OnAttributeChange();
                    // 修改速度
                    Fighter.SetMoveSpeed();
                    break;
            }
        }

        public override void Superposition(BuffDef def)
        {
            // 刷新数据
            _buffData.RefreshBuffDef(def);
            // 在加一次
            OnBuffTrigger();
        }

        // 移除属性效果
        protected override void RemoveBuffEffect()
        {
            Fighter.FighterData.RemoveAttributeAddition(_buffData.AttributeKey);
            Fighter.FighterData.OnAttributeChange();
            // 修改速度
            if(_buffData.AttributeKey == AttributeType.MoveSpeed)
                Fighter.SetMoveSpeed();
        }
    }
}