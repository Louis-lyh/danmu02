using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 增益效果基类
    /// </summary>
    public abstract class BuffActionBase : UpdateBase
    {
        // 数据
        protected BuffData _buffData;
        public BuffData Data => _buffData;
        // 作用对象
        protected FighterUnit Fighter;
        
        protected BuffActionBase(FighterUnit fighterUnit)
        {
            Fighter = fighterUnit;
        }
        // 开始
        public virtual void Start(BuffData data)
        {
            OnInitialize();
            _buffData = data;
            // 是否间隔触发
            _isCheckInterval = true;
            // 间隔事件
            _interval = 0f;
            // 持续时间
            _durationTime = 0f;
        }

        public virtual void Superposition(BuffDef def){}

        // 添加特效
        protected virtual void AddEffect()
        {
            if(_buffData.Effect.IsValid())
                Fighter.AddEffect(_buffData.Effect);
        }
        // 移除特效
        protected virtual void RemoveEffect()
        {
            if(_buffData.Effect.IsValid())
                Fighter.RemoveEffect(_buffData.Effect);
        }
        // 更新
        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            // 是否间隔触发
            if (_isCheckInterval)
                deltaTime = CheckBuffInterval(deltaTime);
            
            // 检查持续时间(-1 永久)
            if (_buffData == null || _buffData.DurationTime < 0)
                return;
            // 检查buff是否到期
            CheckDurationTime(deltaTime);
        }

        protected override void OnDispose()
        {
            // 移除buff
            RemoveEffect();
            // 移除buff
            RemoveBuffEffect();
            Fighter = null;
            _buffData = null;
            base.OnDispose();
        }

        #region 作用间隔逻辑
        // 是否开启间隔
        protected bool _isCheckInterval;
        // 累计间隔时间
        protected float _interval;
        // 间隔检查buff
        protected virtual float CheckBuffInterval(float deltaTime)
        {
            _interval += deltaTime;
            if (_interval < _buffData.Interval)
                return deltaTime;
            
            // 规范时间
            var differ = deltaTime - (_interval - _buffData.Interval);
                
            // 触发buff
            OnBuffTrigger();
            
            // 判断是否间隔触发
            if (_buffData.EffectType == 0)
            {
                _isCheckInterval = false;
                return differ;
            }
            
            _interval = 0;
            return differ;
        }
        // 触发buff
        protected abstract void OnBuffTrigger();

        #endregion

        #region buff持续时间
        // 持续时间
        protected float _durationTime;
        // 检查buff持续时间
        protected void CheckDurationTime(float deltaTime)
        {
            _durationTime += deltaTime;
            if (_durationTime < _buffData.DurationTime)
                return;
            OnBuffEnd();
        }
        // buff结束
        protected virtual void OnBuffEnd()
        {
            Fighter.RemoveBuff(_buffData);
        }

        #endregion
        // 移除buff特效
        protected virtual void RemoveBuffEffect()
        {
        }
    }
}