﻿using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 自动技能bufff
    /// </summary>
    public class AutoSkillBuffAction : BuffActionBase
    {
        // 技能数据
        private SkillData _skillData;
        // 技能
        private List<SkillActionBase> _skillAction;
        
        public AutoSkillBuffAction(FighterUnit fighterUnit)
            : base(fighterUnit)
        {
            
        }
        
        // 启动
        public override void Start(BuffData data)
        {
            OnInitialize();
            _buffData = data;
            // 是否间隔触发
            _isCheckInterval = true;
            // 间隔事件
            _interval = _buffData.Interval;
            // 持续时间
            _durationTime = 0f;
            // 技能数据
            _skillData = SkillDataModel.Instance.CreateSkillData(_buffData.Value);
            // 创建技能
            _skillAction = new List<SkillActionBase>();
        }
        
        // 刷新
        public override void Superposition(BuffDef def)
        {
            // 增加时间
            _buffData.AddDuration(def.Duration);
        }

        protected override void OnUpdate(float deltaTime)
        {
            // 刷新技能
            UpdateSkill(deltaTime);
            
            // 是否间隔触发
            if (_isCheckInterval)
                CheckBuffInterval(deltaTime);
            
            // 检查持续时间(-1 永久)
            if (_buffData == null || _buffData.DurationTime < 0)
                return;
            // 检查buff是否到期
            CheckDurationTime(deltaTime);
        }
        
        // 刷新技能
        private void UpdateSkill(float deltaTime)
        {
            for (int i = 0; i < _skillAction.Count; i++)
            {
                _skillAction[i].Update(deltaTime);
            }
        }

        // 触发
        protected override void OnBuffTrigger()
        {
            // 新建技能
            var skillAction = SkillFactory.CreateSkillActionBase(_skillData);
            _skillAction.Add(skillAction);
            // 找到一个最近目标
            var target = FindTarget();
            // 启动技能
            skillAction.Start(Fighter,target);
        }

        private FighterUnit FindTarget()
        {
            // 半径
            var raduis = _skillData.SkillDef.DamageRadius;

            // 找到攻击范围内的目标
            FighterUnit target = null;
            switch (Fighter.FighterData.TargetType)
            {
                
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    // 攻击范围内的怪兽
                    target = BattleManager.Instance.FindNearestMonster(-1,Fighter.Position, raduis);
                    break;
                case UnitType.Boss:
                case UnitType.MonsterBoss:
                    target =  BattleManager.Instance.BossUnit;
                    break;
                case UnitType.Role:
                    // 攻击范围内的玩家
                    target = BattleManager.Instance.FindNearestHero(-1,-1,Fighter.Position, raduis);
                    break;
            }

            return target;
        }

        // buff结束
        protected override void OnBuffEnd()
        {
            base.OnBuffEnd();
            // 销毁技能
            _skillAction.Clear();
        }
    }
}