﻿using DG.Tweening;

namespace GameInit.Game
{
    public class BodyScaleBuffAction : BuffActionBase
    {
        public BodyScaleBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }

        protected override void OnBuffTrigger()
        {
            // 是否累计
            bool isUpAdd = _buffData.IsUpAdd;
            // 累计上限
            var addUpLimit = _buffData.AddUpLimit;
            
            // 添加增益属性
            Fighter.FighterData.AddAttributeAddition(_buffData.AttributeKey, _buffData.Value,isUpAdd,addUpLimit);
            // 属性改变
            Fighter.FighterData.OnAttributeChange();
            
            // 比例
            var scale = 1 + _buffData.Value / 100f;
            scale *= Fighter.FighterData.ModelScale;
            Fighter.UnitContainerTransform.DOScale(scale, 0.8f);
        }
        
        // 移除属性效果
        protected override void RemoveBuffEffect()
        {
            Fighter.FighterData.RemoveAttributeAddition(_buffData.AttributeKey);
            Fighter.FighterData.OnAttributeChange();
            // 重置
            Fighter.UnitContainerTransform.DOScale(Fighter.FighterData.ModelScale, 0.8f);
        }
    }
}