﻿using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 进化欲望buff
    /// </summary>
    public class UpgradeDesireBuffAction: BuffActionBase
    {
        public UpgradeDesireBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }
        
        // 开始
        public override void Start(BuffData data)
        {
            OnInitialize();
            _buffData = data;
            // 是否间隔触发
            _isCheckInterval = true;
            // 间隔事件
            _interval = _buffData.Interval;
            // 持续时间
            _durationTime = 0f;
        }
        
        // 刷新
        public override void Superposition(BuffDef def)
        {
            // 增加时间
            _buffData.AddDuration(def.Duration);
        }

        protected override void OnBuffTrigger()
        {
            UpgradeDesire upgradeDesire = Fighter.BehaviorData.UpgradeDesire;
            // 处于旺盛不随机
            if(upgradeDesire == UpgradeDesire.Vigorous)
                return;
            
            switch (_buffData.Value)
            {
                // 随机欲望
                case 0:
                    upgradeDesire = Fighter.BehaviorData.RandomUpgradeDesire();
                    break;
                // 旺盛欲望
                case 1:
                    Fighter.BehaviorData.SetUpgradeDesire(UpgradeDesire.Vigorous,0);
                    upgradeDesire = UpgradeDesire.Vigorous;
                    break;
            }
            
            // 气泡通知
            var role = Fighter.FighterData as RoleData;
            var name = role.Name;
            var headUrl = role.HeadUrl;
            var eventContent = GameConstantHelper.UpgradeDesireToString(upgradeDesire);
            
            // 边界气泡
            if(upgradeDesire == UpgradeDesire.Vigorous)
                GameNoticeModel.Instance.SendEdgeBubble(name,headUrl,"进化欲望",eventContent,EdgeBubbleType.PlayerState01);
            else
                GameNoticeModel.Instance.SendEdgeBubble(name,headUrl,"进化欲望",eventContent,EdgeBubbleType.PlayerState02);

        }
    }
}