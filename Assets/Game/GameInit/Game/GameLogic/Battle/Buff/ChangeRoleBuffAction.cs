﻿namespace GameInit.Game
{
    public class ChangeRoleBuffAction : BuffActionBase
    {
        public ChangeRoleBuffAction(FighterUnit fighterUnit) 
            : base(fighterUnit)
        {
        }

        protected override void OnBuffTrigger()
        {
            // 二郎神
            if (_buffData.Value == 60000)
                SoundManager.Instance.PlaySound(1);
            // 变哪吒
            else if(_buffData.Value == 50000)
                SoundManager.Instance.PlaySound(2);
            
            // 相同角色退出
            if(Fighter.FighterData.UnitConfigID == _buffData.Value)
                return;
            // 修改角色
            Fighter.BehaviorData.SetChangeRole(true,_buffData.Value,1f);
        }
    }
}