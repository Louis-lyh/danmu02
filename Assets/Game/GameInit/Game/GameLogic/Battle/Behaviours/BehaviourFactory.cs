using GameHotfix.Framework;

namespace GameInit.Game
{
    public static class BehaviourFactory
    {
        public static BTAction CreateBehaviourTree(FighterUnit unit)
        {
            var action = new BTActionPrioritizedSelector();
            switch (unit.UnitType)
            {
                case UnitType.Role:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 等待复活
                        .AddChild(new NOD_WaitRevive().SetPrecondition(new CON_WaitRevive()))
                        // 复活
                        .AddChild(new NOD_Revive().SetPrecondition(new CON_Revive()))
                        // 击退
                        .AddChild(new NOD_HitBack().SetPrecondition(new CON_HasHitBack()))
                        // 修改模型
                        .AddChild(new NOD_ChangeRole().SetPrecondition(new CON_ChangeRole()))
                        // 升级
                        .AddChild(new NOD_UpGrade().SetPrecondition(new CON_HasUpgrade()))
                        // 被动技能
                        .AddChild(new NOD_PassiveSkillAction().SetPrecondition(new CON_HasPassiveSkill()))
                        // 休息
                        .AddChild(new NOD_RelaxAction().SetPrecondition(new CON_HasRelax()))
                        // 移动
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasMoving())
                            .AddChild(new NOD_MoveAction()))
                        // 采集
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasCollect())
                            .AddChild(new NOD_TurnAction())
                            .AddChild(new NOD_CollectAction()))
                        // 攻击
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasAttack())
                            .AddChild(new NOD_TurnAction())
                            .AddChild(new NOD_SkillAction()))
                        // 受击
                        .AddChild(new NOD_HitAction().SetPrecondition(new CON_HasHit()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.SmallWerewolf:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 移动
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasMoving())
                            .AddChild(new NOD_MoveAction()))
                        // 攻击
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasAttack())
                            .AddChild(new NOD_TurnAction())
                            .AddChild(new NOD_SkillAction()))
                        // 受击
                        .AddChild(new NOD_HitAction().SetPrecondition(new CON_HasHit()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.Monster:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 攻击
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasAttack())
                            .AddChild(new NOD_TurnAction())
                            .AddChild(new NOD_SkillAction()))
                        // 受击
                        .AddChild(new NOD_HitAction().SetPrecondition(new CON_HasHit()))
                        // 移动
                        .AddChild(new BTActionSequence()
                            .SetPrecondition(new CON_HasMoving())
                            .AddChild(new NOD_MoveAction()))
                        // 巡逻
                        .AddChild(new NOD_MonsterPatrol().SetPrecondition(new CON_HasMonsterPatrol()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.MonsterGuard:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 攻击
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasAttack())
                            .AddChild(new NOD_TurnAction())
                            .AddChild(new NOD_SkillAction()))
                        // 受击
                        .AddChild(new NOD_HitAction().SetPrecondition(new CON_HasHit()))
                        // 移动
                        .AddChild(new BTActionSequence()
                            .SetPrecondition(new CON_HasMoving())
                            .AddChild(new NOD_MoveAction()))
                        // 移动到出生点
                        .AddChild(new Node_MoveToStartPos().SetPrecondition(new CON_HasMovToStartPos()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.Boss:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 修改模型
                        .AddChild(new NOD_ChangeRole().SetPrecondition(new CON_ChangeRole()))
                        // 升级
                        .AddChild(new NOD_UpGrade().SetPrecondition(new CON_HasUpgrade()))
                        // 无敌idle
                        .AddChild(new  NOD_BossInvincibleIdle().SetPrecondition(new CON_HasBossInvincibleIdle()))
                        // 移动
                        .AddChild(new BTActionSequence().SetPrecondition(new CON_HasMoving())
                            .AddChild(new NOD_MoveAction()))
                        // 攻击
                        .AddChild(new NOD_BossSkillAction().SetPrecondition(new CON_HasBossAttack()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.MonsterBoss:
                case UnitType.FieldBoss:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 修改模型
                        .AddChild(new NOD_ChangeRole().SetPrecondition(new CON_ChangeRole()))
                        // 升级
                        .AddChild(new NOD_UpGrade().SetPrecondition(new CON_HasUpgrade()))
                        // 无敌idle
                        .AddChild(new  NOD_BossInvincibleIdle().SetPrecondition(new CON_HasBossInvincibleIdle()))
                        // 攻击
                        .AddChild(new NOD_BossSkillAction().SetPrecondition(new CON_HasBossAttack()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.Pet:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()))
                        // 出生动画
                        .AddChild(new NOD_ComingAction().SetPrecondition(new  CON_HasComing()))
                        // 击退
                        .AddChild(new NOD_HitBack().SetPrecondition(new CON_HasHitBack()))
                        // 修改模型
                        .AddChild(new NOD_ChangeRole().SetPrecondition(new CON_ChangeRole()))
                        // 跟随主人
                        .AddChild(new NOD_FollowOwner().SetPrecondition(new CON_FollowOwner()))
                        // idle
                        .AddChild(new NOD_IdleAction());
                    break;
                case UnitType.Fruit:
                    action
                        // 死亡
                        .AddChild(new NOD_DeathAction().SetPrecondition(new CON_HasDeath()));
                    break;

            }
            return action;
        }
    }
}