using GameHotfix.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class FighterBehaviourData : BTWorkingData
    {
        public float DeltaTime;

        // 战斗单位
        public FighterUnit Self { get; }

        // 战斗单位数据
        public FighterUnitData SelfData { get; }

        // 行为类型
        public FighterBehaviorType BehaviorType { get; protected set; }

        // 进化欲望
        public UpgradeDesire UpgradeDesire { get; protected set; }

        // 休息时间
        public float RelaxTime { get; protected set; }

        public FighterBehaviourData(FighterUnit self)
        {
            Self = self;
            SelfData = self.FighterData;
            Init();
        }

        private void Init()
        {
            IsDeath = false;
        }

        // 行为类型
        public void SetBehaviorType(FighterBehaviorType type)
        {
            BehaviorType = type;
        }

        #region 进化欲望
        // 初始化
        public void InitUpgradeDesire()
        {
            // 随机欲望
            RandomUpgradeDesire();
            
            // // 大于最低等级直接旺盛
            // if (Self.FighterData.Level > GameConstantHelper.UpgradeDesireLowLevel)
            // {
            //     SetUpgradeDesire(UpgradeDesire.Vigorous,0);
            // }
            // else
            // {
            //     // 随机欲望
            //     RandomUpgradeDesire();
            // }

        }
        // 是否休息
        public bool IsRelax { get; private set; }
        // 是否攻击目标（用于判断是否需要休息）
        public bool IsAttack_Relax;
        // 进化欲望
        public void SetUpgradeDesire(UpgradeDesire upgradeDesire, float relaxTime)
        {
            UpgradeDesire = upgradeDesire;
            RelaxTime = relaxTime;
        }

        // 修改休息时间
        public void SetRelax(bool isRelax)
        {
            IsRelax = isRelax;
        }
        // 随机一次欲望
        public UpgradeDesire RandomUpgradeDesire()
        {
            // 平静间隔时间
            var normalTimeStr = GameConstantHelper.NormalIntervalTime;
            var normalTimes = GameConstantHelper.StringToFloatList(normalTimeStr);
            // 懒惰间隔时间
            var lazyTimeStr = GameConstantHelper.LazyIntervalTime;
            var lazyTimes = GameConstantHelper.StringToFloatList(lazyTimeStr);
            // 平静时间数量加 懒惰时间数量 加一个旺盛站位
            var count = normalTimes.Count + lazyTimes.Count + 1;
            // 随机值
            var randomValue = Random.Range(0, count);
            // 当前欲望
            UpgradeDesire upgradeDesire;
            float relaxTime = 0;
            // 平静欲望
            if (randomValue < normalTimes.Count)
            {
                upgradeDesire = UpgradeDesire.Normal;
                relaxTime = normalTimes[randomValue];
            }
            // 懒惰欲望
            else if(randomValue - normalTimes.Count < lazyTimes.Count)
            {
                upgradeDesire = UpgradeDesire.Lazy;
                relaxTime = lazyTimes[randomValue - normalTimes.Count];
            }
            // 旺盛
            else
            {
                upgradeDesire = UpgradeDesire.Vigorous;
                relaxTime = 0;
            }
            
            // 需改欲望
            SetUpgradeDesire(upgradeDesire,relaxTime);
            
            return upgradeDesire;
        }
        #endregion
        
        // 设置受击状态
        internal void SetHit()
        {
            // 处于攻击状态不能受击
            if(BehaviorType == FighterBehaviorType.Attack
               || BehaviorType == FighterBehaviorType.Collect)
                return;
            
            SetBehaviorType(FighterBehaviorType.Hit);
        }
        #region 移动相关
        public FighterUnit TargetFighter { get; private set; }
        // 设置攻击目标
        public void SetTarget(FighterUnit target)
        {
            TargetFighter = target;
        }

        public bool IsMoving(float percentage)
        {
            // 存在目标
            if (TargetFighter == null || TargetFighter.IsDeath)
                return false;

            if (TargetFighter.UnitType == UnitType.Fruit)
                // 果实不在攻击范围
                return !IsCollectRange();
            else
                // 目标不在攻击范围
                return !IsTargetInRange(percentage) ;
        }

        public bool IsLookTarget()
        {
            
            var vel = Self.GameAgent.GetVel();
            // 目标位置
            var _targetPos =vel * 5 +  Self.Position;// behaviorData.TargetFighter.Position;
            // 当前位置
            var curPos = Self.Position;
            // 自己朝向
            var forward = Self.UnitContainerTransform.forward;
            //看向目标的旋转
            var targetDir=_targetPos - curPos;
            
            return Vector3.Angle(forward,targetDir) < 2; // 完成
        }

        // 判断攻击目标攻击目标是否在移动范围里
        public bool IsTargetInRange(float percentage = 1f)
        {
            // 没有目标退出
            if (TargetFighter == null 
                || TargetFighter.UnitType == UnitType.Fruit
                || TargetFighter.IsDeath)
                return false;

            // 攻击范围
            var attackRange = SelfData.GetAttackRange(percentage);
            var pos = Self.Position;
            var target = TargetFighter.Position;
            
            // 距离
            var dis = Vector3.Distance(pos, target);
            // 减敌人的半径 
            dis = dis - TargetFighter.FighterData.ModelScale / 2;
            
            return attackRange >= dis;
        }
        // 判断采集是否在采集范围
        public bool IsCollectRange()
        {
            // 没有目标退出 不是果实退出
            if (TargetFighter == null 
                || TargetFighter.IsDeath
                || TargetFighter.UnitType != UnitType.Fruit)
                return false;

            // 采集范围
            var collectRange = 0.5 + Self.Radius;
            // 距离
            var pos = Self.Position;
            var target = TargetFighter.UnitContainerTransform.position;
            var dis = Vector3.Distance(pos, target);
            
            return collectRange >= dis;
        }

        #endregion

        #region 跟随
        // 是否根数
        public bool _isFollw;
        #endregion
        #region 死亡 复活
        public bool IsDeath { get; private set; }
        public bool IsDeathAnimation { get; private set; }
        public bool IsRevive { get; private set; }
        public bool IsWaitRevive { get; private set; }
        public long DeathTimer{ get; private set; }
        internal void SetDeath(bool deathAnimation = false)
        {
            IsDeath = true;
            IsDeathAnimation = deathAnimation;
        }
        // 复活动画开关
        internal void SetDeathAnimation(bool deathAnimation)
        {
            IsDeathAnimation = deathAnimation;
        }
        
        // 复活
        internal void Relive()
        {
            IsRevive = true;
            DeathTimer = -1;
        }
        // 复活结束
        internal void ReviveEnd()
        {
            IsRevive = false;
        }
        // 设置等待复活
        internal void WaitRevive(long deathTimer = -1)
        {
            IsWaitRevive = true;
            DeathTimer = deathTimer;
        }
        internal void WaitReviveEnd()
        {
            IsWaitRevive = false;
            IsDeath = false;
        }
        #endregion
        
        #region coming
        public bool IsComing{ get; private set; }
        public void SetComing(bool isComing)
        {
            IsComing = isComing;
            Self.UnitContainerTransform.gameObject.SetActive(!isComing);
        }
        #endregion

        #region 升级
        public bool IsUpgrade{ get; private set; }
        public void SetUpgrade(bool isUpgrade)
        {
            IsUpgrade = isUpgrade;
        }
        #endregion

        #region 击退

        public bool IsHitBack { get; private set; }
        public float HitBackDis{ get; private set; }
        public Vector3 HitBackDir{ get; private set; }
        public void SetHitBack(bool isHitBack, float hitBackDis, Vector3 hitDir)
        {
            // 等待复活不能击退
            if(IsWaitRevive)
                return;
            
            IsHitBack = isHitBack;
            HitBackDis = hitBackDis;
            HitBackDir = hitDir;
        }
        
        #endregion
        
        #region 击飞

        public bool IsHitFly { get; private set; }

        public void SetHitFly(bool isHitFly)
        {
            IsHitFly = isHitFly;
        }
        
        #endregion

        #region 无敌发呆
        public bool IsInvincibleIdle{ get; private set; }
        public void SetInvincibleIdle(bool isInvincibleIdle)
        {
            IsInvincibleIdle = isInvincibleIdle;
        }
        
        #endregion

        #region 修改模型
        public bool IsChangeRole{ get; private set; }
        public int RoleId;
        public float ChangeRoleTime;
        public void SetChangeRole(bool isChange, int roleId,float changeRoleTime)
        {
            IsChangeRole = isChange;
            RoleId = roleId;
            ChangeRoleTime = changeRoleTime;
        }
        #endregion
        
        
    }
    
    // 战斗单位行为
    public enum FighterBehaviorType
    {
        None,
        Idle,
        Move,
        Attack,
        Collect,
        Hit,
        PassiveSkill,
        Death,
        Revive,
        WaltRevive,
        Patrol,
    }
    // 战斗单位升级欲望
    public enum UpgradeDesire
    {
        Normal,    // 平静
        Lazy,    // 懒惰
        Vigorous,    // 旺盛
    }
}