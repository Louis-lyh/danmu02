﻿namespace GameInit.Game
{
    /// <summary>
    /// 复活行为
    /// </summary>
    public class NOD_Revive : BTActionLeaf
    {
        private float _intervalTime;
        protected override void OnEnter(BTWorkingData wData)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 间隔时间
            _intervalTime = 1f;
            // 播放动画
            behaviourData.Self.PlayAction(ActionName.Idle);
            // 行为
            behaviourData.SetBehaviorType(FighterBehaviorType.Revive);
            // 无敌状态
            behaviourData.Self.FighterData.Invincible = true;
            // 显示
            behaviourData.Self._unitObject.SetActive(true);
            // 复活
            behaviourData.Self.Revive();
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            _intervalTime -= behaviourData.DeltaTime;

            if (_intervalTime > 0)
                return BTRunningStatus.EXECUTING; // 继续
            
            // 完成
            return BTRunningStatus.FINISHED;
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 复活结束
            behaviourData.ReviveEnd();
            // 行为
            behaviourData.SetBehaviorType(FighterBehaviorType.None);
            // 无敌状态
            behaviourData.Self.FighterData.Invincible = false;
            // 修改攻击目标
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                behaviourData.Self.FighterData.SetTargetType(UnitType.Monster);
            else if(BattleManager.Instance.BattleType == BattleType.FieldBoss)
                behaviourData.Self.FighterData.SetTargetType(UnitType.FieldBoss);

            var isUpgrade = behaviourData.Self.FighterData.Exp > behaviourData.Self.FighterData.MaxHp;
            if (isUpgrade)
            {
                // 超过最大经验升级
                behaviourData.SetUpgrade(true);
                // 无敌状态
                behaviourData.Self.FighterData.Invincible = true;
            }
            
        }
    }
}