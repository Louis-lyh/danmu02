﻿using DG.Tweening;
using GameHotfix.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class NOD_ComingAction : BTActionLeaf
    {
        // 动画间隔时间
        private float _actionTime;
        //
        private Vector3 _endPos;
        protected override void OnEnter(BTWorkingData wData)
        {
            // 行为数据
            var btData = wData.As<FighterBehaviourData>();
            // 显示
            btData.Self.UnitContainerTransform.gameObject.SetActive(true);
            // 播放动画
            btData.Self.PlayAction(ActionName.Run);
            _actionTime = btData.Self.FighterData.RoleDef.ComingTime;
            var selfBody = btData.Self._unitObject.transform;
            
            // 动画
            _endPos = selfBody.localPosition;
            var startPos = _endPos - Vector3.up * (btData.Self.Radius * 3);
            selfBody.localPosition = startPos;
            selfBody.DOLocalMove(_endPos, _actionTime).SetEase(Ease.OutBack);
            
            // 生成特效
            var effectID = btData.Self.FighterData.RoleDef.ComingEffect;
            var effectScale = btData.Self.FighterData.ModelScale * Vector3.one;
            if (effectID != 0)
                Effect.CreateSceneEffect(effectID,  btData.Self.Position, effectScale,Quaternion.identity);
            
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            // 行为数据
            var btData = wData.As<FighterBehaviourData>();

            _actionTime -= btData.DeltaTime;
            if (_actionTime >= 0)
                return BTRunningStatus.EXECUTING; // 继续

            return BTRunningStatus.FINISHED; // 完成
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            // 行为数据
            var btData = wData.As<FighterBehaviourData>();
            // 重置位置
            var selfBody = btData.Self._unitObject.transform;
            selfBody.localPosition = _endPos;
            btData.SetComing(false);
            // 播放动画
            btData.Self.PlayAction(ActionName.Idle);
        }

        public override void Dispose()
        {
            base.Dispose();
        }
    }
}