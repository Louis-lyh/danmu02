﻿using DG.Tweening;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class NOD_HitBack: BTActionLeaf
    {
        // 击退速度
        private float _hitBackSpeed = 1;
        // 击退距离
        private float _hitBackDis;
        // 击退时间
        private float _hitBackTime;

        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 停止移动
            behaviorData.Self.StopMove();
            // 播放动画
            var actionName = ActionName.Idle;
            behaviorData.Self.PlayAction(actionName);
            
            // 击退距离
            _hitBackDis = behaviorData.HitBackDis;
            // 击退时间
            _hitBackTime = _hitBackDis / _hitBackSpeed;
            
            // 开始位置
            var startPos = behaviorData.Self.UnitContainerTransform.position;
            // 方向
            var dir = behaviorData.HitBackDir;
            //
            var duration = _hitBackTime;
            DOTween.To(() => 0, (dis) =>
            {
                var pos = startPos + dir * dis;
                behaviorData.Self.SetPos(pos);
            }, _hitBackDis, duration)
                .SetEase(Ease.OutQuint);
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();

            _hitBackTime -= behaviorData.DeltaTime;
            if (_hitBackTime <= 0)
                return BTRunningStatus.FINISHED; // 完成
            
            return BTRunningStatus.EXECUTING; // 继续
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 设置行为类型
            behaviourData.SetBehaviorType(FighterBehaviorType.None);
            // 关闭击退
            behaviourData.SetHitBack(false,0,Vector3.zero);
            // 行为状态
            base.OnExit(wData, runningStatus);
        }
    }
}