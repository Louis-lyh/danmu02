﻿using UnityEngine;

namespace GameInit.Game
{
    public class NOD_ChangeRole : BTActionLeaf
    {
        // 修改时间
        private float _duration = 1.5f;
        // 修改模型
        private bool _isChange; 
        // 特效
        private Effect _effect;
        protected override void OnEnter(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            var unit = behaviorData.Self;
            // 播放idle
            unit.PlayAction(ActionName.Idle);
            // 停止移动
            behaviorData.Self.StopMove();
            // 设置行为状态
            behaviorData.SetBehaviorType(FighterBehaviorType.Idle);
            // 修改模型
            _isChange = false;
            // 修改时间
            _duration = 1.5f;
            // 特效
            var scale = Vector3.one;
            _effect = Effect.CreateEffect("Effect_Upgrade",unit.UnitContainerTransform,Vector3.zero,scale,Quaternion.identity,true);
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            _duration -= behaviourData.DeltaTime;
            if (_duration < behaviourData.ChangeRoleTime && !_isChange)
            {
                // 修改模型
                var unit =  behaviourData.Self;
                unit.ChangeRole(behaviourData.RoleId,unit.FighterData.Exp);
                // 已经需改
                _isChange = true;
            }

            if (_duration > 0.01f)
                return BTRunningStatus.EXECUTING; // 继续    
            else
                return BTRunningStatus.FINISHED; // 结束
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 关闭无敌状态
            behaviourData.Self.FighterData.Invincible = false;
            // 关闭升级
            behaviourData.SetChangeRole(false,-1,0);
            // 销毁
            _effect?.Dispose();
        }
    }
}