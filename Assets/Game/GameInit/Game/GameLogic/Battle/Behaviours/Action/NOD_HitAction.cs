﻿using GameHotfix.Framework;
using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 受击行为
    /// </summary>
    public class NOD_HitAction : BTActionLeaf
    {
        // 受击结束
        private float _hitEndTime;
        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 停止移动
            behaviorData.Self.StopMove();
            // 播放动画
            var actionName = ActionName.Damage;
            behaviorData.Self.PlayAction(actionName);
            // 初始化
            _hitEndTime = behaviorData.Self.ActionTimeDef.DamageEnd;
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            // 减少时间
            _hitEndTime -= wData.As<FighterBehaviourData>().DeltaTime;
            // 动画结束
            if (_hitEndTime < 0.001f)
            {
                return BTRunningStatus.FINISHED; // 完成
            }

            return BTRunningStatus.EXECUTING; // 继续
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 设置行为类型
            behaviourData.SetBehaviorType(FighterBehaviorType.None);
            // 行为状态
            base.OnExit(wData, runningStatus);
        }
    }
}