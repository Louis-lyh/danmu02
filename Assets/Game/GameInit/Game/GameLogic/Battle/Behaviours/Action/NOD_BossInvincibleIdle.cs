﻿using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// boss 无敌闲置行为
    /// </summary>
    public class NOD_BossInvincibleIdle : BTActionLeaf
    {
        // 特效
        private Effect _effect;
        // 进入
        protected override void OnEnter(BTWorkingData wData)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            //
            behaviourData.Self.PlayAction(ActionName.Idle);
            // 设置无敌
            var bossData = behaviourData.Self as FighterUnitBoss;
            bossData?.SetInvincibleIdle(true);
            // 特效
            var unitTra = behaviourData.Self.UnitContainerTransform;
            _effect = Effect.CreateEffect("Effect_Upgrade",unitTra,Vector3.zero,Vector3.one,Quaternion.identity,true);
        }
        // 执行
        protected override int OnExecute(BTWorkingData wData)
        {
            var smallWerewolfCount = BattleManager.Instance.GetFighterCount(UnitType.SmallWerewolf);

            if (smallWerewolfCount <= 0)
                return BTRunningStatus.FINISHED; // 完成
            
            return BTRunningStatus.EXECUTING; // 继续
        }
        // 退出
        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 设置无敌
            var bossData = behaviourData.Self as FighterUnitBoss;
            bossData?.SetInvincibleIdle(false);
            // 销毁
            _effect.Dispose();
        }
    }
}