﻿namespace GameInit.Game
{
    /// <summary>
    /// 被动技能
    /// </summary>
    public class NOD_PassiveSkillAction : BTActionLeaf
    {
        // 技能数据
        protected SkillData SkillData;
        // 技能
        protected SkillActionBase SkillActionBase;
        // 是否循环
        private bool _isLoopSkill;
        // 攻击触发伤害
        private float _attackToDamage;
        // 攻击结束
        private float _attackEndTime;
        // 技能
        protected SkillActionBase _skillAction;
        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 设置攻击行为
            behaviorData.SetBehaviorType(FighterBehaviorType.PassiveSkill);
            // 停止移动
            behaviorData.Self.StopMove();
           
            // 初始化
            _attackEndTime = 0;
            _attackToDamage = 0;
            // 技能数据
            SkillData = behaviorData.SelfData.GetPassiveSKill();
            if(SkillData == null)
                return;

            // 动画时间配置
            var actionTimeDef = behaviorData.Self.ActionTimeDef;
            // 动画名
            var actionName = GetAnimationName();
            // 初始化动画时间
            switch (SkillData.SkillIndex)
            {
                case 0:
                    _attackEndTime = actionTimeDef.AttackEnd1;
                    _attackToDamage = actionTimeDef.AttackToDamage1;
                    break;
                case 1:
                    _attackEndTime = actionTimeDef.AttackEnd2;
                    _attackToDamage = actionTimeDef.AttackToDamage2;
                    break;
                case 2:
                    _attackEndTime = actionTimeDef.AttackEnd3;
                    _attackToDamage = actionTimeDef.AttackToDamage3;
                    break;
            }
            
            // 播放动画
            behaviorData.Self.PlayAction(actionName);
            
            // 创建技能
            _skillAction = SkillFactory.CreateSkillActionBase(SkillData);
            // 启动技能
            OnCastSkill(behaviorData);
            // 启动技能冷却
            SkillData.StartCd();
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 每帧时间
            var deltaTime = behaviorData.DeltaTime;
            
            _attackToDamage -= deltaTime;
            // 触发攻击
            if (_attackToDamage < 0.01f)
            {
                // 处理攻击
                _skillAction?.Update(deltaTime);
            }
            
            _attackEndTime -= deltaTime;
            // 攻击结束
            if (_attackEndTime < 0.01f)
                return BTRunningStatus.FINISHED;

            // 继续
            return BTRunningStatus.EXECUTING;
        }
        // 启动技能
        protected virtual void OnCastSkill(FighterBehaviourData data)
        {
            _skillAction.Start(data.Self,data.TargetFighter);
        }
        
        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 行为状态
            behaviourData.SetBehaviorType(FighterBehaviorType.None);
            _skillAction?.Dispose();
            _skillAction = null;
            // 记录攻击过
            behaviourData.IsAttack_Relax = true;
            base.OnExit(wData, runningStatus);
        }
        // 获取动画名称
        private string GetAnimationName()
        {
            // 动画名
            var actionName = ActionName.Attack1;
            // 动画
            if (SkillData.SkillDef.AnimationType == 3)
            {
                // 初始化动画时间
                switch (SkillData.SkillIndex)
                {
                    case 0:
                        actionName = ActionName.Attack1Body;
                        break;
                    case 1:
                        actionName = ActionName.Attack2Body;
                        break;
                    case 2:
                        actionName = ActionName.Attack3Body;
                        break;
                }
            }
            else
            {
                switch (SkillData.SkillIndex)
                {
                    case 0:
                        actionName = ActionName.Attack1;
                        break;
                    case 1:
                        actionName = ActionName.Attack2;
                        break;
                    case 2:
                        actionName = ActionName.Attack3;
                        break;
                } 
            }


            return actionName;
        }

        public override void Dispose()
        {
            _skillAction?.Dispose();
            _skillAction = null;
            base.Dispose();
        }
    }
}