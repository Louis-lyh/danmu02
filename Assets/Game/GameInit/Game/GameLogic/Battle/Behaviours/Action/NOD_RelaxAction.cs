﻿using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 休息
    /// </summary>
    public class NOD_RelaxAction : BTActionLeaf
    {
        // 休息时间
        private float _relaxTIme;
        // 巡逻刷新时间
        private float _patrolRefreshTime;
        
        // 进入
        protected override void OnEnter(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            // 休息时间
            _relaxTIme = data.RelaxTime;
            // idle动画
            data.Self.PlayAction(ActionName.Idle);
        }
        // 执行
        protected override int OnExecute(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            // 减少休息时间
            _relaxTIme -= data.DeltaTime;

            if (_relaxTIme <= 0)
                return BTRunningStatus.FINISHED; // 完成
            
            _patrolRefreshTime -= data.DeltaTime;
            // 平静进化欲望 会移动
            if (_patrolRefreshTime <= 0 && data.UpgradeDesire == UpgradeDesire.Normal)
            {
                var pos = Patrol(data.Self.Position);
                // 移动
                data.Self.StartMove(pos);
                // 每隔一秒刷新一次
                _patrolRefreshTime = 1f;
                // idle动画
                data.Self.PlayAction(ActionName.Run);
            }
            return BTRunningStatus.EXECUTING; // 继续
        }
        // 退出
        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var data = wData.As<FighterBehaviourData>();
            data.SetRelax(false); // 关闭休息
        }

        private Vector3 Patrol(Vector3 curPos)
        {
            var randomDir = Random.Range(0, 4);
            var dir = Vector3.forward;
            
            switch (randomDir)
            {
                case 1 :
                    dir = Vector3.back;
                    break;
                case 2:
                    dir = Vector3.left;
                    break;
                case 3 :
                    dir = Vector3.right;
                    break;
            }

            var pos = curPos + dir * 2;
            
            // 判断是否在各自内
            if (!BattleConst.IsOffGrid(pos))
                return curPos - dir * 2;

            return pos;
        }
    }
}