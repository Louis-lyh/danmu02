using DG.Tweening;
using GameHotfix.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 死亡行为
    /// </summary>
    public class NOD_DeathAction : BTActionLeaf
    {
        private float _intervalTime;
        protected override void OnEnter(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 播放动画
            behaviorData.Self.PlayAction(ActionName.Death,false);
            // 死亡动画结束时间
            _intervalTime = behaviorData.Self.ActionTimeDef.DeathEnd;
            // 死亡行为
            behaviorData.SetBehaviorType(FighterBehaviorType.Death);
            var unit = behaviorData.Self;
            var selfBody =unit._unitObject.transform;
            // 开始位置
            var pos = selfBody.localPosition;
            // 动画
            selfBody.DOLocalMoveY(-unit.Radius * 2.5f, 1f)
                .OnComplete(() => selfBody.localPosition = pos)
                .OnKill(() => selfBody.localPosition = pos)
                .SetDelay(_intervalTime)
                .SetEase(Ease.Linear);

            _intervalTime += 1.1f;
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            
            // 死亡动画倒计时
            _intervalTime -= data.DeltaTime;
            if (_intervalTime > 0.01f)
                return BTRunningStatus.EXECUTING; // 继续

            // 完成
            return BTRunningStatus.FINISHED;
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            var unit = behaviorData.Self;
            var selfBody =unit._unitObject.transform;
            selfBody.DOKill();
            // 等待复活
            if (behaviorData.IsWaitRevive)
                behaviorData.SetBehaviorType(FighterBehaviorType.WaltRevive);
            else
                behaviorData.SetBehaviorType(FighterBehaviorType.None);
            
            // ------ 死亡动画 ------
            if(!behaviorData.IsDeathAnimation)
                return;
            // 关闭死亡动画
            behaviorData.SetDeathAnimation(false);
            
            // 死亡处理
            behaviorData.Self.RemoveByDeath();
        }
    }
}