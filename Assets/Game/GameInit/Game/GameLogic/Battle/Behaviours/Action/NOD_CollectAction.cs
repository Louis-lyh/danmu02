﻿using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    ///  采集
    /// </summary>
    public class NOD_CollectAction : BTActionLeaf
    {
        // 采集结束
        private bool _isCollectEnd;
        // 启动采集
        private bool _isCollect;
        // 采集间隔时间
        private float _interval;
        // 攻击触发伤害
        private float _attackToDamage;
        // 攻击结束时间
        private float _attackEnd;

        // 进入
        protected override void OnEnter(BTWorkingData wData)
        {
            _isCollectEnd = false;
            _isCollect = false;
            _interval = 1;
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 动画时间配置
            var actionTimeDef = behaviorData.Self.ActionTimeDef;
            _attackToDamage = actionTimeDef.AttackToDamage1;
            _attackEnd = actionTimeDef.AttackEnd1;
            // 播放动画
            behaviorData.Self.PlayAction(ActionName.Idle);
            // 显示搜集条
            BattleUIModel.Instance.SetCollectProgress(behaviorData.Self,true);
        }
        
        // 执行
        protected override int OnExecute(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            var deltaTime = data.DeltaTime;

            if ((data.TargetFighter == null || data.TargetFighter.IsDeath)
                && !_isCollect)
            {
                // 设置攻击行为
                data.SetBehaviorType(FighterBehaviorType.None);
                return BTRunningStatus.FINISHED;
            }
            // 采集倒计时
            _interval -= deltaTime;

            // 刷新进度条
            var fill = (1 - _interval) / 1;
            BattleUIModel.Instance.UpdateCollectProgress(data.Self,fill);
            
            if (_interval > 0)
                return BTRunningStatus.EXECUTING; // 继续
            
            // 采集倒计时结束
            if (!_isCollectEnd)
            {
                // 设置攻击行为
                data.SetBehaviorType(FighterBehaviorType.Collect);
                // 播放动画
                data.Self.PlayAction(ActionName.Attack1);
                // 采集倒计时结束
                _isCollectEnd = true;
            }
            
            // 攻击
            _attackToDamage -= deltaTime;
            if (_attackToDamage < 0 && !_isCollect)
            {
                // 采集
                data.TargetFighter?.DoDamage(data.Self,new DamageInfo());
                _isCollect = true;
            }
            
            // 攻击动画时间
            _attackEnd -= deltaTime;
            if (_attackEnd < 0)
                return BTRunningStatus.FINISHED; // 完成
            
            return BTRunningStatus.EXECUTING;    // 继续
        }
        
        // 退出
        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            if (_interval > 0)
            {
                Logger.LogGreen("没有采集成功");
            }
            else
            {
                Logger.LogGreen("采集成功");
            }
            var data = wData.As<FighterBehaviourData>();
            
            // 记录攻击过
            data.IsAttack_Relax = true;
            
            // 设置攻击行为
            data.SetBehaviorType(FighterBehaviorType.None);
            
            // 隐藏搜集条
            BattleUIModel.Instance.SetCollectProgress(data.Self,false);
        }
    }
}