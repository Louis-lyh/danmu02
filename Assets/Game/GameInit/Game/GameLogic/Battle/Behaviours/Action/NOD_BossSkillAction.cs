﻿using UnityEngine;

namespace GameInit.Game
{
    public class NOD_BossSkillAction : NOD_SkillAction
    {
        private Vector3 _targetPos;
        protected override void OnEnter(BTWorkingData wData)
        {
            // 看向目标
            var behaviorData = wData.As<FighterBehaviourData>();
            LookTarget(behaviorData);
            
            base.OnEnter(wData);
        }
        
        // 看向目标
        private void LookTarget(FighterBehaviourData behaviorData)
        {
            // 目标位置
            _targetPos = behaviorData.TargetFighter.Position;// behaviorData.TargetFighter.Position;
            // 当前位置
            var curPos = behaviorData.Self.Position;
            //看向目标的旋转
            var targetDir=_targetPos - curPos;
            // 新旋转
            var newRotate = Quaternion.LookRotation(targetDir,Vector3.up);
            // 旋转
            behaviorData.Self.UnitContainerTransform.localRotation = newRotate;
        }
    }
}