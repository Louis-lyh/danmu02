using GameHotfix.Framework;
using GameHotfix.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    //移动行为
    public class NOD_MoveAction : BTActionLeaf
    {
        // 移动方向
        private Vector3 _moveDirection;
        // 刷新间隔
        private float _searchInterval;
        protected override void OnEnter(BTWorkingData wData)
        {
            // 设置动画
            var behaviorData = wData.As<FighterBehaviourData>();
            //
            var unit =  behaviorData.Self;
            unit.PlayAction(ActionName.Run);
            // 开始移动
            unit.MoveToTarget();
            var moveAttribute = unit.FighterData.GetAttributeAddition(AttributeType.MoveSpeed);
            // 加速特效
            if(moveAttribute > 0)
                behaviorData.Self.AddEffect("Effect_SmokeTrailing");
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            // 行为数据
            var behaviorData = wData.As<FighterBehaviourData>();
            
             _searchInterval -= Time.deltaTime;
            if (_searchInterval > 0.0f)
                return BTRunningStatus.EXECUTING; // 继续
            // 重置
            _searchInterval = 0.3f;
            
                // 判断是否可以移动
            if (behaviorData.IsMoving(0.8f))
            {
                // 刷新目标位置
                behaviorData.Self.MoveToTarget();
                // 检查是否改变攻击目标
                ChangeTarget(behaviorData);
                return BTRunningStatus.EXECUTING; // 继续
            }
            return BTRunningStatus.FINISHED; // 完成
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            // 行为数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 移除特效
            behaviorData.Self.RemoveEffect("Effect_SmokeTrailing");
            // 停止移动
            behaviorData.Self.StopMove();
            behaviorData.SetBehaviorType(FighterBehaviorType.None);
        }

        private void ChangeTarget(FighterBehaviourData behaviorData)
        {
            // 攻击目标类型
            var targetType = behaviorData.Self.FighterData.TargetType;
            // 当前目标类型
            var targetUnitType = behaviorData.TargetFighter.UnitType;
            
            // 为小怪类型退出
            if(targetType == UnitType.Monster || targetType == UnitType.SmallWerewolf)
                return;
            
            // 攻击目标类型与目标类型不同 重置攻击目标
            if (targetType != targetUnitType)
            {
                behaviorData.SetTarget(null);
            }
        }

    }
}