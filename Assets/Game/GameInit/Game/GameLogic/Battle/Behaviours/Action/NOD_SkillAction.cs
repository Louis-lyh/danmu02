﻿using System;
using GameHotfix.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class NOD_SkillAction : BTActionLeaf
    {
        // 技能数据
        protected SkillData SkillData;
        // 技能
        protected SkillActionBase SkillActionBase;
        // 是否循环
        private bool _isLoopSkill;
        // 攻击触发伤害
        private float _attackToDamage;
        // 攻击结束
        private float _attackEndTime;
        // 技能
        protected SkillActionBase _skillAction;
        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 行为树数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 设置攻击行为
            behaviorData.SetBehaviorType(FighterBehaviorType.Attack);
            // 停止移动
            behaviorData.Self.StopMove();
           
            // 初始化
            _attackEndTime = 0;
            _attackToDamage = 0;
            // 技能数据
            SkillData = behaviorData.SelfData.GetActiveSkill();
            if(SkillData == null)
                return;

            // 动画时间配置
            var actionTimeDef = behaviorData.Self.ActionTimeDef;
            // 动画名
            var actionName = GetAnimationName();
            // 初始化动画时间
            switch (SkillData.ActionId)
            {
                case 1:
                    _attackEndTime = actionTimeDef.AttackEnd1;
                    _attackToDamage = actionTimeDef.AttackToDamage1;
                    break;
                case 2:
                    _attackEndTime = actionTimeDef.AttackEnd2;
                    _attackToDamage = actionTimeDef.AttackToDamage2;
                    break;
                case 3:
                    _attackEndTime = actionTimeDef.AttackEnd3;
                    _attackToDamage = actionTimeDef.AttackToDamage3;
                    break;
                case 4:
                    _attackEndTime = actionTimeDef.AttackEnd4;
                    _attackToDamage = actionTimeDef.AttackToDamage4;
                    break;
                case 5:
                    _attackEndTime = actionTimeDef.AttackEnd5;
                    _attackToDamage = actionTimeDef.AttackToDamage5;
                    break;
            }
            
            // 播放动画
            behaviorData.Self.PlayAction(actionName);
            
            // 创建技能
            _skillAction = SkillFactory.CreateSkillActionBase(SkillData);
            // 启动技能
            OnCastSkill(behaviorData);
            // 启动技能冷却
            SkillData.StartCd();
            
            var effectName = SkillData.SkillDef.PreEffect;
            // 没有特效退出
            if(string.IsNullOrEmpty(effectName))
                return;

            // 创建特效
            Effect.CreateEffect(effectName, behaviorData.Self.UnitContainerTransform, 
                Vector3.zero,Vector3.one ,Quaternion.identity,true);
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 每帧时间
            var deltaTime = behaviorData.DeltaTime;
            
            _attackToDamage -= deltaTime;
            // 触发攻击
            if (_attackToDamage < 0.01f)
            {
                // 处理攻击
                _skillAction?.Update(deltaTime);
            }
            
            _attackEndTime -= deltaTime;
            // 攻击结束
            if (_attackEndTime < 0.01f)
                return BTRunningStatus.FINISHED;

            // 继续
            return BTRunningStatus.EXECUTING;
        }
        // 启动技能
        protected virtual void OnCastSkill(FighterBehaviourData data)
        {
            _skillAction.Start(data.Self,data.TargetFighter);
        }
        
        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviourData = wData.As<FighterBehaviourData>();
            // 行为状态
            behaviourData.SetBehaviorType(FighterBehaviorType.None);
            
            // 销毁技能
            _skillAction?.Dispose();
            _skillAction = null;
            
            // 记录攻击过
            behaviourData.IsAttack_Relax = true;
            
            // 检查切换目标
            //ChangeTarget(behaviourData);
            
            base.OnExit(wData, runningStatus);
        }
        // 获取动画名称
        private string GetAnimationName()
        {
            // 动画名
            var actionName = ActionName.Attack1;
            // 动画
            if (SkillData.SkillDef.AnimationType == 3)
            {
                // 初始化动画时间
                switch (SkillData.ActionId)
                {
                    case 1:
                        actionName = ActionName.Attack1Body;
                        break;
                    case 2:
                        actionName = ActionName.Attack2Body;
                        break;
                    case 3:
                        actionName = ActionName.Attack3Body;
                        break;
                    case 4:
                        actionName = ActionName.Attack4Body;
                        break;
                    case 5:
                        actionName = ActionName.Attack5Body;
                        break;
                }
            }
            else
            {
                switch (SkillData.ActionId)
                {
                    case 1:
                        actionName = ActionName.Attack1;
                        break;
                    case 2:
                        actionName = ActionName.Attack2;
                        break;
                    case 3:
                        actionName = ActionName.Attack3;
                        break;
                    case 4:
                        actionName = ActionName.Attack4;
                        break;
                    case 5:
                        actionName = ActionName.Attack5;
                        break;
                } 
            }


            return actionName;
        }

        public override void Dispose()
        {
            _skillAction?.Dispose();
            _skillAction = null;
            base.Dispose();
        }
    }
}