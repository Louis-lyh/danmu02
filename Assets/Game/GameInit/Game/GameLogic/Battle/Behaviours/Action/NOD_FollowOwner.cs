﻿using GameInit.Game.Pet;
using UnityEngine;

namespace GameInit.Game
{
    public class NOD_FollowOwner : BTActionLeaf
    {
        // 间隔
        private float _interval = 0.5f;
        // 间隔事件
        private float _intervalTime = 0.5f;
        protected override void OnEnter(BTWorkingData wData)
        {
            // 设置动画
            var behaviorData = wData.As<FighterBehaviourData>();
            var unit =  behaviorData.Self;
            unit.PlayAction(ActionName.Run);
            // 主人
            var owner = (unit.FighterData as PetData)._owner;
            // 开始移动
            unit.StartMove(owner.Position);
            // 跟随
            wData.As<FighterBehaviourData>()._isFollw = true;
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            _intervalTime -= wData.As<FighterBehaviourData>().DeltaTime;
            if(_intervalTime > 0)
                return BTRunningStatus.EXECUTING; // 继续
            _intervalTime = 0.5f;
            
            var self  = wData.As<FighterBehaviourData>().Self as PetUnit;
            var owner = (self.FighterData as PetData)._owner;
            if(owner == null || owner.IsDeath)
                return BTRunningStatus.FINISHED; // 完成
            
            // 距离
            var dis = Vector3.Distance(self.Position, owner.Position);

            if (dis > _interval + self.Radius + owner.Radius)
            {
                // 开始移动
                self.StartMove(owner.Position);
                return BTRunningStatus.EXECUTING; // 继续
            }
            else 
               return BTRunningStatus.FINISHED; // 完成
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            // 行为数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 停止移动
            behaviorData.Self.StopMove();
            behaviorData.SetBehaviorType(FighterBehaviorType.None);
            // 取消跟随
            wData.As<FighterBehaviourData>()._isFollw = false;
        }
    }
}