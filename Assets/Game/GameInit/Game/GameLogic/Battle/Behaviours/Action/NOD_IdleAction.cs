using GameHotfix.Framework;
using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    ///  待机行为
    /// </summary>
    public class NOD_IdleAction : BTActionLeaf
    {
        // idle 间隔
        private float _attackInterval;
        // 进入
        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 播放idle动画
            var behaviorData = wData.As<FighterBehaviourData>();
            behaviorData.Self.PlayAction(ActionName.Idle);
            _attackInterval = 0.1f;
            // 设置行为状态
            behaviorData.SetBehaviorType(FighterBehaviorType.Idle);
        }
        // 执行
        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            _attackInterval -= behaviorData.DeltaTime;

            // 切换攻击目标
            ChangeTarget(behaviorData);
            
            if(_attackInterval >= 0.001f)
                return BTRunningStatus.EXECUTING;
            
         
            // 没有目标
            if (behaviorData.TargetFighter == null || behaviorData.TargetFighter.IsDeath)
            {
                // 寻找攻击目标
                behaviorData.Self.SearchAttackTarget();
            }
            return BTRunningStatus.FINISHED;
        }
        
        private void ChangeTarget(FighterBehaviourData behaviorData)
        {
            if (behaviorData.TargetFighter == null)
                return;

            // 攻击目标类型与攻击目标不同 重置攻击目标
            if (behaviorData.Self.FighterData.TargetType != behaviorData.TargetFighter.UnitType)
            {
                behaviorData.SetTarget(null);
            }
        }
    }
}