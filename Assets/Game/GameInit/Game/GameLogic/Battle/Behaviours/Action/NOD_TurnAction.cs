using GameHotfix.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class NOD_TurnAction : BTActionLeaf
    {
        // 旋转速度
        private float _rotateSpeed = 5;
        // 持续时间
        private float _duration;
        protected override void OnEnter(BTWorkingData wData)
        {
            // 设置动画
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 没有看向目标播放移动动画
            if(!LookTarget(behaviorData))
                behaviorData.Self.PlayAction(ActionName.Run);

            // 重置持续时间
            _duration = 0;
        }
        
        private Vector3 _targetPos;
        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();

            // 持续时间累加
            _duration += behaviorData.DeltaTime;

            if (_duration >= 5f)
            {
                // 重置攻击目标
                behaviorData.SetTarget(null);
                return  BTRunningStatus.FINISHED; // 完成
            }

            if(behaviorData.TargetFighter.UnitContainerTransform == null)
                return  BTRunningStatus.FINISHED; // 完成
            
            if(behaviorData.TargetFighter == null || behaviorData.TargetFighter.IsDeath)
                return BTRunningStatus.FINISHED; // 完成
           
            // 看向目标
            if (LookTarget(behaviorData))
                return BTRunningStatus.FINISHED; // 完成
            
            // 继续
            return BTRunningStatus.EXECUTING;
        }
        // 看向目标
        private bool LookTarget(FighterBehaviourData behaviorData)
        {
            
            // 目标位置
            _targetPos = behaviorData.TargetFighter.Position;// behaviorData.TargetFighter.Position;
            // 当前位置
            var curPos = behaviorData.Self.Position;
            // 自己朝向
            var forward = behaviorData.Self.UnitContainerTransform.forward;
            //看向目标的旋转
            var targetDir=_targetPos - curPos;
            
            if(Vector3.Angle(forward,targetDir) < 2)
                return true; // 完成
            
            // 方向插值
            Vector3 newDir =Vector3.RotateTowards(forward,targetDir,_rotateSpeed * behaviorData.DeltaTime,0f);
            // 新旋转
            var newRotate = Quaternion.LookRotation(newDir,Vector3.up);
            // 旋转
            behaviorData.Self.UnitContainerTransform.localRotation = newRotate;
            return false;
        }

    }
}