﻿using UnityEngine;

namespace GameInit.Game
{
    public class NOD_MonsterPatrol: BTActionLeaf
    {
        // 巡逻刷新时间
        private float _patrolRefreshTime;
        // 进入
        protected override void OnEnter(BTWorkingData wData)
        {
            base.OnEnter(wData);
            // 播放idle动画
            var behaviorData = wData.As<FighterBehaviourData>();
            behaviorData.Self.PlayAction(ActionName.Run);
            // 设置行为状态
            behaviorData.SetBehaviorType(FighterBehaviorType.Patrol);
        }
        // 执行
        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 有攻击目标退出
            if(behaviorData.IsTargetInRange(1f))
                return BTRunningStatus.FINISHED;
            
            _patrolRefreshTime -= behaviorData.DeltaTime;
            if (_patrolRefreshTime < 0)
            {
                var pos = Patrol(behaviorData.Self.Position);
                // 移动
                behaviorData.Self.StartMove(pos);
                // 每隔1-2刷新一次
                _patrolRefreshTime = Random.Range(1f,2f);
            }
            
            return BTRunningStatus.EXECUTING;
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 设置行为状态
            behaviorData.SetBehaviorType(FighterBehaviorType.None);
            // 停止移动
            behaviorData.Self.StopMove();
        }

        // 巡逻位置
        private Vector3 Patrol(Vector3 curPos)
        {
            var randomAngle = Random.Range(0, 360);
            var dir = Quaternion.Euler(Vector3.up * randomAngle) *  Vector3.forward;
            dir = dir.normalized;
            
            var pos = curPos + dir * 2;
            
            // 判断是否在各自内
            if (!BattleConst.IsOffGrid(pos))
                return curPos - dir * 2;

            return pos;
        }
    }
}