﻿namespace GameInit.Game
{
    /// <summary>
    /// 等待复活行为
    /// </summary>
    public class NOD_WaitRevive: BTActionLeaf
    {
        protected override void OnEnter(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 播放动画
            behaviorData.Self.PlayAction(ActionName.Death,false);
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 启动复活结束
            if (behaviorData.IsRevive)
                return BTRunningStatus.FINISHED;
            
            if(behaviorData.DeathTimer == -1)
                return BTRunningStatus.EXECUTING;
            
            // 死亡时间
            var curTime = ServerTimerManager.Instance.GameTime;
            var cd = curTime - behaviorData.DeathTimer;
            
            // 没有复活继续继续
            if (cd < GameConstantHelper.FieldBossReviveCd)
                return BTRunningStatus.EXECUTING;
            else
            {
                behaviorData.Relive();
                return BTRunningStatus.FINISHED;
            }
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            behaviorData.WaitReviveEnd();
        }
    }
}