﻿using GameHotfix.Game;
using UnityEngine;

namespace GameInit.Game
{
    public class Node_MoveToStartPos : BTActionLeaf
    {
        // 移动方向
        private Vector3 _moveDirection;

        // 刷新间隔
        private float _searchInterval;

        protected override void OnEnter(BTWorkingData wData)
        {
            // 设置动画
            var behaviorData = wData.As<FighterBehaviourData>();
            //
            var unit = behaviorData.Self;
            unit.PlayAction(ActionName.Run);
            // 移动到开始位置
            unit.StartMove(unit.FighterData.StartPos);
        }

        protected override int OnExecute(BTWorkingData wData)
        {
            // 行为数据
            var behaviorData = wData.As<FighterBehaviourData>();
            //
            var unit = behaviorData.Self;
            _searchInterval -= Time.deltaTime;
            if (_searchInterval > 0.0f)
                return BTRunningStatus.EXECUTING; // 继续
            // 重置
            _searchInterval = 0.3f;

            // 判断是否可以移动
            if (Vector3.Distance(unit.FighterData.StartPos,unit.Position) > 0.5f)
            {
                return BTRunningStatus.EXECUTING; // 继续
            }

            return BTRunningStatus.FINISHED; // 完成
        }

        protected override void OnExit(BTWorkingData wData, int runningStatus)
        {
            // 行为数据
            var behaviorData = wData.As<FighterBehaviourData>();
            // 移除特效
            behaviorData.Self.RemoveEffect("Effect_SmokeTrailing");
            // 停止移动
            behaviorData.Self.StopMove();
            behaviorData.SetBehaviorType(FighterBehaviorType.None);
        }
    }
}