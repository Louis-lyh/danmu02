﻿using GameHotfix.Framework;

namespace GameInit.Game
{
    public class CON_HasComing : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var btData = wData.As<FighterBehaviourData>();
            return btData.IsComing;
        }
    }
}