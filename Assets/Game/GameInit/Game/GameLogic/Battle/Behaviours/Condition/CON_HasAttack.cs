﻿using GameHotfix.Framework;

namespace GameInit.Game
{
    public class CON_HasAttack : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 处于受击状态退出
            var isHit = behaviorData.BehaviorType == FighterBehaviorType.Hit;
            if (isHit)
                return false;
            
            // 处于移动退出
            var isMove = behaviorData.BehaviorType == FighterBehaviorType.Move;
            if (isMove)
                return false;
           
            // 处于攻击状态继续
            var isAttacking = behaviorData.BehaviorType == FighterBehaviorType.Attack;
            if (isAttacking)
                return true;

            // 技能是否准备好
            var isSkillReady = behaviorData.Self.FighterData.IsActiveSkillReady();
            // 目标在攻击范围
            if (behaviorData.IsTargetInRange() && isSkillReady)
            {
                return true;
            }
            else
                return false;
        }
    }
}