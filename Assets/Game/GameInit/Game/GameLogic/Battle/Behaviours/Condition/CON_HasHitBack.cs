﻿namespace GameInit.Game
{
    public class CON_HasHitBack: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 触发受击并且 没有处于攻击状态
            if (behaviorData.IsHitBack)
                return true;
            else
                return false;
        }
    }
}