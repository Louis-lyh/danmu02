﻿namespace GameInit.Game
{
    public class CON_ChangeRole: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var btData = wData.As<FighterBehaviourData>();
            return btData.IsChangeRole;
        }
    }
}