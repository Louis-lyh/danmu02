﻿namespace GameInit.Game
{
    public class CON_HasBossInvincibleIdle : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            // 小狼人数量大于零
            var isInvincibleIdle = BattleManager.Instance.GetFighterCount(UnitType.SmallWerewolf) > 0;

            return false;
        }
    }
}