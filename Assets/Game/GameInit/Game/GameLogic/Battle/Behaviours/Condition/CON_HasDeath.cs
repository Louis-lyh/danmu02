using GameHotfix.Framework;

namespace GameInit.Game
{
    //是否已经死亡
    public class CON_HasDeath : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 已经死亡并且不处于等待复活状态
            var isDeath = behaviorData.IsDeath 
                          && behaviorData.BehaviorType != FighterBehaviorType.WaltRevive;
            return isDeath; 
        }
    }
}