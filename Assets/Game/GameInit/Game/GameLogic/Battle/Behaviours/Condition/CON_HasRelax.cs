﻿namespace GameInit.Game
{
    /// <summary>
    ///  判断是否休息
    /// </summary>
    public class CON_HasRelax : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 角色死亡
            if (behaviorData.TargetFighter != null && behaviorData.TargetFighter.IsDeath)
            {
                // 休息时间大于零 并且攻击过
                if (behaviorData.RelaxTime > 0 && behaviorData.IsAttack_Relax)
                {
                    behaviorData.SetRelax(true);
                    behaviorData.IsAttack_Relax = false; // 重置
                }
            }
            
            // 是否需要休息
            var isRelax = behaviorData.IsRelax;
            return isRelax;
        }
    }
}