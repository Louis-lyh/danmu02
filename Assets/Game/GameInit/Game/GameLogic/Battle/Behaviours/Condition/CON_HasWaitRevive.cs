﻿namespace GameInit.Game
{
    /// <summary>
    /// 判断是否进入等待死亡状态
    /// </summary>
    public class CON_WaitRevive : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            // 是否等待复活
            var isWaitRevive = data.IsWaitRevive && data.BehaviorType == FighterBehaviorType.WaltRevive;
            return isWaitRevive;
        }
    }
}