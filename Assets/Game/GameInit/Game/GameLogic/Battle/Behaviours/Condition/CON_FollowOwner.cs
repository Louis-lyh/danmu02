﻿using GameInit.Game.Pet;
using UnityEngine;

namespace GameInit.Game
{
    public class CON_FollowOwner: BTPreconditionLeaf
    {
        // 间隔
        private float _interval = 1f;
        public override bool IsTrue(BTWorkingData wData)
        {
            var self  = wData.As<FighterBehaviourData>().Self as PetUnit;
            if (self == null || self.IsDeath)
                return false;
            
            var owner = (self.FighterData as PetData)._owner;
            if (owner == null || owner.IsDeath)
                return false;
            
            // 距离
            var dis = Vector3.Distance(self.Position, owner.Position);
            // 距离大于间隔加半径移动
            return dis > _interval + self.Radius + owner.Radius ||  wData.As<FighterBehaviourData>()._isFollw;
        }
    }
}