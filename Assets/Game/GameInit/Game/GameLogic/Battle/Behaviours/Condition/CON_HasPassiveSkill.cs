﻿namespace GameInit.Game
{
    public class CON_HasPassiveSkill: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 处于受击状态退出
            var isHit = behaviorData.BehaviorType == FighterBehaviorType.Hit;
            if (isHit)
                return false;
           
            // 处于攻击状态继续
            var isAttacking = behaviorData.BehaviorType == FighterBehaviorType.Attack;
            if (isAttacking)
                return false;
            
            // 处于被动状态继续
            var isPassiveSkill = behaviorData.BehaviorType == FighterBehaviorType.PassiveSkill;
            if (isPassiveSkill)
                return true;
            
            // 被动技能是否准备好
            var isSkillReady = behaviorData.Self.FighterData.IsPassiveSkillReady();
            // 目标在攻击范围
            if (isSkillReady)
            {
                return true;
            }
            else
                return false;
        }
    }
}