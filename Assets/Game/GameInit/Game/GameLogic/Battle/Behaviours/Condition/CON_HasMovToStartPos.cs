﻿using UnityEngine;

namespace GameInit.Game
{
    public class CON_HasMovToStartPos: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            var unit = behaviorData.Self;
            // 判断是否处于不能打断阶段
            var isMove = behaviorData.BehaviorType != FighterBehaviorType.Attack
                         && behaviorData.BehaviorType != FighterBehaviorType.Hit; 
            
            
            if (Vector3.Distance(unit.FighterData.StartPos,unit.Position) > 0.5f)
                return true;
            else
                return false;
        }
    }
}