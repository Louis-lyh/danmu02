﻿namespace GameInit.Game
{
    public class CON_HasCollect : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 处于受击状态退出
            var isHit = behaviorData.BehaviorType == FighterBehaviorType.Hit;
            if (isHit)
                return false;
            
            var isCollect =  behaviorData.BehaviorType == FighterBehaviorType.Collect;
            if (isCollect)
                return true;
            
            // 判断采集是否在范围内
            if (behaviorData.IsCollectRange())
                return true;
            
            return false;
        }
    }
}