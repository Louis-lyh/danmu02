﻿namespace GameInit.Game
{
    public class CON_HasBossAttack: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();

            // 处于攻击状态继续
            var isAttacking = behaviorData.BehaviorType == FighterBehaviorType.Attack;
            if (isAttacking)
                return true;

            // 技能是否准备好
            var isSkillReady = behaviorData.Self.FighterData.IsActiveSkillReady();
            // 目标在攻击范围
            if (behaviorData.IsTargetInRange() && isSkillReady)
            {
                return true;
            }
            else if(!behaviorData.IsTargetInRange())
            {
                // 不在攻击范围重置目标
                behaviorData.SetTarget(null);
            }
            return false;
        }
    }
}