﻿namespace GameInit.Game
{
    /// <summary>
    /// 是否能复活
    /// </summary>
    public class CON_Revive: BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var data = wData.As<FighterBehaviourData>();
            return data.IsRevive;
        }
    }
}