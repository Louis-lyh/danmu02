using GameHotfix.Framework;

namespace GameInit.Game
{
    //是否已经移动
    public class CON_HasMoving : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            
            // 处于移动退出
            var isMoving = behaviorData.BehaviorType == FighterBehaviorType.Move;
            if (isMoving)
                return true;
            
            // 判断是否处于不能打断阶段
            var isMove = behaviorData.BehaviorType != FighterBehaviorType.Attack
                         && behaviorData.BehaviorType != FighterBehaviorType.Hit; 
            
            
            if (behaviorData.IsMoving(0.8f) && isMove)
            {
                behaviorData.SetBehaviorType(FighterBehaviorType.Move);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}