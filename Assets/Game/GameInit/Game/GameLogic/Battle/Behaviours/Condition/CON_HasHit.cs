﻿using GameHotfix.Framework;

namespace GameInit.Game
{
    public class CON_HasHit  : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 正在处于hit状态
            var isHit = behaviorData.BehaviorType == FighterBehaviorType.Hit;
            // 触发受击并且 没有处于攻击状态
            if (isHit)
            {
                return true;
            }
            else
                return false;
        }
    }
}