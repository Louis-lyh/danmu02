﻿namespace GameInit.Game
{
    public class CON_HasMonsterPatrol : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var behaviorData = wData.As<FighterBehaviourData>();
            // 有攻击目标退出
            if(behaviorData.IsTargetInRange(1f))
                return false;
            return true;
        }
    }
}