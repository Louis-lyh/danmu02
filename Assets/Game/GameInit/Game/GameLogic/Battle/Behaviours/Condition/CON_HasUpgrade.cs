﻿namespace GameInit.Game
{
    public class CON_HasUpgrade : BTPreconditionLeaf
    {
        public override bool IsTrue(BTWorkingData wData)
        {
            var btData = wData.As<FighterBehaviourData>();
            return btData.IsUpgrade && !btData.IsDeath;
        }
    }
}