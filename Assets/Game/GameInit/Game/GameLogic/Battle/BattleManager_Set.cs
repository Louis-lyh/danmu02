﻿using System.Linq;

namespace GameInit.Game
{
    public partial class BattleManager
    {
        #region Set
        // 设置所有玩家攻击目标
        public void SetAllFighterAttackType(UnitType type)
        {
            // 遍历英雄
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];

                role.FighterData?.SetTargetType(type);
            }
        }
        // 所有玩家停止攻击
        public void SetAllFighterAttackStop()
        {
            // 遍历英雄
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];

                role.BehaviorData.SetTarget(null);
            }
        }
        
        // 设置所有英雄生命值
        public void SetAllFighterHp(int value,float percent)
        {
            // 遍历英雄
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];

                role.FighterData?.RestoreHp(value,percent);
            }
        }
        #endregion
    }
}