using System.Collections.Generic;

namespace GameInit.Game
{
    public partial class BattleUIModel
    {
        // 打开胜利排行榜
        public void OpenVictoryRanking(List<VictoryRankingInfo> infos,VictoryRankingType type)
        {
            // 胜利信息
            VictoryInfo victoryInfo = new VictoryInfo();
            victoryInfo.VictoryRankingInfos = infos;
            victoryInfo.VictoryRankingType = type;
            
            // 打开胜利排行榜界面
            DispatchEvent(EventShowVictoryRanking,victoryInfo);
        }
        // 刷新boss血条
        public void UpdateBossBlood(FighterUnitData hp)
        {
            DispatchEvent(EventUpDateBossHp,hp);
        }

        // 刷新战斗单位血条
        public void UpdateUnitBlood(FighterUnit info)
        {
            // 刷新
            _refreshUnitBlood?.Invoke(info);
        }
        // 移除血条
        public void DeleteUnitBlood(long unitId)
        {
            // 删除
            _deleteUnitBlood?.Invoke(unitId);
        }
        
        // 刷新搜集条
        public void UpdateCollectProgress(FighterUnit unitData, float fill)
        {
            _updateCollectProgress?.Invoke(unitData,fill);
        }
        
        // 显示搜集条
        public void SetCollectProgress(FighterUnit unitData, bool isShow)
        {
            _setCollectProgress?.Invoke(unitData,isShow);
        }

    }
}

