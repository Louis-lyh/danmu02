using System;
using System.Collections;
using System.Collections.Generic;
using GameHotfix.Framework;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Game
{
    public partial class BattleUIModel : DataModelBase<BattleUIModel>
    {
        // UI相机
        public Camera UICamera;
        // 打开狼人界面 
        public const string EventOpenBossTips = "Event_OpenTips";
        // 打开boss声波技能界面
        public const string EventOpenBossSkill = "Event_BossSkill";
        // 取消boss技能
        public const string EventCancelSkillProgress = "Event_CancelSkillProgress";
        
        // 刷新玩家排行榜
        public const string EventUpdatePlayerRanking = "Event_UptatePlayerRanking";
        // 刷新boss排行榜
        public const string EventUpdateBossRanking = "Event_UpdateBossRamking";
        // 刷新玩家复活次数
        public const string EventUpdatePlayerHp = "Event_UpdatePlayerHp";
        
        // 刷新bossHp
        public const string EventUpDateBossHp = "Event_UpdateBossHp";
        // 显示boss血条 
        public const string EventShowBossHp = "Event_ShowBossHp";
        // 显示boss声波技能icon
        public const string EventFreeSonicSkillIcon = "Event_FreeSonicSkillIcon";
        // 显示boss小狼人icon
        public const string EventFreeLittleWerewolfSkill = "Event_FreeLittleWerewolfSkill";
        
        // 显示胜利排行榜
        public const string EventShowVictoryRanking = "Event_ShowVictoryRanking";
        
        // 刷新积分
        public const string EventShowScorePool = "Event_ShowScorePool";
        
        // 刷新时间
        public const string EventRefreshGameTime = "Event_RefreshGameTime";

        // 初始化
        public void Init()
        {
        }
        
        // 刷新血条
        private Action<FighterUnit> _refreshUnitBlood;
        public void AddRefreshUnitBlood(Action<FighterUnit> callBack)
        {
            _refreshUnitBlood = callBack;
        }
        public void RemoveRefreshUnitBlood()
        {
            _refreshUnitBlood = null;
        }

        // 删除血条
        private Action<long> _deleteUnitBlood;
        public void AddDeleteUnitBlood(Action<long> callBack)
        {
            _deleteUnitBlood = callBack;
        }
        public void RemoveDeleteUnitBlood()
        {
            _deleteUnitBlood = null;
        }
        
        // 显示搜集进度
        private Action<FighterUnit,bool> _setCollectProgress;
        public void AddSetCollectProgress(Action<FighterUnit,bool> callBack)
        {
            _setCollectProgress = callBack;
        }
        public void RemoveSetCollectProgress()
        {
            _setCollectProgress = null;
        }
        
        // 显示搜集进度
        private Action<FighterUnit,float> _updateCollectProgress;
        public void AddUpdateCollectProgress(Action<FighterUnit,float> callBack)
        {
            _updateCollectProgress = callBack;
        }
        public void RemoveUpdateCollectProgress()
        {
            _updateCollectProgress = null;
        }


    }
    
    // 胜利信息
    public class VictoryInfo
    {
        public VictoryRankingType VictoryRankingType;
        public List<VictoryRankingInfo> VictoryRankingInfos;
    }


    // 胜利排行榜信息
    public class VictoryRankingInfo
    {
        public string Name;
        public string HeadUrl;
        public int Order;
        public int Level;
        public string Species;
        public long CurScore;
        public long MonthlyScore;
        public int MonthlyOrder;
        public int WinStreak;
    }
    // 排行
    public enum VictoryRankingType
    {
        Werewolf,    // 狼人排行榜
        SmallAnimals,// 小动物排行榜
    }
    // 战斗单位血条信息
    public class UnitBloodInfo
    {
        public string Name;    // 名字
        public long UnitId;    // id
        public float BloodRatio;    // 血量比例
        public UnitType UnitType;    // 战斗单位
    }

}


