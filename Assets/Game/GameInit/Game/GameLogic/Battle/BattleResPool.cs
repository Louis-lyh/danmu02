using System.Collections.Generic;
using Cysharp.Threading.Tasks;
// using dnlib.DotNet.Resources;
using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public static class BattleResPool
    {
        // 游戏对象池
        private static readonly Dictionary<string, Queue<GameObject>> UnitObjectPool = new Dictionary<string, Queue<GameObject>>();
        // 材质球
        private static readonly Dictionary<string,List<Material>> ActionMatPool = new Dictionary<string,List<Material>>();

        #region 游戏对象池
        public static async UniTask<GameObject> ConstructObjectAsync(string path)
        {
            if (UnitObjectPool.TryGetValue(path, out var objects) && objects.Count > 0)
                return objects.Dequeue();
            var res = await ResourceManager.ConstructObjAsync(path);
            return res;
        }
        public static void CollectObject(string path, GameObject gameObject)
        {
            if (gameObject == null)
                return;
            
            if (!UnitObjectPool.TryGetValue(path, out var objects))
            {
                objects = new Queue<GameObject>();
                UnitObjectPool.Add(path, objects);
            }

            if (objects.Count > 20)
            {
                ResourceManager.CollectObj(gameObject, true);
                return;
            }

            objects.Enqueue(gameObject);
            gameObject.SetActive(false);
            gameObject.transform.SetParent(ResourceInfo.Parent, false);
        }
        #endregion

        #region 动画材质球对象池
        // 加载材质球
        public static Material ConstructMatAsync(string path)
        {
            // 存在且数量大于一返回一个
            if (ActionMatPool.ContainsKey(path))
            {
                var mats = ActionMatPool[path];
                Material mat = null;
                if (mats.Count == 1)
                {
                    mat = new Material(mats[0]);
                    mat.name = mats[0].name;
                }
                else if(mats.Count > 1)
                {
                    mat = mats[mats.Count - 1];
                    mats.Remove(mat);
                }
                else
                {
                    Logger.LogError("材质球对象池错误");
                }
                
                return mat;
            }
            else
            {
                // 加载一个
                var firstMat = ResourceManager.LoadAssetSync<Material>(path);
                if (firstMat == null)
                {
                    Logger.LogError($"firstMat == null {path}");
                    return null;
                }
                // 新建对象池
                List<Material> newPool = new List<Material>();
                newPool.Add(firstMat);
                // 加入对象池字典
                ActionMatPool.Add(path,newPool);
                // 返回一个新的
                var mat = new Material(firstMat);
                mat.name = firstMat.name;
                return mat;
            }
        }
        // 回收材质球
        public static void CollectMat(string path,Material mat)
        {
            // 为空退出
            if (mat == null)
                return;
            
            // 没有对象池新加
            if (!ActionMatPool.TryGetValue(path, out var mats))
            {
                mats = new List<Material>();
                ActionMatPool.Add(path, mats);
            }
            // 加入对象池
            mats.Add(mat);
        }

        #endregion
        
        
     

        public static void ReleaseBattleRes()
        {
            foreach (var kv in UnitObjectPool)
            {
                while (kv.Value.Count > 0)
                    ResourceManager.CollectObj(kv.Value.Dequeue(), true);
            }
            UnitObjectPool.Clear();
        }
    }
}