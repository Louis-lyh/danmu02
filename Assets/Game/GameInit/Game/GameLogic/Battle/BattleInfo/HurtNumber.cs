/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：
+-----------------------------------------------------------------------------*/

using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Cysharp.Text;

namespace GameInit.Game
{
    public class HurtNumber : XObject
    {
        private GameObject _obj;
        private Text _text;

        public GameObject Obj
        {
            get { return _obj; }
        }

        public HurtNumber(GameObject obj, bool violence, int damage, Vector3 position, Vector3 textPos, float initScale, float riseDis)
        {
            _obj = obj;

            _text = _obj.transform.Find("Hurt").GetComponent<Text>();
            _text.transform.localPosition = textPos;
            _text.transform.localScale = new Vector3(initScale, initScale, initScale);
            
            //暴击伤害
            if (violence)
            {
                _text.text = damage.ToString();

                Color color;
                ColorUtility.TryParseHtmlString("#ea0d01", out color);
                _text.color = color;
            }
            //普通伤害
            else
            {
                _text.text = damage.ToString();
                
                _text.color = Color.white;
            }
            
            SetPosition(position);

            float enlargeScale = initScale * 1.15f;
            float narrowScale = initScale * 0.9f;
            PlayAnim(enlargeScale, narrowScale, riseDis);
        }
        
        //销毁
        protected override void OnDispose()
        {
            
        }

        //位置设置
        private void SetPosition(Vector3 position)
        {
            Vector2 pos = WorldToCanvasPoint(BattleInfoManager.Instance.Camera, BattleInfoManager.Instance.Canvas, position);
            _obj.transform.localPosition = pos;
        }

        //动画
        private void PlayAnim(float enlargeScale, float narrowScale, float riseDis)
        {
            _text.transform.DOScale(new Vector3(enlargeScale, enlargeScale, enlargeScale), 0.15f);
            _text.transform.DOLocalMove(_text.transform.localPosition + Vector3.up * riseDis, 0.2f).OnComplete(() =>
            {
                _text.transform.DOScale(new Vector3(narrowScale, narrowScale, narrowScale), 0.08f).OnComplete(() =>
                {
                    BattleInfoManager.Instance.RecoveryHurtNumber(this);
                });
            });
        }
        
        //坐标转换
        private Vector2 WorldToCanvasPoint(Camera camera, Canvas canvas, Vector3 worldPos)
        {
            var screenPoint = RectTransformUtility.WorldToScreenPoint(camera, worldPos);
            Plane cameraPlane = new Plane(camera.transform.forward, camera.transform.position);
            if (!cameraPlane.SameSide(camera.transform.forward + camera.transform.position, worldPos))
            {
                screenPoint = -screenPoint;
            }

            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, screenPoint,
                canvas.worldCamera, out pos);
            return pos;
        }
    }
}