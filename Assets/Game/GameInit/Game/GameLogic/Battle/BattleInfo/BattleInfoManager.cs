/*------------------------------------------------------------------------------
|AUTHOR: ONE PUNCH-MAN
|CREATION TIME：2022-11-29
|FUNCTION：Manage some battle information, such as blood bar damage numbers
+-----------------------------------------------------------------------------*/

using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Game
{
    public sealed class BattleInfoManager : Singleton<BattleInfoManager>
    {
        //伤害Root
        private GameObject _hurtRoot;
        public GameObject HurtRoot => _hurtRoot;
        
        //Camera
        private Camera _camera;
        public Camera Camera => _camera;
        
        //Canvas
        private Canvas _canvas;
        public Canvas Canvas => _canvas;

        //初始化
        public void Init()
        {
            //得到伤害Root
            if (_hurtRoot == null)
            {
                _hurtRoot = GameObject.Find("BattleInfo/Battle Info Canvas/BattleInfoHurtRoot");
            }
            //得到Camera
            if (_camera == null)
            {
                _camera = Camera.main;
            }
            //得到Canvas
            if (_canvas == null)
            {
                _canvas = GameObject.Find("BattleInfo/Battle Info Canvas").GetComponent<Canvas>();
            }
            
            //构建伤害数字对象池
            BuildHurtNumberPool();
        }
        
        #region 伤害数字
        //伤害数字对象池
        private ObjectPool _hurtNumberPool;

        private List<HurtNumber> _hurtNumberList = new List<HurtNumber>();

        //构建伤害数字对象池
        private void BuildHurtNumberPool()
        {
            if (_hurtNumberPool != null)
                return;
            
            _hurtNumberPool = ObjectPoolManager.Instance.GetPool("GamePlay/HurtNumber", 50);
        }
        
        //产生一个伤害数字
        public void ProduceHurtNumber(bool violence, int damage, Vector3 position, Vector3 textPos, float initScale, float riseDis)
        {
            GameObject obj = _hurtNumberPool.Spawn();
            obj.transform.SetParent(_hurtRoot.transform, false);

            HurtNumber hurtNumber = new HurtNumber(obj, violence, damage, position, textPos, initScale, riseDis);
            _hurtNumberList.Add(hurtNumber);
        }
        
        //销毁
        public void RecoveryHurtNumber(HurtNumber hurtNumber)
        {
            _hurtNumberList.Remove(hurtNumber);
            
            hurtNumber.Dispose();
            
            _hurtNumberPool.Unspawn(hurtNumber.Obj);
        }
        #endregion
    }
}