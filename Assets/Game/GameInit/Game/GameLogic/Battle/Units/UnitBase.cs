using Cysharp.Threading.Tasks;
using GameHotfix.Framework;
using GameInit.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public abstract class UnitBase : UpdateBase
    {
        // 模型路径
        protected string _modelResPath;
        // 角色模型
        public GameObject _unitObject{ get; protected set; }
        // 角色数据
        protected UnitDataBase _unitDataBase;
        // 类型
        public UnitType UnitType { get; private set; }
        // 根节点
        public Transform UnitContainerTransform { get; private set; }
        // 位置
        public Vector3 Position { get=>UnitContainerTransform.position; }

        protected UnitBase(UnitType type)
        {
            UnitType = type;
        }

        protected virtual void Init(UnitDataBase data)
        {
            // 数据
            _unitDataBase = data;
        }

        // 初始化
        public async UniTask CreatUnit<T>(T data) where T : UnitDataBase
        {
            // 初始化
            Init(data);
            // 获取模型路径
            _modelResPath = GetUnitModelPath();
            // 根节点
            var container = new GameObject();
            UnitContainerTransform = container.transform;
            // 加载模型
            _unitObject = await BattleResPool.ConstructObjectAsync(_modelResPath);
            if (_unitObject == null)
            {
                Logger.LogError("UnitBase.Initialize() => 加载场景单位模型失败, _type:" + UnitType + ", uid:" + _unitDataBase.UnitID + ", configId:" + _unitDataBase.UnitConfigID);
                return;
            }
            // 初始化模型位置信息
            _unitObject.transform.SetParent(UnitContainerTransform, false);
            _unitObject.transform.localPosition = Vector3.zero;
            _unitObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
            _unitObject.SetActive(true);
            // 模型加载完毕
            await OnUnitModelLoaded();
            // 启动
            OnInitialize();
        }
        // 设置父对象
        public void SetParent(Transform parent, Vector3 pos, Vector3 localEulerAngles = default)
        {
            if (UnitContainerTransform == null)
                return;
            
            UnitContainerTransform.SetParent(parent, false);
            UnitContainerTransform.localPosition = pos;
            UnitContainerTransform.localEulerAngles = localEulerAngles;
            OnAddToStage();
        }

        protected abstract UniTask OnUnitModelLoaded();
        protected virtual string GetUnitModelPath()
        {
            return string.Empty;
        }
        protected virtual void OnAddToStage(){}

        public T GetUnitData<T>() where T : UnitDataBase
        {
            return (T) _unitDataBase;
        }

        protected override void OnDispose()
        {
            // 回收对象数据
            BattleResPool.CollectObject(_modelResPath, _unitObject);
            // 删除根节点
            if(UnitContainerTransform != null)
                GameObject.Destroy(UnitContainerTransform.gameObject);
            UnitContainerTransform = null;
            _unitDataBase = null;
            _unitObject = null;
            base.OnDispose();
        }
    }
}