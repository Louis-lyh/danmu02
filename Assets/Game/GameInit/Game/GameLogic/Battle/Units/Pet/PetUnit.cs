﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;
using Random = UnityEngine.Random;

namespace GameInit.Game.Pet
{
    public class PetUnit : FighterUnit
    {
        public PetUnit() 
            : base(UnitType.Pet)
        {
        }
        
        public PetData PetData { get; private set; }
        
        protected override void Init(UnitDataBase data)
        {
            base.Init(data);
            PetData = FighterData as PetData;
        }
        // 加入战斗阶段
        protected override void OnAddToStage()
        {
            base.OnAddToStage();
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
           // 看向boss
            UnitContainerTransform.rotation = PetData._owner.UnitContainerTransform.rotation;
            
            // 出现动画
            BehaviorData.SetComing(true);
        }
        
        protected override void UpdateUI(float deltaTime)
        {
            if (IsDeath || FighterData == null)
                return;
            
            // 更新UI
            BattleUIModel.Instance.UpdateUnitBlood(this);
        }
        
        /// <summary>
        /// 更改模型
        /// </summary>
        public override async void ChangeRole(int roleId,int exp)
        {
            // 刷新数据
            if (PetData.UpgradeData(roleId))
            {
                // 升级模型
                await UpgradeModel(FighterData);
                // 跑马灯通知
                GameNoticeModel.Instance.SendHorseRaceLamp(PetData._owner.FighterData.Name,
                    $"获得了{FighterData.Level}级宠物《{FighterData.RoleDef.SpeciesName}》！");
            }
        }
        /// <summary>
        /// 升级模型
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        private async UniTask UpgradeModel(UnitDataBase data)
        {
            // 旧模型
            var oldObject = _unitObject;
            var oldResPath = _modelResPath;
            // 更新数据
            _unitDataBase = data;
            _modelResPath = GetUnitModelPath();
            // 加载模型
            _unitObject = await BattleResPool.ConstructObjectAsync(_modelResPath);
            if (_unitObject == null)
            {
                Logger.LogError("UnitBase.Initialize() => 加载场景单位模型失败, _type:" + UnitType + ", uid:" + _unitDataBase.UnitID + ", configId:" + _unitDataBase.UnitConfigID);
                return;
            }
            // 回收旧模型
            BattleResPool.CollectObject(oldResPath,oldObject);
            // 初始化
            _unitObject.transform.SetParent(UnitContainerTransform, false);
            _unitObject.transform.localPosition = Vector3.zero;
            _unitObject.transform.localRotation = Quaternion.Euler(Vector3.zero);

            // 缩放属性
            var scaleValue = 1 + FighterData.GetAttributeAddition(AttributeType.BodyScale) / 100;
            // 缩放
            var scale = scaleValue * FighterData.ModelScale;
            // 设置大小
            UnitContainerTransform.DOScale(scale, 1f);
            
            // 显示模型
            _unitObject.SetActive(true);
            // 刷新功能组件
            RefreshComponent();
            // 初始化
            OnInitialize();
            
            // 重置动画
            _curAniName = String.Empty;
            // 设置开始默认动画
            PlayAction(ActionName.Idle);
        }
        // 初始化数据
        protected override void PrepareData()
        {
            // 动画时间数据
            ActionTimeDef = BattleModel.Instance.GetActionTimeDef(FighterData.ModelRes);
            // 行为树数据
            BehaviorData = new FighterBehaviourData(this);
            // 创建行为树
            _behaviorTree = BehaviourFactory.CreateBehaviourTree(this);
            // 设置名称
            UnitContainerTransform.name = FighterData.Name;
            // 寻路层级
            RvoLayer = FighterData.RVOLayer;
            // 特效字典
            _dicEffect = new Dictionary<string, Effect>();
            // buff字典
            _dictBuffActionsList = new List<BuffActionBase>();
        }
        
        // 刷新数据
        private void RefreshComponent()
        {
            // 默认动画
            if (FighterData.UnitActionType == UnitAnimationType.Default)
                // 获取动画组件
                Animator = _unitObject.GetComponent<Animator>();
            else if (FighterData.UnitActionType == UnitAnimationType.Gpu)
                // 获取渲染组件
                MeshRenderer = _unitObject.GetComponentInChildren<MeshRenderer>();
            
            // 动画时间数据
            ActionTimeDef = BattleModel.Instance.GetActionTimeDef(FighterData.ModelRes);
            // 重新创建寻路
            if (RvoLayer != FighterData.RVOLayer)
            {
                RvoLayer = FighterData.RVOLayer;
                // 寻路代理
                var speed = FighterData.GetMoveSpeed() * 0.01f;
                RVOManager.Instance.SetLayer(GameAgent,Radius,speed,RvoLayer);
            }
            else
            {
                // 寻路代理
                var speed = FighterData.GetMoveSpeed() * 0.01f;
                RVOManager.Instance.SetAgent(GameAgent.Sid,Radius,speed,RvoLayer); 
            }
        }

        protected override void OnDispose()
        {
            // 移除血条
            BattleUIModel.Instance.DeleteUnitBlood(FighterData.UnitID);
            base.OnDispose();
        }
    }
}