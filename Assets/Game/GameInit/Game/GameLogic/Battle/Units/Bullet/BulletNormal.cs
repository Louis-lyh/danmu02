﻿using System.Collections.Generic;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class BulletNormal : BulletBase
    {
        // 轨道
        protected TrajectoryBase Trajectory;
        
        protected override void OnInitialize()
        {
            base.OnInitialize();
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            // 运行轨道
            Trajectory?.Update(deltaTime);
            // 计算是否击中
            CheckHitTarget(deltaTime);
        }
        
        // 计算伤害
        protected virtual void CheckHitTarget(float deltaTime){}
        
        // 设置位置
        protected virtual void SetPos(Vector3 pos)
        {
            UnitContainerTransform.position = pos;
        }
        // 设置旋转
        protected virtual void UpdateEulerAngles(Vector3 euler)
        {
            UnitContainerTransform.eulerAngles = euler;
        }
        // 击中特效
        protected virtual void HitEffect(){}

        // 子弹移动结束
        protected virtual void OnBulletMoveEnd()
        {
            RemoveBullet();
        }
    }
}