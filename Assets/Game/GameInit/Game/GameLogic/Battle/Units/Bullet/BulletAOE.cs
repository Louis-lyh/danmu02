﻿using System.Collections.Generic;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class BulletAOE : BulletNormal
    {
        // 刷新间隔时间
        private float _intervalTime = 0.05f;

        private float _intervalCount = 0;
        // 攻击过的目标
        private Dictionary<long, FighterUnit> _attackMonsters = new Dictionary<long, FighterUnit>();
        
        protected override void OnAddToStage()
        {
            // 创建轨道
            Trajectory = TrajectoryBase.CreateTrajectory(TrajectoryBase.TargetPosLinear, 
                OnBulletMoveEnd, 
                SetPos,
                UpdateEulerAngles);
            // 初始化
            UnitContainerTransform.localScale = Vector3.one;
            // 初始化名字
            UnitContainerTransform.name = _modelResPath;
            // 开始移动
            Trajectory.StartMove(Position,
                BulletData.TargetPos ,
                Targeter?.UnitContainerTransform, 
                BulletData.BulletMoveSpeed);
        }
        
        protected override void CheckHitTarget(float deltaTime)
        {
            // 攻击者死亡子弹消失
            if (Attacker == null || Attacker.IsDeath || UnitContainerTransform == null)
            {
                OnBulletMoveEnd();
                return;
            }
            
            // 刷新间隔
            _intervalCount += deltaTime;
            if(_intervalCount < _intervalTime)
                return;
            _intervalCount = 0;

            var targetsFighter = FindTarget();
            // 没有目标退出
            if (targetsFighter == null)
                return;
            // 造成伤害
            for (var i = 0; i < targetsFighter.Count; i++)
            {
                var data = targetsFighter[i].GetUnitData<FighterUnitData>();
                if (!_attackMonsters.ContainsKey(data.UnitID))
                {
                    // 造成伤害
                    DoBulletDamage(targetsFighter[i]);
                    // 击退
                    SetHitBack(targetsFighter[i], BulletData.SkillData);
                    _attackMonsters.Add(data.UnitID, targetsFighter[i]);
                }
            }
        }
        // 找寻目标
        private List<FighterUnit> FindTarget()
        {
            // 目标类型
            var targetType = BulletData.Attack.FighterData.TargetType;
            // 目标列表
            List<FighterUnit> targetList = new List<FighterUnit>();
            var radius = BulletData.SkillData.SkillDef.BulletRadius;
            switch (targetType)
            {
                case UnitType.Monster:
                case UnitType.Boss:
                case UnitType.MonsterBoss:
                case UnitType.SmallWerewolf:
                    targetList = BattleManager.Instance.FindMonsterList(Position, radius);
                    // 判断boss在不在范围内
                    FighterUnit boss = BattleManager.Instance.BossUnit;
                    if (boss != null)
                    {
                        if (Vector3.Distance(boss.Position, Position) - boss.Radius <= radius)
                            targetList.Add(boss);
                    }

                    break;
                case UnitType.Role:
                    targetList = BattleManager.Instance.FindHeroList(Position, radius);
                    break;
            }
            return targetList;
        }
        
        // 击退
        protected void SetHitBack(FighterUnit target, SkillData skillData)
        {
            var hitDir = Attacker.Position - target.Position;
            // 击退
            if(skillData.SkillDef.BeatBack > 0)
                target.SetHitBack(true,skillData.SkillDef.BeatBack,hitDir.normalized);
        }

        // 子弹移动结束
        protected override void OnBulletMoveEnd()
        {
            // 移除子弹
            RemoveBullet();
            // 清空记录
            _attackMonsters.Clear();
        }
    }
}