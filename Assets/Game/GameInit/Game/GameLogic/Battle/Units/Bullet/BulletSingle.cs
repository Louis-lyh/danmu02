﻿using System.Threading;
using UnityEngine;

namespace GameInit.Game
{
    public class BulletSingle : BulletNormal
    {
        protected override void CheckHitTarget(float deltaTime)
        {
            // 攻击者死亡子弹消失
            if (Attacker == null 
                || Attacker.IsDeath 
                || Targeter == null
                || Targeter.IsDeath
                || UnitContainerTransform == null)
            {
                OnBulletMoveEnd();
                return;
            }
            
            var dis = Vector3.Distance(Position, TargeterPos);
            // 小于半径
            if (dis >  Radius)
                return;
            
            // 触发伤害
            DoBulletDamage(Targeter);
            // 击中特效
            HitEffect();
            // 击中消失
            OnBulletMoveEnd();
        }

        protected override void OnAddToStage()
        {
            // 创建轨道
            Trajectory = TrajectoryBase.CreateTrajectory(TrajectoryBase.TargetPosLinear, 
                OnBulletMoveEnd, 
                SetPos,
                UpdateEulerAngles);
            
            // 开始移动
            var target = Targeter.UnitContainerTransform;
            Trajectory.StartMove(Position,TargeterPos ,target, BulletData.BulletMoveSpeed);
        }


        // 击中特效
        protected override void HitEffect()
        {
            // 特效id
            var effectId = BulletData.SkillData.SkillDef.TargetEffect;
            // 位置
            var pos = UnitContainerTransform.forward * Radius + Position;
            // 目标方向
            var targetDir = -UnitContainerTransform.forward;
            var rotate = AxisLookAt(Quaternion.identity,targetDir,Vector3.forward);
            // 特效比例
            var scale = Vector3.one * (Radius * 2);
            // 击中特效
            Effect.CreateSceneEffect(effectId, pos, scale,rotate);
        }
        
        // 子弹移动结束
        protected override void OnBulletMoveEnd()
        {
            // 移除子弹
            RemoveBullet();
        }
        
        /// <summary>
        /// 用某个轴去朝向指定方向
        /// </summary>
        /// <param name="rotation">自身旋转</param>
        /// <param name="targetDir">目标方向</param>
        /// <param name="directionAxis">指定的轴</param>
        /// <returns></returns>
        Quaternion AxisLookAt(Quaternion rotation,Vector3 targetDir, Vector3 directionAxis)
        {
            //指定哪根轴朝向目标,自行修改Vector3的方向
            var fromDir = rotation * directionAxis;
            
            //计算垂直于当前方向和目标方向的轴
            var axis = Vector3.Cross(fromDir, targetDir).normalized;
            
            //计算当前方向和目标方向的夹角
            var angle = Vector3.Angle(fromDir, targetDir);

            return Quaternion.AngleAxis(angle, axis) * rotation;
        }
    }
}