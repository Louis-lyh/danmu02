﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace GameInit.Game
{
    public class BulletBase : UnitBase
    {
        // 子弹数据
        protected BulletData BulletData;
        // 攻击者
        protected FighterUnit Attacker => BulletData.Attack;
        // 攻击目标
        protected FighterUnit Targeter => BulletData.Target;
        // 攻击目标
        protected Vector3 TargeterPos => BulletData.TargetPos;
        // 半径
        protected float Radius;
        
        public BulletBase() 
            : base(UnitType.Bullet)
        {
        }

        // 初始化
        protected override void Init(UnitDataBase data)
        {
            base.Init(data);
            BulletData = GetUnitData<BulletData>();
            // 半径
            Radius = BulletData.SkillData.SkillDef.BulletRadius * Attacker.FighterData.ModelScale;
        }

        protected override async UniTask OnUnitModelLoaded()
        {
            // 设置大小
            _unitObject.transform.localScale = Radius * 2 * Vector3.one;
            return;
        }
        // 模型路径
        protected override string GetUnitModelPath()
        {
            // 返回子弹路径
            return GameResPathUtils.GetBulletPrefab(BulletData.BulletRes);
        }
        // 移除子弹
        protected void RemoveBullet()
        {
            BattleManager.Instance.RemoveBullet(BulletData.UnitID);
        }
        // 创建子弹
        public static BulletBase CreateBullet(UnitDataBase dataBase)
        {
            var bulletData = dataBase as BulletData;
            switch (bulletData.BulletDamageType)
            {
                case SkillDamageType.Single:    // 单体伤害
                    return new BulletSingle();
                case SkillDamageType.AOE:    // 范围伤害
                    return new BulletAOE();
            }
            return null;
        }
        
        // 触发子弹伤害
        protected void DoBulletDamage(FighterUnit target)
        {
            // 数据为空退出
            if (BulletData == null)
                return;
            // 技能数据为空退出
            var skillData = BulletData.SkillData;
            if (skillData == null)
                return;
            
            // 触发伤害
            target.DoDamage(Attacker,Attacker.FighterData.CalcAttackDamage(skillData.SkillDef,target.FighterData.MaxHp));
        }
        
        // 获取子弹击中位置
        public static Vector3 GetBulletHitPos(int type)
        {
            var pos = Vector3.zero;
            switch (type)
            {
                case 0:
                    pos = Vector3.zero;
                    break;
                case 1:
                    pos = Vector3.up * 0.55f;
                    break;
                case 2:
                    pos = Vector3.up;
                    break;
            }
            return pos;
        }

    }
}