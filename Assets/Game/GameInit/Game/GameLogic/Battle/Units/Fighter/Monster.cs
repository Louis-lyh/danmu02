using System;
using GameInit.Framework;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameInit.Game
{
    public class Monster : FighterUnit
    {
        public Monster(UnitType unitType)
            : base(unitType)
        {
            
        }

        protected override void OnAddToStage()
        {
            base.OnAddToStage();
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            // 随机朝向
            var angles = Random.rotation.eulerAngles;
            angles.x = 0;
            angles.z = 0;
            UnitContainerTransform.rotation = Quaternion.Euler(angles);
            // 出现动画
            BehaviorData.SetComing(true);
            
            // 更改血量
            var allRoleAttackValue = BattleManager.Instance.GetAllRoleAttackValue(false);
            if(FighterData.MaxHp < 0)
                FighterData.SetHp(allRoleAttackValue * Math.Abs(FighterData.MaxHp));
        }


        public override void SearchAttackTarget()
        {
            // 判断行为数据
            if (BehaviorData == null || BehaviorData.IsDeath || UnitType == UnitType.Monster)
                return;
            // 视野范围
            var searchRange = FighterData.SearchRange;
            // 找寻目标
            var target = BattleManager.Instance.FindNearestHero(-1,-1,Position,searchRange);
            if(BehaviorData.TargetFighter == null || BehaviorData.TargetFighter.IsDeath)
                // 设置攻击目标为空 
                BehaviorData.SetTarget(target);
        }

        public override void DoDamage(FighterUnit attacker, DamageInfo damage,bool ignoreInvincible = false)
        {
            if (FighterData == null && !ignoreInvincible)
                return;
            
            // 扣血
            FighterData.DecCurHp(damage, _unitObject);
            // 闪白
            //ShinyWhite();
           
            // 小于零死亡
            if (FighterData.CurrentHp <= 0)
            {
                var exp = FighterData.ExpReward;
                // 增加经验
                attacker?.AddExp(exp);
                // 判断是否是角色
                var roleData = attacker?.FighterData as RoleData;
                if (roleData is RoleData)
                {
                    // 增加击杀积分
                    roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
                }
                
                // 增加光环经验
                var unitRole =  (attacker as FighterUnitRole);
                unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);
                // 增加宠物经验
                unitRole?.AddPetExp(FighterData.RoleDef.ExpPet);
                
                // 死亡处理
                OnDeath();
                
                // 击杀奖励
                SkillReward(attacker);
            }
            // 受击
            else 
            {
                BehaviorData.SetHit();
                // 设置新的攻击目标 
                if(BehaviorData.TargetFighter == null || BehaviorData.TargetFighter.IsDeath)
                    BehaviorData.SetTarget(attacker);
            }
        }
        // 击杀奖励
        private void SkillReward(FighterUnit fighterUnit)
        {
            var pos = Position;
            var dropId = FighterData.RoleDef.DropEwwaedId;
            if(dropId == 0)
                return;
            TimerHeap.AddTimer(500, 0, () =>
            {
                if(BattleManager.Instance.IsNight)
                    return;
                
                //创建boss奖励糖果
                FruitManager.Instance.CreateCandy_BossReward(pos,dropId,fighterUnit);
            });
        }


        protected override void OnDeath()
        {
            // 停止移动
            StopMove();
            // 设置死亡状态
            BehaviorData?.SetDeath(true);
            // 删除代理
            DeleteAgent();
            base.OnDeath();
        }

        public override void RemoveByDeath()
        {
            // 移除字典
            BattleManager.Instance.RemoveFighterUnit(FighterData);
        }
    }
}