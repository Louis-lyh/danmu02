﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class FighterUnitBoss: FighterUnit
    {
        // boss数据
        public BossData BossData { get;private set; }
        // 宠物
        private long _petUnitId;
        public FighterUnitBoss() 
            : base(UnitType.Boss)
        {
        }

        protected override void OnAddToStage()
        {
            // 设置模型缩放
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            // 设置旋转
            UnitContainerTransform.rotation = Quaternion.Euler(new Vector3(0,180,0));
            // 添加障碍
            CreateObstacle();
            // coming
            ComingAction();
            // 设置生命值
            var allRoleAttackValue = BattleManager.Instance.GetAllRoleAttackValue(true);

            FighterData.SetHp((int)(allRoleAttackValue * BossHpMultiple()));
            // boss数据
            BossData = FighterData as BossData;
            
            // 光环特效
            HaloEff();
           
        }
        // boss生命倍数
        private float BossHpMultiple()
        {
            var numStr = GameConstantHelper.BossHpMultiple.Split(',');
            var day = BattleManager.Instance.Day;

            if (day <= numStr.Length)
                return float.Parse(numStr[day - 1]);
            else
                return float.Parse( numStr[0]);
        }

        // 降临动画
        private void ComingAction()
        {
            // 开始位置 
            var endPos = UnitContainerTransform.position;
            var startPos = Vector3.up * 10 + endPos;
            UnitContainerTransform.position = startPos;
            UnitContainerTransform.DOMove(endPos, 0.5f)
                .SetEase(Ease.InCirc)
                .OnComplete(() =>
            {
                //BehaviorData.SetComing(true);
                // 启动免费技能
                //BattleModel.Instance.StartFreeBossSkill();
                // 创建宠物
                CreatePet();
            });
        }
        // 刷新UI
        protected override void UpdateUI(float deltaTime)
        {
            if(FighterData == null)
                return;
            // 计算百分比
            float curHp = FighterData.CurrentHp;
            var hp = curHp / FighterData.MaxHp;
            hp *= 10000;
            BattleUIModel.Instance.UpdateBossBlood(FighterData);
        }
        #region 经验升级
        // 增加经验
        public override void AddExp(int exp)
        {
            // 增加经验
            if (BossData.AddExp(exp))
            {
                // 超过最大经验升级
                BehaviorData.SetUpgrade(true);
                // 无敌状态
                FighterData.Invincible = true;
            }
        }

        // 升级
        public override void OnUpgrade()
        {
            // 刷新数据
            if (BossData.UpgradeData(BossData.RoleDef.RoleID,BossData.Exp))
            {
                // 升级
                UpgradeModel(FighterData);
                
                // 跑马灯通知
                GameNoticeModel.Instance.SendHorseRaceLamp(FighterData.Name,
                    $"升级为了Lv：{FighterData.Level} {FighterData.RoleDef.SpeciesName}！");
            }
        }
        
        /// <summary>
        /// 更改模型
        /// </summary>
        public override async void ChangeRole(int roleId,int exp)
        {
            // 刷新数据
            if (BossData.UpgradeData(roleId, exp))
            {
                // 升级模型
                await UpgradeModel(FighterData);
                // 跑马灯通知
                GameNoticeModel.Instance.SendHorseRaceLamp(FighterData.Name,
                    $"升级为了Lv：{FighterData.Level} {FighterData.RoleDef.SpeciesName}！");
            }
        }
        
        
        /// <summary>
        /// 升级模型
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        private async UniTask UpgradeModel(UnitDataBase data)
        {
            // 旧模型
            var oldObject = _unitObject;
            var oldResPath = _modelResPath;
            // 更新数据
            _unitDataBase = data;
            _modelResPath = GetUnitModelPath();
            // 加载模型
            _unitObject = await BattleResPool.ConstructObjectAsync(_modelResPath);
            if (_unitObject == null)
            {
                Logger.LogError("UnitBase.Initialize() => 加载场景单位模型失败, _type:" + UnitType + ", uid:" + _unitDataBase.UnitID + ", configId:" + _unitDataBase.UnitConfigID);
                return;
            }
            // 回收旧模型
            BattleResPool.CollectObject(oldResPath,oldObject);
            // 初始化
            _unitObject.transform.SetParent(UnitContainerTransform, false);
            _unitObject.transform.localPosition = Vector3.zero;
            _unitObject.transform.localRotation = Quaternion.Euler(Vector3.zero);

            // 缩放属性
            var scaleValue = 1 + FighterData.GetAttributeAddition(AttributeType.BodyScale) / 100;
            // 设置大小
            UnitContainerTransform.DOScale(scaleValue * FighterData.ModelScale, 1f);
            
            // 显示模型
            _unitObject.SetActive(true);
            // 刷新功能组件
            RefreshComponent();
            // 初始化
            OnInitialize();
            
            // 重置动画
            _curAniName = String.Empty;
            // 设置开始默认动画
            PlayAction(ActionName.Idle);
        }
        // 刷新数据
        private void RefreshComponent()
        {
            // 默认动画
            if (FighterData.UnitActionType == UnitAnimationType.Default)
                // 获取动画组件
                Animator = _unitObject.GetComponent<Animator>();
            else if (FighterData.UnitActionType == UnitAnimationType.Gpu)
                // 获取渲染组件
                MeshRenderer = _unitObject.GetComponentInChildren<MeshRenderer>();
            
            // 动画时间数据
            ActionTimeDef = BattleModel.Instance.GetActionTimeDef(FighterData.ModelRes);
        }
        #endregion
        #region 光环
        public void AddHaloExp(int exp)
        {
            // 增加光环经验
            if (BossData.AddHaloExp(exp))    
            {
                // 光环升级
                HaloUpgrade();
            }
        }
        
        // 光环升级
        private void HaloUpgrade()
        {
            // 光环特效
            HaloEff();
            // 创建宠物
            CreatePet();
            
            // 更新速度
            SetMoveSpeed();
            // 跑马灯通知
            GameNoticeModel.Instance.SendHorseRaceLamp(BossData.Name,
                $"获得了{BossData.HaloDef().ID % 100 - 1}级光环《{BossData.HaloDef().HaloName}》！");
        }
        // 光环特效
        private string _effName;
        private void HaloEff()
        {
            var effectName = BossData.HaloDef().ResHaloModel;
            AddEffect("Effect_Halo", (effect) =>
            {
                // 显示光环
                for (int i = 0; i < effect.Object.transform.childCount; i++)
                {
                    var item = effect.Object.transform.GetChild(i);
                    item.gameObject.SetActive(item.name.Equals(effectName));
                }
            });
        }
        #endregion
        #region 宠物
        // 增加宠物经验
        public void AddPetExp(int exp)
        {
            if (BossData.AddPetExp(exp))
            {
                var pet = BattleManager.Instance.GetPetByUnitId(_petUnitId);
                // 升级宠物模型
                if (pet != null)
                    pet.BehaviorData.SetChangeRole(true, BossData.PetDef().RoleId,0.5f);
                // 创建宠物
                else 
                    CreatePet();
                
                // 增加生命值
                var hpAdd = BossData.HpAddition(BossData.RoleDef.Hp);
                if (hpAdd > BossData.MaxHp)
                    BossData.SetHp(hpAdd,BossData.CurrentHp + hpAdd - BossData.MaxHp);
            }
        }
        
        // 创建PetUnit
        private void CreatePet()
        {
            var pet = BattleManager.Instance.GetPetByUnitId(_petUnitId);
            // 升级宠物模型
            if(pet != null)
                return;
            
            var petDef = BossData.PetDef();
            if(petDef.RoleId <= 0)
                return;
            
            _petUnitId = BattleModel.Instance.AddPet(petDef.RoleId, this);
        }
        #endregion
        // 重置技能cd
        public void ClearSkillCd(int skillId)
        {
            var skill = FighterData.GetSkill_SKillId(skillId);
            
            if(skill == null)
                return;
            
            skill.ClearCd();
        }
        // 设置无敌闲置
        public void SetInvincibleIdle(bool isInvincible)
        {
            // 无敌状态
            FighterData.Invincible = isInvincible;
            // 无敌闲置行为
            BehaviorData.SetInvincibleIdle(isInvincible);
        }

        public override void SearchAttackTarget()
        {
            // 判断行为数据
            if (BehaviorData == null || BehaviorData.IsDeath)
                return;
            
            // 视野范围
            var searchRange = FighterData.SearchRange;
            // 找寻目标
            FighterUnit target = BattleManager.Instance.FindNearestHero(FighterData.UnitID,-1,Position,searchRange);
            // 设置攻击目标 
            BehaviorData.SetTarget(target);
        }

        public override void DoDamage(FighterUnit fighterUnit,DamageInfo damage,bool ignoreInvincible = false)
        {
            if (FighterData == null)
                return;
            // 无敌退出
            if (FighterData.Invincible && !ignoreInvincible)
                return;
            
            // 死亡退出
            if(IsDeath)
                return;
            
            // 玩家
            var roleData = fighterUnit?.FighterData as RoleData;
            
            // 扣血
            FighterData.DecCurHp(damage, _unitObject);

            // 增加伤害积分
            roleData?.AddSkillScale((int)(damage.Value * GameConstantHelper.RoleAttackBossScoreMultiple));
            
            // 小于零死亡
            if (FighterData.CurrentHp <= 0)
            {
                if (roleData != null)
                {
                    // 增加击杀积分
                    roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
                }
                
                // 增加光环经验
                var unitRole =  (fighterUnit as FighterUnitRole);
                unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);
                // 增加宠物经验
                unitRole?.AddPetExp(FighterData.RoleDef.ExpPet);
                // 增加经验
                unitRole?.AddExp(FighterData.RoleDef.ExpReward);
                
                // 死亡
                OnDeath();
                // 夜晚结束
                if (fighterUnit != null)
                {
                    SkillBossTips skillBossTips = new SkillBossTips();
                    skillBossTips.RoleData = roleData;
                    // 奖励字典
                    skillBossTips.Reward = new Dictionary<string, int>()
                    {
                        {"Exp", FighterData.RoleDef.ExpReward},
                        {"Pet", FighterData.RoleDef.ExpPet},
                        {"Halo", FighterData.RoleDef.ExpHalo}
                    };
                    // 显示击杀提示
                    GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowKillTips,skillBossTips);
                    // 击杀奖励
                    SkillReward(fighterUnit);
                    // 记录boss信息
                    var bossRoleData = BattleModel.Instance.GetBossRoleData(BossData.UserId);
                    // 复制信息
                    bossRoleData.Copy(BossData);
                    // 刷新数据
                    bossRoleData.ChangeDef(BossData.Level);
                }
                else
                {
                    // 隐藏boss血条
                    BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowBossHp,null);
                }
            }
            else // 受击
            {
                BehaviorData.SetHit();
            }
        }
        // 击杀奖励
        private void SkillReward(FighterUnit fighterUnit)
        {
            var pos = Position;
            var dropId = FighterData.RoleDef.DropEwwaedId;
            if(dropId == 0)
                return;
            TimerHeap.AddTimer(500, 0, () =>
            {
                //创建boss奖励糖果
                FruitManager.Instance.CreateCandy_BossReward(pos,dropId,fighterUnit);
                // 光圈特效
                Effect.CreateSceneEffect(4018, pos, Vector3.one, Quaternion.identity);
            });
        }

        protected override void OnDeath()
        {
            // 设置死亡状态
            BehaviorData?.SetDeath(true);
            // boss 死亡
            BattleManager.Instance.UnitOnDeath(FighterData.UnitType);
            base.OnDeath();
        }
        public override void RemoveByDeath()
        {
            // 移除字典
            BattleManager.Instance.RemoveFighterUnit(FighterData);
            // 删除宠物
            var petData = BattleManager.Instance.GetPetByUnitId(_petUnitId);
            if(petData != null)
                BattleManager.Instance.RemoveFighterUnit(petData.FighterData);
        }
    }
}