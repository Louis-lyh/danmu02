﻿using System.Collections.Generic;
using DG.Tweening;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class MonsterBoss : FighterUnit
    {
        public MonsterBoss() :
            base(UnitType.MonsterBoss)
        {
        }

        protected override void OnAddToStage()
        {
            // 设置模型缩放
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            // 设置旋转
            UnitContainerTransform.rotation = Quaternion.Euler(new Vector3(0,180,0));
            
            // 添加障碍
            CreateObstacle();
            // coming
            ComingAction();
        }
        
        // 降临动画
        private void ComingAction()
        {
            // 开始位置 
            var endPos = UnitContainerTransform.position;
            var startPos = Vector3.up * 10 + endPos;
            UnitContainerTransform.position = startPos;
            UnitContainerTransform.DOMove(endPos, 0.5f)
                .SetEase(Ease.InCirc)
                .OnComplete(() =>
                {
                    //BehaviorData.SetComing(true);
                    // 启动免费技能
                    BattleModel.Instance.StartFreeBossSkill();
                });
        }

        // 刷新UI
        protected override void UpdateUI(float deltaTime)
        {
            if(FighterData == null)
                return;
            // 计算百分比
            float curHp = FighterData.CurrentHp;
            var hp = curHp / FighterData.MaxHp;
            hp *= 10000;
            BattleUIModel.Instance.UpdateBossBlood(FighterData);
        }
        
        // 设置无敌闲置
        public void SetInvincibleIdle(bool isInvincible)
        {
            // 无敌状态
            FighterData.Invincible = isInvincible;
            // 无敌闲置行为
            BehaviorData.SetInvincibleIdle(isInvincible);
        }
        /// <summary>
        /// 寻找攻击目标
        /// </summary>
        public override void SearchAttackTarget()
        {
            // 判断行为数据
            if (BehaviorData == null || BehaviorData.IsDeath)
                return;
            
            // 视野范围
            var searchRange = FighterData.SearchRange;
            // 找寻目标
            FighterUnit target = BattleManager.Instance.FindNearestHero(FighterData.UnitID,-1,Position,searchRange);
            // 设置攻击目标 
            BehaviorData.SetTarget(target);
        }

        // 受到伤害
        public override void DoDamage(FighterUnit fighterUnit,DamageInfo damage,bool ignoreInvincible = false)
        {
            if (FighterData == null)
                return;
            // 无敌退出
            if (FighterData.Invincible && !ignoreInvincible)
                return;
            // 死亡退出
            if(IsDeath)
                return;
            
            // 玩家
            var roleData = fighterUnit?.FighterData as RoleData;
            
            // 扣血
            FighterData.DecCurHp(damage, _unitObject);
            
            // 增加伤害积分
            roleData?.AddSkillScale((int)(damage.Value * GameConstantHelper.RoleAttackMonsterBossScoreMultiple));
            
            // 小于零死亡
            if (FighterData.CurrentHp <= 0)
            {
                if (roleData != null)
                {
                    // 增加击杀积分
                    roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
                }
                
                // 增加光环经验
                var unitRole =  (fighterUnit as FighterUnitRole);
                unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);
                // 增加宠物经验
                unitRole?.AddPetExp(FighterData.RoleDef.ExpPet);
                // 增加经验
                unitRole?.AddExp(FighterData.RoleDef.ExpReward);
                
                // 死亡
                OnDeath();
                // 夜晚结束
                if (fighterUnit != null)
                {
                    SkillBossTips skillBossTips = new SkillBossTips();
                    skillBossTips.RoleData = roleData;
                    // 奖励字典
                    skillBossTips.Reward = new Dictionary<string, int>()
                    {
                        {"Exp", FighterData.RoleDef.ExpReward},
                        {"Pet", FighterData.RoleDef.ExpPet},
                        {"Halo", FighterData.RoleDef.ExpHalo}
                    };
                    // 显示击杀提示
                    GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowKillTips,skillBossTips);
                    
                    // 击杀奖励
                    SkillReward(fighterUnit);
                }
                else
                    // 隐藏boss血条
                    BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowBossHp,null);
            }
            else // 受击
            {
                BehaviorData.SetHit();
            }
        }
        // 击杀奖励
        private void SkillReward(FighterUnit attacker)
        {
            var pos = Position;
            var dropId = FighterData.RoleDef.DropEwwaedId;
            if(dropId == 0)
                return;
            TimerHeap.AddTimer(500, 0, () =>
            {
                //创建boss奖励糖果
                FruitManager.Instance.CreateCandy_BossReward(pos,dropId,attacker);
                // 光圈特效
                Effect.CreateSceneEffect(4018, pos, Vector3.one, Quaternion.identity);
            });
        }

        protected override void OnDeath()
        {
            // 设置死亡状态
            BehaviorData?.SetDeath(true);
            // boss 死亡
            BattleManager.Instance.UnitOnDeath(UnitType.MonsterBoss);
            base.OnDeath();
        }
        public override void RemoveByDeath()
        {
            // 移除字典
            BattleManager.Instance.RemoveFighterUnit(FighterData);
        }
    }
}