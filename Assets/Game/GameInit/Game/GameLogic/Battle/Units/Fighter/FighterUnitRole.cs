using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Game;
using GameInit.Framework;
using GameInit.Game.Pet;
using UnityEngine;
using Logger = GameInit.Framework.Logger;
using Random = UnityEngine.Random;

namespace GameInit.Game
{
    /// <summary>
    /// 角色
    /// </summary>
    public class FighterUnitRole : FighterUnit
    {
        public RoleData RoleData { get; private set; }

        // 还击cd
        private float _fightBackCD;
        // 宠物
        private long _petUnitId;
        public FighterUnitRole()
            : base(UnitType.Role)
        {
            
        }

        protected override void Init(UnitDataBase data)
        {
            base.Init(data);
            RoleData = FighterData as RoleData;
        }

        // 加入战斗阶段
        protected override void OnAddToStage()
        {
            base.OnAddToStage();
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            // 随机朝向
            var angles = Random.rotation.eulerAngles;
            angles.x = 0;
            angles.z = 0;
            UnitContainerTransform.rotation = Quaternion.Euler(angles);

            // 世界排名特效
            TopPlayerEffects();
            // 光环特效
            HaloEff();
            // 创建宠物
            CreatePet();
            // 出现动画
            BehaviorData.SetComing(true);
        }

        protected override void OnUpdate(float deltaTime)
        {
            base.OnUpdate(deltaTime);
            // 还击cd
            FightBackCd(deltaTime);
        }
        
        // 切换攻击倒计时
        private void FightBackCd(float deltaTime)
        {
            if (_fightBackCD > 0)
                _fightBackCD -= deltaTime;
        }
        
        // 复活
        public override void Revive()
        {
            // 创建代理
            CreateAgent();
            // 世界排名特效
            TopPlayerEffects();
            // 光环特效
            HaloEff();
            // 创建宠物
            CreatePet();
            // 回复生命值
            FighterData.RestoreHp(0,100);
        }
        // 世界排名特效
        private void TopPlayerEffects()
        {
            // 世界排名
            var order = RankInfo.Instance.RankOrder(RoleData.UserId,ServerData.ERankType.e_player);
            
            // 大佬周身特效
            if (order <= GameConstantHelper.TopPlayerLowLimit)
            {
                var effectName = "Effect_Brother3";

                if (order == 1)
                    effectName = "Effect_Brother2";
                else if(order <= 3)
                    effectName = "Effect_Brother2";
                else
                    effectName = "Effect_Brother3";
                // 添加特效
                AddEffect(effectName);
            } 
        }

        // 搜索目标
        public override void SearchAttackTarget()
        {
            // 没有开始战斗退出
            if(!BattleManager.Instance.IsBattle)
                return;
        
            // 判断行为数据
            if (BehaviorData == null || BehaviorData.IsDeath || FighterData == null)
                return;

            FighterUnit target = null;
            switch (BattleManager.Instance.BattleType)
            {
                // 普通模式
                case BattleType.Normal:
                    target = FindTarget_Normal();
                    break;
                // 野外模式
                case BattleType.FieldBoss:
                    target = FindTarget_FieldBoss();
                    break;
            }
            
            // 设置攻击目标 
            BehaviorData.SetTarget(target);
        }
        
        /// <summary>
        /// 普通模式找寻敌人逻辑
        /// </summary>
        /// <returns></returns>
        private FighterUnit FindTarget_Normal()
        {
            FighterUnit target = null;
            if (!BattleManager.Instance.IsNight && FighterData.TargetType == UnitType.Monster)
            {
                // 寻找其他公会最强玩家
                var searchRange = FighterData.SearchRange;
                target = BattleManager.Instance.FindNearestHero(FighterData.UnitID,RoleData.GuildId,Position,searchRange,true);
                // 设置玩家攻击目标
                if(target != null)
                    FighterData.SetTargetType(UnitType.Role);
                // 寻找目标
                else 
                    target = FindTarget();
            }
            else
                // 寻找目标
                target = FindTarget();
            
            // 没有目标
            if (target == null)
            {
                // 没有其他公会玩家
                if(FighterData.TargetType == UnitType.Role)
                    FighterData.SetTargetType(UnitType.Monster);
                
                // 怪兽没有了 切换攻击boss
                if (FighterData.TargetType == UnitType.Monster || FighterData.TargetType == UnitType.SmallWerewolf)
                {
                    if(BattleManager.Instance.BossUnit is FighterUnitBoss)
                        FighterData.SetTargetType(UnitType.Boss);
                    if(BattleManager.Instance.BossUnit is MonsterBoss)
                        FighterData.SetTargetType(UnitType.MonsterBoss);
                }
                // 水果没有了切换为打怪
                else if(FighterData.TargetType == UnitType.Fruit)
                    FighterData.SetTargetType(UnitType.Monster);
                
                // 没有boss 打怪
                else  if(FighterData.TargetType == UnitType.Boss || FighterData.TargetType == UnitType.MonsterBoss)
                    FighterData.SetTargetType(UnitType.Monster);
            }

            return target;
        }
        
        /// <summary>
        /// 野外boss找寻敌人逻辑
        /// </summary>
        /// <returns></returns>
        private FighterUnit FindTarget_FieldBoss()
        {
            FighterUnit target = null;

            
            // 状态不为采集
            if (FighterData.TargetType != UnitType.Fruit)
            {
                // 寻找其他公会玩家
                var searchRange = FighterData.SearchRange;
                target = BattleManager.Instance.FindNearestHero(FighterData.UnitID,RoleData.GuildId,Position,searchRange,true);
                
                // 设置玩家攻击目标
                if (target != null)
                    FighterData.SetTargetType(UnitType.Role);
                // 没有其他公会玩家 攻击boss
                else
                {
                    FighterData.SetTargetType(UnitType.FieldBoss);
                    // 寻找目标
                    target = FindTarget();
                }
            }
            else
            {
                // 寻找目标
                target = FindTarget();
                if (target == null && FighterData.TargetType == UnitType.Fruit)
                {
                    FighterData.SetTargetType(UnitType.FieldBoss);
                    // 寻找目标
                    target = FindTarget();
                }
            }
            
            return target;
        }

        /// <summary>
        /// 选择目标
        /// </summary>
        /// <returns></returns>
        private FighterUnit FindTarget()
        {
            // 视野范围
            var searchRange = FighterData.SearchRange;
            // 当前目标id
            var curTargetId =  -1L;
            if (BehaviorData.TargetFighter != null && BehaviorData.TargetFighter.FighterData != null)
                curTargetId = BehaviorData.TargetFighter.FighterData.UnitID;

            // 找寻目标
            FighterUnit target = null;
            switch (FighterData.TargetType)
            {
                case UnitType.SmallWerewolf:
                case UnitType.Monster:
                    // 晚上优先攻击小狼人
                    if(BattleManager.Instance.IsNight)
                        target = BattleManager.Instance.FindNearestMonster(curTargetId,Position,searchRange,UnitType.SmallWerewolf);
                    // 小狼人为空攻击怪兽
                    if(target == null)
                        target = BattleManager.Instance.FindNearestMonster(curTargetId,Position,searchRange);
                    break;
                case UnitType.Role:
                    // 白天才能攻击其他公会玩家
                    if (!BattleManager.Instance.IsNight)
                        target = BattleManager.Instance.FindNearestHero(FighterData.UnitID,RoleData.GuildId,Position,searchRange);
                    break;
                case UnitType.Fruit:
                    target = BattleManager.Instance.FindFruit(curTargetId,Position, searchRange);
                    break;
                case UnitType.Boss:
                case UnitType.MonsterBoss:
                    target = BattleManager.Instance.BossUnit;
                    break;
                case UnitType.FieldBoss:
                    target = BattleManager.Instance.FindNearestFieldBoss(curTargetId,Position,searchRange);
                    break;
            }
            
            return target;
        }


        protected override void UpdateUI(float deltaTime)
        {
            if (IsDeath)
                return;
            
            // 更新UI
            BattleUIModel.Instance.UpdateUnitBlood(this);
        }

        #region 经验升级
        // 增加经验
        public override void AddExp(int exp)
        {
            // 增加经验
            if (FighterData.AddExp(exp))
            {
                // 超过最大经验升级
                BehaviorData.SetUpgrade(true);
                // 无敌状态
                FighterData.Invincible = true;
            }
        }

        /// <summary>
        /// 升级
        /// </summary>
        public override async void OnUpgrade()
        {
            // 刷新数据
            if (FighterData.UpgradeData(RoleData.RoleDef.RoleID, RoleData.Exp))
            {
                // 升级模型
                await UpgradeModel(FighterData);
                // 跑马灯通知
                GameNoticeModel.Instance.SendHorseRaceLamp(FighterData.Name,
                    $"升级为了Lv：{FighterData.Level} {FighterData.RoleDef.SpeciesName}！");
            }
        }
        
        /// <summary>
        /// 更改模型
        /// </summary>
        public override async void ChangeRole(int roleId,int exp)
        {
            // 刷新数据
            if (FighterData.UpgradeData(roleId, exp))
            {
                // 升级模型
                await UpgradeModel(FighterData);
                // 跑马灯通知
                GameNoticeModel.Instance.SendHorseRaceLamp(FighterData.Name,
                    $"升级为了Lv：{FighterData.Level} {FighterData.RoleDef.SpeciesName}！");
            }
        }

        /// <summary>
        /// 升级模型
        /// </summary>
        /// <param name="data">数据</param>
        /// <returns></returns>
        private async UniTask UpgradeModel(UnitDataBase data)
        {
            // 旧模型
            var oldObject = _unitObject;
            var oldResPath = _modelResPath;
            // 更新数据
            _unitDataBase = data;
            _modelResPath = GetUnitModelPath();
            // 加载模型
            _unitObject = await BattleResPool.ConstructObjectAsync(_modelResPath);
            if (_unitObject == null)
            {
                Logger.LogError("UnitBase.Initialize() => 加载场景单位模型失败, _type:" + UnitType + ", uid:" + _unitDataBase.UnitID + ", configId:" + _unitDataBase.UnitConfigID);
                return;
            }
            // 回收旧模型
            BattleResPool.CollectObject(oldResPath,oldObject);
            // 初始化
            _unitObject.transform.SetParent(UnitContainerTransform, false);
            _unitObject.transform.localPosition = Vector3.zero;
            _unitObject.transform.localRotation = Quaternion.Euler(Vector3.zero);

            // 缩放属性
            var scaleValue = 1 + FighterData.GetAttributeAddition(AttributeType.BodyScale) / 100;
            // 设置大小
            UnitContainerTransform.DOScale(scaleValue * FighterData.ModelScale, 1f);
            
            // 显示模型
            _unitObject.SetActive(true);
            // 刷新功能组件
            RefreshComponent();
            // 初始化
            OnInitialize();
            
            // 重置动画
            _curAniName = String.Empty;
            // 设置开始默认动画
            PlayAction(ActionName.Idle);
        }
        // 刷新数据
        private void RefreshComponent()
        {
            // 默认动画
            if (FighterData.UnitActionType == UnitAnimationType.Default)
                // 获取动画组件
                Animator = _unitObject.GetComponent<Animator>();
            else if (FighterData.UnitActionType == UnitAnimationType.Gpu)
                // 获取渲染组件
                MeshRenderer = _unitObject.GetComponentInChildren<MeshRenderer>();
            
            // 动画时间数据
            ActionTimeDef = BattleModel.Instance.GetActionTimeDef(FighterData.ModelRes);
            // 重新创建寻路
            if (RvoLayer != FighterData.RVOLayer)
            {
                RvoLayer = FighterData.RVOLayer;
                // 寻路代理
                var speed = FighterData.GetMoveSpeed() * 0.01f;
                RVOManager.Instance.SetLayer(GameAgent,Radius,speed,RvoLayer);
            }
            else
            {
                // 寻路代理
                var speed = FighterData.GetMoveSpeed() * 0.01f;
                RVOManager.Instance.SetAgent(GameAgent.Sid,Radius,speed,RvoLayer); 
            }
        }

        #endregion

        #region 动画

        public override void PlayAction(string actionName, bool isLoop = true)
        {
            // 相同动画退出
            if (_curAniName.Equals(actionName))
                return;
            
            // 结束上一个动画
            AnimationEnd();
            _curAniName = actionName;
            
            // 特效处理动画 使用
            switch (actionName)
            {
                case ActionName.Attack1Body:
                case ActionName.Attack2Body:
                case ActionName.Attack3Body:
                case ActionName.Attack4Body:
                    PlayBodyAnimation(actionName, false);
                   return;
            }

            switch (FighterData.UnitActionType)
            {
                case UnitAnimationType.Gpu:    // GPU动画
                    PlayGpuAction(actionName,isLoop);
                    break;
                case UnitAnimationType.Default:    // 默认动画
                    Animator.Play(actionName);
                    break;
            }
        }
        // 动画结束
        protected override void AnimationEnd()
        {
            // 切换渲染组件
            if (_curAniName == ActionName.Attack1Body
                || _curAniName == ActionName.Attack2Body
                || _curAniName == ActionName.Attack3Body)
            {
                // 隐藏
                MeshRenderer.gameObject.SetActive(false);
                // 显示
                var body = _unitObject.transform.Find("Body");
                body.gameObject.SetActive(true);
                // 更改
                MeshRenderer = body.GetComponent<MeshRenderer>();
            }
        }

        private void PlayBodyAnimation(string actionName,bool isLoop = true)
        {
            // 动画
            var body = _unitObject.transform.Find(_curAniName);
            body.gameObject.SetActive(true);
            // 隐藏
            MeshRenderer.gameObject.SetActive(false);
            // 更改
            MeshRenderer = body.GetComponent<MeshRenderer>();
            // 重置动画时间
            _animationTime = 0;
            // 动画长度
            _animLength = MeshRenderer.material.GetFloat(_animLenField);
            // 设置循环
            _isAnimationLoop = isLoop;
            // 动画是否结束
            _isAnimationEnd = false;
        }


        #endregion

        #region 光环
        public void AddHaloExp(int exp)
        {
            // 增加光环经验
            if (RoleData.AddHaloExp(exp))
            {
                // 光环升级
                HaloUpgrade();
            }
        }
        
        // 光环升级
        private void HaloUpgrade()
        {
            // 光环特效
            HaloEff();
            // 创建宠物
            CreatePet();
            // 更新速度
            SetMoveSpeed();
            // 弹窗 
            // 跑马灯通知
            GameNoticeModel.Instance.SendHorseRaceLamp(RoleData.Name,
                $"获得了{RoleData.HaloDef().ID % 100 - 1}级光环《{RoleData.HaloDef().HaloName}》！");
        }
        // 光环特效
        private string _effName;
        private void HaloEff()
        {
            var effectName = RoleData.HaloDef().ResHaloModel;
            AddEffect("Effect_Halo", (effect) =>
            {
                // 显示光环
                for (int i = 0; i < effect.Object.transform.childCount; i++)
                {
                    var item = effect.Object.transform.GetChild(i);
                    item.gameObject.SetActive(item.name.Equals(effectName));
                }
            });
        }
        #endregion

        #region 宠物
        // 增加宠物经验
        public void AddPetExp(int exp)
        {
            if (RoleData.AddPetExp(exp))
            {
                var pet = BattleManager.Instance.GetPetByUnitId(_petUnitId);
                // 升级宠物模型
                if (pet != null)
                    pet.BehaviorData.SetChangeRole(true, RoleData.PetDef().RoleId,0.5f);
                // 创建宠物
                else
                    CreatePet();
                
                // 增加生命值
                var hpAdd = RoleData.HpAddition(RoleData.RoleDef.Hp);
                if (hpAdd > RoleData.MaxHp)
                    RoleData.SetHp(hpAdd,RoleData.CurrentHp + hpAdd - RoleData.MaxHp);
            }
        }
        
        // 创建PetUnit
        private void CreatePet()
        {
            var pet = BattleManager.Instance.GetPetByUnitId(_petUnitId);
            // 升级宠物模型
            if(pet != null)
                return;
            
            var petDef = RoleData.PetDef();
            if(petDef.RoleId <= 0)
                return;
            
            _petUnitId = BattleModel.Instance.AddPet(petDef.RoleId, this);
        }
        #endregion
        // 击退
        public override void SetHitBack(bool isHitBack, float hitBackDis, Vector3 hitDir)
        {
            // 击退
            BehaviorData.SetHitBack(isHitBack,hitBackDis,hitDir);
            // 击退宠物
            var pet = BattleManager.Instance.GetPetByUnitId(_petUnitId);
            pet?.SetHitBack(isHitBack,hitBackDis,hitDir);
        }
        
        // 受伤
        public override void DoDamage(FighterUnit fighterUnit,DamageInfo damage,bool ignoreInvincible = false)
        {
            if (FighterData == null || BehaviorData == null)
                return;
            
            // 无敌退出
            if (FighterData.Invincible && !ignoreInvincible)
                return;
            
            if(BehaviorData.IsDeath)
                return;
            
            // 角色
            var roleData = fighterUnit?.FighterData as RoleData;

            // 扣血
            FighterData.DecCurHp(damage, _unitObject);
            // 闪白
            ShinyWhite();
           
            // 增加伤害积分
            roleData?.AddSkillScale((int)(damage.Value * GameConstantHelper.BossAttackRoleScoreMultiple));
            // 小于零死亡
            if (FighterData.CurrentHp <= 0)
            {
                if (roleData != null)
                {
                    // 击杀积分
                    roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
                    
                    // 击杀数(玩家击杀玩家才增加击杀数)
                    if(roleData.UnitType == UnitType.Role)
                        roleData.AddKillRoleCount();
                }
                
                // 增加boss光环经验
                var unitRole =  (fighterUnit as FighterUnitBoss);
                unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);

                // 被敌人击杀 并且是改公会玩家等待复活
                if(fighterUnit != null && BattleManager.Instance.GuildId.Equals(RoleData.GuildId) && BattleManager.Instance.BattleType == BattleType.Normal)
                    BehaviorData?.WaitRevive();
                else if (BattleManager.Instance.BattleType == BattleType.FieldBoss && BattleManager.Instance.GuildId.Equals(RoleData.GuildId))
                {
                    BehaviorData?.WaitRevive(ServerTimerManager.Instance.GameTime);
                    Logger.LogBlue($"{FighterData.Name} WaitRevive {ServerTimerManager.Instance.GameTime}");
                }

                // 击杀奖励
                if(BattleManager.Instance.BattleType == BattleType.FieldBoss)
                    SkillReward(fighterUnit);
                
                // 死亡
                OnDeath();
            }
            else // 受击
            {
                if (_fightBackCD <= 0 && FighterData.TargetType != fighterUnit.UnitType)
                {
                    // 设置攻击目标 
                    BehaviorData.SetTarget(fighterUnit);
                    // 设置目标类型
                    FighterData.SetTargetType(fighterUnit.UnitType);
                    // 重置cd
                    _fightBackCD = GameConstantHelper.PassiveRoleSetTargetTypeCD;
                }

                BehaviorData.SetHit();
            }
        }
        
        // 死亡
        protected override void OnDeath()
        {
            // 停止移动
            StopMove();
            // 设置死亡状态
            BehaviorData?.SetDeath(true);

            // 移除血条
            BattleUIModel.Instance.DeleteUnitBlood(FighterData.UnitID);
            
            // 删除宠物
            var petData = BattleManager.Instance.GetPetByUnitId(_petUnitId);
            if(petData != null)
                BattleManager.Instance.RemoveFighterUnit(petData.FighterData);
            
            // 玩家死亡
            BattleManager.Instance.UnitOnDeath(FighterData.UnitType);
            
            base.OnDeath();
        }
        
        // 击杀奖励
        private void SkillReward(FighterUnit fighterUnit)
        {
            var pos = Position;
            var dropId = FighterData.RoleDef.DropEwwaedId;
            if(dropId == 0)
                return;
            TimerHeap.AddTimer(500, 0, () =>
            {
                //创建boss奖励糖果
                FruitManager.Instance.CreateCandy_BossReward(pos,dropId,fighterUnit);
                // 光圈特效
                Effect.CreateSceneEffect(4018, pos, Vector3.one, Quaternion.identity);
            });
        }

        public override void RemoveByDeath()
        {
            // 等待复活
            if (BehaviorData.IsWaitRevive)
            {
                // 隐藏
                _unitObject.SetActive(false);
                // 移除buff
                RemoveAllBuffAction();
                // 移除特效
                RemoveAllEffect();
                // 删除寻路 
                DeleteAgent();
                // 移除血条
                BattleUIModel.Instance.DeleteUnitBlood(FighterData.UnitID);
            }
            else
            {
                // 移除字典
                BattleManager.Instance.RemoveFighterUnit(FighterData);
            }
            
        }

        protected override void OnDispose()
        {
            // 移除血条
            BattleUIModel.Instance.DeleteUnitBlood(FighterData.UnitID);
            base.OnDispose();
        }
    }
}