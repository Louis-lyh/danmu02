using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;
using Random = UnityEngine.Random;

namespace GameInit.Game
{
    public abstract class FighterUnit : UnitBase
    {
        // 动画组件
        protected Animator Animator;
        // 网格组件
        protected MeshRenderer MeshRenderer;
        // 战斗数据
        public FighterUnitData FighterData { get; private set; }
        // 行为树数据
        public FighterBehaviourData BehaviorData { get; protected set; }
        // 动画时间数据
        public RoleAnimationTimeDef ActionTimeDef { get; protected set; }
        // 行为树
        protected BTAction _behaviorTree;
        // 寻路代理
        public GameAgent GameAgent { get; private set; }
        // 半径
        public float Radius => FighterData.ModelScale / 2;
        // 障碍物顶点编号
        private List<int> _obstacleIndexs;
        // 寻路类型
        protected RVOLayer RvoLayer;
        // 特效
        protected Dictionary<string, Effect> _dicEffect;
        
        // 是否死亡
        public bool IsDeath
        {
            get
            {
                if (BehaviorData == null)
                    return true;
                
                return BehaviorData.IsDeath;
            }
        }

        protected FighterUnit(UnitType type)
            : base(type)
        {
        }

        protected override void Init(UnitDataBase data)
        {
            base.Init(data);            
            // 战斗数据
            FighterData = GetUnitData<FighterUnitData>();
        }
        // 加入战斗阶段
        protected override void OnAddToStage()
        {
            base.OnAddToStage();
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            // 随机朝向
            var angles = Random.rotation.eulerAngles;
            angles.x = 0;
            angles.z = 0;
            UnitContainerTransform.rotation = Quaternion.Euler(angles);

        }
        // 模型加载完成
        protected override async UniTask OnUnitModelLoaded()
        {
            if (UnitContainerTransform == null)
                return;

            // 默认动画
            if (FighterData.UnitActionType == UnitAnimationType.Default)
                // 获取动画组件
                Animator = _unitObject.GetComponent<Animator>();
            else if (FighterData.UnitActionType == UnitAnimationType.Gpu)
                // 获取渲染组件
                MeshRenderer =  _unitObject.transform.Find("Body").GetComponent<MeshRenderer>();

            // 初始化数据
            PrepareData();
        }

        // 初始化数据
        protected virtual void PrepareData()
        {
            // 动画时间数据
            ActionTimeDef = BattleModel.Instance.GetActionTimeDef(FighterData.ModelRes);
            // 行为树数据
            BehaviorData = new FighterBehaviourData(this);
            // 创建行为树
            _behaviorTree = BehaviourFactory.CreateBehaviourTree(this);
            // 设置名称
            UnitContainerTransform.name = FighterData.Name + " " + FighterData.UnitID;
            // 寻路层级
            RvoLayer = FighterData.RVOLayer;
            // 特效字典
            _dicEffect = new Dictionary<string, Effect>();
            // buff字典
            _dictBuffActionsList = new List<BuffActionBase>();
        }

        // 获取模型路径
        protected override string GetUnitModelPath()
        {
            // GPU动画预制体
            if (FighterData.UnitActionType == UnitAnimationType.Gpu)
                return GameResPathUtils.GetFighterUnitPath_GPU(FighterData.ModelRes);
            // 默认预制体
            else
                return GameResPathUtils.GetFighterUnitPath(FighterData.ModelRes);
        }

        #region Update
        // 更新方法
        // 刷新间隔时间
        private float _updateInterval = 0.03f;
        // 刷新时间
        private float _updateTime = 0;
        protected override void OnUpdate(float deltaTime)
        {
            // 闪白
            //UpdateShinyWhite(_updateTime);
            
            // 刷新间隔
            _updateTime += deltaTime;
            if (_updateTime < _updateInterval)
                return;
            if (FighterData == null)
                return;
            
            // 刷新行为树
            UpdateBehaviorTree(_updateTime);
            // 刷新UI
            UpdateUI(_updateTime);
            // 刷新buff
            UpdateBuff(_updateTime);
            // 刷新动画
            UpdateAnimation(_updateTime);
            base.OnUpdate(_updateTime);
            // 重置
            _updateTime = 0;
        }

        // 刷新行为树
        private void UpdateBehaviorTree(float deltaTime)
        {
            if (_behaviorTree != null)
            {
                // 刷新行为树
                BehaviorData.DeltaTime = deltaTime;
                // 判断是否有可执行节点
                if (_behaviorTree.Evaluate(BehaviorData))
                    _behaviorTree.Update(BehaviorData);
                else
                    _behaviorTree.Transition(BehaviorData);
            }

            // 刷新数据
            if (FighterData != null)
                FighterData.Update(deltaTime);
        }

        // 更新Buff
        private void UpdateBuff(float deltaTime)
        {
            // 刷新buff
            if (_dictBuffActionsList != null)
            {
                for (int index = 0; index < _dictBuffActionsList.Count; index++)
                {
                    _dictBuffActionsList[index].Update(deltaTime);
                }
            }
        }

        // 更新UI
        protected virtual void UpdateUI(float deltaTime){}
        #endregion
        
        #region buff相关
        // buff字典
        protected List<BuffActionBase> _dictBuffActionsList;
        // 添加buff
        public void AddBuff(BuffData buff,BuffDef def)
        {
            // 获取buff
            var action = GetBuffAction(buff);
            
            // 新加buff
            if (action == null)
            {
                action = BuffFactory.CreateBuffAction(buff, this);
                if (action == null)
                {
                    Logger.LogError("FighterUnit.AddBuff() => 未知buff行为，添加失败，buff id:" + buff.BuffId);
                    return;
                }
                _dictBuffActionsList.Add(action);
                // 启动
                action.Start(buff);
            }
            // 叠加
            else
                action.Superposition(def);

        }
        // 获取buff
        private BuffActionBase GetBuffAction(BuffData data)
        {
            var action = GetBuffActionByID(data.BuffId);
            if (action != null)
                return action;
            
            for (int i = 0; i < _dictBuffActionsList.Count; i++)
            {
                if (_dictBuffActionsList[i].Data.IsSameEffect(data))
                    return _dictBuffActionsList[i];
            }

            return null;
        }
        // 获取buff
        private BuffActionBase GetBuffActionByID(int buffId)
        {
            for (int i = 0; i < _dictBuffActionsList.Count; i++)
            {
                if (_dictBuffActionsList[i].Data.BuffId == buffId)
                    return _dictBuffActionsList[i];
            }

            return null;
        }
        // 移除buff
        public void RemoveBuff(BuffData data)
        {
            var action = GetBuffActionByID(data.BuffId);
            if (action == null)
            {
                Logger.LogMagenta("FighterUnit.RemoveBuff() => 移出buff失败，找不到id:" + data.BuffId);
                return;
            }
            FighterData.RemoveBuff(data.BuffId);
            _dictBuffActionsList.Remove(action);
            action.Dispose();
        }
        // 移除所有buff
        protected void RemoveAllBuffAction()
        {
            if (_dictBuffActionsList == null)
                return;
            for (var i = 0; i < _dictBuffActionsList.Count; i++)
                _dictBuffActionsList[i].Dispose();
            _dictBuffActionsList.Clear();
        }
        #endregion
        
        #region 特效
        // 添加特效
        public Effect AddEffect(string effectName,Action<Effect> completeCallBack = null)
        {
            if (_dicEffect.ContainsKey(effectName))
            {
                _dicEffect[effectName].AddLife(effectName);
                // 回调
                completeCallBack?.Invoke(_dicEffect[effectName]);
                return _dicEffect[effectName];
            }

            // 特效
            var effectScale = Vector3.one;
            // 创建特效
            var effect = Effect.CreateEffect(effectName, UnitContainerTransform, Vector3.zero, effectScale,Quaternion.identity, true);
            _dicEffect.Add(effectName,effect);
            
            // 创建成功回调
            effect.OnCreateComplete = ()=> completeCallBack?.Invoke(effect);
            
            // 销毁回调
            effect.OnEffectTimeOver += effect1 =>
            {
                // 销毁
                RemoveEffect(effect1.EffectName);
            };
            
            return effect;
        }
        // 移除特效
        public void RemoveEffect(string effectName)
        {
            if (_dicEffect.ContainsKey(effectName))
            {
                var effect = _dicEffect[effectName];
                // 销毁
                effect.Dispose();
                _dicEffect.Remove(effectName);
            }
        }
        // 移除所有特效
        protected void RemoveAllEffect()
        {
            // 删除特效
            var effectKeys = _dicEffect.Keys.ToList();
            for (int i = 0; i < effectKeys.Count; i++)
            {
                var effectKey = effectKeys[i];
                _dicEffect[effectKey].Dispose();
            }
            _dicEffect.Clear();
        }

        #endregion
        
        #region 移动
        //  开始移动
        public virtual void StartMove(Vector3 pos)
        {
            // 开始移动
            GameAgent?.StartMove(pos);
        }
        
        // 追踪目标
        public void MoveToTarget()
        {
            StartMove(BehaviorData.TargetFighter.Position);
        }

        // 停止移动
        public virtual void StopMove()
        {
            // 停止寻路
            GameAgent?.StopMove();
        }

        // 创建代理
        public void CreateAgent()
        {
            // 寻路代理
            var speed = FighterData.GetMoveSpeed() * 0.01f;
            if (GameAgent == null)
                GameAgent = RVOManager.Instance.CreateAgent(UnitContainerTransform, Radius / 2, speed, RvoLayer);
        }

        // 删除代理
        public void DeleteAgent()
        {
            if (GameAgent != null)
                RVOManager.Instance.DeleteAgent(GameAgent.Sid);
            GameAgent = null;
        }

        // 创建障碍
        public void CreateObstacle()
        {
            var sideLength = Mathf.Pow((Radius * Radius) / 2, 0.5f);
            if (_obstacleIndexs == null)
            {
                _obstacleIndexs = RVOManager.Instance.CreateObstacle(Position, sideLength, RvoLayer);
            }
        }

        // 删除障碍
        public void DeleteObstacle()
        {
            if (_obstacleIndexs != null)
            {
                RVOManager.Instance.DeleteObstacle(_obstacleIndexs, RvoLayer);
                _obstacleIndexs = null;
            }
        }
        // 修改速度
        public void SetMoveSpeed()
        {
            if(GameAgent == null)
                return;
            
            var speed = FighterData.GetMoveSpeed() * 0.01f;
            RVOManager.Instance.SetSpeed(GameAgent.Sid,speed,RvoLayer);
        }

        #endregion
        
        #region 动画
        
        // 动画时间
        protected float _animationTime;
        // 动画长度字段
        protected string _animLenField = "_AnimLen";
        // 动画进度字段
        protected string _animProgressField = "_AnimProgress";
        // 动画长度
        protected float _animLength = 0;
        // 动画进度
        protected float _animProgress;
        // 是否循环
        protected bool _isAnimationLoop = true;
        // 当前动画
        protected string _curAniName = "";
        // 动画结束
        protected bool _isAnimationEnd = false;
        // 更新动画
        private void UpdateAnimation(float deltaTime)
        {
            if(MeshRenderer == null || _animLength <= 0)
                return;
            
            // 动画进度
            _animProgress = _animationTime / _animLength;
            
            // 不循环退出
            if (_animProgress > 0.9f && !_isAnimationLoop)
            {
                // 运行动画
                MeshRenderer.material.SetFloat(_animProgressField,0.9f);
                // 动画结束
                if (!_isAnimationEnd)
                {
                    AnimationEnd();
                    _isAnimationEnd = true;
                }
                return;
            }
            
            // 循环
            if (_isAnimationLoop && _animProgress > 0.9f)
                _animationTime = 0;
            
            // 运行动画
            MeshRenderer.material.SetFloat(_animProgressField,_animProgress);
            _animationTime += deltaTime;
        }
        // 播放动画
        public virtual void PlayAction(string actionName,bool isLoop = true)
        {
            // 相同动画退出
            if (_curAniName.Equals(actionName))
                return;
            _curAniName = actionName;
            // 结束上一个动画
            AnimationEnd();
            
            switch (FighterData.UnitActionType)
            {
                case UnitAnimationType.Gpu:    // GPU动画
                    PlayGpuAction(actionName,isLoop);
                    break;
                case UnitAnimationType.Default:    // 默认动画
                    PlayerDefault(actionName, isLoop);
                    break;
            }
        }

        // 播放GPU动画
        protected  void PlayGpuAction(string actionName,bool isLoop = true)
        {
            if(MeshRenderer == null)
                return;
            
            // 材质球路径
            var path = GameResPathUtils.GetMat_GPUAction(_unitDataBase.ModelRes,actionName);
            // 获取材质球
            var animationMat =  BattleResPool.ConstructMatAsync(path);
            if(animationMat == null)
                return;
            
            var oldMat = MeshRenderer.material;
            MeshRenderer.material = animationMat;
            
            MeshRenderer.material.SetColor("_ColorGlitter", Color.black);
            
            // if (_shinyWhiteTime <= 0) MeshRenderer.material.SetColor("_ColorGlitter", Color.black);
            // else MeshRenderer.material.SetColor("_ColorGlitter", Color.red);
            
            // 回收材质球
            var oldPath = GameResPathUtils.GetMat_GPUAction(_unitDataBase.ModelRes,oldMat.name);
            BattleResPool.CollectMat(oldPath,oldMat);
            
            // 重置动画时间
            _animationTime = 0;
            // 动画长度
            _animLength = MeshRenderer.material.GetFloat(_animLenField);
            // 设置循环
            _isAnimationLoop = isLoop;
            // 动画是否结束
            _isAnimationEnd = false;
        }
        // 播放常规动画
        private void PlayerDefault(string actionName, bool isLoop = true)
        {
            switch (actionName)
            {
                case ActionName.Attack1:
                case ActionName.Attack2:
                case ActionName.Attack3:
                case ActionName.Attack4:
                case ActionName.Damage:
                case ActionName.Coming:
                    Animator.SetTrigger(actionName);
                    break;
                default:
                    Animator.Play(actionName);
                    break;
            }
        }
        
        // 动画结束
        protected virtual void AnimationEnd(){}
        

        #endregion
        
        #region 战斗

        // 受伤
        public virtual void DoDamage(FighterUnit fighterUnit,DamageInfo damage,bool ignoreInvincible = false)
        {
            if (FighterData == null)
                return;
            // 无敌退出
            if (FighterData.Invincible)
                return;
            
            // 扣血
            FighterData.DecCurHp(damage, _unitObject);
            // 闪白
            //ShinyWhite();
           
            // 小于零死亡
            if (FighterData.CurrentHp <= 0)
            {
                var roleData = fighterUnit?.FighterData as RoleData;
                if (roleData != null)
                {
                    // 击杀积分
                    roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
                }
                
                // 增加光环经验
                var unitRole =  (fighterUnit as FighterUnitRole);
                unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);
                // 增加宠物经验
                unitRole?.AddPetExp(FighterData.RoleDef.ExpPet);

                // 死亡
                OnDeath();
            }
            else // 受击
            {
                BehaviorData.SetHit();
            }
        }
        // 死亡
        protected virtual void OnDeath(){}
        // 死亡处理
        public virtual void RemoveByDeath(){}
        // 寻找攻击目标
        public virtual void SearchAttackTarget(){}
        // 击退
        public virtual void SetHitBack(bool isHitBack, float hitBackDis, Vector3 hitDir)
        {
            BehaviorData.SetHitBack(isHitBack,hitBackDis,hitDir);
        }

        #endregion

        #region 升级
        // 增加经验
        public virtual void AddExp(int exp){}
        // 升级
        public virtual void OnUpgrade(){}
        public virtual void ChangeRole(int roleId, int exp){}

        #endregion
        
        #region 闪白
        private float _shinyWhiteTime;

        private void UpdateShinyWhite(float fTick)
        {
            if (MeshRenderer == null)
                return;

            if (_shinyWhiteTime > 0)
            {
                _shinyWhiteTime -= fTick;
                if (_shinyWhiteTime <= 0)
                {
                    _shinyWhiteTime = 0f;
                    MeshRenderer.material.SetColor("_ColorGlitter", Color.black);
                }
            }
        }

        public void ShinyWhite()
        {
            if (MeshRenderer == null || _shinyWhiteTime > 0)
                return;
            
            MeshRenderer.material.SetColor("_ColorGlitter", new Color(0.3f, 0f, 0f));
            _shinyWhiteTime = 0.15f;
        }
        #endregion

        #region 
        // 设置位置
        public void SetPos(Vector3 pos)
        {
            RVOManager.Instance.SetPos(GameAgent.Sid,pos,RvoLayer);
        }

        #endregion
        //复活
        public virtual void Revive(){}

        protected override void OnDispose()
        {
            Animator = null;

            // 移除所有特效
            RemoveAllEffect();
            
            // 移除所有buff
            RemoveAllBuffAction();
            
            base.OnDispose();
            
            // 行为树
            _behaviorTree = null;
            _behaviorTree?.Dispose();
            
            // 行为数据
            BehaviorData?.Dispose();
            BehaviorData = null;

            // 删除寻路代理
            DeleteAgent();
            // 删除障碍
            DeleteObstacle();
            
            // 战斗数据
            FighterData?.Dispose();
            FighterData = null;
        }
    }
}