﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class FruitUnit : FighterUnit
    {
        // 水果数据
        private FruitData _fruitData;
        
        public FruitUnit(UnitDataBase dataBase) 
            : base(UnitType.Fruit)
        {
            _unitDataBase = dataBase;
        }

        protected override void Init(UnitDataBase data)
        {
            base.Init(data);
            _fruitData = FighterData as  FruitData;
        }

        // 预制体路径
        protected override string GetUnitModelPath()
        {
            return GameResPathUtils.GetFruitPrefab(_unitDataBase.ModelRes);
        }
        // 受伤
        public override void DoDamage(FighterUnit fighterUnit, DamageInfo damage,bool ignoreInvincible = false)
        {
            // 增加经验
            fighterUnit?.AddExp(FighterData.RoleDef.ExpReward);
            // 死亡
            OnDeath();
            
            // 角色信息
            var roleData = fighterUnit?.FighterData as RoleData;
            if (roleData != null)
            {
                // 增加击杀积分
                roleData.AddSkillScale(FighterData.RoleDef.ScoreReward);
            }
            
            // 增加光环经验
            var unitRole =  (fighterUnit as FighterUnitRole);
            unitRole?.AddHaloExp(FighterData.RoleDef.ExpHalo);
            // 增加宠物经验
            unitRole?.AddPetExp(FighterData.RoleDef.ExpPet);
            // 移除字典
            BattleManager.Instance.RemoveFighterUnit(FighterData);
        }

        protected override void OnAddToStage()
        {
            UnitContainerTransform.localScale = Vector3.one * FighterData.ModelScale;
            Coming();
        }

        private void Coming()
        {
            if(!_fruitData.IsComing)
                return;

            var p0 = Position;
            var p2 = _fruitData.ComingPos;
            var dis = Vector3.Distance(p0, p2);
            var p1 = (Position + _fruitData.ComingPos) / 2 + Vector3.up * dis;
            // 长度
            var length = GameConstantHelper.GetBezierLength(p0,p1,p2);
            // 拖尾
            Effect.CreateEffect(4019,UnitContainerTransform, Vector3.zero, Vector3.one, Quaternion.identity);
            // 移动
            DOTween.To(
                ()=>0f, 
                (value) =>
                {
                    var pos = GameConstantHelper.BezierCurvePos(p0, p1, p2, value);
                    if(pos.magnitude > 0.01f)
                        UnitContainerTransform.position = pos;
                }, 
                1f, length * 0.3f)
                .OnComplete(FlyTarget)
                .SetEase(Ease.Linear);
        
        }
        
        /// <summary>
        /// 飞向目标
        /// </summary>
        private void FlyTarget()
        {
            if(FlyTargetDeath())
                return;

            var radius = _fruitData.FlyTarget.Radius;
            // 移动
            uint timer = 0;
            timer = TimerHeap.AddTimer(1, 30, () =>
            {
                var targetPos = _fruitData.FlyTarget.Position + Vector3.up * radius;
                UnitContainerTransform.position = Vector3.MoveTowards(Position, targetPos,1 * 0.3f);
                // 目标死亡结束
                if (FlyTargetDeath())
                {
                    var pos = UnitContainerTransform.position;
                    UnitContainerTransform.position = new Vector3(pos.x,0,pos.z);
                    TimerHeap.DelTimer(timer);
                }
                // 获得奖励
                if (Vector3.Distance(UnitContainerTransform.position, targetPos) < 0.01f)
                {
                    DoDamage(_fruitData.FlyTarget, new DamageInfo());
                    TimerHeap.DelTimer(timer);
                }

            });
        }
        /// <summary>
        /// 飞行目标死亡
        /// </summary>
        /// <returns></returns>
        private bool FlyTargetDeath()
        {
            if(_fruitData.FlyTarget == null 
               || _fruitData.FlyTarget.FighterData == null
               || _fruitData.FlyTarget.BehaviorData == null 
               || _fruitData.FlyTarget.BehaviorData.IsDeath)
                return true;
            return false;
        }

      

        // 死亡
        protected override void OnDeath()
        {
            BehaviorData.SetDeath();
        }
    }
}