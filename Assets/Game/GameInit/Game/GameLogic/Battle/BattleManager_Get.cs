﻿using System.Collections.Generic;
using System.Linq;
using GameHotfix.Game;
using GameInit.Game.Pet;
using UnityEngine;

namespace GameInit.Game
{
    public partial class BattleManager
    {
        #region GetData
      
        // 找寻怪兽目标
        public FighterUnit FindNearestMonster(long uId,Vector3 pos,float searchRange,UnitType unitType = UnitType.Monster)
        {
            float minDis = float.MaxValue;
            FighterUnit targret = null;
            // 遍历怪兽
            var keys = _dicAllMonsterFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var monster = _dicAllMonsterFighter[key];
                // 死亡跳过
                if(monster.IsDeath)
                    continue;
                
                // 类型不对跳过
                if(unitType != UnitType.Monster && monster.UnitType != unitType)
                    continue;

                var dis = Vector3.Distance(monster.Position, pos);
                // 在攻击范围内
                if (dis <= searchRange && dis < minDis && key != uId)
                {
                    targret = monster;
                    minDis = dis;
                }
            }
            return targret;
        }
        // 寻找守卫目标 _dicMonsterGuard
        public FighterUnit FindMonsterGuard(long uId,string guildId,Vector3 pos,float searchRange)
        {
            // 同一个公会退出
            if (GuildId.Equals(guildId))
                return null;
            
            float minDis = float.MaxValue;
            FighterUnit targret = null;
            // 遍历怪兽守卫
            var keys = _dicMonsterGuard.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var monster = _dicMonsterGuard[key];
                // 死亡跳过
                if(monster.IsDeath)
                    continue;

                var dis = Vector3.Distance(monster.Position, pos);
                // 在攻击范围内
                if (dis <= searchRange && dis < minDis && key != uId)
                {
                    targret = monster;
                    minDis = dis;
                }
            }
            return targret;
        }
        // 找寻野外boss目标
        public FighterUnit FindNearestFieldBoss(long uId,Vector3 pos,float searchRange)
        {
            float minDis = float.MaxValue;
            FighterUnit target = null;
            // 遍历字典
            var keys = _dicFieldBoss.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var fieldBoss = _dicFieldBoss[key];
                // 死亡跳过
                if(fieldBoss.IsDeath)
                    continue;
                
                var dis = Vector3.Distance(fieldBoss.Position, pos);
                // 在攻击范围内
                if (dis <= searchRange && dis < minDis && key != uId)
                {
                    target = fieldBoss;
                    minDis = dis;
                }
            }
            return target;
        }
        // 找寻英雄目标
        public FighterUnit FindNearestHero(long uId,int guildId,Vector3 pos,float searchRange,bool strongest = false)
        {
            float minDis = float.MaxValue;
            int maxExp = 0;
            FighterUnit targret = null;
            
            // 遍历英雄
            var keys = _dictAllRoleFighter.Keys.ToList();
            
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];
                // 死亡跳过
                if(role.IsDeath)
                    continue;
                
                // 相同公会跳过
                if(role.RoleData == null || role.RoleData.GuildId == (guildId))
                    continue;
                
                var dis = Vector3.Distance(role.Position, pos);
                // 在攻击范围内
                if (dis <= searchRange && key != uId)
                {
                    // 找最强
                    if (strongest)
                    {
                        if (role.RoleData.Exp > maxExp)
                        {
                            targret = role;
                            maxExp = role.RoleData.Exp;
                        }
                    }
                    // 找最近
                    else
                    {
                        if (dis < minDis)
                        {
                            targret = role;
                            minDis = dis;
                        }
                    }
                }
            }

            return targret;
        }
        // 找寻水果目标
        public FighterUnit FindFruit(long uid,Vector3 pos,float searchRange)
        {
            float minDis = float.MaxValue;
            FighterUnit targret = null;
            
            // 遍历水果
            var keys = _dicAllFruit.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var fruit = _dicAllFruit[key];
                // 死亡跳过
                if(fruit == null || fruit.FighterData.UnitID == uid)
                    continue;
                
                var dis = Vector3.Distance(fruit.Position, pos);
                // 在攻击范围内
                if (dis <= searchRange && dis < minDis)
                {
                    targret = fruit;
                    minDis = dis;
                }
            }

            return targret;
        }
        // 该范围内的所有怪兽
        public List<FighterUnit> FindMonsterList(Vector3 pos, float searchRange)
        {
            // 战斗单位
            List<FighterUnit> unitList = new List<FighterUnit>();
            // 遍历英雄
            var keys = _dicAllMonsterFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var monster = _dicAllMonsterFighter[key];
                // 死亡跳过
                if(monster.IsDeath)
                    continue;
                
                var dis = Vector3.Distance(monster.Position, pos);
                // 减去半径
                dis -= monster.Radius;
                // 在攻击范围内
                if (dis <= searchRange)
                {
                    unitList.Add(monster);
                }
            }

            return unitList;
        }
        // 该范围所有英雄
        public List<FighterUnit> FindHeroList(Vector3 pos, float searchRange)
        {
            // 战斗单位
            List<FighterUnit> unitList = new List<FighterUnit>();
            // 遍历英雄
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];
                // 死亡跳过
                if(role.IsDeath)
                    continue;
                
                var dis = Vector3.Distance(role.Position, pos);
                // 减去半径
                dis -= role.Radius;
                // 在攻击范围内
                if (dis <= searchRange)
                {
                    unitList.Add(role);
                }
            }
            return unitList;
        }

        // 获取英雄
        public FighterUnit GetFighterByUID(long uiId,bool isContainsDeath = false)
        {
            if (_dictAllRoleFighter.ContainsKey(uiId))
            {
                var unit = _dictAllRoleFighter[uiId];
                return  (unit.IsDeath && !isContainsDeath) ? null: unit;
            }
            return null;
        }
        // 获取宠物
        public PetUnit GetPetByUnitId(long unitId)
        {
            if (_dicAllPet.ContainsKey(unitId))
            {
                var unit = _dicAllPet[unitId];
                return  unit.IsDeath ? null: unit;
            }
            return null;
        }

        /// <summary>
        /// 获取战斗单位数量
        /// </summary>
        /// <param name="type"></param>
        /// <param name="containDeath"></param>
        /// <returns></returns>
        public int GetFighterCount(UnitType type,bool containDeath = false)
        {
            var count = 0;
            switch (type)
            {
                case UnitType.Role:
                    count =  _dictAllRoleFighter.Values.ToList()
                        .FindAll(item => !item.IsDeath || containDeath)
                        .Count;
                    break;
                case UnitType.Monster:
                    count = _dicAllMonsterFighter.Values.ToList()
                        .FindAll(item => item.UnitType == UnitType.Monster)
                        .Count;
                    break;
                case UnitType.MonsterGuard:
                    count = _dicMonsterGuard.Values.ToList().Count;
                    break;
                case UnitType.SmallWerewolf:
                    count =  _dicAllMonsterFighter.Values.ToList()
                        .FindAll(item => item.UnitType == UnitType.SmallWerewolf)
                        .Count;
                    break;
                case UnitType.Fruit:
                    count = _dicAllFruit.Count;
                    break;
                case UnitType.FieldBoss:
                    count = _dicFieldBoss.Count;
                    break;
            }

            return count;
        }
        // 获取最强大玩家
        public List<FighterUnit> GetTopUnit(int topNum)
        {
            // 前几名
            List<FighterUnit> units = new List<FighterUnit>();
            
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];
                
                // 死亡跳过
                if(role.IsDeath)
                    continue;
                
                // 插入前几名列表
                int index = units.Count;
                for (int j = units.Count - 1; j >= 0; j--)
                {
                    var unit = units[j];
                    // 大于列表向前移动
                    if (role.FighterData.Exp > unit.FighterData.Exp)
                        index = j;
                    else
                        break;
                }
                
                if(index < topNum)
                    units.Insert(index,role);
            }

            if (units.Count > topNum)
                units = units.GetRange(0, topNum);
                    
            return units;
        }
        // 获取所有玩家攻击
        public int GetAllRoleAttackValue(bool isAttackAddition)
        {
            var attackValue = 0;
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var role = _dictAllRoleFighter[key];
                
                // 死亡跳过
                if(role.IsDeath)
                    continue;

                int attack = 0;

                if (isAttackAddition)
                {
                    // 增加值 
                    attack = (int)role.FighterData.AttackAddition(role.FighterData.RoleDef.Attack);
                }
                else
                {
                    // 增加值 
                    var additionValue = 1 + 1 + role.FighterData.GetAttributeAddition(AttributeType.Attack) / 100;
                    attack = (int)(role.FighterData.RoleDef.Attack * additionValue);
                }

                
                attackValue += attack;
            }
            return attackValue;
        }
        // 所有英雄(不包括死亡英雄)
        public List<FighterUnitRole> AllRole(bool isContainsDeath = false)
        {
            var roleList = _dictAllRoleFighter.Values.ToList();

            for (int i = roleList.Count - 1; i >=0 ; i--)
            {
                var role = roleList[i];
                
                if (role.IsDeath && !isContainsDeath)
                    roleList.Remove(role);
            }
            
            return roleList;
        }

        #endregion
    }
}