
namespace GameInit.Game
{
    /// <summary>
    /// 技能工厂
    /// </summary>
    public static class SkillFactory
    {
        public static SkillActionBase CreateSkillActionBase(SkillData data)
        {
            SkillActionBase skillAction = null;

            switch (data.SkillType)
            {
                // 近距离
                case SkillType.DefaultCloseRange:
                    skillAction = new CloseRangeSkillAction(data);
                    break;
                // 远距离
                case SkillType.DefaultRemote:
                    skillAction = new DefaultRemoteSkillAction(data);
                    break;
                // 扇形攻击
                case SkillType.FanShaped:
                    skillAction = new FanShapedSkillAction(data);
                    break;
                // 圆形攻击
                case SkillType.Circular:
                    skillAction = new CircularSkillAction(data);
                    break;
                // 增加属性
                case SkillType.AddAttribute:
                    skillAction = new AddAttributeSkillAction(data);
                    break;
                // 狼人声波技能
                case SkillType.WerewolfSonicSkill:
                    skillAction = new WerewolfSonicSkillAction(data);
                    break;
                // 落雷技能
                case SkillType.Thunderbolt:
                    skillAction = new Thunderbolt(data);
                    break;
                default:
                    skillAction = new CloseRangeSkillAction(data);
                    break;
            }
            
            return skillAction;
        }

    }
}

