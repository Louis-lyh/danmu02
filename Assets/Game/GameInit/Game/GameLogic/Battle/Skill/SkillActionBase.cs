namespace GameInit.Game
{
    public class SkillActionBase : UpdateBase
    {
        // 技能数据
        protected SkillData SkillData;
        // 攻击者
        protected FighterUnit Attacker;
        // 目标
        protected FighterUnit Target;
        // 是否造成伤害
        public bool IsDamage;

        public SkillActionBase(SkillData skillData)
        {
            SkillData = skillData;
            IsDamage = false;
        }
        // 开始
        public void Start(FighterUnit attecker,FighterUnit target)
        {
            Attacker = attecker;
            Target = target;
            IsDamage = false;    
            // 初始化
            OnInitialize();
        }
        
        protected override void OnUpdate(float deltaTime){}
        // 移除事件
        protected virtual void RemoveEvent() {}
        // 添加事件
        protected virtual void AddEvent(){}
        // 添加技能特效
        protected virtual void AddSkillEffect(){}
        // 移除技能特效
        protected virtual void RemoveSkillEffect(){}
        // 执行技能
        protected virtual void OnStart(){}
        // 触发伤害
        protected virtual void OnDamageTrigger(){}
        // 触发buff
        protected virtual void OnBuffTrigger() {}
        // 技能结束
        protected virtual void OnSkillActionEnd() {}
        // 震动
        protected void Shake()
        {
            if(SkillData.SkillDef.ShakeId == 0)
                return;

            var shakeDef = ShakeConfig.Get(SkillData.SkillDef.ShakeId);
            
            // 震动
            BattleManager.Instance.Shake(shakeDef.Duration,shakeDef.Strength,shakeDef.Vibrato,shakeDef.Randomness);
        }
        protected void SkillActionEnd()
        {
            OnSkillActionEnd();
        }

        protected override void OnDispose()
        {
            OnSkillActionEnd();
            base.OnDispose();
        }
    }  
}

