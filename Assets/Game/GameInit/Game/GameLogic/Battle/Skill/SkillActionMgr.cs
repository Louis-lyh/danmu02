using System.Collections;
using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public class SkillActionMgr : Singleton<SkillActionMgr>
    {
         private class HandleSkillAction
        {
            public int ID;
            public List<SkillActionBase> SkillActions = new List<SkillActionBase>();
            private int _index;

            internal void UpdateSkillAction(float deltaTime)
            {
                if (SkillActions == null || SkillActions.Count == 0)
                    return;
                for (_index = 0; _index < SkillActions.Count; _index++)
                    SkillActions[_index].Update(deltaTime);
            }
            
            internal void DisposeActions()
            {
                if (SkillActions == null || SkillActions.Count == 0)
                    return;
                for (_index = 0; _index < SkillActions.Count; _index++)
                    SkillActions[_index].Dispose();
                
                SkillActions.Clear();
            }
        }

        private List<HandleSkillAction> _handleSkillActions;

        public void Init()
        {
            _handleSkillActions = new List<HandleSkillAction>();
        }

        private bool TryGetHandleSkillAction(int id, out HandleSkillAction action)
        {
            for (var i = 0; i < _handleSkillActions.Count; i++)
            {
                if (_handleSkillActions[i].ID == id)
                {
                    action = _handleSkillActions[i];
                    return true;
                }
            }

            action = null;
            return false;
        }
        

        private static readonly Vector3 _pos = new Vector3(0f, -3f, 0f);
        
        // 移除技能
        public void RemoveSkillAction(int skillId)
        {
            if (!TryGetHandleSkillAction(skillId, out var handleSkillAction))
                return;
            
            for (var i = 0; i < handleSkillAction.SkillActions.Count; i++)
                handleSkillAction.DisposeActions();
            _handleSkillActions.Remove(handleSkillAction);
        }
        // 移除所有技能
        public void RemoveAllSkillAction()
        {
            if (_handleSkillActions == null || _handleSkillActions.Count == 0)
                return;
            for(var i = 0; i < _handleSkillActions.Count; i++)
                _handleSkillActions[i].DisposeActions();
            _handleSkillActions.Clear();
        }

        private KeyValuePair<int, List<SkillActionBase>> _keyValuePair;
        private int _index;
        public void Update(float deltaTime)
        {
            if (_handleSkillActions == null)
                return;

            for (_index = 0; _index < _handleSkillActions.Count; _index++)
            {
                _handleSkillActions[_index].UpdateSkillAction(deltaTime);
            }
        }
        
        public void Dispose()
        {
            if (_handleSkillActions == null)
                return;
            RemoveAllSkillAction();
            _handleSkillActions = null;
        }
    }
}

