﻿using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 雷电
    /// </summary>
    public class Thunderbolt : CircularSkillAction 
    {
        // 伤害延迟
        private float _damageDlay;
        // 显示特效 
        private bool _isShowSkillEffect;
        public Thunderbolt(SkillData skillData) 
            : base(skillData)
        {
            _damageDlay = 0.25f;
            _isShowSkillEffect = true;
        }
        
        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束 或者已经攻击过了结束
            if (Attacker == null || Attacker.IsDeath || _isBattle)
            {
                SkillActionEnd(); // 结束
                return;
            }

            // 击中特效
            if (_isShowSkillEffect)
            {
                AddSkillEffect();
                _isShowSkillEffect = false;
            }

            //伤害延迟
            _damageDlay -= deltaTime;
            if (_damageDlay <= 0)
            {
                // 触发伤害
                OnDamageTrigger();
                // 触发震动
                Shake();
                // 记录攻击
                _isBattle = true;
            }
        }
        
        // 技能特效
        protected override void AddSkillEffect()
        {
           
            // 特效id
            var effectId = SkillData.SkillDef.TargetEffect;
            // 伤害半径
            var damageRadius = SkillData.SkillDef.DamageRadius + Attacker.Radius;
            // 目标
            var targetList = FindTarget(damageRadius);
            var effectCount = 11;
            var effectCircle = 3;

            for (int i = 0; /*i < targetList.Count ||*/ i < effectCount; i++)
            {
                for (int j = 0; j < effectCircle; j++)
                {
                    var angle = 360f / effectCount;
                    var dir = Quaternion.Euler(new Vector3(0, (angle * i) + (angle / 2 * (j % 2)))) * Attacker.UnitContainerTransform.forward;
                    //
                    var radius = damageRadius * ((j + 1) / (float) (effectCircle));
                    // 落雷默位置
                    var pos = Attacker.Position + dir.normalized * radius;
                
                    /*if (i < targetList.Count)
                    {
                        pos = targetList[i].Position;
                    }*/
                    // 播放特效
                    Effect.CreateSceneEffect(effectId,pos,Vector3.one,Quaternion.identity);  
                }
            }
        }

        // hit
        protected override void OnDamageTrigger()
        {
            // 缩放后的伤害半径
            var damageRadius = SkillData.SkillDef.DamageRadius * Attacker.FighterData.ModelScale;
            
            var targetList = FindTarget(damageRadius);
            for (int i = 0; i < targetList.Count; i++)
            {
                var target = targetList[i];
                // 造成伤害
                var damage = Attacker.FighterData.CalcAttackDamage(SkillData.SkillDef,target.FighterData.MaxHp);
                target.DoDamage(Attacker,damage);
            }
        }
        
        protected override void OnSkillActionEnd()
        {
            
        }
    }
}