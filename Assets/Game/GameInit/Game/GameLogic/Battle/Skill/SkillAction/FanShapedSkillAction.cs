﻿using System.Collections.Generic;
using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 扇形攻击
    /// </summary>
    public class FanShapedSkillAction : SkillActionBase 
    {
        // 是否攻击
        protected bool _isBattle;
        public FanShapedSkillAction(SkillData skillData) 
            : base(skillData)
        {
            _isBattle = false;
        }

        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束 或者已经攻击过了结束
            if (Attacker == null || Attacker.IsDeath || _isBattle)
            {
                SkillActionEnd(); // 结束
                return;
            }
            
            // 发射子弹
            if (!string.IsNullOrEmpty(SkillData.SkillDef.BulletRes))
            {
                FireBullet();
                SkillActionEnd(); // 结束技能
                _isBattle = true;
            }
            // 触发伤害
            else
            {
                OnDamageTrigger();
                SkillActionEnd(); // 结束技能
                _isBattle = true;
            }
        }
        // hit
        protected override void OnDamageTrigger()
        {
            // 创建特效
            var effectId = SkillData.SkillDef.TargetEffect;
            var scale = Attacker.FighterData.ModelScale * Vector3.one;
            var pos = Attacker.Position;
            var rotate = Attacker.UnitContainerTransform.rotation;
            Effect.CreateSceneEffect(effectId, pos,scale ,rotate);
            
            // 伤害
            var targetList = FindTarget();
            for (int i = 0; i < targetList.Count; i++)
            {
                var target = targetList[i];
                // 造成伤害
                var damage = Attacker.FighterData.CalcAttackDamage(SkillData.SkillDef,target.FighterData.MaxHp);
                target.DoDamage(Attacker,damage);
            }
            // 触发震动
            Shake();
        }

        // 发射子弹
        private void FireBullet()
        {
            // 子弹数量
            var bulletCount = SkillData.SkillDef.BulletCount;
            // 范围角度
            var aoeAngle = SkillData.SkillDef.AoeAngle;
            var averageAngle = aoeAngle / bulletCount;    // 平均角度
            // 范围半径
            var radius = SkillData.SkillDef.DamageRadius + Attacker.Radius;
            
            for (int i = 0; i < bulletCount; i++)
            {
                // 子弹方向角度
                var bulletAngle =aoeAngle / 2 - (averageAngle / 2 + averageAngle * i);
                // 子弹移动方向
                var bulletDir =Quaternion.Euler(0, bulletAngle, 0) * Attacker.UnitContainerTransform.forward;
                bulletDir = bulletDir.normalized;
                // 子弹开始位置
                var startPos = Attacker.Position;
                // 子弹结束位置
                var endPos = Attacker.Position + bulletDir * radius;
                // 创建子弹
                BattleModel.Instance.AddBullet(SkillData,Attacker,startPos,endPos,Target); 
            }
        }
        
        protected List<FighterUnit> FindTarget()
        {
            // 半径
            var raduis = SkillData.SkillDef.DamageRadius * Attacker.FighterData.ModelScale;
            // 角度
            var damageAngle = SkillData.SkillDef.AoeAngle / 2;
            
            // 找到攻击范围内的目标
            List<FighterUnit> targetList = new List<FighterUnit>();
            switch (Attacker.FighterData.TargetType)
            {
                case UnitType.Boss:
                case UnitType.MonsterBoss:
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    // 攻击范围内的怪兽
                    targetList = BattleManager.Instance.FindMonsterList(Attacker.Position, raduis);
                    // 判断boss在不在攻击范围
                    FighterUnit boss = BattleManager.Instance.BossUnit;
                    if (boss != null)
                    {
                        if(Vector3.Distance(boss.Position,Attacker.Position) < raduis)
                            targetList.Add(boss);
                    }
                    break;
                case UnitType.Role:
                    // 攻击范围内的玩家
                    targetList = BattleManager.Instance.FindHeroList(Attacker.Position, raduis);
                    break;
            }
            
            // 判断是否在角度内
            for (int i = targetList.Count - 1; i >= 0; i--)
            {
                var target = targetList[i];
                var dir = target.Position - Attacker.Position;
                dir = dir.normalized;
                var curAngle = Vector3.Angle(dir, Attacker.UnitContainerTransform.forward);
                // 大于伤害角度移除
                if (Mathf.Abs(curAngle) > damageAngle)
                    targetList.Remove(target);
            }

            return targetList;
        }
        
    }
}