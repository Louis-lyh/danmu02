﻿using System.Collections.Generic;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class CircularSkillAction: SkillActionBase 
    {
        // 是否攻击
        protected bool _isBattle;
        // 
        public CircularSkillAction(SkillData skillData)
            : base(skillData)
        {
            _isBattle = false;
        }


        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束 或者已经攻击过了结束
            if (Attacker == null || Attacker.IsDeath || _isBattle)
            {
                SkillActionEnd(); // 结束
                return;
            }
            // 触发伤害
            OnDamageTrigger();
            // 击中特效
            AddSkillEffect();
            // 触发震动
            Shake();
            // 记录攻击
            _isBattle = true;
        }

        // hit
        protected override void OnDamageTrigger()
        {
            // 伤害半径
            var damageRadius = SkillData.SkillDef.DamageRadius + Attacker.Radius;
            
            var targetList = FindTarget(damageRadius);
            for (int i = 0; i < targetList.Count; i++)
            {
                var target = targetList[i];
                // 造成伤害
                var damage = Attacker.FighterData.CalcAttackDamage(SkillData.SkillDef,target.FighterData.MaxHp);
                target.DoDamage(Attacker,damage);
                // 击飞
                target.BehaviorData.SetHitFly(false);
            }
        }
        
        // 技能特效
        protected override void AddSkillEffect()
        {
            // 特效id
            var effectId = SkillData.SkillDef.TargetEffect;
            // 伤害半径
            var damageRadius = SkillData.SkillDef.DamageRadius + Attacker.Radius;
            var damageRange = damageRadius * 2;   
            // 播放特效
            Effect.CreateSceneEffect(effectId,Attacker.Position,damageRange * Vector3.one,Quaternion.identity);
        }

        // 找到目标
        protected List<FighterUnit> FindTarget(float raduis)
        {
            List<FighterUnit> targetList = new List<FighterUnit>();
            switch (Attacker.FighterData.TargetType)
            {
                case UnitType.Boss:
                case UnitType.MonsterBoss:
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    targetList = BattleManager.Instance.FindMonsterList(Attacker.Position, raduis);
                    // 玩家boss
                    FighterUnit boss = BattleManager.Instance.BossUnit;
                    if (boss != null)
                    {
                        if(Vector3.Distance(boss.Position,Attacker.Position) < raduis)
                            targetList.Add(boss);
                    }
                    break;
                case UnitType.Role:
                    // 攻击范围内的玩家
                    targetList = BattleManager.Instance.FindHeroList(Attacker.Position, raduis);
                    break;
            }

            return targetList;
        }

    }
}