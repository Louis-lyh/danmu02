﻿using UnityEngine;

namespace GameInit.Game
{
    /// <summary>
    /// 默认远距离攻击
    /// </summary>
    public class DefaultRemoteSkillAction : SkillActionBase 
    {
        // 是否发射子弹
        private bool _isFireBullet;
        public DefaultRemoteSkillAction(SkillData skillData) 
            : base(skillData)
        {
            _isFireBullet = false;
        }
        
        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束
            if (Attacker == null || Attacker.IsDeath)
            {
                SkillActionEnd(); // 结束
                return;
            }
            
            // 造成伤害
            if (!_isFireBullet)
            {
                _isFireBullet = true;
                // 发射子弹
                FireBullet();
                // 结束
                SkillActionEnd();
            }
        }
        // 发射子弹
        private void FireBullet()
        {
            if(Target.UnitContainerTransform == null)
                return;
            
            // 子弹开始位置
            var prePos = Attacker.UnitContainerTransform.forward * (Attacker.FighterData.ModelScale * 0.25f);
            var startPos = Attacker.Position + Vector3.up * (Attacker.FighterData.ModelScale * 0.6f) + prePos;
            // 结束位置
            var endPos = Target.Position;
            // 创建子弹
            BattleModel.Instance.AddBullet(SkillData,Attacker,startPos,endPos,Target);
        }
        
       
    }
}