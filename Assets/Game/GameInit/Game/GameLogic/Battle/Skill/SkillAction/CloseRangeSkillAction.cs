
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    /// <summary>
    /// 近距离单体攻击
    /// </summary>
    public class CloseRangeSkillAction : SkillActionBase 
    {
        public CloseRangeSkillAction(SkillData skillData) 
            : base(skillData)
        {
            
        }

        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束
            if (Attacker == null || Attacker.IsDeath)
            {
                SkillActionEnd(); // 结束
                return;
            }
            
            // 造成伤害
            if (!IsDamage)
            {
                // 触发伤害
                OnDamageTrigger();
                // 添加技能特效
                AddSkillEffect();
                IsDamage = true;
                // 结束
                SkillActionEnd();
            }
        }
        // 伤害触发
        protected override void OnDamageTrigger()
        { 
            
            if (Attacker == null || Attacker.IsDeath)
                return;
            
            if (Target == null || Target.IsDeath)
                return;
            
            // 造成伤害
            var damage = Attacker.FighterData.CalcAttackDamage(SkillData.SkillDef,Target.FighterData.MaxHp);
            
            Target.DoDamage(Attacker,damage);
        }
        // 添加技能特效
        protected override void AddSkillEffect()
        {
            var effectName = SkillData.SkillDef.TargetEffect;
            // 没有特效退出
            if(string.IsNullOrEmpty(effectName))
                return;

            // 创建特效
            Effect.CreateEffect(effectName, Attacker.UnitContainerTransform, 
                Vector3.zero,Vector3.one ,Quaternion.identity,true);
        }
    }
}

