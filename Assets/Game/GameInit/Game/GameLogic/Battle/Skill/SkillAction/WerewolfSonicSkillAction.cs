﻿using System.Collections.Generic;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public class WerewolfSonicSkillAction : FanShapedSkillAction
    {
        // 持续时间
        private float _duration;
        public WerewolfSonicSkillAction(SkillData skillData) 
            : base(skillData)
        {
            _duration = 1.5f;
        }

        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束 或者已经攻击过了结束
            if (Attacker == null || Attacker.IsDeath || _duration < 0)
            {
                SkillActionEnd(); // 结束
                return;
            }
            // 时间减少
            _duration -= deltaTime;

            
            
            if (!_isBattle)
            {
                // 触发伤害
                OnDamageTrigger();
                // 触发震动
                Shake();
                // 记录
                _isBattle = true;
            }

            
        }
        // hit
        protected override void OnDamageTrigger()
        {
            // 创建特效
            var effectId = SkillData.SkillDef.TargetEffect;
            var effectScale = Vector3.one * Attacker.FighterData.ModelScale;
            var effectRotation = Attacker.UnitContainerTransform.rotation;
            Effect.CreateSceneEffect(effectId, Attacker.UnitContainerTransform.position,effectScale,effectRotation);
            // 伤害
            var targetList = FindTarget();
            for (int i = 0; i < targetList.Count; i++)
            {
                var target = targetList[i];
                // 造成伤害
                var damage = Attacker.FighterData.CalcAttackDamage(SkillData.SkillDef,target.FighterData.MaxHp);
                target.DoDamage(Attacker,damage);

                var hitDir = target.Position - Attacker.Position;
                // 击退
                if(SkillData.SkillDef.BeatBack > 0)
                    target.SetHitBack(true,SkillData.SkillDef.BeatBack,hitDir.normalized);
            }
        }

        protected override void OnSkillActionEnd()
        {
            // 技能结束回调
            BattleModel.Instance.WerewolfSonicSkillEnd();
        }
    }
}