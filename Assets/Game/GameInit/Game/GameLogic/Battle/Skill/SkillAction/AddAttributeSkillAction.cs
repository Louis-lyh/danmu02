﻿namespace GameInit.Game
{
    /// <summary>
    /// 增加属性技能
    /// </summary>
    public class AddAttributeSkillAction : SkillActionBase 
    {
        // 是否触发
        private bool _isTrigger = false;
        public AddAttributeSkillAction(SkillData skillData) 
            
            : base(skillData)
        {
            
        }

        protected override void OnUpdate(float deltaTime)
        {
            // 攻击者死亡攻击结束 或者已经攻击过了结束
            if (Attacker == null || Attacker.IsDeath || _isTrigger)
            {
                SkillActionEnd(); // 结束
                return;
            }
            // 触发buff
            OnBuffTrigger();
            _isTrigger = true;
        }
        // 触发buff
        protected override void OnBuffTrigger()
        {
            // buffId
            var buffId = SkillData.SkillDef.BuffId;
            BattleModel.Instance.AddBuff(buffId,Attacker.FighterData,Attacker.FighterData);
        }
    }
}