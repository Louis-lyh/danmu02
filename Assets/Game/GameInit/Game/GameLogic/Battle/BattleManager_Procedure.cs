﻿using Cysharp.Threading.Tasks;
using GameInit.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 游戏流程
    /// </summary>
    public partial class BattleManager
    {
        #region 游戏流程
        // 准备开始
        public void ReadyBattle()
        {
            IsReadyBattle = true;
        }

        // 开始游戏
        public void StartBattle()
        {
            IsBattle = true;
        }
        // 游戏结束
        public void ExitBattle()
        {
            IsBattle = false;
            IsReadyBattle = false;
        }
        // 设置游戏天数
        private void SetDay(int day,bool isNight)
        {
            Day = day;
            IsNight = isNight;
        }
        
        /// <summary>
        /// 开始流程
        /// </summary>
        public void StartProcedure(int day)
        {
            switch (BattleType)
            {
                case BattleType.Normal:
                    // 开始流程
                    StartProcedure_Normal(day);
                    break;
                case BattleType.FieldBoss:
                    // 开始流程
                    StartProcedure_FieldBoss(day);
                    break;
            }
        }

        /// <summary>
        /// 进入夜晚
        /// </summary>
        public void EnterNight(int day)
        {
            switch (BattleType)
            {
                case BattleType.Normal:
                    // 进入夜晚
                    NightEnter_Normal(day);
                    break;
                case BattleType.FieldBoss:
                    // 进入夜晚
                    NightEnter_FieldBoss(day);
                    break;
            }
        }
        
        /// <summary>
        /// 重置流程
        /// </summary>
        /// <param name="day"></param>
        public void ResetProcedure(int day)
        {
            switch (BattleType)
            {
                case BattleType.Normal:
                    // 重置流程
                    ResetProcedure_Normal(day);
                    break;
                case BattleType.FieldBoss:
                    // 重置流程
                    ResetProcedure_FieldBoss(day);
                    break;
            }
        }
        
        /// <summary>
        /// 结束白天到夜晚的过度
        /// </summary>
        public void EndExcess(int day)
        {
            switch (BattleType)
            {
                case BattleType.Normal:
                    // 过度
                    EndExcess_Normal(day);
                    break;
                case BattleType.FieldBoss:
                    // 过度
                    EndExcess_FieldBoss(day);
                    break;
            }
        }

        /// <summary>
        /// 战斗单位死亡
        /// </summary>
        public void UnitOnDeath(UnitType unitType)
        {
            switch (BattleType)
            {
                case BattleType.Normal:
                    // 判断常规模式的阶段是否结束
                    JudgeNightEnd_Normal(unitType);
                    break;
                    
                case BattleType.FieldBoss:
                    // 判断野外boss是否结束是否结束
                    JudgeEnd_FieldBoss(unitType);
                    break;
            }
        }
        #endregion

        #region 常规模式
        
        /// <summary>
        /// 开始流程
        /// </summary>
        private void StartProcedure_Normal(int curDay)
        {
            //开始刷新小怪
            MonsterManager.Instance.Start();
            //开始刷新采集物
            FruitManager.Instance.Start();
            // 更新天数
            SetDay(curDay,false);
        }
        
        /// <summary>
        /// 进入夜晚
        /// </summary>
        private void NightEnter_Normal(int curDay)
        {
            //停止刷新小怪
            MonsterManager.Instance.Stop();
            //停止刷新采集物
            FruitManager.Instance.Stop();
            //弹出狼人出现UI - UI表现结束 - 狼人变身表现
            BattleModel.Instance.StartBossStage();
            // 所有玩家停止攻击
            SetAllFighterAttackStop();
                    
            // 设置天数
            SetDay(curDay,true);
        }
        /// <summary>
        /// 重置夜晚
        /// </summary>
        /// <param name="day"></param>
        private void ResetProcedure_Normal(int day)
        {
            // 回复生命值
            SetAllFighterHp(0,100);
            // 复活玩家
            ReviveAllRole();
            // 更新天数
            SetDay(day,false);
        }
        
        /// <summary>
        /// 结束夜晚到白天的过度
        /// </summary>
        private void EndExcess_Normal(int day)
        {
            //开始刷新小怪
            MonsterManager.Instance.Start();
            //开始刷新采集物
            FruitManager.Instance.Start();
            // 隐藏boss血条
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowBossHp,null);
            // 更新天数
            SetDay(day,false);
        }
        

        /// <summary>
        /// 判断常规模式的阶段是否结束
        /// </summary>
        /// <param name="unitType"></param>
        private void JudgeNightEnd_Normal(UnitType unitType)
        {
            switch (unitType)
            {
                case UnitType.Role:
                    // 玩家全死亡夜晚结束
                    if (AllRole().Count == 0)
                    {
                        ServerManager.Instance.ShowLogText("------ 玩家全死亡夜晚结束 ------");
                        NightEnd_Normal(false);
                    }
                    break;
                case UnitType.MonsterBoss:
                case UnitType.Boss:
                    // boss 死亡夜晚结束
                    NightEnd_Normal(true);
                    break;
            }
        }


        /// <summary>
        /// 常规模式晚上结束
        /// </summary>
        /// <param name="isSkillBoss">是否击杀boss</param>
        private async void NightEnd_Normal(bool isSkillBoss)
        {
            if(!IsNight)
                return;
            IsNight = false;
            
            // 更换攻击目标
            SetAllFighterAttackType(UnitType.Monster);

            //------ 处理boss ------
            // 怪兽boss消失
            if (MonsterBoss != null)
            {
                await UniTask.Delay(5000);
                MonsterBoss?.DoDamage(null,new DamageInfo(int.MaxValue,true));
            }
            // 复活boss玩家
            else
                BattleModel.Instance.ReviveBossPlayer();
            
            // 夜晚结束
            BattleModel.Instance.DispatchEvent(BattleModel.EventNightEnd,isSkillBoss);
            
            // 杀死所有小狼人
            KillAllLittleWerewolf();
        }

        #endregion
        
        #region 野外boss模式
        
        /// <summary>
        /// 开始流程
        /// </summary>
        private void StartProcedure_FieldBoss(int curDay)
        {
            // 更新天数
            SetDay(curDay,false);
            
            //开始刷新采集物
            FruitManager.Instance.Start();
            // 刷新守卫
            
        }
        
        /// <summary>
        /// 进入夜晚
        /// </summary>
        private void NightEnter_FieldBoss(int curDay)
        {
            // 设置天数
            SetDay(curDay,true);
            
            // 判断是否失败
            JudgeEnd_FieldBoss(UnitType.MonsterBoss);
            // 是否需要停止刷新
            FruitManager.Instance.Stop();

        }
        /// <summary>
        /// 重置夜晚
        /// </summary>
        /// <param name="day"></param>
        private void ResetProcedure_FieldBoss(int day)
        {
            // 更新天数
            SetDay(day,false);
        }
        
        /// <summary>
        /// 结束夜晚到白天的过度
        /// </summary>
        private void EndExcess_FieldBoss(int day)
        {
            SetDay(day,false);
        }
        

        /// <summary>
        /// 判断否结束
        /// </summary>
        /// <param name="unitType"></param>
        private void JudgeEnd_FieldBoss(UnitType unitType)
        {
            // 判断野外boss是否结束
            if (IsNight)
            {
                // 夜晚结束
                if(BattleModel.Instance.FieldBossCount == 0)
                    BattleModel.Instance.DispatchEvent(BattleModel.EventNightEnd,true);
            }
        }

        #endregion
        
    }
}