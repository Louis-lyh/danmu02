using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game;
using GameInit.Framework;
using GameInit.Game;
using GameInit.Game.Pet;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameInit.Game
{
    public enum BattleType
    {
        Normal,    // 常规模式
        FieldBoss, // 野外boss
    }

    public partial class BattleManager : Singleton<BattleManager>
    {
        // 游戏模式
        public BattleType BattleType { get; private set; }
        // 公会id
        public int GuildId{ get; private set; }
        // 准备开启战斗
        public bool IsReadyBattle{ get; private set; }
        // 是否开启战斗
        public bool IsBattle { get; private set; }
        // 游戏天数
        public int Day { get; private set; }
        // 晚上
        public bool IsNight{ get; private set; }
        // 游戏进行时间
        public float NightTime{ get; private set; }
        // 暂停游戏
        public bool IsPause{ get;  set; }
        // 是否接受入侵
        public bool IsPeace{ get;  set; }
        
        // 英雄字典
        private Dictionary<long, FighterUnitRole> _dictAllRoleFighter;
        // 怪兽
        private Dictionary<long, Monster> _dicAllMonsterFighter;
        // 果实
        private Dictionary<long, FruitUnit> _dicAllFruit;
        // 宠物
        private Dictionary<long, PetUnit> _dicAllPet;
        // 子弹
        private Dictionary<long, BulletBase> _dicAllBullet;
        // 怪兽守卫
        private Dictionary<long, MonsterGuard> _dicMonsterGuard;
        // 野外boss
        private Dictionary<long, FieldBoss> _dicFieldBoss;
        // 玩家boss
        private FighterUnitBoss PlayerBoss;
        // 怪兽boss
        private MonsterBoss MonsterBoss;
        // boss
        public FighterUnit BossUnit { get { if (PlayerBoss != null) return PlayerBoss; return MonsterBoss; } }
        // 游戏单位节点
        private Transform _unitRoot;
        public Transform UnitRoot => _unitRoot;
        // 震动CD
        private float _shakeTime;
        // 积分池
        public long ScorePool { get; private set; }
        
        // 初始化
        public void Init(BattleType battleType)
        {
            // 游戏模式
            BattleType = battleType;
            
            // 公会id
            GuildId = ServerManager.Instance.GuildId;
            ServerManager.Instance.ShowLogText("BattleManager.Init -> guildId : "+GuildId);

            // 重置晚上时间时间
            NightTime = 0;
            // 关闭暂停
            IsPause = false;
            
            //找到根节点
            var unitRootGo = GameObject.Find("UnitRoot");
            if(unitRootGo == null)
                unitRootGo = new GameObject("UnitRoot");
            GameObject.DontDestroyOnLoad(unitRootGo);
            _unitRoot = unitRootGo.transform;
            // 注册事件
            AddEvent();
            // 初始化字典
            _dictAllRoleFighter = new Dictionary<long, FighterUnitRole>();
            _dicAllMonsterFighter = new Dictionary<long, Monster>();
            _dicMonsterGuard = new Dictionary<long, MonsterGuard>();
            _dicAllBullet = new Dictionary<long, BulletBase>();
            _dicAllFruit = new Dictionary<long, FruitUnit>();
            _dicAllPet = new Dictionary<long, PetUnit>();
            _dicFieldBoss = new Dictionary<long, FieldBoss>();
            // 初始化战斗模块
            BattleModel.Instance.Init();
            // 初始化技能模块
            SkillDataModel.Instance.Init();
        }

        #region battle event
        // 添加战斗单位
        public void AddFighterUnit(FighterUnitData data)
        {
            switch (data.UnitType)
            {
                case UnitType.Role:
                    // 添加英雄
                    AddFighterRole(data);
                    break;
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    // 添加怪兽
                    AddFighterMonster(data);
                    break;
                case UnitType.Boss:
                    // 添加boss
                    AddFighterBoss(data);
                    break;
                case UnitType.MonsterBoss:
                    // 添加monster boss
                    AddMonsterBoss(data);
                    break;
                case UnitType.FieldBoss:
                    // 添加FieldBoss
                    AddFieldBoss(data);
                    break;
                case UnitType.MonsterGuard:
                    AddMonsterGuard(data);
                    break;
                case UnitType.Fruit:
                    // 添加果实
                    AddFruit(data);
                    break;
                case UnitType.Pet:
                    // 添加宠物
                    AddPet(data);
                    break;
            }
        }
        
        /// <summary>
        /// 移除战斗单位
        /// </summary>
        public void RemoveFighterUnit(FighterUnitData data)
        {
            switch (data.UnitType)
            {
                case UnitType.Role:
                    // 移除英雄
                    RemoveRole(data.UnitID);
                    // 移除英雄信息
                    BattleModel.Instance.RemoveRoleData((data as RoleData).UserId);
                    break;
                case UnitType.Monster:
                case UnitType.SmallWerewolf:
                    // 移除怪兽
                    RemoveMonster(data.UnitID);
                    // 移除boss开始阶段怪兽
                    MonsterManager.Instance.RemoveMonsterInStartBoss(data.UnitID);
                    break;
                case UnitType.MonsterGuard:
                    RemoveMonsterGuard(data.UnitID);
                    break;
                case UnitType.Boss:
                    // 销毁玩家boss
                    PlayerBoss.Dispose();
                    PlayerBoss = null;
                    break;
                case UnitType.Fruit:
                    // 移除水果
                    RemoveFruit(data.UnitID);
                    break;
                case UnitType.MonsterBoss:
                    // 销毁怪兽boss
                    MonsterBoss.Dispose();
                    MonsterBoss = null;
                    break;
                case UnitType.FieldBoss:
                    // 移除野外boss
                    RemoveFieldBoss(data.UnitID);
                    break;
                case UnitType.Pet:
                    RemovePet(data.UnitID);
                    break;
            }
        }

        // 添加英雄
        private async void AddFighterRole(FighterUnitData data)
        {
            // 创建英雄实例
            var unit = await UnitFactory.CreatUnit<FighterUnitRole>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterRole() => 创建玩家失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot, data.StartPos);
            // 创建寻路代理
            unit.CreateAgent();
            // 初始化欲望
            unit.BehaviorData.InitUpgradeDesire();
            // 加入字典
            _dictAllRoleFighter.Add(data.UnitID, unit);
        }
        // 添加怪兽
        private async void AddFighterMonster(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<Monster>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterMonster() => 创建怪兽失败失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 创建寻路代理
            unit.CreateAgent();
            // 加入字典
            _dicAllMonsterFighter.Add(data.UnitID,unit);
        }
        
        // 添加MonsterGuard
        private async void AddMonsterGuard(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<MonsterGuard>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddMonsterBoss() => 创建Monster Boss失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 创建寻路代理
            unit.CreateAgent();
            // 加入字典
            _dicMonsterGuard.Add(data.UnitID,unit);
        }

        // 添加果实
        private async void AddFruit(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<FruitUnit>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterMonster() => 创建果实失败]");
                return;
            }

            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 加入字典
            if(!_dicAllFruit.ContainsKey(data.UnitID))
                _dicAllFruit.Add(data.UnitID,unit);
            else
            {
                unit.Dispose();
                Logger.LogError("添加果实重复"+data.UnitID);
            }

        }
        // 添加宠物
        private async void AddPet(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<PetUnit>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterMonster() => 创建宠物失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 创建寻路代理
            unit.CreateAgent();
            // 加入字典
            _dicAllPet.Add(data.UnitID,unit);
        }

        // 添加Boss
        private async void AddFighterBoss(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<FighterUnitBoss>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterMonster() => 创建boss失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 加入字典
            PlayerBoss = unit;
        }
        // 添加MonsterBoss
        private async void AddMonsterBoss(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<MonsterBoss>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddMonsterBoss() => 创建Monster Boss失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 加入字典
            MonsterBoss = unit;
        }
        
        // 添加野外boss
        private async void AddFieldBoss(FighterUnitData data)
        {
            // 创建怪兽实例
            var unit = await UnitFactory.CreatUnit<FieldBoss>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFieldBoss() => 创建Monster Boss失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot,data.StartPos);
            // 加入字典
            if(!_dicFieldBoss.ContainsKey(data.UnitID))
                _dicFieldBoss.Add(data.UnitID,unit);
        }

        // 添加子弹
        public async void AddBullet(BulletData data)
        {
            // 创建子弹实例
            var unit = await UnitFactory.CreatUnit<BulletBase>(data);
            if (unit == null)
            {
                Logger.LogError("[BattleManager.AddFighterMonster() => 创建boss失败]");
                return;
            }
            // 设置父节点和开始位置
            unit.SetParent(_unitRoot, data.Pos, Vector3MathUtils.GetEulerAngles(data.Pos, data.TargetPos));
            // 加入字典
            _dicAllBullet.Add(data.UnitID,unit);
        }
        // 添加buff
        public void AddBuff(FighterUnitData unitData,BuffData buff,BuffDef def)
        {
            var unit = GetFighterByUID(unitData.UnitID);

            // 判断是否是boss
            if (PlayerBoss != null && unitData.UnitID == PlayerBoss.FighterData.UnitID)
                unit = PlayerBoss;
            
            // 指定单位添加buff
            unit.AddBuff(buff,def);
        }

        // 移除英雄
        private void RemoveRole(long unitID)
        {
            if (_dictAllRoleFighter.ContainsKey(unitID))
            {
                _dictAllRoleFighter[unitID].Dispose();
                _dictAllRoleFighter.Remove(unitID);
            }
        }
        // 移除怪兽
        private void RemoveMonster(long unitID)
        {
            if (_dicAllMonsterFighter.ContainsKey(unitID))
            {
                _dicAllMonsterFighter[unitID].Dispose();
                _dicAllMonsterFighter.Remove(unitID);
            }
        }
        // 移除所有怪兽
        public void RemoveAllMonster()
        {
            var keys = _dicAllMonsterFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var monster = _dicAllMonsterFighter[key];
                if(monster.UnitType != UnitType.Monster || monster.IsDeath)
                    continue;
                
                _dicAllMonsterFighter[key].Dispose();
                _dicAllMonsterFighter.Remove(key);
            }
        }
        // 移除怪兽守卫
        private void RemoveMonsterGuard(long uniId)
        {
            if (_dicMonsterGuard.ContainsKey(uniId))
            {
                _dicMonsterGuard[uniId].Dispose();
                _dicMonsterGuard.Remove(uniId);
            }
        }
        // 移除所有怪兽守卫
        public void RemoveAllMonsterGuard()
        {
            var keys = _dicMonsterGuard.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                var unit = _dicMonsterGuard[key];
                if(unit.IsDeath)
                    continue;
                
                _dicMonsterGuard[key].Dispose();
                _dicMonsterGuard.Remove(key);
            }
        }
        
        // 移除野外boss
        private void RemoveFieldBoss(long uniId)
        {
            if (_dicFieldBoss.ContainsKey(uniId))
            {
                _dicFieldBoss[uniId].Dispose();
                _dicFieldBoss.Remove(uniId);
            }
            
            // 移除数据
            BattleModel.Instance.RemoveFieldBossData(uniId);
        }

        // 移除水果
        private void RemoveFruit(long unitID)
        {
            if (_dicAllFruit.ContainsKey(unitID))
            {
                _dicAllFruit[unitID].Dispose();
                _dicAllFruit.Remove(unitID);
            }
        }
        // 移除所有水果
        public void RemoveAllFruit()
        {
            var keys = _dicAllFruit.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var key = keys[i];
                _dicAllFruit[key].Dispose();
                _dicAllFruit.Remove(key);
            }
        }
        // 移除宠物
        private void RemovePet(long unitId)
        {
            if (_dicAllPet.ContainsKey(unitId))
            {
                _dicAllPet[unitId].Dispose();
                _dicAllPet.Remove(unitId);
            } 
        }

        // 移除子弹
        public void RemoveBullet(long unitId)
        {
            if (_dicAllBullet.ContainsKey(unitId))
            {
                _dicAllBullet[unitId].Dispose();
                _dicAllBullet.Remove(unitId);
            }
        }
        
        // 复活玩家
        private uint _reviveTimer;
        public async void ReviveAllRole()
        {
            // 延迟复活
            await Task.Delay(4000);
            
            var keys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var unit = _dictAllRoleFighter[keys[i]];
                if (unit.IsDeath)
                {
                    unit.BehaviorData.Relive();
                    await Task.Delay(20);
                }
            }
        }
        
        // 杀死小狼人
        private async void KillAllLittleWerewolf()
        {
            await UniTask.Delay(5000);
            var keys = _dicAllMonsterFighter.Keys.ToList();
            for (int i = 0; i < keys.Count; i++)
            {
                var unit = _dicAllMonsterFighter[keys[i]];
                if (unit.UnitType == UnitType.SmallWerewolf)
                {
                   unit.DoDamage(null,new DamageInfo(int.MaxValue,false));
                }
            }
            
        }
        private void AddEvent(){}

        private void RemoveEvent(){}

        #endregion

        #region Update
        // 更新函数
        public void Update(float deltaTime)
        {
            // 暂停游戏退出
            if(IsPause)
                return;
            
            //RVO 寻路
            RVOManager.Instance.Update(deltaTime);
            
            if (IsReadyBattle)
            {
                // 更新战斗单位逻辑
                UpdateUnit(deltaTime);
                // 玩家排行榜
                UpdatePlayerRanking(deltaTime);
                // 更新子弹逻辑
                UpdateBullet(deltaTime);
            }
            
            if(!IsBattle)
                return;
            
            // 更新震动
            UpdateShake(deltaTime);
            // 刷新怪兽
            MonsterManager.Instance.Update(deltaTime);
            // 刷新果实
            FruitManager.Instance.Update(deltaTime);
            
            // 处于晚上
            InNight(deltaTime);
        }
        // 处于晚上
        private void InNight(float deltaTime)
        {
            if (!IsNight)
            {
                // 重置时间
                NightTime = 0;
                return;  
            }
            // 晚上时间累计
            NightTime += deltaTime;
        }

        // 更新战斗单位逻辑
        private void UpdateUnit(float deltaTime)
        {
            // 英雄
            if (_dictAllRoleFighter != null && _dictAllRoleFighter.Count != 0)
            {
                var keys = _dictAllRoleFighter.Keys.ToList();
                // 更新英雄
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    _dictAllRoleFighter[key].Update(deltaTime);
                }
            }
            
            // 怪兽
            if (_dicAllMonsterFighter != null && _dicAllMonsterFighter.Count != 0)
            {
                var keys = _dicAllMonsterFighter.Keys.ToList();
                // 更新怪兽逻辑
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    _dicAllMonsterFighter[key].Update(deltaTime);
                }
            }
            
            // 怪兽守卫
            if (_dicMonsterGuard != null && _dicMonsterGuard.Count != 0)
            {
                var keys = _dicMonsterGuard.Keys.ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    _dicMonsterGuard[keys[i]].Update(deltaTime);
                }

            }

            // 宠物
            if (_dicAllPet != null && _dicAllPet.Count != 0)
            {
                var keys = _dicAllPet.Keys.ToList();
                // 更新宠物逻辑
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    _dicAllPet[key].Update(deltaTime);
                }
            }
            
            // 野外boss
            if (_dicFieldBoss != null && _dicFieldBoss.Count != 0)
            {
                var keys = _dicFieldBoss.Keys.ToList();
                // 更新宠物逻辑
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    _dicFieldBoss[key].Update(deltaTime);
                }
            }
            
            // 更新boss逻辑
            PlayerBoss?.Update(deltaTime);
            // 更新怪兽boss逻辑
            MonsterBoss?.Update(deltaTime);
        }
        // 更新子弹逻辑
        private void UpdateBullet(float deltaTime)
        {
            if (_dicAllBullet != null && _dicAllBullet.Count > 0)
            {
                var keys = _dicAllBullet.Keys.ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    _dicAllBullet[key].Update(deltaTime);
                }
            }
        }
        // 更新震动
        private void UpdateShake(float deltaTime)
        {
            if (_shakeTime > 0)
            {
                _shakeTime -= deltaTime;
                if (_shakeTime <= 0) _shakeTime = 0f;
            }
        }
        
        // 刷新玩家排行榜
        private float _updatePlayerRankingInterval;
        private void UpdatePlayerRanking(float deltaTime)
        {
            if(_dictAllRoleFighter == null)
                return;
            
            // 间隔刷新
            _updatePlayerRankingInterval += deltaTime;
            if(_updatePlayerRankingInterval < 0.3f)
                return;
            _updatePlayerRankingInterval = 0;
            
             var unit = _dictAllRoleFighter.Values.ToList();
             var unitData = new List<RoleData>();    
             for (int i = 0; i < unit.Count; i++)
             {
                 // 加入列表
                 unitData.Add(unit[i].FighterData as RoleData);
             }

            // 刷新玩家排行
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventUpdatePlayerRanking,unitData);
        }

        #endregion

        #region 积分
        // 积分
        public void AddScore(int scoreCount)
        {
            // 加入积分池
            ScorePool += scoreCount;
            
            // 刷新积分池
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowScorePool,ScorePool);
        }
        // 清空
        public void ClearScore()
        {
            ScorePool = 0;
            // 刷新积分池
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventShowScorePool,ScorePool);
        }

        #endregion
        
       
       
       // Dispose
        public void Dispose()
        {
            RemoveEvent();
            
            // 停止时间
            // StopTime();
            
            // 清理化战斗模块
            BattleModel.Instance.DisposeModel();
            // 清理技能模块
            SkillDataModel.Instance.DisposeModel();
            
            // 角色
            var roleKeys = _dictAllRoleFighter.Keys.ToList();
            for (int i = 0; i < roleKeys.Count; i++)
            {
                var key = roleKeys[i];
                var role = _dictAllRoleFighter[key];
                role.Dispose();
            }
            _dictAllRoleFighter.Clear();
            
            // 怪兽
            var monsterKeys = _dicAllMonsterFighter.Keys.ToList();
            for (int i = 0; i < monsterKeys.Count; i++)
            {
                var key = monsterKeys[i];
                var monster = _dicAllMonsterFighter[key];
                monster.Dispose();
            }
            _dicAllMonsterFighter.Clear();
            
            // 怪兽守卫
            var monsterGuardKeys = _dicMonsterGuard.Keys.ToList();
            for (int i = 0; i < monsterGuardKeys.Count; i++)
            {
                var key = monsterGuardKeys[i];
                var monsterGuard = _dicMonsterGuard[key];
                monsterGuard.Dispose();
            }
            _dicMonsterGuard.Clear();

            // 子弹
            var bulletKeys = _dicAllBullet.Keys.ToList();
            for (int i = 0; i < bulletKeys.Count; i++)
            {
                var key = bulletKeys[i];
                var bullet = _dicAllBullet[key];
                bullet.Dispose();
            }
            _dicAllBullet.Clear();
            
            // 果实
            var fruitKeys = _dicAllFruit.Keys.ToList();
            for (int i = 0; i < fruitKeys.Count; i++)
            {
                var fruitKey = fruitKeys[i];
                var fruit = _dicAllFruit[fruitKey];
                fruit.Dispose();
            }
            _dicAllFruit.Clear();
            
            // 宠物
            var petKeys = _dicAllPet.Keys.ToList();
            for (int i = 0; i < petKeys.Count; i++)
            {
                var petKey = petKeys[i];
                var pet = _dicAllPet[petKey];
                pet.Dispose();
            }
            _dicAllPet.Clear();
            
            // boss
            if (PlayerBoss != null)
            {
                PlayerBoss.Dispose();
                PlayerBoss = null;
            }
            
            // 怪兽boss
            if (MonsterBoss != null)
            {
                MonsterBoss.Dispose();
                MonsterBoss = null;
            }
            
            // 野外boss
            var fieldBossKeys = _dicFieldBoss.Keys.ToList();
            for (int i = 0; i < fieldBossKeys.Count; i++)
            {
                var fieldBossKey = fieldBossKeys[i];
                var fieldBoss = _dicFieldBoss[fieldBossKey];
                fieldBoss.Dispose();
            }
            _dicFieldBoss.Clear();

            // 清空积分
            ClearScore();
        }
        
        // 震动
        public void Shake(float duration, float strength, int vibrato, float randomness)
        {
            if (_shakeTime > 0)
                return;
            
            Camera.main.DOShakePosition(duration, strength, vibrato, randomness);
            _shakeTime = GameConstantHelper.ShakeCd;
        }
    }
}