using UnityEngine;

namespace GameInit.Game
{
    public static class BattleConst
    {
        // 地图左下角起始点
        public static readonly Vector3 SrcPos = new Vector3(3f, 0f, 8f);
        // 地图1/9格子大小
        public static readonly Vector3 CellSize = new Vector3(3.666f, 0f, 5.333f);
        // Bossid
        public const int BossId = 200000;
        // 怪兽配置id
        public const int MonsterId = 990000;
        // 怪兽守卫id
        public const string MonsterGuardId = "990006,990007";
        // 小狼人
        public const int SmallWerewolfId = 980000;
        // 果实id
        public const int FruitId = 800000;
        // 糖果id
        public const int CandyId = 810000;
        // boss来临时间（秒）
        public const float BossComingTime = 300;
        
        
        
        // 判断是否在格子内
        public static bool IsOffGrid(Vector3 pos)
        {
            var minPoint = SrcPos;
            var maxPoint = SrcPos + CellSize * 3;

            if (pos.x < minPoint.x || pos.y < minPoint.y)
                return false;
            
            if (pos.x > maxPoint.x || pos.y > maxPoint.y)
                return false;

            return true;
        }

    }

    public static class ActionName
    {
        public const string Idle = "Idle";
        public const string Death = "Death";
        public const string Run = "Run";
        public const string Damage = "Damage";
        public const string Attack1 = "Attack1";    
        public const string Attack2 = "Attack2";
        public const string Attack3 = "Attack3";
        public const string Attack4 = "Attack4";
        public const string Attack5 = "Attack5";
        public const string Attack1Body = "Attack1Body";
        public const string Attack2Body = "Attack2Body";
        public const string Attack3Body = "Attack3Body";
        public const string Attack4Body = "Attack4Body";
        public const string Attack5Body = "Attack5Body";
        public const string Coming = "Coming";

    }
    // 战斗单位类型
    public enum UnitType

    {
        Role,    // 玩家
        Monster,    // 怪兽
        MonsterGuard, // 怪兽守卫
        SmallWerewolf, // 小狼人
        Boss,    // 狼人
        MonsterBoss,
        Bullet,    // 子弹
        Fruit,    // 果实
        Pet,    // 宠物
        FieldBoss,
    }
    // 战斗单位动画类型
    public enum UnitAnimationType
    {
        Default = 1,
        Gpu = 2,
    }
    // 技能伤害类型
    public enum SkillDamageType
    {
        Single = 0, // 单体
        AOE,    // 范围
    }
    // 伤害类型
    public static class BulletConst
    {
        public const int DAMAGE_TYPE_POINT = 1;    // 点
        public const int DAMAGE_TYPE_RECT = 2;    // 矩形
        public const int DAMAGE_TYPE_CIRCLE = 3;    // 圆
        public const int DAMAGE_TYPE_HERO = 4;    // 英雄
    }
    
    // 伤害信息
    public class DamageInfo
    {
        public DamageInfo(int value = 0,bool isCritical = false)
        {
            Value = value;
            IsCritical = isCritical;
        }

        public int Value;    // 伤害值
        public bool IsCritical; // 是否暴击
    }

}