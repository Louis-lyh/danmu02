﻿using System.Collections;
using System.Collections.Generic;
using Cysharp.Text;
using UnityEngine;
using GameInit.Framework;

namespace GameInit.Game
{
    public sealed class CameraManager : Singleton<CameraManager>
    {
        private delegate CameraController CreateController();
        private static CreateController[] _createCtrls =
        {
            () => { return new CameraLgCtrl(); },
        };

        private Transform _curCamera = null;
        private CameraController _curCtrl = null;

        private Dictionary<int, CameraController> _ctrlHash;

        #region property
        public Transform Camera
        {
            get { return _curCamera; }
        }
        public CameraController Ctrl
        {
            get { return _curCtrl; }
        }
        public Dictionary<int, CameraController> CtrlHash
        {
            get { return _ctrlHash; }
        }
        #endregion

        //初始化
        public void Init()
        {
            _ctrlHash = new Dictionary<int, CameraController>();
            for (int i = 0; i < _createCtrls.Length; ++i)
            {
                CameraController ctrl = _createCtrls[i]();
                _ctrlHash.Add(ctrl.GetCtrlType(), ctrl);
            }
        }

        //销毁
        public void Destroy()
        {
            _curCamera = null;
            _curCtrl = null;
            if (_ctrlHash != null)
            {
                _ctrlHash.Clear();
                _ctrlHash = null;
            }
        }
        
        //操作
        public void Update(float fTick)
        {
            if (null == _curCamera || null == _curCtrl)
                return;

            if (UpdateView(fTick)) return;
            
            Vector3 moveVelocity = UpdateMove(fTick);
            float moveZoom = UpdateZoom(fTick);
            
            _curCtrl.Update(moveVelocity, moveZoom, fTick);
        }
        private Vector3 UpdateMove(float fTick)
        {
            Vector3 dir = Vector3.zero;
            
            if (Input.GetKey(KeyCode.W))
            {
                dir += Vector3.forward;
            }
            if (Input.GetKey(KeyCode.S))
            {
                dir += Vector3.back;
            }
            if (Input.GetKey(KeyCode.A))
            {
                dir += Vector3.left;
            }
            if (Input.GetKey(KeyCode.D))
            {
                dir += Vector3.right;
            }

            return dir.normalized * (10f * fTick);
        }
        private float UpdateZoom(float fTick)
        {
            int dir = 0;
            
            if (Input.GetKey(KeyCode.Q))
            {
                dir += -1;
            }
            if (Input.GetKey(KeyCode.E))
            {
                dir += 1;
            }

            return dir * (12f * fTick);
        }
        private bool UpdateView(float fTick)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                _curCtrl.ChangeView(this, 1);
                return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                _curCtrl.ChangeView(this, 2);
                return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                _curCtrl.ChangeView(this, 3);
                return true;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                _curCtrl.ChangeView(this, 4);
                return true;
            }
            
            return false;
        }
        //

        public void LateUpdate(float fTick)
        {
            if (null == _curCamera || null == _curCtrl)
                return;
            
            _curCtrl.LateUpdate(this, fTick);
        }

        public void Reset()
        {
            _curCamera = null;

            if (null != _curCtrl)
            {
                _curCtrl.Reset();
                _curCtrl = null;
            }
        }

        public GameObject CreateLgCameraFollowObj(GameObject root, Vector3 position)
        {
            GameObject obj = new GameObject("LgCameraFollowObj");
            obj.transform.SetParent(root.transform);
            obj.transform.position = position;
            
            return obj;
        }

        public void BindCamera(Transform camera)
        {
            _curCamera = camera;
        }

        public CameraController BindCtrl(int type, params object[] param)
        {
            if (null != _curCtrl)
            {
                _curCtrl.Reset();
                _curCtrl = null;
            }
            if (type == 0)
                return null;

            if (!_ctrlHash.TryGetValue(type, out _curCtrl))
            {
                GameInit.Framework.Logger.LogGreen(ZString.Concat("the type ", type.ToString(), " of camera is not exist!!!"));
            }
            _curCtrl.Init(param);

            return _curCtrl;
        }
    }
}