﻿using UnityEngine;
using System.Collections.Generic;

namespace GameInit.Game
{
    public struct CameraLgView
    {
        public float _zoom;
        public float _pitch;
        public float _yaw;
        public float _fov;

        public Vector3 _position;

        public CameraLgView(float zoom, float pitch, float yaw, float fov, Vector3 position)
        {
            _zoom = zoom;
            _pitch = pitch;
            _yaw = yaw;
            _fov = fov;

            _position = position;
        }
    }
    
    public sealed class CameraLgCtrl : CameraController
    {
        private UCameraLgCtrl _uCtrl;

        private Vector3 _followDirection;

        private Vector3 _initPosition;

        private float _width = 10f;
        private float _height = 10f;

        private float _minZoom = 15f;
        private float _maxZoom = 29f;
        
        //默认视角
        private CameraLgView _view1;
        //拉近视角
        private CameraLgView _view2;
        //左边视角
        private CameraLgView _view3;
        //右边视角
        private CameraLgView _view4;
        
        public int GetCtrlType()
        {
            return 1;
        }

        public void Init(params object[] param)
        {
            _uCtrl = CameraManager.Instance.Camera.gameObject.GetComponent<UCameraLgCtrl>();
            if (_uCtrl == null)
                _uCtrl = CameraManager.Instance.Camera.gameObject.AddComponent<UCameraLgCtrl>();
            _uCtrl.enabled = true;
            _uCtrl._follow = param[0] as GameObject;
            _uCtrl._zoom = (float)param[1];
            _uCtrl._pitch = (float)param[2];
            _uCtrl._yaw = (float)param[3];
            _uCtrl._fov = (float)param[4];
            
            _initPosition = _uCtrl._follow.transform.position;
            
            _followDirection = Quaternion.Euler(_uCtrl._pitch, _uCtrl._yaw, 0) * Vector3.back;
            
            CameraManager.Instance.Camera.GetComponent<Camera>().fieldOfView = _uCtrl._fov;
            CameraManager.Instance.Camera.position = _initPosition + _followDirection * _uCtrl._zoom;
            CameraManager.Instance.Camera.LookAt(_uCtrl._follow.transform);
            
            //默认视角
            _view1 = new CameraLgView(_uCtrl._zoom, _uCtrl._pitch, _uCtrl._yaw, _uCtrl._fov, _initPosition);
            //拉近视角
            _view2 = new CameraLgView(15f, 35f, 0f, 40f, _initPosition);
            //左边视角
            _view3 = new CameraLgView(25f, 35f, 0f, 40f, new Vector3(4.7f, 0f, 14.82f));
            //右边视角
            _view4 = new CameraLgView(25f, 35f, 0f, 40f, new Vector3(13.6f, 0f, 14.82f));
        }

        public void Reset()
        {
            _uCtrl.enabled = false;
        }

        public void Update(Vector3 moveVelocity, float moveZoom, float fTick)
        {
            //moveVelocity
            float left = _initPosition.x - _width * 0.5f;
            float right = _initPosition.x + _width * 0.5f;

            float up = _initPosition.z + _height * 0.5f;
            float down = _initPosition.z - _height * 0.5f;

            Vector3 newPos = _uCtrl._follow.transform.position + moveVelocity;
            float x = newPos.x;
            float y = newPos.y;
            float z = newPos.z;
            if (x < left) x = left;
            if (x > right) x = right;
            if (z > up) z = up;
            if (z < down) z = down;

            _uCtrl._follow.transform.position = new Vector3(x, y, z);
            
            //moveZoom
            float newZoom = _uCtrl._zoom + moveZoom;
            if (newZoom < _minZoom) newZoom = _minZoom;
            if (newZoom > _maxZoom) newZoom = _maxZoom;

            _uCtrl._zoom = newZoom;
        }

        public void LateUpdate(CameraManager mgr, float fTick)
        {
            if (_uCtrl._enableDebug)
            {
                _followDirection = Quaternion.Euler(_uCtrl._pitch, _uCtrl._yaw, 0) * Vector3.back;
                mgr.Camera.GetComponent<Camera>().fieldOfView = _uCtrl._fov;
            }
            if (_uCtrl._follow != null)
            {
                mgr.Camera.position = _uCtrl._follow.transform.position + _followDirection * _uCtrl._zoom;
                mgr.Camera.LookAt(_uCtrl._follow.transform);
            }
        }

        private void SetView(CameraManager mgr, CameraLgView view)
        {
            _uCtrl._zoom = view._zoom;
            _uCtrl._pitch = view._pitch;
            _uCtrl._yaw = view._yaw;
            _uCtrl._fov = view._fov;

            _uCtrl._follow.transform.position = view._position;
            
            _followDirection = Quaternion.Euler(_uCtrl._pitch, _uCtrl._yaw, 0) * Vector3.back;
            mgr.Camera.GetComponent<Camera>().fieldOfView = _uCtrl._fov;
        }
        public void ChangeView(CameraManager mgr, int id)
        {
            switch (id)
            {
                case 1: SetView(mgr, _view1); break;
                case 2: SetView(mgr, _view2); break;
                case 3: SetView(mgr, _view3); break;
                case 4: SetView(mgr, _view4); break;
            }
        }
    }
}