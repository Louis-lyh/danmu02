﻿using UnityEngine;

namespace GameInit.Game
{
    using UnityEngine;

    public class UCameraLgCtrl : MonoBehaviour
    {
        public float _zoom;
        public float _pitch;
        public float _yaw;
        public float _fov;

        public GameObject _follow;
        
        public bool _enableDebug;

        void Awake()
        {
            _zoom = 18f;
            _pitch = 30f;
            _yaw = 0f;
            _fov = 60f;
            
            _enableDebug = false;
        }
    }
}