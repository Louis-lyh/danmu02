﻿using UnityEngine;

namespace GameInit.Game
{
    //相机控制器接口
    public interface CameraController
    {
        int GetCtrlType();
        void Init(params object[] param);
        void Reset();
        void Update(Vector3 moveVelocity, float moveZoom, float fTick);
        void LateUpdate(CameraManager mgr, float fTick);
        void ChangeView(CameraManager mgr, int id);
    }
}