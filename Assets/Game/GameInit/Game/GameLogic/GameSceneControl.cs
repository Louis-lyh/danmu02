using System;
using UnityEngine;

public class GameSceneControl : MonoBehaviour
{
    // 单例
    public static GameSceneControl Instance;
    // sd画布
    public Transform _3dCanvas;
    // 相机
    public Camera BattleCamera;
    // 相机节点
    public Transform battleCameraRoot;
    public float moveSpeed = 15f; // 控制移动速度
    // 阻力
    public float _resistance = 40f;
    // 水平移动
    private Vector3 _horizontalMove;
    private float _curHorizontalSpeed;

    // 垂直移动
    private float _verticalMove;
    private float _curVerticalSpeed;
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }

    private void Update()
    {
        // 按
        KeyPress();
        // 水平移动
        HorizontalMoveTarget(_horizontalMove);
        // 垂直移动
        VerticalMoveTarget(_verticalMove);
        
        // 减速
        if(_curHorizontalSpeed > 0)
            _curHorizontalSpeed -= _resistance * Time.deltaTime;
        else
            _curHorizontalSpeed = 0;
        
        if(_curVerticalSpeed > 0)
            _curVerticalSpeed -= _resistance * Time.deltaTime;
        else
            _curVerticalSpeed = 0;

    }
    
    // 移动相机
    private void HorizontalMoveTarget(Vector3 dir)
    {
        if (battleCameraRoot != null) // 如果目标物体不为空
        {
            dir = dir.normalized;
            battleCameraRoot.position += _curHorizontalSpeed  * Time.deltaTime * dir;
            var pos = battleCameraRoot.position;
            pos.x = Mathf.Clamp(pos.x, 5.7f, 14.5f);
            pos.z = Mathf.Clamp(pos.z, -7.79f, 1.23f);
            battleCameraRoot.position = pos;
        }
    }
    // 上下移动
    private void VerticalMoveTarget(float dis)
    {
        if (dis != 0f)
        {
            var pos = battleCameraRoot.localPosition;
            var moveDis = battleCameraRoot.forward * (dis * _curVerticalSpeed);
            pos -= moveDis;
            if(pos.y < 10 || pos.y > 20)
                return;
            battleCameraRoot.localPosition = pos;
        }
    }

    // 按下按键
    private void KeyPress()
    {
        // 左
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            _horizontalMove = Vector3.left;
            _curHorizontalSpeed = moveSpeed;
        }
        // 右
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            _horizontalMove = Vector3.right;
            _curHorizontalSpeed = moveSpeed;
        }
        // 上
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            _horizontalMove = Vector3.forward;
            _curHorizontalSpeed = moveSpeed;
        }
        // 下
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            _horizontalMove = Vector3.back;
            _curHorizontalSpeed = moveSpeed;
        }
        // 左上
        if ((Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
            || (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.UpArrow)))
        {
            _horizontalMove = (Vector3.left + Vector3.forward).normalized;
            _curHorizontalSpeed = moveSpeed;
        }
        // 左下
        if ((Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
            || (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.DownArrow)))
        {
            _horizontalMove = (Vector3.left + Vector3.back).normalized;
            _curHorizontalSpeed = moveSpeed;
        }
        
        // 右上
        if ((Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))
            || (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.UpArrow)))
        {
            _horizontalMove = (Vector3.right + Vector3.forward).normalized;
            _curHorizontalSpeed = moveSpeed;
        }
        // 右下
        if ((Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S))
            || (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.DownArrow)))
        {
            _horizontalMove = (Vector3.right + Vector3.back).normalized;
            _curHorizontalSpeed = moveSpeed;
        }

        //  
        if (Input.GetKey(KeyCode.Q))
        {
            _verticalMove = 0.02f;
            _curVerticalSpeed = moveSpeed;
        }
        // 
        if (Input.GetKey(KeyCode.E))
        {
            _verticalMove = -0.02f;
            _curVerticalSpeed = moveSpeed;
        }
        // 滚轮
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll != 0)
        {
            _verticalMove = scroll;
            _curVerticalSpeed = 5;
        }
    }
 
}
