﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace GameInit.Game
{
    /// <summary>
    /// UI动画
    /// </summary>
    public partial class GameNoticeModel
    {
        #region UI渐变动画
        // ui渐变动画
        public Tweener UiGradientAni(Image item, UIGradientAxle axle, UIGradientDir dir, float duration,float startValue = 0f,float endValue = 1f)
        {
            // 设置渐变材质球
            item.material = GetMaterial(axle, dir);
            // 填充UI字段   
            var fillAmount = "_FillAmount";
            // 动画
            var doTween = DOTween.To(() => startValue,
                (value) => item.material.SetFloat(fillAmount, value),
                endValue,
                duration);
            
            return doTween;
        }

        // 设置渐变材质球属性
        private Material GetMaterial(UIGradientAxle axle, UIGradientDir dir)
        {
            var mat = new Material(_matUIGradient);
            
            // 材质球属性
            var vertical = "_Vertical";
            var horizontal = "_Horizontal";
            var verReverse = "_VerReverse";
            var horReverse = "_HorReverse";
            // 设置渐变的坐标轴
            switch (axle)
            {
                case UIGradientAxle.Horizontal:
                    mat.SetInt(vertical, 0);
                    mat.SetInt(horizontal, 1);
                    break;
                case UIGradientAxle.Vertical:
                    mat.SetInt(vertical, 1);
                    mat.SetInt(horizontal, 0);
                    break;
                case UIGradientAxle.HorAndVer:
                    mat.SetInt(vertical, 1);
                    mat.SetInt(horizontal, 1);
                    break;
            }
            // 设置渐变方向
            switch (dir)
            {
                case UIGradientDir.Positive:
                    mat.SetInt(verReverse, 0);
                    mat.SetInt(horReverse, 0);
                    break;
                case UIGradientDir.Reverse:
                    mat.SetInt(verReverse, 1);
                    mat.SetInt(horReverse, 1);
                    break;
            }

            return mat;
        }
        #endregion

        #region UI溶解动画
        // ui渐变动画
        public Tweener UiDissolveAni(Image item,float duration)
        {
            // 设置渐变材质球
            item.material = new Material(_matUIDissolve);
            // 溶解字段   
            var fillAmount = "_Threshold";
            // 动画
            var doTween = DOTween.To(() => 0f,
                (value) => item.material.SetFloat(fillAmount, value),
                1f,
                duration);
            
            return doTween;
        }

        

        #endregion
    }
}