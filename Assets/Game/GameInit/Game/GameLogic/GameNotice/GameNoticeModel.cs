using System.Collections;
using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine;

namespace GameInit.Game
{
    public partial class GameNoticeModel : DataModelBase<GameNoticeModel>
    {
        // 发送跑马灯
        public const string EventSendHorseRaceLamp = "Event_SendHorseRaceLamp";
        // 发送屏幕边界气泡
        public const string EventSendEdgeBubble = "Event_SendEdgeBubble";
        // 显示取消技能进度
        public const string EventCancelSkillProgress = "Event_CancelSkillProgres";
        // 关闭boss提示界面
        public const string EventCloseBossTips = "Event_CloseBossTips";
        // 显示击杀boss
        public const string EventShowKillTips = "Event_ShowKillTIps";
        // 显示危险提示
        public const string EventShowDangerousTips = "Event_ShowDangerousTips";
        // 显示搜集提示
        public const string EventShowCollectTips = "Event_ShowCpllectTips";
        // 定位
        public const string EventLocationUnit = "EventLocationUnit";
        // 狼人排行榜
        public const string EventShowBossRanking = "Event_ShowBossRanking";
        // 显示礼物提示
        public const string EventSetGiftTips = "Event_SetGiftTips";
        // 显示双倍经验提示
        public const string EventDoubleExp = "Event_DoubleExpTips";
        // 显示大佬入场
        public const string EventShowTopPlayer = "Event_ShowTopPlayer";
        // 查询
        public const string EventShowQueryPlayerInfo = "Event_ShowQueryPlayerInfo";
        
        // ui渐变材质球 
        private Material _matUIGradient;
        // ui溶解材质球
        private Material _matUIDissolve;

        // 初始化
        public void Init()
        {
            // 加载材质球
            if (_matUIGradient == null)
                _matUIGradient = Resources.Load<Material>("Material/Mat_UITransparentGradient");
            if(_matUIDissolve == null)
                _matUIDissolve = Resources.Load<Material>("Material/Mat_UIDissolve");
        }
    }
    
    

    // 气泡信息
    public struct EdgeBubbleInfo
    {
        public string Name;    // 名字
        public string HeadUrl; // 头像
        public string EventName;    // 事件名
        public string EventContent;    // 事件内容
        public EdgeBubbleType Type;    // 气泡类型
    }
   

    // 屏幕边界气泡类型
    public enum EdgeBubbleType
    {
        KillScore,    // 击杀分数
        LikeCount,    // 点赞数量
        PlayerState01, // 玩家状态
        PlayerState02,
    }
    
    // 渐变轴
    public enum UIGradientAxle
    {
        Horizontal,    // 水平
        Vertical,    // 垂直
        HorAndVer,    // 水平和垂直
    }
    // 方向
    public enum UIGradientDir
    {
        Positive,    // 正方向
        Reverse,    // 反方向
    }
    
    // 世界排名玩家信息
    public class TopPlayerInfo
    {
        public string UserName;    // 用户名·
        public string HeadHurl;    // 头像
        public int Order;    // 排名
        public ServerData.ERankType ERankType; // 排行榜类型
    }
    
    // 击杀boss提示
    public class SkillBossTips
    {
        public RoleData RoleData;
        public Dictionary<string, int> Reward;
    }

}


