namespace GameInit.Game
{
    public partial class GameNoticeModel
    {
        // 发射跑马灯
        public void SendHorseRaceLamp(string name, string describe)
        {
            var info = new HorseRaceLampInfo(name, describe);
            // 通知UI
            DispatchEvent(EventSendHorseRaceLamp, info);
        }

        // 发射气泡
        public void SendEdgeBubble(string name,string headerUrl, string eventName, string eventContent, EdgeBubbleType type)
        {
            // 气泡信息
            var info = new EdgeBubbleInfo();
            info.Name = name;
            info.HeadUrl = headerUrl;
            info.EventName = eventName;
            info.EventContent = eventContent;
            info.Type = type;
            // 通知UI
            DispatchEvent(EventSendEdgeBubble, info);
        }

        // 关闭bss提示界面
        public void CloseBossTips()
        {
            DispatchEvent(EventCloseBossTips);
        }
        // 显示危险提示
        public void ShowDangerousTips(string name, string content)
        {
            // 格式化名字
            name =$"<color=#7AFED1FF>{FormatName(name)}</color>" ;
            // 
            var text = name + content;
            DispatchEvent(EventShowDangerousTips,text);
        }
        // 显示搜集提示
        public void ShowCollectTips(string content)
        {
            DispatchEvent(EventShowCollectTips,content);
        }
        
        // 显示大哥入场
        public void ShowTopPlayer(RoleData roleData)
        {
            // 狼人排行榜
            // var werewolfOrder = RankInfo.Instance.RankOrder(roleData.UserId, ServerData.ERankType.e_werwolf);
            // 动物排行榜
            var animalOrder = RankInfo.Instance.RankOrder(roleData.UserId, ServerData.ERankType.e_player);

            if(animalOrder > GameConstantHelper.TopPlayerLowLimit)
                return;
            
            // 排名信息
            var topPlayerInfo = new TopPlayerInfo();
            topPlayerInfo.UserName = roleData.Name;
            topPlayerInfo.HeadHurl = roleData.HeadUrl;
            topPlayerInfo.Order = animalOrder;
            topPlayerInfo.ERankType =ServerData.ERankType.e_player;
            
            DispatchEvent(EventShowTopPlayer,topPlayerInfo);
        }
        
        //格式化名字
        public static string FormatName(string name)
        {
            if (name.Length <= 5)
                return name;
            
            return name.Substring(0,4) + "...";
        }
    }
    
    // 跑马灯信息
    public class HorseRaceLampInfo
    {
        public HorseRaceLampInfo(string name, string describe)
        {
            Name = name;
            Describe = describe;
        }

        public string Name;    // 名字
        public string Describe;// 描述
    }
}

