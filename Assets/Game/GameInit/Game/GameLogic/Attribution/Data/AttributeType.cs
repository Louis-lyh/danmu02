namespace GameHotfix.Game
{
    /// <summary>
    /// 加成类型
    /// </summary>
    public static class AttributeType
    {
        public const int Other = 0;// 其他
        public const int Attack = 1; // 攻击
        public const int AttackSpeed = 2;// 攻击速度
        public const int Shield = 3;// 护盾
        public const int MoveSpeed = 4; // 移动速度
        public const int BodyScale = 5; // 模型缩放
    }
}