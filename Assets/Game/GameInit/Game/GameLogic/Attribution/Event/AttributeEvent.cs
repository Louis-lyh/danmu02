using GameHotfix.Framework;

namespace GameInit.Game
{
    /// <summary>
    /// 属性事件
    /// </summary>
    public class AttributeEvent : BaseEvent
    {
        public const string UpdateAttribute = "UpdateAttribute";

        public AttributeEvent(string type, object evtObject)
            : base(type, evtObject)
        {
        }
    }
}