﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class BossTipsView : AlertDialogView
    {
		private Image _uiMask;
		private Transform _root;
		private GameObject _uiBossComingTips;
		private Image _uiBossComingBg;
		private Image _uiBossComingBg01;
		private Image _uiBossComingTitle;
		private Image _uiBossComingTitle01;
		private Image _uiBossImg;
		private Text _uiDropTipsText;
		private GameObject _uiWhoIsBossTips;
		private Transform _uiPlayerInfo;
		private Image _uiHeaderIcon;
		private Text _uiBossName;
		private GameObject _uiBossSkillTips;
		private Image _uiSkillBossImg;
		private Text _uiSkillTipsText;
		private Text _uiCountdownText;
		private GameObject _uiClickUp;
		private GameObject _uiClickDown;
		private Image _uiCanceProgress;
		private RectTransform _uiDangerous;
		

        public BossTipsView()
            : base("BossTips")
        {

        }

        protected override void ParseComponent()
        {
			_uiMask = Find<Image>("uiMask");
			_root = Find<Transform>("Root");
			_uiBossComingTips = Find("Root/uiBossComingTips");
			_uiBossComingBg = Find<Image>("Root/uiBossComingTips/uiBossComingBg");
			_uiBossComingBg01 = Find<Image>("Root/uiBossComingTips/uiBossComingBg01");
			_uiBossComingTitle = Find<Image>("Root/uiBossComingTips/uiBossComingTitle");
			_uiBossComingTitle01 = Find<Image>("Root/uiBossComingTips/uiBossComingTitle01");
			_uiBossImg = Find<Image>("Root/uiBossComingTips/uiBossImg");
			_uiDropTipsText = Find<Text>("Root/uiBossComingTips/uiDropTis/uiDropTipsText");
			_uiWhoIsBossTips = Find("Root/uiWhoIsBossTips");
			_uiPlayerInfo = Find<Transform>("Root/uiWhoIsBossTips/uiPlayerInfo");
			_uiHeaderIcon = Find<Image>("Root/uiWhoIsBossTips/uiPlayerInfo/uiHeader/uiHeaderMask/uiHeaderIcon");
			_uiBossName = Find<Text>("Root/uiWhoIsBossTips/uiPlayerInfo/uiBossName");
			_uiBossSkillTips = Find("Root/uiBossSkillTips");
			_uiSkillBossImg = Find<Image>("Root/uiBossSkillTips/uiSkillBossImg");
			_uiSkillTipsText = Find<Text>("Root/uiBossSkillTips/uiSkillTipsText");
			_uiCountdownText = Find<Text>("Root/uiBossSkillTips/uiCountdown/uiCountdownText");
			_uiClickUp = Find("Root/uiBossSkillTips/uiClick/uiClickUp");
			_uiClickDown = Find("Root/uiBossSkillTips/uiClick/uiClickDown");
			_uiCanceProgress = Find<Image>("Root/uiBossSkillTips/uiCanceSkill/uiCanceProgress");
			_uiDangerous = Find<RectTransform>("Root/uiDangerous");
			
        }
    }
}
