﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIFirstItem : UIBaseView
    {
		private Image _uiFirstHeaderIcon;
		private Image _uiHeaderFrame;
		private Text _uiFirstNameText;
		private Text _uiFirstScore;
		

        protected override void ParseComponent()
        {
			_uiFirstHeaderIcon = Find<Image>("uiHeader/uiFirstHeaderIcon");
			_uiHeaderFrame = Find<Image>("uiHeaderFrame");
			_uiFirstNameText = Find<Text>("uiFirstName/uiFirstNameText");
			_uiFirstScore = Find<Text>("uiFirstScore");
			
        }
    }
}
