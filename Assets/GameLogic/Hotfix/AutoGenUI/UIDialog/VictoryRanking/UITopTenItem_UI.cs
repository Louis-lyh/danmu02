﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UITopTenItem : UIBaseView
    {
		private Transform _root;
		private Image _uiRankIcon;
		private Text _uiRanText;
		private Image _uiHeaderIcon;
		private Text _uiName;
		private Text _uiLevel;
		private Text _uiSpeciesText;
		private Text _uiCurScore;
		private Text _uiWeekScore;
		private Text _uiWeekRanking;
		private Text _uiWinStreakText;
		

        protected override void ParseComponent()
        {
			_root = Find<Transform>("Root");
			_uiRankIcon = Find<Image>("Root/uiRankIcon");
			_uiRanText = Find<Text>("Root/uiRankIcon/uiRanText");
			_uiHeaderIcon = Find<Image>("Root/uiHeader/uiHeaderMask/uiHeaderIcon");
			_uiName = Find<Text>("Root/uiName");
			_uiLevel = Find<Text>("Root/uiSpecies/uiLevel");
			_uiSpeciesText = Find<Text>("Root/uiSpecies/uiSpeciesText");
			_uiCurScore = Find<Text>("Root/uiScore/uiCurScore");
			_uiWeekScore = Find<Text>("Root/uiScore/uiWeekScore");
			_uiWeekRanking = Find<Text>("Root/uiWeekRanking");
			_uiWinStreakText = Find<Text>("Root/uiWinStreakText");
			
        }
    }
}
