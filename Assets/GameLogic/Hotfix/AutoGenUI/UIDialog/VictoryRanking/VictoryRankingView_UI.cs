﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class VictoryRankingView : AlertDialogView
    {
		private Transform _root;
		private Image _uiMask;
		private Image _uiTitleImg;
		private Text _uiTitleText;
		private Transform _uiTopThree;
		private GameObject _uIFirstItem;
		private GameObject _uIFirstItem01;
		private GameObject _uIFirstItem02;
		private Transform _uiTopTen;
		private GameObject _uITopTenItem;
		private Transform _uiTopTenContent;
		private Button _uiBtnClose;
		

        public VictoryRankingView()
            : base("VictoryRanking")
        {

        }

        protected override void ParseComponent()
        {
			_root = Find<Transform>("Root");
			_uiMask = Find<Image>("Root/uiMask");
			_uiTitleImg = Find<Image>("Root/Content/uiTitle/uiTitleImg");
			_uiTitleText = Find<Text>("Root/Content/uiTitle/uiTitleText");
			_uiTopThree = Find<Transform>("Root/Content/uiTopThree");
			_uIFirstItem = Find("Root/Content/uiTopThree/UIFirstItem");
			_uIFirstItem01 = Find("Root/Content/uiTopThree/UIFirstItem01");
			_uIFirstItem02 = Find("Root/Content/uiTopThree/UIFirstItem02");
			_uiTopTen = Find<Transform>("Root/Content/uiTopTen");
			_uITopTenItem = Find("Root/Content/uiTopTen/UITopTenItem");
			_uiTopTenContent = Find<Transform>("Root/Content/uiTopTen/ScrollView/Viewport/uiTopTenContent");
			_uiBtnClose = Find<Button>("Root/Content/uiBtnClose");
			
        }
    }
}
