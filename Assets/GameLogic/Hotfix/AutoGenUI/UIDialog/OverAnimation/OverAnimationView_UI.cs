﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class OverAnimationView : AlertDialogView
    {
		private Image _uiMask;
		private Text _uiOverTips;
		

        public OverAnimationView()
            : base("OverAnimation")
        {

        }

        protected override void ParseComponent()
        {
			_uiMask = Find<Image>("uiMask");
			_uiOverTips = Find<Text>("Root/uiOverTips");
			
        }
    }
}
