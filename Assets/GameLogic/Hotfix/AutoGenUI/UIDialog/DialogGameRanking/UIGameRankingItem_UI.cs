﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIGameRankingItem : UIBaseView
    {
		private Transform _root;
		private Image _uiRankIcon;
		private Text _uiRanText;
		private Image _uiHeaderIcon;
		private Text _uiName;
		private Text _uiCurScore;
		private Text _uiWeekScore;
		private Text _uiWeekRanking;
		private Text _uiKillCountText;
		

        protected override void ParseComponent()
        {
			_root = Find<Transform>("Root");
			_uiRankIcon = Find<Image>("Root/uiRankIcon");
			_uiRanText = Find<Text>("Root/uiRankIcon/uiRanText");
			_uiHeaderIcon = Find<Image>("Root/uiHeader/uiHeaderMask/uiHeaderIcon");
			_uiName = Find<Text>("Root/uiName");
			_uiCurScore = Find<Text>("Root/uiScore/uiCurScore");
			_uiWeekScore = Find<Text>("Root/uiScore/uiWeekScore");
			_uiWeekRanking = Find<Text>("Root/uiWeekRanking");
			_uiKillCountText = Find<Text>("Root/uiKillCountText");
			
        }
    }
}
