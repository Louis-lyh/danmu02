﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class DialogGameRankingView : AlertDialogView
    {
		private Transform _root;
		private Image _uiMask;
		private Image _uiTitleImg;
		private Text _uiTitleText;
		private Button _uiCloseBtn;
		private Transform _uiTopTen;
		private GameObject _uIGameRankingItem;
		private Transform _uIGameRankingContent;
		

        public DialogGameRankingView()
            : base("DialogGameRanking")
        {

        }

        protected override void ParseComponent()
        {
			_root = Find<Transform>("Root");
			_uiMask = Find<Image>("Root/uiMask");
			_uiTitleImg = Find<Image>("Root/Content/uiTitle/uiTitleImg");
			_uiTitleText = Find<Text>("Root/Content/uiTitle/uiTitleText");
			_uiCloseBtn = Find<Button>("Root/Content/uiCloseBtn");
			_uiTopTen = Find<Transform>("Root/Content/uiTopTen");
			_uIGameRankingItem = Find("Root/Content/uiTopTen/UIGameRankingItem");
			_uIGameRankingContent = Find<Transform>("Root/Content/uiTopTen/ScrollView/Viewport/UIGameRankingContent");
			
        }
    }
}
