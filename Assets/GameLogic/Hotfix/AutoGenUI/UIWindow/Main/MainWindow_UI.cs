﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class MainWindow : UIWindowBase
    {
		private Button _singleBtn;
		private Button _fieldBossBtn;
		private Image _uiFieldBossBtnGray;
		private Toggle _uiToggle;
		private Transform _uiTips;
		

        public MainWindow()
        {
            _uiName = "Main";
        }

        protected override void ParseComponent()
        {
			_singleBtn = Find<Button>("SingleBtn");
			_fieldBossBtn = Find<Button>("FieldBossBtn");
			_uiFieldBossBtnGray = Find<Image>("FieldBossBtn/uiFieldBossBtnGray");
			_uiToggle = Find<Toggle>("uiToggle");
			_uiTips = Find<Transform>("uiTips");
			
        }
    }
}
