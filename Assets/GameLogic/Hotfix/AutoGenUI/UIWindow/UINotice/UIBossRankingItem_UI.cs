﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIBossRankingItem : UIBaseView
    {
		private Image _uiHeaderIcon;
		private Image _uiOrderFrame;
		private Text _uiName;
		private Text _uiScore;
		private Text _uiOrder;
		

        protected override void ParseComponent()
        {
			_uiHeaderIcon = Find<Image>("uiHeader/uiHeaderMask/uiHeaderIcon");
			_uiOrderFrame = Find<Image>("uiHeader/uiOrderFrame");
			_uiName = Find<Text>("uiName");
			_uiScore = Find<Text>("uiScore");
			_uiOrder = Find<Text>("uiOrder");
			
        }
    }
}
