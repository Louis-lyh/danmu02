﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UINoticeWindow : UIWindowBase
    {
		private LocationObject _locationObject;
		private HorseRaceLamp _horseRaceLamp;
		private EdgeBubble _edgeBubble;
		private BannerTips _bannerTips;
		private Button _uiGameRankingBtn;
		private UIBossRankingView _uIBossRankingView;
		private GameObject _uiGiftTips;
		private Transform _uiGiftTips01;
		private Transform _uiGiftTips02;
		private UITopPlayerTips _uITopPlayerTips;
		private QueryPlayerInfo _queryPlayerInfo;
		private Text _uiTips;
		

        public UINoticeWindow()
        {
            _uiName = "UINotice";
        }

        protected override void ParseComponent()
        {
			_locationObject = new LocationObject();
			_locationObject.SetDisplayObject(Find("Root/LocationObject"));
			_horseRaceLamp = new HorseRaceLamp();
			_horseRaceLamp.SetDisplayObject(Find("Root/HorseRaceLamp"));
			_edgeBubble = new EdgeBubble();
			_edgeBubble.SetDisplayObject(Find("Root/EdgeBubble"));
			_bannerTips = new BannerTips();
			_bannerTips.SetDisplayObject(Find("Root/BannerTips"));
			_uiGameRankingBtn = Find<Button>("Root/uiGameRankingBtn");
			_uIBossRankingView = new UIBossRankingView();
			_uIBossRankingView.SetDisplayObject(Find("Root/UIBossRankingView"));
			_uiGiftTips = Find("Root/uiGiftTips");
			_uiGiftTips01 = Find<Transform>("Root/uiGiftTips/uiGiftTips01");
			_uiGiftTips02 = Find<Transform>("Root/uiGiftTips/uiGiftTips02");
			_uITopPlayerTips = new UITopPlayerTips();
			_uITopPlayerTips.SetDisplayObject(Find("Root/UITopPlayerTips"));
			_queryPlayerInfo = new QueryPlayerInfo();
			_queryPlayerInfo.SetDisplayObject(Find("Root/QueryPlayerInfo"));
			_uiTips = Find<Text>("Root/uiTips");
			
        }
    }
}
