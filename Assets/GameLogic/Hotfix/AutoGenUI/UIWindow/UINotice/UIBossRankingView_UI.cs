﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIBossRankingView : UIBaseView
    {
		private RectTransform _root;
		private GameObject _uIBossRankingItem;
		

        protected override void ParseComponent()
        {
			_root = Find<RectTransform>("Root");
			_uIBossRankingItem = Find("Root/UIBossRankingItem");
			
        }
    }
}
