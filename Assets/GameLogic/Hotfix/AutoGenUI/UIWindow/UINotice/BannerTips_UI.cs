﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class BannerTips : UIBaseView
    {
		private GameObject _uiDangerousTips;
		private GameObject _uiCollectTips;
		private GameObject _uIKillBossTips;
		

        protected override void ParseComponent()
        {
			_uiDangerousTips = Find("uiDangerousTips");
			_uiCollectTips = Find("uiCollectTips");
			_uIKillBossTips = Find("UIKillBossTips");
			
        }
    }
}
