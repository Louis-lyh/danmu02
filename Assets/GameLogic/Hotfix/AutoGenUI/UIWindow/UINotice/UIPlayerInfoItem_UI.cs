﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIPlayerInfoItem : UIBaseView
    {
		private Text _uiLevel;
		private Text _uiNameText;
		private Image _uiIcon;
		private Text _uiFighValue;
		private Text _uiSpeciesName;
		private Text _uiHaloLevel;
		private Text _uiHaloName;
		private Image _uiHaleProgressImg;
		private Text _uiZhuoQiName;
		private Image _uiZhuoQiProgressmg;
		private Text _uiZhuoQiLevel;
		private Image _uiZHouQiImg;
		

        protected override void ParseComponent()
        {
			_uiLevel = Find<Text>("uiPlayer/uiName/uiLevel");
			_uiNameText = Find<Text>("uiPlayer/uiName/uiNameText");
			_uiIcon = Find<Image>("uiPlayer/uiIcon");
			_uiFighValue = Find<Text>("uiPlayer/uiFighValue");
			_uiSpeciesName = Find<Text>("uiPlayer/uiSpeciesName");
			_uiHaloLevel = Find<Text>("uiHalo/uiHaloLevel");
			_uiHaloName = Find<Text>("uiHalo/uiHaloName");
			_uiHaleProgressImg = Find<Image>("uiHalo/uiHaleProgress/uiHaleProgressImg");
			_uiZhuoQiName = Find<Text>("uiZhouQi/uiZhuoQiName");
			_uiZhuoQiProgressmg = Find<Image>("uiZhouQi/uiZhuoQiProgress/uiZhuoQiProgressmg");
			_uiZhuoQiLevel = Find<Text>("uiZhouQi/uiZhuoQiLevel");
			_uiZHouQiImg = Find<Image>("uiZhouQi/uiZHouQiImg");
			
        }
    }
}
