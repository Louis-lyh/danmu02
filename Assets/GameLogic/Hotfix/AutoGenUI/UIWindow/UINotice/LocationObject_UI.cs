﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class LocationObject : UIBaseView
    {
		private Transform _uiPositioningLeftImage;
		private Transform _uiPositioningRightImage;
		

        protected override void ParseComponent()
        {
			_uiPositioningLeftImage = Find<Transform>("uiPositioningLeftImage");
			_uiPositioningRightImage = Find<Transform>("uiPositioningRightImage");
			
        }
    }
}
