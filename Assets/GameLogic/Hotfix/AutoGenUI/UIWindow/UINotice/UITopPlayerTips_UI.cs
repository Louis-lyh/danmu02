﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UITopPlayerTips : UIBaseView
    {
		private GameObject _uiMask;
		private GameObject _uITopPlayerItem;
		

        protected override void ParseComponent()
        {
			_uiMask = Find("Root/uiMask");
			_uITopPlayerItem = Find("Root/UITopPlayerItem");
			
        }
    }
}
