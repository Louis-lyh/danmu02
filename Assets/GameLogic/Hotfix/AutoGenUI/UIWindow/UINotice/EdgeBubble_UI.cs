﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class EdgeBubble : UIBaseView
    {
		private Transform _range;
		private GameObject _uiEdgeBubbleItem_Left_01;
		private GameObject _uiEdgeBubbleItem_Left_02;
		private GameObject _uiEdgeBubbleItem_Right_01;
		

        protected override void ParseComponent()
        {
			_range = Find<Transform>("Range");
			_uiEdgeBubbleItem_Left_01 = Find("Range/uiEdgeBubbleItem_Left_01");
			_uiEdgeBubbleItem_Left_02 = Find("Range/uiEdgeBubbleItem_Left_02");
			_uiEdgeBubbleItem_Right_01 = Find("Range/uiEdgeBubbleItem_Right_01");
			
        }
    }
}
