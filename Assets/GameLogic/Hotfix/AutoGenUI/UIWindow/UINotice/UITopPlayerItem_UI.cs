﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UITopPlayerItem : UIBaseView
    {
		private Transform _uiHead;
		private Image _uiHeadIcon;
		private Transform _uiName;
		private Text _uiNameText;
		private Transform _uiTop;
		private Text _uiTopText;
		

        protected override void ParseComponent()
        {
			_uiHead = Find<Transform>("uiHead");
			_uiHeadIcon = Find<Image>("uiHead/uiHeadImage/uiHeadIcon");
			_uiName = Find<Transform>("uiName");
			_uiNameText = Find<Text>("uiName/uiNameText");
			_uiTop = Find<Transform>("uiTop");
			_uiTopText = Find<Text>("uiTop/uiTopText");
			
        }
    }
}
