﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class HorseRaceLamp : UIBaseView
    {
		private RectTransform _range;
		private GameObject _uiNoticeItem;
		

        protected override void ParseComponent()
        {
			_range = Find<RectTransform>("Range");
			_uiNoticeItem = Find("Range/uiNoticeItem");
			
        }
    }
}
