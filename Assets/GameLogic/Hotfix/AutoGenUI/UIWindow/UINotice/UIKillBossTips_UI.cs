﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIKillBossTips : UIBaseView
    {
		private Image _uiheadIcon;
		private Text _uiName;
		private Text _uiTipsText01;
		private Text _uiTipsText02;
		

        protected override void ParseComponent()
        {
			_uiheadIcon = Find<Image>("uiHeadMask/uiheadIcon");
			_uiName = Find<Text>("uiName/uiName");
			_uiTipsText01 = Find<Text>("uiTips01/uiTipsText01");
			_uiTipsText02 = Find<Text>("uiTips02/uiTipsText02");
			
        }
    }
}
