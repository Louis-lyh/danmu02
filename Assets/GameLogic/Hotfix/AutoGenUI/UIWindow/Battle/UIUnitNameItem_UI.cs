﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIUnitNameItem : UIBaseView
    {
		private GameObject _uiPlayer;
		private GameObject _uiBossIcon;
		private Transform _uiExpImg;
		private Image _uiExpProgress;
		private Transform _uiBloodImg;
		private Image _uiBlooGress;
		private Text _uiNameText;
		private Text _uiLevelText;
		private Transform _uiLike;
		private Transform _uiUpgradeDesire;
		private Transform _uiVigorous;
		private Transform _uiNormal;
		private Transform _uiLazy;
		private Transform _uiLazy01;
		private Transform _uiLazy02;
		private Transform _uiLazy03;
		private GameObject _uiCollect;
		private Image _uiCollectProgress;
		private GameObject _uiDialogBox;
		private Text _uiDialogBoxText;
		private Text _uiGuildId;
		private GameObject _uiPet;
		private Text _uiPetText;
		private GameObject _uiMonsterGuard;
		private Text _uiMonsterGuardText;
		private GameObject _uiBoss;
		private Image _uiBossProgressImg;
		private Text _uiBossHpText;
		private Text _uiBossNameText;
		

        protected override void ParseComponent()
        {
			_uiPlayer = Find("Root/uiPlayer");
			_uiBossIcon = Find("Root/uiPlayer/uiBossIcon");
			_uiExpImg = Find<Transform>("Root/uiPlayer/uiExpImg");
			_uiExpProgress = Find<Image>("Root/uiPlayer/uiExpImg/uiExpProgress");
			_uiBloodImg = Find<Transform>("Root/uiPlayer/uiBloodImg");
			_uiBlooGress = Find<Image>("Root/uiPlayer/uiBloodImg/uiBlooGress");
			_uiNameText = Find<Text>("Root/uiPlayer/uiName/uiNameText");
			_uiLevelText = Find<Text>("Root/uiPlayer/uiName/uiLevelText");
			_uiLike = Find<Transform>("Root/uiPlayer/uiLike");
			_uiUpgradeDesire = Find<Transform>("Root/uiPlayer/uiUpgradeDesire");
			_uiVigorous = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiVigorous");
			_uiNormal = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiNormal");
			_uiLazy = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiLazy");
			_uiLazy01 = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiLazy/uiLazy01");
			_uiLazy02 = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiLazy/uiLazy02");
			_uiLazy03 = Find<Transform>("Root/uiPlayer/uiUpgradeDesire/uiLazy/uiLazy03");
			_uiCollect = Find("Root/uiPlayer/uiCollect");
			_uiCollectProgress = Find<Image>("Root/uiPlayer/uiCollect/uiCollectProgress");
			_uiDialogBox = Find("Root/uiPlayer/uiDialogBox");
			_uiDialogBoxText = Find<Text>("Root/uiPlayer/uiDialogBox/uiDialogBoxText");
			_uiGuildId = Find<Text>("Root/uiPlayer/uiGuildId");
			_uiPet = Find("Root/uiPet");
			_uiPetText = Find<Text>("Root/uiPet/uiPetText");
			_uiMonsterGuard = Find("Root/uiMonsterGuard");
			_uiMonsterGuardText = Find<Text>("Root/uiMonsterGuard/uiMonsterGuardText");
			_uiBoss = Find("Root/uiBoss");
			_uiBossProgressImg = Find<Image>("Root/uiBoss/uiBossHp/uiBossProgressImg");
			_uiBossHpText = Find<Text>("Root/uiBoss/uiBossHp/uiBossHpText");
			_uiBossNameText = Find<Text>("Root/uiBoss/uiBossHp/uiBossNameText");
			
        }
    }
}
