﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIGameRankingList : UIBaseView
    {
		private GameObject _uiPlayerRankingItem;
		private Image _uiReviveTime;
		private Text _uiReviveTimeText;
		private Image _uiHpPressbar;
		

        protected override void ParseComponent()
        {
			_uiPlayerRankingItem = Find("Root/uiOrdinaryPlayer/uiPlayerRanking/uiPlayerRankingItem");
			_uiReviveTime = Find<Image>("Root/uiOrdinaryPlayer/uiPlayerRanking/uiPlayerRankingItem/uiHeader/uiHeaderMask/uiReviveTime");
			_uiReviveTimeText = Find<Text>("Root/uiOrdinaryPlayer/uiPlayerRanking/uiPlayerRankingItem/uiHeader/uiHeaderMask/uiReviveTime/uiReviveTimeText");
			_uiHpPressbar = Find<Image>("Root/uiOrdinaryPlayer/uiPlayerRanking/uiPlayerRankingItem/hp/uiHpPressbar");
			
        }
    }
}
