﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class BattleWindow : UIWindowBase
    {
		private UIUnitStateView _uIUnitStateView;
		private UIGameRankingList _uIGameRankingList;
		private UIBossInfo _uIBossInfo;
		private Button _startBtn;
		private Text _cDText;
		private Button _addBtn;
		private GameObject _uiNightCountdown;
		private Text _uiNightTips;
		private Text _uiNightCountdownText;
		private Text _uIScorePool;
		private Transform _uiBattleTips;
		private Text _uiBattleTipsText;
		private Text _uiVersion;
		private Text _uiGameTime;
		private Text _uiGuildId;
		private Text _uiAvoidWarTips;
		private Button _uiBtnClose;
		

        public BattleWindow()
        {
            _uiName = "Battle";
        }

        protected override void ParseComponent()
        {
			_uIUnitStateView = new UIUnitStateView();
			_uIUnitStateView.SetDisplayObject(Find("UIUnitStateView"));
			_uIGameRankingList = new UIGameRankingList();
			_uIGameRankingList.SetDisplayObject(Find("UIGameRankingList"));
			_uIBossInfo = new UIBossInfo();
			_uIBossInfo.SetDisplayObject(Find("UIBossInfo"));
			_startBtn = Find<Button>("Content/StartBtn");
			_cDText = Find<Text>("Content/CDText");
			_addBtn = Find<Button>("Content/AddBtn");
			_uiNightCountdown = Find("Content/uiNightCountdown");
			_uiNightTips = Find<Text>("Content/uiNightCountdown/uiNightTips");
			_uiNightCountdownText = Find<Text>("Content/uiNightCountdown/uiTimeBg/uiNightCountdownText");
			_uIScorePool = Find<Text>("Content/UIScore/UIScorePool");
			_uiBattleTips = Find<Transform>("Content/uiBattleTips");
			_uiBattleTipsText = Find<Text>("Content/uiBattleTips/uiBattleTipsText");
			_uiVersion = Find<Text>("uiVersion");
			_uiGameTime = Find<Text>("uiGameTime");
			_uiGuildId = Find<Text>("uiGuildId");
			_uiAvoidWarTips = Find<Text>("uiAvoidWarTips");
			_uiBtnClose = Find<Button>("uiBtnClose");
			
        }
    }
}
