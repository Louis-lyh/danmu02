﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIBossInfo : UIBaseView
    {
		private GameObject _uiBossHp;
		private Image _uiBossProgressImg;
		private Text _uiBossHpText;
		private Image _uiBossIcon;
		private Text _uiBossNameText;
		private Text _uiBossScore;
		private GameObject _uiBossSkillCd;
		private Image _uiBossSkillProgress;
		private GameObject _uiLittleWerewolfCd;
		private Image _uiLittleWerewolfProgress;
		

        protected override void ParseComponent()
        {
			_uiBossHp = Find("Root/uiBoss/uiBossHp");
			_uiBossProgressImg = Find<Image>("Root/uiBoss/uiBossHp/uiBossProgressImg");
			_uiBossHpText = Find<Text>("Root/uiBoss/uiBossHp/uiBossHpText");
			_uiBossIcon = Find<Image>("Root/uiBoss/uiBossHp/uiHeader/uiHeader/uiBossIcon");
			_uiBossNameText = Find<Text>("Root/uiBoss/uiBossHp/uiBossNameText");
			_uiBossScore = Find<Text>("Root/uiBoss/uiBossHp/uiBossScore");
			_uiBossSkillCd = Find("Root/uiBoss/uiBossSkillCd");
			_uiBossSkillProgress = Find<Image>("Root/uiBoss/uiBossSkillCd/uiBossSkillProgress");
			_uiLittleWerewolfCd = Find("Root/uiBoss/uiLittleWerewolfCd");
			_uiLittleWerewolfProgress = Find<Image>("Root/uiBoss/uiLittleWerewolfCd/uiLittleWerewolfProgress");
			
        }
    }
}
