﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class UIUnitStateView : UIBaseView
    {
		private GameObject _uIUnitNameItem;
		private GameObject _uIUnitSleepItem;
		private GameObject _uIUnitAngryItem;
		

        protected override void ParseComponent()
        {
			_uIUnitNameItem = Find("UIUnitNameItem");
			_uIUnitSleepItem = Find("UIUnitSleepItem");
			_uIUnitAngryItem = Find("UIUnitAngryItem");
			
        }
    }
}
