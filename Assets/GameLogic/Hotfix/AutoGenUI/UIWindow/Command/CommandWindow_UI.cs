﻿using UnityEngine;
using UnityEngine.UI;
using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public partial class CommandWindow : UIWindowBase
    {
		private GameObject _scrollView;
		private Text _contentTxt;
		private InputField _cmdInput;
		

        public CommandWindow()
        {
            _uiName = "Command";
        }

        protected override void ParseComponent()
        {
			_scrollView = Find("ScrollView");
			_contentTxt = Find<Text>("ScrollView/Viewport/Content/ContentTxt");
			_cmdInput = Find<InputField>("CmdInput");
			
        }
    }
}
