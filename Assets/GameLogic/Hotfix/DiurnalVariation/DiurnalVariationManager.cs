using Cysharp.Text;
using UnityEngine;
using System;
using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;

namespace GameHotfix.Game
{
    public class DiurnalVariationmManager : Framework.DataModelBase<DiurnalVariationmManager>
    {
        //初始化标记
        private bool _isInit;
        
        //所用的所有时间段的颜色
        private Color[] _colors;
        
        //激活与否
        private bool _active;
        
        //一天的时间(秒)
        private float _oneDayTime = 30f;
        
        //昼夜时间
        private float _dayTimer;
        //当前颜色
        private Color _currentColor;
        
        // //左边篝火
        // private GameObject _fireLeft;
        // //右边篝火
        // private GameObject _fireRight;
        //总时间
        private float _time;
        
        // 地图
        private List<GameObject> Maps;
        // 墙
        private Transform _wall;
        private Transform _wallBig;
        // 障碍列表
        private List<int> _obstacleIndex;
        
        //状态
        private enum EDayState
        {
            e_day = 1,      //白天
            e_night,        //黑夜
            e_excess,       //过度
            e_over,         //结束
        }
        private EDayState _state;
        
        //当前天数
        private int _curDay;
        //最大天数
        private int _maxDay;
        //一天进入到黑夜的时间(秒 即狼人出现的时间)
        private int _werewolfTime;
        //过度速度
        private float _excessiveSpeed;
        
        //进入白天回调
        private Action _enterDayAction;
        //进入黑夜倒计时回调
        private Action<float> _nightTimeAction;
        //进入黑夜回调
        private Action _enterNightAction;
        
        //初始化
        public void Init()
        {
            if (_isInit)
            {
                GameInit.Framework.Logger.LogWarning("昼夜管理器,已经初始化了!");
                return;
            }
            
            //得到时段颜色
            _colors = new Color[5];
            Color nowColor = Color.white;
            //早晨颜色
            // ColorUtility.TryParseHtmlString("#E1FFF5", out nowColor);
            ColorUtility.TryParseHtmlString("#FFFFFF", out nowColor);
            _colors[0] = nowColor;
            //中午颜色
            ColorUtility.TryParseHtmlString("#FFFFFF", out nowColor);
            _colors[1] = nowColor;
            //傍晚颜色
            ColorUtility.TryParseHtmlString("#DD7007", out nowColor);
            _colors[2] = nowColor;
            //晚上颜色
            ColorUtility.TryParseHtmlString("#133A95", out nowColor);
            _colors[3] = nowColor;
            //早晨颜色
            ColorUtility.TryParseHtmlString("#E1FFF5", out nowColor);
            _colors[4] = nowColor;
            
            //设置默认颜色
            Shader.SetGlobalColor("_SkyColor", _colors[0]);
            
            // //得到篝火
            // _fireLeft = GameObject.Find("EffectRoot/FireWoodLeft/Fire");
            // _fireLeft.SetActive(false);
            // _fireRight = GameObject.Find("EffectRoot/FireWoodRight/Fire");
            // _fireRight.SetActive(false);
            
            // 地图
            Maps = new List<GameObject>();
            var landRoot = GameObject.Find("LandRoot").transform;
            Maps.Add(landRoot.Find("Land01").gameObject);
            Maps.Add(landRoot.Find("Land02").gameObject);
            Maps.Add(landRoot.Find("Land03").gameObject);
            
            // 墙
            _wall = GameObject.Find("Wall").transform;
            _wallBig = GameObject.Find("WallBig").transform;

            //默认为没有激活
            _active = false;

        }
        /// <summary>
        /// 初始化时间
        /// </summary>
        public void InitTime(float oneDayTime, int maxDay, int werewolfTime, float excessiveSpeed)
        {
            //得到一天的时间(秒)
            _oneDayTime = oneDayTime;
            //得到最大天数
            _maxDay = maxDay;
            //一天进入到黑夜的时间(秒 即狼人出现的时间)
            _werewolfTime = werewolfTime;
            //过度速度
            _excessiveSpeed = excessiveSpeed;
        }

        //Update
        public void Update(float fTick)
        {
            if (_active == false) return;

            float tick = fTick;
            
            if (_state == EDayState.e_day)
            {
                //执行进入黑夜倒计时回调
                float dTime = _werewolfTime - _time;
                if (dTime <= 0f) dTime = 0f;
                if (_nightTimeAction != null) _nightTimeAction.Invoke(dTime);
                
                //时间到达狼人出现时间点
                if (_time >= _werewolfTime)
                {
                    //执行进入黑夜回调
                    if (_enterNightAction != null) _enterNightAction.Invoke();
                    
                    //不再激活
                    _active = false;
                    
                    //状态切换为黑夜
                    _state = EDayState.e_night;

                    //----
                    // 进入夜晚
                    BattleManager.Instance.EnterNight(_curDay);
                    
                    //Log
                    GameInit.Framework.Logger.LogGreen("狼人出现进入黑夜屠杀!!!");
                    //跳出
                    return;
                }
            }
            else if (_state == EDayState.e_night)
            {
                return;
            }
            else if (_state == EDayState.e_excess)
            {
                tick = fTick * _excessiveSpeed;
            }
            else if (_state == EDayState.e_over)
            {
                tick = fTick * _excessiveSpeed * 1.3f;
            }
            
            //总时间流逝
            _time += fTick;
            
            //时间开始推进，乘以4是因为我们有四个时段，这样再乘以后面的一整天时间才是准确的。
            _dayTimer += tick * 4 / _oneDayTime;
            //一个循环结束了
            if (_dayTimer >= (float)_colors.Length - 1)
            {
                _dayTimer = 0;

                if (_state == EDayState.e_excess)
                {
                    //结束过度
                    EndExcess();
                }
                else if (_state == EDayState.e_over)
                {
                    //结束
                    EndRound();
                }
            }
            
            // //采样两个颜色，第一个是即将过去的时间颜色，第二个是即将到来的时间颜色，我们通过两个数学方法向下和向上取整。
            // Color color01 = _colors[Mathf.FloorToInt(_dayTimer)];
            // Color color02 = _colors[Mathf.CeilToInt(_dayTimer)];
            // //两个颜色的占比，我们通过取时间的小数部分，就可以了。
            // float weight = _dayTimer - Mathf.FloorToInt(_dayTimer);
            // //利用权重来对颜色进行融合
            // _currentColor = Color.Lerp(color01, color02, weight);
            // //Debug.Log(_currentColor);
            // //修改全局颜色，_SkyColor已经写到所有的shader里了
            // Shader.SetGlobalColor("_SkyColor", _currentColor);
            // //修改全局时间，这个主要是控制天空盒的贴图融合
            // Shader.SetGlobalFloat("_DayAndNightChange", _dayTimer);
        }
        
        //开始一轮昼夜
        public void StartARound(Action enterDayAction, Action<float> nightTimeAction, Action enterNightAction)
        {
            //激活
            _active = true;
            
            //初为早晨颜色
            _dayTimer = 0;
            _currentColor = _colors[0];
            Shader.SetGlobalColor("_SkyColor", _currentColor);

            //总时间
            _time = 0f;
            
            //设置状态为白天
            _state = EDayState.e_day;
            
            //当前天数
            _curDay = 1;
            
            //设置回调函数
            _enterDayAction = enterDayAction;
            _nightTimeAction = nightTimeAction;
            _enterNightAction = enterNightAction;
            
            //执行进入白天的回调
            if (_enterDayAction != null) _enterDayAction.Invoke();

            //----
            // 开始流程
            BattleManager.Instance.StartProcedure(_curDay);
            
            //Log
            GameInit.Framework.Logger.LogGreen(ZString.Concat("开始一轮游戏! 当前天数为: ", _curDay));
        }
        
        //重新开始昼夜(在狼人死亡或者玩家死完表现后调用)
        public void Restart(bool isKillBoss)
        {
            //Log
            if(isKillBoss)
                GameInit.Framework.Logger.LogGreen("狼人死亡!");
            else 
                GameInit.Framework.Logger.LogGreen("玩家死完!");
            
            //当前天数++
            _curDay += 1;
            //当前天数>总天数 表示本轮游戏结束
            if (_curDay > _maxDay)
            {
                //Log
                GameInit.Framework.Logger.LogGreen("本轮游戏结束!...迅速切换到白天");
                // 结束
                BattleEnd();
                //----
                //走一轮游戏结束流程
                if(isKillBoss)
                    // 玩家胜利
                    BattleModel.Instance.DispatchEvent(BattleModel.EventBattleEnd,UnitType.Role);
                else 
                    // boss胜利
                    BattleModel.Instance.DispatchEvent(BattleModel.EventBattleEnd,UnitType.Boss);
                return;
            }
            
            //状态切换为过度
            _state = EDayState.e_excess;

            // 更换地图

            //总时间为零
            _time = 0f;
            //激活
            _active = true;
            
           // 重置流程
           BattleManager.Instance.ResetProcedure(_curDay);
            
            // 场景过度
            var overAnimationView = new OverAnimationView();
            // 添加全黑屏回调
            overAnimationView.AddFullCallBack(()=>ChangeMap(0));
            // 定时打开场景过度
            TimerHeap.AddTimer(4000, 0,
                () => AlertDialogView.Launch(overAnimationView, OverAnimationView.LeftToRight,$"第{GameConstantHelper.ConvertToChinese(_curDay)}波"));
            
            //Log
            GameInit.Framework.Logger.LogGreen(ZString.Concat("过度到白天中... 当前天数为: ", _curDay));
        }
        
        //结束过度
        private void EndExcess()
        {
            //状态切换为白天
            _state = EDayState.e_day;
            //总时间为零
            _time = 0f;
            //激活
            _active = true;
            
            //执行进入白天的回调
            if (_enterDayAction != null) _enterDayAction.Invoke();
            
            //Log
            GameInit.Framework.Logger.LogGreen(ZString.Concat("过度结束进入白天 当前天数为: ", _curDay));
            
            //----
            // 结束过度
            BattleManager.Instance.EndExcess(_curDay);
        }
        
        //结束一轮
        private void EndRound()
        {
            //状态切换为白天
            _state = EDayState.e_day;
            //总时间为零
            _time = 0f;
            //激活
            _active = false;
            
            //Log
            GameInit.Framework.Logger.LogGreen("本轮游戏结束!...迅速切换到白天完成");
        }
        
        // 战斗结束
        public void BattleEnd()
        {
            //状态切换为结束
            _state = EDayState.e_over;

            // 更换地图

            //总时间为零
            _time = 0f;
            //激活
            _active = true;
        }
        
        // 更换地图
        public void ChangeMap(int mapIndex)
        {
            for (int i = 0; i < Maps.Count; i++)
            {
                Maps[i].SetActive(mapIndex == i);
            }
        }
        
        // 创建地图障碍
        public void CreateWallObstacle(BattleType battleType)
        {
            Transform wall = null;
            switch (battleType)
            {
                case BattleType.Normal:
                    wall = _wall;
                    break;
                case BattleType.FieldBoss:
                    wall = _wallBig;
                    break;
            }
            
            if(_obstacleIndex == null)
                _obstacleIndex = new List<int>();
            // 删除旧障碍
            else if(_obstacleIndex.Count > 0)
            {
                RVOManager.Instance.DeleteObstacle(_obstacleIndex, RVOLayer.Layer01);
                _obstacleIndex.Clear();
            }

            // 创建新障碍
            for (int i = 0; i < wall.childCount; i++)
            {
                var wallItem = _wall.GetChild(i);
                var width = wallItem.localScale.x/2;
                var length = wallItem.localScale.z/2;

                var indexs = RVOManager.Instance.CreateObstacle(wallItem.position, length, width, RVOLayer.Layer01);
                // 记录障碍id
                for(int j = 0; j < indexs.Count;j++)
                    _obstacleIndex.Add(indexs[j]);
            }
        }
    }
}