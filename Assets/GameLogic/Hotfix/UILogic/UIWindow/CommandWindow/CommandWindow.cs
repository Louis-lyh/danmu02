using System.Text;
using GameHotfix.Framework;
using GameInit.Framework;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public partial class CommandWindow
    {
        protected override void InitWindowParam()
        {
            Layer = WindowLayer.Top;
            WindowId = UIWindowId.UIMainWindow;
        }

        private ScrollRect _scrollRect;
        protected override void ParseComponentExt()
        {
            base.ParseComponentExt();
            _scrollRect = _scrollView.GetComponent<ScrollRect>(); 
            _cmdInput.onEndEdit.AddListener(OnSubmit);
            _stringBuilder = new StringBuilder();
        }

        private StringBuilder _stringBuilder;
        private void OnSubmit(string value)
        {
            if (!value.IsValid())
                return;
            _cmdInput.text = "";
            _stringBuilder.AppendLine(value);
            _contentTxt.text = _stringBuilder.ToString();
            _scrollRect.verticalNormalizedPosition = 0f;
        }
    }
}