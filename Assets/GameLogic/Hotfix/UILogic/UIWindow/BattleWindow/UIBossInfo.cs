using System;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public partial class UIBossInfo
    {
        // boss
        private int _bossHp;
        
        protected override void ParseComponentExt()
        {
            base.ParseComponentExt();
            // 初始化
            Init();
        }
        
        // 注册事件
        protected override void RegisterEvent()
        {
            // 刷新bossHP
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventUpDateBossHp,RefreshBossHp);
            // 显示boss血条
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventShowBossHp,ShowBossHp);
            // 显示声波技能icon
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventFreeSonicSkillIcon,ShowFreeSonicSkill);
            // 显示小狼人技能icon
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventFreeLittleWerewolfSkill,ShowLittleWerewolfSkill);
        }

        // 注销事件
        protected override void UnRegisterEvent()
        {
            // 刷新bossHP
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventUpDateBossHp,RefreshBossHp);
            // 显示boss血条
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventShowBossHp,ShowBossHp);
            // 显示声波技能icon
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventFreeSonicSkillIcon,ShowFreeSonicSkill);
            // 显示小狼人技能icon
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventFreeLittleWerewolfSkill,ShowLittleWerewolfSkill);
        }
        
        protected override void Refresh(params object[] arg)
        {
            // 隐藏boss血条
            _uiBossHp.SetActive(false);
            // 隐藏技能
            _uiBossSkillCd.SetActive(false);
            _uiLittleWerewolfCd.SetActive(false);
        }
        
        private void Init()
        {
            _bossHp = 100;
            // 初始化进度条
            _uiBossProgressImg.fillAmount = 1;
            _uiBossHpText.text = "100%";
        }
        
        // 动画队列
        private Sequence _bossHpSeq;

        private Tweener _bossHpTweener;
        // 显示bossHp
        private async void ShowBossHp(BaseEvent e)
        {
            if (e.EventObj == null)
            {
                HideHp();
                return;
            }
            // boss数据
            if (e.EventObj is BossData)
            {
                var bossData = (BossData)  e.EventObj;
                // 名称
                _uiBossNameText.text = GameNoticeModel.FormatName( bossData.Name);
                // 分数
                _uiBossScore.text = ""+bossData.KillScore;
                // 设置头像
                SetHeaderIcon(_uiBossIcon,bossData.HeadUrl);
            }
            else if(e.EventObj is MonsterBossData)
            {
                var bossData = (MonsterBossData)  e.EventObj;
                // 名称
                _uiBossNameText.text = GameNoticeModel.FormatName(bossData.Name);
                
                // 资源配置
                var roleRes = GameInit.Game.RoleResConfig.Get(bossData.RoleDef.ResId);
                _uiBossIcon.sprite =  await SpriteAtlasMgr.Instance.LoadSprite(roleRes.IconAtlas, roleRes.RoleIcon);
                // 分数
                _uiBossScore.text = "";
            }
            
            _uiBossHp.SetActive(true);
        }
        
        // 刷新Boss hp
        private void RefreshBossHp(BaseEvent e)
        {
            // 新值
            var data = (FighterUnitData) e.EventObj;
            var ratio = data.CurrentHp / (float)data.MaxHp;
            var newValue = (int)(ratio * 10000);
            if (newValue <= 0)
                newValue = 0;

            // 提前完成
            if(_bossHpTweener != null)
                _bossHpTweener.Complete();
            // 动画
            _bossHpTweener = RefreshHpProgress(newValue, ref _bossHp, (value) =>
            {
                // 进度条
                _uiBossProgressImg.fillAmount = value / 10000f;
                _uiBossHpText.text = $"{value/100f:0.00}%";
            });
            
            // boss数据
            if (data is BossData)
            {
                var bossData = (BossData)  data;
                //
                var score = long.Parse(_uiBossScore.text);
                score += 10;
                score = Math.Min(score, bossData.KillScore);
                // 更新分数
                _uiBossScore.text = ""+score;
            }
        }
        
        // 刷新进度
        private Tweener RefreshHpProgress(int newValue,ref int oldValue,Action<int> callBack)
        {
            if (newValue <= 0)
                newValue = 0;

            // 创建新队列
            if (_bossHpSeq == null)
                _bossHpSeq = DOTween.Sequence();
           
            // 动画时间
            var duration = (oldValue - newValue) * 0.1f;
            // 动画
            var tempValue = oldValue;
            var doTween = DOTween.To(
                () => tempValue,
                value => callBack(value),
                newValue,duration);
            
            // 记录当前复活次数
            oldValue = newValue;
            
            return doTween;
        }

        // 设置头像
        private async void SetHeaderIcon(Image header,string url)
        {
            // 加载头像
            var speite  = await HeadImageDownloader.LoadHeadSprite(url);
            if(header == null)
                return;
            
            header.sprite = speite;
        }
        
        // 显示声波技能倒计时
        private void ShowFreeSonicSkill(BaseEvent @event)
        {
            // 持续时间
            var duration = (float) @event.EventObj;
            // 显示
            _uiBossSkillCd.SetActive(true);
            // 技能倒计时
            SkillCountdown(_uiBossSkillProgress,duration,
                ()=>_uiBossSkillCd.SetActive(false));
        }
        // 显示召唤小狼人倒计时
        private void ShowLittleWerewolfSkill(BaseEvent @event)
        {
            // 持续时间
            var duration = (float) @event.EventObj;
            // 显示
            _uiLittleWerewolfCd.SetActive(true);
            // 技能倒计时
            SkillCountdown(_uiLittleWerewolfProgress,  duration,
                ()=>_uiLittleWerewolfCd.SetActive(false));
        }
        // 技能倒计时
        private void SkillCountdown(Image image, float duration,Action completeBack)
        {
            DOTween.To(() => 1f, fill => image.fillAmount = fill,0f,duration)
                .SetEase(Ease.Linear)
                .OnComplete(()=>completeBack?.Invoke());
        }
        
        // 隐藏
        private void HideHp()
        {
            _uiBossHp.SetActive(false);
            // 隐藏
            _uiBossSkillCd.SetActive(false);
            _uiLittleWerewolfCd.SetActive(false);
        }
    } 
}


