using System.Collections.Generic;
using GameHotfix.Game.Utils;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class UIUnitStateView
    {
        // nameItem 字典
        private Dictionary<long, UIUnitNameItem> _unitNameItems;
        // 3d 画布
        private Transform _3dCanvas;
        // 3d name Item
        private Transform _3dUnitNameItem;
        protected override void ParseComponentExt()
        {
            // 隐藏item
            _uIUnitNameItem.SetActive(false);
            _uIUnitAngryItem.SetActive(false);
            _uIUnitSleepItem.SetActive(false);
            // 初始化字典
            _unitNameItems = new Dictionary<long, UIUnitNameItem>();
            //
            //_3dCanvas = GameSceneControl.Instance._3dCanvas;
            _3dCanvas =  GameObject.Find("SceneRoot/3DCanvas").transform;
            _3dUnitNameItem = _3dCanvas.Find("UIUnitNameItem");
            _3dUnitNameItem.gameObject.SetActive(false);
        }

        protected override void RegisterEvent()
        {
            
        }

        protected override void UnRegisterEvent()
        {
            
        }
        
        // 更新战斗单位血
        public void UpdateUnitBlood(FighterUnit info)
        {
            // 刷新
            if (_unitNameItems.ContainsKey(info.FighterData.UnitID))
            {
                _unitNameItems[info.FighterData.UnitID].Show(info);
            }
            else
                // 创建
                CreateUnitBlood(info);
        }
        
        // 显示收集进度条
        public void SetUnitCollect(FighterUnit info,bool isShow)
        {
            // 刷新
            if (_unitNameItems.ContainsKey(info.FighterData.UnitID))
            {
                _unitNameItems[info.FighterData.UnitID].SetCollect(isShow);
            }
        }
        
        // 刷新进度条
        public void UpdateCollectProgress(FighterUnit info,float fill)
        {
            // 刷新
            if (_unitNameItems.ContainsKey(info.FighterData.UnitID))
            {
                _unitNameItems[info.FighterData.UnitID].UpdateCollectProgress(fill);
            }
        }
        // 创建血条
        private void CreateUnitBlood(FighterUnit info)
        {
            var item =UIItemPool.GetUIItem(_3dUnitNameItem).gameObject;
            item.SetActive(true);
            item.transform.SetParent(_3dCanvas);
            // 脚本
            var unitBlood = new UIUnitNameItem();
            unitBlood.SetDisplayObject(item);
            unitBlood.Show(info);
            // 加入字典
            _unitNameItems.Add(info.FighterData.UnitID,unitBlood);
        }
        // 删除战斗单位血条
        public void DeleteUnitBlood(long unitId)
        {
            if (_unitNameItems.ContainsKey(unitId))
            {
                // 回收item
                UIItemPool.CollectItem(_unitNameItems[unitId].RectTransform.transform);
                // 销毁脚本
                _unitNameItems[unitId].Dispose();
                _unitNameItems.Remove(unitId);
            }
        }
    }
}

