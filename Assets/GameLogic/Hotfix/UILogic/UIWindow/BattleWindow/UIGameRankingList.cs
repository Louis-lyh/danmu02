using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameInit.Game.BaseEvent;
using Vector2 = UnityEngine.Vector2;

namespace GameHotfix.Game
{
    public partial class UIGameRankingList
    {
        // 玩家排行数据
        private List<RoleData> _playerDataList;

        // 玩家item
        private List<Transform> _playerRankingItems;

        // 玩家当前复活次数
        private int _playerHp;

        protected override void ParseComponentExt()
        {
            base.ParseComponentExt();
            // 初始化
            Init();
        }
        
        // 注册事件
        protected override void RegisterEvent()
        {
            // 刷新玩家排行
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventUpdatePlayerRanking,RefreshPlayerData);
            // 刷新玩家复活次数
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventUpdatePlayerHp,RefreshPlayerHp);
        }

        // 注销事件
        protected override void UnRegisterEvent()
        {
            // 刷新玩家排行
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventUpdatePlayerRanking,RefreshPlayerData);
            // 刷新玩家复活次数
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventUpdatePlayerHp,RefreshPlayerHp);
        }
        
        protected override void Refresh(params object[] arg)
        {
        }
        
        // 初始化
        private void Init()
        {
            // 隐藏Item
            _uiPlayerRankingItem.SetActive(false);
            
            _playerHp = 100;
        }

        // 动画队列
        private Sequence _playerHpSeq;
        // 刷新玩家剩余复活次数
        private void RefreshPlayerHp(BaseEvent e)
        {
            // 新值
            var newValue = (int) e.EventObj;
            if (newValue <= 0)
                newValue = 0;
            
            
            // 动画
            var tweener = RefreshHpProgress(newValue, ref _playerHp, (value) =>
            {
                // // 文本
                // _uiPlayerProgressText.text = value.ToString();
                // // 进度条
                // _uiPlayerProgressImg.fillAmount = value / 100f;
            });
            
            // 加入队列
            if (_playerHpSeq == null)
                _playerHpSeq = DOTween.Sequence();

            _playerHpSeq.Append(tweener);
        }
        
        private Tweener _bossHpTweener;
        // 刷新进度
        private Tweener RefreshHpProgress(int newValue,ref int oldValue,Action<int> callBack)
        {
            if (newValue <= 0)
                newValue = 0;

            // 动画时间
            var duration = (oldValue - newValue) * 0.1f;
            // 动画
            var tempValue = oldValue;
            var doTween = DOTween.To(
                () => tempValue,
                value => callBack(value),
                newValue,duration);
            
            // 记录当前复活次数
            oldValue = newValue;
            
            return doTween;
        }

        // 刷新玩家排行榜
        private void RefreshPlayerData(BaseEvent e)
        {
            // 玩家排行数据
            _playerDataList = (List<RoleData>)e.EventObj;
            _playerDataList.Sort((a,b)=>b.Exp - a.Exp);
            if (_playerDataList.Count > 5)
                _playerDataList = _playerDataList.GetRange(0, 5);
            
            // 初始化列表
            if (_playerRankingItems == null)
            {
                _playerRankingItems = new List<Transform>();
            }
            // 更新排行榜
            UpdateRanking(_playerDataList, _playerRankingItems,_uiPlayerRankingItem);
        }

        // 更新排行榜
        private void UpdateRanking(List<RoleData> rankingData,List<Transform> rankingItemList,GameObject itemPrefab)
        {
            // 新列表
            var newPlayerRankingItems = new List<Transform>();
            
            // 刷新排行榜
            for (int i = 0; i < rankingData.Count; i++)
            {
                // 玩家数据
                var playerData = rankingData[i];
                // 找到Item
                var item = rankingItemList.Find(item => { return item.name.Equals(playerData.Name); });
                
                if( item != null)
                {
                    // 移除旧列表
                    rankingItemList.Remove(item);
                }
                else
                {
                    // 没有对应Item重新生成
                    item = CreatePlayerItem(playerData, itemPrefab,i);
                }
                
                // 加入新列表
                newPlayerRankingItems.Add(item);
                // 更新Item
                UpDatePlayerItem(item, playerData,itemPrefab, i);
            }
            
            // 回收的item
            for (int i = 0; i < rankingItemList.Count; i++)
            {
                rankingItemList[i].DOKill();
                // 重置名称
                rankingItemList[i].name = itemPrefab.name;
                UIItemPool.CollectItem(rankingItemList[i]);
            }
            
            // 保存新列表
            rankingItemList.Clear();
            foreach (var item in newPlayerRankingItems)
            {
                rankingItemList.Add(item);
            }
        }

        // 更新Item
        private void UpDatePlayerItem(Transform item,RoleData playerData,GameObject itemPrefab,int order)
        {
            // 删除动画
            item.DOKill();
            // 移动
            item.DOLocalMove(itemPrefab.transform.localPosition - Vector3.up * order * 110,0.5f);
            // 更新分数
            UpdateScore(item, playerData.KillScore);
            
            // 更新icon
            UpdateOrderIcon(item,order + 1,playerData.HeadUrl,0);
            
            // 狼人Icon
            var langIcon = item.Find("uiLangIcon");
            var existBoss = BattleModel.Instance.GetBossPlayerUerId() != String.Empty;
            langIcon.gameObject.SetActive(BattleModel.Instance.GetBossTopNum(GameConstantHelper.RandomPlayerNum) > order && !existBoss);
            
            // 等级
            var uiLevel = item.Find("uiLevel").GetComponent<Text>();
            uiLevel.text = $"Lv.{playerData.Level} \n {playerData.RoleDef.SpeciesName}";
            
            // 设置颜色
            uiLevel.color = LevelColor(playerData.Level);
            
            // 生命值
            var hpPressbar = item.Find("hp/uiHpPressbar").GetComponent<Image>();
            hpPressbar.fillAmount = (playerData.CurrentHp / (float) playerData.MaxHp);
            
            // 复活倒计时
            var unit = BattleManager.Instance.GetFighterByUID(playerData.UnitID,true);
            var behaviorData = unit.BehaviorData;
            var deathTime = behaviorData.DeathTimer;
            var reviveImage = item.Find("uiHeader/uiHeaderMask/uiReviveTime").GetComponent<Image>();
            reviveImage.gameObject.SetActive(deathTime > 0);
            if (deathTime != -1)
            {
                var curTime = ServerTimerManager.Instance.GameTime;
                var reviveTimeText = item.Find("uiHeader/uiHeaderMask/uiReviveTime/uiReviveTimeText").GetComponent<Text>();
                // 剩余时间
                var cd = GameConstantHelper.FieldBossReviveCd - (curTime - deathTime);
                reviveImage.gameObject.SetActive(cd > 0);
            
                if (cd >= 0)
                {
                    reviveImage.fillAmount = cd / (float)GameConstantHelper.FieldBossReviveCd;
                    reviveTimeText.text = "" + cd;
                }
            }
        }
        // 生成PlayerItem
        private Transform CreatePlayerItem(FighterUnitData playerData,GameObject itemPrefab,int order)
        {
            // 实例化对象            
            var item =  UIItemPool.GetUIItem(itemPrefab.transform);
            
            item.SetParent(itemPrefab.transform.parent);
            item.gameObject.SetActive(true);
            // 设置item信息
            item.name = playerData.Name;
            item.Find("uiNameBg/uiNameText").GetComponent<Text>().text = GameNoticeModel.FormatName(playerData.Name);
            item.localPosition = itemPrefab.transform.localPosition - Vector3.up * 5 * 80;
            // 设置缩放
            item.localScale = Vector3.one;
            
            return item;
        }

        // 更新分数
        private void UpdateScore(Transform item,long score)
        {
            // 分数组件
            var uiScore = item.Find("uiScore");
            // 分数没用改变退出
            if(uiScore.GetComponent<Text>().text.Equals(score.ToString()))
                return;
            //删除动画
            uiScore.DOKill();
           
            // 放大动画
            uiScore.DOScale(1.5f, 0.2f).OnComplete(() =>
            {
                // 更新分数
                uiScore.GetComponent<Text>().text = score.ToString();
                uiScore.DOScale(1f, 0.2f).SetDelay(0.1f);
            });
        }
        // 更新排名Icon 38 45
        private async void UpdateOrderIcon(Transform item,int order,string headUrl,float delay)
        {
            // 延迟
            await UniTask.Delay(TimeSpan.FromSeconds(delay));
            
            if(item == null)
                return;
            
            //名字背景
            var nameBg = item.Find("uiNameBg").GetComponent<Image>();
            nameBg.sprite = await SpriteAtlasMgr.Instance.LoadSprite("mingcheng0"+(order > 4 ? 4 : order));
            
            // 设置头像
            SetHeaderIcon(item.Find("uiHeader/uiHeaderMask/uiHeaderIcon").GetComponent<Image>(),headUrl);
            
            // 设置头像框
            var uiHeaderFrame = item.Find("uiHeader/uiHeaderFrame").GetComponent<Image>();
            uiHeaderFrame.sprite = await SpriteAtlasMgr.Instance.LoadSprite("touxiang0"+(order > 4 ? 4 : order));
            
            // 排名icon
            var uiIcon = item.Find("uiRankingIcon").GetComponent<Image>();
            uiIcon.sprite = await SpriteAtlasMgr.Instance.LoadSprite("LV0"+(order > 5 ? 5 : order));
            uiIcon.SetNativeSize();
            
            // 排名文本
            var uiRankingText = uiIcon.transform.Find("uiRankingText");
            uiRankingText.gameObject.SetActive(order > 5);
        }
        
        // 设置头像
        private async void SetHeaderIcon(Image header,string url)
        {
            // 加载头像
            var speite  = await HeadImageDownloader.LoadHeadSprite(url);
            if(header == null)
                return;
            
            header.sprite = speite;
        }
        
        // 等级颜色
        private Color LevelColor(int level)
        {
            // 名字
            Color nameColor = Color.white;
            
            if(level >= 45)
                nameColor = Color.red;
            else if(level >= 35)
                nameColor = new Color(1f,0.4588236f,0.1098039f,1);
            else if(level >= 25)
                nameColor = new Color(0.8705883f,0.5294118f,1f,1);
            else if(level >= 15)
                nameColor = new Color(0.03137255f,0.909804f,0.9921569f,1f);
            else if(level >= 5)
                nameColor = new Color(0.2196079f,0.8039216f,0.2196079f,1f);
            
            return nameColor;
        }
    }
       

}

