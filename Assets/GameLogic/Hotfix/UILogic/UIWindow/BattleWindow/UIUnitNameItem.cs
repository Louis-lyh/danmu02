using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks.Triggers;
using DG.Tweening;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;
using GameInit.Game.Pet;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameHotfix.Game
{
    public partial class UIUnitNameItem
    {
        // 当前进化欲望
        private UpgradeDesire _upgradeDesire;
        // 动画
        private uint _upgradeDesireTimer;
        // 是否显示
        private bool _isShow;
        // 等级
        private int _level;
        // 是否是玩家
        private bool _isPlayer = true;
        protected override void ParseComponentExt()
        {
            // 隐藏
            HideUpgradeDesireTips();
            _level = 0;
            // 隐藏
            _uiCollect.SetActive(false);
            // 弹窗
            _uiDialogBox.transform.localScale = Vector3.zero;
            // 隐藏点赞
            _uiLike.gameObject.SetActive(false);
            // 隐藏宠物信息
            _uiPet.SetActive(!_isPlayer);
            // 显示玩家信息
            _uiPlayer.SetActive(_isPlayer);
            // 隐藏野外boss
            _uiBoss.SetActive(false);

        }

        protected override void Refresh(params object[] arg)
        {
            var info = arg[0] as FighterUnit;
            
            // 是否是玩家
            var isPlayer = info is FighterUnitRole;
            
            if (isPlayer != _isPlayer)
            {
                _isPlayer = isPlayer;
                _uiPlayer.SetActive(_isPlayer);
                // 置顶层级避免遮挡玩家
                if(!isPlayer)
                    RectTransform.SetAsFirstSibling();
            }
            
            
            // 刷新UI
            if (_isPlayer)
                ShowPlayerInfo(info);
            else if(info is PetUnit)
                ShowPetName(info);
            else if(info is MonsterGuard)
                _uiMonsterGuard.gameObject.SetActive(true);
            else if (info is FieldBoss)
                ShowFieldBossInfo(info);
            
            _uiPet.SetActive(info is PetUnit);
            _uiMonsterGuard.gameObject.SetActive(info is MonsterGuard);

            
            // 跟随单位
            Follow(info);
        }
        
        // 跟随
        private void Follow(FighterUnit info)
        {
            // 设置缩放
            var scaleAttribute = 1 +  info.FighterData.GetAttributeAddition(AttributeType.BodyScale) / 100;
            var scale = info.FighterData.ModelScale * scaleAttribute * 0.02f;
            scale = Mathf.Clamp(scale, 0.01f, 0.05f);
            
            // 名字缩放
            RectTransform.localScale = scale * Vector3.one;
            // 名字高度
            var nameHeight = info.FighterData.NameHeight;
            // 设置名字坐标
            SetPos(info.Position + new Vector3(0,nameHeight,0) * (info.FighterData.ModelScale * scaleAttribute));
        }

        // 显示宠物名称
        private void ShowPetName(FighterUnit info)
        {
            // 宠物数据
            var petData = info.FighterData as PetData;
            if(petData == null || petData._owner == null || petData._owner.FighterData == null)
                return;
            // 显示名称
            var ownerName = petData._owner.FighterData.Name;
            ownerName = GameNoticeModel.FormatName(ownerName);
            _uiPetText.text = $"{ownerName}的神兽";
            // 颜色
            _uiPetText.color = LevelColor(petData._owner.FighterData.Level);
        }

        // 显示玩家信息
        private void ShowPlayerInfo(FighterUnit info)
        {
            // 刷新提示
            UpdateTips(info);

            var fighterData = info.FighterData;
            // 显示名字
            var name = GameNoticeModel.FormatName(fighterData.Name);
            _uiNameText.text = name;
            _uiLevelText.text = "" + fighterData.Level;
            // 显示血量 
            var curHp = (float) fighterData.CurrentHp;
            var ratio = curHp / fighterData.MaxHp;
            _uiBlooGress.fillAmount = ratio;
            
            // 显示经验
            var curExp = (float)fighterData.Exp - fighterData.LastExpMax;
            var ratioExp = curExp / (fighterData.ExpMax - fighterData.LastExpMax);
            _uiExpProgress.fillAmount = ratioExp;

            // 刷新颜色
            FormatPlayerInfo(info);
            
            // 显示对话框
            var roleData = info.FighterData as RoleData;
            if (roleData != null)
            {
                var dialogBos = roleData?.ShowTipsText;
                roleData.SetDialogText(string.Empty);
                ShowDialogBox(dialogBos);
            }
            // 显示点赞
            if (roleData != null&& !_uiLike.gameObject.activeSelf && roleData.ShowLikeTips)
            {
                ShowLikeTips();
                roleData.ShowLikeTips = false;
            }
            
            // 显示bossIcon
            _uiBossIcon.SetActive(roleData?.UserId == BattleModel.Instance.GetBossPlayerUerId());

            if (roleData != null)
            {
                var isOther = !BattleManager.Instance.GuildId.Equals(roleData.GuildId);
                // 显示公会id
                _uiGuildId.text = "公会id："+ roleData.GuildId;
                _uiGuildId.color = isOther ? Color.red : Color.green;

            }
            _uiGuildId.gameObject.SetActive(roleData != null && BattleManager.Instance.GuildId != roleData.GuildId);
        }
        
        // 显示boss信息
        private void ShowFieldBossInfo(FighterUnit info)
        {
            _uiBoss.SetActive(true);
            
            var fighterData = info.FighterData;
            // 显示名字
            var name = fighterData.Name;
            _uiBossNameText.text = name;
            // 显示血量 
            var curHp = (float) fighterData.CurrentHp;
            var ratio = curHp / fighterData.MaxHp;
            _uiBossProgressImg.fillAmount = ratio;

            _uiBossHpText.text = $"{ratio * 100: 00.00}%";
        }

        // 显示对话框
        private Sequence _dialogDoTewwn;
        private void ShowDialogBox(string text)
        {
            if(string.IsNullOrEmpty(text))
                return;
            
            _dialogDoTewwn?.Kill();
            _dialogDoTewwn = DOTween.Sequence();
            // 删除以前动画
            _uiDialogBox.transform.DOKill();
            
            // 显示文本
            _uiDialogBoxText.text = text;
            // 隐藏
            _uiDialogBox.transform.localScale = Vector3.zero;
            // 放大
            _dialogDoTewwn.Append(_uiDialogBox.transform.DOScale(1, 0.5f));
            _dialogDoTewwn.AppendInterval(2f);
            _dialogDoTewwn.Append(_uiDialogBox.transform.DOScale(0, 0.5f));
        }
       
        // 设置位置
        private void SetPos(Vector3 posion)
        {
            // 设置位置
            RectTransform.localPosition = posion;
        }

        #region 进化欲望提示
           // 头顶进化欲望提示
        private void UpdateTips(FighterUnit info)
        {
            return;
            // 显示
            if (info.BehaviorData.IsRelax)
            {
                // 状态不动更新提示
                if (_upgradeDesire != info.BehaviorData.UpgradeDesire || _isShow == false)
                {
                    _upgradeDesire = info.BehaviorData.UpgradeDesire;
                    // 杀死以前动画
                    HideUpgradeDesireTips();
                    // 显示提示
                    UpgradeDesireTips();
                    // 显示
                    _isShow = true;
                }
            }
            // 旺盛状态并且处于移动
            else if(info.BehaviorData.UpgradeDesire == UpgradeDesire.Vigorous 
                    && info.BehaviorData.BehaviorType == FighterBehaviorType.Move)
            {
                // 状态不动更新提示
                if (_upgradeDesire != info.BehaviorData.UpgradeDesire || _isShow == false)
                {
                    _upgradeDesire = info.BehaviorData.UpgradeDesire;
                    // 杀死以前动画
                    HideUpgradeDesireTips();
                    // 显示提示
                    UpgradeDesireTips();
                    // 显示
                    _isShow = true;
                }
            }
            else if(_isShow)
            {
                // 隐藏提示
                HideUpgradeDesireTips();
                // 关闭
                _isShow = false;
            }
        }
        // 隐藏
        private void HideUpgradeDesireTips()
        {
            // 隐藏
            _uiVigorous.localScale = Vector3.zero;
            _uiNormal.localScale = Vector3.zero;
            _uiLazy.localScale = Vector3.zero;
            _uiLazy.gameObject.SetActive(false);
            // 删除动画
            TimerHeap.DelTimer(_upgradeDesireTimer);
            _uiVigorous.DOKill();
            _uiNormal.DOKill();
            _uiLazy01.DOKill();
            _uiLazy02.DOKill();
            _uiLazy03.DOKill();
        }

        // 动画
        private void UpgradeDesireTips()
        {
            return;
            
            switch (_upgradeDesire)
            {
                case UpgradeDesire.Normal:
                    UpgradeDesireTips(_uiNormal,new Vector3(0,54,0), new Vector3(0,74,0));
                    break;
                case UpgradeDesire.Vigorous:
                    UpgradeDesireTips(_uiVigorous,new Vector3(58.7f,7.5f,0),new Vector3(58.7f,27.5f,0));
                    break;
                case UpgradeDesire.Lazy:
                    LazyAnimation();
                    break;
            }
        }
        // 上下移动动画
        private void UpgradeDesireTips(Transform uiTips,Vector3 startPos,Vector3 endPos)
        {
            uiTips.localScale = Vector3.one;
            uiTips.localPosition = startPos;
            uiTips.DOLocalMove(endPos, 0.5f).SetLoops(-1,LoopType.Yoyo);
        }
        // 懒惰动画
        private void LazyAnimation()
        {
            _uiLazy.localScale = Vector3.one;
            _uiLazy.gameObject.SetActive(true);
            _uiLazy01.localPosition = new Vector3(10,14,0);
            _uiLazy01.gameObject.SetActive(false);
            
            _uiLazy02.localPosition = new Vector3(10,14,0);
            _uiLazy02.gameObject.SetActive(false);
            
            _uiLazy03.localPosition = new Vector3(10,14,0);
            _uiLazy03.gameObject.SetActive(false);


            _uiLazy01.DOLocalMove(new Vector3(60, 60, 0), 1.5f)
                .SetLoops(-1, LoopType.Restart)
                .SetEase(Ease.Linear)
                .OnStart(()=>_uiLazy01.gameObject.SetActive(true));
            
            _uiLazy02.DOLocalMove(new Vector3(60, 60, 0), 1.5f)
                .SetLoops(-1, LoopType.Restart)
                .OnStart(()=>_uiLazy02.gameObject.SetActive(true))
                .SetEase(Ease.Linear)
                .SetDelay(0.5f);
            
            _uiLazy03.DOLocalMove(new Vector3(60, 60, 0), 1.5f)
                .SetLoops(-1, LoopType.Restart)
                .OnStart(()=>_uiLazy03.gameObject.SetActive(true))
                .SetEase(Ease.Linear)
                .SetDelay(1.2f);
        }
        
        #endregion
        
        // 显示点赞提示
        private void ShowLikeTips()
        {
            // 隐藏
            _uiLike.gameObject.SetActive(true);
            
            var startPos = _uiLike.localPosition;
            var startRotate = _uiLike.localRotation;

            _uiLike.localPosition = startPos + new Vector3(Random.Range(-10f, 10f), 0, 0);
            
            _uiLike.DOLocalRotate(new Vector3(0,0 , 10), 0.2f).SetLoops(-1,LoopType.Yoyo);
            _uiLike.DOLocalMoveY(25f, 1f).OnComplete(() =>
            {
                // 杀死动画
                _uiLike.DOKill();
                // 重置位置
                _uiLike.localPosition = startPos;
                // 重置旋转
                _uiLike.localRotation = startRotate;
                // 隐藏
                _uiLike.gameObject.SetActive(false);
            });
        }
        // 规范信息
        private void FormatPlayerInfo(FighterUnit info)
        {
            if(_level == info.FighterData.Level)
                return;
            _level = info.FighterData.Level;
            
            //血条
            _uiBloodImg.gameObject.SetActive(_level >= 3);
            //经验
            _uiExpImg.gameObject.SetActive(_level >= 3);
            // 名字颜色
            _uiNameText.color = LevelColor(_level);
        }
        // 等级颜色
        private Color LevelColor(int level)
        {
            // 名字
            Color nameColor = Color.white;
            
            if(level >= 45)
                nameColor = Color.red;
            else if(level >= 35)
                nameColor = new Color(1f,0.4588236f,0.1098039f,1);
            else if(level >= 25)
                nameColor = new Color(0.8705883f,0.5294118f,1f,1);
            else if(level >= 15)
                nameColor = new Color(0.03137255f,0.909804f,0.9921569f,1f);
            else if(level >= 5)
                nameColor = new Color(0.2196079f,0.8039216f,0.2196079f,1f);
            
            return nameColor;
        }

        #region 搜集进度条
        // 显示进度条
        public void SetCollect(bool isShow)
        {
            _uiCollect.gameObject.SetActive(isShow);
        }
        
        // 刷新进度条
        public void UpdateCollectProgress(float fillAmount)
        {
            _uiCollectProgress.fillAmount = fillAmount;
        }


        #endregion
        

    } 
}


