using System;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using Unity.Rendering.HybridV2;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameInit.Game.BaseEvent;
using Random = UnityEngine.Random;

namespace GameHotfix.Game
{
    public partial class BattleWindow
    {
        //
        private int name;
        // 分数
        private long _scorePool;
        protected override void InitWindowParam()
        {
            Layer = WindowLayer.Bottom;
            WindowId = UIWindowId.UIMainWindow;
        }

        protected override void ParseComponentExt()
        {
            base.ParseComponentExt();
            // 注册按钮
            ListenButton(_startBtn, OnStartGame);
            // 退出
            ListenButton(_uiBtnClose,()=> // 进入主页阶段
                StageManager.Instance.EnterStage(GameStageConst.HomeStage));
            // 
            ListenButton(_addBtn, () =>
            {
                name++;
                BattleModel.Instance.AddRole("玩家"+name,"玩家"+name,666666,"",0,Random.Range(1,16800),Random.Range(1,16800),0);
            });
            //
            _uiBattleTips.gameObject.SetActive(false);
        }

        protected override void RegisterEvent()
        {
            // 刷新血条
            BattleUIModel.Instance.AddRefreshUnitBlood(_uIUnitStateView.UpdateUnitBlood);
            // 删除血条
            BattleUIModel.Instance.AddDeleteUnitBlood( _uIUnitStateView.DeleteUnitBlood);
            // 显示搜集条
            BattleUIModel.Instance.AddSetCollectProgress(_uIUnitStateView.SetUnitCollect);
            // 刷新搜集条
            BattleUIModel.Instance.AddUpdateCollectProgress(_uIUnitStateView.UpdateCollectProgress);
            // 刷新积分池
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventShowScorePool,ShowScorePool);
            // 刷新时间
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventRefreshGameTime,RefreshGameTime);
        }

        protected override void UnRegisterEvent()
        {
            // 刷新血条
            BattleUIModel.Instance.RemoveRefreshUnitBlood();
            // 删除血条
            BattleUIModel.Instance.RemoveDeleteUnitBlood();
            // 显示搜集条
            BattleUIModel.Instance.RemoveSetCollectProgress();
            // 刷新搜集条
            BattleUIModel.Instance.RemoveUpdateCollectProgress();
            // 刷新积分池
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventShowScorePool,ShowScorePool);
            // 刷新时间
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventRefreshGameTime,RefreshGameTime);
        }

        protected override void Refresh(params object[] arg)
        {
            base.Refresh(arg);
            _startBtn.gameObject.SetActive(true);
            _uiBtnClose.gameObject.SetActive(true);
            RemoveCoolDownTimer();
            // 隐藏排行榜
            _uIGameRankingList.Hide();
            // 显示战斗单位状态UI
            _uIUnitStateView.Show();
            // 隐藏倒计时
            // 显示倒计时
            _uiNightCountdown.SetActive(false);
            // 显示排行榜
            _uIGameRankingList.Show();
            // 显示boss信息
            _uIBossInfo.Show();
            // 积分池
            _scorePool = 0;
            _uIScorePool.text = "" + _scorePool;
            // 版本号
            _uiVersion.text = "" + Application.version;
            _uiAvoidWarTips.gameObject.SetActive(false);
        }

        #region cool down

        private uint _cdTimerKey;
        private int _coolDownTime;
        // 开始倒计时
        private void StartCoolDown()
        {
            // 显示倒计时文本
            _cDText.gameObject.SetActive(true);
            
            _coolDownTime = GameConstantHelper.StartGameTime;
            _cdTimerKey = TimerHeap.AddTimer(1000, 1000, () =>
            {
                if (_coolDownTime <= 0)
                {
                    // 移除倒计时定时器
                    RemoveCoolDownTimer();
                    // 隐藏倒计时文本
                    _cDText.gameObject.SetActive(false);
                    // 倒计时结束
                    EndCoolDown();
                    return;
                }

                _coolDownTime--;
                _cDText.text = _coolDownTime.ToString();
            });
            _cDText.text = _coolDownTime.ToString();
        }
        // 倒计时结束
        private void EndCoolDown()
        {
            // 开始游戏
            BattleManager.Instance.StartBattle();
            
            // 一局开始 开始一轮昼夜
            DiurnalVariationmManager.Instance.StartARound(ShowNightTIme,RefreshNightTime,CloseNightTime);
        }

        // 移除倒计时定时器
        private void RemoveCoolDownTimer()
        {
            if (_cdTimerKey == 0)
                return;
            TimerHeap.DelTimer(_cdTimerKey);
            _cdTimerKey = 0;
            _cDText.text = "";
            _cDText.gameObject.SetActive(false);
        }

        #endregion

        #region nightCountDown
        // 刷新晚上时间
        private void RefreshNightTime(float time)
        {
            // 显示倒计时
            _uiNightCountdown.SetActive(time > 0);
            // 显示tips
            NightText();
            if(time <= 0)
                return;
            
            var minute = (int)(time / 60);
            var second = time - minute * 60;

            _uiNightCountdownText.text = $"{minute:00}:{second:00}";
        }

        private void NightText()
        {
            if (BattleManager.Instance.BattleType == BattleType.Normal)
            {
                var tips = "黑夜倒计时";
                switch (BattleManager.Instance.Day)
                {
                
                    case 1:
                        tips = "第一夜倒计时";
                        break;
                    case 2:
                        tips = "第二夜倒计时";
                        break;
                    case 3:
                        tips = "第三夜倒计时";
                        break;
                }
                _uiNightTips.text = $"第{GameConstantHelper.ConvertToChinese(BattleManager.Instance.Day)}波倒计时";
            }
            else if (BattleManager.Instance.BattleType == BattleType.FieldBoss)
            {
                _uiNightTips.text = $"结束倒计时";
            }
        }

        // 隐藏倒计时
        private void CloseNightTime()
        {
            _uiNightCountdown.SetActive(false);
        }
        // 显示倒计时
        private void ShowNightTIme()
        {
            _uiNightCountdown.SetActive(true);
        }

        #endregion

        private void ShowTips(string content)
        {
            var tipsItem = UIItemPool.GetUIItem(_uiBattleTips);
            tipsItem.SetParent(RectTransform);
            tipsItem.localScale = Vector3.one;
            tipsItem.localPosition = Vector3.zero;
            tipsItem.gameObject.SetActive(true);
            
            tipsItem.Find("uiBattleTipsText").GetComponent<Text>().text = content;

            tipsItem.DOLocalMoveY(500, 1f).OnComplete(() =>
            {
                UIItemPool.CollectItem(tipsItem);
            });
        }

        private void RefreshGameTime(BaseEvent @event)
        {
            var time = (DateTime) @event.EventObj;
            _uiGameTime.text = "" + time.ToString("HH:mm:ss");
            // 公会id
            _uiGuildId.text = "公会id:" + BattleManager.Instance.GuildId;
            // 免战提示
            if(BattleManager.Instance.BattleType == BattleType.Normal)
                _uiAvoidWarTips.gameObject.SetActive(BattleManager.Instance.IsPeace || (time.Hour < 20 && time.Hour > 22));
            else 
                _uiAvoidWarTips.gameObject.SetActive(false);
        }

        //  刷新动画
        private Tweener _refreshScoreTweener;
        private void ShowScorePool(BaseEvent @event)
        {
            var score = (long) @event.EventObj;

            if (score == 0)
            {
                _scorePool = 0;
                _uIScorePool.text = "" + _scorePool;
                return;
            }

            _scorePool = score;
            _uIScorePool.text = "" + _scorePool;
            
            // // 杀死以前动画
            // if(_refreshScoreTweener != null)
            //     _refreshScoreTweener.Kill();
            //
            // _refreshScoreTweener = RefreshScore(score, _scorePool, (num) =>
            // {
            //     _scorePool = num;
            //     _uIScorePool.text = "" + _scorePool;
            // });
        }
        // 刷新进度
        private Tweener RefreshScore(long newValue, long oldValue,Action<long> callBack)
        {
            if (newValue <= 0)
                newValue = 0;

            // 动画时间
            var duration = (newValue - oldValue) * 0.001f;
            // 动画
            var tempValue = oldValue;
            var doTween = DOTween.To(
                () => tempValue,
                value => callBack(value),
                newValue,duration);

            return doTween;
        }

        //------ 按钮 -----
        // 开始游戏
        private void OnStartGame()
        {
            if(BattleManager.Instance.GetFighterCount(UnitType.Role) < 0)
                ShowTips("开始游戏最少4个人");
            else
            {
                _startBtn.gameObject.SetActive(false);
                // 开始倒计时
                StartCoolDown();
            }
            
            _uiBtnClose.gameObject.SetActive(false);
        }

        public override void Close()
        {
            // 排行榜
            _uIGameRankingList.Hide();
            // 状态UI
            _uIUnitStateView.Hide();
            // boss信息
            _uIBossInfo.Hide();
            // 杀死以前动画
            if(_refreshScoreTweener != null)
                _refreshScoreTweener.Kill();
            base.Close();
        }


        public override void Dispose()
        {
            // 删除倒计时定时器
            RemoveCoolDownTimer();
            // 排行榜
            _uIGameRankingList.Dispose();
            // 状态UI
            _uIUnitStateView.Dispose();
            // boss信息
            _uIBossInfo.Dispose();
            base.Dispose();
        }
    }
}