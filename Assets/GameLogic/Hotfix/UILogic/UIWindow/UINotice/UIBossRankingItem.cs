using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class UIBossRankingItem
    {
        // 数据
        private ServerData.Rank _rankData;
        // 排名
        private int _rankOrder;
        // 开始位置
        private float _startX;
        protected override void Refresh(params object[] arg)
        {
            // 数据
            _rankData = (ServerData.Rank) arg[0];
            // 排名
            _rankOrder = (int) arg[1];
            // 刷新
            UpdateView();
        }

        private async void UpdateView()
        {
            // 名字
            _uiName.text = _rankData.userName;
            // 排名
            _uiOrder.text = $"第{GameConstantHelper.ConvertToChinese(_rankOrder)}名" ;
            // 分数
            _uiScore.text = $"{_rankData.score}";
            // 头像
            // 加载头像
            var speite  = await HeadImageDownloader.LoadHeadSprite(_rankData.headUrl);
            if(_uiHeaderIcon == null)
                return;
            _uiHeaderIcon.sprite = speite;
            // 头像框
            _uiOrderFrame.sprite = await SpriteAtlasMgr.Instance.LoadSprite("touxiang0"+(_rankOrder > 4 ? 4 : _rankOrder));
        }
        
        // 开始移动
        public void StartMove(float startX,float endX,float duration,float delay)
        {
            // 删除动画
            RectTransform.DOKill();
            // 开始位置
            _startX = startX;
            RectTransform.localPosition = new Vector3(startX, 0, 0);

            RectTransform.DOLocalMoveX(endX, duration)
                .SetDelay(delay)
                .SetEase(Ease.Linear)
                .SetLoops(-1,LoopType.Restart);

        }
        // 停止移动
        public void StopMove()
        {
            // 删除动画
            RectTransform.DOKill();
            // 开始位置
            RectTransform.localPosition = new Vector3(_startX, 0, 0);
        }

        public override void Dispose()
        {
            // 停止移动
            StopMove();
            // 默认头像
            _uiHeaderIcon.sprite =  HeadImageDownloader.DefaultHead();
            base.Dispose();
        }
    }
}


