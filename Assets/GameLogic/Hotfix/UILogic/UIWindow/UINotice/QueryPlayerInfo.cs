﻿using System.Collections.Generic;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class QueryPlayerInfo
    {
        private List<UIPlayerInfoItem> _uiPlayerInfoItems;
        // 开始位置
        private Vector3 _startPos;
        // 排队列表
        private List<RoleData> _datas;
        // 显示中
        private bool _isDisplay;
        protected override void ParseComponentExt()
        {
            _uIPlayerInfoItem.SetActive(false);
            _startPos = _uIPlayerInfoItem.transform.localPosition;
            _uiPlayerInfoItems = new List<UIPlayerInfoItem>();
            _datas = new List<RoleData>();
            _isDisplay = false;
        }

        protected override void RegisterEvent()
        {
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowQueryPlayerInfo,ShowPlayerInfo);
        }

        protected override void UnRegisterEvent()
        {
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowQueryPlayerInfo,ShowPlayerInfo);
        }

        protected override void Refresh(params object[] arg)
        {
            
        }
        // 显示玩家信息
        private void ShowPlayerInfo(BaseEvent baseEvent)
        {
            // 玩家数据
             var data = (RoleData)baseEvent.EventObj;
            
             // 排队
            if (_isDisplay)
            {
                _datas.Add(data);
                return;
            }
            
            // 显示玩家信息
            ShowPlayerInfo(data);
        }
        // 显示玩家信息
        private void ShowPlayerInfo(RoleData data)
        {
            // 标记
            _isDisplay = true;
            
            UIPlayerInfoItem item;
            if (_uiPlayerInfoItems.Count > 0)
            {
                item = _uiPlayerInfoItems[0];
                _uiPlayerInfoItems.RemoveAt(0);
            }
            else
            {
                item = new UIPlayerInfoItem();
            }

            var go = UIItemPool.GetUIItem(_uIPlayerInfoItem.GetComponent<Transform>());
            go.SetParent(RectTransform);
            go.localPosition = _startPos;
            go.localScale = Vector3.one;
            
            go.gameObject.SetActive(true);
            item.SetDisplayObject(go.gameObject);
            item.Show(data,_startPos);
            // 显示动画
            item.ShowAnimation(() =>
            {
                // 标记
                _isDisplay = false;
                
                // 动画结束回收
                CollectObj(item);
                
                // 下一个
                if (_datas.Count > 0)
                {
                    var nextData = _datas[0];
                    _datas.RemoveAt(0);
                    ShowPlayerInfo(nextData);
                }
            });
        }

        // 回收对象
        private void CollectObj(UIPlayerInfoItem item)
        {
            UIItemPool.CollectItem(item.RectTransform);
            item.Dispose();
            _uiPlayerInfoItems.Add(item);
        }

        public override void Dispose()
        {
            _uiPlayerInfoItems.Clear();
            _datas.Clear();
            _isDisplay = false;
        }
    }
}