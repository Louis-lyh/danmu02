using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class UITopPlayerItem
    {
        private TopPlayerInfo _rankData;

        protected override void Refresh(params object[] arg)
        {
            _rankData = (TopPlayerInfo) arg[0];
            // 更新
            UpDateView();
            // 显示动画
            ShowAnimation();
        }

        private async void UpDateView()
        {
            // 名字
            _uiNameText.text = _rankData.UserName;
            // 排名
            _uiTopText.text = ""+ _rankData.Order;
            // 头像
            var sprite = await HeadImageDownloader.LoadHeadSprite(_rankData.HeadHurl);
            if(_uiHeadIcon != null)
                _uiHeadIcon.sprite = sprite;
        }

        private void ShowAnimation()
        {
            var parent = RectTransform.parent;
            var parentWidth = parent.GetComponent<RectTransform>().rect.width;
            var pos = RectTransform.localPosition;
            pos.x = parentWidth / 2;
            RectTransform.localPosition = pos;
            
            // 名字
            _uiName.localScale = Vector3.zero;
            // 头像
            _uiHead.localScale = Vector3.one * 4f;
            _uiHead.gameObject.SetActive(false);
            // 排名
            _uiTop.localScale = Vector3.zero;
            
            // 动画
            Sequence sequence = DOTween.Sequence();
            sequence.Append(RectTransform.DOLocalMoveX(0f, 0.1f)).SetEase(Ease.Linear);
            sequence.Append(_uiHead.DOScale(Vector3.one,0.4f).OnStart(()=>_uiHead.gameObject.SetActive(true)).SetEase(Ease.OutExpo));
            sequence.Insert(0.25f, _uiName.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack));
            sequence.Insert(0.3f, _uiTop.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack));

            sequence.InsertCallback(2.6f,HideItem);
        }

        private void HideItem()
        {
            // 默认头像
            _uiHeadIcon.sprite =  HeadImageDownloader.DefaultHead();
            // 隐藏
            Hide();
        }

    }
}


