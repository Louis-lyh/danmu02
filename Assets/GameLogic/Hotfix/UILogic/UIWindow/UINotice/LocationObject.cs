using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameHotfix.Framework.BaseEvent;

namespace GameHotfix.Game
{
    public partial class LocationObject
    {
        // 等待列表
        private List<GameObject> _awaitList;
        // 运行摄像头数量 
        private int _cameraCountWorking;
        // 队列
        private Queue<GameObject> _unitQueue;
        protected override void ParseComponentExt()
        {
            var leftRawImage = _uiPositioningLeftImage.GetComponent<RawImage>();
            var rightRawImage = _uiPositioningRightImage.GetComponent<RawImage>();
            // 初始化定位
            PositioningManager.Instance.Init(leftRawImage,rightRawImage);
            // 重置
            _cameraCountWorking = 0;
            // 初始化
            _awaitList = new List<GameObject>();
            // 队列
            _unitQueue = new Queue<GameObject>();
        }
        // 注册消息
        protected override void RegisterEvent()
        {
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventLocationUnit,AddLocationObject);
        }
        // 注销消息
        protected override void UnRegisterEvent()
        {
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventLocationUnit,AddLocationObject);
        }
        protected override void Refresh(params object[] arg)
        {
            
        }
        
        // 定位
        private void AddLocationObject(GameInit.Game.BaseEvent @event)
        {
            var unit = (GameObject)@event.EventObj;
            if (_cameraCountWorking < 1)
            {
                // 存在退出
                if(_unitQueue.Contains(unit))
                    return;
                
                // 开始定位
                PositioningManager.Instance.Positioning(unit,StartPosition,EndPosition);
                // 加入队列
                _unitQueue.Enqueue(unit);
            }
            else
            {
                // 加入等待列表
                if(!_awaitList.Contains(unit) && !_unitQueue.Contains(unit))
                    _awaitList.Add(unit);
            }
        }
        
        // 开始回调
        private void StartPosition(int index)
        {
            if (index == 0)
                MoveImage(_uiPositioningLeftImage, true, true);
            else if(index == 1)
                MoveImage(_uiPositioningRightImage, false, true);
            
            // 工作数加一
            _cameraCountWorking++;
        }
        // 结束回调
        private void EndPosition(int index,Action moveAboutEnd)
        {
            if (index == 0)
                MoveImage(_uiPositioningLeftImage, true, false, () =>
                {
                    moveAboutEnd?.Invoke();
                    // 工作数减一
                    _cameraCountWorking--;
                    LocationUnit();
                    // 删除队列
                    _unitQueue.Dequeue();
                    
                });
            else if(index == 1)
                MoveImage(_uiPositioningRightImage, false, false,() =>
                {
                    moveAboutEnd?.Invoke();
                    // 工作数减一
                    _cameraCountWorking--;
                    LocationUnit();
                    // 删除队列
                    _unitQueue.Dequeue();
                });
        }
        // 定位
        private void LocationUnit()
        {
            if(_awaitList.Count == 0|| _cameraCountWorking >= 1)
                return;
            
            var unit = _awaitList[0];
            // 存在退出
            if(_unitQueue.Contains(unit))
                return;
            
            // 开始定位
            PositioningManager.Instance.Positioning(unit,StartPosition,EndPosition);
            _awaitList.RemoveAt(0);
            
            // 加入队列
            _unitQueue.Enqueue(unit);
        }

        // 动画
        private void MoveImage(Transform imageTra,bool isLeft,bool isStart,Action callBack = null)
        {
            var xPos = 0;
            
            if (isStart)
                xPos = 300;
            else
                xPos = 700;

            if (isLeft)
                xPos = -xPos;

            imageTra.DOLocalMoveX(xPos, 0.5f)
                .OnComplete(()=>callBack?.Invoke());
        }

        public override void Dispose()
        {
            base.Dispose();
            _awaitList.Clear();
            _unitQueue.Clear();
        }
    }
}

