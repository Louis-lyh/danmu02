using System.Collections.Generic;
using GameInit.Framework;
using GameInit.Game;

namespace GameHotfix.Game
{
    public partial class UITopPlayerTips
    {
        // 玩家数据
        private List<TopPlayerInfo> _roleDatas;

        // item
        private UITopPlayerItem _playerItem;

        // 定时器
        private uint _showTimer;

        // 间隔显示时间
        private int _showInterval = 2600;
        private int _showIntervalCount = 0;

        protected override void ParseComponentExt()
        {
            // 初始化
            _roleDatas = new List<TopPlayerInfo>();

            _playerItem = new UITopPlayerItem();
            _playerItem.SetDisplayObject(_uITopPlayerItem);
            _playerItem.Hide();
        }

        protected override void RegisterEvent()
        {
            // 显示世界排行玩家
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowTopPlayer,ShowTopPlayer);
        }

        protected override void UnRegisterEvent()
        {
            // 显示世界排行玩家
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowTopPlayer,ShowTopPlayer);
            
            // 清理
            Clear();
        }

        protected override void Refresh(params object[] arg)
        {
            // 开启定时
            ShowTimer();
            // 初始化
            _uiMask.SetActive(false);
            _showIntervalCount = _showInterval;
        }
        
        // 显示世界排行玩家
        private void ShowTopPlayer(BaseEvent e)
        {
            var roleData = (TopPlayerInfo) e.EventObj;
            _roleDatas.Add(roleData);
        }
        private void ShowTopPlayer(TopPlayerInfo roleData)
        {
            _playerItem.Show(roleData);
        }

        // 定时
        private void ShowTimer()
        {
            // 删除定时器
            TimerHeap.DelTimer(_showTimer);
            
            // 开启定时
            var interval = 100;
            _showTimer = TimerHeap.AddTimer(0, interval, () =>
            {
                _showIntervalCount += interval;
                if (_showIntervalCount >= _showInterval)
                {
                    // 显示mask
                    _uiMask.SetActive(_roleDatas.Count > 0);
                    if (_roleDatas.Count > 0)
                    {
                        // 数据
                        var roleData = _roleDatas[0];
                        _roleDatas.RemoveAt(0);
                        // 显示player
                        ShowTopPlayer(roleData);
                        // 重置
                        _showIntervalCount = 0;
                    }
                }
            });
        }

        // 清除 
        private void Clear()
        {
            // 删除定时器
            TimerHeap.DelTimer(_showTimer);
            // 清除数据
            _roleDatas.Clear();
            // 隐藏
            _playerItem.Hide();
            _uiMask.SetActive(false);
        }

        public override void Dispose()
        {
            base.Dispose();
            Clear();
        }
    }
}

