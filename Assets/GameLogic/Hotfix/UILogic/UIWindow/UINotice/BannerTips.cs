using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameInit.Framework;
using GameInit.Game;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public partial class BannerTips
    {
        // tipsItem 列表
        private List<Transform> _dangerousTipsItem;
        private List<Transform> _collectTipsItem;
        // 等待列表
        private List<Action> _waitDanTips;
        private List<Action> _waitColTips;
        // 定时器
        private uint _timer;
        // 击杀boss提示列表
        private List<UIKillBossTips> _skillBossTipsList;
        protected override void ParseComponentExt()
        {
            // 新建
            _skillBossTipsList = new List<UIKillBossTips>();
            
            // 隐藏item
            _uiCollectTips.gameObject.SetActive(false);
            _uiDangerousTips.gameObject.SetActive(false);
            // 初始化
            _dangerousTipsItem = new List<Transform>();
            _collectTipsItem = new List<Transform>();
            // 初始化等待列表
            _waitDanTips = new List<Action>();
            _waitColTips = new List<Action>();
        }

        protected override void RegisterEvent()
        {
            // 显示击杀提示
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowKillTips,ShowKillBossTips);
            // 显示危险提示
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowDangerousTips,ShowDangerousTips);
            // 显示搜集提示
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowCollectTips,ShowCollectedTips);
        }

        protected override void UnRegisterEvent()
        {
            // 显示击杀提示
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowKillTips,ShowKillBossTips);
            // 显示危险提示
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowDangerousTips,ShowDangerousTips);
            // 显示搜集提示
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowCollectTips,ShowCollectedTips);
            // 清理
            Clear();
        }

        protected override void Refresh(params object[] arg)
        {
            // 更新提示
            UpdateShowTips();
        }

        private void ShowKillBossTips(BaseEvent @event)
        {
            SkillBossTips skillBossTips = (SkillBossTips) @event.EventObj;

            UIKillBossTips killBossTips = null;

            for (int i = 0; i < _skillBossTipsList.Count; i++)
            {
                var tipsView = _skillBossTipsList[i];
                if (!tipsView.DisplayObject.activeSelf)
                {
                    killBossTips = tipsView;
                    break;
                }
            }
            // 新建
            if (killBossTips == null)
            {
                killBossTips = new UIKillBossTips();
                var obj = GameObject.Instantiate(_uIKillBossTips,_uIKillBossTips.transform.parent);
                killBossTips.SetDisplayObject(obj);
                _skillBossTipsList.Add(killBossTips);
            }

            // 显示
            killBossTips.Show(skillBossTips.RoleData,skillBossTips.Reward);
            killBossTips.RectTransform.SetAsLastSibling();
        }

        private void UpdateShowTips()
        {
            _timer =  TimerHeap.AddTimer(0, 500, () =>
            {
                // 危险提示
                if (_dangerousTipsItem.Count == 0 && _waitDanTips.Count > 0)
                {
                    _waitDanTips[0]?.Invoke();
                    _waitDanTips.RemoveAt(0);
                }
                
                // 搜集提示
                if (_collectTipsItem.Count == 0 && _waitColTips.Count > 0)
                {
                    _waitColTips[0]?.Invoke();
                    _waitColTips.RemoveAt(0);
                }
            });
        }

        // 显示危险提示
        private void ShowDangerousTips(GameInit.Game.BaseEvent e)
        {
            if(_dangerousTipsItem.Count == 0)
                // 创建item
                CreateItem(_uiDangerousTips,(string)e.EventObj,_dangerousTipsItem);
            else 
                // 加入等待列表
                _waitDanTips.Add(()=> CreateItem(_uiDangerousTips,(string)e.EventObj,_dangerousTipsItem));
        }
        // 显示搜集提示
        private void ShowCollectedTips(GameInit.Game.BaseEvent e)
        {
            if(_collectTipsItem.Count == 0)
                // 创建item
                CreateItem(_uiCollectTips,(string)e.EventObj,_collectTipsItem);
            else 
                // 加入等待列表
                _waitColTips.Add(()=> CreateItem(_uiCollectTips,(string)e.EventObj,_collectTipsItem));
        }
        
        // 创建tipsItem
        private void CreateItem(GameObject itemPrefab,string content,List<Transform> itemList)
        {
            // 实例化item
            var item = GameObject.Instantiate(itemPrefab,RectTransform);
            item.SetActive(true);
            var itemTra = item.transform;
            // 设置文本
            itemTra.Find("uiContentText").GetComponent<Text>().text = content;
            // 加入列表
            itemList.Add(itemTra);
            // 设置位置
            SetStartPos(item.GetComponent<RectTransform>());
           // 移动动画
           ItemMoveAni(itemTra,itemList);
        }
        // 移动动画
        private void ItemMoveAni(Transform item, List<Transform> itemList)
        {
            // icon闪烁动画
            var iconAni = item.Find("uiIcon").GetComponent<Image>()
                .DOFade(0.5f, 0.8f)
                .SetLoops(-1, LoopType.Yoyo);
            // 动画结束回调
            Action endAni = () =>
            {
                // 关闭动画
                iconAni.Kill();
                // 移除列表
                itemList.Remove(item);
                // 删除对象
                GameObject.Destroy(item.gameObject);
            };
            // 移动动画
            var startX = item.localPosition.x;
            item.DOLocalMoveX(0, 0.5f).SetEase(Ease.OutBack);
            item.DOLocalMoveX(-startX, 0.5f)
                .SetDelay(3f)
                .SetEase(Ease.InBack)
                .OnKill(()=>endAni.Invoke())
                .OnComplete(()=>endAni.Invoke());
        }


        // 设置开始位置
        private void SetStartPos(RectTransform item)
        {
            // 屏幕一半宽
            var screenWidthHalf = RectTransform.rect.width / 2;
            // item一半宽
            var itemWidthHalf = item.rect.width / 2;
            // 原始位置
            var pos = item.localPosition;
            // 新x轴值
            var newPosX = itemWidthHalf + screenWidthHalf;
            pos.x = newPosX;
            // 设置位置
            item.localPosition = pos;
        }

        public override void Dispose()
        {
            // 清理
            Clear();
            base.Dispose();
        }
        private void Clear()
        {
            // 删除定时器
            TimerHeap.DelTimer(_timer);
            //删除item
            for (int i = 0; i < _collectTipsItem.Count; i++)
            {
                _collectTipsItem[i].DOKill();
            }
            _collectTipsItem.Clear();
            //删除item
            for (int i = 0; i < _dangerousTipsItem.Count; i++)
            {
                _dangerousTipsItem[i].DOKill();
            }
            _dangerousTipsItem.Clear();
            // 清除等待列表
            _waitDanTips.Clear();
            _waitColTips.Clear();
            // 隐藏boss提示
            for (int i = 0; i < _skillBossTipsList.Count; i++)
            {
                GameObject.Destroy(_skillBossTipsList[i].DisplayObject);
            }
            _skillBossTipsList.Clear();
        }
    } 
}


