using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class UIKillBossTips
    {
        // 玩家数据
        private RoleData _roleData;

        protected override void ParseComponentExt()
        {

        }

        protected override void RegisterEvent()
        {
           
        }

        protected override void UnRegisterEvent()
        {
           
        }


        protected override void Refresh(params object[] arg)
        {
            _roleData = (RoleData) arg[0];
            // 显示奖励提示
            ShowRewardTips((Dictionary<string, int>) arg[1]);
            // 更新UI
            UpdateView();
            // 显示动画
            ShowAnimation();
        }

        // 更新UI
        private async void UpdateView()
        {
            _uiName.text = _roleData.Name;
            var sprite = await HeadImageDownloader.LoadHeadSprite(_roleData.HeadUrl);
            if (_uiheadIcon == null)
                return;
            _uiheadIcon.sprite = sprite;
        }
        // 奖励提示
        private void ShowRewardTips(Dictionary<string,int> rewardDic)
        {
            _uiTipsText01.text = "角色修为：" + rewardDic["Exp"];
            _uiTipsText01.transform.parent.gameObject.SetActive(rewardDic["Exp"] > 0);
            
            if(rewardDic["Pet"] > 0)
                _uiTipsText02.text = "宠物修为：" + rewardDic["Pet"];
            else
                _uiTipsText02.text = "光环修为：" + rewardDic["Halo"];
            
            _uiTipsText01.transform.parent.gameObject.SetActive(rewardDic["Pet"] > 0 || rewardDic["Halo"] > 0);
        }

        // 显示动画
        private void ShowAnimation()
        {
            // 移动动画
            var startPos = RectTransform.localPosition;
            RectTransform.DOLocalMoveX(0, 0.5f).SetEase(Ease.OutBack);
            RectTransform.DOLocalMoveX(-startPos.x, 0.5f)
                .SetDelay(3f)
                .SetEase(Ease.InBack)
                .OnComplete(() =>
                {
                    RectTransform.localPosition = startPos;
                    Hide();
                });
        }
    }
}


