using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameHotfix.Framework.BaseEvent;

namespace GameHotfix.Game
{
    public partial class EdgeBubble
    {
        // item列表
        private List<Transform> _items;
        protected override void ParseComponentExt()
        {
            // 初始化
            _items = new List<Transform>();
            // 隐藏
            _uiEdgeBubbleItem_Left_01.SetActive(false);
            _uiEdgeBubbleItem_Left_02.SetActive(false);
            _uiEdgeBubbleItem_Right_01.SetActive(false);
        }

        protected override void RegisterEvent()
        {
            // 显示屏幕边界气泡
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventSendEdgeBubble,ShowEdgeBubble);
        }

        protected override void UnRegisterEvent()
        {
            // 显示屏幕边界气泡
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventSendEdgeBubble,ShowEdgeBubble);
        }

        protected override void Refresh(params object[] arg)
        {
            
        }
        // 显示气泡
        private void ShowEdgeBubble(GameInit.Game.BaseEvent e)
        {
            // 信息
            var info = (EdgeBubbleInfo) e.EventObj;

            switch (info.Type)
            {
                case EdgeBubbleType.KillScore:
                    ShowKillScore(info);    // 显示击杀分数
                    break;
                case EdgeBubbleType.LikeCount:
                    ShowLike(info);    // 显示点赞数量
                    break;
                case EdgeBubbleType.PlayerState01:
                case EdgeBubbleType.PlayerState02:
                    ShowPlayerState(info);    // 显示玩家状态
                    break;
            }
            
        }

        // 显示击杀分数
        private void ShowKillScore(EdgeBubbleInfo info)
        {
            // 创建气泡
            var item = CreateItem(info, _uiEdgeBubbleItem_Left_01);
            // 横移动画
            MoveItem(item,-1);
            // 数字动画
            NumberAni(item.Find("uiEventContent").GetComponent<Text>());
        }
        
        // 显示点赞
        private void ShowLike(EdgeBubbleInfo info)
        {
            // 创建气泡
            var item = CreateItem(info, _uiEdgeBubbleItem_Left_02);
            // 横移动画
            MoveItem(item,-1);
            // 数字动画
            NumberAni(item.Find("uiEventContent").GetComponent<Text>());
        }
        
        // 显示玩家状态
        private void ShowPlayerState(EdgeBubbleInfo info)
        {
            // 创建气泡
            var item = CreateItem(info, _uiEdgeBubbleItem_Right_01);
            // 设置位置
            switch (info.Type)
            {
                case EdgeBubbleType.PlayerState02:
                    item.localPosition -= Vector3.up * 141;
                    break;
            }
            // 横移动画
            MoveItem(item,1);
        }
        
        // 横移动画
        private void MoveItem(Transform item,int dir)
        {
            // item一半宽
            var itemWidth = item.GetComponent<RectTransform>().rect.width;
            // 范围一半宽 
            var rangeWidthHalf = _range.GetComponent<RectTransform>().rect.width / 2;
            // 结束位置
            var endPos = item.localPosition;
            // 开始位置
            var startPosX = dir * (itemWidth + rangeWidthHalf);
            item.localPosition = new Vector3(startPosX,endPos.y,endPos.z);
            // 动画
            var sequence = DOTween.Sequence();
            sequence.Append(item.DOLocalMoveX(endPos.x,1f).SetEase(Ease.OutBack));
            sequence.AppendInterval(1.5f);
            sequence.Append(item.DOLocalMoveX(startPosX,0.3f));
            // 回收对象
            sequence.AppendCallback(() =>
            {
                // 移除列
                _items.Remove(item);
                // 回收
                RecoverItem(item);
            });
        }
        // 数字动画
        private void NumberAni(Text text)
        {
            int number = 0;
            try
            {
                number = int.Parse(text.text);
            }
            catch (Exception e)
            {
                Console.WriteLine($"{text.name} 不是存数字=> {text.text}");
                return;
            }
            // 动画
            DOTween.To(() => 0, (value) => { text.text = $"{value}"; },
                number,1f);
        }

        // 创建气泡（气泡itemPrefab的结构需要保持一样）
        private Transform CreateItem(EdgeBubbleInfo info,GameObject itemPrefab)
        {
            // 实例化
            var item = UIItemPool.GetUIItem(itemPrefab.transform);
            item.SetParent(_range);
            item.gameObject.SetActive(true);
            item.localScale = Vector3.one;
            item.localPosition = itemPrefab.transform.localPosition;
            // 加入列表
            _items.Add(item);
            // 设置信息
            var headerIcon = item.Find("uiHeader/uiHeaderMask/uiHeaderIcon").GetComponent<Image>();
            // 设置头像
            SetHeaderIcon(headerIcon,info.HeadUrl);
            item.Find("uiName/uiNameText").GetComponent<Text>().text = GameNoticeModel.FormatName(info.Name);
            item.Find("uiEventName").GetComponent<Text>().text = info.EventName;
            item.Find("uiEventContent").GetComponent<Text>().text = info.EventContent;
            
            return item;
        }
        // 设置头像
        private async void SetHeaderIcon(Image header,string headUrl)
        {
            // 加载头像
            var speite  = await HeadImageDownloader.LoadHeadSprite(headUrl);
            if(header == null)
                return;
            header.sprite = speite;
        }
        
        // 回收
        private void RecoverItem(Transform item)
        {
            // 删除动画
            item.DOKill();
            UIItemPool.CollectItem(item);
        }
        

        public override void Dispose()
        {
            Clear();
            base.Dispose();
        }

        private void Clear()
        {
            for (int i = _items.Count - 1; i >= _items.Count; i--)
            {
                var item = _items[i];
                // 移除列表
                _items.Remove(item);
                // 回收对象
                RecoverItem(item);
            }
        }
    }
}


