using System;
using System.Collections;
using System.Collections.Generic;
using GameHotfix.Game.Utils;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class UIBossRankingView
    {
        // 排行榜数据
        private List<ServerData.Rank> _rankDataList;
        // 缩放 
        private float _itemScale = 1;
        // 宽
        private float _itemWidth;
        // 间隔
        private float _itemSpace = 100;
        // 时间
        private float _itemMoveSpeed = 100f;
        
        // item
        private List<UIBossRankingItem> _rankingItems;
        
        protected override void ParseComponentExt()
        {
            // 隐藏
            _uIBossRankingItem.transform.localScale = Vector3.zero;
            // item宽
            _itemWidth = _uIBossRankingItem.transform.GetComponent<RectTransform>().rect.width * _itemScale;
            // item列表
            _rankingItems = new List<UIBossRankingItem>();
        }

        protected override void RegisterEvent()
        {
            // 显示狼王排行榜
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventShowBossRanking,ShowBossRanking);
        }

        protected override void UnRegisterEvent()
        {
            // 显示狼王排行榜
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventShowBossRanking,ShowBossRanking);
            // 清理
            ClearItem();
        }


        protected override void Refresh(params object[] arg)
        {
            // ShowBossRanking(null);
        }
        
        // 显示狼王排行榜 
        private void ShowBossRanking(BaseEvent e)
        {
            // 排行榜
            _rankDataList = RankInfo.Instance.WerewolfRanks;
            
            if (_rankDataList != null
                && _rankDataList.Count > 0)
            {
                // 清理
                ClearItem();
                // 获取排行榜数据
                ShowBossRanking_Unit();
            }
        }
        
        // 显示排行榜
        private void ShowBossRanking_Unit()
        {
            // 狼人数量
            var itemCount = Mathf.Min(20, _rankDataList.Count);
            
            // 屏幕宽
            var screenWidth = _root.rect.width;
            // item宽
            var itemAllWidth = itemCount * (_itemWidth + _itemSpace);
            // 开始位置
            var startPosX = (screenWidth + _itemWidth) / 2f;
            // 结束位置
            var endPosX = -(screenWidth + _itemWidth) / 2f;
            // 数量太多 结束位置延长
            if (itemAllWidth > screenWidth)
                endPosX = endPosX - (itemAllWidth - screenWidth);
            
            // 时间
            float duration = (-endPosX + startPosX) / _itemMoveSpeed;

            var delay = (_itemWidth + _itemSpace) / _itemMoveSpeed;
            
            for (int i = 0; i < itemCount; i++)
            {
                UIBossRankingItem item;
                if (i < _rankingItems.Count)
                    item = _rankingItems[i];
                else
                {
                    item = CreateRankingItem();
                    _rankingItems.Add(item);
                }

                // 刷新
                item.Show(_rankDataList[i],i + 1);
                // 移动
                item.StartMove(startPosX,endPosX,duration,delay * i);
            }
        }
        
        // 创建排行榜数据
        private UIBossRankingItem CreateRankingItem()
        {
            var item = UIItemPool.GetUIItem(_uIBossRankingItem.transform);
            item.transform.SetParent(_root);
            item.localScale = _itemScale * Vector3.one;
            
            UIBossRankingItem itemView = new UIBossRankingItem();
            itemView.SetDisplayObject(item.gameObject);

            return itemView;
        }

        private void ClearItem()
        {
            for (int i = 0; i < _rankingItems.Count; i++)
            {
                // 回收对象
                UIItemPool.CollectItem(_rankingItems[i].RectTransform);
                _rankingItems[i].Dispose();
            }
            _rankingItems.Clear();
        }


        public override void Dispose()
        {
            base.Dispose();
            ClearItem();
        }
    }
}


