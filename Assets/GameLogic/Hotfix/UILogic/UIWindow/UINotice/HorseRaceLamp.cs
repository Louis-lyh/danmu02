using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameHotfix.Framework.BaseEvent;

namespace GameHotfix.Game
{
    public partial class HorseRaceLamp
    {
        // 发送的行
        private int _sendRow;
        // 移动时间（标准屏幕）
        private float _moveTime = 6f;
        // 信息列表
        private List<HorseRaceLampInfo> _infoList;
        // 跑马灯item列表
        private List<List<Transform>> _itemList;
        // 定时器
        private uint _updateSendTimer;
        protected override void ParseComponentExt()
        {
            // 隐藏iten
            _uiNoticeItem.SetActive(false);
            // 初始化
            _sendRow = 0;
            _infoList = new List<HorseRaceLampInfo>();
            // 初始化item列表
            _itemList = new List<List<Transform>>();
            _itemList.Add(new List<Transform>()); // 第一行
            _itemList.Add(new List<Transform>()); // 第二行
            _itemList.Add(new List<Transform>()); // 第三行
            
            // 打开定时器
            UpdateHorseRaceLamp();
        }
        protected override void RegisterEvent()
        {
            // 发送跑马灯消息
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventSendHorseRaceLamp,SendHorseRaceLamp);
        }

        protected override void UnRegisterEvent()
        {
            // 发送跑马灯消息
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventSendHorseRaceLamp,SendHorseRaceLamp);
            // 清除
            Clear();
        }
        protected override void Refresh(params object[] arg)
        {
            
        }
        
        // 发送跑马灯消息
        private void SendHorseRaceLamp(GameInit.Game.BaseEvent e)
        {
            // 跑马灯消息
            var info = (HorseRaceLampInfo) e.EventObj;
            // 判断是否能直接发送
            if (JudgeSend())
            {
                // item
                CreateItem(info);
            }
            else
            {
                // 不能直接发送加如等待队列
                _infoList.Add(info);
            }
        }
        
        // 判断是否可以发射
        private bool JudgeSend()
        {
            // 发射行的item列表
            var items = _itemList[_sendRow];
            // item父节点
            var rangeWidthHalf = _range.rect.width / 2;
            // 存在item
            if (items.Count > 0)
            {
                var item = items[items.Count - 1];
                var itemX = item.localPosition.x;
                var itemWidthHalf = item.transform.GetComponent<RectTransform>().rect.width / 2;
                
                // item的x 坐标加上 item的0.5倍宽小于 0.5倍屏幕宽 即 item完全出了屏幕右边
                if (itemX + itemWidthHalf < rangeWidthHalf)
                    return true;
                else
                    return false;
            }
            
            return true;
        }
        
        // 创建跑马灯item
        private void CreateItem(HorseRaceLampInfo info)
        {
            // 获取item
            var item = UIItemPool.GetUIItem(_uiNoticeItem.transform);
            item.gameObject.SetActive(true);
            item.SetParent(_uiNoticeItem.transform.parent);
            item.localScale = Vector3.one;
            // 当前行
            var curRow = _sendRow;
            // 加入列表
            _itemList[_sendRow].Add(item);
            // 下一行
            _sendRow++;
            _sendRow = _sendRow % 3;
            // 设置信息
            item.Find("uiItemName").GetComponent<Text>().text = GameNoticeModel.FormatName(info.Name);
            var uiDescribe = item.Find("uiDescribe");
            uiDescribe.GetComponent<Text>().text = info.Describe;
            // 强制重新布局
            LayoutRebuilder.ForceRebuildLayoutImmediate(uiDescribe.GetComponent<RectTransform>());
            LayoutRebuilder.ForceRebuildLayoutImmediate(item.GetComponent<RectTransform>());

            // 设置位置
            item.transform.localPosition = GetItemStartPos(item, _sendRow);
            // 移动动画
            var speed = 1080f / _moveTime;
            // 移动时间
            var duration = _range.rect.width / speed;
            // 目标位置
            var targetX = -item.transform.localPosition.x;
            item.DOLocalMoveX(targetX, duration).SetEase(Ease.Linear)
                .OnComplete(() =>
                {
                    // 移除列表
                    _itemList[curRow].Remove(item);
                    // 回收item
                    UIItemPool.CollectItem(item);
                });
        }
        // item开始位置
        private Vector3 GetItemStartPos(Transform item,int sendRow)
        {
            // 一半range高
            var rangeHeightHalf = _range.rect.height / 2;
            // 一半range高
            var rangeWidthHalf = _range.rect.width / 2;
            // 一半item宽
            var itemWidth = item.GetComponent<RectTransform>().rect.width / 2;
            // 一半item高
            var itemHeight = item.GetComponent<RectTransform>().rect.height / 2;
            
            // item Y 轴
            float y = 0;
            // 第一行
            if (sendRow == 0)
                y = rangeHeightHalf - itemHeight;
            // 第二行
            else if(sendRow == 1)
                y = 0;
            // 第三行
            else if (sendRow == 2)
                y = itemHeight - rangeHeightHalf;

            var x = rangeWidthHalf + itemWidth;
            
            return new Vector3(x,y,0);
        }
        
        // 定时发送跑马灯消息
        private void UpdateHorseRaceLamp()
        {
            _updateSendTimer =  TimerHeap.AddTimer(0, 500, () =>
            {
                // 没有信息退出
                if(_infoList == null || _infoList.Count == 0)
                    return;
                // 不能发送退出
                if(!JudgeSend())
                    return;
                
                // 发送
                CreateItem(_infoList[0]);
                // 移除
                _infoList.RemoveAt(0);
            });
        }

        public override void Dispose()
        {
            // 清空
            Clear();
            base.Dispose();
        }

        // 清空对象
        private void Clear()
        {
            // 删除定时器
            TimerHeap.DelTimer(_updateSendTimer);
            // 删除跑马灯
            for (int i = _itemList.Count -1; i >= 0 ; i--)
            {
                for (int j = _itemList[i].Count - 1; j >= 0; j--)
                {
                    var item = _itemList[i][j];
                    // 回收item
                    UIItemPool.CollectItem(item);
                }
                // 清空
                _itemList[i].Clear();
            }

            // 清空跑马灯信息
            _infoList.Clear();
        }
    }

}

