using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using BaseEvent = GameInit.Game.BaseEvent;

namespace GameHotfix.Game
{
    public partial class UINoticeWindow
    {
        // 显示礼物提示定时器
        private uint _giftTipsTimer;
        // 方向
        private int _showGiftTipsDir;
        
        protected override void InitWindowParam()
        {
            Layer = WindowLayer.Middle;
            WindowId = UIWindowId.UINoticeWindow;
        }

        protected override void ParseComponentExt()
        {
            ListenButton(_uiGameRankingBtn,()=>AlertDialogView.Launch(new DialogGameRankingView()));
        }

        protected override void RegisterEvent()
        {
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventSetGiftTips,SetGiftTips);
            GameNoticeModel.Instance.AddEventListener(GameNoticeModel.EventDoubleExp,SetTips);
        }

        protected override void UnRegisterEvent()
        {
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventSetGiftTips,SetGiftTips);
            GameNoticeModel.Instance.RemoveEventListener(GameNoticeModel.EventDoubleExp,SetTips);
        }


        protected override void Refresh(params object[] arg)
        {
            // 显示跑马灯
            _horseRaceLamp.Show();
            // 显示屏幕边界气泡
            _edgeBubble.Show();
            // 显示banner
            _bannerTips.Show();
            // 定位
            _locationObject.Show();
            // boss排行榜
            _uIBossRankingView.Show();
            // 显示世界排行玩家
            _uITopPlayerTips.Show();
            // 查询信息界面
            _queryPlayerInfo.Show();
            
            // 默认方向
            _showGiftTipsDir = -1;
            // giftTips 动画
            ShowGiftTipsAnimation();
            _uiTips.gameObject.SetActive(false);
        }

        private void SetGiftTips(BaseEvent e)
        {
            _uiGiftTips.SetActive(!_uiGiftTips.activeSelf);
        }
        
        private void SetTips(BaseEvent e)
        {
            //_uiTips.gameObject.SetActive(!_uiTips.gameObject.activeSelf);
            _uiTips.gameObject.SetActive(false);
        }

        private void ShowGiftTipsAnimation()
        {
            var width = _uiGiftTips01.GetComponent<RectTransform>().rect.width;
            // 删除旧对象
            TimerHeap.DelTimer(_giftTipsTimer);
            _giftTipsTimer = TimerHeap.AddTimer(0, 5000, () =>
            {
                if (_showGiftTipsDir == -1)
                {
                    _uiGiftTips01.DOLocalMoveX(-width, 1f);
                    _uiGiftTips02.DOLocalMoveX(0, 1f);
                }
                else
                {
                    _uiGiftTips01.DOLocalMoveX(0, 1f);
                    _uiGiftTips02.DOLocalMoveX(width, 1f);
                }
                
                _showGiftTipsDir = -_showGiftTipsDir;
            });

        }


        public override void Dispose()
        {
            base.Dispose();
            _horseRaceLamp.Dispose();
            _edgeBubble.Dispose();
            _bannerTips.Dispose();
            _locationObject.Dispose();
            // boss排行榜
            _uIBossRankingView.Dispose();
            // 显示世界排行玩家
            _uITopPlayerTips.Dispose();
            // 查询信息界面
            _queryPlayerInfo.Dispose();
            // 删除旧对象
            TimerHeap.DelTimer(_giftTipsTimer);
        }

        public override void Close()
        {
            base.Close();
            // 隐藏
            _horseRaceLamp.Hide();
            _edgeBubble.Hide();
            _bannerTips.Hide();
            _locationObject.Hide();
            // boss排行榜
            _uIBossRankingView.Hide();
            // 显示世界排行玩家
            _uITopPlayerTips.Hide();
            // 查询信息界面
            _queryPlayerInfo.Hide();
            // 删除旧对象
            TimerHeap.DelTimer(_giftTipsTimer);
            // boss排行榜
            _uIBossRankingView.Hide();
        }
    }

}

