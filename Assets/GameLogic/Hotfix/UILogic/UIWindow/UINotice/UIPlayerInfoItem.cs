﻿using System;
using DG.Tweening;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

namespace GameHotfix.Game
{
    public partial class UIPlayerInfoItem
    {
        // 玩家数据
        private RoleData _data;
        // 开始位置
        private Vector3 _startPos;
        protected override void ParseComponentExt()
        {
            
        }

        protected override void Refresh(params object[] arg)
        {
            _data = (RoleData)arg[0];
            _startPos = (Vector3) arg[1];
            // 显示玩家信息
            ShowPlayerInfo();
            // 显示光环
            ShowHalo();
            // 显示坐骑
            ShowZouQi();
        }
        
        // 显示玩家信息
        private void ShowPlayerInfo()
        {
            // 等级
            _uiLevel.text = _data.Level.ToString();
            // 名字
            _uiNameText.text = _data.Name;
            // 战斗力
            var fightValue = (int)_data.AttackAddition(_data.RoleDef.Attack) * 50 + _data.MaxHp * 10;
            _uiFighValue.text = "战斗力:" + fightValue;
            // imag
            var resId = _data.RoleDef.ResId;
            var resDef = GameInit.Game.RoleResConfig.Get(resId);
            var roleImag = SpriteAtlasMgr.Instance.LoadSpriteSync(resDef.ImageAtlas, resDef.RoleImage);
            // 缩放
            var ratio = roleImag.bounds.size.y / 170f;
            _uiIcon.sprite = roleImag;
            _uiIcon.GetComponent<RectTransform>().sizeDelta = new Vector2(roleImag.bounds.size.x / ratio,170f);
            // 名字
            _uiSpeciesName.text = _data.RoleDef.SpeciesName;
        }
        // 显示光环
        private void ShowHalo()
        {
            var haloDef = _data.HaloDef();
            // 等级
            _uiHaloLevel.text = $"Lv.{haloDef.ID % 100 - 1}";
            // 名字
            _uiHaloName.text = "" + haloDef.HaloName;
            // 进度条
            var progress = (float) (_data.HaloExp - haloDef.PreExpLimit) / (haloDef.ExpLimit - haloDef.PreExpLimit);
            _uiHaleProgressImg.fillAmount = progress;
        }
        // 显示坐骑
        private void ShowZouQi()
        {
            var zouQiDef = _data.PetDef();
            // 显示等级
            _uiZhuoQiLevel.text = $"Lv.{zouQiDef.ID % 100 - 1}:";
            // 等级进度
            var progress = (float) (_data.PetExp - zouQiDef.PreExpLimit) / (zouQiDef.ExpLimit - zouQiDef.PreExpLimit);
            _uiZhuoQiProgressmg.fillAmount = progress;
            if (zouQiDef.RoleId > 0)
            {
                // 配置
                var roleDef = GameInit.Game.RoleConfig.Get(zouQiDef.RoleId);
                var roleResDef = GameInit.Game.RoleResConfig.Get(roleDef.ResId);
                // 名字
                _uiZhuoQiName.text = "" + roleDef.SpeciesName;
                // icon
                var imag = SpriteAtlasMgr.Instance.LoadSpriteSync(roleResDef.ImageAtlas, roleResDef.RoleImage);
                // 缩放
                var imageSize = imag.bounds.size;
                var heightRatio = imageSize.y / 90f;
                var width = imageSize.x / 215f;
                var minRation = Math.Max(heightRatio, width);
                _uiZHouQiImg.sprite = imag;
                _uiZHouQiImg.GetComponent<RectTransform>().sizeDelta = imageSize / minRation;
                // 显示
                _uiZHouQiImg.color = Color.white;
            }
            else
            {
                // 名字
                _uiZhuoQiName.text = "宠物";
                // 隐藏
                _uiZHouQiImg.color = Color.black;
            }
        }

        // 显示动画
        public void ShowAnimation(Action callBAck)
        {
            // 高
            var height = RectTransform.rect.height;
            // 屏幕高
            var screenHeight = RectTransform.parent.GetComponent<RectTransform>().rect.height;
            // 隐藏位置
            Vector3 hidePos = _startPos;
            hidePos.y = -(screenHeight + height)/2;
            // 隐藏到屏幕外
            RectTransform.localPosition = hidePos;
            // 显示动画
            RectTransform.DOLocalMove(_startPos, 0.5f);
            RectTransform.DOLocalMove(hidePos, 0.5f).SetDelay(4.5f).OnComplete(() => callBAck?.Invoke());
        }
    }
}