using System;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using BaseEvent = GameInit.Game.BaseEvent;

namespace GameHotfix.Game
{
    public partial class MainWindow
    {
        protected override void InitWindowParam()
        {
            Layer = WindowLayer.Bottom;
            WindowId = UIWindowId.UIMainWindow;
        }

        protected override void ParseComponentExt()
        {
            base.ParseComponentExt();
            ListenButton(_singleBtn, () => { StageManager.Instance.EnterStage(GameStageConst.BattleStage);});
            ListenButton(_fieldBossBtn,StartFieldBoss);
            _uiToggle.isOn = true;
            BattleManager.Instance.IsPeace = true;
            _uiToggle.onValueChanged.AddListener(AcceptIntrusion);
            // 提示
            _uiTips.gameObject.SetActive(false);
        }

        protected override void RegisterEvent()
        {
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventRefreshGameTime,RefreshGameTime);
        }

        protected override void UnRegisterEvent()
        {
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventRefreshGameTime,RefreshGameTime);
        }


        /// <summary>
        /// 复选框
        /// </summary>
        /// <param name="isSelect"></param>
        private void AcceptIntrusion(bool isSelect)
        {
            BattleManager.Instance.IsPeace = isSelect;
        }
        /// <summary>
        /// 刷新时间
        /// </summary>
        /// <param name="event"></param>
        private void RefreshGameTime(BaseEvent @event)
        {
            var time = (DateTime) @event.EventObj;
            bool isOpenFieldBoss = false;

            var timerArray = GameConstantHelper.FieldBossOpenTime.Split(',');
            
            // 判断是否到达时间
            for (int i = 0; i < timerArray.Length; i++)
            {
                var timeRange = timerArray[i].Split('|');
                var minTIme = int.Parse(timeRange[0]);
                var maxTime = int.Parse(timeRange[1]);

                if (time.Hour >= minTIme && time.Hour <= maxTime)
                {
                    isOpenFieldBoss = true;
                    break;
                }
            }

            _uiFieldBossBtnGray.gameObject.SetActive(!isOpenFieldBoss);
        }
        
        /// <summary>
        /// 开始野外boss
        /// </summary>
        private void StartFieldBoss()
        {
            if (_uiFieldBossBtnGray.gameObject.activeSelf)
            {
                Tips("未到时间");
                return;
            }
            
            StageManager.Instance.EnterStage(GameStageConst.FieldBoss);
        }

        private void Tips(string tips)
        {
            var tipsObj = GameObject.Instantiate(_uiTips.gameObject, _uiTips.parent);
            tipsObj.gameObject.SetActive(true);
            tipsObj.transform.Find("uiTipsText").GetComponent<Text>().text = tips;
            tipsObj.transform.DOLocalMoveY(200, 1f).OnComplete(() =>
            {
                GameObject.Destroy(tipsObj);
            });
            tipsObj.GetComponent<CanvasGroup>().DOFade(0.3f, 1f);
        }
    }
}