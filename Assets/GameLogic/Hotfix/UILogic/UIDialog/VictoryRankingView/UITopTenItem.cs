using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public partial class UITopTenItem
    {
        // 玩家信息
        private VictoryRankingInfo _victoryRankingInfo;
        protected override void Refresh(params object[] arg)
        {
            _victoryRankingInfo = (VictoryRankingInfo) arg[0];
            // 更新UI
            UpdateView();
        }
        
        // 刷新UI
        private void UpdateView()
        {
            // 设置排名Icon
            SetOrderIcon();
            // 设置头像
            SetHeaderIcon(_uiHeaderIcon);
            // 设置名称
            _uiName.text = GameNoticeModel.FormatName( _victoryRankingInfo.Name);
            // 设置等级
            _uiLevel.text = "Lv."+_victoryRankingInfo.Level;
            // 设置物种
            _uiSpeciesText.text = " "+ _victoryRankingInfo.Species;
            // 设置分数
            _uiCurScore.text = "" + _victoryRankingInfo.CurScore;
            // 设置月分数
            _uiWeekScore.text = $"本月分数({_victoryRankingInfo.MonthlyScore})";
            // 设置月的排名
            _uiWeekRanking.text = "" + _victoryRankingInfo.MonthlyOrder;
            // 设置连胜
            _uiWinStreakText.text = "连胜：" + _victoryRankingInfo.WinStreak;

        }
        // 设置排名Icon
        private async void SetOrderIcon()
        {
            // icon名称
            var iconName = "LV0" + _victoryRankingInfo.Order;
            if (_victoryRankingInfo.Order > 5)
                iconName = "NumberBox";
            // 设置Icon
            _uiRankIcon.sprite = await SpriteAtlasMgr.Instance.LoadSprite(iconName);
            _uiRankIcon.SetNativeSize();
            // 排名文本
            _uiRanText.text = "" + _victoryRankingInfo.Order;
            _uiRanText.gameObject.SetActive(_victoryRankingInfo.Order > 5);
        }
        // 设置头像
        private async void SetHeaderIcon(Image header)
        {
            var sprite = await HeadImageDownloader.LoadHeadSprite(_victoryRankingInfo.HeadUrl);
            if (header != null)
                header.sprite = sprite;
        }
        
        // 显示动画
        public void PlayShowAni(float delay)
        {
            // 缩小
            RectTransform.localScale = Vector3.zero;
            // 放大动画
            RectTransform.DOScale(Vector3.one, 0.2f)
                .SetEase(Ease.OutCirc)
                .SetDelay(delay);
        }
    }
}


