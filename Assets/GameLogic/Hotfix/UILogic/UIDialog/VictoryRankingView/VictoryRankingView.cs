using System.Collections;
using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Game
{
    public partial class VictoryRankingView
    {
        // 玩家信息
        private List<VictoryRankingInfo> _victoryRankingInfos;
        // 排行榜类型
        private VictoryRankingType _type;
        //
        private List<GameObject> _topThreeObj;
        // 前三名
        private List<UIFirstItem> _topThreeItem;
        // 前十名
        private List<UITopTenItem> _topTenItem;
        
        protected override void ParseComponentExt()
        {
            // 隐藏item
            _topThreeObj = new List<GameObject>();
            _topThreeObj.Add(_uIFirstItem);
            _topThreeObj.Add(_uIFirstItem01);
            _topThreeObj.Add(_uIFirstItem02);
            for (int i = 0; i < _topThreeObj.Count; i++)
                _topThreeObj[i].SetActive(false);
            
            _uITopTenItem.SetActive(false);
            // 初始化
            _topThreeItem = new List<UIFirstItem>();
            _topTenItem = new List<UITopTenItem>();
            // 注册按钮
            ListenButton(_uiBtnClose,ClickBtnClose);
            
            
        }

        protected override void RegisterEvent()
        {
            
        }

        protected override void UnRegisterEvent()
        {
            
        }

        protected override void Refresh(params object[] arg)
        {
            // 玩家信息
            _victoryRankingInfos = (List<VictoryRankingInfo>) arg[0];
            // 排行榜类型
            _type = (VictoryRankingType) arg[1];
            // 更新UI
            UpdateView();
            // 创建前三名
            CreateTopThree();
            // 创建前十名
            CreateTenThree();
            Logger.LogBlue("打开排行榜界面");

            
        }
        // 更新UI
        private async void UpdateView()
        {
            switch (_type)
            {
                case VictoryRankingType.Werewolf:
                    _uiTitleText.text = "冒险结束";
                    _uiTitleText.color = new Color(0.9803922f,0.4745098f,0.4980392f,1);
                    break;
                case VictoryRankingType.SmallAnimals:
                    _uiTitleText.text = "冒险结束";
                    _uiTitleText.color = new Color(0.4313726f,0.7333333f,0.9529412f,1);
                    break;
            }
        }

        // 创建前三名
        private void  CreateTopThree()
        {
            for (int i = 0; i < _victoryRankingInfos.Count && i < _topThreeObj.Count; i++)
            {
                // 实例化预制体
                var item = _topThreeObj[i];
                item.SetActive(true);
                // 实例化item
                var firstItem = new UIFirstItem();
                firstItem.SetDisplayObject(item);
                firstItem.Show(_victoryRankingInfos[i]);
                // 加入列表
                _topThreeItem.Add(firstItem);
                // 动画
                firstItem.PlayShowAni(0.3f + 0.2f * i);
            }
        }
        
        // 创建前十名
        private void CreateTenThree()
        {
            for (int i = 0; i < _victoryRankingInfos.Count; i++)
            {
                // 实例化预制体
                var item = GameObject.Instantiate(_uITopTenItem,_uiTopTenContent);
                item.SetActive(true);
                // 实例化Item
                var topTenItem = new UITopTenItem();
                topTenItem.SetDisplayObject(item);
                topTenItem.Show(_victoryRankingInfos[i]);
                // 加入列表
                _topTenItem.Add(topTenItem);
                // 显示动画
                topTenItem.PlayShowAni(0.3f + 0.05f * i);
            }
            // 重置UI布局
            LayoutRebuilder.ForceRebuildLayoutImmediate(_uiTopTenContent.GetComponent<RectTransform>());
        }
        
        //按钮-关闭界面
        private void ClickBtnClose()
        {
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                CloseAlert();
                // 进入主页阶段
                StageManager.Instance.EnterStage(GameStageConst.HomeStage);
            }
            else
            {
                ServerManager.Instance.StartGame((info) =>
                {
                    CloseAlert();
                    // 进入主页阶段
                    StageManager.Instance.EnterStage(GameStageConst.HomeStage);
                });
            }
        }

        public override void Dispose()
        {
            Clear();
            base.Dispose();
        }

        // 清除
        private void Clear()
        {
            // 删除前三item
            if(_topThreeItem != null)
                _topThreeItem.Clear();
            // 删除前十item
            for (int i = 0; i < _topTenItem.Count; i++)
            {
                var item = _topTenItem[i];
                if (item.DisplayObject != null) 
                    GameObject.Destroy(item.DisplayObject);
            }
            
            if(_topThreeItem != null)
                _topTenItem.Clear();
        }
    }
}


