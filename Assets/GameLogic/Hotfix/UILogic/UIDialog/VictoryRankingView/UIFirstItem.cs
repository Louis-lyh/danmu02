using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using Vector2 = UnityEngine.Vector2;

namespace GameHotfix.Game
{
    public partial class UIFirstItem
    {
        // 玩家信息
        private VictoryRankingInfo _victoryRankingInfo;
        // 动画队列
        private Sequence _showAniSeq;
        protected override void Refresh(params object[] arg)
        {
            _victoryRankingInfo = (VictoryRankingInfo) arg[0];
            // 刷新UI
            UpdateView();
        }
        
        // 刷新UI
        private void UpdateView()
        {
            // 头像
            SetHeaderIcon(_uiFirstHeaderIcon);
            // 名字
            _uiFirstNameText.text = GameNoticeModel.FormatName(_victoryRankingInfo.Name);
            // 积分
            _uiFirstScore.text = ""+ _victoryRankingInfo.CurScore;
        }
        
        // 设置头像
        private async void SetHeaderIcon(Image header)
        {
            var sprite = await HeadImageDownloader.LoadHeadSprite(_victoryRankingInfo.HeadUrl);
            if (header != null)
                header.sprite = sprite;
        }

        // 动画
        public void PlayShowAni(float delay)
        {
            RectTransform.gameObject.SetActive(false);
            // 放大
            RectTransform.localScale = Vector3.one * 2.5f;
            // 队列
            _showAniSeq = DOTween.Sequence();
            // 缩小动画
            _showAniSeq.Append(RectTransform.DOScale(1, 0.5f)
                .SetEase(Ease.OutElastic)
                .SetDelay(delay)
                .OnStart(()=>RectTransform.gameObject.SetActive(true)));
        }
    } 
}


