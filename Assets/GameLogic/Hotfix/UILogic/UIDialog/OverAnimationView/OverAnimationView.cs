﻿using System;
using DG.Tweening;
using GameInit.Framework;
using GameInit.Game;

namespace GameHotfix.Game
{
    public partial class OverAnimationView
    {
        // 左到右过度
        public  const string LeftToRight = "LeftToRight";
        // 左到右过度
        public const string RightToLeft = "RightToLeft";
        // 全黑回调
        private Action _fullCallBack;
        protected override void ParseComponentExt()
        {
            ShowFadeAni = false;
        }
        protected override void RegisterEvent()
        {
            
        }
        protected override void UnRegisterEvent()
        {
            
        }
        protected override void Refresh(params object[] arg)
        {
            var animationName = (string) arg[0];
            _uiOverTips.text = (string) arg[1];
            
            switch (animationName)
            {
                case LeftToRight:
                    LeftToRightAnimation();
                    break;
                case RightToLeft:
                    RightToLeftAnimation();
                    break;
                
            }
        }
        
        // 左到右过度
        private void LeftToRightAnimation()
        {
            HorizontalOver(UIGradientAxle.Horizontal, UIGradientDir.Reverse, UIGradientDir.Positive);
        }
        // 左到右过度
        private void RightToLeftAnimation()
        {
            HorizontalOver(UIGradientAxle.Horizontal, UIGradientDir.Positive, UIGradientDir.Reverse);
        }
        
        
        // 水平过度
        private void HorizontalOver(UIGradientAxle axle,UIGradientDir startDir,UIGradientDir endDir)
        {
            // 隐藏文字
            _uiOverTips.gameObject.SetActive(false);

            GameNoticeModel.Instance.UiGradientAni(_uiMask, axle, startDir, 0.5f)
                .SetEase(Ease.Linear)
                .OnComplete(() =>
            {
                // 显示文字
                _uiOverTips.gameObject.SetActive(true);
                // 显示
                GameNoticeModel.Instance.UiGradientAni(_uiMask, axle, endDir, 0.5f, 1, 0)
                    .SetEase(Ease.Linear)
                    .SetDelay(1f)
                    .OnComplete(() =>
                {
                    DoCloseAlert();
                });
            });
            
            TimerHeap.AddTimer(500, 0, () =>
            {
                // 全黑回调
                _fullCallBack?.Invoke();
            });
        }
        
        // 添加全黑回调
        public void AddFullCallBack(Action callBack)
        {
            _fullCallBack += callBack;
        }
    }
}