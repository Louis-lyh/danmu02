using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Game
{
    public partial class BossTipsView
    {
        // 头像地址
        private string _headUrl;
        
        // 打开的界面
        private string _panelName;
        
        // bossComing 动画队列
        private Sequence _bossComingSeq;
        // 显示危险提示动画
        private Tweener _showDangerousDoTween;
        // 显示危险提示文本动画
        private Tweener _showDanTextDoTween;
        
        // 技能倒计时
        private uint _bossSkillTimer;
        // 当前点赞总数
        private float _bossSkillLikeCount;
        // 目标点赞数
        private int _bossSkillLikeTarget;
        // 技能id
        private int _bossSkillId;
        // 资源id
        private int _resId;
        // 资源配置
        private GameInit.Game.RoleResDef _roleRes;
        // 技能结束
        private bool _bossSkillEnd;
       
        protected override async void ParseComponentExt()
        {
            // 隐藏
            _uiBossComingTips.SetActive(false);
            _uiWhoIsBossTips.SetActive(false);
            _uiBossSkillTips.SetActive(false);
            _uiDangerous.gameObject.SetActive(false);
            // 初始化
            _bossSkillEnd = true;
        }

        protected override void RegisterEvent()
        {
            // 刷新取消技能进度
           BattleUIModel.Instance.AddEventListener(BattleUIModel.EventCancelSkillProgress,RefreshCancelProgress);
        }

        protected override void UnRegisterEvent()
        {
            // 刷新取消技能进度
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventCancelSkillProgress,RefreshCancelProgress);
        }

        protected override void Refresh(params object[] arg)
        {
            _panelName = (string)arg[0];
            _root.GetComponent<CanvasGroup>().alpha = 1;
            // mask 关闭动画
            MaskAni(true,0.1f);
            // 打开界面
            if (_panelName.Equals("BossTips"))
            {
                // 头像地址
                _headUrl = (string) arg[2];
                // 资源id
                _resId = (int) arg[3];
                // 资源配置
                _roleRes = GameInit.Game.RoleResConfig.Get(_resId);
                // 打开BossTips
                OpenBossComing((string)arg[1],(UnitType) arg[4]);
            }
            else if (_panelName.Equals("BossSkill"))
            {
                // 需要的点赞数
                _bossSkillLikeTarget = (int)arg[2];
                _bossSkillLikeCount = 0;
                // 技能id
                _bossSkillId = (int)arg[3];
                // 资源id
                _resId = (int) arg[4];
                // 资源配置
                _roleRes = GameInit.Game.RoleResConfig.Get(_resId);
                // 开启技能
                _bossSkillEnd = false;
                // 技能进度条
                _uiCanceProgress.fillAmount = 0;
                // BossSkill
                ShowBossSkillTips((int)arg[1]);
            }
        }
        // uiMask渐变动画
        private void MaskAni(bool open,float duration = 1f)
        {
            if(_uiMask == null)
                return;
            
            // 渐变mask
            if (open)
            {
                _uiMask.color = new Color(0,0,0,0);
                var maskDoTween = _uiMask.DOFade(0.8f, duration);
            }
            else
            {
                _uiMask.color = new Color(0,0,0,0.8f);
                var maskDoTween = _uiMask.DOFade(0f, duration);
            }
        }

        private string DropTips()
        {
            var day = BattleManager.Instance.Day;

            switch (day)
            {
                case 1:return "光环星星";
                case 2:return "积分糯米";
                case 3:return "宠物宝石";
                case 4:return "积分糯米";
                case 5:return "光环星星";
                case 6:return "积分糯米";
                case 7:return "宠物宝石";
                case 8:return "积分糯米";
            }
            return "光环星星";
        }

        #region bossComing
         // 打开bossTips
        private void OpenBossComing(string bossName,UnitType unitType)
        {
            // 关闭打开动画
            ShowFadeAni = false;
            
            // 显示界面
            _uiBossComingTips.SetActive(true);

            // 显示渐变动画
            var canvasGroup = _root.GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0;
            var bgDoTween = canvasGroup.DOFade(1, 2f);
            // 背景
            _uiBossComingBg01.gameObject.SetActive(unitType == UnitType.Boss);
            _uiBossComingBg.gameObject.SetActive(unitType == UnitType.MonsterBoss);
            // 标题
            _uiBossComingTitle.gameObject.SetActive(unitType == UnitType.MonsterBoss);
            _uiBossComingTitle01.gameObject.SetActive(unitType == UnitType.Boss);
            // 掉落物提示
            _uiDropTipsText.text = "掉落物：" +DropTips();
            
            // mask 显示动画
            MaskAni(true);
            
            // bossName
            _uiBossName.text = "" + bossName;
            // 显示boss image
            _uiBossImg.sprite = SpriteAtlasMgr.Instance.LoadSpriteSync(_roleRes.ImageAtlas,_roleRes.RoleImage);
            var size = _uiBossImg.sprite.rect;
            var ratio = 1000 / size.height;
            _uiBossImg.rectTransform.sizeDelta = new UnityEngine.Vector2(ratio * size.width,1000);
            
            // boss image 渐变动画
            _uiBossImg.gameObject.SetActive(false);
            var bossImgDoTween = GameNoticeModel.Instance.UiGradientAni(_uiBossImg,
                    UIGradientAxle.Vertical, UIGradientDir.Reverse, 2f)
                .SetEase(Ease.Linear)
                .OnStart(() => _uiBossImg.gameObject.SetActive(true));

            // 初始化队列
            if (_bossComingSeq == null)
                _bossComingSeq = DOTween.Sequence();

            // 加入队列
            _bossComingSeq.Append(bgDoTween);
            // _bossComingSeq.AppendCallback(() =>
            // {
            //    
            // });
            // 修改玩家位置
            TimerHeap.AddTimer(2000, 0, () =>
            {
                // 显示危险提示
                ShowDangerous();
                // 改变地图
                if (BattleManager.Instance.BattleType == BattleType.Normal)
                    DiurnalVariationmManager.Instance.ChangeMap(1);
                // 修改玩家位置
                BattleModel.Instance.ChangeAllPlayerPos();
            });
            
            // 显示boss image
            _bossComingSeq.Append(bossImgDoTween);
            // 显示boss卡片
            _bossComingSeq.AppendCallback(ShowBossInfo);
        }
        // 显示Boss信息
        private void ShowBossInfo()
        {
            // 显示
            _uiWhoIsBossTips.SetActive(true);
            // 隐藏玩家信息
            _uiPlayerInfo.gameObject.SetActive(false);

            //  显示Boss信息
            SetHeaderIcon(_uiHeaderIcon);
            _uiPlayerInfo.localScale = Vector3.one * 2;

            TimerHeap.AddTimer(100, 0, () =>
            {
                _uiPlayerInfo.DOScale(Vector3.one, 0.3f)
                    .SetEase(Ease.OutElastic)
                    .OnStart(() => _uiPlayerInfo.gameObject.SetActive(true));
            });
            
            TimerHeap.AddTimer(1500, 0, () =>
            {
                CloseBossComing();
            });
        }
        // 显示危险提示
        private void ShowDangerous()
        {
            // 显示
            _uiDangerous.gameObject.SetActive(true);
            // 组件
            var canvasGroup = _uiDangerous.GetComponent<CanvasGroup>();
            canvasGroup.alpha = 0f;
            // 循环显示Item
            _showDanTextDoTween = canvasGroup.DOFade(1, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }
        
        // 设置头像
        private async void SetHeaderIcon(Image header)
        {
            if (!string.IsNullOrEmpty(_headUrl))
            {
                var sprite = await HeadImageDownloader.LoadHeadSprite(_headUrl);
                if (header != null)
                    header.sprite = sprite;
            }
            else
            {
                var sprite = await SpriteAtlasMgr.Instance.LoadSprite(_roleRes.IconAtlas, _roleRes.RoleIcon);
                if (header != null)
                    header.sprite = sprite;
            }

           
        }
        
        #endregion

        #region BossSkillTips
        // 显示boss
        private void ShowBossSkillTips(float time)
        {
            // 暂停游戏
            BattleManager.Instance.IsPause = true;
            // 显示boss
            _uiSkillBossImg.sprite = SpriteAtlasMgr.Instance.LoadSpriteSync(_roleRes.ImageAtlas, _roleRes.RoleImage);
            _uiSkillBossImg.SetNativeSize();
            // 显示
            _uiBossSkillTips.SetActive(true);
            // 显示倒计时
            SkillTipsCountdown(time);
        }
        // 倒计时
        private void SkillTipsCountdown(float time)
        {
            var click = true;
            int interval = 100;
            _bossSkillTimer = TimerHeap.AddTimer( 0,interval, () =>
            {
                // 倒计时间
                _uiCountdownText.text = "" + Mathf.Ceil(time);
                if (time > 0)
                    time -= interval / 1000f;
                else
                {
                    TimerHeap.DelTimer(_bossSkillTimer);
                    ClosePanel();
                    // 清除技能cd
                    BattleManager.Instance.BossUnit.FighterData.GetSkill_SKillId(_bossSkillId).ClearCd();
                }
                // 点击d动画
                _uiClickUp.SetActive(click);
                _uiClickDown.SetActive(!click);
                click = !click;
            });
        }
        
        // 显示取消进度
        private void RefreshCancelProgress(GameInit.Game.BaseEvent e)
        {
            // 结束退出
            if(_bossSkillEnd)
                return;
            
            // 点赞数量
            var likeCount = (int) e.EventObj;
            _bossSkillLikeCount += likeCount;
            
            // 进度
            var progress = _bossSkillLikeCount / _bossSkillLikeTarget;
            _uiCanceProgress.fillAmount = progress;
            
            Logger.LogPurple($"取消技能 {_bossSkillLikeCount } / { _bossSkillLikeTarget} = {progress}");
            
            if (progress >= 1)
            {
                // 停止倒计时
                TimerHeap.DelTimer(_bossSkillTimer);
                // 关闭界面
                ClosePanel();
                // 定时
                TimerHeap.AddTimer(1000, 0, () =>
                {
                    // 清除技能cd
                    BattleManager.Instance.BossUnit.FighterData.GetSkill_SKillId(_bossSkillId)?.ClearCd();
                });
                // 群体护盾
                BattleModel.Instance.AddShield_AllRole(1);
            }
        }

        #endregion
        // 关闭bossComing提示UI
        private void CloseBossComing()
        {
            // 关闭消失动画
            ShowFadeAni = false;
            // 重新设置材质球
            if(_uiBossComingBg != null)
                _uiBossComingBg.material = null;
            
            if(_uiBossImg != null)
                _uiBossImg.material = null;
            
            // 关闭危险提示动画
            _showDanTextDoTween?.Kill();
            // 隐藏危险提示
            if (_uiDangerous != null)
            {
                _uiDangerous.GetComponent<CanvasGroup>().DOFade(0, 0.5f);
                TimerHeap.AddTimer(500, 0, () =>
                {
                    // mask 关闭动画
                    MaskAni(false, 0.1f);
                });
            }

            if (_root != null)
            {
                // 消失
                _root.GetComponent<CanvasGroup>().DOFade(0, 0.7f);
                
                TimerHeap.AddTimer(700, 0, () =>
                {
                    BattleModel.Instance.CreateBoss();
                    ClosePanel();
                });
            }
        }

        private void ClosePanel()
        {
            // 恢复游戏
            BattleManager.Instance.IsPause = false;
            Clear();
            // 关闭
            CloseAlert();
            // 结束
            _bossSkillEnd = true;
        }
        public override void Dispose()
        {
            base.Dispose();
            Clear();
        }

        private void Clear()
        {
            TimerHeap.DelTimer(_bossSkillTimer);
        }
    }
}


