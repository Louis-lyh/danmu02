using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public partial class UIGameRankingItem
    {
        // 玩家信息
        private RoleData _roleData;
        //
        private int _order;
        // 信息
        private ServerData.LacEventExecute _data;
        protected override void Refresh(params object[] arg)
        {
            _roleData = (RoleData) arg[0];
            // 排名
            _order = (int) arg[1];
            // 数据
            _data = BattleModel.Instance.GetPlayerExecuteData(_roleData.UserId);
            // 更新UI
            if(_data != null)
                UpdateView();
            else 
                Hide();
        }
        
        // 刷新UI
        private void UpdateView()
        {
            // 设置排名Icon
            SetOrderIcon();
            // 设置头像
            SetHeaderIcon(_uiHeaderIcon);
            // 设置名称
            _uiName.text = GameNoticeModel.FormatName( _data.userName);
            // 设置分数
            _uiCurScore.text = "" + _roleData.KillScore;
            // 设置月分数
            _uiWeekScore.text = $"本月分数({_data.score})";
            // 设置月的排名
            _uiWeekRanking.text = "" + _data.rank;
            // 击败数
            if (_data.extension != null && _data.extension.ContainsKey(ServerData.ExtensionType.KillCount.ToString()))
                _uiKillCountText.text = "" + _data.extension[ServerData.ExtensionType.KillCount.ToString()];
            else
                _uiKillCountText.text = "0";

        }
        // 设置排名Icon
        private async void SetOrderIcon()
        {
            // icon名称
            var iconName = "LV0" + _order;
            if (_order > 5)
                iconName = "NumberBox";
            // 设置Icon
            _uiRankIcon.sprite = await SpriteAtlasMgr.Instance.LoadSprite(iconName);
            _uiRankIcon.SetNativeSize();
            // 排名文本
            _uiRanText.text = "" + _order;
            _uiRanText.gameObject.SetActive(_order > 5);
        }
        // 设置头像
        private async void SetHeaderIcon(Image header)
        {
            var sprite = await HeadImageDownloader.LoadHeadSprite(_data.headUrl);
            if (header != null)
                header.sprite = sprite;
        }
        
        // 显示动画
        public void PlayShowAni(float delay)
        {
            // 缩小
            RectTransform.localScale = Vector3.zero;
            // 放大动画
            RectTransform.DOScale(Vector3.one, 0.2f)
                .SetEase(Ease.OutCirc)
                .SetDelay(delay);
        }
    }
}
