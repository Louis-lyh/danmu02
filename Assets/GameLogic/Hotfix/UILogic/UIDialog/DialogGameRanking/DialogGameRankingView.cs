using System.Collections;
using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public partial class DialogGameRankingView
    {
        private List<UIGameRankingItem> _gameRanking;
        protected override void ParseComponentExt()
        {
            _uIGameRankingItem.gameObject.SetActive(false);
            // 初始化
            _gameRanking = new List<UIGameRankingItem>();
            // 关闭
            ListenButton(_uiCloseBtn,CloseAlert);
        }

        protected override void Refresh(params object[] arg)
        {
            // 玩家数据排序
            var playerList = BattleManager.Instance.AllRole(true);
            playerList.Sort((a,b)=>b.RoleData.Exp - a.RoleData.Exp);

            for (int i = 0; i < playerList.Count || i < _gameRanking.Count; i++)
            {
                UIGameRankingItem item = null;
                if (i < _gameRanking.Count)
                    item = _gameRanking[i];
                else
                {
                    var itemObj = GameObject.Instantiate(_uIGameRankingItem,_uIGameRankingContent);
                    itemObj.SetActive(true);
                    item = new UIGameRankingItem();
                    item.SetDisplayObject(itemObj);
                    _gameRanking.Add(item);
                }

                if (i < playerList.Count) 
                    item.Show(playerList[i].RoleData,i + 1);
                else 
                    item.Hide();
            }
        }

        public override void Dispose()
        {
            for (int i = 0; i < _gameRanking.Count; i++)
            {
                GameObject.Destroy(_gameRanking[i].DisplayObject);
                _gameRanking[i].Dispose();
            }

            _gameRanking.Clear();
            
            base.Dispose();
        }
    }

}

