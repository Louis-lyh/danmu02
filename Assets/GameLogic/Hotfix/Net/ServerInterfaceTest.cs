using System.Collections.Generic;
using Cysharp.Text;
using GameHotfix.Framework;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public class ServerInterfaceTest : MonoBehaviour
    {
        //玩家创建
        private int _cfgID;
        private long _number;
        private Dictionary<string, string> _userDict;
        
        //随机字典
        private List<string> _randomDict;
        
        // boss礼物s
        private int _bossGiftCount;
        
        // 公会id随机列表
        private IList<int> _guildIdList = new List<int>(){0,1,1,1};

        void Start()
        {
            _cfgID = 1;
            _number = 1;
            _userDict = new Dictionary<string, string>();
            
            _randomDict = new List<string>();

            _bossGiftCount = 0;
        }
        
        void Update()
        {
            //弹幕
            UpdateComment();
            
            //点赞
            UpdateLike();
            
            //礼物
            UpdateGift();
        }
        
        #region 弹幕
        private void UpdateComment()
        {
            //加入1个玩家
            if (Input.GetKeyDown(KeyCode.F1))
            {
                CommentBirth(1);
            }
            //加入10个玩家
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                CommentBirth(10);
            }
            //加入20个玩家
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                CommentBirth(20);
            }
            //加入30个玩家
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                CommentBirth(30);
            }
            
            //打怪
            if (Input.GetKeyDown(KeyCode.F5))
            {
                CommentAction(ServerData.EBarrageType.e_attack);
            }
            //采集
            else if (Input.GetKeyDown(KeyCode.F6))
            {
                CommentAction(ServerData.EBarrageType.e_collect);
            }
            //定位
            else if (Input.GetKeyDown(KeyCode.F7))
            {
                CommentAction(ServerData.EBarrageType.e_position);
            }
            //666
            else if (Input.GetKeyDown(KeyCode.F8))
            {
                CommentAction(ServerData.EBarrageType.e_exp);
            }
            // 查询
            else if (Input.GetKeyDown(KeyCode.F9))
            {
                CommentAction(ServerData.EBarrageType.e_check);
            }
        }
        
        private void CommentBirth(int count)
        {
            for (int i = 0; i < count; i++)
            {
                //配置id 归1
                Dictionary<int, TestUserDef> dict = TestUserConfig.GetDefDic();
                if (_cfgID > dict.Count)
                {
                    _cfgID = 1;
                    _number++;
                }
                //得到配置
                TestUserDef def = TestUserConfig.Get(_cfgID);
                //得到userId
                string userId = ZString.Concat(def.UserId, _number);
                //得到userName
                string userName = ZString.Concat(def.UserName, _number);
                //得到headUrl
                string headUrl = "TestHeadUrl";
                //加入
                _userDict.Add(userName, userId);

               

                //封装消息
                ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
                lacEventExecute.userId = userId;
                lacEventExecute.userName = userName;
                lacEventExecute.headUrl = headUrl;
                lacEventExecute.lcType = ServerData.ELogicActionType.e_comment;
                lacEventExecute.bType = ServerData.EBarrageType.e_birth;
                lacEventExecute.guildId = 0;//Random.Range(BattleManager.Instance.GuildId,2);
                lacEventExecute.extension = new Dictionary<string, int>();
                lacEventExecute.extension.Add(ServerData.ExtensionType.PetExp.ToString(),11000);
                lacEventExecute.extension.Add(ServerData.ExtensionType.KillCount.ToString(),1);
                lacEventExecute.extension.Add(ServerData.ExtensionType.HaloExp.ToString(),1);
                if (_cfgID == 1)
                {
                    //lacEventExecute.extension.Add(ServerData.ExtensionType.HeroExp.ToString(),1732190);
                }
                //处理
                CommandMgr.Instance.CommandExecuted(lacEventExecute);
                
                //cfgID++
                _cfgID++;
                //加入随机字典
                _randomDict.Add(userName);
            }
        }

        private void CommentAction(ServerData.EBarrageType eBarrageType)
        {
            if (_randomDict.Count <= 0) return;
            
            int index = UnityEngine.Random.Range(0, _randomDict.Count);
            string nickname = _randomDict[index];
            
            string userId;
            if (_userDict.TryGetValue(nickname, out userId))
            {
                string userName = nickname;
                string headUrl = "TestHeadUrl";
                
                //封装消息
                ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
                lacEventExecute.userId = userId;
                lacEventExecute.userName = userName;
                lacEventExecute.headUrl = headUrl;
                lacEventExecute.lcType = ServerData.ELogicActionType.e_comment;
                lacEventExecute.bType = eBarrageType;
                
                //处理
                CommandMgr.Instance.CommandExecuted(lacEventExecute);
            }
        }
        #endregion
        
        #region 点赞
        private void UpdateLike()
        {
            if (Input.GetKeyDown(KeyCode.F9))
            {
                Like(1);
            }
            else if (Input.GetKeyDown(KeyCode.F10))
            {
                Like(5);
            }
            else if (Input.GetKeyDown(KeyCode.F11))
            {
                Like(10);
            }
            else if (Input.GetKeyDown(KeyCode.F12))
            {
                Like(20);
            }
        }

        private void Like(int likeNum)
        {
            if (_randomDict.Count <= 0) return;
            
            int index = UnityEngine.Random.Range(0, _randomDict.Count);
            string nickname = _randomDict[index];
            
            string userId;
            if (_userDict.TryGetValue(nickname, out userId))
            {
                string userName = nickname;
                string headUrl = "TestHeadUrl";
                
                //封装消息
                ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
                lacEventExecute.userId = userId;
                lacEventExecute.userName = userName;
                lacEventExecute.headUrl = headUrl;
                lacEventExecute.lcType = ServerData.ELogicActionType.e_like;
                lacEventExecute.likeNum = likeNum;
                
                //处理
                CommandMgr.Instance.CommandExecuted(lacEventExecute);
            }
        }
        #endregion

        #region 礼物
        private void UpdateGift()
        {
            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                Gift(1, 1, ServerData.EGiftType.e_wand);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                Gift(1, 19, ServerData.EGiftType.e_magicmirror);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                Gift(1, 52, ServerData.EGiftType.e_doughnut);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad4))
            {
                Gift(1, 99, ServerData.EGiftType.e_energycell);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                Gift(1, 199, ServerData.EGiftType.e_demonbomb);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad6))
            {
                Gift(1, 520, ServerData.EGiftType.e_mysteriousairdrop);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad7))
            {
                Gift(5, 520, ServerData.EGiftType.e_mysteriousairdrop);
            }
            else if (Input.GetKeyDown(KeyCode.Keypad8))
            {
                Gift(6, 520, ServerData.EGiftType.e_mysteriousairdrop);
            }
            else if(Input.GetKeyDown(KeyCode.L))
            {
                _bossGiftCount++;
                Gift(5, 19, ServerData.EGiftType.e_magicmirror);
            }
            else if (Input.GetKeyDown(KeyCode.F))
            {
                Gift(10, 1, ServerData.EGiftType.e_doughnut);
            }
            else if (Input.GetKeyDown(KeyCode.G))
            {
                Gift(10, 1, ServerData.EGiftType.e_magicmirror);
            } else if (Input.GetKeyDown(KeyCode.H))
            {
                Gift(66, 1, ServerData.EGiftType.e_wand);
            }
            
            
            if (Input.GetKeyDown(KeyCode.Z))
            {
                Gift(5, 1, ServerData.EGiftType.e_wand);
            }
            else if (Input.GetKeyDown(KeyCode.X))
            {
                Gift(5, 19, ServerData.EGiftType.e_magicmirror);
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                Gift(5, 52, ServerData.EGiftType.e_doughnut);
            }
            else if (Input.GetKeyDown(KeyCode.V))
            {
                Gift(5, 99, ServerData.EGiftType.e_energycell);
            }
            else if (Input.GetKeyDown(KeyCode.B))
            {
                Gift(5, 199, ServerData.EGiftType.e_demonbomb);
            }
            else if (Input.GetKeyDown(KeyCode.N))
            {
                Gift(5, 520, ServerData.EGiftType.e_mysteriousairdrop);
            }
            
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Gift(1, 1, ServerData.EGiftType.e_wand);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Gift(1, 19, ServerData.EGiftType.e_magicmirror);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Gift(1, 52, ServerData.EGiftType.e_doughnut);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Gift(1, 99, ServerData.EGiftType.e_energycell);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                Gift(1, 199, ServerData.EGiftType.e_demonbomb);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                Gift(1, 520, ServerData.EGiftType.e_mysteriousairdrop);
            }
        }

        private void Gift(int giftNum, int giftValue, ServerData.EGiftType giftType)
        {
            if (_randomDict.Count <= 0) return;
            
            int index = UnityEngine.Random.Range(0, _randomDict.Count);
            string nickname = _randomDict[index];
            
            string userId;
            if (_userDict.TryGetValue(nickname, out userId))
            {
                string userName = nickname;
                string headUrl = "TestHeadUrl";
                
                //封装消息
                ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
                lacEventExecute.userId = userId;
                lacEventExecute.userName = userName;
                lacEventExecute.headUrl = headUrl;
                lacEventExecute.lcType = ServerData.ELogicActionType.e_gift;
                lacEventExecute.giftNum = giftNum;
                lacEventExecute.giftValue = giftValue;
                lacEventExecute.giftType = giftType;
                
                //处理
                CommandMgr.Instance.CommandExecuted(lacEventExecute);
            }
        }
        
        public void BossGift(int giftNum, int giftValue, ServerData.EGiftType giftType)
        {
            var bossUnit = BattleManager.Instance.BossUnit as FighterUnitBoss;
            if(bossUnit == null)
                return;
            
            string userName = bossUnit.FighterData.Name;
            string headUrl = "TestHeadUrl";
                
            //封装消息
            ServerData.LacEventExecute lacEventExecute = new ServerData.LacEventExecute();
            lacEventExecute.userId = (bossUnit.FighterData as RoleData)?.UserId;;
            lacEventExecute.userName = userName;
            lacEventExecute.headUrl = headUrl;
            lacEventExecute.lcType = ServerData.ELogicActionType.e_gift;
            lacEventExecute.giftNum = giftNum;
            lacEventExecute.giftValue = giftValue;
            lacEventExecute.giftType = giftType;
                
            //处理
            CommandMgr.Instance.CommandExecuted(lacEventExecute);
        }

        
        #endregion
    }
}