using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;

namespace GameHotfix.Game
{
    public class HotfixEntry : HotfixBaseEntry
    {
        public override void Awake()
        {
            // 音效
            SoundManager.Instance.Init();
            // 加载本地资源初始化
            LocalResLoader.Instance.Init();
            // 阶段管理初始化
            StageManager.Instance.Init();
            // 注册阶段
            StageHelper.InitGameStage();
            // 注册界面
            UIWindowHelper.Init();
            // 初始化UiItem对象池
            UIItemPool.Init();
            // 指令管理
            CommandMgr.Instance.Init();
            // game通信
            GameBattleModel.Instance.Init();
        }
        
        public override async void Start()
        {
            // 加载本地资源启动
            LocalResLoader.Instance.Load();
        }
        
        public override void OnDestroy()
        {
            // 音效
            SoundManager.Instance.Destroy();
            // uiItem对象池
            UIItemPool.Destory();
            // 阶段管理
            StageManager.Instance.Destroy();
            // game通信
            GameBattleModel.Instance.Destroy();
        }

        private float _deltaTime;
        public override void Update()
        {
            _deltaTime = Time.deltaTime;
            
            // 加载本地资源
            LocalResLoader.Instance.Update();
            
            // 音效
            SoundManager.Instance.Update(_deltaTime);
            
            // 阶段管理
            StageManager.Instance.Update(_deltaTime);

            // 定位
            PositioningManager.Instance.Update(_deltaTime);
            
            // 昼夜
            DiurnalVariationmManager.Instance.Update(_deltaTime);
            // --------------------------
            //
        }
        
        public override void FixedUpdate()
        {
            
        }
    
        public override void LateUpdate()
        {
            
        }
    }
}