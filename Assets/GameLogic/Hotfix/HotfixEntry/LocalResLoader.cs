using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using GameHotfix.Framework;
using GameInit.Game;
using GameInit.Framework;

namespace GameHotfix.Game
{
    public class LocalResLoader : Framework.Singleton<LocalResLoader>
    {
        private class LoadingView
        {
            private GameObject _loadingObject;
            private Text _contentText;
            private Image _barImage;

            internal void SetDisplayObject(GameObject gameObject)
            {
                _loadingObject = gameObject;
                _contentText = _loadingObject.transform.Find("ProgressBar/TextProgress").GetComponent<Text>();
                _barImage = _loadingObject.transform.Find("ProgressBar/ImageProgress").GetComponent<Image>();
                
                _contentText.text = "Loading Local Resources...";
                _barImage.fillAmount = 0f;
                
                TweenHelper.NumberTo(3.0f, 0f, 1f, SetLoadingProgress);
            }

            internal void Close()
            {
                _barImage.DOKill();
                _loadingObject.SetActive(false);
            }
            
            //是否完成加载
            internal bool IsComplete()
            {
                bool result = false;
                if (_barImage == null)
                    return result;
                
                if (_barImage.fillAmount >= 1f)
                {
                    _barImage.DOKill();
                    result = true;
                }
                
                return result;
            }
            
            public void SetLoadingProgress(float pro)
            {
                _barImage.fillAmount = pro;
            }

            public void SetText(string text)
            {
                _contentText.text = text;
            }
        }
        
        //加载状态
        private enum EGameLoadingState
        {
            e_gls_null = 1,     //没有启动
            e_gls_loading,      //加载中
            e_gls_complete,     //完成
            e_gls_out,          //退出
        }
        
        //状态
        private EGameLoadingState _state = EGameLoadingState.e_gls_null;
        //本地数据加载完成标志
        private bool _dbLoadComplete;
        //游戏配置加载完成标志
        private bool _gameConfigComplete;
        //游戏管理(业务层)完成标志
        private bool _gameMgrComplete;
        // //内购加载完成标志
        // private bool _iapComplete;
        // //sdk加载完成标志
        // private bool _sdkComplete;
        
        // //添加本地化
        // private void PreventLinkerFromStrippingCommonLocalizationReferences()
        // {
        //     _ = new System.Globalization.ChineseLunisolarCalendar();
        //     _ = new System.Globalization.GregorianCalendar();
        //     _ = new System.Globalization.HebrewCalendar();
        //     _ = new System.Globalization.HijriCalendar();
        //     _ = new System.Globalization.JapaneseCalendar();
        //     _ = new System.Globalization.JapaneseLunisolarCalendar();
        //     _ = new System.Globalization.JulianCalendar();
        //     _ = new System.Globalization.KoreanCalendar();
        //     _ = new System.Globalization.KoreanLunisolarCalendar();
        //     _ = new System.Globalization.PersianCalendar();
        //     _ = new System.Globalization.TaiwanCalendar();
        //     _ = new System.Globalization.TaiwanLunisolarCalendar();
        //     _ = new System.Globalization.ThaiBuddhistCalendar();
        //     _ = new System.Globalization.UmAlQuraCalendar();
        // }

        public void Init()
        {
            // //添加本地化
            // PreventLinkerFromStrippingCommonLocalizationReferences();
            
            //标志=false
            _dbLoadComplete = false;
            _gameConfigComplete = false;
            _gameMgrComplete = false;
            // _iapComplete = false;
            // _sdkComplete = false;
        }
        
        public void Load()
        {
            //Log
            GameInit.Framework.Logger.LogGreen("Game Loading... Enter");
            
            //激活加载
            _state = EGameLoadingState.e_gls_loading;

            //打开加载界面
            OpenLoadingWindow();
            
            //加载游戏配置
            LoadingGameConfig();
            //加载本地数据
            LoadingDB();
            //加载游戏管理器
            LoadingGameManager();
            //启动游戏服务器连接
            StartGameServerConnection();
        }

        public void Update()
        {
            if (_state == EGameLoadingState.e_gls_out || _state == EGameLoadingState.e_gls_null)
                return;
            
            if (_state == EGameLoadingState.e_gls_loading)
            {
                bool b = false;
                if (Application.platform == RuntimePlatform.WindowsEditor) b = IsComplete();
                else b = IsComplete() && ServerManager.Instance.ConnectLoginComplete;
                
                if (b)
                {
                    //Log
                    GameInit.Framework.Logger.LogGreen("Game Loading... Completed");
                    //标记完成
                    _state = EGameLoadingState.e_gls_complete;
                    //切换到游戏界面
                    UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
                }
            }
            else if (_state == EGameLoadingState.e_gls_complete)
            {
                if (IsCanOut())
                {
                    //Log
                    GameInit.Framework.Logger.LogGreen("Game Loading... Out");
                    //标记退出
                    _state = EGameLoadingState.e_gls_out;
                    //打开主页
                    GameHotfix.Framework.Global.DispatchEvent("Event_OpenHomeWindow");
                    //打开主页
                    StageManager.Instance.EnterStage(GameStageConst.HomeStage);
                    //关闭加載界面
                    CloseLoadingWindow();
                    //初始化礼物管理器
                    GiftEffectManager.Instance.Init();
                    //初始化昼夜管理器
                    DiurnalVariationmManager.Instance.Init();
                    //初始化战斗信息管理器
                    BattleInfoManager.Instance.Init();
                    // 获取服务器时间
                    ServerTimerManager.Instance.StartTime();
                    //构建第三人称相机
                    string cameraPosStr = GameConstantHelper.LgCameraFollowCurPos;
                    string[] cameraPosArrays = cameraPosStr.Split(',');
                    float x = float.Parse(cameraPosArrays[0]);
                    float y = float.Parse(cameraPosArrays[1]);
                    float z = float.Parse(cameraPosArrays[2]);
                    GameObject followObj = CameraManager.Instance.CreateLgCameraFollowObj(GameObject.Find("SceneRoot"), new Vector3(x, y, z));
                    CameraManager.Instance.BindCamera(GameObject.Find("SceneRoot/BattleCamera").transform);
                    CameraManager.Instance.BindCtrl(1, followObj, 25f, 35f, 0f, 40f);
                }
            }
        }
        
        public bool IsComplete()
        {
            // return _loadingView != null &&
            //        _gameMgrComplete && _gameConfigComplete && _dbLoadComplete &&
            //        _iapComplete && _sdkComplete;
            return _loadingView != null && _gameMgrComplete && _gameConfigComplete && _dbLoadComplete;
        }
        
        public bool IsCanOut()
        {
            return _loadingView != null && _loadingView.IsComplete();
        }
        
        #region 加载界面
        private LoadingView _loadingView;
        
        //打开加载界面
        private void OpenLoadingWindow()
        {
            GameObject obj = GameObject.Find("uiLoading");
            if (obj == null)
            {
                var pfb = GameInit.Framework.ResourceManager.LoadAssetSync<GameObject>(GameInit.Framework.ResPathUtils.GetUIPrefab("uiLoading"));
                obj = GameObject.Instantiate(pfb);
                obj.name = "uiLoading";
                var parent = GameObject.Find("TopRoot").transform;
                obj.transform.SetParent(parent, false);
                obj.transform.localPosition = Vector3.zero;
                obj.transform.localScale = Vector3.one;
            }
            else
            {
                obj.SetActive(true);
            }
            _loadingView = new LoadingView();
            _loadingView.SetDisplayObject(obj);
        }
        
        //关闭加载界面
        private void CloseLoadingWindow()
        {
            if (_loadingView != null)
            {
                _loadingView.Close();
            }
        }
        #endregion
        
        #region 游戏管理器
        private void LoadingGameManager()
        {
            //创建游戏核心玩法管理器
            GameObject go = new GameObject();
            go.name = "GameCoreGameplayMng";
            go.AddComponent<GameCoreGameplayManager>();
            GameObject.DontDestroyOnLoad(go);
            
            //标记完成
            _gameMgrComplete = true;
            
            //测试
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                GameObject obj = new GameObject("ServerInterfaceTest");
                obj.AddComponent<ServerInterfaceTest>();
                GameObject.DontDestroyOnLoad(obj);
            }
        }
        #endregion
        
        #region 本地数据
        private void LoadingDB()
        {
            //本地数据初始化
            LocalDataManager.Init();
            
            //标记完成
            _dbLoadComplete = true;
        }
        #endregion
        
        #region 游戏配置
        private void AddConfig()
        {
            //非热更配置注册
            GameConfigManager.Instance.AddConfig("Role", new GameInit.Game.RoleConfig());
            GameConfigManager.Instance.AddConfig("Skill", new GameInit.Game.SkillConfig());
            GameConfigManager.Instance.AddConfig("Effect", new GameInit.Game.EffectConfig());
            GameConfigManager.Instance.AddConfig("Constant", new GameInit.Game.ConstantConfig());
            GameConfigManager.Instance.AddConfig("RoleAnimationTime", new GameInit.Game.RoleAnimationTimeConfig());
            GameConfigManager.Instance.AddConfig("Buff",new GameInit.Game.BuffConfig());
            GameConfigManager.Instance.AddConfig("Gift",new GameInit.Game.GiftConfig());
            GameConfigManager.Instance.AddConfig("Shake",new GameInit.Game.ShakeConfig());
            GameConfigManager.Instance.AddConfig("Halo",new GameInit.Game.HaloConfig());
            GameConfigManager.Instance.AddConfig("RoleRes",new GameInit.Game.RoleResConfig());
            GameConfigManager.Instance.AddConfig("Pet",new GameInit.Game.PetConfig());
            GameConfigManager.Instance.AddConfig("DropReward",new GameInit.Game.DropRewardConfig());
            GameConfigManager.Instance.AddConfig("SoundVol", new GameInit.Game.SoundVolConfig());
            
            //热更配置注册
            ConfigManager.Instance.AddConfig("Role", new RoleConfig());
            ConfigManager.Instance.AddConfig("Skill", new SkillConfig());
            ConfigManager.Instance.AddConfig("Effect",new EffectConfig());
            ConfigManager.Instance.AddConfig("Constant",new ConstantConfig());
            ConfigManager.Instance.AddConfig("RoleAnimationTime",new RoleAnimationTimeConfig());
            ConfigManager.Instance.AddConfig("TestUser", new TestUserConfig());
            ConfigManager.Instance.AddConfig("SoundVol", new SoundVolConfig());
        }
        
        private async void LoadingGameConfig()
        {
            //配置注册
            AddConfig();
            
            //非热更配置读入
            await GameConfigManager.Instance.LoadConfig();
            
            //热更配置读入
            await ConfigManager.Instance.LoadConfig();
            
            //图集读入
            await SpriteAtlasMgr.Instance.ReloadAtlasInfo();
            
            //初始化常量
            ConstantHelper.Init();
            GameConstantHelper.Init();
            
            //标记完成
            _gameConfigComplete = true;
        }
        #endregion

        #region 启动游戏服务器连接
        private void StartGameServerConnection()
        {
            //1.从启动参数中获取Token
            string token = GetToken();
            if (string.IsNullOrEmpty(token))
            {
                _loadingView.SetText("error: failed to obtain token...");
                return;
            }
            
            //GUIUtility.systemCopyBuffer = token;
            
            ServerManager.Instance.Connect(token, LogConnectionMsg, CommandMgr.Instance.CommandExecuted);
        }

        private string GetToken()
        {
            string token = string.Empty;
            string[] CommandLineArgs = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < CommandLineArgs.Length; i++)
            {
                string s = CommandLineArgs[i];
                if (s.Contains("-token="))
                {
                    token = s.Replace("-token=", "");
                    break;
                }
            }

            return token;
        }

        private void LogConnectionMsg(string msg)
        {
            _loadingView.SetText(msg);
        }
        #endregion
    }
}
