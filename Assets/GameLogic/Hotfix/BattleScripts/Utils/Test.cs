using System;
using System.Collections.Generic;
using GameHotfix.Game;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

public class Test : MonoBehaviour
{
    private int _playerHp = 100;
    private int _bossHp = 100;
    private int _bossValue = 100;

    private float _skillProgress = 1;

    private int name;

    public float duration = 0.5f;       //震动时间
    public float strength = 0.1f;       //震动力度
    public int vibrato = 10;            //晃动幅度
    public float randomness = 90f;      //随机性 0为单方向震动 0-180 最好不超过90

    public UnitType _targrtUnittype;
    
    
    // Start is called before the first frame update
    void Start()
    {
        _targrtUnittype = UnitType.Monster;
        // Logger.LogBlue("Time "+GameConstantHelper.UnixTimeStampToDateTime(1724124142141));
        // Logger.LogBlue("Time "+DateTime.Now.Ticks);
        
    }

    // Update is called once per frame
    void Update()
    {
        
        // 发送信息
        if(Input.GetKeyDown(KeyCode.O))
        {
            // 场景过度
            var overAnimationView = new OverAnimationView();
            // 定时打开场景过度
            TimerHeap.AddTimer(0, 0,
                () => AlertDialogView.Launch(overAnimationView, OverAnimationView.LeftToRight,$"第{GameConstantHelper.ConvertToChinese(1)}波"));
        }
        // bosstis
        if (Input.GetKeyDown(KeyCode.R))
        {
            // RoleData roleData = new BossData("1",1,UnitType.Boss);
            // roleData.Name = "user1";
            // GameNoticeModel.Instance.ShowTopPlayer(roleData);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            SkillBossTips skillBossTips = new SkillBossTips();
            skillBossTips.RoleData = new RoleData("",0);
            // 奖励字典
            skillBossTips.Reward = new Dictionary<string, int>()
            {
                {"Exp", 1},
                {"Pet",1},
                {"Halo",1}
            };
            // 显示击杀提示
            GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowKillTips,skillBossTips);
        }

        //
        // if (Input.GetKeyDown(KeyCode.N))
        // {
        //     BattleModel.Instance.WerewolfSonicSkills(1,100022);
        // }

        // if (Input.GetKeyDown(KeyCode.M))
        // {
        //     GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventCancelSkillProgress,_skillProgress-=0.1f);
        // }
        
        // if (Input.GetKeyDown(KeyCode.B))
        // {
        //     BattleManager.Instance.UnitBack();
        // }
        
        
        // if (Input.GetKeyDown(KeyCode.H))
        // {
        //     name++;
        //     BattleModel.Instance.AddRole("玩家"+name,"玩家"+name,"",Random.Range(1,25862));
        // }
        //
        // if (Input.GetKeyDown(KeyCode.J))
        // {
        //     name++;
        //     BattleModel.Instance.AddRole("玩家"+name,"玩家"+name,"",0);
        // }
        //
        // if (Input.GetKeyDown(KeyCode.K))
        // {
        //     MonsterManager.Instance.CreateSmallWerewolf();
        // }
        //
        // if (Input.GetKeyDown(KeyCode.F1))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(1);
        // }
        // if (Input.GetKeyDown(KeyCode.F2))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(2);
        // }
        // if (Input.GetKeyDown(KeyCode.F3))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(3);
        // }
        // if (Input.GetKeyDown(KeyCode.F4))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(4);
        // }
        // if (Input.GetKeyDown(KeyCode.F5))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(5);
        // }
        // if (Input.GetKeyDown(KeyCode.F6))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(6);
        // }
        // if (Input.GetKeyDown(KeyCode.F7))
        // {
        //     GiftEffectManager.Instance.PlayGiftEff(7);
        // }
        //
        // if (Input.GetKeyDown(KeyCode.Keypad8))
        // {
        //     DiurnalVariationmManager.Instance.StartARound(null, null, null);
        // }
        // if (Input.GetKeyDown(KeyCode.Keypad9))
        // {
        //     DiurnalVariationmManager.Instance.Restart();
        // }
        //
        // if (Input.GetKeyDown(KeyCode.Z))
        // {
        //     // CommandMgr.Instance.ExecCommand("666",Random.Range(0,10).ToString(),"1");
        //     var RefreshData = new List<FighterUnitData>();
        //     var a1 = new RoleData("",1);
        //     a1.InitFighter(10000, "01", 100);
        //     RefreshData.Add(a1);
        //     var a2 = new RoleData("",1);
        //     a2.InitFighter(10000, "02", 80);
        //     RefreshData.Add(a2);
        //     var a3 = new RoleData("",1);
        //     a3.InitFighter(10000, "03", 60);
        //     RefreshData.Add(a3);
        //     var a4 = new RoleData("",1);
        //     a4.InitFighter(10000, "04", 40);
        //     RefreshData.Add(a4);
        //     var a5 = new RoleData("",1);
        //     a5.InitFighter(10000, "05", 20);
        //     RefreshData.Add(a5);
        //     BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventUpdatePlayerRanking,RefreshData);
        //     // 刷新玩家复活次数
        //     BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventUpdatePlayerHp,_playerHp -= 10);
        //     
        //     //------ boss ------
        //     //CommandMgr.Instance.ExecCommand("666",Random.Range(0,10).ToString(),"1");
        //     var bossData = new List<FighterUnitData>();
        //     var boss = new RoleData("",1);
        //     boss.InitFighter(10000, "boss", _bossValue +=10);
        //     RefreshData.Add(boss);
        //     BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventUpdateBossRanking,bossData);
        // }
        // if (Input.GetKeyDown(KeyCode.X))
        // {
        //     // CommandMgr.Instance.ExecCommand("666",Random.Range(0,10).ToString(),"1");
        //     var RefreshData = new List<FighterUnitData>();
        //     var a1 = new RoleData("",1);
        //     a1.InitFighter(10000, "01", 100);
        //     RefreshData.Add(a1);
        //     var a2 = new RoleData("",1);
        //     a2.InitFighter(10000, "04", 80);
        //     RefreshData.Add(a2);
        //     var a3 = new RoleData("",1);
        //     a3.InitFighter(10000, "03", 60);
        //     RefreshData.Add(a3);
        //     var a4 = new RoleData("",1);
        //     a4.InitFighter(10000, "05", 40);
        //     RefreshData.Add(a4);
        //     var a5 = new RoleData("",1);
        //     a5.InitFighter(10000, "02", 20);
        //     RefreshData.Add(a5);
        //     BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventUpdatePlayerRanking,RefreshData);
        // }
        // // 发送信息
        // if(Input.GetKeyDown(KeyCode.C))
        // {
        //     GameNoticeModel.Instance.SendHorseRaceLamp("无敌大帅哥","描述消息:变成狼人 杀光全场 获得胜利");
        // }
        //
        // // bosstis
        // if (Input.GetKeyDown(KeyCode.R))
        // {
        //     List<VictoryRankingInfo> vicList = new List<VictoryRankingInfo>();
        //     
        //     for (int i = 0; i < 10; i++)
        //     {
        //         VictoryRankingInfo victoryRankingInfo = new VictoryRankingInfo();
        //         victoryRankingInfo.Name = "开朗的五六七";
        //         victoryRankingInfo.Order = i + 1;
        //         victoryRankingInfo.Level = 10;
        //         victoryRankingInfo.Species = "狼人";
        //         victoryRankingInfo.CurScore = 100 * (i + 1); 
        //         victoryRankingInfo.WinStreak = 500 * (i + 1);
        //         victoryRankingInfo.MonthlyScore = 5 + i;
        //         vicList.Add(victoryRankingInfo);
        //     }
        //     
        //     BattleUIModel.Instance.OpenVictoryRanking(vicList,VictoryRankingType.SmallAnimals);
        // }
        
        if (Input.GetKeyDown(KeyCode.B))
        {
            // 打开boss来临界面
            //AlertDialogView.Launch(new BossTipsView(),"BossTips","变坏的五六七","",202,UnitType.Boss);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            // 打开boss来临界面
            //AlertDialogView.Launch(new BossTipsView(),"BossTips","变坏的五六七","",202,UnitType.MonsterBoss);
        }
        //
        // if (Input.GetKeyDown(KeyCode.N))
        // {
        //     AlertDialogView.Launch(new BossTipsView(),"BossSkill",5);
        // }
        //
        // if (Input.GetKeyDown(KeyCode.M))
        // {
        //     GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventCancelSkillProgress,_skillProgress-=0.1f);
        // }
        //
        // // 发射边界气泡
        // if(Input.GetKeyDown(KeyCode.Alpha1))
        // {
        //     GameNoticeModel.Instance.SendEdgeBubble("开朗的五六七","击杀得分","10000",EdgeBubbleType.KillScore);
        // }
        // // 发射边界气泡
        // if(Input.GetKeyDown(KeyCode.Alpha2))
        // {
        //     GameNoticeModel.Instance.SendEdgeBubble("开朗的五六七","点赞次数","666",EdgeBubbleType.LikeCount);
        // }
        // // 发射边界气泡
        // if(Input.GetKeyDown(KeyCode.Alpha3))
        // {
        //     GameNoticeModel.Instance.SendEdgeBubble("开朗的五六七","状态","休息",EdgeBubbleType.PlayerState01);
        // }
        // // 发射边界气泡
        // if(Input.GetKeyDown(KeyCode.Alpha4))
        // {
        //     GameNoticeModel.Instance.SendEdgeBubble("开朗的五六七","状态","激进",EdgeBubbleType.PlayerState02);
        // }
        //
        // if (Input.GetKeyDown(KeyCode.T))
        // {
        //     GameNoticeModel.Instance.ShowDangerousTips("变坏的五六七","召唤<color=#FFFA7CFF>小狼人</color>");
        // }
        //
        // if (Input.GetKeyDown(KeyCode.Y))
        // {
        //     GameNoticeModel.Instance.ShowCollectTips("输入“<color=#FFFA7CFF>果实</color>’命令，抢果实啦!");
        // }
    }
}
