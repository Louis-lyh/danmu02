﻿using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Game;

namespace GameHotfix.Game
{
    public class FieldBossStage  : StageBase
    {
        public FieldBossStage() 
            : base(GameStageConst.BattleStage)
        {
        }

        protected override void OnExit()
        {
            // 战斗管理
            BattleManager.Instance.Dispose();
            // 清理化刷新模块
            FruitManager.Instance.Dispose();
            
            // 清理缓存
            HeadImageDownloader.Clear();
            // 关闭弹幕接受
            CommandMgr.Instance.StopCommand();
            // 打开消息界面
            WindowManager.Instance.CloseWindow(UIWindowId.UINoticeWindow);
        }

        protected override async void OnEnter(params object[] args)
        {
            //初始化昼夜管理器时间
            DiurnalVariationmManager.Instance.InitTime(20 * 60, 1, GameConstantHelper.FieldBossTime, GameConstantHelper.ExcessiveSpeed);
            DiurnalVariationmManager.Instance.CreateWallObstacle(BattleType.FieldBoss);
            // 打开战斗界面
            WindowManager.Instance.OpenWindow(UIWindowId.UIBattleWindow);
            BattleManager.Instance.Init(BattleType.FieldBoss);
            
            // 准备战斗
            BattleManager.Instance.ReadyBattle();
            
            // 初始化战斗界面模块
            BattleUIModel.Instance.Init();
            
            // ------ 刷新模块 ------
            // 初始化水果刷新
            FruitManager.Instance.Init();
            
            // 开启弹幕接受
            CommandMgr.Instance.StartCommand();
            
            // 更改地图
            DiurnalVariationmManager.Instance.ChangeMap(2);
            // 打开消息界面
            await WindowManager.Instance.OpenWindow(UIWindowId.UINoticeWindow);
            GameNoticeModel.Instance.Init();
            
            // 加载排行榜
            RankInfo.Instance.LoadRanks();
        }
    }
}