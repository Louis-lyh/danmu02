using GameHotfix.Framework;
using GameHotfix.Game.Utils;
using GameInit.Framework;

namespace GameHotfix.Game
{
    public class HomeStage : StageBase
    {
        public HomeStage()
            : base(GameStageConst.HomeStage)
        {
        }

        protected override void OnEnter(params object[] args)
        {
            WindowManager.Instance.OpenWindow(UIWindowId.UIMainWindow);
            // 初始化头像加载
            HeadImageDownloader.Init();
        }

        protected override void OnExit()
        {
            WindowManager.Instance.CloseWindow(UIWindowId.UIMainWindow);
        }
    }
}