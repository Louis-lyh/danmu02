﻿using System.Collections.Generic;
using GameHotfix.Framework;
using GameInit.Game;

namespace GameHotfix.Game
{
    public class BattleEndStage : StageBase
    {
        public BattleEndStage() 
            : base(GameStageConst.BattleEnd)
        {
        }

        protected override void OnExit()
        {
            
        }

        protected override void OnEnter(params object[] args)
        {
            // 排行榜数据
            List<VictoryRankingInfo> vicList = new List<VictoryRankingInfo>();
            for (int i = 0; i < 10; i++)
            {
                VictoryRankingInfo victoryRankingInfo = new VictoryRankingInfo();
                victoryRankingInfo.Name = "开朗的五六七";
                victoryRankingInfo.Order = i + 1;
                victoryRankingInfo.Level = 10;
                victoryRankingInfo.Species = "狼人";
                victoryRankingInfo.CurScore = 100 * (i + 1); 
                victoryRankingInfo.MonthlyScore = 500 * (i + 1);
                victoryRankingInfo.MonthlyOrder = 5 + i;
                vicList.Add(victoryRankingInfo);
            }
            // 打开排行榜界面
            BattleUIModel.Instance.OpenVictoryRanking(vicList,VictoryRankingType.SmallAnimals);
        }
    }
}