using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public static class GameStageConst
    {
        public static readonly int LoginStage = 2;
        public static readonly int HomeStage = 3;
        public static readonly int BattleStage = 4;
        public static readonly int BattleEnd = 5;
        public static readonly int FieldBoss = 6;
    }
    public static class StageHelper
    {
        public static void InitGameStage()
        {
            StageManager.Instance.RegisterStage(GameStageConst.HomeStage, new HomeStage());
            StageManager.Instance.RegisterStage(GameStageConst.BattleStage, new BattleStage());
            StageManager.Instance.RegisterStage(GameStageConst.BattleEnd,new BattleEndStage());
            StageManager.Instance.RegisterStage(GameStageConst.FieldBoss,new FieldBossStage());
        }
    }
}