using System.Collections.Generic;
using System.Linq;
using Cysharp.Text;
using GameInit.Framework;
using GameInit.Game;

namespace GameHotfix.Game
{
    public class CommandMgr : Framework.Singleton<CommandMgr>
    {
        private bool _startCommand;
        public void Init()
        {
          
        }

        public void StartCommand()
        {
            _startCommand = true;
        }

        public void StopCommand()
        {
            _startCommand = false;
        }

        // 执行指令
        public void CommandExecuted(ServerData.LacEventExecute lacEventExecute)
        {
            ServerManager.Instance.ShowLogText("执行指令 => CommandExecuted");
            
            if(!_startCommand)
                return;
            
            // 记录玩家执行命令
            BattleModel.Instance.AddPlayerExecuteData(lacEventExecute);
            
            switch (lacEventExecute.lcType)
            {
                case ServerData.ELogicActionType.e_comment:
                    ExecutedComment(lacEventExecute); // 执行弹幕
                    break;
                case ServerData.ELogicActionType.e_gift:
                    ExecutedGift(lacEventExecute); // 礼物
                    break;
                case ServerData.ELogicActionType.e_like:
                    ExecuteLike(lacEventExecute); // 点赞
                    // 取消boss技能
                    CancelBossKill(lacEventExecute);
                    break;
            }

        }

        #region 弹幕
        // 执行弹幕
        private void ExecutedComment(ServerData.LacEventExecute lacEventExecute)
        {
            ServerManager.Instance.ShowLogText("执行弹幕 => ExecutedComment");
            // 玩家数据
            var roleData = BattleModel.Instance.GetRoleData(lacEventExecute.userId,false);
            
            switch (lacEventExecute.bType)
            {
                case ServerData.EBarrageType.e_birth:
                    ServerManager.Instance.ShowLogText("弹幕添加玩家 => "+lacEventExecute.userName);
                    AddRole(lacEventExecute);         // 添加玩家
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>加入玩家"));
                    break;
                case ServerData.EBarrageType.e_collect:
                    ChangeTarget(lacEventExecute);    // 切换收集目标
                    roleData?.SetDialogText("采集"); // 气泡框
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>切换收集目标"));
                    break;
                case ServerData.EBarrageType.e_attack:
                    ChangeTarget(lacEventExecute);    // 切换攻击目标
                    roleData?.SetDialogText("打怪"); // 气泡框
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>切换攻击目标"));
                    break;
                case ServerData.EBarrageType.e_position:
                    PositionUnit(lacEventExecute);    // 定位玩家
                    roleData?.SetDialogText("定位"); // 气泡框
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>定位玩家"));
                    break;
                case ServerData.EBarrageType.e_exp:
                    AddExp(lacEventExecute,GameConstantHelper.DanMuExp);    // 增加5点经验
                    roleData?.SetDialogText("666"); // 气泡框
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>666"));
                    break;
                case ServerData.EBarrageType.e_check:
                    QueryPlayerInfo(lacEventExecute);// 查询玩家信息
                    roleData?.SetDialogText("查询"); // 气泡框
                    Logger.LogGreen(ZString.Concat(lacEventExecute.userName, " 指令=>666"));
                    break;
                
            }
        }
        // 添加玩家
        private void AddRole(ServerData.LacEventExecute lacEventExecute,int exp = 0)
        {
            // 用户id
            var userId = lacEventExecute.userId;
            // 用户名
            var userName = lacEventExecute.userName;
            // 公会id
            var guildId = lacEventExecute.guildId;
            // 头像地址
            var headUrl = lacEventExecute.headUrl;
            // 连胜次数
            var winStreak = lacEventExecute.winStreak;
            // 玩家经验
            var heroExp = 0;
            if (lacEventExecute.extension != null && lacEventExecute.extension.ContainsKey(ServerData.ExtensionType.HeroExp.ToString()))
                heroExp = lacEventExecute.extension[ServerData.ExtensionType.HeroExp.ToString()];
            heroExp += exp;
            // 光环经验
            var haloExp = 0;
            if (lacEventExecute.extension != null && lacEventExecute.extension.ContainsKey(ServerData.ExtensionType.HaloExp.ToString()))
                haloExp = lacEventExecute.extension[ServerData.ExtensionType.HaloExp.ToString()];
            // 宠物经验
            var petExp = 0;
            if (lacEventExecute.extension != null && lacEventExecute.extension.ContainsKey(ServerData.ExtensionType.PetExp.ToString()))
                petExp = lacEventExecute.extension[ServerData.ExtensionType.PetExp.ToString()];
            // 击杀数
            var killCount = 0;
            if (lacEventExecute.extension != null && lacEventExecute.extension.ContainsKey(ServerData.ExtensionType.KillCount.ToString()))
                killCount = lacEventExecute.extension[ServerData.ExtensionType.KillCount.ToString()];
            
            // log
            ServerManager.Instance.ShowLogText($"公会id {guildId},经验 => {haloExp} {petExp} , 击杀数 =>{killCount}");
            // 添加玩家
            var isAddComplete = BattleModel.Instance.AddRole(userId,userName,guildId,headUrl,winStreak,haloExp,petExp,heroExp);
            
            ServerManager.Instance.ShowLogText($"在朝歌城诞生啦");
            // 跑马灯通知
            if(isAddComplete)
                GameNoticeModel.Instance.SendHorseRaceLamp(userName,"在朝歌城诞生啦！");
        }
        // 切换攻击目标
        private void ChangeTarget(ServerData.LacEventExecute lacEventExecute)
        {
            // 用户名
            var userId = lacEventExecute.userId;
            switch (lacEventExecute.bType)
            {
                case ServerData.EBarrageType.e_collect:
                    BattleModel.Instance.SetTargetType(userId,UnitType.Fruit);// 采集果实
                    break;
                case ServerData.EBarrageType.e_attack:
                    BattleModel.Instance.SetTargetType(userId,UnitType.Monster);// 攻击怪兽
                    break;
            }
        }
        // 定位目标
        private void PositionUnit(ServerData.LacEventExecute lacEventExecute)
        {
            // 玩家id
            var userId = lacEventExecute.userId;
            // 玩家(不包含boss)
            var roleUnit = BattleModel.Instance.GetRoleUnit(userId,false);
            if (roleUnit == null)
            {
                Logger.LogError($"没有玩家数据 =》 {lacEventExecute.userName}");
                return;
            }
            var unitGameObject = roleUnit.UnitContainerTransform.gameObject;
            if(unitGameObject == null)
                return;
            // 定位
            GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventLocationUnit,unitGameObject);
        }
        
        // 666
        private void AddExp(ServerData.LacEventExecute lacEventExecute,int exp)
        {
            // 玩家id
            var userId = lacEventExecute.userId;
            // 玩家(包含boss)
            var roleUnit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (roleUnit == null)
            {
                Logger.LogError($"没有玩家数据 =》 {lacEventExecute.userName}");
                return;
            }
            roleUnit.AddExp(exp);
        }
        // 查询
        private void QueryPlayerInfo(ServerData.LacEventExecute lacEventExecute)
        {
            // 玩家id
            var userId = lacEventExecute.userId;
            // 玩家(包含boss)
            var roleUnit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (roleUnit == null)
            {
                Logger.LogError($"没有玩家数据 =》 {lacEventExecute.userName}");
                return;
            }
            var roleData = roleUnit.FighterData as RoleData;
            if(roleData == null)
                return;
            
            // 查询
            GameNoticeModel.Instance.DispatchEvent(GameNoticeModel.EventShowQueryPlayerInfo,roleData);
        }


        #endregion

        #region 点赞
        private void ExecuteLike(ServerData.LacEventExecute lacEventExecute)
        {
            // 玩家id
            var userId = lacEventExecute.userId;
            // 玩家数据(包含boss)
            var roleData = BattleModel.Instance.GetRoleData(userId,true);
            if (roleData == null)
            {
                // 点赞加入游戏
                if (GameConstantHelper.OpenLikeAddGame == 1)
                {
                    ServerManager.Instance.ShowLogText("点赞添加玩家 => "+lacEventExecute.userName);
                    AddRole(lacEventExecute, lacEventExecute.likeNum);
                }
                return;
            }
            // 点赞
            roleData.Like(lacEventExecute.likeNum);
        }

        private void CancelBossKill(ServerData.LacEventExecute lacEventExecute)
        {
            // 玩家id
            var userId = lacEventExecute.userId;
            
            // 玩家数据(包含boss)
            var roleData = BattleModel.Instance.GetRoleData(userId,true);
            if (roleData == null)
            {
                Logger.LogError($"没有玩家数据 =》 {lacEventExecute.userName}");
                return;
            }
            
            // 点赞取消boss技能
            BattleUIModel.Instance.DispatchEvent(BattleUIModel.EventCancelSkillProgress,lacEventExecute.likeNum);
        }

        #endregion
        
        #region 礼物
        // 执行礼物
        private void ExecutedGift(ServerData.LacEventExecute lacEventExecute)
        {
            // 用户id
            var userId = lacEventExecute.userId;
            // 用户名
            var userName = lacEventExecute.userName;
            // 礼物数量
            var giftNum = lacEventExecute.giftNum;
            // 头像地址
            var headUrl = lacEventExecute.headUrl;

            switch (lacEventExecute.giftType)
            {
                case ServerData.EGiftType.e_wand: //仙女棒
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6001);
                    // 随机进化欲望
                    ExecuteWand(userId,giftNum,6001);
                    // 显示气泡
                    GameNoticeModel.Instance.SendEdgeBubble(userName, headUrl,
                        "自动点赞", "10秒双倍",EdgeBubbleType.KillScore);
                    if(giftNum >= 66 && BattleManager.Instance.BattleType == BattleType.FieldBoss)
                        BattleModel.Instance.Revive(userId); 
                    break;
                case ServerData.EGiftType.e_magicmirror: //魔方镜
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6002);
                    // 旺盛进化
                    ExecutedMagicmirror(userId,giftNum,6002);
                    // 气泡通知
                    GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"品尝","烧鸡",EdgeBubbleType.PlayerState02);
                    // 召唤boss
                    if(giftNum >= 10 && BattleManager.Instance.BattleType == BattleType.FieldBoss)
                        BattleModel.Instance.CallFieldBoss(0);
                    break;
                case ServerData.EGiftType.e_doughnut: //甜甜圈
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6003);
                    // 自动点赞
                    ExecutedDoughnut(userId,giftNum,6003);
                    // 气泡通知
                    GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"提升","移动速度",EdgeBubbleType.PlayerState02);
                    // 召唤boss
                    if(giftNum >= 10 && BattleManager.Instance.BattleType == BattleType.FieldBoss)
                        BattleModel.Instance.CallFieldBoss(1);
                    break;
                case ServerData.EGiftType.e_energycell: //能量电池
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6004);
                    // 攻击速度
                    ExecutedEnergycell(userId,giftNum,6004);
                    // 气泡通知
                    GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"获得","力量！",EdgeBubbleType.PlayerState01);
                    break;
                case ServerData.EGiftType.e_demonbomb: //恶魔炸弹
                    // 攻击力增加模型变大
                    ExecuteddDmonbomb(userId,giftNum,6005);
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6005);
                    // 气泡通知
                    GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"变身","哪吒！",EdgeBubbleType.PlayerState01);
                    break;
                case ServerData.EGiftType.e_mysteriousairdrop: //神秘空投
                    // 礼物经验
                    ExecuteGiftExp(lacEventExecute, 6006);
                    // 落雷 - boss技能
                    ExecutedMysteriousAirdrop(lacEventExecute,6006);
                    break;
            }
        }
        // 执行礼物经验
        private void ExecuteGiftExp(ServerData.LacEventExecute lacEventExecute,int giftId)
        {
            // 用户id
            var userId = lacEventExecute.userId;
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);
            // 礼物数量
            var giftNum = lacEventExecute.giftNum;
            
            // 玩家(包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null)
            {
                ServerManager.Instance.ShowLogText("礼物添加玩家 => "+lacEventExecute.userName);
                AddRole(lacEventExecute, giftDef.Exp * giftNum);
                return;
            }
            
            // 增加经验
            unit.AddExp(giftDef.Exp * giftNum);
            // 增加玩家光环经验
            (unit as FighterUnitRole)?.AddHaloExp(giftDef.ExpHalo * giftNum);
            // 增加玩家宠物经验
            (unit as FighterUnitRole)?.AddPetExp(giftDef.ExpPet * giftNum);
            
            // 增加boss光环经验
            (unit as FighterUnitBoss)?.AddHaloExp(giftDef.ExpHalo * giftNum);
            // 增加boss宠物经验
            (unit as FighterUnitBoss)?.AddPetExp(giftDef.ExpPet * giftNum);
        }
        
        // 执行仙女榜
        private void ExecuteWand(string userId,int giftNum,int giftId)
        {
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);

            // 玩家(不包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null || unit.IsDeath)
                return;
            
            // 礼物特效
            //GiftEffectManager.Instance.PlayGiftEff(1);

            // buff id 字符串
            var buffIdStr = unit.UnitType == UnitType.Boss ? giftDef.BossBuffId : giftDef.BuffId;
            
            // 为空退出
            if(string.IsNullOrEmpty(buffIdStr))
                return;

            // 执行buff
            ExecuteBuff(buffIdStr, giftNum, unit.FighterData, unit.FighterData);
        }

        // 执行魔方镜（旺盛进化欲望）
        private void ExecutedMagicmirror(string userId,int giftNum,int giftId)
        {
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);
            
            // 玩家(不包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null || unit.IsDeath)
                return;

            // 礼物特效
            GiftEffectManager.Instance.PlayGiftEff(2);
            
            // buff id 字符串
            var buffIdStr = unit.UnitType == UnitType.Boss ? giftDef.BossBuffId : giftDef.BuffId;
            // 为空退出
            if(string.IsNullOrEmpty(buffIdStr))
                return;
            
            // 执行buff
            ExecuteBuff(buffIdStr, giftNum, unit.FighterData, unit.FighterData);
        }
        // 执行甜甜圈（自动点赞）
        private void ExecutedDoughnut(string userId,int giftNum,int giftId)
        {
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);

            // 玩家(包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null || unit.IsDeath)
                return;
            
            // 礼物特效
            GiftEffectManager.Instance.PlayGiftEff(3);
            
            // buff id 字符串
            var buffIdStr = unit.UnitType == UnitType.Boss ? giftDef.BossBuffId : giftDef.BuffId;
            // 为空退出
            if(string.IsNullOrEmpty(buffIdStr))
                return;
            
            // 执行buff
            ExecuteBuff(buffIdStr, giftNum, unit.FighterData, unit.FighterData);
        }
        // 执行能量电池(攻击速度)
        private void ExecutedEnergycell(string userId,int giftNum,int giftId)
        {
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);

            // 玩家(包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null || unit.IsDeath)
                return;
            
            // 礼物特效
            GiftEffectManager.Instance.PlayGiftEff(4);
            
            // buff id 字符串
            var buffIdStr = unit.UnitType == UnitType.Boss ? giftDef.BossBuffId : giftDef.BuffId;
            // 为空退出
            if(string.IsNullOrEmpty(buffIdStr))
                return;
            
            // 执行buff
            ExecuteBuff(buffIdStr, giftNum, unit.FighterData, unit.FighterData);
        }
        
        // 执行恶魔炸弹(攻击力、模型变大)
        private void ExecuteddDmonbomb(string userId,int giftNum,int giftId)
        {
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);

            // 玩家(包含boss)
            var unit = BattleModel.Instance.GetRoleUnit(userId,true);
            if (unit == null || unit.IsDeath)
                return;
            
            // 礼物特效
            if(unit.UnitType != UnitType.Boss)
                GiftEffectManager.Instance.PlayGiftEff(5);
            
            // buff id 字符串
            var buffIdStr = unit.UnitType == UnitType.Boss ? giftDef.BossBuffId : giftDef.BuffId;
            // 为空退出
            if(string.IsNullOrEmpty(buffIdStr))
                return;
            
            // 执行buff
            ExecuteBuff(buffIdStr, giftNum, unit.FighterData, unit.FighterData);
        }
        
        // 执行神秘空投（落雷，狼人技能）
        private void ExecutedMysteriousAirdrop(ServerData.LacEventExecute lacEventExecute,int giftId)
        {
            // 用户id
            var userId = lacEventExecute.userId;
            // 用户名
            var userName = lacEventExecute.userName;
            // 礼物数量
            var giftNum = lacEventExecute.giftNum;
            // 头像地址
            var headUrl = lacEventExecute.headUrl;
            
            // 礼物配置
            var giftDef = GameInit.Game.GiftConfig.Get(giftId);
            // 玩家(包含boss)
            var player = BattleModel.Instance.GetRoleUnit(userId,true);
            
            if(player == null || player.IsDeath)
                return;
            
            if (player.UnitType == UnitType.Role)
            {
                // 礼物特效
                GiftEffectManager.Instance.PlayGiftEff(6);

                // 执行buff
                ExecuteBuff(giftDef.BuffId, giftNum, player.FighterData, player.FighterData);
               
                // 气泡通知
                GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"变身","二郎神！",EdgeBubbleType.PlayerState01);
            }
            else if(player.UnitType == UnitType.Boss)
            {
                // // 小狼人技能
                // var littleWerewolfSkillCount = giftNum % 5;
                // if (littleWerewolfSkillCount > 0)
                // {
                //     // 礼物特效
                //     //GiftEffectManager.Instance.PlayGiftEff(7);
                //     // 边界气泡
                //     GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"boss技能","召唤小怪！",EdgeBubbleType.PlayerState01);
                //     BattleModel.Instance.LittleWerewolfSkills(littleWerewolfSkillCount);
                //     // 显示小狼人
                //     GameNoticeModel.Instance.ShowDangerousTips(userName,"召唤了<color=#EAE678FF>小怪</color>，" +
                //                                                         "输入“<color=#00CFFFFF>打怪</color>“攻击小怪");
                // }
                
                // 声波技能
                var werewolfSonicSkillCount = giftNum / 5;
                if (werewolfSonicSkillCount > 0)
                {
                    // 礼物特效
                    //GiftEffectManager.Instance.PlayGiftEff(8);
                    GameNoticeModel.Instance.SendEdgeBubble(userName,headUrl,"boss技能","声波技能！",EdgeBubbleType.PlayerState01);
                    BattleModel.Instance.WerewolfSonicSkills(werewolfSonicSkillCount,100022);
                }
                
                // buff id 字符串
                var buffIdStr = giftDef.BossBuffId;
                // 为空退出
                if(string.IsNullOrEmpty(buffIdStr))
                    return;
                // 执行buff
                ExecuteBuff(buffIdStr, giftNum % 5, player.FighterData, player.FighterData);
            }
        }
        
        // 执行buff
        private void ExecuteBuff(string buffIdStr,int giftNum,FighterUnitData source,FighterUnitData target)
        {
            // buff id
            var buffDic = GameConstantHelper.StringToIntDic(buffIdStr);
            var buffDicKey = buffDic.Keys.ToList();
            
            for (int i = 0; i < buffDicKey.Count; i++)
            {
                var id = buffDicKey[i];
                var count = giftNum / buffDic[id];
                
                if(count > 0)
                    // buff 作用时间叠加 做用值叠加
                    BattleModel.Instance.AddBuff(id,source,target, count,count);
            }
        }

        #endregion
        
    }
}