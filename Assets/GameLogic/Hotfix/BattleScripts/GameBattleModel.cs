﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using Cysharp.Text;
using GameHotfix.Framework;
using GameInit.Framework;
using GameInit.Game;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using UnityEngine;
using BaseEvent = GameInit.Game.BaseEvent;
using Logger = GameInit.Framework.Logger;
using Random = UnityEngine.Random;

namespace GameHotfix.Game
{
    public partial class GameBattleModel : Framework.DataModelBase<GameBattleModel>
    {
        // 排行榜
        private DialogGameRankingView _gameRankingView;
        public void Init()
        {
            // 注册
            AddEvent();
            // 局内排行榜
            _gameRankingView = new DialogGameRankingView();
        }
        // 注册事件
        private void AddEvent()
        {
            // 打开boss界面
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventOpenBossTips,OpenBossTips);
            // 打开boss技能界面
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventOpenBossSkill,OpenBossSkill);
            // 打开排行榜
            BattleUIModel.Instance.AddEventListener(BattleUIModel.EventShowVictoryRanking,ShowVictoryRanking);
            // 夜晚结束
            BattleModel.Instance.AddEventListener(BattleModel.EventNightEnd,NightEnd);
            // 游戏结束
            BattleModel.Instance.AddEventListener(BattleModel.EventBattleEnd,BattleEnd);
        }
        // 注销界面
        private void RemoveEvent()
        {
            // 打开boss界面
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventOpenBossTips,OpenBossTips);
            // 打开boss技能界面
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventOpenBossSkill,OpenBossSkill);
            // 打开排行榜
            BattleUIModel.Instance.RemoveEventListener(BattleUIModel.EventShowVictoryRanking,ShowVictoryRanking);
            // 夜晚结束
            BattleModel.Instance.RemoveEventListener(BattleModel.EventNightEnd,NightEnd);
            // 游戏结束
            BattleModel.Instance.RemoveEventListener(BattleModel.EventBattleEnd,BattleEnd);
        }

        #region boss提示界面
        // 打开boss界面
        private void OpenBossTips(BaseEvent baseEvent)
        {
            // 提示玩家boss来临
            if (baseEvent.EventObj is RoleData)
            {
                var roleData = (RoleData) baseEvent.EventObj;
                AlertDialogView.Launch(new BossTipsView(),"BossTips",roleData.Name,roleData.HeadUrl,roleData.RoleDef.ResId,UnitType.Boss);
            }
            // 提示怪兽boss来临
            else if(baseEvent.EventObj is GameInit.Game.RoleDef)
            {
                var roleDef = (GameInit.Game.RoleDef) baseEvent.EventObj;
                AlertDialogView.Launch(new BossTipsView(),"BossTips",roleDef.SpeciesName,"",roleDef.ResId,UnitType.MonsterBoss);
            }
        }
        
        // 打开boss界面
        private void OpenBossSkill(BaseEvent baseEvent)
        {
            // 技能id
            var skillId = (int) baseEvent.EventObj;
            // 技能时间
            var skillTIme = GameConstantHelper.CancelBossSkillTime;
            // 需要的点赞数
            var likeCount = GameConstantHelper.CancelBossSkillLikeMultiple *
                            BattleManager.Instance.GetFighterCount(UnitType.Role);
            // 资源id
            var resId = BattleManager.Instance.BossUnit.FighterData.RoleDef.ResId;
            // 打开boss技能界面
            AlertDialogView.Launch(new BossTipsView(),"BossSkill",skillTIme,likeCount,skillId,resId);
        }
        

        #endregion
        
       

        #region 游戏结算
        // 打开排行榜
        private void ShowVictoryRanking(BaseEvent baseEvent)
        {
            VictoryInfo info = (VictoryInfo) baseEvent.EventObj;
            AlertDialogView.Launch(new VictoryRankingView(),info.VictoryRankingInfos,info.VictoryRankingType);
        }
        // 游戏结束
        private void BattleEnd(BaseEvent baseEvent)
        {
            // 结束
            DiurnalVariationmManager.Instance.BattleEnd();
            
            // 前十分成积分池数据
            BattleModel.Instance.TopTenAddScore();
            // 胜利阵营
            UnitType unitType = (UnitType)baseEvent.EventObj;

            // 打开排行榜界面
            var victoryRankingType = unitType == UnitType.Boss
                ? VictoryRankingType.Werewolf
                : VictoryRankingType.SmallAnimals;
            
            // 编辑器
            if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                // 排行榜数据
                var vicList = RankingData(TestData().rankList);
                // 打开排行榜界面
                BattleUIModel.Instance.OpenVictoryRanking(vicList,victoryRankingType);
                Logger.LogBlue("打开排行榜界面:"+Application.platform);
            }
            else
            {
                // 像服务器发送的数据
                var sendData = SendServerData(unitType);
                // 发送结束本轮游戏请求-成功后才弹出排行榜
                ServerManager.Instance.StopGame(sendData, (ServerData.StopGameData data) =>
                {
                    // 排行榜数据
                    var vicList = RankingData(data.rankList);
                   
                    // 打开排行榜界面
                    BattleUIModel.Instance.OpenVictoryRanking(vicList,victoryRankingType);
                });
            }
        }

        // 夜晚阶段结束
        private void NightEnd(BaseEvent baseEvent)
        {
            // 是否击杀boss
            var isKillBoss = (bool) baseEvent.EventObj;
            // 重置夜晚
            DiurnalVariationmManager.Instance.Restart(isKillBoss);
            // 清楚boss礼物技能
            BattleModel.Instance.ClearBossGiftSkill();
            // 更新服务器数据
            SetExtension();
        }
        
        // 更新玩家宠物数据、光环数据
        private void SetExtension()
        {
            // 玩家数据
            var roleDatas = BattleModel.Instance.GetAllRoleData();
            // 数据
            var serverData = new ServerData.ExtensionData();
            serverData.type = 3;
            serverData.extensions = new Dictionary<string, Dictionary<string, int>>();
            for (int i = 0; i < roleDatas.Count; i++)
            {
                var roleData = roleDatas[i];
                var expDic = new Dictionary<string,int>();
                expDic.Add(ServerData.ExtensionType.HaloExp.ToString(),roleData.HaloExp);
                expDic.Add(ServerData.ExtensionType.PetExp.ToString(),roleData.PetExp);
                expDic.Add(ServerData.ExtensionType.HeroExp.ToString(),roleData.Exp);
                serverData.extensions.Add(roleData.UserId,expDic);
            }
            
            // 上传扩展数据
            ServerManager.Instance.SetExtension(serverData, () =>
            {
                TimerHeap.AddTimer(1000, 0, () =>
                {
                    // 上传击杀数据
                    SetExtensionKill();
                });
            });
            
        }
        // 上传击杀数
        private void SetExtensionKill()
        {
            // 玩家数据
            var roleDatas = BattleModel.Instance.GetAllRoleData();
            // 数据
            var serverData = new ServerData.ExtensionData();
            serverData.type = 2;
            serverData.extensions = new Dictionary<string, Dictionary<string, int>>();
            for (int i = 0; i < roleDatas.Count; i++)
            {
                var roleData = roleDatas[i];
                var expDic = new Dictionary<string,int>();
                expDic.Add(ServerData.ExtensionType.KillCount.ToString(),roleData.KillRoleCount);
                serverData.extensions.Add(roleData.UserId,expDic);
                // 重置击杀数
                roleData.ResetKillRoleCount();
            }
            
            // 上传扩展数据
            ServerManager.Instance.SetExtension(serverData,null);
        }
        
        /// <summary>
        /// 获取扩展数据
        /// </summary>
        public void GetExtension(Action<Dictionary<string,Dictionary<string,int>>> callback)
        {
            // 玩家数据
            var roleList = BattleManager.Instance.AllRole(true);
            List<string> userIdList = new List<string>();
            for (int i = 0; i < roleList.Count; i++)
            {
                userIdList.Add(roleList[i].RoleData.UserId);
            }
            // 获取
            ServerManager.Instance.GetExtension(userIdList.ToArray(), callback);
        }


        // 排行数据
        private List<VictoryRankingInfo> RankingData(List<ServerData.Settlement> stopGameData = null)
        {
            // 所有玩家分数
            var allRole = BattleModel.Instance.GetAllRoleData();
            // 排序玩家分数
            allRole.Sort((a, b) =>
                {
                    if (b.KillScore > a.KillScore)
                        return 1;
                    else if (b.KillScore < a.KillScore)
                        return -1;
                    else
                        return 0;
                }
            );
            if (allRole.Count > 10)
                allRole = allRole.GetRange(0, 10);
            
            // 排序月排行分数
            if(stopGameData != null)
                stopGameData.Sort((a, b) =>
                {
                    if (b.monthlyScore > a.monthlyScore)
                        return 1;
                    else if (b.monthlyScore < a.monthlyScore)
                        return -1;
                    else
                        return 0;
                });
            
            // 排行榜数据
            List<VictoryRankingInfo> vicList = new List<VictoryRankingInfo>();
            for (int i = 0; i < allRole.Count; i++)
            {
                var roleData = allRole[i];
                
                // 找到对应服务器数据
                ServerData.Settlement stopGameDataItem = FindSettlement(stopGameData,roleData);

                VictoryRankingInfo victoryRankingInfo = new VictoryRankingInfo();
                victoryRankingInfo.Name = roleData.Name;    // 名字
                victoryRankingInfo.HeadUrl = roleData.HeadUrl;    // 头像
                victoryRankingInfo.Order = i + 1;    // 排名
                victoryRankingInfo.Level = roleData.Level;    // 等级
                victoryRankingInfo.Species = roleData.RoleDef.SpeciesName;    // 动物名
                victoryRankingInfo.CurScore = roleData.KillScore; // 击杀分数
                victoryRankingInfo.MonthlyScore = stopGameDataItem == null ? 0 : stopGameDataItem.monthlyScore;    // 月分数
                victoryRankingInfo.MonthlyOrder = stopGameDataItem == null ? 0 : stopGameDataItem.rank;    // 月排名
                victoryRankingInfo.WinStreak = stopGameDataItem == null ? 0 : stopGameDataItem.winStreak; // 连胜
                vicList.Add(victoryRankingInfo);
            }
            
            return vicList;
        }
        
        // 找到对应服务器数据
        private ServerData.Settlement FindSettlement(List<ServerData.Settlement> stopGameData ,RoleData roleData)
        {
            ServerData.Settlement stopGameDataItem = null;
            if (stopGameData != null )
            {
                stopGameDataItem = stopGameData.Find((item) =>
                {
                    // id相同
                    var isFind = item.userId.Equals(roleData.UserId);
                    
                    // 阵营相同
                    if (item.camp == 1)
                        isFind = isFind && roleData.UnitType == UnitType.Boss;
                    else if (item.camp == 0)
                        isFind = isFind && roleData.UnitType != UnitType.Boss;
                    
                    return isFind;
                });
            }

            return stopGameDataItem;
        }

        // 像服务器发送的数据
        private ServerData.StopGameSendData SendServerData(UnitType unitType)
        {
            // 向服务器发送本轮游戏结束
            ServerData.StopGameSendData sendData = new ServerData.StopGameSendData();

            if (unitType == UnitType.Boss) 
                sendData.winCamp = 1;
            else 
                sendData.winCamp = 0;
            // 积分
            sendData.scores = new Dictionary<string, long>();
            // 阵营
            sendData.camps = new Dictionary<string, int>();
            // 扩展数据
            sendData.extensions = new Dictionary<string, Dictionary<string,int>>();
            // 得到当局玩家
            var roleList = BattleModel.Instance.GetAllRoleData();
            
            for (int i = 0; i < roleList.Count; i++)
            {
                var role = roleList[i];
                if (sendData.scores.ContainsKey(role.UserId))
                {
                    Logger.LogWarning($"重复数据 ：{role.UserId} {role.Name} ");
                    continue;
                }

                //积分
                sendData.scores.Add(role.UserId, role.KillScore);
                //阵营
                sendData.camps.Add(role.UserId, role.UnitType == UnitType.Boss ? 1 : 0);
                // 扩展数据
                var extension = new Dictionary<string, int>();
                extension.Add(ServerData.ExtensionType.HaloExp.ToString(),role.HaloExp);
                extension.Add(ServerData.ExtensionType.PetExp.ToString(),role.PetExp);
                extension.Add(ServerData.ExtensionType.HeroExp.ToString(),role.Exp);
                
                sendData.extensions.Add(role.UserId,extension);
            }

            return sendData;
        }
        
        //
        private int testId = 1;
        private int _number = 1;
        private ServerData.StopGameData TestData()
        {
            var stopGameData = new ServerData.StopGameData();
            stopGameData.roundId = "1";
            stopGameData.rankList = new List<ServerData.Settlement>();
            
            for (int i = 0; i < 20; i++)
            {
                //得到配置
                TestUserDef def = TestUserConfig.Get(testId);
                if(def == null)
                    continue;
                //得到userId
                string userId = ZString.Concat(def.UserId, _number);
                //得到userName
                string userName = ZString.Concat(def.UserName, _number);
                
                ServerData.Settlement item = new ServerData.Settlement();

                item.userId = userId;
                item.userName = userName;
                item.camp = 0;
                item.score = 20 + Random.Range(0,1000);
                item.monthlyScore = 999999 + Random.Range(0,1000);
                item.winStreak = 999;
                item.rank = 66;
                
                stopGameData.rankList.Add(item);

                testId++;
            }
            
            return stopGameData;
        }
        
        #endregion
        

        public void Destroy()
        {
            // 注销
            RemoveEvent();
        }

        // 销毁
        public override void Dispose()
        {
            // 注销
            RemoveEvent();
            base.Dispose();
        }
    }
}