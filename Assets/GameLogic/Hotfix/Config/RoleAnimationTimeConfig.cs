﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class RoleAnimationTimeConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            RoleAnimationTimeDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new RoleAnimationTimeDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.RoleID, def);
            }
        }
        
        /// <summary>
        /// 通过RoleID获取RoleAnimationTimeDef的实例
        /// </summary>
        /// <param name="RoleID">索引</param>
        public static RoleAnimationTimeDef Get(int RoleID)
        {
            _defDic.TryGetValue(RoleID, out RoleAnimationTimeDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, RoleAnimationTimeDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, RoleAnimationTimeDef> _defDic = new Dictionary<int, RoleAnimationTimeDef>();
    }
    
}
