﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class RoleResConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            RoleResDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new RoleResDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取RoleResDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static RoleResDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out RoleResDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, RoleResDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, RoleResDef> _defDic = new Dictionary<int, RoleResDef>();
    }
    
}
