﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class PetConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            PetDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new PetDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取PetDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static PetDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out PetDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, PetDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, PetDef> _defDic = new Dictionary<int, PetDef>();
    }
    
}
