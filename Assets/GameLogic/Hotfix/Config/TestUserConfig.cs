﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class TestUserConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            TestUserDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new TestUserDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取TestUserDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static TestUserDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out TestUserDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, TestUserDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, TestUserDef> _defDic = new Dictionary<int, TestUserDef>();
    }
    
}
