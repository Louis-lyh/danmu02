﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class EffectDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 特效名字
        /// </summary>
        public string EffectName;

        /// <summary>
        /// 特效时长（秒）
        /// </summary>
        public float Time;

        /// <summary>
        /// 是否为循环特效
        /// </summary>
        public int Loop;

        /// <summary>
        /// 特效位置类型
        /// </summary>
        public int PosType;

        /// <summary>
        /// 特效初始大小
        /// </summary>
        public float PosScale;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			EffectName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			Time = ByteUtil.ReadFloat(bytes,ref byteIndex);
			Loop = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			PosType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			PosScale = ByteUtil.ReadFloat(bytes,ref byteIndex);

		}
    }
    
}
