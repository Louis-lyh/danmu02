﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class TestUserDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 用户编号
        /// </summary>
        public string UserId;

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string UserName;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			UserId = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			UserName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
    }
    
}
