﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class RoleDef
    {
        /// <summary>
        /// 索引ID
        /// </summary>
        public int ID;

        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID;

        /// <summary>
        /// 物种名
        /// </summary>
        public string SpeciesName;

        /// <summary>
        /// 角色等级
        /// </summary>
        public uint Level;

        /// <summary>
        /// 角色类型
        /// </summary>
        public uint RoleType;

        /// <summary>
        /// 阵营
        /// </summary>
        public uint Camp;

        /// <summary>
        /// 资源
        /// </summary>
        public int ResId;

        /// <summary>
        /// 资源类型
        /// </summary>
        public uint ResModelType;

        /// <summary>
        /// 模型大小
        /// </summary>
        public float ModelScale;

        /// <summary>
        /// 是否为小怪
        /// </summary>
        public int SmallMonster;

        /// <summary>
        /// 出生动画时长
        /// </summary>
        public float ComingTime;

        /// <summary>
        /// 出生特效
        /// </summary>
        public int ComingEffect;

        /// <summary>
        /// 寻路层级
        /// </summary>
        public int RVOLayer;

        /// <summary>
        /// 生命
        /// </summary>
        public int Hp;

        /// <summary>
        /// 攻击力
        /// </summary>
        public int Attack;

        /// <summary>
        /// 暴击率
        /// </summary>
        public int CriticalRate;

        /// <summary>
        /// 暴击伤害
        /// </summary>
        public int CriticalDamage;

        /// <summary>
        /// 移速
        /// </summary>
        public float MoveSpeed;

        /// <summary>
        /// 攻速
        /// </summary>
        public float AttackSpeed;

        /// <summary>
        /// 视野范围
        /// </summary>
        public float SearchRange;

        /// <summary>
        /// 当前等级经验上限
        /// </summary>
        public int ExpMax;

        /// <summary>
        /// 上一级经验上限
        /// </summary>
        public int LastExpMax;

        /// <summary>
        /// 击杀经验奖励
        /// </summary>
        public int ExpReward;

        /// <summary>
        /// 击杀光环经验奖励
        /// </summary>
        public int ExpHalo;

        /// <summary>
        /// 宠物经验
        /// </summary>
        public int ExpPet;

        /// <summary>
        /// 击杀积分奖励
        /// </summary>
        public int ScoreReward;

        /// <summary>
        /// 技能配置
        /// </summary>
        public string SkillConfig;

        /// <summary>
        /// 名字高度
        /// </summary>
        public float NameHeight;

        /// <summary>
        /// 伤害数字高度
        /// </summary>
        public float HurtNumberH;

        /// <summary>
        /// 伤害数字大小
        /// </summary>
        public float HurtNumberS;

        /// <summary>
        /// 掉落配置
        /// </summary>
        public int DropEwwaedId;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			RoleID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			SpeciesName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			Level = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			RoleType = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			Camp = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			ResId = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ResModelType = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			ModelScale = ByteUtil.ReadFloat(bytes,ref byteIndex);
			SmallMonster = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ComingTime = ByteUtil.ReadFloat(bytes,ref byteIndex);
			ComingEffect = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			RVOLayer = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Hp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Attack = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			CriticalRate = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			CriticalDamage = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			MoveSpeed = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackSpeed = ByteUtil.ReadFloat(bytes,ref byteIndex);
			SearchRange = ByteUtil.ReadFloat(bytes,ref byteIndex);
			ExpMax = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			LastExpMax = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ExpReward = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ExpHalo = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ExpPet = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ScoreReward = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			SkillConfig = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			NameHeight = ByteUtil.ReadFloat(bytes,ref byteIndex);
			HurtNumberH = ByteUtil.ReadFloat(bytes,ref byteIndex);
			HurtNumberS = ByteUtil.ReadFloat(bytes,ref byteIndex);
			DropEwwaedId = ByteUtil.ReadSignedInt(bytes,ref byteIndex);

		}
    }
    
}
