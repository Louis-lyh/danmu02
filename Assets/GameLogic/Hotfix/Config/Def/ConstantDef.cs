﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class ConstantDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 说明
        /// </summary>
        public string Explain;

        /// <summary>
        /// 字段名
        /// </summary>
        public string FieldName;

        /// <summary>
        /// 数值
        /// </summary>
        public string Value;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Explain = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			FieldName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			Value = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
    }
    
}
