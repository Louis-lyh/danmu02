﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class DropRewardDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 掉落倍数
        /// </summary>
        public int DropMultiple;

        /// <summary>
        /// 掉落数量范围
        /// </summary>
        public string DropRange;

        /// <summary>
        /// 掉落种类概率
        /// </summary>
        public string DropOdds;

        /// <summary>
        /// 掉落半径范围
        /// </summary>
        public string DropRadius;

        /// <summary>
        /// 飞向击杀单位
        /// </summary>
        public bool FlyingKiller;

        /// <summary>
        /// 额外飞行动画
        /// </summary>
        public string FlyingAnimation;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			DropMultiple = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			DropRange = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			DropOdds = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			DropRadius = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			FlyingKiller = ByteUtil.ReadBoolean(bytes,ref byteIndex);
			FlyingAnimation = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);

		}
    }
    
}
