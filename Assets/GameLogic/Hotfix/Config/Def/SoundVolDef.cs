﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class SoundVolDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 音效名字
        /// </summary>
        public string SoundName;

        /// <summary>
        /// 音量
        /// </summary>
        public float Volume;

        /// <summary>
        /// 延时播放时间
        /// </summary>
        public float DelayTime;

        /// <summary>
        /// 队列播放上限
        /// </summary>
        public int QueueLimit;

        /// <summary>
        /// 是否开启淡入
        /// </summary>
        public int FadeIn;

        /// <summary>
        /// 淡入时间
        /// </summary>
        public float FadeInTime;

        /// <summary>
        /// 是否开启淡出
        /// </summary>
        public int FadeOut;

        /// <summary>
        /// 淡出时间
        /// </summary>
        public float FadeOutTime;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			SoundName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			Volume = ByteUtil.ReadFloat(bytes,ref byteIndex);
			DelayTime = ByteUtil.ReadFloat(bytes,ref byteIndex);
			QueueLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			FadeIn = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			FadeInTime = ByteUtil.ReadFloat(bytes,ref byteIndex);
			FadeOut = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			FadeOutTime = ByteUtil.ReadFloat(bytes,ref byteIndex);

		}
    }
    
}
