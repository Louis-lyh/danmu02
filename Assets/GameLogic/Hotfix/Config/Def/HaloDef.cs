﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class HaloDef
    {
        /// <summary>
        /// 编号
        /// </summary>
        public int ID;

        /// <summary>
        /// 预制体
        /// </summary>
        public string ResHaloModel;

        /// <summary>
        /// 光环名
        /// </summary>
        public string HaloName;

        /// <summary>
        /// 经验上限
        /// </summary>
        public int ExpLimit;

        /// <summary>
        /// 上一级经验
        /// </summary>
        public int PreExpLimit;

        /// <summary>
        /// 增加攻击力
        /// </summary>
        public int HaloAttack;

        /// <summary>
        /// 增加移速
        /// </summary>
        public float HaloSpeed;

        /// <summary>
        /// 增加攻击系数
        /// </summary>
        public int AttackBonus;

        /// <summary>
        /// 增加移速加成
        /// </summary>
        public int SpeedBonus;

        /// <summary>
        /// 增加生命
        /// </summary>
        public int HaloHp;

        /// <summary>
        /// 增生命值加成
        /// </summary>
        public int HpBonus;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ResHaloModel = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			HaloName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			ExpLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			PreExpLimit = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			HaloAttack = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			HaloSpeed = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			SpeedBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			HaloHp = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			HpBonus = ByteUtil.ReadSignedInt(bytes,ref byteIndex);

		}
    }
    
}
