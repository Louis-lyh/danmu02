﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class RoleAnimationTimeDef
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public int RoleID;

        /// <summary>
        /// 角色名
        /// </summary>
        public string RoleName;

        /// <summary>
        /// 死亡结束时间(秒)
        /// </summary>
        public float DeathEnd;

        /// <summary>
        /// 受击结束时间（秒）
        /// </summary>
        public float DamageEnd;

        /// <summary>
        /// 攻击1触发伤害时间(秒)
        /// </summary>
        public float AttackToDamage1;

        /// <summary>
        /// 攻击1触发结束时间(秒)
        /// </summary>
        public float AttackEnd1;

        /// <summary>
        /// 攻击2触发伤害时间(秒)
        /// </summary>
        public float AttackToDamage2;

        /// <summary>
        /// 攻击2触发结束时间(秒)
        /// </summary>
        public float AttackEnd2;

        /// <summary>
        /// 攻击3触发伤害时间(秒)
        /// </summary>
        public float AttackToDamage3;

        /// <summary>
        /// 攻击3触发结束时间(秒)
        /// </summary>
        public float AttackEnd3;

        /// <summary>
        /// 攻击4触发伤害时间(秒)
        /// </summary>
        public float AttackToDamage4;

        /// <summary>
        /// 攻击4触发结束时间(秒)
        /// </summary>
        public float AttackEnd4;

        /// <summary>
        /// 攻击4触发伤害时间(秒)
        /// </summary>
        public float AttackToDamage5;

        /// <summary>
        /// 攻击4触发结束时间(秒)
        /// </summary>
        public float AttackEnd5;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			RoleID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			RoleName = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			DeathEnd = ByteUtil.ReadFloat(bytes,ref byteIndex);
			DamageEnd = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackToDamage1 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackEnd1 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackToDamage2 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackEnd2 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackToDamage3 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackEnd3 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackToDamage4 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackEnd4 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackToDamage5 = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackEnd5 = ByteUtil.ReadFloat(bytes,ref byteIndex);

		}
    }
    
}
