﻿using GameInit.Framework;

namespace GameHotfix.Game
{

    public class SkillDef
    {
        /// <summary>
        /// 技能ID
        /// </summary>
        public int ID;

        /// <summary>
        /// 技能名字
        /// </summary>
        public string Name;

        /// <summary>
        /// 技能图标
        /// </summary>
        public string Icon;

        /// <summary>
        /// 技能执行逻辑类型
        /// </summary>
        public int SkillType;

        /// <summary>
        /// 技能释放类型
        /// </summary>
        public uint CastType;

        /// <summary>
        /// 生命值百分比伤害
        /// </summary>
        public float DamageHpRatio;

        /// <summary>
        /// 伤害值
        /// </summary>
        public int DamageValue;

        /// <summary>
        /// 伤害系数
        /// </summary>
        public float DamageRatio;

        /// <summary>
        /// 冷却时间
        /// </summary>
        public float SkillCD;

        /// <summary>
        /// 攻击距离
        /// </summary>
        public float AttackDistance;

        /// <summary>
        /// 伤害类型
        /// </summary>
        public uint DamageType;

        /// <summary>
        /// 伤害半径
        /// </summary>
        public float DamageRadius;

        /// <summary>
        /// AOE角度
        /// </summary>
        public float AoeAngle;

        /// <summary>
        /// 击退距离（0表示不击退）
        /// </summary>
        public float BeatBack;

        /// <summary>
        /// 击飞距离（0表示不击飞）
        /// </summary>
        public float BlowUp;

        /// <summary>
        /// 释放前特效
        /// </summary>
        public string PreEffect;

        /// <summary>
        /// 命中目标时的特效
        /// </summary>
        public string TargetEffect;

        /// <summary>
        /// 子弹资源
        /// </summary>
        public string BulletRes;

        /// <summary>
        /// 子弹数量
        /// </summary>
        public int BulletCount;

        /// <summary>
        /// 子弹半径
        /// </summary>
        public float BulletRadius;

        /// <summary>
        /// 子弹飞行速度
        /// </summary>
        public float BulletSpeed;

        /// <summary>
        /// 子弹飞行最远距离
        /// </summary>
        public float BulletMaxDistance;

        /// <summary>
        /// 子弹击中位置
        /// </summary>
        public int  BulletHitPosType;

        /// <summary>
        /// 动画类型
        /// </summary>
        public int AnimationType;

        /// <summary>
        /// BuffId
        /// </summary>
        public int BuffId;

        /// <summary>
        /// Shake
        /// </summary>
        public int ShakeId;

    

        public void Parse(byte[] bytes, ref int byteIndex)
		{
			ID = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			Name = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			Icon = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			SkillType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			CastType = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			DamageHpRatio = ByteUtil.ReadFloat(bytes,ref byteIndex);
			DamageValue = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			DamageRatio = ByteUtil.ReadFloat(bytes,ref byteIndex);
			SkillCD = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AttackDistance = ByteUtil.ReadFloat(bytes,ref byteIndex);
			DamageType = ByteUtil.ReadUnsignedInt(bytes,ref byteIndex);
			DamageRadius = ByteUtil.ReadFloat(bytes,ref byteIndex);
			AoeAngle = ByteUtil.ReadFloat(bytes,ref byteIndex);
			BeatBack = ByteUtil.ReadFloat(bytes,ref byteIndex);
			BlowUp = ByteUtil.ReadFloat(bytes,ref byteIndex);
			PreEffect = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			TargetEffect = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			BulletRes = ByteUtil.ReadUTFByte(bytes,ByteUtil.ReadUnsignedShort(bytes,ref byteIndex),ref byteIndex);
			BulletCount = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			BulletRadius = ByteUtil.ReadFloat(bytes,ref byteIndex);
			BulletSpeed = ByteUtil.ReadFloat(bytes,ref byteIndex);
			BulletMaxDistance = ByteUtil.ReadFloat(bytes,ref byteIndex);
			 BulletHitPosType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			AnimationType = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			BuffId = ByteUtil.ReadSignedInt(bytes,ref byteIndex);
			ShakeId = ByteUtil.ReadSignedInt(bytes,ref byteIndex);

		}
    }
    
}
