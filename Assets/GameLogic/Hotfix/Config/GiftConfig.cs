﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class GiftConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            GiftDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new GiftDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取GiftDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static GiftDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out GiftDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, GiftDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, GiftDef> _defDic = new Dictionary<int, GiftDef>();
    }
    
}
