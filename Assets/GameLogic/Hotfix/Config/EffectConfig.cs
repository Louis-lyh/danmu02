﻿using UnityEngine;
using GameHotfix.Framework;
using GameInit.Framework;
using System.Collections.Generic;

namespace GameHotfix.Game
{
    public class EffectConfig : BaseConfig
    {
        public override void Deserialize(byte[] bytes)
        {
            _defDic.Clear();
            int byteIndex = 0;
            int defCount = ByteUtil.ReadSignedInt(bytes, ref byteIndex);
            EffectDef def;
            for (int i = 0; i < defCount; i++)
            {
                def = new EffectDef();
                def.Parse(bytes, ref byteIndex);
                _defDic.Add(def.ID, def);
            }
        }
        
        /// <summary>
        /// 通过ID获取EffectDef的实例
        /// </summary>
        /// <param name="ID">索引</param>
        public static EffectDef Get(int ID)
        {
            _defDic.TryGetValue(ID, out EffectDef def);
            return def;
        }
        
        /// <summary>
        /// 获取字典
        /// </summary>
        public static Dictionary<int, EffectDef> GetDefDic()
        {
            return _defDic;
        }

        private static Dictionary<int, EffectDef> _defDic = new Dictionary<int, EffectDef>();
    }
    
}
