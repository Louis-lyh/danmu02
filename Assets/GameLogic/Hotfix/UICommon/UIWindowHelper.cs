using GameHotfix.Framework;

namespace GameHotfix.Game
{
    public static class UIWindowHelper
    {
        public static void Init()
        {
            //初始化UI管理器
            WindowManager.Instance.Init();
            //将游戏中的UI进行注册
            RegisterUIModule(UIWindowId.UIMainWindow, new MainWindow());
            RegisterUIModule(UIWindowId.UIBattleWindow, new BattleWindow());
            RegisterUIModule(UIWindowId.UICommandWindow, new CommandWindow());
            RegisterUIModule(UIWindowId.UINoticeWindow,new UINoticeWindow());
        }

        private static void RegisterUIModule(uint moduleId, UIWindowBase uiWindowBase)
        {
            WindowManager.Instance.RegisterWindow(moduleId, uiWindowBase);
        }
    }
}