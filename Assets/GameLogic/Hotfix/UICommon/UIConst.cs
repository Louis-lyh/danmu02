namespace GameHotfix.Game
{
    public static class UIWindowId
    {
        public const uint UIMainWindow = 1;           //主页界面
        public const uint UIBattleWindow = 2;           //战斗界面
        public const uint UINoticeWindow = 3;            //游戏提示界面

        public const uint UICommandWindow = 999;              //指令
    }
}