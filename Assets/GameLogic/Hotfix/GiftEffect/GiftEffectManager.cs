using GameHotfix.Framework;
using System.Collections.Generic;
using Cysharp.Text;
using UnityEngine;

namespace GameHotfix.Game
{
    public class GiftEffectManager : DataModelBase<GiftEffectManager>
    {
        //初始化标记
        private bool _isInit;
        
        //Root节点
        private GameObject _root;
        
        //Mask
        private GiftMask _mask;
        
        //礼物
        private List<GiftEffect> _giftEffectList;
        
        //初始化
        public void Init()
        {
            if (_isInit)
            {
                GameInit.Framework.Logger.LogWarning("礼物管理器,已经初始化了!");
                return;
            }
            
            //得到 Root节点
            _root = GameObject.FindWithTag("Gift");
            
            //Mask
            SpriteRenderer maskSpriteRenderer = _root.transform.Find("GiftMaskCamera/GiftMask").GetComponent<SpriteRenderer>();
            _mask = new GiftMask(maskSpriteRenderer);
            
            //礼物
            _giftEffectList = new List<GiftEffect>();
            Vector3 position = new Vector3(0f, -2f, 8f);
            Vector3 rotation = new Vector3(0f, 180f, 0f);
            for (int i = 1; i <= 8; i++)
            {
                //相机前缀
                string prefix = "GiftCamera_Lv";
                //得到相机
                Camera camera = _root.transform.Find(ZString.Concat(prefix, i)).GetComponent<Camera>();
                camera.enabled = false;
                
                //创建礼物
                GiftEffect giftEffect = new GiftEffect();
                if (i == 4) 
                    giftEffect.Init(camera, ZString.Concat("gift", i), position, Vector3.zero);
                else 
                    giftEffect.Init(camera, ZString.Concat("gift", i), position, rotation);
                _giftEffectList.Add(giftEffect);
            }

            //标记初始化完成
            _isInit = true;
        }
        
        //播放礼物
        public void PlayGiftEff(int lv)
        {
            string name = "start";
            if ( lv == 2 || lv == 3 || lv == 4 || lv == 5 || lv == 6 || lv == 7 || lv == 8) name = "loop";
            
            int index = lv - 1;
            
            // //当有比自己等级高的特效-本次播放无效
            // for (int i = index; i < _giftEffectList.Count; i++)
            // {
            //     GiftEffect gef = _giftEffectList[i];
            //     if (gef.Ready == false)
            //     {
            //         return;
            //     }
            // }
            //
            // //立即结束比自己等级低的特效
            // for (int i = 0; i < index; i++)
            // {
            //     GiftEffect gef = _giftEffectList[i];
            //     if (gef.Ready == false)
            //     {
            //         gef.End();
            //     }
            // }
            
            //播放自己
            GiftEffect giftEffect = _giftEffectList[index];
            if (giftEffect.Ready)
            {
                giftEffect.Play(name);
                _mask.Show();
            }
        }
        
        //mask hide
        public void HideMask()
        {
            for (int i = 0; i < _giftEffectList.Count; i++)
            {
                GiftEffect giftEffect = _giftEffectList[i];
                if (giftEffect.Ready == false)
                {
                    return;
                }
            }
            
            _mask.Hide();
        }
    }
}