using Cysharp.Text;
using UnityEngine;
using GameInit.Framework;
using DG.Tweening;

namespace GameHotfix.Game
{
    public sealed class GiftMask
    {
        // 开始透明度 
        private float _maskAlpah;
        private SpriteRenderer _spriteRenderer;
        private Tweener _tweenerShow;
        private Tweener _tweenerHide;

        public GiftMask(SpriteRenderer spriteRenderer)
        {
            _spriteRenderer = spriteRenderer;
            _maskAlpah = spriteRenderer.color.a;
            _spriteRenderer.color = new Color(0f, 0f, 0f, 0f);
        }

        public void Show()
        {
            if (_tweenerShow != null) return;
            
            if (_tweenerHide != null)
            {
                _tweenerHide.Kill();
                _tweenerHide = null;
            }
            
            _tweenerShow = _spriteRenderer.DOFade(_maskAlpah, 0.35f);
            _tweenerShow.OnComplete(() =>
            {
                _tweenerShow = null;
            });
        }

        public void Hide()
        {
            if (_tweenerShow != null)
            {
                _tweenerShow.Kill();
                _tweenerShow = null;
            }
            
            if (_tweenerHide != null)
            {
                _tweenerHide.Kill();
                _tweenerHide = null;
            }
            
            _tweenerHide = _spriteRenderer.DOFade(0f, 0.5f);
        }
    }
}