using Cysharp.Text;
using UnityEngine;
using GameInit.Framework;

namespace GameHotfix.Game
{
    public class GiftEffect
    {
        private Camera _camera;
        private GameObject _obj;
        private Animation _animation;
        private bool _ready;
        
        public bool Ready
        {
            get { return _ready; }
        }

        public async void Init(Camera camera, string name, Vector3 position, Vector3 rotation)
        {
            GameObject prefab = await ResourceManager.LoadAssetAsync<GameObject>(ZString.Concat("Gift/", name, ".prefab"));
            if (prefab == null)
            {
                GameInit.Framework.Logger.LogError(ZString.Concat("礼物: ", name, " 不存在!"));
                return;
            }

            _camera = camera;
            
            _obj = GameObject.Instantiate(prefab);
            _obj.transform.SetParent(_camera.transform);
            _obj.transform.localPosition = position;
            _obj.transform.localRotation = Quaternion.Euler(rotation);
            _obj.SetActive(false);

            _animation = _obj.GetComponent<Animation>();

            GameInit.Game.GiftEffectListener listener = _obj.AddComponent<GameInit.Game.GiftEffectListener>();
            listener.InitListen(_animation, StartAction, LoopAction);

            _ready = true;
        }

        private void StartAction()
        {
            _animation.Play("loop");
        }

        private void LoopAction()
        {
            _obj.SetActive(false);
            _camera.enabled = false;
            
            _ready = true;
            
            //mask
            GiftEffectManager.Instance.HideMask();
        }
        
        public void Play(string name)
        {
            _obj.SetActive(true);
            _camera.enabled = true;

            _animation.Play(name);
            
            _ready = false;
        }

        public void End()
        {
            _obj.SetActive(false);
            _camera.enabled = false;
            
            _ready = true;
        }
    }
}