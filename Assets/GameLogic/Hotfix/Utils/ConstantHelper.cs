﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace GameHotfix.Game
{
    public class ConstantHelper
    {
       
        //初始化
        public static void Init()
        {
            Dictionary<int, ConstantDef> dict = ConstantConfig.GetDefDic();
            var mytype = typeof(ConstantHelper);
            foreach (FieldInfo info in mytype.GetFields())
            {
                using (Dictionary<int, ConstantDef>.Enumerator itr = dict.GetEnumerator())
                {
                    while (itr.MoveNext())
                    {
                        ConstantDef def = itr.Current.Value;
                        if (def.FieldName == info.Name)
                        {
                            if (info.FieldType.Name == "Int32")
                            {
                                int i = Convert.ToInt32(def.Value);
                                info.SetValue(mytype, i);
                            }
                            else if (info.FieldType.Name == "Single")
                            {
                                float f = Convert.ToSingle(def.Value);
                                info.SetValue(mytype, f);
                            }
                            else
                                info.SetValue(mytype, def.Value);
                            break;
                        }
                    }
                }
            }
        }
    }
}