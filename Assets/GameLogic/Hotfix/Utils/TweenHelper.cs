using DG.Tweening;

namespace GameHotfix.Game
{
    public static class TweenHelper
    {
        //Number
        public static Tweener NumberTo(float delay, float start, float end, System.Action<float> callback = null, System.Action completeAction = null)
        {
            Tweener tweener = null;
            tweener = DOTween.To(v =>
            {
                if (callback != null)
                    callback(v);
            }, start, end, delay).OnComplete(() =>
            {
                if (completeAction != null)
                    completeAction();
            });
            return tweener;
        }
    }
}