using GameHotfix.Framework;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public class PositioningManager : DataModelBase<PositioningManager>
    {
        //初始化标记
        private bool _isInit;
        
        //左边-定位
        private Positioning _positioningLeft;
        //右边-定位
        private Positioning _positioningRight;
        
        //参数
        public Vector3 positionCameraLocalPosition = new Vector3(0f, 10.8f, -9f);   //定位相机-本地位置
        public Vector3 positionCameraLocalRotation = new Vector3(37f, 0f, 0f);   //定位相机-本地旋转
        public float duration = 10f;   //持续时间
        
        //初始化
        public void Init(RawImage rawImageLeft, RawImage rawImageRight)
        {
            if (_isInit)
            {
                GameInit.Framework.Logger.LogWarning("定位管理器,已经初始化了!");
                return;
            }
            
            //创建左边-定位
            GameObject go_left = GameObject.FindWithTag("PositioningCameraLeft");
            Camera camera_left = go_left.GetComponent<Camera>();
            RenderTexture rt_left = Resources.Load<RenderTexture>("Positioning/PositioningRt_Left");
            _positioningLeft = new Positioning(0,camera_left, rt_left, rawImageLeft);
            
            //创建右边-定位
            GameObject go_right = GameObject.FindWithTag("PositioningCameraRight");
            Camera camera_right = go_right.GetComponent<Camera>();
            RenderTexture rt_right = Resources.Load<RenderTexture>("Positioning/PositioningRt_Right");
            _positioningRight = new Positioning(1,camera_right, rt_right, rawImageRight);

            //初始化完成
            _isInit = true;
        }
        
        //Update
        public void Update(float fTick)
        {
            if (_positioningLeft != null) _positioningLeft.Update(fTick);
            if (_positioningRight != null) _positioningRight.Update(fTick);
        }

        //定位
        public void Positioning(GameObject obj, Action<int> startAction, Action<int,Action> endAction)
        {
            if (_positioningLeft != null && _positioningLeft.Ready)
            {
                _positioningLeft.MoveAbout(obj, startAction, endAction);
                return;
            }

            if (_positioningRight != null && _positioningRight.Ready)
            {
                _positioningRight.MoveAbout(obj, startAction, endAction);
            }
        }
    }
}