using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Game
{
    public sealed class Positioning
    {
        // 序号
        private int _index;
        
        private Camera _camera;
        private RenderTexture _renderTexture;
        private RawImage _rawImage;

        private GameObject _target;
        
        private float _time;

        private Action<int,Action> _endCallback;

        public bool Ready
        {
            get { return _time <= 0f; }
        }

        public Positioning(int index,Camera camera, RenderTexture renderTexture, RawImage rawImage)
        {
            _index = index;
            _time = 0f;

            _camera = camera;
            _renderTexture = renderTexture;
            _rawImage = rawImage;

            _camera.targetTexture = _renderTexture;
            _rawImage.texture = _renderTexture;
                
            _camera.enabled = false;
        }

        public void Update(float fTick)
        {
            if (_time <= 0f) return;

            if (_target == null || _target.activeSelf == false)
            {
                if (_endCallback == null) MoveAboutEnd();
                else _endCallback.Invoke(_index,MoveAboutEnd);
                return;
            }

            _time -= fTick;
            if (_time <= 0f)
            {
                _time = 999999f;
                if (_endCallback == null) MoveAboutEnd();
                else _endCallback.Invoke(_index,MoveAboutEnd);
            }
            // 定位玩家
            Transform transform = _camera.gameObject.transform;
            transform.position = _target.transform.position + PositioningManager.Instance.positionCameraLocalPosition * (_target.transform.localScale.x / 3);
            transform.rotation = Quaternion.Euler(PositioningManager.Instance.positionCameraLocalRotation);
        }

        private void MoveAboutEnd()
        {
            _endCallback = null;
            
            _camera.gameObject.transform.SetParent(null);
            _camera.enabled = false;
            
            _time = 0f;
        }

        public void MoveAbout(GameObject obj, Action<int> startAcion, Action<int,Action> endAction)
        {
            _endCallback = endAction;

            _target = obj;
            _time = PositioningManager.Instance.duration;

            Transform transform = _camera.gameObject.transform;
            // transform.SetParent(_target.transform);
            // transform.localPosition = PositioningManager.Instance.positionCameraLocalPosition;
            transform.localRotation = Quaternion.Euler(PositioningManager.Instance.positionCameraLocalRotation);
            _camera.enabled = true;
            
            if (startAcion != null) startAcion.Invoke(_index);
        }
    }
}