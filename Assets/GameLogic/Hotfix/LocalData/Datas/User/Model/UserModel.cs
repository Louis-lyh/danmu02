using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameInit.Game;
using ILRuntime.Runtime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using UnityEngine;
using UnityEngine.UIElements;

namespace GameHotfix.Game
{
    public class UserModel : Framework.DataModelBase<UserModel>
    {
        public const string UPDATE_DRESS = "UPDATE_DRESS";

        private float pullTime = 0;

        //是否打开声音
        public bool IsOpenVoice
        {
            get => LocalDataManager.UserData.isOpenVoice;
            set
            {
                LocalDataManager.UserData.isOpenVoice = value;
                SoundManager.Instance.IsOpenVoice = value;
                LocalDataManager.UserData.Save();
            }
        }

        //是否打开音乐
        public bool IsOpenMusic
        {
            get => LocalDataManager.UserData.isOpenMusic;
            set
            {
                LocalDataManager.UserData.isOpenMusic = value;
                SoundManager.Instance.IsOpenMusic = value;
                if (value)
                {
                    SoundManager.Instance.RecoveryBgm();
                }
                else
                {
                    SoundManager.Instance.StopBgm();
                }

                LocalDataManager.UserData.Save();
            }
        }
        
        public long GetCurrentTimeSecond()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return ts.TotalSeconds.ToInt64();
        }
    }
}