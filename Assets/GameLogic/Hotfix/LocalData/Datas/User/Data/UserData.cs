using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameHotfix.Game
{
    public class UserData : SerializeData
    {
        public bool isOpenVoice;//是否打开声音
        public bool isOpenMusic;//是否打开音乐
        
        public UserData()
        {
            isOpenVoice = true;
            isOpenMusic = true;
        }
    }
}


