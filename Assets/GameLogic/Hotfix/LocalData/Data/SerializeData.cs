using System;
using GameInit.Framework;

namespace GameHotfix.Game
{
    [Serializable]
    public class SerializeData
    {
        [NonSerialized] 
        public LocalDataType Type;
        
        // public long LastSyncTime;
        
        public void Save()
        {
            // LastSyncTime = TimeUtils.GetTimestampToSeconds();
            
            LocalDataManager.Serializable(this);
        }
    }
}