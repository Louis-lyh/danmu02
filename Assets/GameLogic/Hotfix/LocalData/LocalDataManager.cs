using System;
using GameInit.Framework;
using GameInit.Game;
using LitJson;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Game
{
    public enum LocalDataType
    {
        UserData,
        ActivityData,
        BagData,
        TrackData,
        PurchaseData,
        PlayerData, //玩家数据
        UserRoleData, //用户角色的数据
        ShopData, //商店数据
        TaskData, //任务数据
        GuideUnlockData,//引导和解锁相关数据
    }

    public static class LocalDataManager
    {
        public static UserData UserData { get; private set; }
        
        
        public static void Init()
        {
            UserData = Deserialize<UserData>(LocalDataType.UserData);
           
            InitData();
            
            //设置音乐和音效运行时的开关
            SoundManager.Instance.IsOpenVoice = LocalDataManager.UserData.isOpenVoice;
            SoundManager.Instance.IsOpenMusic = LocalDataManager.UserData.isOpenMusic;
        }

        public static void SwitchAccount()
        {
            PlayerPrefs.DeleteAll();
            Init();
        }

        private static void InitData()
        {
            SoundManager.Instance.IsOpenMusic = UserData.isOpenMusic;
            SoundManager.Instance.IsOpenVoice = UserData.isOpenVoice;
        }

        public static void Serializable<T>(T data) where T : SerializeData
        {
            try
            {
                var localData = JsonMapper.ToJson(data);
                PlayerPrefs.SetString(data.Type.ToString(), localData);
                PlayerPrefs.Save();
            }
            catch (Exception e)
            {
                Logger.LogError("LocalDataManager.Serializable() => 序列化数据出错，保存失败, Error:" + e.Message);
            }
        }

        public static T Deserialize<T>(LocalDataType dataType) where T : SerializeData, new()
        {
            T t = null;
            try
            {
                var localData = PlayerPrefs.HasKey(dataType.ToString())
                    ? PlayerPrefs.GetString(dataType.ToString())
                    : "";
                t = string.IsNullOrEmpty(localData) ? new T() : JsonMapper.ToObject<T>(localData);
                // Logger.LogMagenta("LocalDataManager.Deserialize() => 初始化本地数据,type:" + dataType + ", data:" + localData);
            }
            catch (Exception e)
            {
                t = new T();
                Logger.LogError("LocalDataManager.Deserialize() => 反序列化数据出错，Error:" + e.Message);
            }

            t.Type = dataType;
            return t;
        }
    }
}