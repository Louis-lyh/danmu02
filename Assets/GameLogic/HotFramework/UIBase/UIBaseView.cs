using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Framework
{
    public abstract class UIBaseView : EventDispatcher
    {
        public RectTransform RectTransform { get; private set; }
        public GameObject DisplayObject { get; private set; }

        private List<Button> _allButtons;
        private List<InputField> _allInputFields;
        private bool _isShow;

        private bool _loadFinishNeedShow = false;

        protected object[] _data;

        protected UIBaseView()
        {
            _allButtons = new List<Button>();
            _allInputFields = new List<InputField>();
        }

        public void SetDisplayObject(GameObject gameObject)
        {
            DisplayObject = gameObject;
            if (gameObject == null)
            {
                Logger.LogError("UIBaseView.SetDisplayObject() : 设置显示对象为null, View:" + this.GetType().Name);
                return;
            }

            RectTransform = DisplayObject.transform as RectTransform;
            ParseComponent();
            ParseComponentExt();
            if (_loadFinishNeedShow)
            {
                Show(_data);
                _loadFinishNeedShow = false;
                _data = null;
            }
        }

        protected static async UniTask<T> AddToParent<T>(string prefabPath, RectTransform parent)
            where T : UIBaseView, new()
        {
            var src = new T();
            var o = await ResourceManager.ConstructObjAsync(prefabPath);
            src.SetDisplayObject(o);
            src.SetParent(parent);
            return src;
        }
        
        public virtual void Show(params object[] arg)
        {
            if (DisplayObject == null)
            {
                _data = arg;
                _loadFinishNeedShow = true;
                return;
            }

            DisplayObject.SetActive(true);
            if (!_isShow)
            {
                RegisterEvent();
            }

            _isShow = true;
            Refresh(arg);
        }

        /// <summary>
        /// 页面完全打开完成后调用
        /// </summary>
        protected virtual void ShowComplete()
        {
        }

        protected virtual void RegisterEvent()
        {
        }

        protected virtual void UnRegisterEvent()
        {
        }

        public void Hide()
        {
            if (_loadFinishNeedShow)
            {
                _loadFinishNeedShow = false;
                _data = null;
            }

            if (DisplayObject != null && DisplayObject.activeSelf)
                DisplayObject.SetActive(false);
            if (!_isShow)
                return;
            _isShow = false;
            UnRegisterEvent();
        }

        protected virtual void Refresh(params object[] arg)
        {
        }

        #region Button事件处理函数

        protected void ListenButton<T>(string path, Action<T> action, T p)
        {
            var btn = Find<Button>(path);
            if (btn == null)
            {
                Logger.LogMagenta("[UIBaseView.ListenButton() => 获取Button失败，path:" + path + "]");
                return;
            }

            ListenButton(btn, action, p);
        }

        protected void ListenButton<T>(Button btn, Action<T> action, T p)
        {
            if (btn == null)
                return;
            btn.Click(() => { action?.Invoke(p); });
            _allButtons.Add(btn);
        }

        protected void ListenButton(string path, Action action)
        {
            var btn = Find<Button>(path);
            if (btn == null)
            {
                Logger.LogMagenta("[UIBaseView.ListenButton() => 获取Button失败，path:" + path + "]");
                return;
            }

            btn.Click(action);
            _allButtons.Add(btn);
        }

        protected void ListenButton(Button btn, Action action)
        {
            if (btn == null)
                return;
            btn.Click(action);
            _allButtons.Add(btn);
        }

        protected void ListenButton(string path, string evtType)
        {
            var btn = Find<Button>(path);
            if (btn == null)
            {
                Logger.LogMagenta("[UIBaseView.ListenButton() => 获取Button失败，path:" + path + ", opCode:" + evtType +
                                  "]");
                return;
            }

            ListenButton(btn, evtType);
        }

        protected void ListenButton(Button btn, string evtType)
        {
            if (btn == null)
                return;
            _allButtons.Add(btn);
            btn.Click(() => { DispatchEvent(evtType); });
        }

        #endregion

        #region InputField事件处理函数

        /// 监听输入事件
        protected void ListenInput(string path, UnityAction<string> onEndEdit, UnityAction<string> onValueChanged,
            string defaultStr = "")
        {
            var ipfld = Find<InputField>(path);
            ListenInput(ipfld, onEndEdit, onValueChanged, defaultStr);
        }

        /// 监听输入
        protected void ListenInput(InputField input, UnityAction<string> onEndEdit, UnityAction<string> onValueChanged,
            string defaultStr = "")
        {
            input.text = defaultStr;
            input.onEndEdit.AddListener(onEndEdit);
            input.onValueChanged.AddListener(onValueChanged);
            _allInputFields.Add(input);
        }

        protected T Find<T>(string path) where T : Component
        {
            var obj = Find(path);
            return obj?.GetComponent<T>();
        }

        protected GameObject Find(string path)
        {
            return RectTransform.Find(path)?.gameObject;
        }

        protected RectTransform FindRectTransform(string path)
        {
            return RectTransform.Find(path) as RectTransform;
        }

        protected T FindOnSelf<T>() where T : Component
        {
            return RectTransform.GetComponent<T>();
        }

        #endregion

        public void SetParent(RectTransform parent)
        {
            RectTransform.SetParent(parent, false);
            RectTransform.localPosition = Vector3.zero;
        }

        public override void Dispose()
        {
            UnRegisterEvent();
            if (_allButtons != null)
            {
                for (int i = 0; i < _allButtons.Count; i++)
                {
                    _allButtons[i].onClick.RemoveAllListeners();
                }

                _allButtons.Clear();
                _allButtons = null;
            }

            if (_allInputFields != null)
            {
                for (int i = 0; i < _allInputFields.Count; i++)
                {
                    _allInputFields[i].onEndEdit.RemoveAllListeners();
                    _allInputFields[i].onValueChanged.RemoveAllListeners();
                }

                _allInputFields.Clear();
                _allInputFields = null;
            }

            DisplayObject = null;
            RectTransform = null;
            base.Dispose();
        }

        protected abstract void ParseComponent();

        protected virtual void ParseComponentExt()
        {
        }

        public void OnUIBaseViewOpenComplete()
        {
            Global.DispatchEvent("OnWindowOpenComplete");
        }
    }
}