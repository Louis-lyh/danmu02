using System;
using System.Collections;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;

namespace GameHotfix.Framework
{
    public class ListView : UIBaseView
    {
        #region HANDLER ItemLoaded

        private readonly List<UnityAction<Vector2>> _onValueChangeHandlers = new List<UnityAction<Vector2>>();

        public delegate void OnItemLoadedHandler(ListItemBase item);

        private OnItemLoadedHandler onItemLoaded;

        public void ItemLoaded(ListItemBase item, bool clear = false)
        {
            if (onItemLoaded != null)
            {
                try
                {
                    onItemLoaded(item);
                    item.Alpha = 1;
                }
                catch (Exception e)
                {
                    item.Alpha = 0;
                }

                if (clear)
                {
                    onItemLoaded = null;
                }
            }
        }

        #endregion

        #region HANDLER ItemSelected

        public delegate void OnItemSelectedHandler(ListItemBase item);

        public OnItemSelectedHandler onItemSelected;

        public void ItemSelected(ListItemBase item, bool clear = false)
        {
            if (onItemSelected != null)
            {
                onItemSelected(item);

                if (clear)
                {
                    onItemSelected = null;
                }
            }
        }

        #endregion


        private enum ScrollOrientation
        {
            HORIZONTAL,
            VERTICAL
        }

        private enum ScrollDirection
        {
            NEXT,
            PREVIOUS
        }


        protected ScrollRect _scrollRect;

        private RectTransform _viewport;

        private RectTransform _content;

        public Transform Content
        {
            get { return _content; }
        }

        private float _totalContentExpand;

        public float _leftContentExpand;

        private ScrollOrientation _scrollOrientation = ScrollOrientation.VERTICAL;

        private float _spacing;

        private bool _fitItemToViewport;

        private bool _centerOnItem;

        private float _changeItemDragFactor;


        private List<ListItemBase> _itemsList;

        private float _itemSize;
        private float _lastPosition;

        private int _itemsTotal;
        private int _itemsVisible;

        private int _itemsToRecycleBefore;
        private int _itemsToRecycleAfter;

        private int _currentItemIndex;
        private int _lastItemIndex;

        private Vector2 _dragInitialPosition;


        protected override void ParseComponent()
        {
        }


        private void Init()
        {
            _scrollRect = DisplayObject.GetComponent<ScrollRect>();
            if (!_scrollRect)
            {
                Debug.LogError("ScrollRect script not found!");
                return;
            }

            // _viewport = _scrollRect.viewport;
            _viewport = RectTransform;
            _content = _scrollRect.content;
            foreach (Transform child in _content)
            {
                GameObject.Destroy(child.gameObject);
            }
        }

        public void Reload(int items, ListItemBase listItemPrefab, OnItemLoadedHandler handler, bool isKeep = true)
        {
            onItemLoaded = handler;
            if (_scrollRect)
            {
                if (isKeep == false)
                {
                    Destroy();
                    Create(items, listItemPrefab);
                }
                else
                {
                    _itemsTotal = items;
                    switch (_scrollOrientation)
                    {
                        case ScrollOrientation.HORIZONTAL:
                            _content.sizeDelta =
                                new Vector2(_itemSize * items + _spacing * (items - 1) + _totalContentExpand, 0);
                            break;
                        case ScrollOrientation.VERTICAL:
                            _content.sizeDelta =
                                new Vector2(0, _itemSize * items + _spacing * (items - 1) + _totalContentExpand);
                            break;
                    }

                    foreach (var item in _itemsList)
                    {
                        ItemLoaded(item);
                    }
                }
            }
            else
            {
                Create(items, listItemPrefab);
            }
        }

        private void Create(int items, ListItemBase listItemPrefab)
        {
            Init();
            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    _scrollRect.vertical = false;
                    _scrollRect.horizontal = true;

                    _content.anchorMin = new Vector2(0, 0);
                    _content.anchorMax = new Vector2(0, 1);

                    if (_fitItemToViewport)
                    {
                        listItemPrefab.Size = new Vector2(_viewport.rect.width, listItemPrefab.Size.y);
                    }

                    _itemSize = listItemPrefab.Size.x;

                    _content.sizeDelta =
                        new Vector2(_itemSize * items + _spacing * (items - 1) + _totalContentExpand, 0);
                    break;

                case ScrollOrientation.VERTICAL:
                    _scrollRect.vertical = true;
                    _scrollRect.horizontal = false;

                    _content.anchorMin = new Vector2(0, 1);
                    _content.anchorMax = new Vector2(1, 1);

                    if (_fitItemToViewport)
                    {
                        listItemPrefab.Size = new Vector2(listItemPrefab.Size.x, _viewport.rect.height);
                    }

                    _itemSize = listItemPrefab.Size.y;

                    _content.sizeDelta =
                        new Vector2(0, _itemSize * items + _spacing * (items - 1) + _totalContentExpand);
                    break;
            }

            if (_centerOnItem)
            {
                _scrollRect.inertia = false;
            }


            _itemsVisible = Mathf.CeilToInt(GetViewportSize() / _itemSize);

            var itemsToInstantiate = _itemsVisible + 1;
            _itemsList = new List<ListItemBase>();
            listItemPrefab.DisplayObject.SetActive(false);
            for (int i = 0; i < itemsToInstantiate; i++)
            {
                var item = CreateNewItem(listItemPrefab, i, _itemSize);
                item.Index = i;

                _itemsList.Add(item);

                ItemLoaded(item);
            }

            _itemsTotal = items;

            _lastItemIndex = _itemsList.Count - 1;

            _itemsToRecycleAfter = _itemsList.Count - _itemsVisible;


            _scrollRect.onValueChanged.AddListener((position) =>
            {
                foreach (var handler in _onValueChangeHandlers)
                {
                    handler?.Invoke(position);
                }

                if (!_centerOnItem)
                {
                    Recycle();
                }
            });
        }

        public void AddScrollHandler(UnityAction<Vector2> handler)
        {
            _onValueChangeHandlers.Add(handler);
        }

        protected void ScrollToNextItem()
        {
            var pos = _content.localPosition;
            switch (_scrollOrientation)
            {
                case ScrollOrientation.VERTICAL:
                    var targetY = pos.y + _itemSize;
                    var h = targetY + GetViewportSize();
                    if (h < _content.sizeDelta.y)
                    {
                        _content.DOLocalMove(new Vector3(pos.x, targetY, pos.z), 0.5f);
                    }

                    break;
            }
        }

        protected void ScrollToItem(int idx)
        {
            switch (_scrollOrientation)
            {
                case ScrollOrientation.VERTICAL:
                    var halfH = _itemSize * Mathf.Round(_itemsVisible / 2.0f);
                    var targetY = _itemSize * idx - halfH;
                    targetY = Mathf.Max(0, Mathf.Min(targetY, _content.sizeDelta.y - halfH));
                    _content.DOLocalMove(new Vector3(0, targetY, 0), 0.1f);
                    break;
            }
        }

        private ListItemBase CreateNewItem(ListItemBase listItem, int index, float dimension)
        {
            GameObject instance = GameObject.Instantiate(listItem.DisplayObject, Vector3.zero, Quaternion.identity);
            instance.transform.SetParent(_content.transform);
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localScale = Vector3.one;
            instance.SetActive(true);

            float position = index * (dimension + _spacing) + dimension / 2 + _leftContentExpand;

            RectTransform rectTransform = instance.GetComponent<RectTransform>();

            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    rectTransform.anchorMin = new Vector2(0, 0);
                    rectTransform.anchorMax = new Vector2(0, 1);
                    rectTransform.anchoredPosition = new Vector2(position, 0);
                    rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, 0);
                    rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, 0);
                    break;

                case ScrollOrientation.VERTICAL:
                    rectTransform.anchorMin = new Vector2(0, 1);
                    rectTransform.anchorMax = new Vector2(1, 1);
                    rectTransform.anchoredPosition = new Vector2(0, -position);
                    rectTransform.offsetMin = new Vector2(0, rectTransform.offsetMin.y);
                    rectTransform.offsetMax = new Vector2(0, rectTransform.offsetMax.y);
                    break;
            }

            return listItem.Clone(instance);
        }

        private void Recycle()
        {
            if (_lastPosition == -1)
            {
                _lastPosition = GetContentPosition();

                return;
            }

            var displacedRows = Mathf.FloorToInt(Mathf.Abs(GetContentPosition() - _lastPosition) / _itemSize);

            var direction = GetScrollDirection();
            if (displacedRows == 0)
            {
                if (_scrollOrientation == ScrollOrientation.VERTICAL && direction == ScrollDirection.PREVIOUS)
                {
                    var fy = _itemsList[0].RectTransform.localPosition.y;
                    var cy = GetContentPosition();
                    if (cy > 0 && cy + fy < -_itemSize / 2)
                    {
                        displacedRows = 1;
                    }
                }

                if (_scrollOrientation == ScrollOrientation.VERTICAL && direction == ScrollDirection.NEXT)
                {
                    var fy = _itemsList[0].RectTransform.localPosition.y;
                    var cy = GetContentPosition();
                    var dt = _viewport.rect.height % _itemSize - _itemSize / 2;
                    if (Math.Abs((cy + fy + dt) / _itemSize % 1) < 0.5 && cy + fy > _itemSize)
                    {
                        displacedRows = 1;
                    }
                }

                if (displacedRows == 0)
                {
                    return;
                }
            }

            for (var i = 0; i < displacedRows; i++)
            {
                switch (direction)
                {
                    case ScrollDirection.NEXT:

                        NextItem();

                        break;

                    case ScrollDirection.PREVIOUS:

                        PreviousItem();

                        break;
                }

                if ((direction == ScrollDirection.NEXT && _scrollOrientation == ScrollOrientation.VERTICAL) ||
                    (direction == ScrollDirection.PREVIOUS && _scrollOrientation == ScrollOrientation.HORIZONTAL))
                {
                    _lastPosition += _itemSize + _spacing;
                }
                else
                {
                    _lastPosition -= _itemSize + _spacing;
                }
            }
        }

        private void NextItem()
        {
            if (_itemsToRecycleBefore >= (_itemsList.Count - _itemsVisible) / 2 && _lastItemIndex < _itemsTotal - 1)
            {
                _lastItemIndex++;

                RecycleItem(ScrollDirection.NEXT);
            }
            else
            {
                _itemsToRecycleBefore++;
                _itemsToRecycleAfter--;
            }
        }

        private void PreviousItem()
        {
            if (_itemsToRecycleAfter >= (_itemsList.Count - _itemsVisible) / 2 && _lastItemIndex > _itemsList.Count - 1)
            {
                RecycleItem(ScrollDirection.PREVIOUS);

                _lastItemIndex--;
            }
            else
            {
                _itemsToRecycleBefore--;
                _itemsToRecycleAfter++;
            }
        }

        private void RecycleItem(ScrollDirection direction)
        {
            ListItemBase firstItem = _itemsList[0];
            ListItemBase lastItem = _itemsList[_itemsList.Count - 1];

            float targetPosition = (_itemSize + _spacing);

            switch (direction)
            {
                case ScrollDirection.NEXT:

                    switch (_scrollOrientation)
                    {
                        case ScrollOrientation.HORIZONTAL:
                            firstItem.Position =
                                new Vector2(lastItem.Position.x + targetPosition, firstItem.Position.y);
                            break;

                        case ScrollOrientation.VERTICAL:
                            firstItem.Position =
                                new Vector2(firstItem.Position.x, lastItem.Position.y - targetPosition);
                            break;
                    }

                    firstItem.Index = _lastItemIndex;
                    firstItem.RectTransform.SetAsLastSibling();

                    _itemsList.RemoveAt(0);
                    _itemsList.Add(firstItem);

                    ItemLoaded(firstItem);
                    break;

                case ScrollDirection.PREVIOUS:

                    switch (_scrollOrientation)
                    {
                        case ScrollOrientation.HORIZONTAL:
                            lastItem.Position = new Vector2(firstItem.Position.x - targetPosition, lastItem.Position.y);
                            break;

                        case ScrollOrientation.VERTICAL:
                            lastItem.Position = new Vector2(lastItem.Position.x, firstItem.Position.y + targetPosition);
                            break;
                    }

                    lastItem.Index = _lastItemIndex - _itemsList.Count;
                    lastItem.RectTransform.SetAsFirstSibling();

                    _itemsList.RemoveAt(_itemsList.Count - 1);
                    _itemsList.Insert(0, lastItem);

                    ItemLoaded(lastItem);
                    break;
            }

            Canvas.ForceUpdateCanvases();
        }


        public void OnDragBegin(BaseEventData eventData)
        {
            if (_centerOnItem)
            {
                _dragInitialPosition = ((PointerEventData) eventData).position;
            }
        }

        public void OnDragEnd(BaseEventData eventData)
        {
            if (_centerOnItem)
            {
                float delta = GetDragDelta(_dragInitialPosition, ((PointerEventData) eventData).position);

                if (_itemsList != null && Mathf.Abs(delta) > _itemSize * _changeItemDragFactor)
                {
                    if (Mathf.Sign(delta) == -1 && _currentItemIndex < _itemsTotal - 1)
                    {
                        NextItem();

                        _currentItemIndex++;
                    }
                    else if (Mathf.Sign(delta) == 1 && _currentItemIndex > 0)
                    {
                        _currentItemIndex--;

                        PreviousItem();
                    }
                }

                CenterOnItem(_currentItemIndex);
            }
        }

        private async void CenterOnItem(int index)
        {
            if (DisplayObject.activeInHierarchy && DisplayObject.activeSelf)
            {
                await CenterOnItemCoroutine(index);
            }
        }

        private Vector2 GetCenterOnItemPosition(int index)
        {
            float positionX = 0;
            float positionY = 0;

            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    positionX = -(index * (_itemSize + _spacing));
                    break;

                case ScrollOrientation.VERTICAL:
                    positionY = -(index * (_itemSize + _spacing));
                    break;
            }

            return new Vector2(positionX, positionY);
        }

        private async UniTask CenterOnItemCoroutine(int index)
        {
            await UniTask.DelayFrame(1);
            if (_itemsList != null && _itemsList.Count > 0)
            {
                _content.anchoredPosition = GetCenterOnItemPosition(index);
            }
            else
            {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.Log("CENTER ON ITEM BUT ITEMS LIST IS NULL");
#endif
            }
        }

        public void MoveToLocation(float location, float duration = 0.3f)
        {
            if (_itemsList != null && _itemsList.Count > 0)
            {
                float positionX = 0;
                float positionY = 0;

                switch (_scrollOrientation)
                {
                    case ScrollOrientation.HORIZONTAL:
                        positionX = location;
                        break;

                    case ScrollOrientation.VERTICAL:
                        positionY = location;
                        break;
                }

                _content.DOAnchorPos(new Vector2(positionX, positionY), duration);
            }
            else
            {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                Debug.Log("CENTER ON ITEM BUT ITEMS LIST IS NULL");
#endif
            }
        }


        private void Destroy()
        {
            _scrollRect.onValueChanged.RemoveAllListeners();
            _onValueChangeHandlers.Clear();
            _scrollRect.verticalNormalizedPosition = 1;

            if (_itemsList != null)
            {
                foreach (var item in _itemsList)
                {
                    GameObject.Destroy(item.DisplayObject);
                    item.Dispose();
                }

                _itemsList.Clear();
                _itemsList = null;
            }

            _lastPosition = -1;
            onItemLoaded = null;
        }


        public override void Dispose()
        {
            Destroy();
            base.Dispose();
        }

        #region UTILS

        private float GetContentPosition()
        {
            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    return _content.anchoredPosition.x;

                case ScrollOrientation.VERTICAL:
                    return _content.anchoredPosition.y;

                default:
                    return 0;
            }
        }

        private float GetViewportSize()
        {
            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    return _viewport.rect.width;

                case ScrollOrientation.VERTICAL:
                    return _viewport.rect.height;

                default:
                    return 0;
            }
        }

        private ScrollDirection GetScrollDirection()
        {
            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    return _lastPosition < GetContentPosition() ? ScrollDirection.PREVIOUS : ScrollDirection.NEXT;

                case ScrollOrientation.VERTICAL:
                    return _lastPosition > GetContentPosition() ? ScrollDirection.PREVIOUS : ScrollDirection.NEXT;

                default:
                    return ScrollDirection.NEXT;
            }
        }

        private float GetDragDelta(Vector2 initial, Vector2 current)
        {
            switch (_scrollOrientation)
            {
                case ScrollOrientation.HORIZONTAL:
                    return current.x - initial.x;

                case ScrollOrientation.VERTICAL:
                    return (current.y - initial.y) * -1;

                default:
                    return 0;
            }
        }

        #endregion
    }
}