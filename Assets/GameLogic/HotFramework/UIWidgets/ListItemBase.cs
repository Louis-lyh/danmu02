using System;
using UnityEngine;

namespace GameHotfix.Framework
{
    public class ListItemBase : UIBaseView
    {
        #region HANDLER Selected

        public float Alpha
        {
            set
            {
                var cg = RectTransform.GetComponent<CanvasGroup>();
                if (!cg)
                {
                    cg = DisplayObject.AddComponent<CanvasGroup>();
                }

                cg.alpha = value;
            }
        }

        #endregion
        
        public virtual ListItemBase Clone(GameObject instance)
        {
            return null;
        }

        protected override void ParseComponent()
        {
        }


        protected void AddView(Transform view, Transform parent)
        {
            view.parent = parent;
            view.localScale = Vector3.one;
            view.localPosition = Vector3.zero;
            view.localEulerAngles = Vector3.zero;
            view.gameObject.SetActive(true);
        }


        public int Index { get; set; }

        public Vector2 Size
        {
            get { return RectTransform.sizeDelta; }

            set { RectTransform.sizeDelta = value; }
        }

        public Vector2 Position
        {
            get { return RectTransform.anchoredPosition; }

            set { RectTransform.anchoredPosition = value; }
        }
    }
}