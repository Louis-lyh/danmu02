﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;
using UnityEngine; 
using UnityEngine.Networking;
using UnityEngine.UI;
using Logger = GameInit.Framework.Logger;
using Vector2 = UnityEngine.Vector2;

namespace GameHotfix.Game.Utils
{
    public class HeadImageDownloader
    {
        // 测试地址
        public static string _imageUrl = "http://www.baidu.com/img/bdlogo.png";
        // 默认头像
        private static Sprite _defaultSprite;
        
        // 玩家头像
        private static Dictionary<string, Sprite> _playerHeadImage;

        // 初始化
        public static async void Init()
        {
            // 头像
            _playerHeadImage = new Dictionary<string, Sprite>();
            
            // 默认头像
            if(_defaultSprite == null)
                _defaultSprite = await SpriteAtlasMgr.Instance.LoadSprite("icon_jian");
        }
        
        // 下载头像
        public static async UniTask<Sprite> LoadHeadSprite(string headUrl)
        {
            // 地址为空
            if (string.IsNullOrEmpty(headUrl))
                return _defaultSprite;
            
            // 有缓存返回
            if (_playerHeadImage.ContainsKey(headUrl))
                return _playerHeadImage[headUrl];
            
            // 不是地址退出
            if (!IsUrl(headUrl))
                return _defaultSprite;
            
            // 加载头像
            UnityWebRequest request = UnityWebRequestTexture.GetTexture(headUrl);
            // 检测链接错误
            try
            {
                await request.SendWebRequest();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // 返回默认头像
                return _defaultSprite;
            }
           
            // 加载成功
            if (request.result == UnityWebRequest.Result.Success)
            {
                // 头像
                Texture2D texture = DownloadHandlerTexture.GetContent(request);
                var headSprite = Texture2DToSprite(texture);
                // 缓存
                if(!_playerHeadImage.ContainsKey(headUrl))
                    _playerHeadImage.Add(headUrl,headSprite);
                
                // 在这里使用下载的纹理 
                return headSprite;
            }
            else
            {
                Logger.LogWarning($"头像地址不正确 =》 {headUrl}");
            }
            
            // 返回默认头像
            return _defaultSprite;
        }
        
        // 默认头像
        public static Sprite DefaultHead()
        {
            return _defaultSprite;
        }

        // texture 转 sprite
        private static Sprite Texture2DToSprite(Texture2D texture)
        {
            Sprite sprite = Sprite.Create(texture,new Rect(0,0,texture.width,texture.height),new Vector2(0.5f,0.5f));

            return sprite;
        }
        
        // 判断是否是Url地址
        private static bool IsUrl(string value)
        {
            if (value.Length < 8)
                return false;
            
            if((value.Substring(0,7) != "http://") 
               && (value.Substring(0,8) != "https://"))
            {
                return false;
            }
            return true;
        }


        public static void Clear()
        {
            _playerHeadImage.Clear();
        }
    }
}

