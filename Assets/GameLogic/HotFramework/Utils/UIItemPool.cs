﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace GameHotfix.Game.Utils
{
    /// <summary>
    /// ui节点对象池
    /// </summary>
    public static class UIItemPool
    {
        // 对象池根节点
        private static Transform _uiItemRoot;
        // 对象池
        private static Dictionary<string, List<Transform>> _uiItemPool;
        // 初始化
        public static void Init()
        {
            // 找到根节点
            var uiRoot = GameObject.Find("UIRoot");
            _uiItemRoot = uiRoot.transform.Find("UIItemPoolRoot");
            // 初始化对象池
            _uiItemPool = new Dictionary<string, List<Transform>>();
        }
        
        // 获取对象
        public static Transform GetUIItem(Transform uiItem)
        {
            // 名字
            var name = uiItem.name;
            // ui对象
            Transform item = null;
            // 存在
            if (_uiItemPool.ContainsKey(name))
            {
                var uiItems = _uiItemPool[name];
                var count = uiItems.Count;
                // 取一个对象
                if (count > 0)
                {
                    item = uiItems[0];
                    uiItems.RemoveAt(0);
                }
                // 实例化对象
                else
                {
                    item = GameObject.Instantiate(uiItem);
                    item.name = name;
                }
            }
            // 不存在
            else
            {
                // 增加对象池列表
                var items = new List<Transform>();
                _uiItemPool.Add(name,items);
                // 实例化对象
                item = GameObject.Instantiate(uiItem);
                item.name = name;
            }
            // 设置缩放
            item.localScale = Vector3.one;
            return item;
        }
        
        // 回收对象
        public static void CollectItem(Transform uiItem)
        {
            // 名字
            var name = uiItem.name;
            // 设置根节点
            uiItem.SetParent(_uiItemRoot);
            // 设置缩放
            uiItem.localScale = Vector3.zero;
            //
            List<Transform> items = null;
            // 存在队列
            if (_uiItemPool.ContainsKey(name))
            {
                items = _uiItemPool[name];
            }
            // 不存在创建
            else
            {
                items = new List<Transform>(); 
                _uiItemPool.Add(name,items);
            }
            
            // 加入列表
            items.Add(uiItem);
        }
        // 销毁
        public static void Destory()
        {
            if (_uiItemRoot != null)
            {
                // 删除item
                for (int i = _uiItemRoot.childCount - 1; i >= 0; i--)
                {
                    GameObject.Destroy(_uiItemRoot.GetChild(i).gameObject);
                }
            }
            
            // 清空对象池
            _uiItemPool.Clear();
        }

    }
}