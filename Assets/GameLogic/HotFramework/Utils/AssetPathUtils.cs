using Cysharp.Text;

namespace GameHotfix.Framework
{
    public static class AssetPathUtils
    {
        public static string GetAtlasPrefab(string name)
        {
            return ZString.Concat("Atlas/", name, ".prefab");
        }

        public static string GetSpriteAtlasPath(string name)
        {
            return ZString.Concat("Atlas/", name, ".spriteAtlas");
        }

        //获取UI路径
        public static string GetUIPrefab(string name)
        {
            return ZString.Concat("UI/", name, ".prefab");
        }

        //获取配置数据
        public static string GetConfigPath(string cfgName)
        {
            return ZString.Concat("Config/", cfgName, ".bytes");
        }

        public static string GetFontPath(string fontName)
        {
            return ZString.Concat("Fonts/", fontName);
        }
    }
}