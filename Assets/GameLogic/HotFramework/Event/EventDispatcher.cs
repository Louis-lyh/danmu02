using System;
using System.Collections.Generic;

namespace GameHotfix.Framework
{
    public class EventDispatcher : IDispose
    {
        private Dictionary<string, Action<BaseEvent>> _dictEventActions;

        public EventDispatcher()
        {
            _dictEventActions = new Dictionary<string, Action<BaseEvent>>();
        }

        public bool HasEventListener(string type)
        {
            return _dictEventActions.ContainsKey(type);
        }

        public void AddEventListener(string type, Action<BaseEvent> listener)
        {
            if (_dictEventActions.ContainsKey(type))
            {
                var dictListener = _dictEventActions[type];
                dictListener = (Action<BaseEvent>)Delegate.Combine(dictListener, listener);
                _dictEventActions[type] = dictListener;
            }
            else
            {
                _dictEventActions.Add(type, listener);
            }
        }

        public void DispatchEvent(string type, object eventObj = null)
        {
            if (!_dictEventActions.ContainsKey(type))
                return;
            var dictListener = _dictEventActions[type];
            if (dictListener == null)
            {
                return;
            }
            var baseEvent = new BaseEvent(type, eventObj);
            dictListener.Invoke(baseEvent);
        }

        public void DispatchEvent<T>(T evt) where T : BaseEvent
        {
            if (!_dictEventActions.ContainsKey(evt.Type))
                return;
            var dictListener = _dictEventActions[evt.Type];
            if (dictListener == null)
            {
                return;
            }
            dictListener.Invoke(evt);
        }

        public void RemoveEventListener(string type, Action<BaseEvent> listener)
        {
            if (!_dictEventActions.ContainsKey(type))
                return;
            var dictListener = _dictEventActions[type];
            dictListener = (Action<BaseEvent>) Delegate.Remove(dictListener, listener);
            if (dictListener == null)
            {
                _dictEventActions.Remove(type);
            }
            else 
            {
                _dictEventActions[type] = dictListener;
            }
        }
        
        public void RemoveAllEventListener()
        {
            if (_dictEventActions == null)
                return;
            _dictEventActions.Clear();
        }

        public virtual void Dispose()
        {
            RemoveAllEventListener();
            _dictEventActions = null;
        }
    }
}