namespace GameHotfix.Framework
{
    public abstract class UpdateBase : IDispose
    {
        protected bool _isInitialed;

        protected virtual void OnInitialize()
        {
            _isInitialed = true;
        }
        
        public void Update(float deltaTime)
        {
            if (!_isInitialed)
                return;
            OnUpdate(deltaTime);
        }
        
        public void Dispose()
        {
            OnDispose();
            _isInitialed = false;
        }

        protected virtual void OnUpdate(float deltaTime)
        {
            
        }
        
        protected virtual void OnDispose()
        {
            
        }
    }
}