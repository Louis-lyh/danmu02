using Cysharp.Threading.Tasks;
using GameInit.Framework;

namespace GameHotfix.Framework
{
    public abstract class StageBase : IDispose
    {
        private int _stageType;
        private string _stageScene;
        protected StageBase(int stageType, string stageScene = "")
        {
            _stageType = stageType;
            _stageScene = stageScene;
        }

        public async UniTask Enter(params object[] args)
        {
            if (_stageScene.IsValid())
            {
                await ResourceManager.LoadSceneAsync(_stageScene);
                await UniTask.DelayFrame(10);
            }
            OnEnter(args);
        }

        public async UniTask Exit()
        {
            if (_stageScene.IsValid())
                ResourceManager.UnloadSceneAsync(_stageScene);
            OnExit();
        }

        public void Update(float deltaTime)
        {
            OnUpdate(deltaTime);
        }

        protected virtual void OnUpdate(float deltaTime)
        {
        }

        protected abstract void OnExit();
        
        protected abstract void OnEnter(params object[] args);

        public virtual void Dispose()
        {
            
        }
    }
}