using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameInit.Framework;

namespace GameHotfix.Framework
{
    public class StageManager : Singleton<StageManager>
    {
        private Dictionary<int, StageBase> _dictAllStages;
        private int _stageType;
        private StageBase _curStageBase;
        private bool _isInit;
        public void Init()
        {
            _dictAllStages = new Dictionary<int, StageBase>();
        }

        public void RegisterStage(int stageType, StageBase stage)
        {
            if (_dictAllStages.ContainsKey(stageType))
            {
                Logger.LogError("StageManager.RegisterStage() => stage 重复注册, id:" + stageType);
                return;
            }
            _dictAllStages.Add(stageType, stage);
        }

        public async void EnterStage(int type, params object[] args)
        {
            if (type == _stageType)
                return;
            _curStageBase?.Exit();
            if (!_dictAllStages.TryGetValue(type, out _curStageBase))
            {
                Logger.LogError("StageManager.EnterStage() => 进入Stage失败，stage type:" + type + "未注册!!");
                return;
            }

            _isInit = false;
            await _curStageBase.Enter(args);
            _isInit = true;
            _stageType = type;
        }

        public void Update(float deltaTime)
        {
            if (!_isInit)
                return;
            _curStageBase?.Update(deltaTime);
        }

        public void Destroy()
        {
            _curStageBase?.Dispose();
            _curStageBase = null;
            _dictAllStages?.Clear();
            _dictAllStages = null;
            _isInit = false;
        }
    }
}