using System.Collections.Generic;
using Cysharp.Threading.Tasks;
//using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Framework
{
    public class ConfigEvent
    {
        public const string CONFIG_RELOAD = "configReload";
    }

    public class ConfigManager : DataModelBase<ConfigManager>
    {
        private readonly Dictionary<string, BaseConfig> _allConfigs;// = new Dictionary<string, BaseConfig>();

        public ConfigManager()
        {
            _allConfigs = new Dictionary<string, BaseConfig>();
            UserGroup.Instance.SetChangeLabelListener(OnUserGroupLabelChange);
        }

        private void OnUserGroupLabelChange(string key, string value)
        {
            var level = UserGroup.Instance.SelfMatchLevel();
            if (_level == level)
                return;
            _level = level;
            ReloadConfig();
        }

        public void AddConfig(string cfgName, BaseConfig config)
        {
            if (_allConfigs.ContainsKey(cfgName))
            {
                Logger.LogMagenta("ConfigMgr.AddConfig() => 重复添加配置表，cfgName:" + cfgName);
                return;
            }

            _allConfigs[cfgName] = config;
        }

        public async UniTask LoadConfig(bool isReload = false)
        {
            var level = GetLevel();
            foreach (var kv in _allConfigs)
            {
                var path = ResPathUtils.GetConfigPath(kv.Key);
                if (!ResourceManager.AssetExists(path))
                {
                    if(isReload)
                        continue;
                    path = ResPathUtils.GetConfigPath(kv.Key);
                }
                var configText = await ResourceManager.LoadAssetAsync<TextAsset>(path);
                kv.Value.Deserialize(configText?.bytes);
            }
        }

        private async UniTask ReloadConfig()
        {
            await DownloadMgr.Instance.StartDownload();
            await LoadConfig(true);
            DispatchEvent(ConfigEvent.CONFIG_RELOAD);
        }

        private string _level = "";
        private string GetLevel()
        {
            _level = UserGroup.Instance.SelfMatchLevel();
            return _level;
        }

        public void Destroy()
        {
            UserGroup.Instance.SetChangeLabelListener(null);
            _allConfigs.Clear();
        }
    }
}