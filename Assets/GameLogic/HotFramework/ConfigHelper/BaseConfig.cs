﻿namespace GameHotfix.Framework
{
    public abstract class BaseConfig
    {
        public abstract void Deserialize(byte[] bytes);
    }
}