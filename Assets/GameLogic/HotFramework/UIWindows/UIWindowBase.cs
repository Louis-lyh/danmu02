using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;

namespace GameHotfix.Framework
{
    public enum WindowLayer
    {
        Bottom,
        Middle,
        Top,
        Guide,
    }
    public abstract class UIWindowBase : UIBaseView
    {
        public uint WindowId { get; protected set; }
        public WindowLayer Layer { get; protected set; }

        protected string _uiName;
        private bool _isActive;
        protected UIWindowBase()
        {
            InitWindowParam();
        }

        protected abstract void InitWindowParam();

        protected void OnClose()
        {
            WindowManager.Instance.CloseWindow(this);
        }

        public async UniTask Open(params object[] args)
        {
            _isActive = true;
            if (DisplayObject != null)
            {
                Show(args);
                return;
            }

            var o = await ResourceManager.ConstructObjAsync(ResPathUtils.GetUIPrefab(_uiName));
            if (!_isActive)
            {
                ResourceManager.CollectObj(o);
                return;
            }
            OnPrefabLoaded(o);
            Show(args);
            OnWindowOpenComplete();
        }

        public virtual void Close()
        {
            if (!_isActive)
                return;
            _isActive = false;
            Hide();
        }

        public override void Dispose()
        {
            _isActive = false;
            if (DisplayObject != null)
            {
                ResourceManager.CollectObj(DisplayObject);
            }
            base.Dispose();
        }

        private void OnPrefabLoaded(GameObject uiPrefab)
        {
            SetDisplayObject(uiPrefab);
            WindowManager.Instance.AddWindowToStage(this);
        }

        public virtual bool Stack()
        {
            return true;
        }

        public void OnWindowOpenComplete()
        {
            Global.DispatchEvent("OnWindowOpenComplete");
        }
    }
}