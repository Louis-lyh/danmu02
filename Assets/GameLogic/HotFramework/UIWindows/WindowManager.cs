using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameHotfix.Game;
using UnityEngine;
using UnityEngine.UI;
using Logger = GameInit.Framework.Logger;

namespace GameHotfix.Framework
{
    public class WindowManager : Singleton<WindowManager>
    {
        private Dictionary<uint, UIWindowBase> _allWindows;
        private Dictionary<WindowLayer, UIWindowBase> _showWindows;
        private Dictionary<WindowLayer, Stack<uint>> _dictHideStack;

        private RectTransform _bottomRoot;
        private RectTransform _middleRoot;
        private RectTransform _topRoot;
        private RectTransform _guideRoot;
        private CanvasScaler _canvasScaler;

        public Camera UICamera { get; private set; }
        public Canvas UICanves { get; private set; }

        public void Init()
        {
            _allWindows = new Dictionary<uint, UIWindowBase>();
            _showWindows = new Dictionary<WindowLayer, UIWindowBase>();
            _dictHideStack = new Dictionary<WindowLayer, Stack<uint>>();

            var uiRoot = GameObject.Find("UIRoot");
            UICamera = uiRoot.transform.Find("UICamera").GetComponent<Camera>();
            _canvasScaler = uiRoot.transform.Find("UICanvas").GetComponent<CanvasScaler>();
            _bottomRoot = uiRoot.transform.Find("UICanvas/BottomRoot") as RectTransform;
            _middleRoot = uiRoot.transform.Find("UICanvas/MiddleRoot") as RectTransform;
            _topRoot = uiRoot.transform.Find("UICanvas/TopRoot") as RectTransform;
            _guideRoot = uiRoot.transform.Find("UICanvas/GuideRoot") as RectTransform;

            UICanves = uiRoot.transform.Find("UICanvas").GetComponent<Canvas>();

            AlertDialogView.AlertRoot = _topRoot;
            AlertDialogView.GuildRoot = _guideRoot;
            
            InitCanvas();
        }

        private void InitCanvas()
        {
            if (Screen.height * 9 > Screen.width * 16)
            {
                _canvasScaler.matchWidthOrHeight = 0; // fixed with width
            }
            else
            {
                _canvasScaler.matchWidthOrHeight = 1; // fixed with height
            }
        }

        public bool FixedWithWidth()
        {
            return _canvasScaler.matchWidthOrHeight < 0.5;
        }

        public float GetAdapterFactor()
        {
            if (FixedWithWidth())
            {
                return 768.0f / Screen.width;
            }

            return 1366.0f / Screen.height;
        }

        public UniTask OpenWindow(uint windowId, params object[] args)
        {
            if (!_allWindows.TryGetValue(windowId, out var window))
            {
                Logger.LogError("打开Window失败，windowId:" + windowId + "未注册");
                return UniTask.CompletedTask;
            }

            if (_showWindows.TryGetValue(window.Layer, out var showWindow))
            {
                if (showWindow == window)
                {
                    window.Show(args);
                    window.OnWindowOpenComplete();
                    return UniTask.CompletedTask;
                }

                if (showWindow.Stack())
                {
                    if (!_dictHideStack.TryGetValue(showWindow.Layer, out var hideStack))
                    {
                        hideStack = new Stack<uint>();
                        _dictHideStack[showWindow.Layer] = hideStack;
                    }

                    if (!hideStack.Contains(showWindow.WindowId))
                        hideStack.Push(showWindow.WindowId);
                    showWindow.Hide();
                }
            }

            _showWindows[window.Layer] = window;
            return window.Open(args);
        }

        public void CloseWindow(uint windowId)
        {
            if (_allWindows.TryGetValue(windowId, out var window))
            {
                CloseWindow(window);
            }
        }

        public void CloseWindow(UIWindowBase uiWindow)
        {
            if (_showWindows.ContainsKey(uiWindow.Layer))
                _showWindows.Remove(uiWindow.Layer);
            uiWindow.Close();
            if (!uiWindow.Stack())
                return;
            if (_dictHideStack.TryGetValue(uiWindow.Layer, out var stack) && stack.Count > 0)
            {
                OpenWindow(stack.Pop());
            }
        }

        public void RegisterWindow(uint moduleId, UIWindowBase uiWindow)
        {
            if (_allWindows.ContainsKey(moduleId))
            {
                Logger.LogMagenta("重复注册Module, window id:" + moduleId);
                return;
            }

            _allWindows[moduleId] = uiWindow;
        }

        public void AddWindowToStage(UIWindowBase window)
        {
            switch (window.Layer)
            {
                case WindowLayer.Bottom:
                    window.SetParent(_bottomRoot);
                    break;
                case WindowLayer.Middle:
                    window.SetParent(_middleRoot);
                    break;
                case WindowLayer.Top:
                    window.SetParent(_topRoot);
                    break;
                case WindowLayer.Guide:
                    window.SetParent(_guideRoot);
                    break;
            }
            //Debug.Log($"AddWindowToStage() " +
            //    $"name:{window.RectTransform.name} parent:{window.RectTransform.parent.name}");
        }

        public UIWindowBase GetWindow(uint moduleId)
        {
            UIWindowBase uiWindow = null;
            _allWindows.TryGetValue(moduleId, out uiWindow);
            return uiWindow;
        }

        public T GetWindow<T>(uint moduleId) where T : UIWindowBase
        {
            var window = GetWindow(moduleId);
            return (T)window;
        }

        public void AddGameObjectToStage(GameObject gameObject, WindowLayer layer, Vector3 localPos = default)
        {
            if (gameObject == null)
                return;
            var parent = _bottomRoot;
            switch (layer)
            {
                case WindowLayer.Bottom:
                    parent = _bottomRoot;
                    break;
                case WindowLayer.Middle:
                    parent = _middleRoot;
                    break;
                case WindowLayer.Top:
                    parent = _topRoot;
                    break;
                case WindowLayer.Guide:
                    parent = _guideRoot;
                    break;
            }
            gameObject.transform.SetParent(parent, false);
            gameObject.transform.localPosition = localPos;
        }

        public void Destroy()
        {
            if (_showWindows != null)
            {
                foreach(var kv in _showWindows)
                    kv.Value.Dispose();
                _showWindows = null;
            }

            if (_allWindows != null)
            {
                foreach (var kv in _allWindows)
                    kv.Value.Dispose();
                _allWindows.Clear();
            }
            
            _dictHideStack?.Clear();
            _dictHideStack = null;
        }
    }
}