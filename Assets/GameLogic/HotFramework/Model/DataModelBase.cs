namespace GameHotfix.Framework
{
    /// <summary>
    /// 数据Model基类，带事件分发机制
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DataModelBase<T> : EventDispatcher where T :new()
    {
        private static T _instance;

        public static T Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new T();
                return _instance;
            }
        }
    }
}