namespace GameHotfix.Framework
{
    public interface IDispose
    {
        void Dispose();
    }
}