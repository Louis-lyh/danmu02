using System;

namespace GameHotfix.Framework
{
    public static class Global
    {
        private static readonly EventDispatcher _dispatcher;

        static Global()
        {
            _dispatcher = new EventDispatcher();
        }
        
        #region 全局事件

        public static void AddEventListener(string type, Action<BaseEvent> listener)
        {
            _dispatcher?.AddEventListener(type, listener);
        }

        public static void DispatchEvent(string type, object eventObj = null)
        {
            _dispatcher?.DispatchEvent(type, eventObj);
        }
        
        public static void DispatchEvent<T>(T evt) where T : BaseEvent
        {
            _dispatcher?.DispatchEvent(evt);
        }

        public static void RemoveEventListener(string type, Action<BaseEvent> listener)
        {
            _dispatcher?.RemoveEventListener(type, listener);
        }

        #endregion
    }
}