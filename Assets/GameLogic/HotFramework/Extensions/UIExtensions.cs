﻿using System;
using DG.Tweening;
using GameInit.Framework;
using GameInit.Game;
using UnityEngine;
using UnityEngine.UI;

namespace GameHotfix.Framework
{
    public static class UIExtensions
    {
        public static void SetLanguageID(this Text self, int languageId, string extContent = "")
        {
            if (LocationManager.TodoGetLocationTextByHashCode(self.GetHashCode(), out var locationText))
                locationText.SetLanguageID(languageId, extContent);
        }

        public static void Click(this Button self, Action action,bool openAni = true)
        {
            self.onClick.AddListener(() =>
            {
                action?.Invoke();
                if (openAni)
                {
                    Sequence quence = DOTween.Sequence();
                    quence.Append(self.transform.DOScale(Vector3.one * 0.8f, 0.1f));
                    quence.Append(self.transform.DOScale(Vector3.one, 0.1f));
                }
            });
        }

        /// <summary>
        /// 将十六进制颜色转成 Color
        /// </summary>
        /// <param name="hexColor">带#号的16进制颜色</param>
        /// <returns></returns>
        private static Color ColorFromHex(string hexColor)
        {
            ColorUtility.TryParseHtmlString(hexColor, out var color);
            return color;
        }

        // 设置文本颜色
        public static void SetTextColor(this GameObject obj, Color color)
        {
            if (obj != null)
            {
                SetTextColor(obj.GetComponent<Text>(), color);
            }
        }

        public static void SetTextColor(this Text text, Color color)
        {
            if (text != null)
            {
                text.color = color;
            }
        }

        public static void SetTextColor(this GameObject obj, string hexColor) =>
            SetTextColor(obj, ColorFromHex(hexColor));

        public static void SetTextColor(this Text text, string hexColor) => SetTextColor(text, ColorFromHex(hexColor));

        // 设置物体描边颜色
        public static void SetOutlineColor(this GameObject obj, Color color)
        {
            if (obj != null)
            {
                Outline outline = obj.GetComponent<Outline>();
                if (outline != null)
                    outline.effectColor = color;
            }
        }

        public static void SetOutlineColor(this GameObject obj, string hexString) =>
            SetOutlineColor(obj, ColorFromHex(hexString));

        public static void SetOutlineColor(this Text text, Color color)
        {
            if (text != null)
            {
                SetOutlineColor(text.gameObject, color);
            }
        }

        public static void SetOutlineColor(this Text text, string hexString) =>
            SetOutlineColor(text, ColorFromHex(hexString));

        // 设置物体阴影颜色
        public static void SetShadowColor(this GameObject obj, Color color)
        {
            if (obj != null)
            {
                Shadow shadow = obj.GetComponent<Shadow>();
                if (shadow != null)
                    shadow.effectColor = color;
            }
        }

        public static void SetShadowColor(this GameObject obj, string hexColor) =>
            SetShadowColor(obj, ColorFromHex(hexColor));

        public static void SetShadowColor(this Text text, Color color)
        {
            if (text != null)
            {
                SetShadowColor(text.gameObject, color);
            }
        }

        public static void SetShadowColor(this Text text, string hexColor) =>
            SetShadowColor(text, ColorFromHex(hexColor));

        /// <summary>
        /// 置灰
        /// </summary>
        /// <param name="target"></param>
        /// <param name="isGray">是否置灰</param>
        public static async void SetGray(this GameObject target, bool isGray = true)
        {
            if (isGray)
            {
                var mat = await ResourceManager.LoadAssetAsync<Material>("Material/Gray.mat");
                target.SetMaterial(mat);
            }
            else
            {
                target.SetMaterial(null);
            }
        }

        private static void SetMaterial(this GameObject target, Material material)
        {
            if (target == null) return;

            var image = target.GetComponent<Image>();
            if (image != null)
                image.material = material;

            if (target.transform.childCount > 0)
            {
                for (var i = 0; i < target.transform.childCount; ++i)
                {
                    var obj = target.transform.GetChild(i);
                    SetMaterial(obj.gameObject, material);
                }
            }
        }

        public static void DoFadeOut(this Transform target, float time, Action callback = null)
        {
            target.DoFade(1, 0, time, callback);
        }

        public static void DoFadeIn(this Transform target, float time, Action callback = null)
        {
            target.DoFade(0, 1, time, callback);
        }

        private static void DoFade(this Transform target, float from, float to, float time, Action callback = null)
        {
            var group = target.GetComponent<CanvasGroup>();
            if (!group)
            {
                group = target.gameObject.AddComponent<CanvasGroup>();
            }

            group.alpha = from;
            DOTween.Sequence().Append(group.DOFade(to, time)).AppendCallback(() => { callback?.Invoke(); });
        }

        public static void ChangeLayer(this Transform target, int layer, bool recursion = true)
        {
            target.gameObject.layer = layer;
            if (recursion)
            {
                foreach (Transform child in target)
                {
                    ChangeLayer(child, layer);
                }
            }
        }

        public static Tween TweenTextNum(this Text target, int num)
        {
            float.TryParse(target.text, out var old);
            if (num <= old)
            {
                target.text = num.ToString();
                return null;
            }

            const float n = 20.0f;
            var dt = (num - old) / n;
            return DOTween.Sequence().AppendCallback(() =>
            {
                old += dt;
                target.text = ((int) old).ToString();
            }).AppendInterval(1 / 40.0f).SetLoops((int) n - 1).AppendCallback(() => { target.text = num.ToString(); });
        }


        public static void AdapterFullScreen(this RectTransform rect)
        {
            const int offset = 72;
            if (Screen.height * 9 < Screen.width * 18)
            {
                return;
            }

            if (rect.anchorMax.y - rect.anchorMin.y < 1e-3)
            {
                if (rect.anchorMax.y < 1e-3)
                {
                    // 底对齐
                    rect.localPosition += new Vector3(0, offset, 0);
                }

                if (rect.anchorMax.y > 1 - 1e-3)
                {
                    // 顶对齐v
                    rect.localPosition -= new Vector3(0, offset, 0);
                }

                return;
            }

            // 铺满尺寸
            if (rect.anchorMin.x - rect.anchorMin.y < 1e-3 && rect.anchorMax.x - rect.anchorMax.y < 1e-3)
            {
                if (rect.anchorMin.x < 1e-3 && rect.anchorMax.x > 1 - 1e-3)
                {
                    rect.sizeDelta -= new UnityEngine.Vector2(0, offset * 2);
                }
            }
        }
    }
}