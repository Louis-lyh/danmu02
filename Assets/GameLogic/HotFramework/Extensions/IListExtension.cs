using System.Collections.Generic;
using UnityEngine;

namespace GameHotfix.Framework
{
    public static class IListExtension
    {
        public static T RandomOne<T>(this IList<T> self)
        {
            if (self == null)
                return default(T);
            var index = Random.Range(0, self.Count);
            return self[index];
        }
    }
}