﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameHotfix.Game;
using GameInit.Framework;
using UnityEngine;

namespace GameHotfix.Framework
{
    public abstract class AlertDialogView : UIBaseView
    {
        private readonly string _uiName;

        private bool _isForceHide;

        protected AlertDialogView(string uiName)
        {
            _uiName = uiName;
        }

        private async UniTask InitialDialog()
        {
            if (DisplayObject == null)
            {
                var o = await ResourceManager.ConstructObjAsync(ResPathUtils.GetUIPrefab(_uiName));
                SetDisplayObject(o);
            }
        }

        private void ForceHide()
        {
            if (DisplayObject == null)
            {
                _isForceHide = true;
                return;
            }

            Hide();
        }

        protected void CloseAlert()
        {
            DialogOut();
            if (OtherDialogs.Contains(this))
            {
                OtherDialogs.Remove(this);
            }
        }

        protected void DoCloseAlert()
        {
            ResourceManager.CollectObj(DisplayObject);
            Dispose();
            OnAlertClose(this);
        }

        /// <summary>
        /// 是否需要走队列
        /// </summary>
        /// <returns></returns>
        protected virtual bool Stack()
        {
            return true;
        }

        public static void Destroy()
        {
            if (StackDialogs != null)
            {
                while (StackDialogs.Count > 0)
                {
                    var dialog = StackDialogs.Dequeue();
                    dialog.Dispose();
                }
            }

            _stackShowView?.Dispose();
            _stackShowView = null;
        }

        #region Alert显示逻辑，队列管理，纯静态方法

        private static readonly Queue<AlertDialogView> StackDialogs = new Queue<AlertDialogView>();
        private static readonly List<AlertDialogView> OtherDialogs = new List<AlertDialogView>();
        public static RectTransform AlertRoot;
        public static RectTransform GuildRoot;
        private static AlertDialogView _stackShowView;
        protected WindowLayer _windowLayer = WindowLayer.Top;

        public static bool DialogOpening(string dialogName)
        {
            if (_stackShowView != null && string.Equals( _stackShowView._uiName,dialogName))
            {
                return true;
            }
            return false;
        }

        private static void OnAlertClose(AlertDialogView view)
        {
            if (!view.Stack())
            {
                return;
            }
            _stackShowView = null;
            if (StackDialogs.Count == 0)
                return;
            var dialog = StackDialogs.Dequeue();
            ShowAlertView(dialog, dialog._data);
            _stackShowView = dialog;
        }

        public static void LaunchImmediate(AlertDialogView view, params object[] args)
        {
            view._data = args;
            if (_stackShowView != null)
            {
                StackDialogs.Enqueue(_stackShowView);
                _stackShowView.ForceHide();
            }

            _stackShowView = view;
            ShowAlertView(view, args);
        }

        public static void Launch(AlertDialogView view, params object[] args)
        {
            view._data = args;
            if (view.Stack())
            {
                if (_stackShowView != null)
                {
                    StackDialogs.Enqueue(view);
                    return;
                }

                _stackShowView = view;
            }
            else
            {
                OtherDialogs.Add(view);
            }

            ShowAlertView(view, args);
        }

        private static async void ShowAlertView(AlertDialogView view, params object[] args)
        {
            await view.InitialDialog();
            view._data = args;
            if (view._isForceHide)
            {
                view._isForceHide = false;
                return;
            }

            view.Show(args);
            if (view._windowLayer == WindowLayer.Guide)
            {
                view.SetParent(GuildRoot);
            }
            else
            {
                view.SetParent(AlertRoot);
            }
            view.DialogIn();
            view.ShowComplete();
        }

        public static void CloseOtherDialog()
        {
            for (int i = 0; i < OtherDialogs.Count; i++)
            {
                OtherDialogs[i].CloseAlert();
            }
            OtherDialogs.Clear();
        }

        #endregion

        #region Alert淡入淡出

        protected bool ShowFadeAni = true; // 显示淡入淡出动画

        private void DialogIn()
        {
            if (!ShowFadeAni)
            {
                Global.DispatchEvent("OnDialogOpenComplete");
                return;
            }

            var group = GetCanvasGroup();
            RectTransform.localPosition = new Vector3(0, 200, 0);
            group.alpha = 0;
            DOTween.Sequence().SetUpdate(true).Append(group.DOFade(1, 0.25f))
                .Join(RectTransform.DOLocalMove(new Vector3(0, -10, 0), 0.25f))
                .Append(RectTransform.DOLocalMove(Vector3.zero, 0.1f))
                .OnComplete(() =>
                {
                    Global.DispatchEvent("OnDialogOpenComplete");
                });
        }

        private void DialogOut()
        {
            if (!ShowFadeAni)
            {
                DoCloseAlert();
                return;
            }

            var group = GetCanvasGroup();
            group.alpha = 1;
            DOTween.Sequence().SetUpdate(true).Append(RectTransform.DOLocalMove(new Vector3(0, -10, 0), 0.1f))
                .Append(group.DOFade(0, 0.25f)).Join(RectTransform.DOLocalMove(new Vector3(0, 200, 0), 0.25f))
                .AppendCallback(() =>
                {
                    RectTransform.localPosition = Vector3.zero;
                    group.alpha = 1;
                    DoCloseAlert();
                });
        }

        private CanvasGroup GetCanvasGroup()
        {
            var group = RectTransform.GetComponent<CanvasGroup>();
            if (!group)
            {
                group = RectTransform.gameObject.AddComponent<CanvasGroup>();
            }

            return group;
        }

        #endregion
    }
}