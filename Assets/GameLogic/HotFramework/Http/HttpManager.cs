﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Cysharp.Threading.Tasks;
using GameInit.Game;
using GameInit.Framework;
using LitJson;
using UnityEngine.Networking;

namespace GameHotfix.Framework
{
    public static class HttpManager
    {
        private static string URL;
        private static string SECRET_KEY;
        private static string SOLT_KEY;
        
        public static Action<int> OnNetErrorAction;

        #region Post

        public static async UniTask<string> Post(string url, string postData = null, Dictionary<string, string> header = null)
        {
            var request = new UnityWebRequest(url, "POST");
            if (postData.IsValid())
            {
                var bytes = Encoding.UTF8.GetBytes(postData);
                request.uploadHandler = new UploadHandlerRaw(bytes);
            }
            request.downloadHandler = new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "text/plain");
            request.certificateHandler = new WebRequestCertificate();
            request.timeout = 3;
            request.redirectLimit = 0;
            request.useHttpContinue = false;
            request.chunkedTransfer = false;
            string result = null;
            try
            {
                if (header != null)
                {
                    foreach (var kv in header)
                    {
                        if(kv.Value == null)
                            continue;
                        request.SetRequestHeader(kv.Key, kv.Value);
                    }
                }
                var async = await request.SendWebRequest();
                if (async.result != UnityWebRequest.Result.Success)
                {
                    OnNetErrorAction?.Invoke((int)async.result);
                    Logger.LogError("HttpManager.Get() => Get请求失败,ErrorCode:" + async.result);
                    return null;
                }
                result = Encoding.UTF8.GetString(async.downloadHandler.data);
                async.Dispose();
            }
            catch (Exception e)
            {
                Logger.LogError("HttpManager.Post() => Post请求异常,Error:" + e.Message);
            }
            request.Dispose();
            if (!result.IsValid())
                return null;
            var t = JsonMapper.ToObject(result);
            if (t == null || !t.Keys.Contains("code"))
                return null;
            int.TryParse(t["code"].ToJson(), out var code);
            if (code <= 1)
                return t["data"].ToJson();
            OnNetErrorAction?.Invoke(code);
            return null;
        }
        #endregion

        #region Get
        
        public static async UniTask<string> Get(string url)
        {
            if (url.StartsWith("https"))
                ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
            var request = UnityWebRequest.Get(url);
            request.timeout = 5000;
            string result = null;
            try
            {
                var async = await request.SendWebRequest();
                if (async.result == UnityWebRequest.Result.Success)
                {
                    //成功
                    result = Encoding.UTF8.GetString(async.downloadHandler.data);
                }
                else
                {
                    Logger.LogError("HttpManager.Get() => Get请求失败,error:" + async.error);
                }
            }
            catch (Exception e)
            {
                Logger.LogError("HttpManager.Get() => Get请求出错,Error:" + e.Message);
            }
            request.Dispose();
            return result;
        }

        private static bool MyRemoteCertificateValidationCallback(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            var isOk = true;
            // If there are errors in the certificate chain,
            // look at each error to determine the cause.
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                for (int i=0; i<chain.ChainStatus.Length; i++)
                {
                    if (chain.ChainStatus[i].Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        continue;
                    }
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan (0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    var chainIsValid = chain.Build ((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                        break;
                    }
                }
            }
            return isOk;
        }
        #endregion
    }
}